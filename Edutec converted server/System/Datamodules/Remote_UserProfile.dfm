inherited rdtmUserProfile: TrdtmUserProfile
  OldCreateOrder = True
  Height = 174
  Width = 241
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_COMPLETE_USERNAME, '
      '  F_PROFILE_ID, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_USER_PROFILE'
      '')
    object adoqryListF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryListF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_COMPLETE_USERNAME, '
      '  F_PROFILE_ID, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_USER_PROFILE'
      '')
    object adoqryRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryRecordF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
