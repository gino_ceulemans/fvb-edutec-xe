{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_SYS_USER.

  @Name       Remote_User
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  09/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_User;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmUser = class(TrdtmEduBase, IrdtmUser)
    adoqryListF_USER_ID: TIntegerField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_PHONE_INT: TStringField;
    adoqryListF_PHONE_EXT: TStringField;
    adoqryListF_GSM: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryListF_LOGIN_ID: TStringField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_GENDER_ID: TIntegerField;
    adoqryListF_LOCKED: TBooleanField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_LOGIN_TRY: TSmallintField;
    adoqryListF_DOMAIN: TStringField;
    adoqryListF_LANGUAGE_ID: TIntegerField;
    adoqryListF_LANGUAGE_NAME: TStringField;
    adoqryListF_COMPLETE_USERNAME: TStringField;
    adoqryRecordF_USER_ID: TIntegerField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_PHONE_INT: TStringField;
    adoqryRecordF_PHONE_EXT: TStringField;
    adoqryRecordF_GSM: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_LOGIN_ID: TStringField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_GENDER_ID: TIntegerField;
    adoqryRecordF_LOCKED: TBooleanField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_LOGIN_TRY: TSmallintField;
    adoqryRecordF_DOMAIN: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_COMPLETE_USERNAME: TStringField;
    adoqryListF_GENDER_NAME: TStringField;
    adoqryRecordF_GENDER_NAME: TStringField;
    adoqryListF_PASSWORD: TStringField;
    adoqryRecordF_PASSWORD: TStringField;
    adospSP_DEACTIVATE_SYS_USER: TFVBFFCStoredProc;
    adoqryRecordF_PRINTER_LETTER: TStringField;
    adoqryRecordF_PRINTER_CERTIFICATE: TStringField;
    adoqryRecordF_PATH_CONFIRMATIONS: TStringField;
    adoqryRecordF_PATH_INVOICES: TStringField;
    adoqryRecordF_PRINTER_EVALUATION: TStringField;
    adoqryRecordF_PATH_CRREPORTS: TStringField;
    adoqryRecordF_EMAIL_PUBLIC_FOLDER: TStringField;
    adoqryRecordF_DEFAULT_INFO_CONFIRMATION: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofUser : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmUser.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmUser.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmUser.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmUser.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmUser.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmUser.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmUser.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmUser.GetTableName: String;
begin
  Result := 'T_SYS_USER';
end;

procedure TrdtmUser.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_SYS_USER(' + DeltaDS.FieldByName('F_USER_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_SYS_USER.Close;
    adospSP_DEACTIVATE_SYS_USER.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_USER_ID').Value;
    adospSP_DEACTIVATE_SYS_USER.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
end;

function TrdtmUser.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofUser := TComponentFactory.Create(ComServer, TrdtmUser,
    Class_rdtmUser, ciInternal, tmApartment);
end.
