inherited rdtmProfile: TrdtmProfile
  OldCreateOrder = True
  Height = 193
  Width = 316
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFILE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_PROFILE'
      '')
    object adoqryListF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFILE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_PROFILE'
      '')
    object adoqryRecordF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
