inherited rdtmUser: TrdtmUser
  OldCreateOrder = True
  Height = 167
  Width = 384
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_PHONE_INT, '
      '  F_PHONE_EXT, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_LOGIN_ID, '
      '  F_ACTIVE, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME,'
      '  F_LOCKED, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_LOGIN_TRY, '
      '  F_DOMAIN, '
      '  F_LANGUAGE_ID,'
      '  F_LANGUAGE_NAME,'
      '  F_PASSWORD, '
      '  F_COMPLETE_USERNAME'
      'FROM '
      '  V_SYS_USER')
    object adoqryListF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_PHONE_INT: TStringField
      FieldName = 'F_PHONE_INT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PHONE_EXT: TStringField
      FieldName = 'F_PHONE_EXT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_LOGIN_ID: TStringField
      FieldName = 'F_LOGIN_ID'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LOCKED: TBooleanField
      FieldName = 'F_LOCKED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LOGIN_TRY: TSmallintField
      FieldName = 'F_LOGIN_TRY'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_DOMAIN: TStringField
      FieldName = 'F_DOMAIN'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object adoqryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object adoqryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryListF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PASSWORD: TStringField
      FieldName = 'F_PASSWORD'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_PHONE_INT, '
      '  F_PHONE_EXT, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_LOGIN_ID, '
      '  F_ACTIVE, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME,'
      '  F_LOCKED, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_LOGIN_TRY, '
      '  F_DOMAIN, '
      '  F_LANGUAGE_ID,'
      '  F_LANGUAGE_NAME, '
      '  F_PASSWORD,'
      '  F_COMPLETE_USERNAME,'
      '  F_PRINTER_LETTER,'
      '  F_PRINTER_CERTIFICATE,'
      '  F_PATH_CONFIRMATIONS,'
      '  F_PATH_INVOICES,'
      '  F_PRINTER_EVALUATION,'
      '  F_PATH_CRREPORTS,'
      '  F_EMAIL_PUBLIC_FOLDER,'
      '  F_DEFAULT_INFO_CONFIRMATION'
      'FROM '
      '  V_SYS_USER'
      '')
    object adoqryRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_PHONE_INT: TStringField
      FieldName = 'F_PHONE_INT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PHONE_EXT: TStringField
      FieldName = 'F_PHONE_EXT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_LOGIN_ID: TStringField
      FieldName = 'F_LOGIN_ID'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LOCKED: TBooleanField
      FieldName = 'F_LOCKED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LOGIN_TRY: TSmallintField
      FieldName = 'F_LOGIN_TRY'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DOMAIN: TStringField
      FieldName = 'F_DOMAIN'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PASSWORD: TStringField
      FieldName = 'F_PASSWORD'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRINTER_LETTER: TStringField
      FieldName = 'F_PRINTER_LETTER'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PRINTER_CERTIFICATE: TStringField
      FieldName = 'F_PRINTER_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PATH_CONFIRMATIONS: TStringField
      FieldName = 'F_PATH_CONFIRMATIONS'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryRecordF_PATH_INVOICES: TStringField
      FieldName = 'F_PATH_INVOICES'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryRecordF_PRINTER_EVALUATION: TStringField
      FieldName = 'F_PRINTER_EVALUATION'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PATH_CRREPORTS: TStringField
      FieldName = 'F_PATH_CRREPORTS'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryRecordF_EMAIL_PUBLIC_FOLDER: TStringField
      FieldName = 'F_EMAIL_PUBLIC_FOLDER'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_DEFAULT_INFO_CONFIRMATION: TStringField
      FieldName = 'F_DEFAULT_INFO_CONFIRMATION'
      ProviderFlags = []
      ReadOnly = True
      Visible = False
      Size = 50
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_SYS_USER: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_SYS_USER;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 248
    Top = 48
  end
end
