{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_SYS_USER_PROF.

  @Name       Remote_UserProfile
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  29/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_UserProfile;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmUserProfile = class(TrdtmEduBase, IrdtmUserProfile)
    adoqryListF_USER_ID: TIntegerField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_COMPLETE_USERNAME: TStringField;
    adoqryListF_PROFILE_ID: TIntegerField;
    adoqryListF_PROFILE_NAME: TStringField;
    adoqryRecordF_USER_ID: TIntegerField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_COMPLETE_USERNAME: TStringField;
    adoqryRecordF_PROFILE_ID: TIntegerField;
    adoqryRecordF_PROFILE_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofUserProfile : TComponentFactory;
  
implementation

{$R *.DFM}

class procedure TrdtmUserProfile.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmUserProfile.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmUserProfile.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmUserProfile.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmUserProfile.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmUserProfile.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmUserProfile.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmUserProfile.GetTableName: String;
begin
  Result := 'T_SYS_USER_PROF';
end;

function TrdtmUserProfile.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofUserProfile := TComponentFactory.Create(ComServer, TrdtmUserProfile,
    Class_rdtmUserProfile, ciInternal, tmApartment);
end.
