inherited rdtmQueryWizard: TrdtmQueryWizard
  OldCreateOrder = True
  Height = 238
  Width = 689
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ''
      ' F_QUERY_GROUP_ID,'
      ' F_QUERY_GROUP_NAME,'
      ''
      ' F_QUERY_ID,'
      ' F_NAME,'
      ' F_DESCRIPTION,'
      ' F_TEXT,'
      ' F_PARAMETER_COUNT'
      ' '
      'FROM'
      ' V_DBW_QUERY'
      '')
    object adoqryListF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_QUERY_GROUP_NAME: TStringField
      FieldName = 'F_QUERY_GROUP_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_DESCRIPTION: TMemoField
      FieldName = 'F_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_TEXT: TMemoField
      FieldName = 'F_TEXT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_PARAMETER_COUNT: TIntegerField
      FieldName = 'F_PARAMETER_COUNT'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ''
      ' F_QUERY_GROUP_ID,'
      ' F_QUERY_GROUP_NAME,'
      ''
      ' F_QUERY_ID,'
      ' F_NAME,'
      ' F_DESCRIPTION,'
      ' F_TEXT,'
      ' F_PARAMETER_COUNT'
      ' '
      'FROM'
      ' V_DBW_QUERY')
    object adoqryRecordF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_QUERY_GROUP_NAME: TStringField
      FieldName = 'F_QUERY_GROUP_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_DESCRIPTION: TMemoField
      FieldName = 'F_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_TEXT: TMemoField
      FieldName = 'F_TEXT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_PARAMETER_COUNT: TIntegerField
      FieldName = 'F_PARAMETER_COUNT'
      ProviderFlags = [pfInUpdate]
    end
  end
  object adoqryGroup: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      ' F_QUERY_GROUP_ID,'
      ' F_NAME,'
      ' F_PARENT_ID,'
      ' F_PARENT_NAME,'
      ' F_QUERY_COUNT'
      'FROM'
      ' V_DBW_QUERYGROUP')
    AutoOpen = False
    Left = 224
    Top = 24
    object adoqryGroupF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryGroupF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryGroupF_PARENT_ID: TIntegerField
      FieldName = 'F_PARENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryGroupF_PARENT_NAME: TStringField
      FieldName = 'F_PARENT_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryGroupF_QUERY_COUNT: TIntegerField
      FieldName = 'F_QUERY_COUNT'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvGroup: TFVBFFCDataSetProvider
    DataSet = adoqryGroup
    OnGetTableName = prvListGetTableName
    Left = 224
    Top = 72
  end
  object adoqryParameter: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      'SELECT '
      ' F_QUERY_GROUP_ID,'
      ''
      ' F_QUERY_ID,'
      ' F_QUERY_NAME,'
      ''
      ' F_PARAMETER_ID,'
      ' F_NAME,'
      ' F_CAPTION,'
      ' F_PARAMETER_TYPE_ID,'
      ' F_PARAMETER_TYPE,'
      ' F_TABLE,'
      ' F_KEY,'
      ' F_RESULT'
      ''
      'FROM'
      ' V_DBW_PARAMETER'
      ' ')
    AutoOpen = False
    Left = 328
    Top = 24
    object IntegerField1: TIntegerField
      FieldName = 'F_QUERY_ID'
    end
    object StringField1: TStringField
      FieldName = 'F_QUERY_NAME'
      Size = 256
    end
    object IntegerField2: TIntegerField
      FieldName = 'F_PARAMETER_ID'
    end
    object StringField2: TStringField
      FieldName = 'F_NAME'
      Size = 256
    end
    object StringField3: TStringField
      FieldName = 'F_CAPTION'
      ReadOnly = True
      Size = 256
    end
    object IntegerField3: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
    end
    object StringField4: TStringField
      FieldName = 'F_PARAMETER_TYPE'
      Size = 50
    end
    object StringField5: TStringField
      FieldName = 'F_TABLE'
      Size = 256
    end
    object StringField6: TStringField
      FieldName = 'F_KEY'
      Size = 256
    end
    object StringField7: TStringField
      FieldName = 'F_RESULT'
      ReadOnly = True
      Size = 256
    end
    object IntegerField4: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvParameter: TFVBFFCDataSetProvider
    DataSet = adoqryParameter
    OnGetTableName = prvListGetTableName
    Left = 328
    Top = 72
  end
  object adospSelectLookUp: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_SELECT_LOOKUP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@TableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@FieldId'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@FieldResult'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 488
    Top = 24
  end
  object prvSelectLookUp: TFVBFFCDataSetProvider
    DataSet = adospSelectLookUp
    OnGetTableName = prvListGetTableName
    Left = 488
    Top = 72
  end
end
