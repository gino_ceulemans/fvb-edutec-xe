inherited rdtmParameterType: TrdtmParameterType
  OldCreateOrder = True
  Height = 169
  Width = 215
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      ' F_PARAMETER_TYPE_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR,'
      ' F_NAME'
      'FROM  '
      ' V_DBW_PARAMETER_TYPE')
    object adoqryListF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 50
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      ' F_PARAMETER_TYPE_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR,'
      ' F_NAME'
      'FROM  '
      ' V_DBW_PARAMETER_TYPE')
    object adoqryRecordF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 50
    end
  end
end
