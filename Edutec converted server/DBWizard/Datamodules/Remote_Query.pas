unit Remote_Query;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmQuery = class(TrdtmEduBase, IrdtmQuery)
    adoqryListF_QUERY_ID: TIntegerField;
    adoqryListF_NAME_NL: TStringField;
    adoqryListF_NAME_FR: TStringField;
    adoqryListF_QUERY_GROUP_ID: TIntegerField;
    adoqryListF_TEXT: TMemoField;
    adoqryListF_DESCRIPTION_NL: TMemoField;
    adoqryListF_DESCRIPTION_FR: TMemoField;
    adoqryListF_DESCRIPTION: TMemoField;
    adoqryListF_QUERY_GROUP_NAME: TStringField;
    adoqryRecordF_QUERY_ID: TIntegerField;
    adoqryRecordF_NAME_NL: TStringField;
    adoqryRecordF_NAME_FR: TStringField;
    adoqryRecordF_QUERY_GROUP_ID: TIntegerField;
    adoqryRecordF_TEXT: TMemoField;
    adoqryRecordF_DESCRIPTION_NL: TMemoField;
    adoqryRecordF_DESCRIPTION_FR: TMemoField;
    adoqryRecordF_DESCRIPTION: TMemoField;
    adoqryRecordF_QUERY_GROUP_NAME: TStringField;
    adoqryListF_NAME: TStringField;
    adoqryRecordF_NAME: TStringField;
    adospDBW_DeleteParameter: TFVBFFCStoredProc;
    adospDBW_UpdateParameter: TFVBFFCStoredProc;
    procedure prvRecordAfterUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind);
    procedure prvRecordBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
    function GetNewParameterID: SYSINT;
    // Extra functionaliteit om de nodige parameters te genereren ...
    procedure CreateParameterRecordsForQuery(aCon : TADOConnection; aQueryID: Integer; aSQLStatement: String);
    function GetFieldValue(const aField : TField): Variant;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofQuery: TComponentFactory;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

{*****************************************************************************
  Name           : TrdtmQuery.CreateParameterRecordsForQuery
  Author         : slesage
  Arguments      : aQueryID      - The ID of the Query Record.
                   aSQLStatement - The SQL Statement of the Query Record.  It
                                   is this statement that will be used to
                                   create the Parameters.
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to Recreate the Parameter Records
                   for a specific Query in a Query Record.
  History        :

  Date         By                   Description
  ----         --                   -----------
  15/01/2004   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TrdtmQuery.CreateParameterRecordsForQuery(aCon : TADOConnection; aQueryID : Integer; aSQLStatement : String);
var
  aParamQuery, aADOQuery : TADOQuery;
  aLCV      : Integer;
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'CreateParameterRecordsForQuery' );
  csFVBFFCDataModule.SendString( 'aSQLStatement', aSQLStatement );
  csFVBFFCDataModule.SendInteger( 'aQueryID', aQueryID );
  {$ENDIF}

  aADOQuery := TADOQuery.Create( Self );
  Try
    aADOQuery.Connection := aCon;
    aADOQuery.SQL.Text := aSQLStatement;

    aParamQuery := TADOQuery.Create( Self );

    Try
      aParamQuery.Connection := aCon;
      aParamQuery.SQL.Text := 'SELECT * FROM T_DBW_PARAMETER WHERE F_QUERY_ID = ' + IntToStr( aQueryID );
      aParamQuery.Open;

      for aLCV := 0 to Pred( aADOQuery.Parameters.Count ) do
      begin
        {$IFDEF CODESITE}
        csFVBFFCDataModule.SendObject( 'Parameter', aADOQuery.Parameters[ aLCV ] );
        {$ENDIF}

        if not ( aParamQuery.Locate( 'F_NAME', aADOQuery.Parameters[ aLCV ].Name, [ loCaseInsensitive ] ) ) then
        begin
          aParamQuery.Append;
          aParamQuery.FieldByName( 'F_PARAMETER_ID' ).AsInteger := GetNewParameterID; // Identity( 'T_DBW_PARAMETER' );
          aParamQuery.FieldByName( 'F_QUERY_ID' ).AsInteger := aQueryID;
          aParamQuery.FieldByName( 'F_NAME' ).AsString := aADOQuery.Parameters[ aLCV ].Name;
          aParamQuery.FieldByName( 'F_CAPTION_NL' ).AsString := aADOQuery.Parameters[ aLCV ].Name;
          aParamQuery.FieldByName( 'F_CAPTION_FR' ).AsString := aADOQuery.Parameters[ aLCV ].Name;

          case aADOQuery.Parameters[ alcv ].DataType of
            ftInteger, ftSmallInt, ftWord, ftLargeInt :
            begin
              {$IFDEF CODESITE}
              csFVBFFCDataModule.SendMsg( 'Integer Parameter' );
              {$ENDIF}
              aParamQuery.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger := 1;
            end;
            ftBoolean :
            begin
              {$IFDEF CODESITE}
              csFVBFFCDataModule.SendMsg( 'Boolean Parameter' );
              {$ENDIF}
              aParamQuery.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger := 5;
            end;
            ftFloat, ftCurrency, ftBCD :
            begin
              {$IFDEF CODESITE}
              csFVBFFCDataModule.SendMsg( 'Decimal Parameter' );
              {$ENDIF}
              aParamQuery.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger := 2;
            end;
            ftDate, ftTime, ftDateTime :
            begin
              {$IFDEF CODESITE}
              csFVBFFCDataModule.SendMsg( 'DateTime Parameter' );
              {$ENDIF}
              aParamQuery.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger := 11;
            end;
            else
            begin
              {$IFDEF CODESITE}
              csFVBFFCDataModule.SendMsg( 'String Parameter' );
              {$ENDIF}
              aParamQuery.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger := 3;
            end;
          end;
          aParamQuery.Post;
        end;
      end;

      aParamQuery.First;

      While not aParamQuery.Eof do
      begin
        if ( aADOQuery.Parameters.FindParam( aParamQuery.FieldByName( 'F_NAME' ).AsString ) = nil ) then
        begin
          adospDBW_DeleteParameter.Parameters.ParamByName( '@QueryID' ).Value := aQueryID;
          adospDBW_DeleteParameter.Parameters.ParamByName( '@ParameterID' ).Value := aParamQuery.FieldByName( 'F_PARAMETER_ID' ).Value;
          adospDBW_DeleteParameter.ExecProc;
        end;
        aParamQuery.Next;
      end;

      aParamQuery.Close;
    finally
      FreeAndNil( aParamQuery );
    end;
  finally
    FreeAndNil( aADOQuery );
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'CreateParameterRecordsForQuery' );
  {$ENDIF}
end;

class procedure TrdtmQuery.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmQuery.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmQuery.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmQuery.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmQuery.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmQuery.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmQuery.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmQuery.GetTableName: String;
begin
  Result := 'T_DBW_QUERY';
end;

{*****************************************************************************
  Name           : TrdtmQuery.prvRecordAfterUpdateRecord
  Author         : slesage
  Arguments      : Sender   - The Object from which the method is invoked.
                   SourceDS - The dataset from which the data originated.
                   DeltaDS  - A client dataset containing all the updates that
                              are being applied.
                   UpdateKind - Indicates whether this update is the
                                modification of an existing record (ukModify),
                                a new record to insert (ukInsert), or an
                                existing record to delete (ukDelete).
  Return Values  : None
  Exceptions     : None
  Description    : This event is executed immediately after each record is
                   applied to the remote dataset.  In here we will execute the
                   method that will recreate the parameters if necessary.
  History        :

  Date         By                   Description
  ----         --                   -----------
  15/01/2004   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TrdtmQuery.prvRecordAfterUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind);
var
  aQueryID : Integer;
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvRecordAfterUpdateRecord' );
  {$ENDIF}
  inherited;


  if ( ( UpdateKind = ukInsert ) or
       ( UpdateKind = ukModify ) ) and
   not ( DeltaDS.FieldByName( 'F_TEXT' ).IsNull ) then
  begin
    aQueryID := DeltaDS.FieldByName( 'F_QUERY_ID' ).OldValue;
    CreateParameterRecordsForQuery( adoqryRecord.Connection, aQueryID, DeltaDS.FieldByName( 'F_TEXT' ).AsString );
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvRecordAfterUpdateRecord' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TrdtmQuery.prvRecordBeforeUpdateRecord
  Author         : slesage
  Arguments      : Sender   - The Object from which the method is invoked.
                   SourceDS - The dataset from which the data originated.
                   DeltaDS  - A client dataset containing all the updates that
                              are being applied.
                   UpdateKind - Indicates whether this update is the
                                modification of an existing record (ukModify),
                                a new record to insert (ukInsert), or an
                                existing record to delete (ukDelete).
  Return Values  : None
  Exceptions     : None
  Description    : This event is executed immediately before each record is
                   applied to the remote dataset.  In here we will do the
                   updates to the Parameter records ourselves.
  History        :

  Date         By                   Description
  ----         --                   -----------
  26/01/2004   Ren� Clerckx         Added parameters to adospDBW_UpdateParameter.
  15/01/2004   slesage              Initial creation of the Unit.
 *****************************************************************************}

 procedure TrdtmQuery.prvRecordBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvRecordBeforeUpdateRecord' );
  {$ENDIF}

  inherited;

  if ( UpdateKind = ukModify ) then
  begin
    Try
      with adospDBW_UpdateParameter do
      begin
        Parameters.ParamByName( '@ParameterID' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_PARAMETER_ID' ) );
        Parameters.ParamByName( '@QueryID' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_QUERY_ID' ) );
        Parameters.ParamByName( '@NewName' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_NAME' ) );
        Parameters.ParamByName( '@NewCaptionNL' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_CAPTION_NL' ) );
        Parameters.ParamByName( '@NewCaptionFR' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_CAPTION_FR' ) );
        Parameters.ParamByName( '@NewParameterTypeID' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_PARAMETER_TYPE_ID' ) );
        Parameters.ParamByName( '@NewTable' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_TABLE' ) );
        Parameters.ParamByName( '@NewKey' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_KEY' ) );
        Parameters.ParamByName( '@NewResultNL' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_RESULT_NL' ) );
        Parameters.ParamByName( '@NewResultFR' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_RESULT_FR' ) );
        ExecProc;

        if ( Parameters.ParamByName( '@RETURN_VALUE' ).Value = 1 ) then
        begin
          Applied := True;
        end
        else
        begin
          Raise Exception.Create( 'Something went wrong while executing SP_DBW_UPDATE_PARAMETER');
        end;
      end;
    Except
      Applied := False;
    End;
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvRecordBeforeUpdateRecord' );
  {$ENDIF}
end;

function TrdtmQuery.GetFieldValue(const aField: TField): Variant;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetFieldValue' );
  csFVBFFCServer.SendObject( 'aField', aField );
  {$ENDIF}
  if ( Assigned( aField ) ) then
  begin
    if aField.IsNull then
    begin
      Result := aField.OldValue;
    end
    else
    begin
      Result := aField.Value;
    end;
  end
  else
  begin
    Result := varNull;
  end;
  {$IFDEF CODESITE}
  csFVBFFCServer.SendVariant( 'Result', Result );
  csFVBFFCServer.ExitMethod( Self, 'GetFieldValue' );
  {$ENDIF}
end;

function TrdtmQuery.GetNewParameterID: SYSINT;
begin
  Result := SELF.FrdtmEDUMain.GetNewRecordID('T_DBW_PARAMETER');
end;

function TrdtmQuery.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofQuery := TComponentFactory.Create(ComServer, TrdtmQuery,
    Class_rdtmQuery, ciInternal, tmApartment);

end.
