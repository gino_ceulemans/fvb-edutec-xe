inherited rdtmQuery: TrdtmQuery
  OldCreateOrder = True
  Height = 246
  Width = 298
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' F_QUERY_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_QUERY_GROUP_ID,'
      ' F_TEXT,'
      ' F_DESCRIPTION_NL,'
      ' F_DESCRIPTION_FR,'
      ' F_DESCRIPTION,'
      ' F_QUERY_GROUP_NAME'
      'FROM'
      ' V_DBW_QUERY')
    object adoqryListF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_TEXT: TMemoField
      FieldName = 'F_TEXT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_DESCRIPTION_NL: TMemoField
      FieldName = 'F_DESCRIPTION_NL'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_DESCRIPTION_FR: TMemoField
      FieldName = 'F_DESCRIPTION_FR'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_DESCRIPTION: TMemoField
      FieldName = 'F_DESCRIPTION'
      ProviderFlags = []
      BlobType = ftMemo
    end
    object adoqryListF_QUERY_GROUP_NAME: TStringField
      FieldName = 'F_QUERY_GROUP_NAME'
      ProviderFlags = []
      Size = 256
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 256
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' F_QUERY_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_QUERY_GROUP_ID,'
      ' F_TEXT,'
      ' F_DESCRIPTION_NL,'
      ' F_DESCRIPTION_FR,'
      ' F_DESCRIPTION,'
      ' F_QUERY_GROUP_NAME'
      'FROM'
      ' V_DBW_QUERY')
    object adoqryRecordF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TEXT: TMemoField
      FieldName = 'F_TEXT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_DESCRIPTION_NL: TMemoField
      FieldName = 'F_DESCRIPTION_NL'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_DESCRIPTION_FR: TMemoField
      FieldName = 'F_DESCRIPTION_FR'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_DESCRIPTION: TMemoField
      FieldName = 'F_DESCRIPTION'
      ProviderFlags = []
      BlobType = ftMemo
    end
    object adoqryRecordF_QUERY_GROUP_NAME: TStringField
      FieldName = 'F_QUERY_GROUP_NAME'
      ProviderFlags = []
      Size = 256
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 256
    end
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    AfterUpdateRecord = prvRecordAfterUpdateRecord
    BeforeUpdateRecord = prvRecordBeforeUpdateRecord
  end
  object adospDBW_DeleteParameter: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DBW_DELETE_PARAMETER;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ParameterID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@QueryID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 256
    Top = 24
  end
  object adospDBW_UpdateParameter: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DBW_UPDATE_PARAMETER;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ParameterID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@QueryID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NewName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewCaptionNL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewCaptionFR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewParameterTypeID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NewTable'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewKey'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewResultNL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewResultFR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@message_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 256
    Top = 72
  end
end
