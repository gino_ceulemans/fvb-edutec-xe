unit Remote_QueryWizard;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmQueryWizard = class(TrdtmEduBase, IrdtmQueryWizard)
    adoqryGroup: TFVBFFCQuery;
    prvGroup: TFVBFFCDataSetProvider;
    adoqryListF_QUERY_GROUP_ID: TIntegerField;
    adoqryListF_QUERY_GROUP_NAME: TStringField;
    adoqryListF_QUERY_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_DESCRIPTION: TMemoField;
    adoqryListF_TEXT: TMemoField;
    adoqryListF_PARAMETER_COUNT: TIntegerField;
    adoqryGroupF_QUERY_GROUP_ID: TIntegerField;
    adoqryGroupF_NAME: TStringField;
    adoqryGroupF_PARENT_ID: TIntegerField;
    adoqryGroupF_PARENT_NAME: TStringField;
    adoqryGroupF_QUERY_COUNT: TIntegerField;
    adoqryParameter: TFVBFFCQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    IntegerField3: TIntegerField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    IntegerField4: TIntegerField;
    prvParameter: TFVBFFCDataSetProvider;
    adoqryRecordF_QUERY_GROUP_ID: TIntegerField;
    adoqryRecordF_QUERY_GROUP_NAME: TStringField;
    adoqryRecordF_QUERY_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_DESCRIPTION: TMemoField;
    adoqryRecordF_TEXT: TMemoField;
    adoqryRecordF_PARAMETER_COUNT: TIntegerField;
    adospSelectLookUp: TFVBFFCStoredProc;
    prvSelectLookUp: TFVBFFCDataSetProvider;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofQueryWizard: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmQueryWizard.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmQueryWizard.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmQueryWizard.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmQueryWizard.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmQueryWizard.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmQueryWizard.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmQueryWizard.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmQueryWizard.GetTableName: String;
begin
  Result := 'T_DBW_QUERY_WIZARD';
end;

function TrdtmQueryWizard.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofQueryWizard := TComponentFactory.Create(ComServer, TrdtmQueryWizard,
    Class_rdtmQueryWizard, ciInternal, tmApartment);

end.
