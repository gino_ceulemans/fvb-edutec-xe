inherited rdtmParameter: TrdtmParameter
  OldCreateOrder = True
  Height = 187
  Width = 215
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' F_PARAMETER_ID, '
      ' F_QUERY_ID, '
      ' F_QUERY_NAME,'
      ' F_NAME, '
      ' F_CAPTION_NL, '
      ' F_CAPTION_FR,'
      ' F_CAPTION,'
      ' F_PARAMETER_TYPE_ID,'
      ' F_PARAMETER_TYPE,'
      ' F_TABLE,'
      ' F_KEY,'
      ' F_RESULT_NL,'
      ' F_RESULT_FR,'
      ' F_RESULT'
      'FROM'
      ' V_DBW_PARAMETER')
    object adoqryListF_PARAMETER_ID: TIntegerField
      FieldName = 'F_PARAMETER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_QUERY_NAME: TStringField
      FieldName = 'F_QUERY_NAME'
      ProviderFlags = []
      Size = 256
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_CAPTION_NL: TStringField
      FieldName = 'F_CAPTION_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_CAPTION_FR: TStringField
      FieldName = 'F_CAPTION_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_CAPTION: TStringField
      FieldName = 'F_CAPTION'
      ProviderFlags = []
      Size = 256
    end
    object adoqryListF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PARAMETER_TYPE: TStringField
      FieldName = 'F_PARAMETER_TYPE'
      ProviderFlags = []
      Size = 50
    end
    object adoqryListF_TABLE: TStringField
      FieldName = 'F_TABLE'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object adoqryListF_KEY: TStringField
      FieldName = 'F_KEY'
      ProviderFlags = []
      Visible = False
      Size = 256
    end
    object adoqryListF_RESULT_NL: TStringField
      FieldName = 'F_RESULT_NL'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object adoqryListF_RESULT_FR: TStringField
      FieldName = 'F_RESULT_FR'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object adoqryListF_RESULT: TStringField
      FieldName = 'F_RESULT'
      ProviderFlags = []
      Visible = False
      Size = 256
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' F_PARAMETER_ID, '
      ' F_QUERY_ID, '
      ' F_QUERY_NAME,'
      ' F_NAME, '
      ' F_CAPTION_NL, '
      ' F_CAPTION_FR,'
      ' F_CAPTION,'
      ' F_PARAMETER_TYPE_ID,'
      ' F_PARAMETER_TYPE,'
      ' F_TABLE,'
      ' F_KEY,'
      ' F_RESULT_NL,'
      ' F_RESULT_FR,'
      ' F_RESULT'
      'FROM'
      ' V_DBW_PARAMETER')
    object adoqryRecordF_PARAMETER_ID: TIntegerField
      FieldName = 'F_PARAMETER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_QUERY_NAME: TStringField
      FieldName = 'F_QUERY_NAME'
      ProviderFlags = []
      Size = 256
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_CAPTION_NL: TStringField
      FieldName = 'F_CAPTION_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_CAPTION_FR: TStringField
      FieldName = 'F_CAPTION_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_CAPTION: TStringField
      FieldName = 'F_CAPTION'
      ProviderFlags = []
      Size = 256
    end
    object adoqryRecordF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PARAMETER_TYPE: TStringField
      FieldName = 'F_PARAMETER_TYPE'
      ProviderFlags = []
      Size = 50
    end
    object adoqryRecordF_TABLE: TStringField
      FieldName = 'F_TABLE'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_KEY: TStringField
      FieldName = 'F_KEY'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_RESULT_NL: TStringField
      FieldName = 'F_RESULT_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_RESULT_FR: TStringField
      FieldName = 'F_RESULT_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_RESULT: TStringField
      FieldName = 'F_RESULT'
      ProviderFlags = []
      Size = 256
    end
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvRecordBeforeUpdateRecord
  end
  object adospDBW_UpdateParameter: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DBW_UPDATE_PARAMETER;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ParameterID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@QueryID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NewName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewCaptionNL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewCaptionFR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewParameterTypeID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@NewTable'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewKey'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewResultNL'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@NewResultFR'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = Null
      end
      item
        Name = '@message_id'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 456
    Top = 24
  end
  object adoqryTables: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      ' ID,'
      ' NAME,'
      ' TYPE'
      'FROM'
      ' SysObjects'
      'WHERE ('
      ' TYPE IN ( '#39'U'#39', '#39'V'#39')'
      ' )'
      'ORDER BY NAME')
    AutoOpen = False
    Left = 208
    Top = 24
    object adoqryTablesID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryTablesNAME: TWideStringField
      FieldName = 'NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryTablesTYPE: TStringField
      FieldName = 'TYPE'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
      FixedChar = True
      Size = 2
    end
  end
  object prvTables: TFVBFFCDataSetProvider
    DataSet = adoqryTables
    BeforeUpdateRecord = prvRecordBeforeUpdateRecord
    OnGetTableName = prvListGetTableName
    Left = 208
    Top = 80
  end
  object adoqryFields: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      ' ID,'
      ' NAME'
      'FROM'
      ' SysColumns'
      'WHERE ('
      ' ID = :ID'
      ')')
    AutoOpen = False
    Left = 320
    Top = 24
    object adoqryFieldsID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryFieldsNAME: TWideStringField
      FieldName = 'NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
  object prvFields: TFVBFFCDataSetProvider
    DataSet = adoqryFields
    BeforeUpdateRecord = prvRecordBeforeUpdateRecord
    OnGetTableName = prvListGetTableName
    Left = 320
    Top = 80
  end
end
