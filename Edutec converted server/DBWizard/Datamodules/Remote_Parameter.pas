unit Remote_Parameter;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmParameter = class(TrdtmEduBase, IrdtmParameter)
    adoqryListF_PARAMETER_ID: TIntegerField;
    adoqryListF_QUERY_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_CAPTION_NL: TStringField;
    adoqryListF_CAPTION_FR: TStringField;
    adoqryListF_CAPTION: TStringField;
    adoqryListF_PARAMETER_TYPE: TStringField;
    adoqryRecordF_PARAMETER_ID: TIntegerField;
    adoqryRecordF_QUERY_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_CAPTION_NL: TStringField;
    adoqryRecordF_CAPTION_FR: TStringField;
    adoqryRecordF_CAPTION: TStringField;
    adoqryRecordF_PARAMETER_TYPE: TStringField;
    adoqryRecordF_PARAMETER_TYPE_ID: TIntegerField;
    adoqryListF_PARAMETER_TYPE_ID: TIntegerField;
    adoqryListF_QUERY_NAME: TStringField;
    adoqryRecordF_QUERY_NAME: TStringField;
    adospDBW_UpdateParameter: TFVBFFCStoredProc;
    adoqryRecordF_TABLE: TStringField;
    adoqryRecordF_KEY: TStringField;
    adoqryRecordF_RESULT_NL: TStringField;
    adoqryRecordF_RESULT_FR: TStringField;
    adoqryTables: TFVBFFCQuery;
    prvTables: TFVBFFCDataSetProvider;
    adoqryTablesID: TIntegerField;
    adoqryTablesNAME: TWideStringField;
    adoqryTablesTYPE: TStringField;
    adoqryFields: TFVBFFCQuery;
    prvFields: TFVBFFCDataSetProvider;
    adoqryFieldsID: TIntegerField;
    adoqryFieldsNAME: TWideStringField;
    adoqryListF_TABLE: TStringField;
    adoqryListF_KEY: TStringField;
    adoqryListF_RESULT_NL: TStringField;
    adoqryListF_RESULT_FR: TStringField;
    adoqryListF_RESULT: TStringField;
    adoqryRecordF_RESULT: TStringField;
    procedure prvRecordBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
//GCXE    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;  var TableName: String);
  private
    { Private declarations }
    function GetFieldValue(const aField: TField): Variant;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofParameter: TComponentFactory;

implementation

  {$IFDEF CODESITE}
Uses Unit_FVBFFCCodeSite;
  {$ENDIF}


{$R *.DFM}

class procedure TrdtmParameter.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmParameter.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmParameter.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmParameter.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmParameter.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmParameter.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmParameter.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmParameter.GetTableName: String;
begin
  Result := 'T_DBW_PARAMETER';
end;

function TrdtmParameter.GetFieldValue(const aField: TField): Variant;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetFieldValue' );
  csFVBFFCServer.SendObject( 'aField', aField );
  {$ENDIF}
  if ( Assigned( aField ) ) then
  begin
    if aField.IsNull then
    begin
      Result := aField.OldValue;
    end
    else
    begin
      Result := aField.Value;
    end;
  end
  else
  begin
    Result := varNull;
  end;
  {$IFDEF CODESITE}
  csFVBFFCServer.SendVariant( 'Result', Result );
  csFVBFFCServer.ExitMethod( Self, 'GetFieldValue' );
  {$ENDIF}
end;

procedure TrdtmParameter.prvRecordBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'prvRecordBeforeUpdateRecord' );
  {$ENDIF}

  inherited;

  if ( UpdateKind = ukModify ) then
  begin
    Try
      with adospDBW_UpdateParameter do
      begin
        Prepared := True;

        Parameters.ParamByName( '@ParameterID' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_PARAMETER_ID' ) );
        Parameters.ParamByName( '@QueryID' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_QUERY_ID' ) );
        Parameters.ParamByName( '@NewName' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_NAME' ) );
        Parameters.ParamByName( '@NewCaptionNL' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_CAPTION_NL' ) );
        Parameters.ParamByName( '@NewCaptionFR' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_CAPTION_FR' ) );
        Parameters.ParamByName( '@NewParameterTypeID' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_PARAMETER_TYPE_ID' ) );
        Parameters.ParamByName( '@NewTable' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_TABLE' ) );
        Parameters.ParamByName( '@NewKey' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_KEY' ) );
        Parameters.ParamByName( '@NewResultNL' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_RESULT_NL' ) );
        Parameters.ParamByName( '@NewResultFR' ).Value := GetFieldValue( DeltaDS.FieldByName( 'F_RESULT_FR' ) );

        ExecProc;

        if ( Parameters.ParamByName( '@RETURN_VALUE' ).Value = 1 ) then
        begin
          Applied := True;
        end
        else
        begin
          Raise Exception.Create( 'Something went wrong while executing SP_DBW_UPDATE_PARAMETER');
        end;
      end;
    Except
      on E: Exception do
      begin
        Applied := False;
      end
      else
      begin
        Applied := False;
      end;
    End;
  end;

  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'prvRecordBeforeUpdateRecord' );
  {$ENDIF}

end;

{GCXE
procedure TrdtmParameter.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_DBW_PARAMETER';
end;
}

function TrdtmParameter.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofParameter := TComponentFactory.Create(ComServer, TrdtmParameter,
    Class_rdtmParameter, ciInternal, tmApartment);

end.
