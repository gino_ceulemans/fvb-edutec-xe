inherited rdtmQueryGroup: TrdtmQueryGroup
  OldCreateOrder = True
  Height = 237
  Width = 298
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_QUERY_GROUP_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_PARENT_ID,'
      ' F_PARENT_NAME'
      'FROM'
      ' V_DBW_QUERYGROUP')
    object adoqryListF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 256
    end
    object adoqryListF_PARENT_ID: TIntegerField
      FieldName = 'F_PARENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PARENT_NAME: TStringField
      FieldName = 'F_PARENT_NAME'
      ProviderFlags = []
      Size = 256
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_QUERY_GROUP_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_PARENT_ID,'
      ' F_PARENT_NAME'
      'FROM'
      ' V_DBW_QUERYGROUP')
    object adoqryRecordF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 256
    end
    object adoqryRecordF_PARENT_ID: TIntegerField
      FieldName = 'F_PARENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PARENT_NAME: TStringField
      FieldName = 'F_PARENT_NAME'
      ProviderFlags = []
      Size = 256
    end
  end
end
