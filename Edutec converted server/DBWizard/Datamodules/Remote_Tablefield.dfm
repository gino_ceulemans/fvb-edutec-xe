inherited rdtmTableField: TrdtmTableField
  OldCreateOrder = True
  Height = 191
  Width = 215
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' TableName,'
      ' FieldName'
      'FROM'
      ' V_DBW_TABLEFIELD'
      'ORDER BY TableName, FieldName')
    object adoqryListTableName: TWideStringField
      FieldName = 'TableName'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListFieldName: TWideStringField
      FieldName = 'FieldName'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' TableName,'
      ' FieldName'
      'FROM'
      ' V_DBW_TABLEFIELD'
      'ORDER BY TableName, FieldName')
    object adoqryRecordTableName: TWideStringField
      FieldName = 'TableName'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordFieldName: TWideStringField
      FieldName = 'FieldName'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
end
