inherited rdtmProgDiscipline: TrdtmProgDiscipline
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFESSION_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFESSION_NAME'
      'FROM '
      '  V_PROG_PROFESSION'
      '')
    object adoqryListF_PROFESSION_ID: TIntegerField
      FieldName = 'F_PROFESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFESSION_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFESSION_NAME'
      'FROM '
      '  V_PROG_PROFESSION'
      '')
    object adoqryRecordF_PROFESSION_ID: TIntegerField
      FieldName = 'F_PROFESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
