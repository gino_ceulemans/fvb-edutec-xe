{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_PROG_PROFESSION.

  @Name       Remote_ProgDiscipline
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  09/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_ProgDiscipline;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmProgDiscipline = class(TrdtmEduBase, IrdtmProgDiscipline)
    adoqryRecordF_PROFESSION_ID: TIntegerField;
    adoqryRecordF_NAME_NL: TStringField;
    adoqryRecordF_NAME_FR: TStringField;
    adoqryRecordF_PROFESSION_NAME: TStringField;
    adoqryListF_PROFESSION_ID: TIntegerField;
    adoqryListF_NAME_NL: TStringField;
    adoqryListF_NAME_FR: TStringField;
    adoqryListF_PROFESSION_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
     // safecall;
    { Public declarations }
  end;

var
  ofProgDiscipline : TComponentFactory;
  
implementation

{$R *.DFM}

class procedure TrdtmProgDiscipline.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmProgDiscipline.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmProgDiscipline.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmProgDiscipline.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmProgDiscipline.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmProgDiscipline.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmProgDiscipline.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmProgDiscipline.GetTableName: String;
begin
  Result := 'T_PROG_PROFESSION';
end;

function TrdtmProgDiscipline.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofProgDiscipline := TComponentFactory.Create(ComServer, TrdtmProgDiscipline,
    Class_rdtmProgDiscipline, ciInternal, tmApartment);
    
end.
