inherited rdtmProgProgram: TrdtmProgProgram
  OldCreateOrder = True
  Height = 363
  Width = 562
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROGRAM_ID, '
      '  F_PROGRAM_TYPE,'
      '  F_PROGRAM_TYPE_NAME,'
      '  F_MAX_PUNTEN,'
      '  F_NAME, '
      '  F_DURATION, '
      '  F_DURATION_DAYS, '
      '  F_MINIMUM, '
      '  F_MAXIMUM, '
      '  F_SHORT_DESC, '
      '  F_PROFESSION_NAME,'
      '  F_ACTIVE,'
      '  F_USER_ID,'
      '  F_COMPLETE_USERNAME,'
      '  F_ENROLMENT_DATE, '
      '  F_CONTROL_DATE, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_PRICING_SCHEME, '
      '  F_PRICE_SESSION, '
      '  F_PRICE_DAY,'
      '/*  ,F_PRICE_WORKER, '
      '  F_PRICE_CLERK, */'
      '  F_PRICE_OTHER,'
      '  F_CODE,'
      '  F_TK_FVB,'
      '  F_TK_CEVORA,'
      '  F_BT_FVB,'
      '  F_BT_CEVORA,'
      '  F_PRICE_WORKER,'
      '  F_PRICE_FIXED,'
      '  F_START_HOUR,'
      'externalcode1'
      'FROM '
      '  V_PROG_PROGRAM'
      '')
    object adoqryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_DURATION: TSmallintField
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_DURATION_DAYS: TSmallintField
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SHORT_DESC: TStringField
      FieldName = 'F_SHORT_DESC'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryListF_ENROLMENT_DATE: TIntegerField
      FieldName = 'F_ENROLMENT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CONTROL_DATE: TIntegerField
      FieldName = 'F_CONTROL_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_SESSION: TFloatField
      FieldName = 'F_PRICE_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_DAY: TFloatField
      FieldName = 'F_PRICE_DAY'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CODE: TStringField
      FieldName = 'F_CODE'
      Size = 50
    end
    object adoqryListF_TK_FVB: TFloatField
      FieldName = 'F_TK_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_TK_CEVORA: TFloatField
      FieldName = 'F_TK_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_BT_FVB: TFloatField
      FieldName = 'F_BT_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_BT_CEVORA: TFloatField
      FieldName = 'F_BT_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_FIXED: TFloatField
      FieldName = 'F_PRICE_FIXED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_START_HOUR: TStringField
      FieldName = 'F_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListexternalcode1: TStringField
      FieldName = 'externalcode1'
      ProviderFlags = [pfInUpdate]
      Size = 6
    end
    object adoqryListF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MAX_PUNTEN: TIntegerField
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROGRAM_TYPE_NAME: TStringField
      FieldName = 'F_PROGRAM_TYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_PROGRAM_ID, '
      '  F_PROGRAM_TYPE,'
      '  F_PROGRAM_TYPE_NAME,'
      '  F_MAX_PUNTEN,'
      '  F_NAME, '
      '  F_USER_ID, '
      '  F_COMPLETE_USERNAME,'
      '  F_DURATION, '
      '  F_DURATION_DAYS, '
      '  F_MINIMUM, '
      '  F_MAXIMUM, '
      '  F_SHORT_DESC, '
      '  F_LONG_DESC, '
      '  F_ENROLMENT_DATE, '
      '  F_CONTROL_DATE, '
      '  F_PROFESSION_ID, '
      '  F_PROFESSION_NAME, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  /* F_EQUIPMENT, '
      '  F_SECURITY_EQ, '
      '  F_KNOWLEDGE, '
      '  F_DIDAC_MATERIAL, '
      '  F_RAW_MATERIAL, '
      '  F_INFRASTRUCTURE, */'
      '  F_COMMENT, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_EDUCATION_ID, '
      '  F_PRICING_SCHEME, '
      '  F_PRICE_SESSION, '
      '  F_PRICE_DAY, '
      '/*  F_PRICE_WORKER, '
      '  F_PRICE_CLERK, */'
      '  F_PRICE_OTHER,'
      '  F_CODE,'
      '  F_TK_FVB,'
      '  F_TK_CEVORA,'
      '  F_BT_FVB,'
      '  F_BT_CEVORA,'
      '  F_REMARKS_INSTRUCTOR,'
      '  F_REMARKS_ORGANISATION,'
      '  F_REMARKS_INFRASTRUCTURE,'
      '  F_PRICE_WORKER,'
      '  F_START_HOUR,'
      '  F_PRICE_FIXED,'
      'externalcode1'
      ' '
      'FROM'
      '  V_PROG_PROGRAM')
    object adoqryRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryRecordF_DURATION: TSmallintField
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DURATION_DAYS: TSmallintField
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SHORT_DESC: TStringField
      FieldName = 'F_SHORT_DESC'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_LONG_DESC: TMemoField
      FieldName = 'F_LONG_DESC'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_ENROLMENT_DATE: TIntegerField
      FieldName = 'F_ENROLMENT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CONTROL_DATE: TIntegerField
      FieldName = 'F_CONTROL_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROFESSION_ID: TIntegerField
      FieldName = 'F_PROFESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EDUCATION_ID: TIntegerField
      FieldName = 'F_EDUCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_SESSION: TFloatField
      FieldName = 'F_PRICE_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_DAY: TFloatField
      FieldName = 'F_PRICE_DAY'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TK_FVB: TFloatField
      FieldName = 'F_TK_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TK_CEVORA: TFloatField
      FieldName = 'F_TK_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_BT_FVB: TFloatField
      FieldName = 'F_BT_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_BT_CEVORA: TFloatField
      FieldName = 'F_BT_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_START_HOUR: TStringField
      FieldName = 'F_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PRICE_FIXED: TFloatField
      FieldName = 'F_PRICE_FIXED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_REMARKS_INSTRUCTOR: TMemoField
      FieldName = 'F_REMARKS_INSTRUCTOR'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_REMARKS_ORGANISATION: TMemoField
      FieldName = 'F_REMARKS_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_REMARKS_INFRASTRUCTURE: TMemoField
      FieldName = 'F_REMARKS_INFRASTRUCTURE'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordexternalcode1: TStringField
      FieldName = 'externalcode1'
      ProviderFlags = [pfInUpdate]
      Size = 6
    end
    object adoqryRecordF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MAX_PUNTEN: TIntegerField
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROGRAM_TYPE_NAME: TStringField
      FieldName = 'F_PROGRAM_TYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_PROG_PROGRAM: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_PROG_PROGRAM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 312
    Top = 56
  end
end
