{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_PROG_PROGRAM.

  @Name       Remote_ProgProgram
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2008   ivdbossche           Added F_PRICE_WORKER, F_START_HOUR (Mantis 3197)
  11/09/2006   sLesage              Added fields for 'Tussenkomst' and
                                    'Boetes'.
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_ProgProgram;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmProgProgram = class(TrdtmEduBase, IrdtmProgProgram)
    adoqryListF_PROGRAM_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_DURATION: TSmallintField;
    adoqryListF_DURATION_DAYS: TSmallintField;
    adoqryListF_MINIMUM: TSmallintField;
    adoqryListF_MAXIMUM: TSmallintField;
    adoqryListF_SHORT_DESC: TStringField;
    adoqryListF_PROFESSION_NAME: TStringField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_USER_ID: TIntegerField;
    adoqryListF_COMPLETE_USERNAME: TStringField;
    adoqryListF_ENROLMENT_DATE: TIntegerField;
    adoqryListF_CONTROL_DATE: TIntegerField;
    adoqryListF_LANGUAGE_ID: TIntegerField;
    adoqryListF_LANGUAGE_NAME: TStringField;
    adoqryListF_PRICING_SCHEME: TIntegerField;
    adoqryListF_PRICE_SESSION: TFloatField;
    adoqryListF_PRICE_DAY: TFloatField;
    adoqryRecordF_PROGRAM_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_USER_ID: TIntegerField;
    adoqryRecordF_COMPLETE_USERNAME: TStringField;
    adoqryRecordF_DURATION: TSmallintField;
    adoqryRecordF_DURATION_DAYS: TSmallintField;
    adoqryRecordF_MINIMUM: TSmallintField;
    adoqryRecordF_MAXIMUM: TSmallintField;
    adoqryRecordF_SHORT_DESC: TStringField;
    adoqryRecordF_LONG_DESC: TMemoField;
    adoqryRecordF_ENROLMENT_DATE: TIntegerField;
    adoqryRecordF_CONTROL_DATE: TIntegerField;
    adoqryRecordF_PROFESSION_ID: TIntegerField;
    adoqryRecordF_PROFESSION_NAME: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_COMMENT: TMemoField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_EDUCATION_ID: TIntegerField;
    adoqryRecordF_PRICING_SCHEME: TIntegerField;
    adoqryRecordF_PRICE_SESSION: TFloatField;
    adoqryRecordF_PRICE_DAY: TFloatField;
    adoqryRecordF_CODE: TStringField;
    adospSP_DEACTIVATE_PROG_PROGRAM: TFVBFFCStoredProc;
    adoqryListF_PRICE_OTHER: TFloatField;
    adoqryRecordF_PRICE_OTHER: TFloatField;
    adoqryListF_CODE: TStringField;
    adoqryRecordF_TK_FVB: TFloatField;
    adoqryRecordF_TK_CEVORA: TFloatField;
    adoqryRecordF_BT_FVB: TFloatField;
    adoqryRecordF_BT_CEVORA: TFloatField;
    adoqryListF_TK_FVB: TFloatField;
    adoqryListF_TK_CEVORA: TFloatField;
    adoqryListF_BT_FVB: TFloatField;
    adoqryListF_BT_CEVORA: TFloatField;
    adoqryRecordF_PRICE_WORKER: TFloatField;
    adoqryRecordF_START_HOUR: TStringField;
    adoqryRecordF_PRICE_FIXED: TFloatField;
    adoqryListF_PRICE_WORKER: TFloatField;
    adoqryListF_PRICE_FIXED: TFloatField;
    adoqryListF_START_HOUR: TStringField;
    adoqryRecordF_REMARKS_INSTRUCTOR: TMemoField;
    adoqryRecordF_REMARKS_ORGANISATION: TMemoField;
    adoqryRecordF_REMARKS_INFRASTRUCTURE: TMemoField;
    adoqryRecordexternalcode1: TStringField;
    adoqryListexternalcode1: TStringField;
    adoqryListF_PROGRAM_TYPE: TIntegerField;
    adoqryListF_MAX_PUNTEN: TIntegerField;
    adoqryRecordF_PROGRAM_TYPE: TIntegerField;
    adoqryRecordF_MAX_PUNTEN: TIntegerField;
    adoqryListF_PROGRAM_TYPE_NAME: TStringField;
    adoqryRecordF_PROGRAM_TYPE_NAME: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofProgProgram : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmProgProgram.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmProgProgram.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmProgProgram.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmProgProgram.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmProgProgram.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmProgProgram.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmProgProgram.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmProgProgram.GetTableName: String;
begin
  Result := 'T_PROG_PROGRAM';
end;

procedure TrdtmProgProgram.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_PROG_PROGRAM(' + DeltaDS.FieldByName('F_PROGRAM_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_PROG_PROGRAM.Close;
    adospSP_DEACTIVATE_PROG_PROGRAM.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_PROGRAM_ID').Value;
    adospSP_DEACTIVATE_PROG_PROGRAM.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmProgProgram.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofProgProgram := TComponentFactory.Create(ComServer, TrdtmProgProgram,
    Class_rdtmProgProgram, ciInternal, tmApartment);
end.
