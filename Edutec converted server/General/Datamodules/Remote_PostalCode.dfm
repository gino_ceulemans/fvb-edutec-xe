inherited rdtmPostalCode: TrdtmPostalCode
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_POSTALCODE_ID, '
      '  F_PROVINCE_ID, '
      '  F_CITY_NL, '
      '  F_CITY_FR, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PROVINCE_NAME '
      'FROM '
      '  V_GE_POSTALCODE')
    object adoqryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CITY_NL: TStringField
      FieldName = 'F_CITY_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_CITY_FR: TStringField
      FieldName = 'F_CITY_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_POSTALCODE_ID, '
      '  F_PROVINCE_ID, '
      '  F_CITY_NL, '
      '  F_CITY_FR, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PROVINCE_NAME '
      'FROM '
      '  V_GE_POSTALCODE'
      '')
    object adoqryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CITY_NL: TStringField
      FieldName = 'F_CITY_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CITY_FR: TStringField
      FieldName = 'F_CITY_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
