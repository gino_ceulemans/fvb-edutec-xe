inherited rdtmTitle: TrdtmTitle
  OldCreateOrder = True
  Height = 179
  Width = 238
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_TITLE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_TITLE_NAME,'
      '  F_GENDER_ID,'
      '  F_GENDER_NAME'
      'FROM '
      '  V_GE_TITLE'
      '')
    object adoqryListF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object adoqryListF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_TITLE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_TITLE_NAME,'
      '  F_GENDER_ID,'
      '  F_GENDER_NAME'
      'FROM '
      '  V_GE_TITLE'
      '')
    object adoqryRecordF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object adoqryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
