inherited rdtmCooperation: TrdtmCooperation
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_COOPERATION_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_COOPERATION_NAME '
      'FROM '
      '  V_GE_COOPERATION'
      '')
    object adoqryListF_COOPERATION_ID: TIntegerField
      FieldName = 'F_COOPERATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object adoqryListF_COOPERATION_NAME: TStringField
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_COOPERATION_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_COOPERATION_NAME '
      'FROM '
      '  V_GE_COOPERATION'
      '')
    object adoqryRecordF_COOPERATION_ID: TIntegerField
      FieldName = 'F_COOPERATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 64
    end
    object adoqryRecordF_COOPERATION_NAME: TStringField
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
  end
end
