inherited rdtmProgramType: TrdtmProgramType
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_PROGRAMTYPE_ID,'
      '  F_NAME_NL,'
      '  F_NAME_FR,'
      '  F_PROGRAMTYPE_NAME'
      'FROM'
      '  V_T_PROGRAM_TYPE'
      ''
      '')
    object adoqryListF_PROGRAMTYPE_ID: TIntegerField
      FieldName = 'F_PROGRAMTYPE_ID'
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 40
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 40
    end
    object adoqryListF_PROGRAMTYPE_NAME: TStringField
      FieldName = 'F_PROGRAMTYPE_NAME'
      ReadOnly = True
      Size = 40
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_PROGRAMTYPE_ID,'
      '  F_NAME_NL,'
      '  F_NAME_FR,'
      '  F_PROGRAMTYPE_NAME'
      'FROM'
      '  V_T_PROGRAM_TYPE'
      ''
      '')
    object adoqryRecordF_PROGRAMTYPE_ID: TIntegerField
      FieldName = 'F_PROGRAMTYPE_ID'
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 40
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 40
    end
    object adoqryRecordF_PROGRAMTYPE_NAME: TStringField
      FieldName = 'F_PROGRAMTYPE_NAME'
      ReadOnly = True
      Size = 40
    end
  end
end
