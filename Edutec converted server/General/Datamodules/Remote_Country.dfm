inherited rdtmCountry: TrdtmCountry
  OldCreateOrder = True
  Height = 194
  Width = 262
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_COUNTRY_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ISO_CODE, '
      '  F_COUNTRY_NAME'
      'FROM '
      '  V_GE_COUNTRY')
    object adoqryListF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_ISO_CODE: TStringField
      FieldName = 'F_ISO_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 3
    end
    object adoqryListF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    SQL.Strings = (
      'SELECT '
      '  F_COUNTRY_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ISO_CODE, '
      '  F_COUNTRY_NAME'
      'FROM '
      '  V_GE_COUNTRY'
      '')
    object adoqryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_ISO_CODE: TStringField
      FieldName = 'F_ISO_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 3
    end
    object adoqryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
