inherited rdtmProvince: TrdtmProvince
  OldCreateOrder = True
  Height = 214
  Width = 319
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROVINCE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRYPART_ID, '
      '  F_COUNTRY_PART_NAME,'
      '  F_PROVINCE_NAME'
      'FROM '
      '  V_GE_PROVINCE'
      '')
    object adoqryListF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROVINCE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRYPART_ID, '
      '  F_COUNTRY_PART_NAME,'
      '  F_PROVINCE_NAME'
      'FROM '
      '  V_GE_PROVINCE'
      '')
    object adoqryRecordF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
