object dtmReconcileError: TdtmReconcileError
  OldCreateOrder = True
  Height = 289
  Width = 524
  object adospGetErrorMsgFromConstraint: TADOStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_SYS_GETERRORFROMCONSTRAINT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Error_Input'
        Attributes = [paNullable]
        DataType = ftString
        Size = 256
        Value = 'null'
      end
      item
        Name = '@Error_Message'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 256
        Value = 'Undefined Error'
      end>
    Left = 80
    Top = 24
  end
  object adospGetErrorMsgFromNumber: TADOStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_SYS_GETERRORFROMNUMBER;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@Error_Number'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@Error_Message'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdOutput
        Precision = 255
        Size = 256
        Value = 'Unknown Error !'
      end>
    Left = 80
    Top = 72
  end
end
