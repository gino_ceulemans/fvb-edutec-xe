{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_GE_STATUS.

  @Name       Remote_Status
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  08/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Status;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, DB,
  Provider, Unit_FVBFFCDBComponents, ADODB;

type
  TrdtmStatus = class(TrdtmEduBase, IrdtmStatus)
    adoqryListF_STATUS_ID: TIntegerField;
    adoqryListF_NAME_NL: TStringField;
    adoqryListF_NAME_FR: TStringField;
    adoqryListF_LONG_DESC_NL: TStringField;
    adoqryListF_LONG_DESC_FR: TStringField;
    adoqryListF_SESSION: TBooleanField;
    adoqryListF_ENROL: TBooleanField;
    adoqryListF_WAITING_LIST: TBooleanField;
    adoqryListF_FOREGROUNDCOLOR: TIntegerField;
    adoqryListF_BACKGROUNDCOLOR: TIntegerField;
    adoqryListF_STATUS_NAME: TStringField;
    adoqryRecordF_STATUS_ID: TIntegerField;
    adoqryRecordF_NAME_NL: TStringField;
    adoqryRecordF_NAME_FR: TStringField;
    adoqryRecordF_LONG_DESC_NL: TStringField;
    adoqryRecordF_LONG_DESC_FR: TStringField;
    adoqryRecordF_SESSION: TBooleanField;
    adoqryRecordF_ENROL: TBooleanField;
    adoqryRecordF_WAITING_LIST: TBooleanField;
    adoqryRecordF_FOREGROUNDCOLOR: TIntegerField;
    adoqryRecordF_BACKGROUNDCOLOR: TIntegerField;
    adoqryRecordF_STATUS_NAME: TStringField;
    adoqryRecordF_LONG_DESC: TStringField;
    adoqryListF_LONG_DESC: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofStatus: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmStatus.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmStatus.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmStatus.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmStatus.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmStatus.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmStatus.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmStatus.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmStatus.GetTableName: String;
begin
  Result := 'T_GE_STATUS';
end;

function TrdtmStatus.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofStatus := TComponentFactory.Create(ComServer, TrdtmStatus,
    Class_rdtmStatus, ciInternal, tmApartment);
end.
