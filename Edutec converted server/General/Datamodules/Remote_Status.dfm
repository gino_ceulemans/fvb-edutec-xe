inherited rdtmStatus: TrdtmStatus
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_STATUS_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_LONG_DESC_NL, '
      ' F_LONG_DESC_FR, '
      ' F_SESSION, '
      ' F_ENROL, '
      ' F_WAITING_LIST, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR, '
      ' F_STATUS_NAME,'
      ' F_LONG_DESC '
      'FROM '
      '  V_GE_STATUS'
      '')
    object adoqryListF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object adoqryListF_LONG_DESC_NL: TStringField
      FieldName = 'F_LONG_DESC_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_LONG_DESC_FR: TStringField
      FieldName = 'F_LONG_DESC_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_SESSION: TBooleanField
      FieldName = 'F_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROL: TBooleanField
      FieldName = 'F_ENROL'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_WAITING_LIST: TBooleanField
      FieldName = 'F_WAITING_LIST'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object adoqryListF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_LONG_DESC: TStringField
      FieldName = 'F_LONG_DESC'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_STATUS_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_LONG_DESC_NL, '
      ' F_LONG_DESC_FR, '
      ' F_SESSION, '
      ' F_ENROL, '
      ' F_WAITING_LIST, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR, '
      ' F_STATUS_NAME,'
      ' F_LONG_DESC '
      'FROM '
      '  V_GE_STATUS'
      '')
    object adoqryRecordF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 64
    end
    object adoqryRecordF_LONG_DESC_NL: TStringField
      FieldName = 'F_LONG_DESC_NL'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 128
    end
    object adoqryRecordF_LONG_DESC_FR: TStringField
      FieldName = 'F_LONG_DESC_FR'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 128
    end
    object adoqryRecordF_SESSION: TBooleanField
      FieldName = 'F_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENROL: TBooleanField
      FieldName = 'F_ENROL'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_WAITING_LIST: TBooleanField
      FieldName = 'F_WAITING_LIST'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object adoqryRecordF_LONG_DESC: TStringField
      FieldName = 'F_LONG_DESC'
      ProviderFlags = []
      Visible = False
      Size = 128
    end
  end
end
