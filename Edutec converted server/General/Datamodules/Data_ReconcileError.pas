{*****************************************************************************
  Name           : Data_ReconcileError
  Author         : Cools Pascal
  Copyright      : (c) 2001 Peopleware
  Description    : This datamodule is used for error message handling. All
                   detailed information for all error messages are gathered
                   from database via this datamodule.
  History        :

  Date         By                   Description
  ----         --                   -----------
  08/07/2005   WL                   Copied from Construct server: no longer
                                    inherits from TdtmProvider !
                                    <TODO WL>Convert this into a remote datamodule ?
 *****************************************************************************}

unit Data_ReconcileError;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Provider, Db, //DBTables,
  ADODB;

type
  TdtmReconcileError = class(TDatamodule)
    adospGetErrorMsgFromConstraint: TADOStoredProc;
    adospGetErrorMsgFromNumber: TADOStoredProc;
  private
    procedure SetADOConnection(const Value: TADOConnection);
    { Private declarations }
  public
    { Public declarations }
    property ADOConnection: TADOConnection write SetADOConnection;
    function GetErrorMessageFromNumber(const aNumber : Integer) : String;
    function GetErrorMessageFromConstraint(const aMessage : String) : String;
  end;

var
  dtmReconcileError: TdtmReconcileError;

implementation

uses Remote_EduMain;

{$R *.DFM}

{*****************************************************************************
  Name           : TdtmReconcileError.GetErrorMessageFromConstraint 
  Author         : Cools Pascal                                          
  Arguments      : aMessage - The complete error string returned from the
                              database.
  Return Values  : None
  Exceptions     : None
  Description    : This function executes the stored procedure :
                   SP_SYS_GETERRORMESSAGEFROMCONSTRAINT
                   This stored procedure looks for the constraint name
                   (from T_SYS_CONSTRAINT) where the  constraint is part
                   of the given input message. If found, the corresponding
                   error message is retreived from (T_SYS_MESSAGE)
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  08/07/2005   WL                   Copied from Construct server
 *****************************************************************************}

function TdtmReconcileError.GetErrorMessageFromConstraint(
  const aMessage: String): String;
begin
  assert (adospGetErrorMsgFromConstraint.Connection <> nil);
  with adospGetErrorMsgFromConstraint do
  begin
    Parameters.ParamByName('@Error_Input').Value := aMessage;
    ExecProc;
    Result := Parameters.ParamByName('@Error_Message').Value;
  end;
end;

{*****************************************************************************
  Name           : TdtmReconcileError.GetErrorMessageFromNumber 
  Author         : Cools Pascal                                          
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This function executes the stored procedure :
                   SP_SYS_GETERRORMESSAGEFROMNUMBER
                   When the database generates a TRIGGER error, a message
                   is generated with the name of the TRIGGER followed by
                   the number. This function gets the detailed error message
                   from T_SYS_MESSAGE with the use of the given input number.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  08/07/2005   WL                   Copied from Construct server
 *****************************************************************************}

function TdtmReconcileError.GetErrorMessageFromNumber(
  const aNumber: Integer): String;
begin
  assert (adospGetErrorMsgFromNumber.Connection <> nil);
  with adospGetErrorMsgFromNumber do
  begin
    Parameters.ParamByName('@Error_Number').Value := aNumber;
    ExecProc;
    Result := Parameters.ParamByName('@Error_Message').Value;
  end;
end;

{*****************************************************************************
  Name           : TdtmReconcileError.SetADOConnection 
  Author         : Wim Lambrechts                                           
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Sets the ADOConnection of the needed datasets to the ADOConnection
                   received.
  History        :

  Date         By                   Description
  ----         --                   -----------
  08/07/2005   Wim Lambrechts       Initial creation of the Procedure.
 *****************************************************************************}
procedure TdtmReconcileError.SetADOConnection(const Value: TADOConnection);
begin
  adospGetErrorMsgFromConstraint.Connection := Value;
  adospGetErrorMsgFromNumber.Connection := value;
end;

end.
