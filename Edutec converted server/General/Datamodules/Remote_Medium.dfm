inherited rdtmMedium: TrdtmMedium
  OldCreateOrder = True
  Height = 184
  Width = 239
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_MEDIUM_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_MEDIUM_NAME '
      'FROM '
      '  V_GE_MEDIUM'
      '')
    object adoqryListF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_MEDIUM_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_MEDIUM_NAME '
      'FROM '
      '  V_GE_MEDIUM'
      '')
    object adoqryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
