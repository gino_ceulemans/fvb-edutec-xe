inherited rdtmCountryPart: TrdtmCountryPart
  OldCreateOrder = True
  Height = 184
  Width = 281
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_COUNTRYPART_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME,'
      '  F_COUNTRY_PART_NAME'
      'FROM         '
      '  V_GE_COUNTRY_PART')
    object adoqryListF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    SQL.Strings = (
      'SELECT     '
      '  F_COUNTRYPART_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME,'
      '  F_COUNTRY_PART_NAME'
      'FROM         '
      '  V_GE_COUNTRY_PART')
    object adoqryRecordF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
