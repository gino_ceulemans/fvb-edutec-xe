{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_GE_COUNTRY.

  @Name       Remote_Country
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  06/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Country;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmCountry = class(TrdtmEduBase, IrdtmCountry)
    adoqryListF_COUNTRY_ID: TIntegerField;
    adoqryListF_NAME_NL: TStringField;
    adoqryListF_NAME_FR: TStringField;
    adoqryListF_ISO_CODE: TStringField;
    adoqryListF_COUNTRY_NAME: TStringField;
    adoqryRecordF_COUNTRY_ID: TIntegerField;
    adoqryRecordF_NAME_NL: TStringField;
    adoqryRecordF_NAME_FR: TStringField;
    adoqryRecordF_ISO_CODE: TStringField;
    adoqryRecordF_COUNTRY_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;

  public
    function ApplyDetailFilter(const aProvider, aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer; //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer; //override; safecall;
    function GetNewRecordID: SYSINT; //override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer; //safecall;
    { Public declarations }
  end;

var
  ofCountry: TComponentFactory;

implementation

uses Remote_EduMain;

{$R *.DFM}

class procedure TrdtmCountry.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmCountry.ApplyDetailFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmCountry.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmCountry.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmCountry.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmCountry.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmCountry.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmCountry.GetTableName: String;
begin
  Result := 'T_GE_COUNTRY';
end;

function TrdtmCountry.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofCountry := TComponentFactory.Create(ComServer, TrdtmCountry,
    Class_rdtmCountry, ciInternal, tmApartment);

end.
