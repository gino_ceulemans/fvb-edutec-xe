object rdtmEDUMain: TrdtmEDUMain
  OldCreateOrder = False
  OnCreate = RemoteDataModuleCreate
  Height = 391
  Width = 513
  object adocnnMain: TADOConnection
    Connected = True
    ConnectionString = 
      'FILE NAME=C:\Program Files\Common Files\System\OLE DB\Data Links' +
      '\edutec.udl'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AfterConnect = adocnnMainAfterConnect
    Left = 32
    Top = 24
  end
  object adospRecordID: TFVBFFCStoredProc
    Connection = adocnnMain
    ProcedureName = 'SP_SYS_READHIGHESTVALUE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@StrTableName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = ''
      end
      item
        Name = '@nrintLatestValue'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = 0
      end>
    AutoOpen = False
    Left = 32
    Top = 72
  end
  object adoqryCurrentUser: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Username'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '  F_USER_ID,'
      '  F_LOCKED,'
      '  F_LOGIN_TRY,'
      '  F_LANGUAGE_ID,'
      '  F_NAME,'
      '  @@SPID as SPID'
      'from'
      '  V_SYS_USER_ACTIVE'
      'where'
      '  F_LOGIN_ID = :Username'
      ' ')
    AutoOpen = False
    Left = 364
    Top = 16
    object adoqryCurrentUserF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
    end
    object adoqryCurrentUserF_LOCKED: TBooleanField
      FieldName = 'F_LOCKED'
    end
    object adoqryCurrentUserF_LOGIN_TRY: TSmallintField
      FieldName = 'F_LOGIN_TRY'
    end
    object adoqryCurrentUserF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 129
    end
    object adoqryCurrentUserSPID: TSmallintField
      FieldName = 'SPID'
      ReadOnly = True
    end
    object adoqryCurrentUserF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object adocmnSetAppRole: TFVBFFCCommand
    CommandText = 'EXEC sp_setapprole '#39'EDU'#39', {Encrypt N'#39'EDU'#39'}, '#39'Odbc'#39
    Connection = adocnnMain
    Parameters = <>
    Left = 264
    Top = 16
  end
  object adospConnectionLog: TFVBFFCStoredProc
    Connection = adocnnMain
    ProcedureName = 'SP_CONNECTION;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@USERID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 268
    Top = 88
  end
  object adoqryGetNameForState: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@F_STATUS_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select F_NAME_NL from T_GE_STATUS where F_STATUS_ID = :@F_STATUS' +
        '_ID')
    AutoOpen = False
    Left = 64
    Top = 160
  end
  object adoqryReportRecordCount: TADOQuery
    Connection = adocnnMain
    Parameters = <>
    Left = 192
    Top = 152
  end
  object adoqryEnrolFromSessionId: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '1'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select'
      'F_ENROL_ID,'
      'F_COMPANY '
      'from'
      'V_PROG_GET_ENROL_FROM_SESSION'
      'where F_SESSION_ID = :1')
    AutoOpen = False
    Left = 196
    Top = 200
    object adoqryEnrolFromSessionIdF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
    end
    object adoqryEnrolFromSessionIdF_COMPANY: TStringField
      FieldName = 'F_COMPANY'
      Size = 64
    end
  end
  object adoqryPrintersCurrentUser: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      'F_PRINTER_LETTER,'
      'F_PRINTER_CERTIFICATE,'
      'F_PRINTER_EVALUATION'
      'from'
      'V_PROG_PRINTERS')
    AutoOpen = False
    Left = 372
    Top = 144
    object adoqryPrintersCurrentUserF_PRINTER_LETTER: TStringField
      FieldName = 'F_PRINTER_LETTER'
      ProviderFlags = []
      Size = 50
    end
    object adoqryPrintersCurrentUserF_PRINTER_CERTIFICATE: TStringField
      FieldName = 'F_PRINTER_CERTIFICATE'
      ProviderFlags = []
      Size = 50
    end
    object adoqryPrintersCurrentUserF_PRINTER_EVALUATION: TStringField
      FieldName = 'F_PRINTER_EVALUATION'
      ProviderFlags = []
      Size = 50
    end
  end
  object adospPrepareConfReports: TFVBFFCStoredProc
    Connection = adocnnMain
    ProcedureName = 'SP_PREPARE_CONFIRMATION_REPORTS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 136
    Top = 104
  end
  object adoqryGetCurrentUser: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select'
      '  F_FIRSTNAME + '#39' '#39' + F_LASTNAME AS F_USER'
      'from'
      '  V_SYS_USERINFO')
    AutoOpen = False
    Left = 372
    Top = 200
    object adoqryGetCurrentUserF_USER: TStringField
      FieldName = 'F_USER'
      ReadOnly = True
      Size = 129
    end
  end
  object adoqrySettingsCurrentUser: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '  [F_PATH_CRREPORTS],'
      '  [F_EMAIL_PUBLIC_FOLDER],'
      '  [F_PATH_CONFIRMATIONS]'
      'FROM'
      '  V_PROG_USER_SETTINGS'
      '')
    AutoOpen = False
    Left = 392
    Top = 88
    object adoqrySettingsCurrentUserF_PATH_CRREPORTS: TStringField
      FieldName = 'F_PATH_CRREPORTS'
      ProviderFlags = []
      Size = 255
    end
    object adoqrySettingsCurrentUserF_EMAIL_PUBLIC_FOLDER: TStringField
      FieldName = 'F_EMAIL_PUBLIC_FOLDER'
      ProviderFlags = []
      Size = 128
    end
    object adoqrySettingsCurrentUserF_PATH_CONFIRMATIONS: TStringField
      FieldName = 'F_PATH_CONFIRMATIONS'
      ProviderFlags = []
      Size = 255
    end
  end
  object adoqryGetNameForCooperation: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@F_COOPERATION_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select F_NAME_NL '
      'from T_GE_COOPERATION'
      'where F_COOPERATION_ID = :@F_COOPERATION_ID')
    AutoOpen = False
    Left = 64
    Top = 224
  end
  object adoqryGetNameForKMO_Portefeuille_State: TFVBFFCQuery
    Connection = adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@F_STATUS_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select F_NAME_NL from T_KMO_PORT_STATUS where F_KMO_PORT_STATUS_' +
        'ID = :@F_STATUS_ID')
    AutoOpen = False
    Left = 104
    Top = 280
  end
end
