{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_GE_POSTALCODE.
  
  @Name       Remote_PostalCode
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  08/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_PostalCode;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmPostalCode = class(TrdtmEduBase, IrdtmPostalCode)
    adoqryListF_POSTALCODE_ID: TIntegerField;
    adoqryListF_PROVINCE_ID: TIntegerField;
    adoqryListF_CITY_NL: TStringField;
    adoqryListF_CITY_FR: TStringField;
    adoqryListF_POSTALCODE: TStringField;
    adoqryListF_CITY_NAME: TStringField;
    adoqryListF_PROVINCE_NAME: TStringField;
    adoqryRecordF_POSTALCODE_ID: TIntegerField;
    adoqryRecordF_PROVINCE_ID: TIntegerField;
    adoqryRecordF_CITY_NL: TStringField;
    adoqryRecordF_CITY_FR: TStringField;
    adoqryRecordF_POSTALCODE: TStringField;
    adoqryRecordF_CITY_NAME: TStringField;
    adoqryRecordF_PROVINCE_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofPostalCode: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmPostalCode.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmPostalCode.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmPostalCode.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmPostalCode.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmPostalCode.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmPostalCode.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmPostalCode.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmPostalCode.GetTableName: String;
begin
  Result := 'T_GE_POSTALCODE';
end;

function TrdtmPostalCode.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofPostalCode := TComponentFactory.Create(ComServer, TrdtmPostalCode,
    Class_rdtmPostalCode, ciInternal, tmApartment);
    
end.
