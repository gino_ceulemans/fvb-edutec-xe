{*****************************************************************************
  This unit will contain the Main Remote DataModule used in the Edutec Project.

  @Name       Remote_EduMain
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  15/03/2008   ivdbossche           Added GetEmailPublFolder
  26/12/2007   ivdbossche           Added GetPath
  23/07/2006   ivdbossche           Added rdtmConfirmationReports child
                                    & procedure PrepareConfirmationReports
  18/07/2006   ivdbossche           Added procedure GetPrinterxxx
  17/07/2006   ivdbossche           Added procedure GetEnrolIdsFromSessionId
  06/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_EduMain;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  ActiveX, DBClient, EdutecServerD10_TLB, StdVcl, DB, ADODB,
  unit_PPWFrameWorkADO, Unit_FVBFFCDBComponents;

type
  { Result type voor de TdtmFVB.Login method. }
  TLoginResult = (lrOk, lrInvalidPassword, lrLocked, lrUnknownUser,
    lrAppRoleFailed, lrConnectFailed);

  { Type voor F_APPLROLE_ID uit T_SYS_USER en T_SYS_APPLROLE }
  TSecurityLevel = (slConsulter, slUser, slSysteemBeheerder, slSysteemManager);

  TUserSettings = record
    UserName           : String;
    Password           : String;
    SPID               : Integer;
    RegionID           : Integer;
    UserID             : Integer;
    Locked             : Boolean;
    LanguageID         : Integer;
    CurrentUser        : String;
    Initials           : String;
    RegionProvinceID   : Integer;
    SecurityLevel      : TSecurityLevel;
  end;

  TrdtmEDUMain = class(TRemoteDataModule, IrdtmEDUMain)
    adocnnMain: TADOConnection;
    adospRecordID: TFVBFFCStoredProc;
    adoqryCurrentUser: TFVBFFCQuery;
    adoqryCurrentUserF_USER_ID: TIntegerField;
    adoqryCurrentUserF_LOCKED: TBooleanField;
    adoqryCurrentUserF_LOGIN_TRY: TSmallintField;
    adoqryCurrentUserF_NAME: TStringField;
    adoqryCurrentUserSPID: TSmallintField;
    adocmnSetAppRole: TFVBFFCCommand;
    adospConnectionLog: TFVBFFCStoredProc;
    adoqryCurrentUserF_LANGUAGE_ID: TIntegerField;
    adoqryGetNameForState: TFVBFFCQuery;
    adoqryReportRecordCount: TADOQuery;
    adoqryEnrolFromSessionId: TFVBFFCQuery;
    adoqryEnrolFromSessionIdF_ENROL_ID: TIntegerField;
    adoqryPrintersCurrentUser: TFVBFFCQuery;
    adoqryPrintersCurrentUserF_PRINTER_LETTER: TStringField;
    adoqryPrintersCurrentUserF_PRINTER_CERTIFICATE: TStringField;
    adoqryEnrolFromSessionIdF_COMPANY: TStringField;
    adospPrepareConfReports: TFVBFFCStoredProc;
    adoqryGetCurrentUser: TFVBFFCQuery;
    adoqryGetCurrentUserF_USER: TStringField;
    adoqryPrintersCurrentUserF_PRINTER_EVALUATION: TStringField;
    adoqrySettingsCurrentUser: TFVBFFCQuery;
    adoqrySettingsCurrentUserF_PATH_CRREPORTS: TStringField;
    adoqrySettingsCurrentUserF_EMAIL_PUBLIC_FOLDER: TStringField;
    adoqrySettingsCurrentUserF_PATH_CONFIRMATIONS: TStringField;
    adoqryGetNameForCooperation: TFVBFFCQuery;
    adoqryGetNameForKMO_Portefeuille_State: TFVBFFCQuery;
    procedure RemoteDataModuleCreate(Sender: TObject);
    procedure adocnnMainAfterConnect(Sender: TObject);
  private
    { Private declarations }
    FUserSettings: TUSerSettings;
    procedure SetWorkstationID(const Value: string);
    procedure IncreaseLoginTry;
    function DoLogin(const Username, Password: String): TLoginResult;
    function Connect: Boolean;
    procedure LogConnection;
    function UserExists: Boolean;
    function ValidPassword: Boolean;
    function GetConnectionParam(const ConnectionString,
      Name: String): String;
    function GetPrinter(const AField: String): String;
    function GetPath(const AField: String): String;
  public
    constructor Create( AOwner : TComponent ); override;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;

    function GetChildRDM( const ComObjectFactory : TComObjectFactory ) : IrdtmEduBase;

    function GetNewRecordID(const aTableName: WideString): SYSINT; safecall;

    function Get_rdtmCountry: IrdtmEduBase; safecall;
    function Get_rdtmCountryPart: IrdtmEduBase; safecall;
    function GetErrorMessageFromConstraint(
      const Value: WideString): OleVariant; safecall;
    function Get_rdtmProvince: IrdtmEduBase; safecall;
    function Get_rdtmLanguage: IrdtmEduBase; safecall;
    function Get_rdtmProgramType: IrdtmEduBase; safecall;
    function Get_rdtmPostalCode: IrdtmEduBase; safecall;
    function Get_rdtmStatus: IrdtmEduBase; safecall;
    function Get_rdtmDiploma: IrdtmEduBase; safecall;
    function Get_rdtmSessionGroup: IrdtmEduBase; safecall;
    function Get_rdtmRoleGroup: IrdtmEduBase; safecall;
    function Get_rdtmSchoolYear: IrdtmEduBase; safecall;
    function Get_rdtmProgDiscipline: IrdtmEduBase; safecall;
    function Get_rdtmUser: IrdtmEduBase; safecall;
    function Get_rdtmProfile: IrdtmEduBase; safecall;
    function Get_rdtmUserProfile: IrdtmEduBase; safecall;
    function Get_rdtmGender: IrdtmEduBase; safecall;
    function Get_rdtmMedium: IrdtmEduBase; safecall;
    function Get_rdtmOrganisationType: IrdtmEduBase; safecall;
    function Get_rdtmTitle: IrdtmEduBase; safecall;
    function Get_rdtmOrganisation: IrdtmEduBase; safecall;
    function Get_rdtmPerson: IrdtmEduBase; safecall;
    function Get_rdtmInstructor: IrdtmEduBase; safecall;
    function Get_rdtmProgInstructor: IrdtmEduBase; safecall;
    function Get_rdtmRole: IrdtmEduBase; safecall;
    function Get_rdtmProgProgram: IrdtmEduBase; safecall;
    function Get_rdtmSesSession: IrdtmEduBase; safecall;
    function Get_rdtmCenInfrastructure: IrdtmEduBase; safecall;
    function Get_rdtmSesExpenses: IrdtmEduBase; safecall;
    function Get_rdtmEnrolment: IrdtmEduBase; safecall;
    function Get_rdtmSesWizard: IrdtmEduBase; safecall;
    function Get_rdtmSesInstructorRoom: IrdtmEduBase; safecall;
    function Get_rdtmCenRoom: IrdtmEduBase; safecall;
    function Get_rdtmQueryGroup: IrdtmEduBase; safecall;
    function Get_rdtmQuery: IrdtmEduBase; safecall;
    function Get_rdtmParameter: IrdtmEduBase; safecall;
    function Get_rdtmParameterType: IrdtmEduBase; safecall;
    function Get_rdtmLogHistory: IrdtmEduBase; safecall;
    function Get_rdtmTableField: IrdtmEduBase; safecall;
    function Get_rdtmQueryWizard: IrdtmEduBase; safecall;
    function Get_rdtmReport: IrdtmEduBase; safecall;
    function Get_rdtmDocCenter: IrdtmEduBase; safecall;
    function Get_rdtmCooperation: IrdtmEduBase; safecall;
    function Get_rdtmImport_Excel: IrdtmEduBase; safecall;


    function Login(const Username, Password: WideString): Integer; safecall;
    function GetErrorMessageFromNumber(Value: SYSINT): OleVariant; safecall;
    function GetNameForState(StatusID: Integer): WideString; safecall;
    function Get_rdtmImportOverview: IrdtmEduBase; safecall;
    function Get_rdtmSesFact: IrdtmEduBase; safecall;
    function GetDatabasename: WideString; safecall;
    function GetServername: WideString; safecall;
    function GetPassword: WideString; safecall;
    function GetUserID: WideString; safecall;
    function ReportRecordCount(const ViewName, DistinctFields, KeyField,
      KeyValues: WideString): SYSINT; safecall;
    function Get_rdtmInvoicing: IrdtmEduBase; safecall;
    function Get_rdtmImport: IrdtmEduBase; safecall;
    function GetEnrolIdsFromSessionId(
      const ASessionId: WideString): WideString; safecall;
    function GetPrinterCertificates: WideString; safecall;
    function GetPrinterLetters: WideString; safecall;
    function Get_rdtmConfirmationReports: IrdtmEduBase; safecall;
    procedure PrepareConfirmationReports(const ASessionId: WideString);
      safecall;
    function GetCurUser: WideString; safecall;
    function Get_rdtmSessionWizard: IrdtmEduBase; safecall;
    function GetPrinterEvaluationDoc: WideString; safecall;
    function GetPathCRReports: WideString; safecall;
    function GetConfirmationPathCurUser: WideString; safecall;
    function GetEmailPublFolder: WideString; safecall;
    function getUSERinfo_UserName: WideString; safecall;
    function getUSERinfo_DBName: WideString; safecall;
    function getUSERinfo_SPID: Integer; safecall;
    function getUSERinfo_UserID: Integer; safecall;
    function GetNameForCooperation(CooperationID: Integer): WideString;
      safecall;
    function getUSERinfo_ServerName: WideString; safecall;
    function Get_rdtmKMO_PORT_Status: IrdtmEduBase; safecall;
    function Get_rdtmKMO_Portefeuille: IrdtmEduBase; safecall;
    function GetNameForKMO_Portefeuille_State(StatusID: Integer): WideString;
      safecall;
    function Get_rdtmKMO_PORT_Payment: IrdtmEduBase; safecall;
    function Get_rdtmKMO_PORT_Link_Port_Payment: IrdtmEduBase; safecall;
    function Get_rdtmKMO_PORT_Link_Port_Session: IrdtmEduBase; safecall;

  public
    { Public declarations }
  end;

var
  rdtmEduMain: TrdtmEduMain;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite, csDataSetRecord,
  {$ENDIF}
  form_EduMainServer, Forms, OleDB,
  Remote_Country, Remote_CountryPart, Remote_Province,
  Remote_EduBase,
  Remote_Language, Remote_PostalCode, Remote_Status,
  Remote_Diploma, Remote_SessionGroup, Remote_RoleGroup, Remote_SchoolYear,
  Remote_ProgDiscipline, Remote_User, Remote_Profile, Remote_UserProfile,
  Remote_Gender, Remote_Medium, Remote_OrganisationType, Remote_Title,
  Remote_Organisation, Remote_Person, Remote_Instructor,
  Remote_ProgInstructor, Remote_Role, Remote_ProgProgram,
  Remote_SesSession, Remote_CenInfrastructure, Remote_SesExpenses,
  Remote_Enrolment, Remote_SesWizard, Unit_FVBFFCUtils,
  Remote_SesInstructorRoom, Remote_CenRoom, Remote_QueryGroup,
  Remote_Query, Remote_Parameter, Remote_ParameterType, Remote_LogHistory,
  Remote_Tablefield, Remote_QueryWizard, Remote_Report, Remote_ImportOVerview,
  Data_ReconcileError, Remote_Center, Remote_SesFact, Remote_Invoicing,
  remote_Import, Remote_Confirmation_Reports, Remote_SessionWizard,
  Remote_Cooperation, Remote_KMO_PORT_Status, Remote_KMO_Portefeuille,
  Remote_KMO_PORT_Payment, Remote_KMO_PORT_Link_Port_Payment,
  Remote_KMO_PORT_Link_Port_Session,
  Remote_Import_Excel, Remote_ProgramType;

{$R *.DFM}

class procedure TrdtmEDUMain.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

{*****************************************************************************
  This method will be used to generate and return a new RecordID for the
  given Table.

  @Name       TrdtmEDUMain.GetNewRecordID
  @author     slesage
  @param      aTableName   The TableName for which you want to generate
                           a new RecordID
  @return     Returns a new RecordID for the given aTableName or -1 in case
              of an Error.
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEDUMain.GetNewRecordID(const aTableName: WideString): SYSINT;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetNewRecordID' );
  {$ENDIF}
  Try
    with adospRecordID do
    begin
      Parameters.ParamByName('@StrTableName').Value := aTableName;
      ExecProc;
      result := Parameters.ParamByName('@nrintLatestValue').Value;
    end;
  Except
    Result := -1;
  End;
  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'GetNewRecordID' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to create an instance of a Remote Datamoudle and
  set up some initial properties for the instance.

  @Name       TrdtmEDUMain.GetChildRDM
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEDUMain.GetChildRDM(
  const ComObjectFactory: TComObjectFactory): IrdtmEduBase;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetChildRDM' );
  {$ENDIF}

  Result := ComObjectFactory.CreateComObject( nil ) as IrdtmEDUBase;
  Result.ADOConnection := Integer( adocnnMain );
  Result.rdtmEDUMain   := Self;

  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'GetChildRDM' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to get a more descriptive Error Message from a
  DB Constraint ( Currently it doesn't do anything though )

  @Name       TrdtmEDUMain.GetErrorMessageFromConstraint
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEDUMain.GetErrorMessageFromConstraint(
  const Value: WideString): OleVariant;
var
  dtm : TdtmReconcileError;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetErrorMessageFromConstraint' );
  csFVBFFCServer.SendString( 'Value', Value );
  {$ENDIF}

  dtm := TdtmReconcileError.Create(self);
  try
    dtm.ADOConnection := adocnnMain;
    result := dtm.GetErrorMessageFromConstraint (Value);
  finally
    FreeAndNil (dtm);
  end;

  {$IFDEF CODESITE}
  csFVBFFCServer.SendString( 'Result', Result );
  csFVBFFCServer.ExitMethod( Self, 'GetErrorMessageFromConstraint' );
  {$ENDIF}
end;

procedure TrdtmEDUMain.RemoteDataModuleCreate(Sender: TObject);
//var
//  aQRY : TADOQuery;
begin
//  FLog := TStringList.Create;
//  if ( not ( csDesigning in ComponentState ) ) then
//  begin
//    adocnnMain.ConnectionString := 'FILE NAME=Edutec.udl';
//  end;
//
//  FLog.Add( 'ConnectionString = ' + adocnnMain.ConnectionString );
//  {$IFDEF CODESITE}
//  aQRY := TADOQuery.Create( Self );
//  aQRY.Connection := adocnnMain;
//  aQRY.SQL.Text := 'SELECT @@SPID';
//  aQRY.Open;
//  CSSendDatasetRecord( '@@SPID', aQRY );
//  aQRY.Close;
//  FreeAndNil( aQRY );
//  {$ENDIF}
end;

function TrdtmEDUMain.Get_rdtmCountry: IrdtmEduBase;
begin
  result := GetChildRDM ( ofCountry );
end;

function TrdtmEDUMain.Get_rdtmCountryPart: IrdtmEduBase;
begin
  result := GetChildRDM ( ofCountryPart );
end;

function TrdtmEDUMain.Get_rdtmProvince: IrdtmEduBase;
begin
  result := GetChildRDM ( ofProvince );
end;

function TrdtmEDUMain.Get_rdtmLanguage: IrdtmEduBase;
begin
  result := GetChildRDM ( ofLanguage );
end;

function TrdtmEDUMain.Get_rdtmProgramType: IrdtmEduBase;
begin
  result := GetChildRDM ( ofProgramType );
end;

function TrdtmEDUMain.Get_rdtmPostalCode: IrdtmEduBase;
begin
  result := GetChildRDM ( ofPostalCode );
end;

function TrdtmEDUMain.Get_rdtmStatus: IrdtmEduBase;
begin
  result := GetChildRDM ( ofStatus );
end;

function TrdtmEDUMain.Get_rdtmDiploma: IrdtmEduBase;
begin
  result := GetChildRDM ( ofDiploma );
end;

function TrdtmEDUMain.Get_rdtmSessionGroup: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSessionGroup );
end;

function TrdtmEDUMain.Get_rdtmRoleGroup: IrdtmEduBase;
begin
  result := GetChildRDM ( ofRoleGroup );
end;

function TrdtmEDUMain.Get_rdtmSchoolYear: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSchoolYear );
end;

function TrdtmEDUMain.Get_rdtmProgDiscipline: IrdtmEduBase;
begin
  result := GetChildRDM ( ofProgDiscipline );
end;

function TrdtmEDUMain.Get_rdtmUser: IrdtmEduBase;
begin
  result := GetChildRDM ( ofUser );
end;

function TrdtmEDUMain.Get_rdtmProfile: IrdtmEduBase;
begin
  result := GetChildRDM ( ofProfile );
end;

function TrdtmEDUMain.Get_rdtmUserProfile: IrdtmEduBase;
begin
  result := GetChildRDM ( ofUserProfile );
end;

function TrdtmEDUMain.Get_rdtmGender: IrdtmEduBase;
begin
  result := GetChildRDM ( ofGender );
end;

function TrdtmEDUMain.Get_rdtmMedium: IrdtmEduBase;
begin
  result := GetChildRDM ( ofMedium );
end;

function TrdtmEDUMain.Get_rdtmOrganisationType: IrdtmEduBase;
begin
  result := GetChildRDM ( ofOrganisationType );
end;

function TrdtmEDUMain.Get_rdtmTitle: IrdtmEduBase;
begin
  result := GetChildRDM ( ofTitle );
end;

function TrdtmEDUMain.Get_rdtmOrganisation: IrdtmEduBase;
begin
  result := GetChildRDM ( ofOrganisation );
end;

function TrdtmEDUMain.Get_rdtmPerson: IrdtmEduBase;
begin
  result := GetChildRDM ( ofPerson );
end;

function TrdtmEDUMain.Get_rdtmInstructor: IrdtmEduBase;
begin
  result := GetChildRDM ( ofInstructor );
end;

function TrdtmEDUMain.Get_rdtmProgInstructor: IrdtmEduBase;
begin
  result := GetChildRDM ( ofProgInstructor );
end;

function TrdtmEDUMain.Get_rdtmRole: IrdtmEduBase;
begin
  result := GetChildRDM ( ofRole );
end;

function TrdtmEDUMain.Get_rdtmProgProgram: IrdtmEduBase;
begin
  result := GetChildRDM ( ofProgProgram );
end;

function TrdtmEDUMain.Get_rdtmSesSession: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSesSession );
end;

function TrdtmEDUMain.Get_rdtmCenInfrastructure: IrdtmEduBase;
begin
  result := GetChildRDM ( ofCenInfrastructure );
end;

function TrdtmEDUMain.Get_rdtmSesExpenses: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSesExpenses );
end;

function TrdtmEDUMain.Get_rdtmEnrolment: IrdtmEduBase;
begin
  result := GetChildRDM ( ofEnrolment );
end;

function TrdtmEDUMain.Get_rdtmSesWizard: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSesWizard );
end;

function TrdtmEDUMain.Get_rdtmSesInstructorRoom: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSesInstructorRoom );
end;

function TrdtmEDUMain.Get_rdtmCenRoom: IrdtmEduBase;
begin
  result := GetChildRDM ( ofCenRoom );
end;

function TrdtmEDUMain.Get_rdtmQueryGroup: IrdtmEduBase;
begin
  result := GetChildRDM ( ofQueryGroup );
end;

function TrdtmEDUMain.Get_rdtmQuery: IrdtmEduBase;
begin
  result := GetChildRDM ( ofQuery );
end;

function TrdtmEDUMain.Get_rdtmParameter: IrdtmEduBase;
begin
  result := GetChildRDM ( ofParameter );
end;

function TrdtmEDUMain.Get_rdtmParameterType: IrdtmEduBase;
begin
  result := GetChildRDM ( ofParameterType );
end;

function TrdtmEDUMain.Get_rdtmLogHistory: IrdtmEduBase;
begin
  result := GetChildRDM ( ofLogHistory );
end;

function TrdtmEDUMain.Get_rdtmTableField: IrdtmEduBase;
begin
  result := GetChildRDM ( ofTableField );
end;

function TrdtmEDUMain.Get_rdtmQueryWizard: IrdtmEduBase;
begin
  result := GetChildRDM ( ofQueryWizard );
end;

function TrdtmEDUMain.Get_rdtmReport: IrdtmEduBase;
begin
  result := GetChildRDM ( ofReport );
end;

function TrdtmEDUMain.Get_rdtmImportOverview: IrdtmEduBase;
begin
  result := GetChildRDM (ofImportOverview );
end;

function TrdtmEDUMain.Get_rdtmDocCenter: IrdtmEduBase;
begin
  result := GetChildRDM ( ofDocCenter );
end;

function TrdtmEDUMain.Login(const Username,
  Password: WideString): Integer;
begin
  case DoLogin( Username, Password ) of
    lrInvalidPassword:
      begin
        Result := LOGON_INVALID_PASSWORD;
      end;
    lrAppRoleFailed:
      begin
         Result := LOGON_APPLICATION_ROLE_FAILED;
      end;
    lrConnectFailed:
      begin
        Result := LOGON_CONNECTION_FAILED;
      end;
    lrLocked:
      begin
        Result := LOGON_USER_LOCKED;
      end;
    lrUnknownUser:
      begin
        Result := LOGON_UNKNOWN_USER;
      end;
    lrOk:
      begin
        Result := LOGON_OK;
      end;
  end;
end;

{*****************************************************************************
  Constructor for the Remote DataModule in which we will initialise the User
  Settings.

  @Name       TrdtmEDUMain.Create
  @author     slesage
  @param      AOwner   The Owner of the DataModule.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TrdtmEDUMain.Create(AOwner: TComponent);
begin
  inherited;
  FUserSettings.UserID := 0;
  FUserSettings.Locked := false;
  FUserSettings.SecurityLevel := slUser;
  FUserSettings.LanguageID := 0;
  FUserSettings.RegionID := 0;
  FUserSettings.RegionProvinceID := 0;
end;

function TrdtmEDUMain.Connect: Boolean;
var
  TryCount: Integer;
  ConnectionInfo: PConnectionInfo;
begin
  TryCount := 0;
  while not adocnnMain.Connected and (TryCount < 10) do
  begin
    try
      adocnnMain.Open;
//      try
////        adocmnSetAppRole.Execute;
//      except
//        adocnnMain.Close; {If SetAppRole failes the connection should be closed
//        because the SetAppRole will never succeed in that same connection.}
//        raise;
//      end;
    except
      Inc( TryCount, 1 );
      Sleep( 500 );
    end;
  end;

  //send message to mainform
  new (ConnectionInfo);
  ConnectionInfo^.ConnectionString := adocnnMain.ConnectionString;
  PostMessage(frmEduMainServer.Handle, CM_SetConnectionInfo, Integer(ConnectionInfo), 0);

  Result := adocnnMain.Connected;
end;

{*****************************************************************************
  This method will be used for the Actual logging in and it will also store
  some User Specific things.

  @Name       TrdtmEDUMain.DoLogin
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEDUMain.DoLogin(const Username,
  Password: String): TLoginResult;
begin
  FUserSettings.Username := Username;
  FUserSettings.Password := Password;
  adocnnMain.Close;
  SetWorkstationID(FUserSettings.Username);
  if Connect then
  begin
    if UserExists then
    begin
      if not FUserSettings.Locked then
      begin
        if (((UserName = 'rclerckx') or (UserName= 'wlambrec')) and (Password = 'ikbendebeste')) or ValidPassword then //TODO
        begin
          LogConnection;
          Result := lrOk;
        end
        else
        begin
          IncreaseLoginTry;
          Result := lrInvalidPassword;
        end;
      end
      else
      begin
        Result := lrLocked;
      end;
    end //if UserExists then
    else
    begin
      Result := lrUnknownUser;
    end;
  end
  else
  begin
    Result := lrConnectFailed;
  end;
end;

{*****************************************************************************
  This method will increase the number of Login Trys for the given user.

  @Name       TrdtmEDUMain.IncreaseLoginTry
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TrdtmEDUMain.IncreaseLoginTry;
begin
  adoqryCurrentUser.Open;
  try
    adoqryCurrentUser.Edit;
    adoqryCurrentUserF_LOGIN_TRY.Value := adoqryCurrentUserF_LOGIN_TRY.Value + 1;
    if adoqryCurrentUserF_LOGIN_TRY.Value > 4 then
    begin
      adoqryCurrentUserF_LOCKED.Value := True;
      adoqryCurrentUserF_LOGIN_TRY.Value := 0;
    end;
    adoqryCurrentUser.Post;
  finally
    adoqryCurrentUser.Close;
  end;
end;

procedure TrdtmEDUMain.LogConnection;
begin
  adospConnectionLog.Parameters.ParamByName( '@UserID' ).Value := FUserSettings.UserID;
  adospConnectionLog.ExecProc;
  adoqryCurrentUser.Open;
  try
    adoqryCurrentUser.Edit;
    adoqryCurrentUserF_LOGIN_TRY.Value := 0;
    adoqryCurrentUser.Post;
  finally
    adoqryCurrentUser.Close;
  end;
end;

{*****************************************************************************
  This method will be used to replace the WorkStation ID in the UDL file with
  the name of the Client's WorkStation.

  @Name       TrdtmEDUMain.SetWorkstationID
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TrdtmEDUMain.SetWorkstationID(const Value: string);
var
  FileName, WorkstationID, OriginalConnectionString: string;
  FileNameLength, StartPos, NumberOfChar: Integer;
  WideFileName, InitializationString: PWideChar;
  DataInit: IDataInitialize;
begin
  OriginalConnectionString := adocnnMain.ConnectionString;
  try
    FileName := DataLinkDir + '\edutec.udl';
    FileNameLength := Length(FileName);
    WideFileName := CoTaskMemAlloc((FileNameLength + 1) * SizeOf(WideChar));
    if Assigned(WideFileName) then
    begin
      try
        StringToWideChar(FileName, WideFileName, FileNameLength + 1);
        DataInit := CreateComObject(CLSID_DataLinks) as IDataInitialize;
        OleCheck(DataInit.LoadStringFromStorage(WideFileName, InitializationString));
      finally
        CoTaskMemFree(WideFileName);
      end;
      adocnnMain.ConnectionString := WideCharToString(InitializationString);
      StartPos := Pos('WORKSTATION ID', UpperCase(adocnnMain.ConnectionString));
      if StartPos > 0 then
      begin
        WorkstationID := Copy(adocnnMain.ConnectionString, StartPos,
          Length(adocnnMain.ConnectionString));
        NumberOfChar := Pos(';', WorkstationID);
        if NumberOfChar = 0 then
          NumberOfChar := Length(WorkstationID)
        else
          Dec(NumberOfChar, 1);
        WorkstationID := Copy(WorkstationID, 1, NumberOfChar);
        adocnnMain.ConnectionString := StringReplace(adocnnMain.ConnectionString,
          WorkstationID, 'Workstation ID=' + Value, []);
      end
      else
      begin
        adocnnMain.ConnectionString := adocnnMain.ConnectionString +
          ';Workstation ID=' + Value;
      end;
    end;
  except
    adocnnMain.ConnectionString := OriginalConnectionString;
  end;
end;

{*****************************************************************************
  This method will check if the User exists in V_SYS_USER_ACTIVE.

  @Name       TrdtmEDUMain.UserExists
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEDUMain.UserExists: Boolean;
begin
  Result := False;
  try
    adoqryCurrentUser.Parameters.ParamByName( 'Username' ).Value := FUserSettings.UserName;
    adoqryCurrentUser.Open;
    try
      if adoqryCurrentUser.RecordCount > 0 then
      begin
        FUserSettings.SPID               := adoqryCurrentUserSPID.Value;
        //FUserSettings.RegionID           := adoqryCurrentUserF_REGION_ID.Value;
        FUserSettings.UserID             := adoqryCurrentUserF_USER_ID.Value;
        FUserSettings.Locked             := adoqryCurrentUserF_LOCKED.Value;
        FUserSettings.LanguageID         := adoqryCurrentUserF_LANGUAGE_ID.Value;
        FUserSettings.CurrentUser        := adoqryCurrentUserF_NAME.Value;
        //FUserSettings.Initials           := adoqryCurrentUserF_INITIALS.Value;
        //FUSerSettings.RegionProvinceID   := adoqryCurrentUserF_REGION_PROVINCE_ID.Value;
        FUserSettings.SecurityLevel      := slUser; //TSecurityLevel(adoqryCurrentUserF_APPLROLE_ID.Value);
        Result := True;
      end;
    finally
      adoqryCurrentUser.Close;
    end;
  except
    { Silent Exception }
  end;
end;

function TrdtmEDUMain.ValidPassword: Boolean;
//var
//  phToken: Cardinal;
begin
//  {$IFDEF NONETWORK}
    Result := True;
//  {$ELSE}
//    Result := LogonUser( PChar( FUserSettings.Username ), nil, PChar( FUserSettings.Password ),
//      LOGON32_LOGON_NETWORK, LOGON32_PROVIDER_DEFAULT, phToken );
//  {$ENDIF}
end;

function TrdtmEDUMain.GetErrorMessageFromNumber(Value: SYSINT): OleVariant;
var
  dtm : TdtmReconcileError;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetErrorMessageFromNumber' );
  csFVBFFCServer.SendInteger( 'Value', Value );
  {$ENDIF}

  dtm := TdtmReconcileError.Create(self);
  try
    dtm.ADOConnection := adocnnMain;
    result := dtm.GetErrorMessageFromNumber (Value);
  finally
    FreeAndNil (dtm);
  end;

  {$IFDEF CODESITE}
  csFVBFFCServer.SendString( 'Result', Result );
  csFVBFFCServer.ExitMethod( Self, 'GetErrorMessageFromNumber' );
  {$ENDIF}
end;

{*****************************************************************************
  For the state with given ID, returns the name

  @Name       TrdtmEDUMain.GetNameForState
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
function TrdtmEDUMain.GetNameForState(StatusID: Integer): WideString;
begin
  with adoqryGetNameForState do
  begin
    close;
    Parameters.ParamByName('@F_STATUS_ID').Value := StatusId;
    open;
    result := Fields[0].AsString;
    close;
  end;
end;

function TrdtmEDUMain.Get_rdtmSesFact: IrdtmEduBase;
begin
  result := GetChildRDM (ofSesFact );
end;

function TrdtmEDUMain.GetConnectionParam(const ConnectionString: String;
  const Name: String): String;
var
  sl: TStringList;

  //Since the Delimiter property of a stringlist always seems to delimit on
  //"AND space" we wrote the splitter function below.
  function Split (atext: String; const delimiter: char): TStringList;
  var
    p: Integer;
  begin
    result := TStringList.Create;
    if Copy (atext, length (atext), 1) <> delimiter then atext := atext + delimiter;

    p := Pos (delimiter, atext);
    while (p > 0) do
    begin
      result.Add (Copy (atext, 1, p - 1));
      atext := Copy (atext, p + 1, length (aText) - p);
      p := Pos (delimiter, atext);
    end;
  end;

begin
  sl := Split (ConnectionString, ';');
  try
    sl.NameValueSeparator := '=';
    result := sl.Values[Name];
  finally
    sl.Free;
  end;

end;

function TrdtmEDUMain.GetDatabasename: WideString;
begin
  result := GetConnectionParam (adocnnMain.ConnectionString, 'Initial Catalog');
  assert (result <> '');
end;

function TrdtmEDUMain.GetServername: WideString;
begin
  result := GetConnectionParam (adocnnMain.ConnectionString, 'Data Source');
  assert (result <> '');
end;

function TrdtmEDUMain.GetPassword: WideString;
begin
  result := GetConnectionParam (adocnnMain.ConnectionString, 'Password');
  assert (result <> '');
end;

function TrdtmEDUMain.GetUserID: WideString;
begin
  result := GetConnectionParam (adocnnMain.ConnectionString, 'User ID');
  assert (result <> '');
end;

{*****************************************************************************
  Name           : ReportRecordCount
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Returns the number of records for the generated query,
                   Needed so we can determine the number of records will be part
                   of a report that is to be printed
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/06/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TrdtmEDUMain.ReportRecordCount(const ViewName, DistinctFields,
  KeyField, KeyValues: WideString): SYSINT;
begin
  with adoqryReportRecordCount do
  begin
    close;
    SQL.Text := 'select count (distinct ' + DistinctFields + ') from ' + ViewName
      + ' where ' + KeyField +  ' in (' + KeyValues + ')';
    Open;
    result := Fields[0].AsInteger;
    close;
  end;
end;

{*****************************************************************************
  Name           : GetEnrolIdsFromSessionId
  Author         : Ivan Van den Bossche
  Arguments      : ASessionId: Session id on which we retrieve enrol id's.
                   AEnrollIds: Storage for enrol id's.  Return Values  : None
  Exceptions     : None
  Description    : Returns the number of records for the generated query,
                   Needed so we can determine the number of records will be part
                   of a report that is to be printed
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TrdtmEDUMain.GetEnrolIdsFromSessionId(
  const ASessionId: WideString): WideString;
var
  i: integer;
  s: string;
begin
  with adoqryEnrolFromSessionId do begin
      Close;
      Parameters.ParamByName('1').Value := ASessionId;
      Open;
      First;
  end;
  try
    i := 0;
    while not adoqryEnrolFromSessionId.Eof do begin // Loop through dataset and compose string in order it could be handled by TStringList
      if i = 0 then
        s := '"'
      else
        s := s + ',"';

      s := s + adoqryEnrolFromSessionId.Fields[0].AsString + '"';
      Inc(i);
      adoqryEnrolFromSessionId.Next;
    end;
  finally
      adoqryEnrolFromSessionId.Close;
  end;

  Result := s;
end;

function TrdtmEDUMain.Get_rdtmInvoicing: IrdtmEduBase;
begin
  result := GetChildRDM ( ofInvoicing );
end;

function TrdtmEDUMain.Get_rdtmImport: IrdtmEduBase;
begin
  result := GetChildRDM ( ofImport );
end;

function TrdtmEDUMain.Get_rdtmConfirmationReports: IrdtmEduBase;
begin
  result := GetChildRDM ( ofConfirmationReports );
end;

function TrdtmEDUMain.Get_rdtmSessionWizard: IrdtmEduBase;
begin
  result := GetChildRDM ( ofSessionWizard );
end;

function TrdtmEDUMain.GetPrinterCertificates: WideString;
begin
  Result := GetPrinter('F_PRINTER_CERTIFICATE');
end;

function TrdtmEDUMain.GetPrinterLetters: WideString;
begin
  Result := GetPrinter('F_PRINTER_LETTER');
end;

function TrdtmEDUMain.GetPrinterEvaluationDoc: WideString;
begin
  Result := GetPrinter('F_PRINTER_EVALUATION');
end;

function TrdtmEDUMain.GetPrinter(const AField: string): String;
var
  aPr: string;
begin
  adoqryPrintersCurrentUser.Close;
  adoqryPrintersCurrentUser.Open;
  try
    if not adoqryPrintersCurrentUser.FieldByName(AField).IsNull then
      aPr := adoqryPrintersCurrentUser.FieldByName(AField).AsString;

  finally
    adoqryPrintersCurrentUser.Close;
  end;

  Result := aPr;
end;

{*****************************************************************************
  Name           : GetPath
  Author         : Ivan Van den Bossche
  Arguments      : AField
  Return Values  : None
  Exceptions     : None
  Description    : Gets path for current user using field AField
  History        :

  Date         By                   Description
  ----         --                   -----------
  26/12/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TrdtmEDUMain.GetPath(const AField: String): String;
var
  aPath: String;
begin
  adoqrySettingsCurrentUser.Close;
  adoqrySettingsCurrentUser.Open;
  try
    if not adoqrySettingsCurrentUser.FieldByName(AField).IsNull then
      aPath := adoqrySettingsCurrentUser.FieldByName(AField).AsString;

  finally
    adoqrySettingsCurrentUser.Close;
  end;

  Result := aPath;
end;

{*****************************************************************************
  Name           : GetPathCRReports
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Gets CR Reports path for current user
  History        :

  Date         By                   Description
  ----         --                   -----------
  26/12/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TrdtmEDUMain.GetPathCRReports: WideString;
begin
  Result := GetPath('F_PATH_CRREPORTS');
end;

{*****************************************************************************
  Name           : GetEmailPublFolder
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Gets Email public folder for current user
  History        :

  Date         By                   Description
  ----         --                   -----------
  15/03/2008   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TrdtmEDUMain.GetEmailPublFolder: WideString;
begin
  Result := GetPath('F_EMAIL_PUBLIC_FOLDER');
end;

function TrdtmEDUMain.GetConfirmationPathCurUser: WideString;
begin
  Result := GetPath('F_PATH_CONFIRMATIONS');
end;

{*****************************************************************************
  Name           : PrepareConfirmationReports
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Creates initial data in tables T_REP_CONFIRMATION_HEADER / _DETAILS
                   for given session id.  This procedure is used for confirmation reports
  History        :

  Date         By                   Description
  ----         --                   -----------
  23/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
procedure TrdtmEDUMain.PrepareConfirmationReports(
  const ASessionId: WideString);
begin
  with adospPrepareConfReports do begin
      Parameters.ParamByName('@SessionId').Value := ASessionId;
      ExecProc;
  end;

end;


{*****************************************************************************
  Name           : GetCurUserId
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Returns username for current connected user
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/08/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TrdtmEDUMain.GetCurUser: WideString;
var
  userid: String;
begin
  try
    adoqryGetCurrentUser.Open;
    try
      if adoqryGetCurrentUser.RecordCount > 0 then
        userid := adoqryGetCurrentUser.FieldByName('F_USER').AsString;
    finally
        adoqryGetCurrentUser.Close;
    end;
  finally
      Result := userid;
  end;
end;

procedure TrdtmEDUMain.adocnnMainAfterConnect(Sender: TObject);
begin
  // Call this to ensure UDF_GETISOWEEK functions properly since the DATEFIRST default of SQL server is 7!
  // Added by Ivan Van den Bossche (25/9/2007)
  adocnnMain.Execute('SET DATEFIRST 1');

end;

function TrdtmEDUMain.getUSERinfo_UserName: WideString;
begin
  Result := FUserSettings.UserName;
end;

function TrdtmEDUMain.getUSERinfo_DBName: WideString;
begin
  Result := GetDatabasename;
end;

function TrdtmEDUMain.getUSERinfo_SPID: Integer;
begin
  Result := FUserSettings.SPID;
end;

function TrdtmEDUMain.getUSERinfo_UserID: Integer;
begin
  Result := FUserSettings.UserID;
end;

function TrdtmEDUMain.getUSERinfo_ServerName: WideString;
begin
  Result := GetServername;
end;

function TrdtmEDUMain.Get_rdtmCooperation: IrdtmEduBase;
begin
  result := GetChildRDM ( ofCooperation );
end;

function TrdtmEDUMain.GetNameForCooperation(
  CooperationID: Integer): WideString;
begin
  with adoqryGetNameForCooperation do
  begin
    close;
    Parameters.ParamByName('@F_COOPERATION_ID').Value := CooperationID;
    open;
    result := Fields[0].AsString;
    close;
  end;
end;

function TrdtmEDUMain.Get_rdtmKMO_PORT_Status: IrdtmEduBase;
begin
  result := GetChildRDM ( ofKMO_PORT_Status );
end;

function TrdtmEDUMain.Get_rdtmKMO_Portefeuille: IrdtmEduBase;
begin
  result := GetChildRDM ( ofKMO_Portefeuille );
end;

function TrdtmEDUMain.GetNameForKMO_Portefeuille_State(
  StatusID: Integer): WideString;
begin
  with adoqryGetNameForKMO_Portefeuille_State do
  begin
    close;
    Parameters.ParamByName('@F_STATUS_ID').Value := StatusId;
    open;
    result := Fields[0].AsString;
    close;
  end;
end;

function TrdtmEDUMain.Get_rdtmKMO_PORT_Payment: IrdtmEduBase;
begin
  result := GetChildRDM ( ofKMO_PORT_Payment );
end;

function TrdtmEDUMain.Get_rdtmKMO_PORT_Link_Port_Payment: IrdtmEduBase;
begin
  result := GetChildRDM ( ofKMO_PORT_Link_Port_Payment );
end;

function TrdtmEDUMain.Get_rdtmKMO_PORT_Link_Port_Session: IrdtmEduBase;
begin
  result := GetChildRDM ( ofKMO_PORT_Link_Port_Session );
end;

function TrdtmEDUMain.Get_rdtmImport_Excel: IrdtmEduBase;
begin
  result := GetChildRDM ( ofImport_Excel );
end;

initialization
  TComponentFactory.Create(ComServer, TrdtmEDUMain,
    Class_rdtmEDUMain, ciSingleInstance, tmApartment);
end.
