object dtmDataSnapServer: TdtmDataSnapServer
  OldCreateOrder = False
  Height = 702
  Width = 1017
  object DSServerEduTec: TDSServer
    OnError = DSServerEduTecError
    Left = 56
    Top = 24
  end
  object DSTCPServerTransport215: TDSTCPServerTransport
    Port = 215
    Server = DSServerEduTec
    Filters = <>
    Left = 64
    Top = 96
  end
  object dsscLanguage: TDSServerClass
    OnGetClass = dsscLanguageGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 144
  end
  object dsscProgProgram: TDSServerClass
    OnGetClass = dsscProgProgramGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 88
  end
  object dsscCountryPart: TDSServerClass
    OnGetClass = dsscCountryPartGetClass
    Server = DSServerEduTec
    Left = 352
    Top = 88
  end
  object dsscProvince: TDSServerClass
    OnGetClass = dsscProvinceGetClass
    Server = DSServerEduTec
    Left = 352
    Top = 144
  end
  object dsscPostalCode: TDSServerClass
    OnGetClass = dsscPostalCodeGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 200
  end
  object dsscCooperation: TDSServerClass
    OnGetClass = dsscCooperationGetClass
    Server = DSServerEduTec
    Left = 352
    Top = 200
  end
  object dfsscGender: TDSServerClass
    OnGetClass = dfsscGenderGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 88
  end
  object dsscMedium: TDSServerClass
    OnGetClass = dsscMediumGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 144
  end
  object dsscSchoolYear: TDSServerClass
    OnGetClass = dsscSchoolYearGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 200
  end
  object dsscStatus: TDSServerClass
    OnGetClass = dsscStatusGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 144
  end
  object dsscTitle: TDSServerClass
    OnGetClass = dsscTitleGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 200
  end
  object dsscInstructor: TDSServerClass
    OnGetClass = dsscInstructorGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 88
  end
  object dsscProgInstructor: TDSServerClass
    OnGetClass = dsscProgInstructorGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 144
  end
  object dsscLogHistory: TDSServerClass
    OnGetClass = dsscLogHistoryGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 200
  end
  object dsscInvoicing: TDSServerClass
    OnGetClass = dsscInvoicingGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 88
  end
  object dsscSessionWizard: TDSServerClass
    OnGetClass = dsscSessionWizardGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 144
  end
  object dsscDiploma: TDSServerClass
    OnGetClass = dsscDiplomaGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 200
  end
  object dsscCenInfrastructure: TDSServerClass
    OnGetClass = dsscCenInfrastructureGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 256
  end
  object dsscCenRoom: TDSServerClass
    OnGetClass = dsscCenRoomGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 312
  end
  object dsscParameter: TDSServerClass
    OnGetClass = dsscParameterGetClass
    Server = DSServerEduTec
    Left = 352
    Top = 256
  end
  object dsscParameterType: TDSServerClass
    OnGetClass = dsscParameterTypeGetClass
    Server = DSServerEduTec
    Left = 352
    Top = 312
  end
  object dsscQuery: TDSServerClass
    OnGetClass = dsscQueryGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 256
  end
  object dsscQueryGroup: TDSServerClass
    OnGetClass = dsscQueryGroupGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 312
  end
  object dsscQueryWizard: TDSServerClass
    OnGetClass = dsscQueryWizardGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 256
  end
  object dsscReport: TDSServerClass
    OnGetClass = dsscReportGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 312
  end
  object dsscTablefield: TDSServerClass
    OnGetClass = dsscTablefieldGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 256
  end
  object dsscProfile: TDSServerClass
    OnGetClass = dsscProfileGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 312
  end
  object dsscUser: TDSServerClass
    OnGetClass = dsscUserGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 256
  end
  object dsscUserProfile: TDSServerClass
    OnGetClass = dsscUserProfileGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 312
  end
  object dsscImport: TDSServerClass
    OnGetClass = dsscImportGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 368
  end
  object dsscImportOverview: TDSServerClass
    OnGetClass = dsscImportOverviewGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 424
  end
  object dsscImport_Excel: TDSServerClass
    OnGetClass = dsscImport_ExcelGetClass
    Server = DSServerEduTec
    Left = 348
    Top = 368
  end
  object dsscKMO_PORT_Link_Port_Payment: TDSServerClass
    OnGetClass = dsscKMO_PORT_Link_Port_PaymentGetClass
    Server = DSServerEduTec
    Left = 348
    Top = 424
  end
  object dsscKMO_PORT_Link_Port_Session: TDSServerClass
    OnGetClass = dsscKMO_PORT_Link_Port_SessionGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 368
  end
  object dsscKMO_PORT_Payment: TDSServerClass
    OnGetClass = dsscKMO_PORT_PaymentGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 424
  end
  object dsscKMO_PORT_Status: TDSServerClass
    OnGetClass = dsscKMO_PORT_StatusGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 368
  end
  object dsscKMO_Portefeuille: TDSServerClass
    OnGetClass = dsscKMO_PortefeuilleGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 424
  end
  object dsscOrganisation: TDSServerClass
    OnGetClass = dsscOrganisationGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 368
  end
  object dsscOrganisationType: TDSServerClass
    OnGetClass = dsscOrganisationTypeGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 424
  end
  object dsscPerson: TDSServerClass
    OnGetClass = dsscPersonGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 368
  end
  object dsscRole: TDSServerClass
    OnGetClass = dsscRoleGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 424
  end
  object dsscProgDiscipline: TDSServerClass
    OnGetClass = dsscProgDisciplineGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 480
  end
  object dsscRoleGroup: TDSServerClass
    OnGetClass = dsscRoleGroupGetClass
    Server = DSServerEduTec
    Left = 344
    Top = 480
  end
  object dsscSesFact: TDSServerClass
    OnGetClass = dsscSesFactGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 480
  end
  object dsscConfirmation_Reports: TDSServerClass
    OnGetClass = dsscConfirmation_ReportsGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 480
  end
  object dsscConfirmReports: TDSServerClass
    OnGetClass = dsscConfirmReportsGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 480
  end
  object dsscEnrolment: TDSServerClass
    OnGetClass = dsscEnrolmentGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 480
  end
  object dsscSesExpenses: TDSServerClass
    OnGetClass = dsscSesExpensesGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 536
  end
  object dsscSesInstructorRoom: TDSServerClass
    OnGetClass = dsscSesInstructorRoomGetClass
    Server = DSServerEduTec
    Left = 344
    Top = 536
  end
  object dsscSesSession: TDSServerClass
    OnGetClass = dsscSesSessionGetClass
    Server = DSServerEduTec
    Left = 520
    Top = 536
  end
  object dsscSessionGroup: TDSServerClass
    OnGetClass = dsscSessionGroupGetClass
    Server = DSServerEduTec
    Left = 680
    Top = 536
  end
  object dsscSesWizard: TDSServerClass
    OnGetClass = dsscSesWizardGetClass
    Server = DSServerEduTec
    Left = 800
    Top = 536
  end
  object dsscProgramType: TDSServerClass
    OnGetClass = dsscProgramTypeGetClass
    Server = DSServerEduTec
    Left = 896
    Top = 536
  end
  object dsscCenter: TDSServerClass
    OnGetClass = dsscCenterGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 592
  end
  object dsscCountry: TDSServerClass
    OnGetClass = dsscCountryGetClass
    Server = DSServerEduTec
    Left = 200
    Top = 96
  end
end
