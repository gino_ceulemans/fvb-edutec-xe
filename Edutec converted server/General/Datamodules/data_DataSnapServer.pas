unit data_DataSnapServer;

interface

uses
  System.SysUtils, System.Classes, IPPeerServer, Datasnap.DSCommonServer,
  Datasnap.DSTCPServerTransport, Datasnap.DSServer;

type
  TdtmDataSnapServer = class(TDataModule)
    DSServerEduTec: TDSServer;
    DSTCPServerTransport215: TDSTCPServerTransport;
    dsscLanguage: TDSServerClass;
    dsscProgProgram: TDSServerClass;
    dsscCountryPart: TDSServerClass;
    dsscProvince: TDSServerClass;
    dsscPostalCode: TDSServerClass;
    dsscCooperation: TDSServerClass;
    dfsscGender: TDSServerClass;
    dsscMedium: TDSServerClass;
    dsscSchoolYear: TDSServerClass;
    dsscStatus: TDSServerClass;
    dsscTitle: TDSServerClass;
    dsscInstructor: TDSServerClass;
    dsscProgInstructor: TDSServerClass;
    dsscLogHistory: TDSServerClass;
    dsscInvoicing: TDSServerClass;
    dsscSessionWizard: TDSServerClass;
    dsscDiploma: TDSServerClass;
    dsscCenInfrastructure: TDSServerClass;
    dsscCenRoom: TDSServerClass;
    dsscParameter: TDSServerClass;
    dsscParameterType: TDSServerClass;
    dsscQuery: TDSServerClass;
    dsscQueryGroup: TDSServerClass;
    dsscQueryWizard: TDSServerClass;
    dsscReport: TDSServerClass;
    dsscTablefield: TDSServerClass;
    dsscProfile: TDSServerClass;
    dsscUser: TDSServerClass;
    dsscUserProfile: TDSServerClass;
    dsscImport: TDSServerClass;
    dsscImportOverview: TDSServerClass;
    dsscImport_Excel: TDSServerClass;
    dsscKMO_PORT_Link_Port_Payment: TDSServerClass;
    dsscKMO_PORT_Link_Port_Session: TDSServerClass;
    dsscKMO_PORT_Payment: TDSServerClass;
    dsscKMO_PORT_Status: TDSServerClass;
    dsscKMO_Portefeuille: TDSServerClass;
    dsscOrganisation: TDSServerClass;
    dsscOrganisationType: TDSServerClass;
    dsscPerson: TDSServerClass;
    dsscRole: TDSServerClass;
    dsscProgDiscipline: TDSServerClass;
    dsscRoleGroup: TDSServerClass;
    dsscSesFact: TDSServerClass;
    dsscConfirmation_Reports: TDSServerClass;
    dsscConfirmReports: TDSServerClass;
    dsscEnrolment: TDSServerClass;
    dsscSesExpenses: TDSServerClass;
    dsscSesInstructorRoom: TDSServerClass;
    dsscSesSession: TDSServerClass;
    dsscSessionGroup: TDSServerClass;
    dsscSesWizard: TDSServerClass;
    dsscProgramType: TDSServerClass;
    dsscCenter: TDSServerClass;
    dsscCountry: TDSServerClass;
    procedure dsscCountryGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure DSServerEduTecError(DSErrorEventObject: TDSErrorEventObject);
    procedure dsscLanguageGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscProgProgramGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscCountryPartGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscProvinceGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscPostalCodeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscCooperationGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dfsscGenderGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscMediumGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSchoolYearGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscStatusGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscTitleGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscInstructorGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscProgInstructorGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscLogHistoryGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscInvoicingGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSessionWizardGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscDiplomaGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscCenInfrastructureGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscCenRoomGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscParameterGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscParameterTypeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscQueryGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscQueryGroupGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscQueryWizardGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscReportGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscTablefieldGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscProfileGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscUserGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscUserProfileGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscImportGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscImportOverviewGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscImport_ExcelGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscKMO_PORT_Link_Port_PaymentGetClass(
      DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure dsscKMO_PORT_Link_Port_SessionGetClass(
      DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure dsscKMO_PORT_PaymentGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscKMO_PORT_StatusGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscKMO_PortefeuilleGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscOrganisationGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscOrganisationTypeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscPersonGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscRoleGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscProgDisciplineGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscRoleGroupGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSesFactGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscConfirmation_ReportsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscConfirmReportsGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscEnrolmentGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSesExpensesGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSesInstructorRoomGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSesSessionGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSessionGroupGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscSesWizardGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscProgramTypeGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
    procedure dsscCenterGetClass(DSServerClass: TDSServerClass;
      var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dtmDataSnapServer: TdtmDataSnapServer;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses Remote_EduMain, Remote_Country, Remote_CountryPart, Remote_Language, Remote_Province,
     Remote_PostalCode, Remote_Cooperation, Remote_Gender, Remote_Medium, Remote_SchoolYear,
     Remote_ProgProgram, Remote_Status, Remote_Title, Remote_Instructor, Remote_ProgInstructor,
     Remote_LogHistory, Remote_Invoicing, Remote_SessionWizard, Remote_Diploma,
     Remote_CenInfrastructure, Remote_CenRoom, Remote_Parameter, Remote_ParameterType,
     Remote_Query, Remote_QueryGroup, Remote_QueryWizard, Remote_Report, Remote_Tablefield,
     Remote_Profile, Remote_User, Remote_UserProfile, remote_Import, remote_ImportOverview,
     Remote_Import_Excel, Remote_KMO_PORT_Link_Port_Payment, Remote_KMO_PORT_Link_Port_Session,
     Remote_KMO_PORT_Payment, Remote_KMO_PORT_Status, Remote_KMO_Portefeuille,
     Remote_Organisation, Remote_OrganisationType, Remote_Person, Remote_Role,
     Remote_ProgDiscipline, Remote_RoleGroup, Remote_SesFact, Remote_SesWizard,
     Remote_Confirmation_Reports, Remote_ConfirmReports, Remote_Enrolment, Remote_ProgramType,
     Remote_SesExpenses, Remote_SesInstructorRoom, Remote_SesSession, Remote_SessionGroup,
     Remote_Center,
     Form_EduMainServer, Windows;

{$R *.dfm}

procedure TdtmDataSnapServer.dsscCountryGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Country.TrdtmCountry;
end;

procedure TdtmDataSnapServer.dsscCountryPartGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_CountryPart.TrdtmCountryPart;
end;

procedure TdtmDataSnapServer.dsscDiplomaGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Diploma.TrdtmDiploma;
end;

procedure TdtmDataSnapServer.dsscEnrolmentGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Enrolment.TrdtmEnrolment;
end;

procedure TdtmDataSnapServer.dsscImportGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := remote_Import.TrdtmImport;
end;

procedure TdtmDataSnapServer.dsscImportOverviewGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := remote_ImportOverview.TrdtmImportOverview;
end;

procedure TdtmDataSnapServer.dsscImport_ExcelGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Import_Excel.TrdtmImport_Excel;
end;

procedure TdtmDataSnapServer.dsscInstructorGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Instructor.TrdtmInstructor;
end;

procedure TdtmDataSnapServer.dsscInvoicingGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Invoicing.TrdtmInvoicing;
end;

procedure TdtmDataSnapServer.dsscKMO_PortefeuilleGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_KMO_Portefeuille.TrdtmKMO_Portefeuille;
end;

procedure TdtmDataSnapServer.dsscKMO_PORT_Link_Port_PaymentGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_KMO_PORT_Link_Port_Payment.TrdtmKMO_PORT_Link_Port_Payment;
end;

procedure TdtmDataSnapServer.dsscKMO_PORT_Link_Port_SessionGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_KMO_PORT_Link_Port_Session.TrdtmKMO_PORT_Link_Port_Session;
end;

procedure TdtmDataSnapServer.dsscKMO_PORT_PaymentGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_KMO_PORT_Payment.TrdtmKMO_PORT_Payment;
end;

procedure TdtmDataSnapServer.dsscKMO_PORT_StatusGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_KMO_PORT_Status.TrdtmKMO_PORT_Status;
end;

procedure TdtmDataSnapServer.dsscLanguageGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Language.TrdtmLanguage;
end;

procedure TdtmDataSnapServer.dsscLogHistoryGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_LogHistory.TrdtmLogHistory;
end;

procedure TdtmDataSnapServer.dsscMediumGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Medium.TrdtmMedium;
end;

procedure TdtmDataSnapServer.dsscOrganisationGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Organisation.TrdtmOrganisation;
end;

procedure TdtmDataSnapServer.dsscOrganisationTypeGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_OrganisationType.TrdtmOrganisationType;
end;

procedure TdtmDataSnapServer.dsscParameterGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Parameter.TrdtmParameter;
end;

procedure TdtmDataSnapServer.dsscParameterTypeGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_ParameterType.TrdtmParameterType;
end;

procedure TdtmDataSnapServer.dsscPersonGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Person.TrdtmPerson;
end;

procedure TdtmDataSnapServer.dsscPostalCodeGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_PostalCode.TrdtmPostalCode;
end;

procedure TdtmDataSnapServer.dsscProfileGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Profile.TrdtmProfile;
end;

procedure TdtmDataSnapServer.dsscProgDisciplineGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_ProgDiscipline.TrdtmProgDiscipline;
end;

procedure TdtmDataSnapServer.dsscProgInstructorGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_ProgInstructor.TrdtmProgInstructor;
end;

procedure TdtmDataSnapServer.dsscProgProgramGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_ProgProgram.TrdtmProgProgram;
end;

procedure TdtmDataSnapServer.dsscProgramTypeGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_ProgramType.TrdtmProgramType;
end;

procedure TdtmDataSnapServer.dsscProvinceGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Province.TrdtmProvince;
end;

procedure TdtmDataSnapServer.dsscQueryGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Query.TrdtmQuery;
end;

procedure TdtmDataSnapServer.dsscQueryGroupGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_QueryGroup.TrdtmQueryGroup;
end;

procedure TdtmDataSnapServer.dsscQueryWizardGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_QueryWizard.TrdtmQueryWizard;
end;

procedure TdtmDataSnapServer.dsscReportGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Report.TrdtmReport;
end;

procedure TdtmDataSnapServer.dsscRoleGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Role.TrdtmRole;
end;

procedure TdtmDataSnapServer.dsscRoleGroupGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_RoleGroup.TrdtmRoleGroup;
end;

procedure TdtmDataSnapServer.dsscSchoolYearGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SchoolYear.TrdtmSchoolYear;
end;

procedure TdtmDataSnapServer.dsscSesExpensesGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SesExpenses.TdtmRdtmSesExpenses;
end;

procedure TdtmDataSnapServer.dsscSesFactGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SesFact.TrdtmSesFact;
end;

procedure TdtmDataSnapServer.dsscSesInstructorRoomGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SesInstructorRoom.TrdtmSesInstructorRoom;
end;

procedure TdtmDataSnapServer.dsscSesSessionGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SesSession.TrdtmSesSession;
end;

procedure TdtmDataSnapServer.dsscSessionGroupGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SessionGroup.TrdtmSessionGroup;
end;

procedure TdtmDataSnapServer.dsscSessionWizardGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SessionWizard.Trdtm_SessionWizard;
end;

procedure TdtmDataSnapServer.dsscSesWizardGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_SesWizard.TrdtmSesWizard;
end;

procedure TdtmDataSnapServer.dsscStatusGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Status.TrdtmStatus;
end;

procedure TdtmDataSnapServer.dsscTablefieldGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Tablefield.TrdtmTableField;
end;

procedure TdtmDataSnapServer.dsscTitleGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Title.TrdtmTitle;
end;

procedure TdtmDataSnapServer.dsscUserGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_User.TrdtmUser;
end;

procedure TdtmDataSnapServer.dsscUserProfileGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_UserProfile.TrdtmUserProfile;
end;

procedure TdtmDataSnapServer.dfsscGenderGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Gender.TrdtmGender;
end;

procedure TdtmDataSnapServer.dsscCenInfrastructureGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_CenInfrastructure.TrdtmCenInfrastructure;
end;

procedure TdtmDataSnapServer.dsscCenRoomGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_CenRoom.TrdtmCenRoom;
end;

procedure TdtmDataSnapServer.dsscCenterGetClass(DSServerClass: TDSServerClass;
  var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Center.TrdtmDocCenter;
end;

procedure TdtmDataSnapServer.dsscConfirmation_ReportsGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Confirmation_Reports.TrdtmConfirmationReports;
end;

procedure TdtmDataSnapServer.dsscConfirmReportsGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_ConfirmReports.TrdtmConfirmReports;
end;

procedure TdtmDataSnapServer.dsscCooperationGetClass(
  DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := Remote_Cooperation.TrdtmCooperation;
end;

procedure TdtmDataSnapServer.DSServerEduTecError(
  DSErrorEventObject: TDSErrorEventObject);
var
  ConnectionInfo: PConnectionInfo;
begin
  //send message to mainform
  new (ConnectionInfo);
  ConnectionInfo^.ConnectionString := 'TDataSnapServerData.DataSnapServerError';
  PostMessage(frmEduMainServer.Handle, CM_SetConnectionInfo, Integer(ConnectionInfo), 0);

//  frmEduMainServer.SetConnectionInfo(Format('DSErrorEventObject = %s-%s', [DSErrorEventObject.Error.ClassName, DSErrorEventObject.Error.Message]));
end;

end.
