inherited rdtmSchoolYear: TrdtmSchoolYear
  OldCreateOrder = True
  Height = 171
  Width = 383
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_SCHOOLYEAR_ID, '
      '  F_SCHOOLYEAR, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM         '
      '  V_GE_SCHOOLYEAR'
      '')
    object adoqryListF_SCHOOLYEAR_ID: TIntegerField
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_SCHOOLYEAR: TStringField
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_SCHOOLYEAR_ID, '
      '  F_SCHOOLYEAR, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM         '
      '  V_GE_SCHOOLYEAR'
      '')
    object adoqryRecordF_SCHOOLYEAR_ID: TIntegerField
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_SCHOOLYEAR: TStringField
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_GE_SCHOOLYEAR: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_GE_SCHOOLYEAR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 248
    Top = 24
  end
end
