{*****************************************************************************
  This unit will contain the Main form for the Edutec Application Server.
  
  @Name       Form_EduMainServer
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  06/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_EduMainServer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

const
  CM_SetConnectionInfo = CM_BASE + 205;

type
  TConnectionInfo = record
    ConnectionString: string;
  end;

  PConnectionInfo = ^TConnectionInfo;

  TfrmEduMainServer = class(TForm)
    mmConnectionString: TMemo;
    Label1: TLabel;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure SetConnectionInfo (var Msg: TMessage); message CM_SetConnectionInfo;
  end;

var
  frmEduMainServer: TfrmEduMainServer;

implementation

{$R *.dfm}

{ TfrmEduMainServer }

{*****************************************************************************
  Name           : TfrmMain.SetConnectionInfo
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Shows the connectionstring in the mainform
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/09/2006   Wim Lambrechts       Copied from convenanten
 *****************************************************************************}
procedure TfrmEduMainServer.SetConnectionInfo(var Msg: TMessage);
begin
  try
    mmConnectionString.Text := PConnectionInfo(Msg.WParam)^.ConnectionString;
  finally
    Dispose (PConnectionInfo(Msg.WParam));
  end;
end;

procedure TfrmEduMainServer.FormActivate(Sender: TObject);
begin
  self.Left := Screen.WorkAreaWidth - self.Width;
  self.Top  := Screen.WorkAreaHeight - self.Height;
end;

end.

