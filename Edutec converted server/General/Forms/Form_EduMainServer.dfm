object frmEduMainServer: TfrmEduMainServer
  Left = 590
  Top = 453
  Width = 267
  Height = 125
  Caption = 'Edutec server'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  DesignSize = (
    259
    98)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 100
    Height = 13
    Caption = 'Connectionstring:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object mmConnectionString: TMemo
    Left = 8
    Top = 29
    Width = 243
    Height = 60
    TabStop = False
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clActiveBorder
    ReadOnly = True
    TabOrder = 0
  end
end
