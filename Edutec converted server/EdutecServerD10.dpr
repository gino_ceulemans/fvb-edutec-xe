program EdutecServerD10;

uses
  Forms,
  Form_EduMainServer in 'General\Forms\Form_EduMainServer.pas' {frmEduMainServer},
  Remote_EduMain in 'General\Datamodules\Remote_EduMain.pas' {rdtmEDUMain: TRemoteDataModule} {rdtmEDUMain: CoClass},
  data_ServerMethods in 'General\Datamodules\data_ServerMethods.pas' {ServerMethodsModule: TDSServerModule},
  data_DataSnapServer in 'General\Datamodules\data_DataSnapServer.pas' {dtmDataSnapServer: TDataModule} {rdtmImportOverview: CoClass} {rdtmDocCenter: CoClass} {rdtmSesFact: CoClass} {rdtmInvoicing: CoClass},
  Remote_EduBase in 'Templates\Datamodules\Remote_EduBase.pas' {rdtmEduBase: TRemoteDataModule} {rdtmEduBase: CoClass},
  Data_ReconcileError in 'General\Datamodules\Data_ReconcileError.pas' {dtmReconcileError: TDataModule} {rdtmImportOverview: CoClass} {rdtmDocCenter: CoClass} {rdtmSesFact: CoClass} {rdtmInvoicing: CoClass},
  Remote_Country in 'General\Datamodules\Remote_Country.pas' {rdtmCountry: TRemoteDataModule} {rdtmCountry: CoClass},
  remote_ImportOverview in 'Import\Datamodules\remote_ImportOverview.pas' {rdtmImportOverview: TRemoteDataModule} {rdtmImportOverview: CoClass},
  Remote_Center in 'Instructor\Remote_Center.pas' {rdtmDocCenter: TRemoteDataModule} {rdtmDocCenter: CoClass},
  Remote_SesFact in 'SesFact\Datamodules\Remote_SesFact.pas' {rdtmSesFact: TRemoteDataModule} {rdtmSesFact: CoClass},
  Remote_Invoicing in 'Invoicing\Remote_Invoicing.pas' {rdtmInvoicing: TRemoteDataModule} {rdtmInvoicing: CoClass},
  remote_Import in 'Import\Datamodules\remote_Import.pas' {rdtmImport: TRemoteDataModule},
  Remote_Confirmation_Reports in 'Sessions\Datamodules\Remote_Confirmation_Reports.pas' {rdtmConfirmationReports: TRemoteDataModule},
  Remote_SessionWizard in 'SessionWizard\Remote_SessionWizard.pas' {rdtm_SessionWizard: TRemoteDataModule},
  Remote_Cooperation in 'General\Datamodules\Remote_Cooperation.pas',
  Remote_KMO_PORT_Status in 'KMO_Portefeuille\Datamodules\Remote_KMO_PORT_Status.pas' {rdtmKMO_PORT_Status: TRemoteDataModule},
  Remote_KMO_Portefeuille in 'KMO_Portefeuille\Datamodules\Remote_KMO_Portefeuille.pas' {rdtmKMO_Portefeuille: TRemoteDataModule},
  Remote_KMO_PORT_Payment in 'KMO_Portefeuille\Datamodules\Remote_KMO_PORT_Payment.pas' {rdtmKMO_PORT_Payment: TRemoteDataModule},
  Remote_KMO_PORT_Link_Port_Payment in 'KMO_Portefeuille\Datamodules\Remote_KMO_PORT_Link_Port_Payment.pas' {rdtmKMO_PORT_Link_Port_Payment: TRemoteDataModule},
  Remote_KMO_PORT_Link_Port_Session in 'KMO_Portefeuille\Datamodules\Remote_KMO_PORT_Link_Port_Session.pas' {rdtmKMO_PORT_Link_Port_Session: TRemoteDataModule},
  Remote_Import_Excel in 'Import_Excel\Datamodules\Remote_Import_Excel.pas' {rdtmImport_Excel: TRemoteDataModule},
  Remote_Person in 'Person\DataModules\Remote_Person.pas' {rdtmPerson: TRemoteDataModule};

//  {$R *.TLB}

{$R *.TLB}

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmEduMainServer, frmEduMainServer);
  Application.CreateForm(TrdtmEduMain, rdtmEduMain);
  Application.CreateForm(TdtmDataSnapServer, dtmDataSnapServer);
  Application.Run;
end.

// cheuten : 2.6.0.0 : SQL Server 2005 versie ( verder geen wijzigingen t.o.v 2.5.3.0 )
