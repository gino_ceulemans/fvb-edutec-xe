{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_RO_ROLE.

  @Name       Remote_Role
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Role;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmRole = class(TrdtmEduBase, IrdtmRole)
    adoqryListF_ROLE_ID: TIntegerField;
    adoqryListF_PERSON_ID: TIntegerField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_RSZ_NR: TStringField;
    adoqryListF_ROLEGROUP_ID: TIntegerField;
    adoqryListF_ROLEGROUP_NAME: TStringField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_PRINCIPAL: TBooleanField;
    adoqryListF_SCHOOLYEAR_ID: TIntegerField;
    adoqryListF_SCHOOLYEAR: TStringField;
    adoqryRecordF_ROLE_ID: TIntegerField;
    adoqryRecordF_PERSON_ID: TIntegerField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_RSZ_NR: TStringField;
    adoqryRecordF_ROLEGROUP_ID: TIntegerField;
    adoqryRecordF_ROLEGROUP_NAME: TStringField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_PRINCIPAL: TBooleanField;
    adoqryRecordF_SCHOOLYEAR_ID: TIntegerField;
    adoqryRecordF_SCHOOLYEAR: TStringField;
    adospSP_DEACTIVATE_RO_ROLE: TFVBFFCStoredProc;
    adoqryListF_REMARK: TStringField;
    adoqryRecordF_REMARK: TStringField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_GSM: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_GSM: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofRole : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmRole.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmRole.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmRole.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmRole.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmRole.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmRole.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmRole.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmRole.GetTableName: String;
begin
  Result := 'T_RO_ROLE';
end;

procedure TrdtmRole.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_RO_ROLE(' + DeltaDS.FieldByName('F_ROLE_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_RO_ROLE.Close;
    adospSP_DEACTIVATE_RO_ROLE.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_ROLE_ID').Value;
    adospSP_DEACTIVATE_RO_ROLE.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmRole.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofRole := TComponentFactory.Create(ComServer, TrdtmRole,
    Class_rdtmRole, ciInternal, tmApartment);
end.
