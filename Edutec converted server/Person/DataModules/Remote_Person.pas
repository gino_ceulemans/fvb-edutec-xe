{*****************************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  <T_PE_PERSON>.

  @Name       Remote_Person
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/04/2008   ivdbossche           Added P_SYS_JOIN_PERSON2 and JoinPersons (Mantis 2073)
  29/09/2005   slesage              Initial creation of the Unit.
******************************************************************************************}

unit Remote_Person;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmPerson = class(TrdtmEduBase, IrdtmPerson)
    adoqryListF_PERSON_ID: TIntegerField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_POSTALCODE_ID: TIntegerField;
    adoqryListF_POSTALCODE: TStringField;
    adoqryListF_CITY_NAME: TStringField;
    adoqryListF_COUNTRY_ID: TIntegerField;
    adoqryListF_COUNTRY_NAME: TStringField;
    adoqryListF_LANGUAGE_ID: TIntegerField;
    adoqryListF_LANGUAGE_NAME: TStringField;
    adoqryListF_MEDIUM_ID: TIntegerField;
    adoqryListF_MEDIUM_NAME: TStringField;
    adoqryRecordF_PERSON_ID: TIntegerField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_STREET: TStringField;
    adoqryRecordF_NUMBER: TStringField;
    adoqryRecordF_MAILBOX: TStringField;
    adoqryRecordF_POSTALCODE_ID: TIntegerField;
    adoqryRecordF_POSTALCODE: TStringField;
    adoqryRecordF_CITY_NAME: TStringField;
    adoqryRecordF_COUNTRY_ID: TIntegerField;
    adoqryRecordF_COUNTRY_NAME: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_GSM: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_DATEBIRTH: TDateTimeField;
    adoqryRecordF_GENDER_ID: TIntegerField;
    adoqryRecordF_GENDER_NAME: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_TITLE_ID: TIntegerField;
    adoqryRecordF_TITLE_NAME: TStringField;
    adoqryRecordF_MEDIUM_ID: TIntegerField;
    adoqryRecordF_MEDIUM_NAME: TStringField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_ACC_NR: TStringField;
    adospSP_DEACTIVATE_PE_PERSON: TFVBFFCStoredProc;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryRecordF_MANUALLY_ADDED: TBooleanField;
    adoqryRecordF_CONSTRUCT_ID: TIntegerField;
    adoqryListF_MANUALLY_ADDED: TBooleanField;
    adoqryListF_CONSTRUCT_ID: TIntegerField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_GSM: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryListF_ROLE: TStringField;
    adoqryRecordF_DIPLOMA_ID: TIntegerField;
    adoqryRecordF_DIPLOMA_NAME: TStringField;
    P_SYS_RETRIEVE_DOUBLE_PERSONS_IN_SAME_COMPANY: TFVBFFCStoredProc;
    prvP_SYS_RETRIEVE_DOUBLE_PERSONS: TFVBFFCDataSetProvider;
    P_SYS_JOIN_PERSON2: TFVBFFCStoredProc;
    adoqryRecordF_SYNERGY_ID: TLargeintField;
    adoqryListF_SYNERGY_ID: TLargeintField;
    adoqryListF_PLACE_OF_BIRTH: TStringField;
    adoqryRecordF_PLACE_OF_BIRTH: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    procedure JoinPersons(const APersonList: WideString;
      ACopyToPersonID: Integer); safecall;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofPerson : TComponentFactory;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmPerson.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmPerson.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmPerson.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmPerson.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmPerson.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmPerson.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmPerson.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmPerson.GetTableName: String;
begin
  Result := 'T_PE_PERSON';
end;

procedure TrdtmPerson.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_PE_PERSON(' + DeltaDS.FieldByName('F_PERSON_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_PE_PERSON.Close;
    adospSP_DEACTIVATE_PE_PERSON.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_PERSON_ID').Value;
    adospSP_DEACTIVATE_PE_PERSON.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmPerson.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

{************************************************************************************
  @Name       TrdtmPerson.JoinPersons
  @author     Ivan Van den Bossche (25/04/2008)
  @param      APersonList --> PersonId(s) which must be converted
              ACopyToPersonId --> PersonId to which APersonList must be converted to
  @return     None
  @Exception  None
  @See        None
  @Descr      Procedure converts different persons to 1 person. (Mantis 2073).

**************************************************************************************}
procedure TrdtmPerson.JoinPersons(const APersonList: WideString;
  ACopyToPersonID: Integer);
begin
  P_SYS_JOIN_PERSON2.Close;
  P_SYS_JOIN_PERSON2.Parameters.ParamByName('@PersonList').Value := APersonList;
  P_SYS_JOIN_PERSON2.Parameters.ParamByName('@CopyToPersonID').Value := ACopyToPersonId;
  P_SYS_JOIN_PERSON2.ExecProc;

end;

initialization
  ofPerson := TComponentFactory.Create(ComServer, TrdtmPerson,
    Class_rdtmPerson, ciInternal, tmApartment);
end.
