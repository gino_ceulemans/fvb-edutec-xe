inherited rdtmRole: TrdtmRole
  OldCreateOrder = True
  Height = 401
  Width = 610
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT top 1000'
      '  F_ROLE_ID, '
      '  F_PERSON_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_SOCSEC_NR, '
      '  F_ORGANISATION_ID, '
      '  F_NAME, '
      '  F_RSZ_NR, '
      '  F_ROLEGROUP_ID, '
      '  F_ROLEGROUP_NAME,'
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE, '
      '  F_PRINCIPAL, '
      '  F_SCHOOLYEAR_ID,'
      '  F_SCHOOLYEAR,'
      '  F_REMARK,'
      '  F_PHONE,'
      '  F_FAX,'
      '  F_GSM,'
      '  F_EMAIL'
      'FROM '
      '  V_RO_ROLE'
      'where'
      '  F_ACTIVE=1')
    Top = 16
    object adoqryListF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
    object adoqryListF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = []
    end
    object adoqryListF_PRINCIPAL: TBooleanField
      FieldName = 'F_PRINCIPAL'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SCHOOLYEAR_ID: TIntegerField
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SCHOOLYEAR: TStringField
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = []
      Size = 50
    end
    object adoqryListF_REMARK: TStringField
      FieldName = 'F_REMARK'
      Size = 1024
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object adoqryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROLE_ID, '
      '  F_PERSON_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_SOCSEC_NR, '
      '  F_ORGANISATION_ID, '
      '  F_NAME, '
      '  F_RSZ_NR, '
      '  F_ROLEGROUP_ID, '
      '  F_ROLEGROUP_NAME,'
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE, '
      '  F_PRINCIPAL, '
      '  F_SCHOOLYEAR_ID,'
      '  F_SCHOOLYEAR,'
      '  F_REMARK,'
      '  F_PHONE,'
      '  F_FAX,'
      '  F_GSM,'
      '  F_EMAIL'
      'FROM '
      '  V_RO_ROLE')
    Top = 8
    object adoqryRecordF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
    object adoqryRecordF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object adoqryRecordF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = []
    end
    object adoqryRecordF_PRINCIPAL: TBooleanField
      FieldName = 'F_PRINCIPAL'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SCHOOLYEAR_ID: TIntegerField
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SCHOOLYEAR: TStringField
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = []
      Size = 50
    end
    object adoqryRecordF_REMARK: TStringField
      FieldName = 'F_REMARK'
      Size = 1024
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object adoqryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_RO_ROLE: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_RO_ROLE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 248
    Top = 24
  end
end
