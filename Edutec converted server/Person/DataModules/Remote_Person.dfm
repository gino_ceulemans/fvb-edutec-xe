inherited rdtmPerson: TrdtmPerson
  OldCreateOrder = True
  Height = 303
  Width = 485
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_PERSON_ID,'
      '  F_SOCSEC_NR,'
      '  F_LASTNAME,'
      '  F_FIRSTNAME, '
      '  F_POSTALCODE_ID,'
      '  F_POSTALCODE,'
      '  F_CITY_NAME, '
      '  F_COUNTRY_ID,'
      '  F_COUNTRY_NAME,'
      '  F_LANGUAGE_ID,'
      '  F_LANGUAGE_NAME,'
      '  F_MEDIUM_ID, '
      '  F_MEDIUM_NAME,'
      '  F_ACTIVE,'
      '  F_MANUALLY_ADDED,'
      '  F_CONSTRUCT_ID,'
      '  F_PHONE,'
      '  F_FAX,'
      '  F_GSM,'
      '  F_EMAIL,'
      '  F_ROLE,'
      '  F_SYNERGY_ID,'
      '  F_PLACE_OF_BIRTH'
      'FROM'
      '  V_PE_PERSON_SCREEN'
      '')
    object adoqryListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object adoqryListF_MANUALLY_ADDED: TBooleanField
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
    end
    object adoqryListF_GSM: TStringField
      FieldName = 'F_GSM'
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object adoqryListF_ROLE: TStringField
      FieldName = 'F_ROLE'
      ProviderFlags = []
      ReadOnly = True
      Size = 131
    end
    object adoqryListF_SYNERGY_ID: TLargeintField
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PERSON_ID, '
      '  F_SOCSEC_NR, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_DATEBIRTH, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_TITLE_ID, '
      '  F_TITLE_NAME, '
      '  F_MEDIUM_ID, '
      '  F_MEDIUM_NAME, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_ACC_NR,'
      '  F_MANUALLY_ADDED,'
      '  F_CONSTRUCT_ID,'
      '  F_PHONE,'
      '  F_FAX,'
      '  F_GSM,'
      '  F_EMAIL,'
      '  F_DIPLOMA_ID,'
      '  F_DIPLOMA_NAME,'
      '  F_SYNERGY_ID,'
      '  F_PLACE_OF_BIRTH'
      'FROM '
      '  V_PE_PERSON_SCREEN'
      '')
    object adoqryRecordF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryRecordF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object adoqryRecordF_MANUALLY_ADDED: TBooleanField
      DisplayLabel = 'Manueel toegevoegd'
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CONSTRUCT_ID: TIntegerField
      DisplayLabel = 'Construct (ID)'
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SYNERGY_ID: TLargeintField
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_PE_PERSON: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_PE_PERSON;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 272
    Top = 40
  end
  object P_SYS_RETRIEVE_DOUBLE_PERSONS_IN_SAME_COMPANY: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_SYS_RETRIEVE_DOUBLE_PERSONS_IN_SAME_COMPANY;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@APersonId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 256
    Top = 152
  end
  object prvP_SYS_RETRIEVE_DOUBLE_PERSONS: TFVBFFCDataSetProvider
    DataSet = P_SYS_RETRIEVE_DOUBLE_PERSONS_IN_SAME_COMPANY
    BeforeUpdateRecord = prvBeforeUpdateRecord
    OnGetTableName = prvListGetTableName
    Left = 256
    Top = 104
  end
  object P_SYS_JOIN_PERSON2: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_SYS_JOIN_PERSON2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@PersonList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8000
        Value = Null
      end
      item
        Name = '@CopyToPersonID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 256
    Top = 208
  end
end
