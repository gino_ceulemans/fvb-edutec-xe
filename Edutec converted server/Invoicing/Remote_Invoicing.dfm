inherited rdtmInvoicing: TrdtmInvoicing
  OldCreateOrder = True
  Height = 351
  Width = 563
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    CommandTimeout = 300
    SQL.Strings = (
      'SELECT '
      '*'
      'FROM '
      '  V_INV_ORGANISATION'
      'ORDER BY '
      '    F_SESSION_ID'
      '  , F_INVOICE_TYPE')
    Top = 16
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAAM: TStringField
      FieldName = 'F_NAAM'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_STRAAT: TStringField
      FieldName = 'F_STRAAT'
      ProviderFlags = [pfInUpdate]
      Size = 134
    end
    object adoqryListF_POSTCODE: TStringField
      FieldName = 'F_POSTCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_WOONPLAATS: TStringField
      FieldName = 'F_WOONPLAATS'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_FACTUUR_NR: TStringField
      FieldName = 'F_FACTUUR_NR'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object adoqryListF_DATUM: TDateTimeField
      FieldName = 'F_DATUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_KLANT_NR: TStringField
      FieldName = 'F_KLANT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object adoqryListF_VERVALDATUM: TDateTimeField
      FieldName = 'F_VERVALDATUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CODE: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INVOICE_TYPE: TIntegerField
      FieldName = 'F_INVOICE_TYPE'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_INVOICE_AMOUNT: TFloatField
      FieldName = 'F_INVOICE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FILENAME: TStringField
      FieldName = 'F_FILENAME'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryListF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryListF_ANA_SLEUTEL: TStringField
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_REFERENTIE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_REFERENTIE'
      ProviderFlags = []
      ReadOnly = True
      Size = 12
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NUMBER_PARTICIPANTS: TIntegerField
      FieldName = 'F_NUMBER_PARTICIPANTS'
      ProviderFlags = []
    end
    object adoqryListF_AANTAL_UREN: TIntegerField
      FieldName = 'F_AANTAL_UREN'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_PRIJS_PER_UUR: TIntegerField
      FieldName = 'F_PRIJS_PER_UUR'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_TOTAAL: TFloatField
      FieldName = 'F_TOTAAL'
    end
    object adoqryListF_REKENINGNR: TStringField
      FieldName = 'F_REKENINGNR'
      ReadOnly = True
      Size = 6
    end
    object adoqryListF_TAALCODE: TStringField
      FieldName = 'F_TAALCODE'
      ReadOnly = True
      Size = 1
    end
    object adoqryListF_LANDCODE: TStringField
      FieldName = 'F_LANDCODE'
      ReadOnly = True
      Size = 1
    end
    object adoqryListF_VAT_NR: TStringField
      DisplayLabel = 'BTWnr'
      FieldName = 'F_VAT_NR'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_SYNERGY_ID: TStringField
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    CommandTimeout = 300
    SQL.Strings = (
      'SELECT '
      '  *'
      'FROM '
      '  V_INV_ORGANISATION'
      'ORDER BY '
      '    F_SESSION_ID'
      '  , F_INVOICE_TYPE'
      '')
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAAM: TStringField
      FieldName = 'F_NAAM'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_STRAAT: TStringField
      FieldName = 'F_STRAAT'
      ProviderFlags = [pfInUpdate]
      Size = 134
    end
    object adoqryRecordF_POSTCODE: TStringField
      FieldName = 'F_POSTCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRecordF_WOONPLAATS: TStringField
      FieldName = 'F_WOONPLAATS'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_FACTUUR_NR: TStringField
      FieldName = 'F_FACTUUR_NR'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object adoqryRecordF_DATUM: TDateTimeField
      FieldName = 'F_DATUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_KLANT_NR: TStringField
      FieldName = 'F_KLANT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object adoqryRecordF_VERVALDATUM: TDateTimeField
      FieldName = 'F_VERVALDATUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INVOICE_TYPE: TIntegerField
      FieldName = 'F_INVOICE_TYPE'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_INVOICE_AMOUNT: TFloatField
      FieldName = 'F_INVOICE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FILENAME: TStringField
      FieldName = 'F_FILENAME'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryRecordF_REFERENTIE: TStringField
      FieldName = 'F_REFERENTIE'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
      Size = 1
    end
    object adoqryRecordF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryRecordF_ANA_SLEUTEL: TStringField
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
  end
  object prvInvoicing: TFVBFFCDataSetProvider
    DataSet = spInvoicing
    OnGetTableName = prvListGetTableName
    Left = 232
    Top = 72
  end
  object spInvoicing: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    CommandTimeout = 300
    ProcedureName = 'SP_INVOICING;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 232
    Top = 24
  end
  object prvMarkInvoiced: TFVBFFCDataSetProvider
    DataSet = spMarkInvoiced
    OnGetTableName = prvListGetTableName
    Left = 312
    Top = 72
  end
  object spMarkInvoiced: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    CommandTimeout = 300
    ProcedureName = 'SP_SET_SESSIONS_INVOICED;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@aFileName'
        Attributes = [paNullable]
        DataType = ftString
        Size = 255
        Value = Null
      end
      item
        Name = '@aStartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@aEndDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@aSessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@aReference'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@NumberOfSessions'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumberOfEnrollments'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@NumberOfInvoices'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 312
    Top = 24
  end
  object adoqryPathInvoices: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      '   F_PATH_INVOICES'
      'FROM'
      '  V_PROG_USER_PATHS'
      '')
    AutoOpen = False
    Left = 40
    Top = 144
    object adoqryPathInvoicesF_PATH_INVOICES: TStringField
      FieldName = 'F_PATH_INVOICES'
      Size = 255
    end
  end
end
