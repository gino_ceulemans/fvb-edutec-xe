unit Remote_Invoicing;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmInvoicing = class(TrdtmEduBase, IrdtmInvoicing)
    adoqryListF_NAAM: TStringField;
    adoqryListF_STRAAT: TStringField;
    adoqryListF_POSTCODE: TStringField;
    adoqryListF_WOONPLAATS: TStringField;
    adoqryListF_FACTUUR_NR: TStringField;
    adoqryListF_DATUM: TDateTimeField;
    adoqryListF_KLANT_NR: TStringField;
    adoqryListF_VERVALDATUM: TDateTimeField;
    adoqryListF_CODE: TStringField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_INVOICE_TYPE: TIntegerField;
    adoqryListF_INVOICE_AMOUNT: TFloatField;
    adoqryListF_FILENAME: TStringField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_NAAM: TStringField;
    adoqryRecordF_STRAAT: TStringField;
    adoqryRecordF_POSTCODE: TStringField;
    adoqryRecordF_WOONPLAATS: TStringField;
    adoqryRecordF_FACTUUR_NR: TStringField;
    adoqryRecordF_DATUM: TDateTimeField;
    adoqryRecordF_KLANT_NR: TStringField;
    adoqryRecordF_VERVALDATUM: TDateTimeField;
    adoqryRecordF_CODE: TStringField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_INVOICE_TYPE: TIntegerField;
    adoqryRecordF_INVOICE_AMOUNT: TFloatField;
    adoqryRecordF_FILENAME: TStringField;
    prvInvoicing: TFVBFFCDataSetProvider;
    spInvoicing: TFVBFFCStoredProc;
    prvMarkInvoiced: TFVBFFCDataSetProvider;
    spMarkInvoiced: TFVBFFCStoredProc;
    adoqryListF_COMMENT: TStringField;
    adoqryListF_ANA_SLEUTEL: TStringField;
    adoqryListF_REFERENTIE: TStringField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryRecordF_REFERENTIE: TStringField;
    adoqryRecordF_COMMENT: TStringField;
    adoqryRecordF_ANA_SLEUTEL: TStringField;
    adoqryListF_NUMBER_PARTICIPANTS: TIntegerField;
    adoqryListF_AANTAL_UREN: TIntegerField;
    adoqryListF_PRIJS_PER_UUR: TIntegerField;
    adoqryPathInvoices: TFVBFFCQuery;
    adoqryPathInvoicesF_PATH_INVOICES: TStringField;
    adoqryListF_TOTAAL: TFloatField;
    adoqryListF_REKENINGNR: TStringField;
    adoqryListF_TAALCODE: TStringField;
    adoqryListF_LANDCODE: TStringField;
    adoqryListF_VAT_NR: TStringField;
    adoqryListF_SYNERGY_ID: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function GetInvoicePathCurUser: WideString; safecall;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofInvoicing : TComponentFactory;

implementation

uses Remote_EduMain;

{$R *.DFM}

class procedure TrdtmInvoicing.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmInvoicing.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmInvoicing.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmInvoicing.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmInvoicing.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmInvoicing.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmInvoicing.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmInvoicing.GetTableName: String;
begin
  Result := 'T_INV_ORGANISATION';
end;

function TrdtmInvoicing.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

function TrdtmInvoicing.GetInvoicePathCurUser: WideString;
var
  path: String;
begin
  adoqryPathInvoices.Close;
  adoqryPathInvoices.Open;

  try
    if not adoqryPathInvoices.FieldByName('F_PATH_INVOICES').IsNull then
      path := adoqryPathInvoices.FieldByName('F_PATH_INVOICES').AsString;
  finally
      adoqryPathInvoices.Close;
  end;

  Result := path;
end;

initialization
  ofInvoicing := TComponentFactory.Create(ComServer, TrdtmInvoicing,
    Class_rdtmInvoicing, ciInternal, tmApartment);
end.


