inherited rdtmOrganisationType: TrdtmOrganisationType
  OldCreateOrder = True
  Height = 181
  Width = 233
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ORGTYPE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ORGTYPE_NAME '
      'FROM '
      '  V_OR_TYPE'
      '')
    object adoqryListF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object adoqryListF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ORGTYPE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ORGTYPE_NAME '
      'FROM '
      '  V_OR_TYPE'
      '')
    object adoqryRecordF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object adoqryRecordF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
end
