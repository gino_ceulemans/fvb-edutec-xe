{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_OR_ORGANISATION.

  @Name       Remote_Organisation
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  29/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Organisation;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmOrganisation = class(TrdtmEduBase, IrdtmOrganisation)
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_RSZ_NR: TStringField;
    adoqryListF_STREET: TStringField;
    adoqryListF_NUMBER: TStringField;
    adoqryListF_MAILBOX: TStringField;
    adoqryListF_POSTALCODE_ID: TIntegerField;
    adoqryListF_POSTALCODE: TStringField;
    adoqryListF_CITY_NAME: TStringField;
    adoqryListF_COUNTRY_ID: TIntegerField;
    adoqryListF_COUNTRY_NAME: TStringField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_ORGTYPE_ID: TIntegerField;
    adoqryListF_ORGTYPE_NAME: TStringField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_RSZ_NR: TStringField;
    adoqryRecordF_STREET: TStringField;
    adoqryRecordF_NUMBER: TStringField;
    adoqryRecordF_MAILBOX: TStringField;
    adoqryRecordF_POSTALCODE_ID: TIntegerField;
    adoqryRecordF_POSTALCODE: TStringField;
    adoqryRecordF_CITY_NAME: TStringField;
    adoqryRecordF_COUNTRY_ID: TIntegerField;
    adoqryRecordF_COUNTRY_NAME: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_WEBSITE: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_ACC_NR: TStringField;
    adoqryRecordF_ACC_HOLDER: TStringField;
    adoqryRecordF_MEDIUM_ID: TIntegerField;
    adoqryRecordF_MEDIUM_NAME: TStringField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_INVOICE_STREET: TStringField;
    adoqryRecordF_INVOICE_NUMBER: TStringField;
    adoqryRecordF_INVOICE_MAILBOX: TStringField;
    adoqryRecordF_INVOICE_POSTALCODE_ID: TIntegerField;
    adoqryRecordF_INVOICE_COUNTRY_ID: TIntegerField;
    adoqryRecordF_ORGTYPE_ID: TIntegerField;
    adoqryRecordF_ORGTYPE_NAME: TStringField;
    adoqryRecordF_INVOICE_POSTALCODE: TStringField;
    adoqryRecordF_INVOICE_CITY_NAME: TStringField;
    adoqryRecordF_INVOICE_COUNTRY_NAME: TStringField;
    adospSP_DEACTIVATE_OR_ORGANISATION: TFVBFFCStoredProc;
    adoqryListF_ORG_NR: TStringField;
    adoqryListF_SCHOOL_NR: TStringField;
    adoqryRecordF_ORG_NR: TStringField;
    adoqryRecordF_SCHOOL_NR: TStringField;
    adoqryListF_MANUALLY_ADDED: TBooleanField;
    adoqryRecordF_MANUALLY_ADDED: TBooleanField;
    adoqryListF_CONSTRUCT_ID: TIntegerField;
    adoqryRecordF_CONSTRUCT_ID: TIntegerField;
    adoqryListF_CONTACT_DATA: TStringField;
    adoqryRecordF_CONTACT_DATA: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT1: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT2: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    stprJoinOrganisations: TADOStoredProc;
    stprJoinPersons: TADOStoredProc;
    adoqryListF_EMAIL: TStringField;
    adoqryRecordF_VAT_NR: TStringField;
    adoqryListF_VAT_NR: TStringField;
    adoqryRecordF_EXTRA_INFO: TStringField;
    adoqryRecordF_INVOICE_EXTRA_INFO: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT3: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField;
    adoqryListF_SYNERGY_ID: TLargeintField;
    adoqryRecordF_SYNERGY_ID: TLargeintField;
    adoqryRecordF_PARTNERWINTER: TBooleanField;
    adoqryRecordF_AUTHORIZATION_NR: TStringField;
    adoqryRecordF_ADMIN_COST: TFloatField;
    adoqryRecordF_ADMIN_COST_TYPE: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function JoinOrganisations(var OrganisationList: WideString;
      CopyToOrganisationID: SYSINT): SYSINT; safecall;
    function JoinPersons(OrganisationID: SYSINT): SYSINT; safecall;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofOrganisation : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmOrganisation.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmOrganisation.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmOrganisation.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmOrganisation.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmOrganisation.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmOrganisation.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmOrganisation.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmOrganisation.GetTableName: String;
begin
  Result := 'T_OR_ORGANISATION';
end;

procedure TrdtmOrganisation.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_OR_ORGANISATION(' + DeltaDS.FieldByName('F_ORGANISATION_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_OR_ORGANISATION.Close;
    adospSP_DEACTIVATE_OR_ORGANISATION.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_ORGANISATION_ID').Value;
    adospSP_DEACTIVATE_OR_ORGANISATION.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmOrganisation.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

function TrdtmOrganisation.JoinOrganisations(
  var OrganisationList: WideString; CopyToOrganisationID: SYSINT): SYSINT;
begin
  stprJoinOrganisations.Parameters.ParamByName('@OrganisationList').Value := OrganisationList;
  stprJoinOrganisations.Parameters.ParamByName('@CopyToOrganisationID').Value := CopyToOrganisationID;
  stprJoinOrganisations.ExecProc;
  result := stprJoinOrganisations.Parameters.ParamByName('@RETURN_VALUE').Value;
end;

function TrdtmOrganisation.JoinPersons(OrganisationID: SYSINT): SYSINT;
begin
  stprJoinPersons.Parameters.ParamByName('@OrganisationID').Value := OrganisationID;
  stprJoinPersons.ExecProc;
  result := stprJoinPersons.Parameters.ParamByName('@RETURN_VALUE').Value;
end;

initialization
  ofOrganisation := TComponentFactory.Create(ComServer, TrdtmOrganisation,
    Class_rdtmOrganisation, ciInternal, tmApartment);
end.
