inherited rdtmOrganisation: TrdtmOrganisation
  OldCreateOrder = True
  Height = 299
  Width = 422
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ORGANISATION_ID, '
      '  F_NAME, '
      '  F_RSZ_NR, '
      '  F_VAT_NR, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_ACTIVE, '
      '  F_ORGTYPE_ID, '
      '  F_ORGTYPE_NAME,'
      '  F_MANUALLY_ADDED,'
      '  F_ORG_NR,'
      '  F_SCHOOL_NR,'
      '  F_CONSTRUCT_ID,'
      '  F_CONTACT_DATA,'
      '  F_EMAIL,'
      '  F_SYNERGY_ID'
      'FROM '
      '  V_OR_ORGANISATION'
      '')
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object adoqryListF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
    object adoqryListF_ORG_NR: TStringField
      FieldName = 'F_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_SCHOOL_NR: TStringField
      FieldName = 'F_SCHOOL_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_MANUALLY_ADDED: TBooleanField
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CONTACT_DATA: TStringField
      DisplayLabel = 'Contact data'
      FieldName = 'F_CONTACT_DATA'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_VAT_NR: TStringField
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SYNERGY_ID: TLargeintField
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ORGANISATION_ID, '
      '  F_NAME, '
      '  F_RSZ_NR, '
      '  F_VAT_NR, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_EMAIL, '
      '  F_WEBSITE, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_ACC_NR, '
      '  F_ACC_HOLDER, '
      '  F_MEDIUM_ID, '
      '  F_MEDIUM_NAME, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_INVOICE_STREET, '
      '  F_INVOICE_NUMBER, '
      '  F_INVOICE_MAILBOX, '
      '  F_INVOICE_POSTALCODE_ID, '
      '  F_INVOICE_COUNTRY_ID, '
      '  F_ORGTYPE_ID, '
      '  F_ORGTYPE_NAME,'
      '  F_INVOICE_POSTALCODE, '
      '  F_INVOICE_CITY_NAME, '
      '  F_INVOICE_COUNTRY_NAME,'
      '  F_MANUALLY_ADDED,'
      '  F_ORG_NR,'
      '  F_SCHOOL_NR,'
      '  F_CONSTRUCT_ID,'
      '  F_CONTACT_DATA,'
      '  F_CONFIRMATION_CONTACT1,'
      '  F_CONFIRMATION_CONTACT1_EMAIL,'
      '  F_CONFIRMATION_CONTACT2,'
      '  F_CONFIRMATION_CONTACT2_EMAIL,'
      '  F_CONFIRMATION_CONTACT3,'
      '  F_CONFIRMATION_CONTACT3_EMAIL,'
      '  F_EXTRA_INFO,'
      '  F_INVOICE_EXTRA_INFO,'
      '  F_SYNERGY_ID,'
      '  F_PARTNER_WINTER,'
      '  F_AUTHORIZATION_NR,'
      '  F_ADMIN_COST,'
      '  F_ADMIN_COST_TYPE'
      'FROM '
      '  V_OR_ORGANISATION')
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object adoqryRecordF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryRecordF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Visible = False
      Size = 128
    end
    object adoqryRecordF_WEBSITE: TStringField
      FieldName = 'F_WEBSITE'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object adoqryRecordF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object adoqryRecordF_ACC_HOLDER: TStringField
      FieldName = 'F_ACC_HOLDER'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = []
      Visible = False
    end
    object adoqryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INVOICE_STREET: TStringField
      FieldName = 'F_INVOICE_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_INVOICE_NUMBER: TStringField
      FieldName = 'F_INVOICE_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryRecordF_INVOICE_MAILBOX: TStringField
      FieldName = 'F_INVOICE_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRecordF_INVOICE_POSTALCODE_ID: TIntegerField
      FieldName = 'F_INVOICE_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INVOICE_COUNTRY_ID: TIntegerField
      FieldName = 'F_INVOICE_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
    object adoqryRecordF_INVOICE_POSTALCODE: TStringField
      FieldName = 'F_INVOICE_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_INVOICE_CITY_NAME: TStringField
      FieldName = 'F_INVOICE_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_INVOICE_COUNTRY_NAME: TStringField
      FieldName = 'F_INVOICE_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_ORG_NR: TStringField
      FieldName = 'F_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_SCHOOL_NR: TStringField
      FieldName = 'F_SCHOOL_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_MANUALLY_ADDED: TBooleanField
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CONTACT_DATA: TStringField
      DisplayLabel = 'Contact data'
      FieldName = 'F_CONTACT_DATA'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryRecordF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_VAT_NR: TStringField
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EXTRA_INFO: TStringField
      FieldName = 'F_EXTRA_INFO'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_INVOICE_EXTRA_INFO: TStringField
      FieldName = 'F_INVOICE_EXTRA_INFO'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CONFIRMATION_CONTACT3: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT3'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT3_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_SYNERGY_ID: TLargeintField
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PARTNERWINTER: TBooleanField
      FieldName = 'F_PARTNER_WINTER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_AUTHORIZATION_NR: TStringField
      FieldName = 'F_AUTHORIZATION_NR'
      ProviderFlags = [pfInUpdate]
      Size = 25
    end
    object adoqryRecordF_ADMIN_COST: TFloatField
      FieldName = 'F_ADMIN_COST'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ADMIN_COST_TYPE: TStringField
      FieldName = 'F_ADMIN_COST_TYPE'
      ProviderFlags = [pfInUpdate]
      FixedChar = True
      Size = 1
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_OR_ORGANISATION: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_OR_ORGANISATION;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 272
    Top = 88
  end
  object stprJoinOrganisations: TADOStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_SYS_JOIN_ORGANISATION;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrganisationList'
        Attributes = [paNullable]
        DataType = ftString
        Size = 8000
        Value = Null
      end
      item
        Name = '@CopyToOrganisationID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 136
    Top = 152
  end
  object stprJoinPersons: TADOStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_SYS_JOIN_PERSON;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrganisationID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 240
    Top = 152
  end
end
