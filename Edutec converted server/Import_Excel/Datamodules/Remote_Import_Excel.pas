unit Remote_Import_Excel;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX, Variants;

type
  TrdtmImport_Excel = class(TrdtmEduBase, IrdtmImport_Excel)
    adoqryXLS: TFVBFFCQuery;
    prvXLS: TFVBFFCDataSetProvider;
    stprValidate_Person: TFVBFFCStoredProc;
    stprValidate_Organisation: TFVBFFCStoredProc;
    stpr_XLSSetProcessed: TFVBFFCStoredProc;
    prvGetSessionsForProgram: TFVBFFCDataSetProvider;
    adospGetSessionsForProgram: TFVBFFCStoredProc;
    adosp_IMPORTMANUAL: TFVBFFCStoredProc;
    stpr_XLSAutCreate: TFVBFFCStoredProc;
    stprValidate_Session: TFVBFFCStoredProc;
    adoqryListF_VCA_IMPORT_ID: TIntegerField;
    adoqryListF_VCA_EXAM_NR: TStringField;
    adoqryListF_PERSON_LASTNAME: TStringField;
    adoqryListF_PERSON_FIRSTNAME: TStringField;
    adoqryListF_DATE_OF_BIRTH: TDateTimeField;
    adoqryListF_PLACE_OF_BIRTH: TStringField;
    adoqryListF_MAILBOX: TStringField;
    adoqryListF_REEKS: TStringField;
    adoqryListF_SCORE: TStringField;
    adoqryListF_ORG_NAME: TStringField;
    adoqryListF_ADDRESS: TStringField;
    adoqryListF_POSTAL_CODE: TStringField;
    adoqryListF_COUNTRY: TStringField;
    adoqryListF_CITY: TStringField;
    adoqryListF_EXTRA: TStringField;
    adoqryListF_INSERT_DATE: TDateTimeField;
    adoqryListF_UPDATE_DATE: TDateTimeField;
    adoqryListF_FLAG: TStringField;
    adoqryListF_COMMENT: TStringField;
    adoqryListF_PERSON_ID: TIntegerField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_PROGRAM_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_SESSION_NAME: TStringField;
    adoqryListF_SESSION_CODE: TStringField;
    adoqryListF_SESSION_START_DATE: TDateTimeField;
    adoqryListF_SESSION_END_DATE: TDateTimeField;
    stpr_XLSSetSession: TFVBFFCStoredProc;
    adoqryListF_FUNCTIE: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    procedure ProcessValidateKeys; safecall;
    procedure XLSSetProcessed(AImportId: Integer); safecall;
    procedure XLSSetSession(AImportId, ASession: Integer); safecall;
    procedure XLSAutCreate(AImportId: Integer); safecall;
    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): WideString;
      safecall;
    { Public declarations }
  end;

var
  ofImport_Excel: TComponentFactory;

implementation

uses Remote_EduMain;

{$R *.dfm}



{ TrdtmImport_Excel }

class procedure TrdtmImport_Excel.UpdateRegistry(Register: Boolean;
  const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmImport_Excel.ProcessSingleRecord(EducationIdentity,
  SessionIdentity: Integer): WideString;
begin
  adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_EDUCATION_IDENTITY' ).Value := EducationIdentity;
  adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_SESSION_IDENTITY' ).Value := SessionIdentity;
//  adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_FLAG' ).Value := '';
  adosp_IMPORTMANUAL.ExecProc;
    if VarIsEmpty( adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_FLAG' ).Value ) then
      Result := ''
    else
      Result := adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_FLAG' ).Value;
end;

function TrdtmImport_Excel.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmImport_Excel.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmImport_Excel.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmImport_Excel.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmImport_Excel.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmImport_Excel.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmImport_Excel.GetTableName: String;
begin
  Result := 'T_XLS_VCA_IMPORT';
end;

function TrdtmImport_Excel.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

procedure TrdtmImport_Excel.ProcessValidateKeys;
begin
  stprValidate_Person.ExecProc;
  stprValidate_Organisation.ExecProc;
  stprValidate_Session.ExecProc;
end;

procedure TrdtmImport_Excel.XLSSetProcessed(AImportId: Integer);
begin
  stpr_XLSSetProcessed.Parameters.ParamByName('@AImportId').Value := AImportId;
  stpr_XLSSetProcessed.ExecProc;
end;

procedure TrdtmImport_Excel.XLSSetSession(AImportId, ASession: Integer);
begin
  stpr_XLSSetSession.Parameters.ParamByName('@AImportId').Value := AImportId;
  stpr_XLSSetSession.Parameters.ParamByName('@Session').Value := ASession;
  stpr_XLSSetSession.ExecProc;
end;

procedure TrdtmImport_Excel.XLSAutCreate(AImportId: Integer);
begin
  stpr_XLSAutCreate.Parameters.ParamByName('@F_VCA_IMPORT_ID').Value := AImportId;
  stpr_XLSAutCreate.ExecProc;
end;

initialization
  ofImport_Excel := TComponentFactory.Create(ComServer, TrdtmImport_Excel,
    CLASS_rdtmImport_Excel, ciInternal, tmApartment);

end.
