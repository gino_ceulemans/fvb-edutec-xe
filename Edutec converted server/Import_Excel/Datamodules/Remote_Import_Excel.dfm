inherited rdtmImport_Excel: TrdtmImport_Excel
  OldCreateOrder = True
  Height = 422
  Width = 637
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '       F_VCA_IMPORT_ID'
      '      ,F_VCA_EXAM_NR'
      '      ,F_PERSON_LASTNAME'
      '      ,F_PERSON_FIRSTNAME'
      '      ,F_DATE_OF_BIRTH'
      '      ,F_PLACE_OF_BIRTH'
      '      ,F_MAILBOX'
      '      ,F_REEKS'
      '      ,F_SCORE'
      '      ,F_ORG_NAME'
      '      ,F_ADDRESS'
      '      ,F_POSTAL_CODE'
      '      ,F_COUNTRY'
      '      ,F_CITY'
      '      ,F_EXTRA'
      '      ,F_FUNCTIE'
      '      ,F_INSERT_DATE'
      '      ,F_UPDATE_DATE'
      '      ,F_FLAG'
      '      ,F_COMMENT'
      '      ,F_PERSON_ID'
      '      ,F_ORGANISATION_ID'
      '      ,F_PROGRAM_ID'
      '      ,F_NAME'
      '      ,F_SESSION_ID'
      '      ,F_SESSION_NAME'
      '      ,F_SESSION_CODE'
      '      ,F_SESSION_START_DATE'
      '      ,F_SESSION_END_DATE'
      '  FROM'
      '       dbo.V_XLS_VCA_IMPORT')
    object adoqryListF_VCA_IMPORT_ID: TIntegerField
      FieldName = 'F_VCA_IMPORT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryListF_VCA_EXAM_NR: TStringField
      FieldName = 'F_VCA_EXAM_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryListF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_DATE_OF_BIRTH: TDateTimeField
      FieldName = 'F_DATE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryListF_REEKS: TStringField
      FieldName = 'F_REEKS'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryListF_SCORE: TStringField
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryListF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_ADDRESS: TStringField
      FieldName = 'F_ADDRESS'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_POSTAL_CODE: TStringField
      FieldName = 'F_POSTAL_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_COUNTRY: TStringField
      FieldName = 'F_COUNTRY'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_CITY: TStringField
      FieldName = 'F_CITY'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_EXTRA: TStringField
      FieldName = 'F_EXTRA'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_FUNCTIE: TStringField
      FieldName = 'F_FUNCTIE'
      Size = 50
    end
    object adoqryListF_INSERT_DATE: TDateTimeField
      FieldName = 'F_INSERT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_UPDATE_DATE: TDateTimeField
      FieldName = 'F_UPDATE_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object adoqryListF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
  end
  object adoqryXLS: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT F_VCA_IMPORT_ID'
      '      ,F_VCA_EXAM_NR'
      '      ,F_PERSON_LASTNAME'
      '      ,F_PERSON_FIRSTNAME'
      '      ,F_DATE_OF_BIRTH'
      '      ,F_PLACE_OF_BIRTH'
      '      ,F_MAILBOX'
      '      ,F_REEKS'
      '      ,F_SCORE'
      '      ,F_ORG_NAME'
      '      ,F_ADDRESS'
      '      ,F_POSTAL_CODE'
      '      ,F_COUNTRY'
      '      ,F_CITY'
      '      ,F_EXTRA'
      '      ,F_FUNCTIE'
      '      ,F_INSERT_DATE'
      '      ,F_UPDATE_DATE'
      '      ,F_FLAG'
      '      ,F_COMMENT'
      '      ,F_PERSON_ID'
      '      ,F_ORGANISATION_ID'
      '      ,F_PROGRAM_ID'
      '      ,F_SESSION_ID'
      '  FROM T_XLS_VCA_IMPORT'
      '  WHERE F_FLAG = '#39'I'#39)
    AutoOpen = False
    Left = 232
    Top = 24
  end
  object prvXLS: TFVBFFCDataSetProvider
    DataSet = adoqryXLS
    OnGetTableName = prvListGetTableName
    Left = 232
    Top = 72
  end
  object stprValidate_Person: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XLS_VALIDATE_PERSON'
    Parameters = <>
    AutoOpen = False
    Left = 64
    Top = 136
  end
  object stprValidate_Organisation: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XLS_VALIDATE_ORGANISATION'
    Parameters = <>
    AutoOpen = False
    Left = 64
    Top = 192
  end
  object stpr_XLSSetProcessed: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XLS_SET_PROCESSED'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@AImportId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 200
    Top = 136
  end
  object prvGetSessionsForProgram: TFVBFFCDataSetProvider
    DataSet = adospGetSessionsForProgram
    OnGetTableName = prvListGetTableName
    Left = 368
    Top = 224
  end
  object adospGetSessionsForProgram: TFVBFFCStoredProc
    AutoCalcFields = False
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_GET_SESSIONS_FOR_PROGRAM;1'
    Parameters = <
      item
        Name = '@ATransfertId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AMsgType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@AStartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    AutoOpen = False
    Left = 368
    Top = 176
  end
  object adosp_IMPORTMANUAL: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XLS_AUT_CREATE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_VCA_IMPORT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 512
    Top = 176
  end
  object stpr_XLSAutCreate: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    ProcedureName = 'SP_XLS_AUT_CREATE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = -6
      end
      item
        Name = '@F_VCA_IMPORT_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 200
    Top = 192
  end
  object stprValidate_Session: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XLS_VALIDATE_SESSION'
    Parameters = <>
    AutoOpen = False
    Left = 64
    Top = 256
  end
  object stpr_XLSSetSession: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XLS_SET_SESSION;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@AImportId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@Session'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 200
    Top = 256
  end
end
