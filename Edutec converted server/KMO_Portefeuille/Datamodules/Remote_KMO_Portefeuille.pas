unit Remote_KMO_Portefeuille;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, DB,
  Provider, Unit_FVBFFCDBComponents, ADODB;

type
  TrdtmKMO_Portefeuille = class(TrdtmEduBase, IrdtmKMO_Portefeuille)
    adoqryListF_KMO_PORTEFEUILLE_ID: TIntegerField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_PROJECT_NR: TStringField;
    adoqryListF_KMO_PORT_STATUS_ID: TIntegerField;
    adoqryListF_AMOUNT_REQUESTED: TFloatField;
    adoqryListF_PAY_TO_SODEXO_OK: TBooleanField;
    adoqryListF_REMARK: TStringField;
    adoqryListF_STATUS_NAME: TStringField;
    adoqryListF_STATUS_NL: TStringField;
    adoqryListF_STATUS_FR: TStringField;
    adoqryListF_FOREGROUNDCOLOR: TIntegerField;
    adoqryListF_BACKGROUNDCOLOR: TIntegerField;
    adoqryRecordF_KMO_PORTEFEUILLE_ID: TIntegerField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_PROJECT_NR: TStringField;
    adoqryRecordF_KMO_PORT_STATUS_ID: TIntegerField;
    adoqryRecordF_AMOUNT_REQUESTED: TFloatField;
    adoqryRecordF_PAY_TO_SODEXO_OK: TBooleanField;
    adoqryRecordF_REMARK: TStringField;
    adoqryRecordF_STATUS_NAME: TStringField;
    adoqryRecordF_STATUS_NL: TStringField;
    adoqryRecordF_STATUS_FR: TStringField;
    adoqryRecordF_FOREGROUNDCOLOR: TIntegerField;
    adoqryRecordF_BACKGROUNDCOLOR: TIntegerField;
    adoqryListF_ORG_NAME: TStringField;
    adoqryListF_ORG_ORG_NR: TStringField;
    adoqryListF_ORG_VAT_NR: TStringField;
    adoqryListF_ORG_SYNERGY_ID: TLargeintField;
    adoqryListF_ORG_RSZ_NR: TStringField;
    adoqryRecordF_ORG_NAME: TStringField;
    adoqryRecordF_ORG_ORG_NR: TStringField;
    adoqryRecordF_ORG_VAT_NR: TStringField;
    adoqryRecordF_ORG_SYNERGY_ID: TLargeintField;
    adoqryRecordF_ORG_RSZ_NR: TStringField;
    adoqryListF_PAYMENT_OK: TBooleanField;
    adoqryRecordF_PAYMENT_OK: TBooleanField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_ANNULATION_REASON: TStringField;
    adoqryListF_PAYMENT_AMOUNT1: TBCDField;
    adoqryListF_PAYMENT_AMOUNT2: TBCDField;
    adoqryListF_PAYMENT_AMOUNT3: TBCDField;
    adoqryListF_SESSION: TStringField;
    adoqryListF_PROGRAM: TStringField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_ANNULATION_REASON: TStringField;
    adoqryRecordF_PAYMENT_AMOUNT1: TBCDField;
    adoqryRecordF_PAYMENT_AMOUNT2: TBCDField;
    adoqryRecordF_PAYMENT_AMOUNT3: TBCDField;
    adoqryRecordF_SESSION: TStringField;
    adoqryRecordF_PROGRAM: TStringField;
    adoqryListF_DEMAND_DATE: TDateTimeField;
    adoqryRecordF_DEMAND_DATE: TDateTimeField;
    adoqryListF_SESSION_DATE: TDateTimeField;
    adoqryRecordF_SESSION_DATE: TDateTimeField;
    adoqryRecordF_PAYMENT_STATUS_ID: TIntegerField;
    adoqryRecordF_PAYMENT_STATUS_NAME: TStringField;
    adoqryListF_PAYMENT_STATUS_ID: TIntegerField;
    adoqryListF_PAYMENT_STATUS_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofKMO_Portefeuille: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmKMO_Portefeuille.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmKMO_Portefeuille.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmKMO_Portefeuille.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmKMO_Portefeuille.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmKMO_Portefeuille.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmKMO_Portefeuille.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmKMO_Portefeuille.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmKMO_Portefeuille.GetTableName: String;
begin
  Result := 'T_KMO_PORTEFEUILLE';
end;

function TrdtmKMO_Portefeuille.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofKMO_Portefeuille := TComponentFactory.Create(ComServer, TrdtmKMO_Portefeuille,
    Class_rdtmKMO_Portefeuille, ciInternal, tmApartment);
end.
