inherited rdtmKMO_Portefeuille: TrdtmKMO_Portefeuille
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_KMO_PORTEFEUILLE_ID , '
      ' F_ORGANISATION_ID , '
      ' F_PROJECT_NR , '
      ' F_KMO_PORT_STATUS_ID , '
      ' F_AMOUNT_REQUESTED , '
      ' F_PAY_TO_SODEXO_OK , '
      ' F_PAYMENT_OK ,'
      ' F_REMARK, '
      ' F_STATUS_NAME, '
      ' F_STATUS_NL, '
      ' F_STATUS_FR, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR,'
      ' F_ORG_NAME,'
      ' F_ORG_ORG_NR,'
      ' F_ORG_VAT_NR,'
      ' F_ORG_SYNERGY_ID,'
      ' F_ORG_RSZ_NR,'
      'F_DEMAND_DATE,'
      'F_SESSION_ID,'
      'F_ANNULATION_REASON ,'
      'F_PAYMENT_AMOUNT1,'
      'F_PAYMENT_AMOUNT2,'
      'F_PAYMENT_AMOUNT3,'
      'F_SESSION,'
      'F_PROGRAM,'
      'F_SESSION_DATE,'
      'F_PAYMENT_STATUS_ID,'
      'F_PAYMENT_STATUS_NAME'
      'FROM '
      '  V_KMO_PORTEFEUILLE'
      '')
    object adoqryListF_KMO_PORTEFEUILLE_ID: TIntegerField
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROJECT_NR: TStringField
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryListF_KMO_PORT_STATUS_ID: TIntegerField
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_AMOUNT_REQUESTED: TFloatField
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PAY_TO_SODEXO_OK: TBooleanField
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PAYMENT_OK: TBooleanField
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_REMARK: TStringField
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_STATUS_NL: TStringField
      FieldName = 'F_STATUS_NL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_STATUS_FR: TStringField
      FieldName = 'F_STATUS_FR'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = []
    end
    object adoqryListF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = []
    end
    object adoqryListF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ORG_ORG_NR: TStringField
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_ORG_VAT_NR: TStringField
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = []
    end
    object adoqryListF_ORG_SYNERGY_ID: TLargeintField
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = []
    end
    object adoqryListF_ORG_RSZ_NR: TStringField
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ANNULATION_REASON: TStringField
      FieldName = 'F_ANNULATION_REASON'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryListF_PAYMENT_AMOUNT1: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT1'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object adoqryListF_PAYMENT_AMOUNT2: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT2'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object adoqryListF_PAYMENT_AMOUNT3: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT3'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object adoqryListF_SESSION: TStringField
      FieldName = 'F_SESSION'
      ProviderFlags = []
      Size = 73
    end
    object adoqryListF_PROGRAM: TStringField
      FieldName = 'F_PROGRAM'
      ProviderFlags = []
      Size = 75
    end
    object adoqryListF_DEMAND_DATE: TDateTimeField
      FieldName = 'F_DEMAND_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_DATE: TDateTimeField
      FieldName = 'F_SESSION_DATE'
      ProviderFlags = []
    end
    object adoqryListF_PAYMENT_STATUS_ID: TIntegerField
      FieldName = 'F_PAYMENT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PAYMENT_STATUS_NAME: TStringField
      FieldName = 'F_PAYMENT_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_KMO_PORTEFEUILLE_ID , '
      ' F_ORGANISATION_ID , '
      ' F_PROJECT_NR , '
      ' F_KMO_PORT_STATUS_ID , '
      ' F_AMOUNT_REQUESTED , '
      ' F_PAY_TO_SODEXO_OK , '
      ' F_PAYMENT_OK ,'
      ' F_REMARK, '
      ' F_STATUS_NAME, '
      ' F_STATUS_NL, '
      ' F_STATUS_FR, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR,'
      ' F_ORG_NAME,'
      ' F_ORG_ORG_NR,'
      ' F_ORG_VAT_NR,'
      ' F_ORG_SYNERGY_ID,'
      ' F_ORG_RSZ_NR,'
      'F_DEMAND_DATE,'
      'F_SESSION_ID,'
      'F_ANNULATION_REASON ,'
      'F_PAYMENT_AMOUNT1,'
      'F_PAYMENT_AMOUNT2,'
      'F_PAYMENT_AMOUNT3,'
      'F_SESSION,'
      'F_PROGRAM,'
      'F_SESSION_DATE,'
      'F_PAYMENT_STATUS_ID,'
      'F_PAYMENT_STATUS_NAME'
      'FROM '
      '  V_KMO_PORTEFEUILLE'
      '')
    object adoqryRecordF_KMO_PORTEFEUILLE_ID: TIntegerField
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROJECT_NR: TStringField
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryRecordF_KMO_PORT_STATUS_ID: TIntegerField
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_AMOUNT_REQUESTED: TFloatField
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PAY_TO_SODEXO_OK: TBooleanField
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PAYMENT_OK: TBooleanField
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_REMARK: TStringField
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_STATUS_NL: TStringField
      FieldName = 'F_STATUS_NL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_STATUS_FR: TStringField
      FieldName = 'F_STATUS_FR'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = []
    end
    object adoqryRecordF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = []
    end
    object adoqryRecordF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_ORG_ORG_NR: TStringField
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_ORG_VAT_NR: TStringField
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = []
    end
    object adoqryRecordF_ORG_SYNERGY_ID: TLargeintField
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = []
    end
    object adoqryRecordF_ORG_RSZ_NR: TStringField
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ANNULATION_REASON: TStringField
      FieldName = 'F_ANNULATION_REASON'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryRecordF_PAYMENT_AMOUNT1: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT1'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object adoqryRecordF_PAYMENT_AMOUNT2: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT2'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object adoqryRecordF_PAYMENT_AMOUNT3: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT3'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object adoqryRecordF_SESSION: TStringField
      FieldName = 'F_SESSION'
      ProviderFlags = []
      Size = 73
    end
    object adoqryRecordF_PROGRAM: TStringField
      FieldName = 'F_PROGRAM'
      ProviderFlags = []
      Size = 75
    end
    object adoqryRecordF_DEMAND_DATE: TDateTimeField
      FieldName = 'F_DEMAND_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_DATE: TDateTimeField
      FieldName = 'F_SESSION_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_PAYMENT_STATUS_ID: TIntegerField
      FieldName = 'F_PAYMENT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PAYMENT_STATUS_NAME: TStringField
      FieldName = 'F_PAYMENT_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
  end
end
