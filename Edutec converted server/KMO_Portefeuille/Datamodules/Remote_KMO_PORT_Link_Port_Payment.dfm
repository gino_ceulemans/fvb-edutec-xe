inherited rdtmKMO_PORT_Link_Port_Payment: TrdtmKMO_PORT_Link_Port_Payment
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      ' SELECT '
      ' F_KMO_PORT_LNK_PORT_PAY_ID , '
      ' F_KMO_PORTEFEUILLE_ID , '
      ' F_KMO_PORT_PAYMENT_ID , '
      ' F_AMOUNT_LINKED , '
      ' F_INVOICE_NR , '
      ' F_AMOUNT_PAYED , '
      ' F_DATE_PAYED , '
      ' F_SESSION_ID , '
      ' F_SESSION_CODE , '
      ' F_SESSION_NAME , '
      ' F_SESSION_START_DATE , '
      ' F_SESSION_END_DATE , '
      ' F_SESSION_STATUS_NAME , '
      ' F_PROJECT_NR , '
      ' F_ORGANISATION_ID , '
      ' F_KMO_PORT_STATUS_ID , '
      ' F_AMOUNT_REQUESTED , '
      ' F_PAY_TO_SODEXO_OK , '
      ' F_PAYMENT_OK , '
      ' F_STATUS_NAME , '
      ' F_ORG_NAME , '
      ' F_ORG_ORG_NR , '
      ' F_ORG_VAT_NR , '
      ' F_ORG_SYNERGY_ID , '
      ' F_ORG_RSZ_NR '
      ' FROM dbo.V_KMO_PORT_LINK_PORT_PAYMENT '
      '')
    object adoqryListF_KMO_PORT_LNK_PORT_PAY_ID: TIntegerField
      FieldName = 'F_KMO_PORT_LNK_PORT_PAY_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryListF_KMO_PORTEFEUILLE_ID: TIntegerField
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_KMO_PORT_PAYMENT_ID: TIntegerField
      FieldName = 'F_KMO_PORT_PAYMENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_AMOUNT_LINKED: TFloatField
      FieldName = 'F_AMOUNT_LINKED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INVOICE_NR: TStringField
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = []
      Size = 30
    end
    object adoqryListF_AMOUNT_PAYED: TFloatField
      FieldName = 'F_AMOUNT_PAYED'
      ProviderFlags = []
    end
    object adoqryListF_DATE_PAYED: TDateTimeField
      FieldName = 'F_DATE_PAYED'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_STATUS_NAME: TStringField
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PROJECT_NR: TStringField
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = []
      Size = 30
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object adoqryListF_KMO_PORT_STATUS_ID: TIntegerField
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = []
    end
    object adoqryListF_AMOUNT_REQUESTED: TFloatField
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = []
    end
    object adoqryListF_PAY_TO_SODEXO_OK: TBooleanField
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = []
    end
    object adoqryListF_PAYMENT_OK: TBooleanField
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = []
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ORG_ORG_NR: TStringField
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_ORG_VAT_NR: TStringField
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = []
    end
    object adoqryListF_ORG_SYNERGY_ID: TLargeintField
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = []
    end
    object adoqryListF_ORG_RSZ_NR: TStringField
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      ' SELECT '
      ' F_KMO_PORT_LNK_PORT_PAY_ID , '
      ' F_KMO_PORTEFEUILLE_ID , '
      ' F_KMO_PORT_PAYMENT_ID , '
      ' F_AMOUNT_LINKED , '
      ' F_INVOICE_NR , '
      ' F_AMOUNT_PAYED , '
      ' F_DATE_PAYED , '
      ' F_SESSION_ID , '
      ' F_SESSION_CODE , '
      ' F_SESSION_NAME , '
      ' F_SESSION_START_DATE , '
      ' F_SESSION_END_DATE , '
      ' F_SESSION_STATUS_NAME , '
      ' F_PROJECT_NR , '
      ' F_ORGANISATION_ID , '
      ' F_KMO_PORT_STATUS_ID , '
      ' F_AMOUNT_REQUESTED , '
      ' F_PAY_TO_SODEXO_OK , '
      ' F_PAYMENT_OK , '
      ' F_STATUS_NAME , '
      ' F_ORG_NAME , '
      ' F_ORG_ORG_NR , '
      ' F_ORG_VAT_NR , '
      ' F_ORG_SYNERGY_ID , '
      ' F_ORG_RSZ_NR '
      ' FROM dbo.V_KMO_PORT_LINK_PORT_PAYMENT '
      '')
    object adoqryRecordF_KMO_PORT_LNK_PORT_PAY_ID: TIntegerField
      FieldName = 'F_KMO_PORT_LNK_PORT_PAY_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryRecordF_KMO_PORTEFEUILLE_ID: TIntegerField
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_KMO_PORT_PAYMENT_ID: TIntegerField
      FieldName = 'F_KMO_PORT_PAYMENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_AMOUNT_LINKED: TFloatField
      FieldName = 'F_AMOUNT_LINKED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INVOICE_NR: TStringField
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = []
      Size = 30
    end
    object adoqryRecordF_AMOUNT_PAYED: TFloatField
      FieldName = 'F_AMOUNT_PAYED'
      ProviderFlags = []
    end
    object adoqryRecordF_DATE_PAYED: TDateTimeField
      FieldName = 'F_DATE_PAYED'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_STATUS_NAME: TStringField
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PROJECT_NR: TStringField
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = []
      Size = 30
    end
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object adoqryRecordF_KMO_PORT_STATUS_ID: TIntegerField
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = []
    end
    object adoqryRecordF_AMOUNT_REQUESTED: TFloatField
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = []
    end
    object adoqryRecordF_PAY_TO_SODEXO_OK: TBooleanField
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = []
    end
    object adoqryRecordF_PAYMENT_OK: TBooleanField
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = []
    end
    object adoqryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_ORG_ORG_NR: TStringField
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_ORG_VAT_NR: TStringField
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = []
    end
    object adoqryRecordF_ORG_SYNERGY_ID: TLargeintField
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = []
    end
    object adoqryRecordF_ORG_RSZ_NR: TStringField
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
  end
end
