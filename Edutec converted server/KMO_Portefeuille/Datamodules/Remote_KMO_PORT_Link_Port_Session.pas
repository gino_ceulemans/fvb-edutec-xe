unit Remote_KMO_PORT_Link_Port_Session;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, DB,
  Provider, Unit_FVBFFCDBComponents, ADODB;

type
  TrdtmKMO_PORT_Link_Port_Session = class(TrdtmEduBase, IrdtmKMO_PORT_Link_Port_Session)
    adoqryListF_KMO_PORT_LNK_PORT_SES_ID: TIntegerField;
    adoqryListF_KMO_PORTEFEUILLE_ID: TIntegerField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_SESSION_CODE: TStringField;
    adoqryListF_SESSION_NAME: TStringField;
    adoqryListF_SESSION_START_DATE: TDateTimeField;
    adoqryListF_SESSION_END_DATE: TDateTimeField;
    adoqryListF_SESSION_STATUS_NAME: TStringField;
    adoqryListF_PROJECT_NR: TStringField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_KMO_PORT_STATUS_ID: TIntegerField;
    adoqryListF_AMOUNT_REQUESTED: TFloatField;
    adoqryListF_PAY_TO_SODEXO_OK: TBooleanField;
    adoqryListF_STATUS_NAME: TStringField;
    adoqryListF_ORG_NAME: TStringField;
    adoqryListF_ORG_ORG_NR: TStringField;
    adoqryListF_ORG_VAT_NR: TStringField;
    adoqryListF_ORG_SYNERGY_ID: TLargeintField;
    adoqryListF_ORG_RSZ_NR: TStringField;
    adoqryRecordF_KMO_PORT_LNK_PORT_SES_ID: TIntegerField;
    adoqryRecordF_KMO_PORTEFEUILLE_ID: TIntegerField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_SESSION_CODE: TStringField;
    adoqryRecordF_SESSION_NAME: TStringField;
    adoqryRecordF_SESSION_START_DATE: TDateTimeField;
    adoqryRecordF_SESSION_END_DATE: TDateTimeField;
    adoqryRecordF_SESSION_STATUS_NAME: TStringField;
    adoqryRecordF_PROJECT_NR: TStringField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_KMO_PORT_STATUS_ID: TIntegerField;
    adoqryRecordF_AMOUNT_REQUESTED: TFloatField;
    adoqryRecordF_PAY_TO_SODEXO_OK: TBooleanField;
    adoqryRecordF_STATUS_NAME: TStringField;
    adoqryRecordF_ORG_NAME: TStringField;
    adoqryRecordF_ORG_ORG_NR: TStringField;
    adoqryRecordF_ORG_VAT_NR: TStringField;
    adoqryRecordF_ORG_SYNERGY_ID: TLargeintField;
    adoqryRecordF_ORG_RSZ_NR: TStringField;
    adoqryListF_AMOUNT_TO_PAY: TFloatField;
    adoqryListF_PAYMENT_OK: TBooleanField;
    adoqryRecordF_AMOUNT_TO_PAY: TFloatField;
    adoqryRecordF_PAYMENT_OK: TBooleanField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofKMO_PORT_Link_Port_Session: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmKMO_PORT_Link_Port_Session.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmKMO_PORT_Link_Port_Session.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmKMO_PORT_Link_Port_Session.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmKMO_PORT_Link_Port_Session.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmKMO_PORT_Link_Port_Session.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmKMO_PORT_Link_Port_Session.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmKMO_PORT_Link_Port_Session.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmKMO_PORT_Link_Port_Session.GetTableName: String;
begin
  Result := 'T_KMO_PORT_LNK_PORT_SES';
end;

function TrdtmKMO_PORT_Link_Port_Session.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofKMO_PORT_Link_Port_Session := TComponentFactory.Create(ComServer, TrdtmKMO_PORT_Link_Port_Session,
    Class_rdtmKMO_PORT_Link_Port_Session, ciInternal, tmApartment);
end.
