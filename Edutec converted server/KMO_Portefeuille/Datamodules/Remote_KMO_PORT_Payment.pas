unit Remote_KMO_PORT_Payment;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, DB,
  Provider, Unit_FVBFFCDBComponents, ADODB;

type
  TrdtmKMO_PORT_Payment = class(TrdtmEduBase, IrdtmKMO_PORT_Payment)
    adoqryListF_KMO_PORT_PAYMENT_ID: TIntegerField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_INVOICE_NR: TStringField;
    adoqryListF_AMOUNT_PAYED: TFloatField;
    adoqryListF_DATE_PAYED: TDateTimeField;
    adoqryListF_REMARK: TStringField;
    adoqryListF_SESSION_CODE: TStringField;
    adoqryListF_SESSION_NAME: TStringField;
    adoqryListF_SESSION_START_DATE: TDateTimeField;
    adoqryListF_SESSION_END_DATE: TDateTimeField;
    adoqryListF_SESSION_STATUS_NAME: TStringField;
    adoqryRecordF_KMO_PORT_PAYMENT_ID: TIntegerField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_INVOICE_NR: TStringField;
    adoqryRecordF_AMOUNT_PAYED: TFloatField;
    adoqryRecordF_DATE_PAYED: TDateTimeField;
    adoqryRecordF_REMARK: TStringField;
    adoqryRecordF_SESSION_CODE: TStringField;
    adoqryRecordF_SESSION_NAME: TStringField;
    adoqryRecordF_SESSION_START_DATE: TDateTimeField;
    adoqryRecordF_SESSION_END_DATE: TDateTimeField;
    adoqryRecordF_SESSION_STATUS_NAME: TStringField;
    adoqryListSUM_AMOUNT_LINKED: TFloatField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofKMO_PORT_Payment: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmKMO_PORT_Payment.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmKMO_PORT_Payment.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmKMO_PORT_Payment.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmKMO_PORT_Payment.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmKMO_PORT_Payment.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmKMO_PORT_Payment.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmKMO_PORT_Payment.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmKMO_PORT_Payment.GetTableName: String;
begin
  Result := 'T_KMO_PORT_PAYMENT';
end;

function TrdtmKMO_PORT_Payment.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofKMO_PORT_Payment := TComponentFactory.Create(ComServer, TrdtmKMO_PORT_Payment,
    Class_rdtmKMO_PORT_Payment, ciInternal, tmApartment);
end.
