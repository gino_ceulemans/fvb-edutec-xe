inherited rdtmKMO_PORT_Status: TrdtmKMO_PORT_Status
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_KMO_PORT_STATUS_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR, '
      ' F_STATUS_NAME '
      'FROM '
      '  V_KMO_PORT_STATUS'
      '')
    object adoqryListF_KMO_PORT_STATUS_ID: TIntegerField
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 128
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 128
    end
    object adoqryListF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_KMO_PORT_STATUS_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR, '
      ' F_STATUS_NAME '
      'FROM '
      '  V_KMO_PORT_STATUS'
      '')
    object adoqryRecordF_KMO_PORT_STATUS_ID: TIntegerField
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 128
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 128
    end
    object adoqryRecordF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
  end
end
