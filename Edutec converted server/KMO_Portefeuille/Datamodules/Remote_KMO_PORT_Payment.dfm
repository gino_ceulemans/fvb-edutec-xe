inherited rdtmKMO_PORT_Payment: TrdtmKMO_PORT_Payment
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      ' SELECT '
      ' F_KMO_PORT_PAYMENT_ID , '
      ' F_SESSION_ID , '
      ' F_INVOICE_NR , '
      ' F_AMOUNT_PAYED , '
      ' F_DATE_PAYED , '
      ' F_REMARK , '
      ' SUM_AMOUNT_LINKED ,'
      ' F_SESSION_CODE , '
      ' F_SESSION_NAME , '
      ' F_SESSION_START_DATE , '
      ' F_SESSION_END_DATE , '
      ' F_SESSION_STATUS_NAME '
      ' FROM dbo.V_KMO_PORT_PAYMENT '
      '')
    object adoqryListF_KMO_PORT_PAYMENT_ID: TIntegerField
      FieldName = 'F_KMO_PORT_PAYMENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INVOICE_NR: TStringField
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryListF_AMOUNT_PAYED: TFloatField
      FieldName = 'F_AMOUNT_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_DATE_PAYED: TDateTimeField
      FieldName = 'F_DATE_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_REMARK: TStringField
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryListSUM_AMOUNT_LINKED: TFloatField
      FieldName = 'SUM_AMOUNT_LINKED'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_STATUS_NAME: TStringField
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      ' SELECT '
      ' F_KMO_PORT_PAYMENT_ID , '
      ' F_SESSION_ID , '
      ' F_INVOICE_NR , '
      ' F_AMOUNT_PAYED , '
      ' F_DATE_PAYED , '
      ' F_REMARK , '
      ' F_SESSION_CODE , '
      ' F_SESSION_NAME , '
      ' F_SESSION_START_DATE , '
      ' F_SESSION_END_DATE , '
      ' F_SESSION_STATUS_NAME '
      ' FROM dbo.V_KMO_PORT_PAYMENT '
      '')
    object adoqryRecordF_KMO_PORT_PAYMENT_ID: TIntegerField
      FieldName = 'F_KMO_PORT_PAYMENT_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INVOICE_NR: TStringField
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object adoqryRecordF_AMOUNT_PAYED: TFloatField
      FieldName = 'F_AMOUNT_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DATE_PAYED: TDateTimeField
      FieldName = 'F_DATE_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_REMARK: TStringField
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryRecordF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_SESSION_STATUS_NAME: TStringField
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
