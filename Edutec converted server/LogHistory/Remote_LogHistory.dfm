inherited rdtmLogHistory: TrdtmLogHistory
  OldCreateOrder = True
  Height = 180
  Width = 345
  inherited prvList: TFVBFFCDataSetProvider
    DataSet = adospSysLogRetrieve
  end
  object adospSysLogRetrieve: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_SYS_LOGRETRIEVE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@entity'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@entityid'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 248
    Top = 24
  end
end
