{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_CEN_INFRASTRUCTURE record.

  @Name       Remote_CenInfrastructure
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_CenInfrastructure;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmCenInfrastructure = class(TrdtmEduBase, IrdtmCenInfrastructure)
    adoqryListF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_FVB_CENTER_ID: TIntegerField;
    adoqryListF_RSZ_NR: TStringField;
    adoqryListF_VAT_NR: TStringField;
    adoqryListF_STREET: TStringField;
    adoqryListF_NUMBER: TStringField;
    adoqryListF_MAILBOX: TStringField;
    adoqryListF_POSTALCODE_ID: TIntegerField;
    adoqryListF_POSTALCODE: TStringField;
    adoqryListF_CITY_NAME: TStringField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryListF_WEBSITE: TStringField;
    adoqryListF_LANGUAGE_ID: TIntegerField;
    adoqryListF_LANGUAGE_NAME: TStringField;
    adoqryListF_ACC_NR: TStringField;
    adoqryListF_ACC_HOLDER: TStringField;
    adoqryListF_MEDIUM_ID: TIntegerField;
    adoqryListF_MEDIUM_NAME: TStringField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_COMMENT: TMemoField;
    adoqryListF_SATURDAY: TBooleanField;
    adoqryListF_EVENING: TBooleanField;
    adoqryListF_WEEKDAY: TBooleanField;
    adoqryListF_LOCATION_ID: TIntegerField;
    adoqryListF_LOCATION: TStringField;
    adoqryListF_PRECONDITION_ID: TIntegerField;
    adoqryListF_PRECONDITION: TStringField;
    adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_FVB_CENTER_ID: TIntegerField;
    adoqryRecordF_RSZ_NR: TStringField;
    adoqryRecordF_VAT_NR: TStringField;
    adoqryRecordF_STREET: TStringField;
    adoqryRecordF_NUMBER: TStringField;
    adoqryRecordF_MAILBOX: TStringField;
    adoqryRecordF_POSTALCODE_ID: TIntegerField;
    adoqryRecordF_POSTALCODE: TStringField;
    adoqryRecordF_CITY_NAME: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_WEBSITE: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_ACC_NR: TStringField;
    adoqryRecordF_ACC_HOLDER: TStringField;
    adoqryRecordF_MEDIUM_ID: TIntegerField;
    adoqryRecordF_MEDIUM_NAME: TStringField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_COMMENT: TMemoField;
    adoqryRecordF_SATURDAY: TBooleanField;
    adoqryRecordF_EVENING: TBooleanField;
    adoqryRecordF_WEEKDAY: TBooleanField;
    adoqryRecordF_LOCATION_ID: TIntegerField;
    adoqryRecordF_LOCATION: TStringField;
    adoqryRecordF_PRECONDITION_ID: TIntegerField;
    adoqryRecordF_PRECONDITION: TStringField;
    adospSP_DEACTIVATE_CEN_INFRASTRUCTURE: TFVBFFCStoredProc;
    adoqryRecordF_CONFIRMATION_CONTACT1: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT2: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT3: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField;
    adoqryRecordF_INFO: TMemoField;
    adoqryListF_MAIL_ATTACHMENT: TStringField;
    adoqryRecordF_MAIL_ATTACHMENT: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofCenInfrastructure : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmCenInfrastructure.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmCenInfrastructure.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmCenInfrastructure.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmCenInfrastructure.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmCenInfrastructure.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmCenInfrastructure.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmCenInfrastructure.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmCenInfrastructure.GetTableName: String;
begin
  Result := 'T_CEN_INFRASTRUCTURE';
end;

procedure TrdtmCenInfrastructure.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_CEN_INFRASTRUCTURE(' + DeltaDS.FieldByName('F_INFRASTRUCTURE_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_CEN_INFRASTRUCTURE.Close;
    adospSP_DEACTIVATE_CEN_INFRASTRUCTURE.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_INFRASTRUCTURE_ID').Value;
    adospSP_DEACTIVATE_CEN_INFRASTRUCTURE.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmCenInfrastructure.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofCenInfrastructure := TComponentFactory.Create(ComServer, TrdtmCenInfrastructure,
    Class_rdtmCenInfrastructure, ciInternal, tmApartment);
end.
