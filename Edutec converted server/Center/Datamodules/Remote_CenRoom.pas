{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_CEN_ROOM record.

  @Name       Remote_CenRoom
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/10/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_CenRoom;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmCenRoom = class(TrdtmEduBase, IrdtmCenRoom)
    adoqryListF_ROOM_ID: TIntegerField;
    adoqryListF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryListF_NAME_NL: TStringField;
    adoqryListF_NAME_FR: TStringField;
    adoqryListF_DESCRIPTION_NL: TStringField;
    adoqryListF_DESCRIPTION_FR: TStringField;
    adoqryListF_CUBIC_METRES: TSmallintField;
    adoqryListF_HEIGHT: TSmallintField;
    adoqryListF_EQUIPMENT: TMemoField;
    adoqryListF_TECH_FICHE: TStringField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_ROOM_NAME: TStringField;
    adoqryListF_INFRASTRUCTURE_NAME: TStringField;
    adoqryRecordF_ROOM_ID: TIntegerField;
    adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryRecordF_NAME_NL: TStringField;
    adoqryRecordF_NAME_FR: TStringField;
    adoqryRecordF_DESCRIPTION_NL: TStringField;
    adoqryRecordF_DESCRIPTION_FR: TStringField;
    adoqryRecordF_CUBIC_METRES: TSmallintField;
    adoqryRecordF_HEIGHT: TSmallintField;
    adoqryRecordF_EQUIPMENT: TMemoField;
    adoqryRecordF_TECH_FICHE: TStringField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_ROOM_NAME: TStringField;
    adoqryRecordF_INFRASTRUCTURE_NAME: TStringField;
    adospSP_DEACTIVATE_CEN_ROOM: TFVBFFCStoredProc;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);

  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofCenRoom : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmCenRoom.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmCenRoom.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmCenRoom.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmCenRoom.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmCenRoom.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmCenRoom.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmCenRoom.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmCenRoom.GetTableName: String;
begin
  Result := 'T_CEN_ROOM';
end;

procedure TrdtmCenRoom.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_CEN_ROOM(' + DeltaDS.FieldByName('F_ROOM_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_CEN_ROOM.Close;
    adospSP_DEACTIVATE_CEN_ROOM.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_ROOM_ID').Value;
    adospSP_DEACTIVATE_CEN_ROOM.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmCenRoom.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofCenRoom := TComponentFactory.Create(ComServer, TrdtmCenRoom,
    Class_RdtmCenRoom, ciInternal, tmApartment);
end.
