inherited rdtmCenRoom: TrdtmCenRoom
  OldCreateOrder = True
  Height = 196
  Width = 378
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROOM_ID, '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_DESCRIPTION_NL, '
      '  F_DESCRIPTION_FR, '
      '  F_CUBIC_METRES, '
      '  F_HEIGHT, '
      '  F_EQUIPMENT, '
      '  F_TECH_FICHE, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_ROOM_NAME, '
      '  F_INFRASTRUCTURE_NAME '
      'FROM '
      '  V_CEN_ROOM')
    object adoqryListF_ROOM_ID: TIntegerField
      FieldName = 'F_ROOM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_DESCRIPTION_NL: TStringField
      FieldName = 'F_DESCRIPTION_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_DESCRIPTION_FR: TStringField
      FieldName = 'F_DESCRIPTION_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_CUBIC_METRES: TSmallintField
      FieldName = 'F_CUBIC_METRES'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_HEIGHT: TSmallintField
      FieldName = 'F_HEIGHT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EQUIPMENT: TMemoField
      FieldName = 'F_EQUIPMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_TECH_FICHE: TStringField
      FieldName = 'F_TECH_FICHE'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ROOM_NAME: TStringField
      FieldName = 'F_ROOM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROOM_ID, '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_DESCRIPTION_NL, '
      '  F_DESCRIPTION_FR, '
      '  F_CUBIC_METRES, '
      '  F_HEIGHT, '
      '  F_EQUIPMENT, '
      '  F_TECH_FICHE, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_ROOM_NAME, '
      '  F_INFRASTRUCTURE_NAME '
      'FROM '
      '  V_CEN_ROOM')
    object adoqryRecordF_ROOM_ID: TIntegerField
      FieldName = 'F_ROOM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_DESCRIPTION_NL: TStringField
      FieldName = 'F_DESCRIPTION_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_DESCRIPTION_FR: TStringField
      FieldName = 'F_DESCRIPTION_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CUBIC_METRES: TSmallintField
      FieldName = 'F_CUBIC_METRES'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_HEIGHT: TSmallintField
      FieldName = 'F_HEIGHT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EQUIPMENT: TMemoField
      FieldName = 'F_EQUIPMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_TECH_FICHE: TStringField
      FieldName = 'F_TECH_FICHE'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ROOM_NAME: TStringField
      FieldName = 'F_ROOM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_CEN_ROOM: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_CEN_ROOM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 248
    Top = 24
  end
end
