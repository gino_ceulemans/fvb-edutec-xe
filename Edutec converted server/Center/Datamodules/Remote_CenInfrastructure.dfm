inherited rdtmCenInfrastructure: TrdtmCenInfrastructure
  OldCreateOrder = True
  Height = 348
  Width = 485
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME, '
      '  F_FVB_CENTER_ID, '
      '  F_RSZ_NR, '
      '  F_VAT_NR, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_EMAIL, '
      '  F_WEBSITE, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME ,'
      '  F_ACC_NR, '
      '  F_ACC_HOLDER, '
      '  F_MEDIUM_ID,'
      '  F_MEDIUM_NAME, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_COMMENT, '
      '  F_SATURDAY, '
      '  F_EVENING, '
      '  F_WEEKDAY, '
      '  F_LOCATION_ID, '
      '  F_LOCATION, '
      '  F_PRECONDITION_ID, '
      '  F_PRECONDITION,'
      '  F_MAIL_ATTACHMENT '
      'FROM '
      '  V_CEN_INFRASTRUCTURE')
    object adoqryListF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_FVB_CENTER_ID: TIntegerField
      FieldName = 'F_FVB_CENTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object adoqryListF_VAT_NR: TStringField
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_WEBSITE: TStringField
      FieldName = 'F_WEBSITE'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object adoqryListF_ACC_HOLDER: TStringField
      FieldName = 'F_ACC_HOLDER'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_SATURDAY: TBooleanField
      FieldName = 'F_SATURDAY'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EVENING: TBooleanField
      FieldName = 'F_EVENING'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_WEEKDAY: TBooleanField
      FieldName = 'F_WEEKDAY'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LOCATION_ID: TIntegerField
      FieldName = 'F_LOCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LOCATION: TStringField
      FieldName = 'F_LOCATION'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PRECONDITION_ID: TIntegerField
      FieldName = 'F_PRECONDITION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRECONDITION: TStringField
      FieldName = 'F_PRECONDITION'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = [pfInUpdate]
      Size = 512
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME, '
      '  F_FVB_CENTER_ID, '
      '  F_RSZ_NR, '
      '  F_VAT_NR, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_EMAIL, '
      '  F_WEBSITE, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME ,'
      '  F_ACC_NR, '
      '  F_ACC_HOLDER, '
      '  F_MEDIUM_ID,'
      '  F_MEDIUM_NAME, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_COMMENT, '
      '  F_SATURDAY, '
      '  F_EVENING, '
      '  F_WEEKDAY, '
      '  F_LOCATION_ID, '
      '  F_LOCATION, '
      '  F_PRECONDITION_ID, '
      '  F_PRECONDITION,'
      '  F_CONFIRMATION_CONTACT1,'
      '  F_CONFIRMATION_CONTACT1_EMAIL,'
      '  F_CONFIRMATION_CONTACT2,'
      '  F_CONFIRMATION_CONTACT2_EMAIL,'
      '  F_CONFIRMATION_CONTACT3,'
      '  F_CONFIRMATION_CONTACT3_EMAIL,'
      '  F_INFO,'
      '  F_MAIL_ATTACHMENT'
      'FROM '
      '  V_CEN_INFRASTRUCTURE'
      '')
    object adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_FVB_CENTER_ID: TIntegerField
      FieldName = 'F_FVB_CENTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = []
      Visible = False
      Size = 14
    end
    object adoqryRecordF_VAT_NR: TStringField
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryRecordF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Visible = False
      Size = 128
    end
    object adoqryRecordF_WEBSITE: TStringField
      FieldName = 'F_WEBSITE'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object adoqryRecordF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object adoqryRecordF_ACC_HOLDER: TStringField
      FieldName = 'F_ACC_HOLDER'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object adoqryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = []
      Visible = False
    end
    object adoqryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_SATURDAY: TBooleanField
      FieldName = 'F_SATURDAY'
      ProviderFlags = []
      Visible = False
    end
    object adoqryRecordF_EVENING: TBooleanField
      FieldName = 'F_EVENING'
      ProviderFlags = []
      Visible = False
    end
    object adoqryRecordF_WEEKDAY: TBooleanField
      FieldName = 'F_WEEKDAY'
      ProviderFlags = []
      Visible = False
    end
    object adoqryRecordF_LOCATION_ID: TIntegerField
      FieldName = 'F_LOCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LOCATION: TStringField
      FieldName = 'F_LOCATION'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PRECONDITION_ID: TIntegerField
      FieldName = 'F_PRECONDITION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRECONDITION: TStringField
      FieldName = 'F_PRECONDITION'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CONFIRMATION_CONTACT3: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT3'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT3_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_INFO: TMemoField
      FieldName = 'F_INFO'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = [pfInUpdate]
      Size = 512
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_CEN_INFRASTRUCTURE: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_CEN_INFRASTRUCTURE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 232
    Top = 128
  end
end
