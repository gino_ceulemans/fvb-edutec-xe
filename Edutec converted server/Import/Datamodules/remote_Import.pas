unit remote_Import;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Remote_EduBase, Provider, Unit_FVBFFCDBComponents, DB, ADODB,
  EdutecServerD10_TLB, ActiveX, ComServ, ComObj, VCLCom, DataBkr, DBClient,
  StdVcl;

type
  TrdtmImport = class(TrdtmEduBase, IrdtmImport)
    adosp_IMPORTMANUAL: TFVBFFCStoredProc;
    adoqryListF_SESSION_START_DATE: TDateTimeField;
    adoqryListF_SESSION_END_DATE: TDateTimeField;
    adoqryListF_SESSION_DURATION_HOURS: TIntegerField;
    adoqryListF_COMPANY_NAME: TStringField;
    adoqryListF_COMPANY_PHONE: TStringField;
    adoqryListF_COMPANY_FAX: TStringField;
    adoqryListF_COMPANY_EMAIL: TStringField;
    adoqryListF_PERSON_DATEBIRTH: TDateTimeField;
    adoqryListF_IMPORT_ID: TIntegerField;
    adoqryListF_MSG: TStringField;
    adoqryListF_EDUCATION_DESCRIPTION: TStringField;
    adoqryListF_EDGARD_PROCESS_COMMENT: TStringField;
    adoqryListF_EDGARD_SESSION_ID: TIntegerField;
    adoqryListF_COMPANY_ADDRESS: TStringField;
    adoqryListF_PERSON_ADDRESS: TStringField;
    adoqryListF_PERSON_NAME: TStringField;
    adoqryListF_PERSON_FUNCTION: TStringField;
    adospGetSessionsForProgram: TFVBFFCStoredProc;
    prvGetSessionsForProgram: TFVBFFCDataSetProvider;
    adoqryListF_PERSON_CONSTRUCT_ID: TIntegerField;
    adoqryListF_PERSON_LASTNAME: TStringField;
    adoqryListF_PERSON_FIRSTNAME: TStringField;
    adoqryListF_EDGARD_PROCESS_STATUS: TStringField;
    adop_XmlEducationSetProcessed: TFVBFFCStoredProc;
    adop_UpdatePersonConstructId: TFVBFFCStoredProc;
//GCXE    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet; var TableName: String);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): WideString;
      safecall;
    procedure XmlEducationSetProcessed(AImportId: Integer; const AMsgType: WideString); safecall;

	procedure UpdatePersonConstructId(APersonId: Integer; AConstructId: Integer; AImportId: Integer; const AMsgType: WideString); safecall;

  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofImport : TComponentFactory;

implementation

uses Remote_EduMain;

{$R *.dfm}

{ TrdtmImport }

function TrdtmImport.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmImport.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmImport.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmImport.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

function TrdtmImport.GetTableName: String;
begin
  Result := 'V_XML_IMPORT_INFO';
end;

procedure TrdtmImport.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmImport.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmImport.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

class procedure TrdtmImport.UpdateRegistry(Register: Boolean;
  const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmImport.ProcessSingleRecord(EducationIdentity,
  SessionIdentity: Integer): WideString;
begin
  adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_EDUCATION_IDENTITY' ).Value := EducationIdentity;
  adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_SESSION_IDENTITY' ).Value := SessionIdentity;
//  adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_FLAG' ).Value := '';
  adosp_IMPORTMANUAL.ExecProc;
    if VarIsEmpty( adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_FLAG' ).Value ) then
      Result := ''
    else
      Result := adosp_IMPORTMANUAL.Parameters.ParamByName( '@F_FLAG' ).Value;
end;

procedure TrdtmImport.UpdatePersonConstructId(APersonId,
  AConstructId: Integer; AImportId: Integer; const AMsgType: WideString);
begin
  adop_UpdatePersonConstructId.Parameters.ParamByName('@APersonId').Value := APersonId;
  adop_UpdatePersonConstructId.Parameters.ParamByName('@AConstructId').Value := AConstructId;
  adop_UpdatePersonConstructId.Parameters.ParamByName('@AImportId').Value := AImportId;
  adop_UpdatePersonConstructId.Parameters.ParamByName('@AMsgType').Value := AMsgType;
  adop_UpdatePersonConstructId.ExecProc;
end;

procedure TrdtmImport.XmlEducationSetProcessed(AImportId: Integer;
  const AMsgType: WideString);
begin
  adop_XmlEducationSetProcessed.Parameters.ParamByName('@AImportId').Value := AImportId;
  adop_XmlEducationSetProcessed.Parameters.ParamByName('@AMsgType').Value := AMsgType;
  adop_XmlEducationSetProcessed.ExecProc;
end;

{GCXE
procedure TrdtmImport.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
    TableName := 'V_SYS_IMPORT_ERROR';
end;
}

initialization
  ofImport := TComponentFactory.Create(ComServer, TrdtmImport,
    Class_rdtmImport, ciInternal, tmApartment);

end.
