inherited rdtmImport: TrdtmImport
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'select *'
      'from V_SYS_IMPORT_ERROR'
      ''
      '---select * '
      '---from V_XML_IMPORT_MANUAL'
      ''
      '--select *'
      '--from V_XML_IMPORT_INFO'
      '--where F_FLAG = '#39'O'#39)
    object adoqryListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object adoqryListF_SESSION_DURATION_HOURS: TIntegerField
      FieldName = 'F_SESSION_DURATION_HOURS'
      ProviderFlags = []
    end
    object adoqryListF_COMPANY_NAME: TStringField
      FieldName = 'F_COMPANY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_COMPANY_PHONE: TStringField
      FieldName = 'F_COMPANY_PHONE'
      ProviderFlags = []
    end
    object adoqryListF_COMPANY_FAX: TStringField
      FieldName = 'F_COMPANY_FAX'
      ProviderFlags = []
    end
    object adoqryListF_COMPANY_EMAIL: TStringField
      FieldName = 'F_COMPANY_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_PERSON_DATEBIRTH: TDateTimeField
      FieldName = 'F_PERSON_DATEBIRTH'
      ProviderFlags = []
    end
    object adoqryListF_IMPORT_ID: TIntegerField
      FieldName = 'F_IMPORT_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryListF_MSG: TStringField
      FieldName = 'F_MSG'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
      Size = 3
    end
    object adoqryListF_EDUCATION_DESCRIPTION: TStringField
      FieldName = 'F_EDUCATION_DESCRIPTION'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_EDGARD_PROCESS_COMMENT: TStringField
      FieldName = 'F_EDGARD_PROCESS_COMMENT'
      ProviderFlags = []
      Size = 1024
    end
    object adoqryListF_EDGARD_SESSION_ID: TIntegerField
      FieldName = 'F_EDGARD_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COMPANY_ADDRESS: TStringField
      FieldName = 'F_COMPANY_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 294
    end
    object adoqryListF_PERSON_ADDRESS: TStringField
      FieldName = 'F_PERSON_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 282
    end
    object adoqryListF_PERSON_NAME: TStringField
      FieldName = 'F_PERSON_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 129
    end
    object adoqryListF_PERSON_FUNCTION: TStringField
      FieldName = 'F_PERSON_FUNCTION'
      ProviderFlags = []
      ReadOnly = True
      Size = 8
    end
    object adoqryListF_PERSON_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_PERSON_CONSTRUCT_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_EDGARD_PROCESS_STATUS: TStringField
      FieldName = 'F_EDGARD_PROCESS_STATUS'
      ProviderFlags = []
      Size = 1
    end
  end
  object adosp_IMPORTMANUAL: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XML_AUT_TOBEIMPORTED_MANUAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = -6
      end
      item
        Name = '@F_EDUCATION_IDENTITY'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@F_SESSION_IDENTITY'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@F_FLAG'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1
        Value = ' '
      end>
    AutoOpen = False
    Left = 256
    Top = 24
  end
  object adospGetSessionsForProgram: TFVBFFCStoredProc
    AutoCalcFields = False
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_GET_SESSIONS_FOR_PROGRAM;1'
    Parameters = <
      item
        Name = '@ATransfertId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AMsgType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@AStartDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    AutoOpen = False
    Left = 264
    Top = 96
  end
  object prvGetSessionsForProgram: TFVBFFCDataSetProvider
    DataSet = adospGetSessionsForProgram
    OnGetTableName = prvListGetTableName
    Left = 264
    Top = 144
  end
  object adop_XmlEducationSetProcessed: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_XML_EDUCATION_SET_PROCESSED;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@AImportId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AMsgType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    Prepared = True
    AutoOpen = False
    Left = 264
    Top = 256
  end
  object adop_UpdatePersonConstructId: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_UPDATE_PERSON_CONSTRUCTID;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@APersonId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AConstructId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AImportId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@AMsgType'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end>
    AutoOpen = False
    Left = 264
    Top = 208
  end
end
