inherited rdtmImportOverview: TrdtmImportOverview
  OldCreateOrder = True
  Height = 236
  Width = 367
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      #9'F_EDUCATION_IDENTITY,'
      #9'F_SENT,'
      #9'F_FLAG,'
      #9'F_FLAG_DESCRIPTION,'
      #9'F_ERROR_MESSAGE,'
      #9'F_HOURS_TOT,'
      #9'F_EXT_EDUCATION_ID,'
      #9'F_EDUCATION_DESCRIPTION,'
      #9'F_START_DATE,'
      #9'F_END_DATE,'
      #9'F_PERSON_IDENTITY,'
      #9'F_PERSON_FIRSTNAME,'
      #9'F_PERSON_LASTNAME,'
      #9'F_SOCSEC_NR,'
      #9'F_COMPANY_IDENTITY,'
      #9'F_COM_NAME'
      'FROM'
      '  V_XML_IMPORT_INFO2'
      '')
    object adoqryListF_EDUCATION_IDENTITY: TAutoIncField
      FieldName = 'F_EDUCATION_IDENTITY'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object adoqryListF_SENT: TBooleanField
      FieldName = 'F_SENT'
      ProviderFlags = []
    end
    object adoqryListF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object adoqryListF_FLAG_DESCRIPTION: TStringField
      FieldName = 'F_FLAG_DESCRIPTION'
      ProviderFlags = []
      Size = 32
    end
    object adoqryListF_ERROR_MESSAGE: TStringField
      FieldName = 'F_ERROR_MESSAGE'
      ProviderFlags = []
      Size = 256
    end
    object adoqryListF_HOURS_TOT: TIntegerField
      FieldName = 'F_HOURS_TOT'
      ProviderFlags = []
    end
    object adoqryListF_EXT_EDUCATION_ID: TIntegerField
      FieldName = 'F_EXT_EDUCATION_ID'
      ProviderFlags = []
    end
    object adoqryListF_EDUCATION_DESCRIPTION: TStringField
      FieldName = 'F_EDUCATION_DESCRIPTION'
      ProviderFlags = []
      Size = 75
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = []
    end
    object adoqryListF_PERSON_IDENTITY: TAutoIncField
      FieldName = 'F_PERSON_IDENTITY'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryListF_COMPANY_IDENTITY: TAutoIncField
      FieldName = 'F_COMPANY_IDENTITY'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_COM_NAME: TStringField
      FieldName = 'F_COM_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      #9'F_EDUCATION_IDENTITY,'
      #9'F_SENT,'
      #9'F_FLAG,'
      #9'F_FLAG_DESCRIPTION,'
      #9'F_ERROR_MESSAGE,'
      #9'F_HOURS_TOT,'
      #9'F_EXT_EDUCATION_ID,'
      #9'F_EDUCATION_DESCRIPTION,'
      #9'F_START_DATE,'
      #9'F_END_DATE,'
      #9'F_PERSON_IDENTITY,'
      #9'F_PERSON_FIRSTNAME,'
      #9'F_PERSON_LASTNAME,'
      #9'F_SOCSEC_NR,'
      #9'F_COMPANY_IDENTITY,'
      #9'F_COM_NAME'
      'FROM'
      '  V_XML_IMPORT_INFO2'
      '')
    object adoqryRecordF_EDUCATION_IDENTITY: TAutoIncField
      FieldName = 'F_EDUCATION_IDENTITY'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object adoqryRecordF_SENT: TBooleanField
      FieldName = 'F_SENT'
      ProviderFlags = []
    end
    object adoqryRecordF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object adoqryRecordF_FLAG_DESCRIPTION: TStringField
      FieldName = 'F_FLAG_DESCRIPTION'
      ProviderFlags = []
      ReadOnly = True
      Size = 32
    end
    object adoqryRecordF_ERROR_MESSAGE: TStringField
      FieldName = 'F_ERROR_MESSAGE'
      ProviderFlags = []
      Size = 256
    end
    object adoqryRecordF_HOURS_TOT: TIntegerField
      FieldName = 'F_HOURS_TOT'
      ProviderFlags = []
    end
    object adoqryRecordF_EXT_EDUCATION_ID: TIntegerField
      FieldName = 'F_EXT_EDUCATION_ID'
      ProviderFlags = []
    end
    object adoqryRecordF_EDUCATION_DESCRIPTION: TStringField
      FieldName = 'F_EDUCATION_DESCRIPTION'
      ProviderFlags = []
      Size = 75
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_PERSON_IDENTITY: TAutoIncField
      FieldName = 'F_PERSON_IDENTITY'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryRecordF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryRecordF_COMPANY_IDENTITY: TAutoIncField
      FieldName = 'F_COMPANY_IDENTITY'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryRecordF_COM_NAME: TStringField
      FieldName = 'F_COM_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  object stprToBeImported: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_XML_AUT_TOBEIMPORTED;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_EDUCATION_IDENTITY'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_FLAG'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1
        Value = Null
      end>
    AutoOpen = False
    Left = 256
    Top = 24
  end
  object adoqryStatus: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select '#9'F_FLAG,'
      #9'F_DESCRIPTION'
      'from '#9'V_XML_STATUS_LIST')
    AutoOpen = False
    Left = 256
    Top = 80
    object adoqryStatusF_FLAG: TStringField
      FieldName = 'F_FLAG'
      FixedChar = True
      Size = 1
    end
    object adoqryStatusF_DESCRIPTION: TStringField
      FieldName = 'F_DESCRIPTION'
      Size = 32
    end
  end
  object prvStatus: TFVBFFCDataSetProvider
    DataSet = adoqryStatus
    Left = 256
    Top = 144
  end
  object adoqryGetStatusDescription: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    Parameters = <
      item
        Name = '@F_FLAG'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select dbo.UDF_GET_DESCRIPTION_OF_STATUS (:@F_FLAG)')
    AutoOpen = False
    Left = 64
    Top = 144
  end
end
