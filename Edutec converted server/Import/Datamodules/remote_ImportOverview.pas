unit remote_ImportOverview;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX, dialogs;

type
  TrdtmImportOverview = class(TrdtmEduBase, IrdtmImportOverview)
    stprToBeImported: TFVBFFCStoredProc;
    adoqryListF_EDUCATION_IDENTITY: TAutoIncField;
    adoqryListF_SENT: TBooleanField;
    adoqryListF_FLAG: TStringField;
    adoqryListF_FLAG_DESCRIPTION: TStringField;
    adoqryListF_ERROR_MESSAGE: TStringField;
    adoqryListF_HOURS_TOT: TIntegerField;
    adoqryListF_EXT_EDUCATION_ID: TIntegerField;
    adoqryListF_EDUCATION_DESCRIPTION: TStringField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_PERSON_IDENTITY: TAutoIncField;
    adoqryListF_PERSON_FIRSTNAME: TStringField;
    adoqryListF_PERSON_LASTNAME: TStringField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_COMPANY_IDENTITY: TAutoIncField;
    adoqryListF_COM_NAME: TStringField;
    adoqryRecordF_EDUCATION_IDENTITY: TAutoIncField;
    adoqryRecordF_SENT: TBooleanField;
    adoqryRecordF_FLAG: TStringField;
    adoqryRecordF_FLAG_DESCRIPTION: TStringField;
    adoqryRecordF_ERROR_MESSAGE: TStringField;
    adoqryRecordF_HOURS_TOT: TIntegerField;
    adoqryRecordF_EXT_EDUCATION_ID: TIntegerField;
    adoqryRecordF_EDUCATION_DESCRIPTION: TStringField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_PERSON_IDENTITY: TAutoIncField;
    adoqryRecordF_PERSON_FIRSTNAME: TStringField;
    adoqryRecordF_PERSON_LASTNAME: TStringField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_COMPANY_IDENTITY: TAutoIncField;
    adoqryRecordF_COM_NAME: TStringField;
    adoqryStatus: TFVBFFCQuery;
    adoqryStatusF_FLAG: TStringField;
    adoqryStatusF_DESCRIPTION: TStringField;
    prvStatus: TFVBFFCDataSetProvider;
    adoqryGetStatusDescription: TFVBFFCQuery;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName: string; override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      // safecall;
    function ProcessSingleRecord(EducationIdentity: Integer): WideString;
      safecall;
    function GetDescriptionForStatus(const Status: WideString): WideString;
      safecall;
    { Public declarations }
  end;

var
  ofImportOverview: TComponentFactory;


implementation

uses Remote_EduMain, Variants;

{$R *.DFM}

class procedure TrdtmImportOverview.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmImportOverview.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  result := inherited ApplyDetailFilter (aProvider, aFilter);
end;

function TrdtmImportOverview.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  result := inherited ApplyFilter (aProvider, aFilter);
end;

function TrdtmImportOverview.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  result := inherited ApplyOrderBy (aProvider, aOrderBy);
end;

function TrdtmImportOverview.GetNewRecordID: SYSINT;
begin
  result := inherited GetNewRecordID;
end;

function TrdtmImportOverview.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin
  result := inherited SetSQLStatement (aProvider, aSQL);
end;

procedure TrdtmImportOverview.Set_ADOConnection(Param1: Integer);
begin
  inherited Set_ADOConnection (Param1);
end;

procedure TrdtmImportOverview.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  inherited Set_rdtmEDUMain (Param1);
end;

function TrdtmImportOverview.GetTableName: string;
begin
  result := 'T_XML_EDUCATION';
end;

{*****************************************************************************
  Processes a single T_XML_EDUCATION record and returns the status of this record
  afterwards
  @Name       TrdtmImportOverview.ProcessSingleRecord
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmImportOverview.ProcessSingleRecord(
  EducationIdentity: Integer): WideString;
begin
  with stprToBeImported do
  begin
    Parameters.ParamByName('@F_EDUCATION_IDENTITY').Value := EducationIdentity;
    ExecProc;
    if VarIsEmpty (Parameters.ParamByName('@F_FLAG').Value) then
      result := ''
    else
      result := Parameters.ParamByName('@F_FLAG').Value;
  end;

end;

function TrdtmImportOverview.GetDescriptionForStatus(
  const Status: WideString): WideString;
begin
  adoqryGetStatusDescription.Close;
  adoqryGetStatusDescription.Parameters.ParamByName('@F_FLAG').Value := Status;
  adoqryGetStatusDescription.open;
  result := adoqryGetStatusDescription.Fields[0].AsString;
  adoqryGetStatusDescription.Close;
end;

initialization
  ofImportOverview := TComponentFactory.Create(ComServer, TrdtmImportOverview,
    Class_rdtmImportOverview, ciInternal, tmApartment);
end.
