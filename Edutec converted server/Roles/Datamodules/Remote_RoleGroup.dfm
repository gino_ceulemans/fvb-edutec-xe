inherited rdtmRoleGroup: TrdtmRoleGroup
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROLEGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ROLEGROUP_NAME '
      'FROM '
      '  V_RO_GROUP'
      '')
    object adoqryListF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROLEGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ROLEGROUP_NAME '
      'FROM '
      '  V_RO_GROUP'
      '')
    object adoqryRecordF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
