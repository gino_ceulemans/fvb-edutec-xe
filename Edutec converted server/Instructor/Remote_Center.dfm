inherited rdtmDocCenter: TrdtmDocCenter
  OldCreateOrder = True
  Height = 189
  Width = 312
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '[F_SES_INSTRUCTOR_ID], '
      '[F_INSTRUCTOR_ID], '
      '[F_SOCSEC_NR], '
      '[F_PHONE], '
      '[F_FAX], '
      '[F_GSM], '
      '[F_EMAIL], '
      '[F_SESSION_ID], '
      '[F_NAME], '
      '[F_START_DATE], '
      '[F_END_DATE], '
      '[F_FULLNAME] ,'
      '[F_PROGRAM_ID],'
      '[F_CODE]'
      'FROM '
      '[dbo].[V_SES_INSTRUCTOR]')
    object adoqryListF_SES_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_SES_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object adoqryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object adoqryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = []
    end
    object adoqryListF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = []
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '[F_SES_INSTRUCTOR_ID], '
      '[F_INSTRUCTOR_ID], '
      '[F_SOCSEC_NR], '
      '[F_PHONE], '
      '[F_FAX], '
      '[F_GSM], '
      '[F_EMAIL], '
      '[F_SESSION_ID], '
      '[F_NAME], '
      '[F_START_DATE], '
      '[F_END_DATE], '
      '[F_FULLNAME] ,'
      '[F_PROGRAM_ID]'
      'FROM '
      '[dbo].[V_SES_INSTRUCTOR]')
    object adoqryRecordF_SES_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_SES_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object adoqryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object adoqryRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = []
    end
  end
end
