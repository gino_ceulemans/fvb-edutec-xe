inherited rdtmInstructor: TrdtmInstructor
  OldCreateOrder = True
  Height = 190
  Width = 485
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INSTRUCTOR_ID, '
      '  F_FULLNAME,'
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_ACC_NR,'
      '  F_SOCSEC_NR,'
      '  F_PHONE,'
      '  F_FAX,'
      '  F_EMAIL,'
      '  F_GSM,'
      '  F_ACTIVE,'
      '  F_BTW,'
      '  F_ORGANISATION_NAME,'
      '  F_CONFIRMATION_CONTACT1,'
      '  F_CONFIRMATION_CONTACT1_EMAIL,'
      '  F_CONFIRMATION_CONTACT2,'
      '  F_CONFIRMATION_CONTACT2_EMAIL'
      'FROM '
      '  V_DOC_INSTRUCTOR'
      '')
    object adoqryListF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_BTW: TStringField
      FieldName = 'F_BTW'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ORGANISATION_NAME: TStringField
      FieldName = 'F_ORGANISATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryListF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INSTRUCTOR_ID, '
      '  F_TITLE_ID, '
      '  F_TITLE_NAME, '
      '  F_FULLNAME,'
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME, '
      '  F_MEDIUM_ID, '
      '  F_MEDIUM_NAME, '
      '  F_ACC_NR,'
      '  F_FOREIGN_ACC_NR, '
      '  F_SOCSEC_NR, '
      '  F_DATEBIRTH, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_DUTCH, '
      '  F_FRENCH, '
      '  F_ENGLISH, '
      '  F_GERMAN, '
      '  F_OTHER_LANGUAGE, '
      '  F_DIPLOMA_ID, '
      '  F_DIPLOMA_NAME, '
      '  F_COMMENT, '
      '  F_CODE, '
      '  F_CV_ID, '
      '  F_DOCUMENT_NAME,'
      '  [F_BTW],'
      '  F_ORGANISATION_ID,'
      '  F_ORGANISATION_NAME,'
      '  F_CONFIRMATION_CONTACT1,'
      '  F_CONFIRMATION_CONTACT1_EMAIL,'
      '  F_CONFIRMATION_CONTACT2,'
      '  F_CONFIRMATION_CONTACT2_EMAIL'
      'FROM'
      '  V_DOC_INSTRUCTOR')
    object adoqryRecordF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryRecordF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
    end
    object adoqryRecordF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ReadOnly = True
      Size = 64
    end
    object adoqryRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ReadOnly = True
      Size = 130
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object adoqryRecordF_STREET: TStringField
      FieldName = 'F_STREET'
      Size = 128
    end
    object adoqryRecordF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object adoqryRecordF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object adoqryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
    end
    object adoqryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object adoqryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
    object adoqryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
    end
    object adoqryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ReadOnly = True
      Size = 64
    end
    object adoqryRecordF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      Size = 14
    end
    object adoqryRecordF_FOREIGN_ACC_NR: TStringField
      FieldName = 'F_FOREIGN_ACC_NR'
      Size = 30
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object adoqryRecordF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object adoqryRecordF_DUTCH: TBooleanField
      FieldName = 'F_DUTCH'
    end
    object adoqryRecordF_FRENCH: TBooleanField
      FieldName = 'F_FRENCH'
    end
    object adoqryRecordF_ENGLISH: TBooleanField
      FieldName = 'F_ENGLISH'
    end
    object adoqryRecordF_GERMAN: TBooleanField
      FieldName = 'F_GERMAN'
    end
    object adoqryRecordF_OTHER_LANGUAGE: TStringField
      FieldName = 'F_OTHER_LANGUAGE'
      Size = 50
    end
    object adoqryRecordF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
    end
    object adoqryRecordF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ReadOnly = True
      Size = 64
    end
    object adoqryRecordF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      BlobType = ftMemo
    end
    object adoqryRecordF_CODE: TStringField
      FieldName = 'F_CODE'
    end
    object adoqryRecordF_CV_ID: TIntegerField
      FieldName = 'F_CV_ID'
    end
    object adoqryRecordF_DOCUMENT_NAME: TStringField
      FieldName = 'F_DOCUMENT_NAME'
      Size = 64
    end
    object adoqryRecordF_BTW: TStringField
      FieldName = 'F_BTW'
    end
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ORGANISATION_NAME: TStringField
      FieldName = 'F_ORGANISATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_DOC_INSTRUCTOR: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_DOC_INSTRUCTOR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 280
    Top = 40
  end
end
