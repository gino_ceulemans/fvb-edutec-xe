unit Remote_Center;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmDocCenter = class(TrdtmEduBase, IrdtmDocCenter)
    adoqryListF_SES_INSTRUCTOR_ID: TIntegerField;
    adoqryListF_INSTRUCTOR_ID: TIntegerField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_GSM: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_FULLNAME: TStringField;
    adoqryRecordF_SES_INSTRUCTOR_ID: TIntegerField;
    adoqryRecordF_INSTRUCTOR_ID: TIntegerField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_GSM: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_FULLNAME: TStringField;
    adoqryRecordF_PROGRAM_ID: TIntegerField;
    adoqryListF_PROGRAM_ID: TIntegerField;
    adoqryListF_CODE: TStringField;
  private
    { Private declarations }

  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;

    { Public declarations }

  end;

var
  ofDocCenter : TComponentFactory;

implementation

uses Remote_EduMain,Unit_FVBFFCInterfaces;

{$R *.DFM}

class procedure TrdtmDocCenter.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmDocCenter.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmDocCenter.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmDocCenter.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmDocCenter.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmDocCenter.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmDocCenter.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmDocCenter.GetTableName: String;
begin
  Result := 'T_SES_INSTRUCTOR';
end;

function TrdtmDocCenter.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofDocCenter := TComponentFactory.Create(ComServer, TrdtmDocCenter,
    Class_rdtmDocCenter, ciInternal, tmApartment);
end.
