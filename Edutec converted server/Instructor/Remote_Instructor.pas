{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_DOC_INSTRUCTOR.

  @Name       Remote_Instructor
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  21/10/2005   sLesage              Fixed the Provider Flags on some Lookup
                                    Fields.
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Instructor;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmInstructor = class(TrdtmEduBase, IrdtmInstructor)
    adoqryListF_INSTRUCTOR_ID: TIntegerField;
    adoqryListF_FULLNAME: TStringField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_STREET: TStringField;
    adoqryListF_NUMBER: TStringField;
    adoqryListF_MAILBOX: TStringField;
    adoqryListF_POSTALCODE_ID: TIntegerField;
    adoqryListF_POSTALCODE: TStringField;
    adoqryListF_CITY_NAME: TStringField;
    adoqryListF_LANGUAGE_ID: TIntegerField;
    adoqryListF_LANGUAGE_NAME: TStringField;
    adoqryListF_ACC_NR: TStringField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryListF_GSM: TStringField;
    adospSP_DEACTIVATE_DOC_INSTRUCTOR: TFVBFFCStoredProc;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryListF_BTW: TStringField;
    adoqryRecordF_INSTRUCTOR_ID: TIntegerField;
    adoqryRecordF_TITLE_ID: TIntegerField;
    adoqryRecordF_TITLE_NAME: TStringField;
    adoqryRecordF_FULLNAME: TStringField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_STREET: TStringField;
    adoqryRecordF_NUMBER: TStringField;
    adoqryRecordF_MAILBOX: TStringField;
    adoqryRecordF_POSTALCODE_ID: TIntegerField;
    adoqryRecordF_POSTALCODE: TStringField;
    adoqryRecordF_CITY_NAME: TStringField;
    adoqryRecordF_COUNTRY_ID: TIntegerField;
    adoqryRecordF_COUNTRY_NAME: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_GSM: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_GENDER_ID: TIntegerField;
    adoqryRecordF_GENDER_NAME: TStringField;
    adoqryRecordF_MEDIUM_ID: TIntegerField;
    adoqryRecordF_MEDIUM_NAME: TStringField;
    adoqryRecordF_ACC_NR: TStringField;
    adoqryRecordF_FOREIGN_ACC_NR: TStringField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_DATEBIRTH: TDateTimeField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_DUTCH: TBooleanField;
    adoqryRecordF_FRENCH: TBooleanField;
    adoqryRecordF_ENGLISH: TBooleanField;
    adoqryRecordF_GERMAN: TBooleanField;
    adoqryRecordF_OTHER_LANGUAGE: TStringField;
    adoqryRecordF_DIPLOMA_ID: TIntegerField;
    adoqryRecordF_DIPLOMA_NAME: TStringField;
    adoqryRecordF_COMMENT: TMemoField;
    adoqryRecordF_CODE: TStringField;
    adoqryRecordF_CV_ID: TIntegerField;
    adoqryRecordF_DOCUMENT_NAME: TStringField;
    adoqryRecordF_BTW: TStringField;
    adoqryListF_ORGANISATION_NAME: TStringField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_ORGANISATION_NAME: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT1: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT2: TStringField;
    adoqryRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    adoqryListF_CONFIRMATION_CONTACT1: TStringField;
    adoqryListF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    adoqryListF_CONFIRMATION_CONTACT2: TStringField;
    adoqryListF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);

  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofInstructor : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmInstructor.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmInstructor.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmInstructor.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmInstructor.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmInstructor.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmInstructor.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmInstructor.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmInstructor.GetTableName: String;
begin
  Result := 'T_DOC_INSTRUCTOR';
end;

procedure TrdtmInstructor.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_DOC_INSTRUCTOR(' + DeltaDS.FieldByName('F_INSTRUCTOR_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_DOC_INSTRUCTOR.Close;
    adospSP_DEACTIVATE_DOC_INSTRUCTOR.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_INSTRUCTOR_ID').Value;
    adospSP_DEACTIVATE_DOC_INSTRUCTOR.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmInstructor.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofInstructor := TComponentFactory.Create(ComServer, TrdtmInstructor,
    Class_rdtmInstructor, ciInternal, tmApartment);
end.
