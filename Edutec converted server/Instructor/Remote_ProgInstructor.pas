{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_PROG_INSTRUCTOR.

  @Name       Remote_ProgInstructor
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_ProgInstructor;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmProgInstructor = class(TrdtmEduBase, IrdtmProgInstructor)
    adoqryListF_PROG_INSTRUCTOR_ID: TIntegerField;
    adoqryListF_INSTRUCTOR_ID: TIntegerField;
    adoqryListF_FULLNAME: TStringField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_PHONE: TStringField;
    adoqryListF_FAX: TStringField;
    adoqryListF_GSM: TStringField;
    adoqryListF_EMAIL: TStringField;
    adoqryListF_PROGRAM_ID: TIntegerField;
    adoqryListF_NAME: TStringField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_ACTIVE: TBooleanField;
    adoqryRecordF_PROG_INSTRUCTOR_ID: TIntegerField;
    adoqryRecordF_INSTRUCTOR_ID: TIntegerField;
    adoqryRecordF_FULLNAME: TStringField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_PHONE: TStringField;
    adoqryRecordF_FAX: TStringField;
    adoqryRecordF_GSM: TStringField;
    adoqryRecordF_EMAIL: TStringField;
    adoqryRecordF_PROGRAM_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_ACTIVE: TBooleanField;
    adospSP_DEACTIVATE_PROG_INSTRUCTOR: TFVBFFCStoredProc;
    procedure prvBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofProgInstructor : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmProgInstructor.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmProgInstructor.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmProgInstructor.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmProgInstructor.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmProgInstructor.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmProgInstructor.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmProgInstructor.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmProgInstructor.GetTableName: String;
begin
  Result := 'T_PROG_INSTRUCTOR';
end;

procedure TrdtmProgInstructor.prvBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
  UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Inherited;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}
  inherited;

  if ( UpdateKind = ukDelete ) then
  begin
    // Voer de stored proc uit
    {$IFDEF CODESITE}
    csFVBFFCDataModule.SendMsg( 'SP_DEACTIVATE_PROG_INSTRUCTOR(' + DeltaDS.FieldByName('F_PROG_INSTRUCTOR_ID').AsString + ')' );
    {$ENDIF}

    adospSP_DEACTIVATE_PROG_INSTRUCTOR.Close;
    adospSP_DEACTIVATE_PROG_INSTRUCTOR.Parameters.ParamByName('@Id').Value := DeltaDS.FieldByName('F_PROG_INSTRUCTOR_ID').Value;
    adospSP_DEACTIVATE_PROG_INSTRUCTOR.ExecProc;
    // Zorg ervoor dat er wordt doorgegeven dat alles al is verwerkt ...
    Applied := True
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'prvBeforeUpdateRecord' );
  {$ENDIF}

end;

function TrdtmProgInstructor.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofProgInstructor := TComponentFactory.Create(ComServer, TrdtmProgInstructor,
    Class_rdtmProgInstructor, ciInternal, tmApartment);
end.
