inherited rdtmProgInstructor: TrdtmProgInstructor
  OldCreateOrder = True
  Height = 184
  Width = 238
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROG_INSTRUCTOR_ID, '
      '  F_INSTRUCTOR_ID, '
      '  F_FULLNAME,'
      '  F_LASTNAME,'
      '  F_FIRSTNAME,'
      '  F_SOCSEC_NR, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_PROGRAM_ID, '
      '  F_NAME, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM '
      '  V_PROG_INSTRUCTOR')
    object adoqryListF_PROG_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_PROG_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object adoqryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object adoqryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object adoqryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROG_INSTRUCTOR_ID, '
      '  F_INSTRUCTOR_ID, '
      '  F_FULLNAME,'
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_SOCSEC_NR, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_PROGRAM_ID, '
      '  F_NAME, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM '
      '  V_PROG_INSTRUCTOR'
      '')
    object adoqryRecordF_PROG_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_PROG_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object adoqryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object adoqryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object adoqryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    BeforeUpdateRecord = prvBeforeUpdateRecord
  end
  object adospSP_DEACTIVATE_PROG_INSTRUCTOR: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_DEACTIVATE_PROG_INSTRUCTOR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 248
    Top = 24
  end
end
