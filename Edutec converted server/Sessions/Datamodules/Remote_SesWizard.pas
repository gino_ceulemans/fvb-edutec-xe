{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_SYS_SESWIZARD.

  @Name       Remote_SesWizard
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_SesWizard;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmSesWizard = class(TrdtmEduBase, IrdtmSesWizard)
    adospSES_WIZ_CLEAR: TFVBFFCStoredProc;
    adospSES_WIZ_EXECUTE: TFVBFFCStoredProc;
    adospSES_WIZ_FORECAST: TFVBFFCStoredProc;
    prvSES_WIZ_CLEAR: TFVBFFCDataSetProvider;
    prvSES_WIZ_EXECUTE: TFVBFFCDataSetProvider;
    prvSES_WIZ_FORECAST: TFVBFFCDataSetProvider;
    adoqrySES_WIZ_GET_PROG: TFVBFFCQuery;
    prvSES_WIZ_GET_PROG: TFVBFFCDataSetProvider;
    adoqryListF_SESWIZARD_ID: TIntegerField;
    adoqryListF_SPID: TIntegerField;
    adoqryListF_EXTERN_NR: TStringField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_COMPANY_NAME: TStringField;
    adoqryListF_PERSON_ID: TIntegerField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_PROGRAM_ID: TIntegerField;
    adoqryListF_PROGRAM_NAME: TStringField;
    adoqryListF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryListF_INFRASTRUCTURE_NAME: TStringField;
    adoqryListF_SESSION_NAME: TStringField;
    adoqryListF_SESSION_CODE: TStringField;
    adoqryListF_SESSION_TRANSPORT_INS: TStringField;
    adoqryListF_SESSION_START_DATE: TDateTimeField;
    adoqryListF_SESSION_END_DATE: TDateTimeField;
    adoqryListF_SESSION_COMMENT: TStringField;
    adoqryListF_SESSION_START_HOUR: TStringField;
    adoqryListF_ENROL_ID: TIntegerField;
    adoqryListF_ENROL_EVALUATION: TBooleanField;
    adoqryListF_ENROL_CERTIFICATE: TBooleanField;
    adoqryListF_ENROL_COMMENT: TStringField;
    adoqryListF_ENROL_INVOICED: TBooleanField;
    adoqryListF_ENROL_LAST_MINUTE: TBooleanField;
    adoqryListF_ENROL_HOURS: TIntegerField;
    adoqryListF_SES_CREATE: TBooleanField;
    adoqryListF_SES_UPDATE: TBooleanField;
    adoqryListF_ENR_CREATE: TBooleanField;
    adoqryListF_ENR_UPDATE: TBooleanField;
    adoqryListF_FORECAST_COMMENT: TStringField;
    adoqryRecordF_SESWIZARD_ID: TIntegerField;
    adoqryRecordF_SPID: TIntegerField;
    adoqryRecordF_EXTERN_NR: TStringField;
    adoqryRecordF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_COMPANY_NAME: TStringField;
    adoqryRecordF_PERSON_ID: TIntegerField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_PROGRAM_ID: TIntegerField;
    adoqryRecordF_PROGRAM_NAME: TStringField;
    adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryRecordF_INFRASTRUCTURE_NAME: TStringField;
    adoqryRecordF_SESSION_NAME: TStringField;
    adoqryRecordF_SESSION_CODE: TStringField;
    adoqryRecordF_SESSION_TRANSPORT_INS: TStringField;
    adoqryRecordF_SESSION_START_DATE: TDateTimeField;
    adoqryRecordF_SESSION_END_DATE: TDateTimeField;
    adoqryRecordF_SESSION_COMMENT: TStringField;
    adoqryRecordF_SESSION_START_HOUR: TStringField;
    adoqryRecordF_ENROL_ID: TIntegerField;
    adoqryRecordF_ENROL_EVALUATION: TBooleanField;
    adoqryRecordF_ENROL_CERTIFICATE: TBooleanField;
    adoqryRecordF_ENROL_COMMENT: TStringField;
    adoqryRecordF_ENROL_INVOICED: TBooleanField;
    adoqryRecordF_ENROL_LAST_MINUTE: TBooleanField;
    adoqryRecordF_ENROL_HOURS: TIntegerField;
    adoqryRecordF_SES_CREATE: TBooleanField;
    adoqryRecordF_SES_UPDATE: TBooleanField;
    adoqryRecordF_ENR_CREATE: TBooleanField;
    adoqryRecordF_ENR_UPDATE: TBooleanField;
    adoqryRecordF_FORECAST_COMMENT: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofSesWizard : TComponentFactory;
  
implementation

{$R *.DFM}

class procedure TrdtmSesWizard.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmSesWizard.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmSesWizard.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmSesWizard.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmSesWizard.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmSesWizard.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmSesWizard.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmSesWizard.GetTableName: String;
begin
  Result := 'T_SYS_SESWIZARD';
end;

function TrdtmSesWizard.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofSesWizard := TComponentFactory.Create(ComServer, TrdtmSesWizard,
    Class_rdtmSesWizard, ciInternal, tmApartment);
end.
