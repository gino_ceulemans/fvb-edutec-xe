inherited rdtmSesWizard: TrdtmSesWizard
  OldCreateOrder = True
  Height = 316
  Width = 785
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  T_SYS_SESWIZARD'
      'WHERE'
      '  ( F_SPID = @@SPID )')
    object adoqryListF_SESWIZARD_ID: TIntegerField
      FieldName = 'F_SESWIZARD_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_SPID: TIntegerField
      FieldName = 'F_SPID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EXTERN_NR: TStringField
      FieldName = 'F_EXTERN_NR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COMPANY_NAME: TStringField
      FieldName = 'F_COMPANY_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROGRAM_NAME: TStringField
      FieldName = 'F_PROGRAM_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_TRANSPORT_INS: TStringField
      FieldName = 'F_SESSION_TRANSPORT_INS'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_COMMENT: TStringField
      FieldName = 'F_SESSION_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object adoqryListF_SESSION_START_HOUR: TStringField
      FieldName = 'F_SESSION_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROL_EVALUATION: TBooleanField
      FieldName = 'F_ENROL_EVALUATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROL_CERTIFICATE: TBooleanField
      FieldName = 'F_ENROL_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROL_COMMENT: TStringField
      FieldName = 'F_ENROL_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object adoqryListF_ENROL_INVOICED: TBooleanField
      FieldName = 'F_ENROL_INVOICED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROL_LAST_MINUTE: TBooleanField
      FieldName = 'F_ENROL_LAST_MINUTE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROL_HOURS: TIntegerField
      FieldName = 'F_ENROL_HOURS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SES_CREATE: TBooleanField
      FieldName = 'F_SES_CREATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SES_UPDATE: TBooleanField
      FieldName = 'F_SES_UPDATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENR_CREATE: TBooleanField
      FieldName = 'F_ENR_CREATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENR_UPDATE: TBooleanField
      FieldName = 'F_ENR_UPDATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_FORECAST_COMMENT: TStringField
      FieldName = 'F_FORECAST_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  T_SYS_SESWIZARD'
      'WHERE'
      '  ( F_SPID = @@SPID )')
    object adoqryRecordF_SESWIZARD_ID: TIntegerField
      FieldName = 'F_SESWIZARD_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_SPID: TIntegerField
      FieldName = 'F_SPID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EXTERN_NR: TStringField
      FieldName = 'F_EXTERN_NR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COMPANY_NAME: TStringField
      FieldName = 'F_COMPANY_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROGRAM_NAME: TStringField
      FieldName = 'F_PROGRAM_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_TRANSPORT_INS: TStringField
      FieldName = 'F_SESSION_TRANSPORT_INS'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_COMMENT: TStringField
      FieldName = 'F_SESSION_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object adoqryRecordF_SESSION_START_HOUR: TStringField
      FieldName = 'F_SESSION_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENROL_EVALUATION: TBooleanField
      FieldName = 'F_ENROL_EVALUATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENROL_CERTIFICATE: TBooleanField
      FieldName = 'F_ENROL_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENROL_COMMENT: TStringField
      FieldName = 'F_ENROL_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
    object adoqryRecordF_ENROL_INVOICED: TBooleanField
      FieldName = 'F_ENROL_INVOICED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENROL_LAST_MINUTE: TBooleanField
      FieldName = 'F_ENROL_LAST_MINUTE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENROL_HOURS: TIntegerField
      FieldName = 'F_ENROL_HOURS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SES_CREATE: TBooleanField
      FieldName = 'F_SES_CREATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SES_UPDATE: TBooleanField
      FieldName = 'F_SES_UPDATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENR_CREATE: TBooleanField
      FieldName = 'F_ENR_CREATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ENR_UPDATE: TBooleanField
      FieldName = 'F_ENR_UPDATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FORECAST_COMMENT: TStringField
      FieldName = 'F_FORECAST_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 250
    end
  end
  object adospSES_WIZ_CLEAR: TFVBFFCStoredProc
    Connection = dtmEDUMainClient.adocnnMain
    ProcedureName = 'SP_SYS_SESWIZARD_EMPTY;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    AutoOpen = False
    Left = 520
    Top = 24
  end
  object adospSES_WIZ_EXECUTE: TFVBFFCStoredProc
    Connection = dtmEDUMainClient.adocnnMain
    ProcedureName = 'SP_SYS_SESWIZARD_EXECUTE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    AutoOpen = False
    Left = 520
    Top = 80
  end
  object adospSES_WIZ_FORECAST: TFVBFFCStoredProc
    Connection = dtmEDUMainClient.adocnnMain
    ProcedureName = 'SP_SYS_SESWIZARD_FORECAST;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end>
    AutoOpen = False
    Left = 520
    Top = 136
  end
  object prvSES_WIZ_CLEAR: TFVBFFCDataSetProvider
    DataSet = adospSES_WIZ_CLEAR
    Left = 664
    Top = 24
  end
  object prvSES_WIZ_EXECUTE: TFVBFFCDataSetProvider
    DataSet = adospSES_WIZ_EXECUTE
    Left = 664
    Top = 80
  end
  object prvSES_WIZ_FORECAST: TFVBFFCDataSetProvider
    DataSet = adospSES_WIZ_FORECAST
    Left = 664
    Top = 136
  end
  object adoqrySES_WIZ_GET_PROG: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'F_PROGRAM_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM'
      '  V_PROG_PROGRAM'
      'WHERE'
      '  F_PROGRAM_ID = :F_PROGRAM_ID')
    AutoOpen = False
    Left = 520
    Top = 192
  end
  object prvSES_WIZ_GET_PROG: TFVBFFCDataSetProvider
    DataSet = adoqrySES_WIZ_GET_PROG
    Left = 664
    Top = 192
  end
end
