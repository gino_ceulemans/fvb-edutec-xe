{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  confirmation reports

  @Name       Remote_Confirmation_Reports
  @Author     Ivan Van den Bossche
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/04/2008  ivdbossche            Added MarkConfrepSent
  07/01/2008  ivdbossche            Added GetStartdateFromSessionId, GetMailingSignature
  17/12/2007  ivdbossche            Added UpdateContactCompany & UpdateContactInfrastruct
  22/10/2007  ivdbossche            Added function GetSessionGroupIdFromSessionId
  JUL-2007  ivdbossche              Initial Creation
******************************************************************************}

unit Remote_Confirmation_Reports;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Remote_EduBase, ADODB, Unit_FVBFFCDBComponents, DB, Provider,
  VCLCom, ComServ, ComObj, ActiveX, DataBkr, EdutecServerD10_TLB;

type
  TrdtmConfirmationReports = class(TrdtmEduBase, IrdtmConfirmationReports)
    adoqryRecordCompaniesSchools: TFVBFFCQuery;
    adoqryRecordCompaniesSchoolsF_CONF_SUB_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_CONF_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ORGANISATION: TStringField;
    adoqryRecordCompaniesSchoolsF_ORGANISATION_TYPE: TSmallintField;
    adoqryRecordCompaniesSchoolsF_NO_PARTICIPANTS: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT1_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT2_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_CONFIRMATION_DATE: TDateTimeField;
    prvRecordOrganisation: TFVBFFCDataSetProvider;
    adoqryRecordOrganisation: TFVBFFCQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField4: TStringField;
    SmallintField2: TSmallintField;
    IntegerField6: TIntegerField;
    StringField5: TStringField;
    StringField6: TStringField;
    DateTimeField2: TDateTimeField;
    prvRecordCompaniesSchools: TFVBFFCDataSetProvider;
    adoqryRecordInfrastructure: TFVBFFCQuery;
    prvRecordInfrastructure: TFVBFFCDataSetProvider;
    adoqryRecordF_CONF_ID: TAutoIncField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adosrcRecord: TFVBFFCDataSource;
    adoqryRecordCompaniesSchoolsF_SESSION_ID: TIntegerField;
    adoqryRecordOrganisationF_SESSION_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_PRINT_CONFIRMATION: TBooleanField;
    adoqryRecordOrganisationF_PRINT_CONFIRMATION: TBooleanField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT2: TStringField;
    adoqryRecordOrganisationF_ORG_CONTACT1: TStringField;
    adoqryRecordOrganisationF_ORG_CONTACT2: TStringField;
    adoqryRecordInfrastructureF_CONF_SUB_ID: TIntegerField;
    adoqryRecordInfrastructureF_CONF_ID: TIntegerField;
    adoqryRecordInfrastructureF_ORGANISATION: TStringField;
    adoqryRecordInfrastructureF_ORGANISATION_TYPE: TSmallintField;
    adoqryRecordInfrastructureF_NO_PARTICIPANTS: TIntegerField;
    adoqryRecordInfrastructureF_CONFIRMATION_DATE: TDateTimeField;
    adoqryRecordInfrastructureF_SESSION_ID: TIntegerField;
    adoqryRecordInfrastructureF_PRINT_CONFIRMATION: TBooleanField;
    adoqryRecordInfrastructureF_ORG_CONTACT1_EMAIL: TStringField;
    adoqryRecordInfrastructureF_ORG_CONTACT2: TStringField;
    adoqryRecordInfrastructureF_ORG_CONTACT2_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT1: TStringField;
    adoqryRecordInfrastructureF_ORG_CONTACT1: TStringField;
    adoqryFieldValueForSession: TFVBFFCQuery;
    adoqryFieldValueForSessionF_PROGRAM_NAME: TStringField;
    adoqryRecordCompaniesSchoolsF_ORGTYPE_ID: TIntegerField;
    adocmdTemp: TFVBFFCCommand;
    adospConfirmationCalcRolegroups: TFVBFFCStoredProc;
    adoqryRecordCompaniesSchoolsF_ORGANISATION_ID: TIntegerField;
    adoqryRecordInfrastructureF_LOCATION: TStringField;
    adoqryRecordOrganisationF_INSTRUCTOR_NAME: TStringField;
    adoqryRecordOrganisationF_ORG_INSTRUCTOR_NAME: TStringField;
    adoqryFieldValueForSessionF_CODE: TStringField;
    adoqryFieldValueForSessionF_SESSION_ID: TIntegerField;
    adoqryFieldValueForSessionF_SESSIONGROUP_ID: TIntegerField;
    adoqryRecordInfrastructureF_INFRASTRUCTURE_ID: TIntegerField;
    adop_update_contact_company: TFVBFFCStoredProc;
    adop_update_contact_infrastruct: TFVBFFCStoredProc;
    adop_update_contact_instructororg: TFVBFFCStoredProc;
    adoqryRecordOrganisationF_INSTRUCTOR_ID: TIntegerField;
    adoqryFieldValueForSessionF_START_DATE: TDateTimeField;
    adoqryMailingSignature: TFVBFFCQuery;
    adoqryMailingSignatureF_SIGNATURE: TStringField;
    P_MARK_CONFREP_SENT: TFVBFFCStoredProc;
    adoqryRecordF_EXTRA_COMPANIES: TMemoField;
    adoqryRecordF_EXTRA_INFRASTRUCTURE: TMemoField;
    adoqryRecordF_EXTRA_ORGANISATION: TMemoField;
    adoqryRecordCompaniesSchoolsF_MAIL_ATTACHMENT: TStringField;
    adoqryRecordOrganisationF_MAIL_ATTACHMENT: TStringField;
    adospAttestRegistered: TFVBFFCStoredProc;
    prvAttestRegistered: TFVBFFCDataSetProvider;
    adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_NAME: TStringField;
    adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ISCCENABLED: TBooleanField;
    adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1: TStringField;
    adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2: TStringField;
    adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2_EMAIL: TStringField;
    adoqryFieldValueForSessionF_PROGRAM_TYPE: TIntegerField;
    adoqryMailingBedrijfVCASignature: TFVBFFCQuery;
    StringField1: TStringField;
    adoqryRecordCompaniesSchoolsF_LANGUAGE_ID: TIntegerField;
    adoqryRecordInfrastructureF_LANGUAGE_ID: TIntegerField;
    adoqryRecordOrganisationF_LANGUAGE_ID: TIntegerField;
    adoqryMailingVCASignature: TFVBFFCQuery;
    StringField2: TStringField;
    adoqryFieldValueForSessionTX_ExamenVCA: TStringField;
  private
    function GetFieldValueForSessionId (SessionId: Integer; FieldName: String): String;
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function GetProgramNameFromSessionId(SessionId: Integer): WideString; safecall;
    function GetTextFromSessionId(SessionId: Integer): WideString; safecall;
    function GetCodeFromSessionId(SessionId: Integer): WideString; safecall;
    function GetSessionGroupIdFromSessionId(SessionId: Integer): SYSINT; safecall;
    function GetProgramTypeFromSessionId(SessionId: Integer): SYSINT; safecall;
    function GetStartdateFromSessionId(SessionId: Integer): WideString; safecall;
    function GetMailingSignature(ALanguage: Integer): WideString; safecall;
    function GetMailingVCASignature(ALanguage: Integer): WideString; safecall;
    function GetMailingBedrijfVCASignature(ALanguage: Integer): WideString; safecall;

    procedure MarkConfirmationRptAsPrinted(conf_id: Integer; conf_sub_id: Integer); safecall;
    procedure RecalcRolegroups(SessionId: Integer; TotalParticipants: Integer; OrganisationId: Integer; PartnerWinterId: Integer); safecall;
    procedure SetToBePrinted(ConfId: Integer); safecall;

    procedure UpdateContactCompany(AOrganisationId: Integer; ANoContact: Integer;
                                   const AContactName: WideString; const AContactEmail: WideString;
                                   AConfSubId: Integer; AIsCCEnabled: Integer); safecall;

    procedure UpdateContactInfrastruct(AInfrastructure_Id: Integer; ANoContact: Integer;
                                       const AContactName: WideString;
                                       const AContactEmail: WideString); safecall;
    procedure UpdateContactInstructorOrg(AInstructorId: Integer; ANoContact: Integer;
                                         const AContactName: WideString;
                                         const AContactEmail: WideString); safecall;

    procedure MarkConfrepSent(AConfId: Integer); safecall;


  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofConfirmationReports:TComponentFactory;

implementation

{$R *.dfm}

uses Remote_EduMain;

{ TrdtmConfirmationReports }

function TrdtmConfirmationReports.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmConfirmationReports.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, AFilter );
end;

function TrdtmConfirmationReports.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy(aProvider, aOrderBy);
end;

function TrdtmConfirmationReports.GetNewRecordID: SYSINT;
begin
  inherited GetNewRecordID;
end;

function TrdtmConfirmationReports.GetTableName: String;
begin

end;

procedure TrdtmConfirmationReports.Set_ADOConnection(Param1: Integer);
begin
  inherited Set_ADOConnection(Param1);

end;

procedure TrdtmConfirmationReports.Set_rdtmEDUMain(
  const Param1: IrdtmEDUMain);
begin
  inherited Set_rdtmEDUMain(Param1);

end;

function TrdtmConfirmationReports.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

class procedure TrdtmConfirmationReports.UpdateRegistry(Register: Boolean;
  const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;

end;

function TrdtmConfirmationReports.GetFieldValueForSessionId(
  SessionId: Integer; FieldName: String): String;
begin
  result := '';
  with adoqryFieldValueForSession do begin
    Close;
    Parameters.ParamByName('@F_SESSION_ID').Value := SessionId;
    Open;
  end;

  try
    if not adoqryFieldValueForSession.FieldByName(FieldName).IsNull then
    begin
      result := adoqryFieldValueForSession.FieldByName(FieldName).AsString;
    end;
    if result = '' then result := 'Session ID ' + adoqryFieldValueForSession.FieldByName('F_SESSION_ID').AsString;
  finally
      adoqryFieldValueForSession.Close;
  end;
end;

function TrdtmConfirmationReports.GetProgramNameFromSessionId(
  SessionId: Integer): WideString;
begin
  result := GetFieldValueForSessionId (SessionId, 'F_PROGRAM_NAME');
end;

function TrdtmConfirmationReports.GetTextFromSessionId(
  SessionId: Integer): WideString;
begin
  result := GetFieldValueForSessionId (SessionId, 'TX_ExamenVCA');
end;

function TrdtmConfirmationReports.GetCodeFromSessionId(
  SessionId: Integer): WideString;
begin
  result := GetFieldValueForSessionId (SessionId, 'F_CODE');
end;

function TrdtmConfirmationReports.GetSessionGroupIdFromSessionId(
  SessionId: Integer): SYSINT;
begin
  result := StrToInt(GetFieldValueForSessionId (SessionId, 'F_SESSIONGROUP_ID'));
end;

function TrdtmConfirmationReports.GetProgramTypeFromSessionId(
  SessionId: Integer): SYSINT;
begin
  result := StrToInt(GetFieldValueForSessionId (SessionId, 'F_PROGRAM_TYPE'));
end;

function TrdtmConfirmationReports.GetStartdateFromSessionId(
  SessionId: Integer): WideString;
begin
  result := GetFieldValueForSessionId (SessionId, 'F_START_DATE');
end;

function TrdtmConfirmationReports.GetMailingSignature(ALanguage: Integer): WideString;
var
  aSignature: String;
begin

  with adoqryMailingSignature do
  begin
      Close;
      Parameters.ParamByName('@Language').Value := ALanguage;
      Open;
  end;
  try
    if adoqryMailingSignature.RecordCount > 0 then
      aSignature := adoqryMailingSignature.FieldByName('F_SIGNATURE').AsString;

  finally
      adoqryMailingSignature.Close;
  end;

  Result := aSignature;
end;

function TrdtmConfirmationReports.GetMailingVCASignature(ALanguage: Integer): WideString;
var
  aSignature: String;
begin

  with adoqryMailingVCASignature do
  begin
      Close;
      Parameters.ParamByName('@Language').Value := ALanguage;
      Open;
  end;
  try
    if adoqryMailingVCASignature.RecordCount > 0 then
      aSignature := adoqryMailingVCASignature.FieldByName('F_SIGNATURE').AsString;

  finally
      adoqryMailingVCASignature.Close;
  end;

  Result := aSignature;
end;

function TrdtmConfirmationReports.GetMailingBedrijfVCASignature(ALanguage: Integer): WideString;
var
  aSignature: String;
begin

  with adoqryMailingBedrijfVCASignature do
  begin
      Close;
      Parameters.ParamByName('@Language1').Value := ALanguage;
      Parameters.ParamByName('@Language2').Value := ALanguage;
      Parameters.ParamByName('@Language3').Value := ALanguage;
      Open;
  end;
  try
    if adoqryMailingBedrijfVCASignature.RecordCount > 0 then
      aSignature := adoqryMailingBedrijfVCASignature.FieldByName('F_SIGNATURE').AsString;

  finally
      adoqryMailingBedrijfVCASignature.Close;
  end;

  Result := aSignature;
end;

procedure TrdtmConfirmationReports.MarkConfirmationRptAsPrinted(conf_id,
  conf_sub_id: Integer);
begin
  with adocmdTemp do begin
      CommandText := 'UPDATE T_REP_CONFIRMATION_DETAILS SET F_PRINT_CONFIRMATION = 0, F_CONFIRMATION_DATE = GETDATE()' +
                      ' WHERE F_CONF_ID = :1 AND F_CONF_SUB_ID = :2';
      Parameters.ParamByName('1').DataType := ftInteger;
      Parameters.ParamByName('2').DataType := ftInteger;
      Parameters.ParamByName('1').Value := conf_id;
      Parameters.ParamByName('2').Value := conf_sub_id;
      Execute;
  end;

end;

{****************************************************************************************
  Name           : RecalcRolegroups
  Author         : Ivan Van den Bossche
  Arguments      : SessionId, TotalParticipants, OrganisationId
  Return Values  : None
  Exceptions     : None
  Description    : Recalculate totals per rolegroup for given session id
                    and inserts this numbers into table T_REP_CONFIRMATION_COMPANY_ROLES
  History        :

  Date         By                   Description
  ----         --                   -----------
  31/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
  28/08/2013  Ivan Van den Bossche  Added PartnerWinterId
 ****************************************************************************************}
procedure TrdtmConfirmationReports.RecalcRolegroups(SessionId,
  TotalParticipants, OrganisationId: Integer; PartnerWinterId: Integer);
begin
  with adospConfirmationCalcRolegroups do begin
      Parameters.ParamByName('@SessionId').Value := SessionId;
      Parameters.ParamByName('@TotalParticipants').Value := TotalParticipants;
      Parameters.ParamByName('@OrganisationId').Value := OrganisationId;
      Parameters.ParamByName('@PartnerWinterId').Value := PartnerWinterId;
      ExecProc;
  end;
end;


{****************************************************************************************
  Name           : SetToBePrinted
  Author         : WL
  Arguments      : ConfId
  Return Values  : None
  Exceptions     : None
  Description    : For the given records in T_REP_CONFIRMATION_DETAILS
                   sets F_PRINT_CONFIRMATION = 1 where F_CONFIRMATION_DATE is null.
  History        :

  Date         By                   Description
  ----         --                   -----------
  16/08/2007   WL                   Initial creation of the Procedure.
 ****************************************************************************************}
procedure TrdtmConfirmationReports.SetToBePrinted(ConfId: Integer);
begin
  with adocmdTemp do begin
      CommandText := 'UPDATE T_REP_CONFIRMATION_DETAILS SET F_PRINT_CONFIRMATION = 1' +
                      ' WHERE F_CONF_ID = :@F_CONF_ID and F_CONFIRMATION_DATE is NULL';
      Parameters.ParamByName('@F_CONF_ID').DataType := ftInteger;
      Parameters.ParamByName('@F_CONF_ID').Value := ConfId;
      Execute;
  end;
end;

procedure TrdtmConfirmationReports.UpdateContactCompany(AOrganisationId,
  ANoContact: Integer; const AContactName, AContactEmail: WideString;
  AConfSubId: Integer; AIsCCEnabled: Integer);
begin
  with adop_update_contact_company do
  begin
      Parameters.ParamByName('@AOrganisation_Id').Value := AOrganisationId;
      Parameters.ParamByName('@ANoContact').Value := ANoContact;
      Parameters.ParamByName('@AContactName').Value := AContactName;
      Parameters.ParamByName('@AContactEmail').Value := AContactEmail;
      Parameters.ParamByName('@ConfSubId').Value := AConfSubId;
      Parameters.ParamByName('@IsCCEnabled').Value := AIsCCEnabled;
      ExecProc;
  end;
end;

procedure TrdtmConfirmationReports.UpdateContactInfrastruct(
  AInfrastructure_Id, ANoContact: Integer; const AContactName,
  AContactEmail: WideString);
begin
  with adop_update_contact_infrastruct do
  begin
      Parameters.ParamByName('@AInfrastructure_Id').Value := AInfrastructure_Id;
      Parameters.ParamByName('@ANoContact').Value := ANoContact;
      Parameters.ParamByName('@AContactName').Value := AContactName;
      Parameters.ParamByName('@AContactEmail').Value := AContactEmail;
      ExecProc;
  end;
end;

procedure TrdtmConfirmationReports.UpdateContactInstructorOrg(
  AInstructorId, ANoContact: Integer; const AContactName,
  AContactEmail: WideString);
begin
  with adop_update_contact_instructororg do
  begin
      Parameters.ParamByName('@AInstructorId').Value := AInstructorId;
      Parameters.ParamByName('@ANoContact').Value := ANoContact;
      Parameters.ParamByName('@AContactName').Value := AContactName;
      Parameters.ParamByName('@AContactEmail').Value := AContactEmail;
      ExecProc;
  end;

end;

{****************************************************************************************
  Name           : MarkConfrepSent
  Author         : Ivan Van den Bossche
  Arguments      : AConfId
  Return Values  : None
  Exceptions     : None
  Description    : Set [F_CONFIRMATION_REPORT_SENT] in T_SES_SESSION when all corresponding
                    records in T_REP_CONFIRMATION_DETAILS have F_CONFIRMATION_DATE set.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/04/2008   Ivan Van den Bossche Initial creation of the Procedure.
 ****************************************************************************************}
procedure TrdtmConfirmationReports.MarkConfrepSent(AConfId: Integer);
begin
  with P_MARK_CONFREP_SENT do
  begin
      Parameters.ParamByName('@ACONF_ID').Value := AConfId;
      ExecProc;
  end;
end;

initialization
  ofConfirmationReports := TComponentFactory.Create(ComServer, TrdtmConfirmationReports,
    Class_rdtmConfirmationReports, ciInternal, tmApartment);

end.
