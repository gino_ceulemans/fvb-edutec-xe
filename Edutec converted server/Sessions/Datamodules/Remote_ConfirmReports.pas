unit Remote_ConfirmReports;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Remote_EduBase, Provider, Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmConfirmReports = class(TrdtmEduBase)
    adoqryRecordCompaniesSchools: TFVBFFCQuery;
    adoqryRecordCompaniesSchoolsF_CONF_SUB_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_CONF_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ORGANISATION: TStringField;
    adoqryRecordCompaniesSchoolsF_ORGANISATION_TYPE: TSmallintField;
    adoqryRecordCompaniesSchoolsF_NO_PARTICIPANTS: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT1_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT2_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_CONFIRMATION_DATE: TDateTimeField;
    adoqryHeader: TFVBFFCQuery;
    adoqryHeaderF_CONF_ID: TAutoIncField;
    adoqryHeaderF_SESSION_ID: TIntegerField;
    adoqryHeaderF_EXTRA_COMPANIES: TStringField;
    adoqryHeaderF_EXTRA_INFRASTRUCTURE: TStringField;
    adoqryHeaderF_EXTRA_ORGANISATION: TStringField;
    prvHeader: TFVBFFCDataSetProvider;
    prvRecordOrganisation: TFVBFFCDataSetProvider;
    adoqryRecordOrganisation: TFVBFFCQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField4: TStringField;
    SmallintField2: TSmallintField;
    IntegerField6: TIntegerField;
    StringField5: TStringField;
    StringField6: TStringField;
    DateTimeField2: TDateTimeField;
    prvRecordCompaniesSchools: TFVBFFCDataSetProvider;
    adoqryRecordInfrastructure: TFVBFFCQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    SmallintField1: TSmallintField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    prvRecordInfrastructure: TFVBFFCDataSetProvider;
    adospPREPARE_CONFIRMATION_REPORTS: TFVBFFCStoredProc;
    prvPREPARE_CONFIRMATION_REPORTS: TFVBFFCDataSetProvider;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rdtmConfirmReports: TrdtmConfirmReports;

implementation

{$R *.dfm}

end.
