inherited rdtmConfirmReports: TrdtmConfirmReports
  OldCreateOrder = True
  Left = 466
  Top = 307
  Height = 439
  Width = 662
  object adoqryRecordCompaniesSchools: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE]'
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 0')
    AutoOpen = False
    Left = 288
    Top = 136
    object adoqryRecordCompaniesSchoolsF_CONF_SUB_ID: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere]
    end
    object adoqryRecordCompaniesSchoolsF_CONF_ID: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION_TYPE: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT1_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT2_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_CONFIRMATION_DATE: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  object adoqryHeader: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '[F_CONF_ID],'
      '[F_SESSION_ID],'
      '[F_EXTRA_COMPANIES],'
      '[F_EXTRA_INFRASTRUCTURE],'
      '[F_EXTRA_ORGANISATION]'
      'FROM '
      '  V_REP_CONFIRMATION_HEADER')
    AutoOpen = False
    Left = 176
    Top = 280
    object adoqryHeaderF_CONF_ID: TAutoIncField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
      ReadOnly = True
    end
    object adoqryHeaderF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object adoqryHeaderF_EXTRA_COMPANIES: TStringField
      FieldName = 'F_EXTRA_COMPANIES'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryHeaderF_EXTRA_INFRASTRUCTURE: TStringField
      FieldName = 'F_EXTRA_INFRASTRUCTURE'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryHeaderF_EXTRA_ORGANISATION: TStringField
      FieldName = 'F_EXTRA_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
  end
  object prvHeader: TFVBFFCDataSetProvider
    DataSet = adoqryHeader
    Left = 176
    Top = 328
  end
  object prvRecordOrganisation: TFVBFFCDataSetProvider
    DataSet = adoqryRecordOrganisation
    Left = 288
    Top = 288
  end
  object adoqryRecordOrganisation: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE]'
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 2')
    AutoOpen = False
    Left = 288
    Top = 240
    object IntegerField4: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere]
    end
    object IntegerField5: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
    end
    object StringField4: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object SmallintField2: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField6: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object StringField5: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField6: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvRecordCompaniesSchools: TFVBFFCDataSetProvider
    DataSet = adoqryRecordCompaniesSchools
    Left = 288
    Top = 184
  end
  object adoqryRecordInfrastructure: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE]'
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 1')
    AutoOpen = False
    Left = 472
    Top = 136
    object IntegerField1: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere]
    end
    object IntegerField2: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
    end
    object StringField1: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object SmallintField1: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField3: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object StringField2: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField3: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvRecordInfrastructure: TFVBFFCDataSetProvider
    DataSet = adoqryRecordInfrastructure
    Left = 472
    Top = 184
  end
  object adospPREPARE_CONFIRMATION_REPORTS: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_PREPARE_CONFIRMATION_REPORTS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 496
    Top = 280
  end
  object prvPREPARE_CONFIRMATION_REPORTS: TFVBFFCDataSetProvider
    DataSet = adospPREPARE_CONFIRMATION_REPORTS
    Left = 496
    Top = 336
  end
end
