inherited dtmRdtmSesExpenses: TdtmRdtmSesExpenses
  OldCreateOrder = True
  Height = 179
  Width = 235
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_EXPENSE_ID, '
      '  F_SESSION_ID, '
      '  F_SESSION_NAME, '
      '  F_EXPENSE_TYPE, '
      '  F_EXPENSE_DESC, '
      '  F_EXPENSE_AMOUNT,'
      '  F_EXPENSE_DATE '
      'FROM '
      '  V_SES_EXPENSE')
    object adoqryListF_EXPENSE_ID: TIntegerField
      FieldName = 'F_EXPENSE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_EXPENSE_TYPE: TIntegerField
      FieldName = 'F_EXPENSE_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EXPENSE_DESC: TMemoField
      FieldName = 'F_EXPENSE_DESC'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryListF_EXPENSE_AMOUNT: TFloatField
      FieldName = 'F_EXPENSE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EXPENSE_DATE: TDateTimeField
      FieldName = 'F_EXPENSE_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_EXPENSE_ID, '
      '  F_SESSION_ID, '
      '  F_SESSION_NAME, '
      '  F_EXPENSE_TYPE, '
      '  F_EXPENSE_DESC, '
      '  F_EXPENSE_AMOUNT,'
      '  F_EXPENSE_DATE '
      'FROM '
      '  V_SES_EXPENSE')
    object adoqryRecordF_EXPENSE_ID: TIntegerField
      FieldName = 'F_EXPENSE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_EXPENSE_TYPE: TIntegerField
      FieldName = 'F_EXPENSE_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EXPENSE_DESC: TMemoField
      FieldName = 'F_EXPENSE_DESC'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_EXPENSE_AMOUNT: TFloatField
      FieldName = 'F_EXPENSE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EXPENSE_DATE: TDateTimeField
      FieldName = 'F_EXPENSE_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
end
