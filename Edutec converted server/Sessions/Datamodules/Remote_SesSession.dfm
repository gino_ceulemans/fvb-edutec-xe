inherited rdtmSesSession: TrdtmSesSession
  OldCreateOrder = True
  Height = 272
  Width = 335
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    ParamCheck = False
    SQL.Strings = (
      'SELECT distinct'
      '  F_SESSION_ID,'
      '  F_PROGRAM_ID,'
      '  F_PROGRAM_NAME,'
      '  F_PROGRAM_TYPE,'
      '  F_PROGRAM_TYPE_NAME, '
      '  F_NAME,'
      '  F_CODE,'
      '  F_CODE_EPYC,'
      '  F_DURATION,'
      '  F_DURATION_DAYS,'
      '  F_MINIMUM,'
      '  F_MAXIMUM,'
      '  F_START_DATE,'
      '  F_END_DATE,'
      '  F_STATUS_ID,'
      '  F_STATUS_NAME,'
      '  F_LANGUAGE_ID,'
      '  F_LANGUAGE_NAME,'
      '  F_PRICING_SCHEME,'
      '  F_PRICE_WORKER,'
      '  F_PRICE_CLERK,'
      '  F_PRICE_OTHER,'
      '  F_INFRASTRUCTURE_ID,'
      '  F_INFRASTRUCTURE_NAME,'
      '  F_ANA_SLEUTEL,'
      '  F_CONSTRUCT_COMMENT,'
      '  F_SES_AANW_PRINTED,'
      '  F_ALL_CERT_PRINTED,'
      '  F_NO_PARTICIPANTS,'
      '  F_WEEK,'
      '  F_SESSIONGROUP_NAME,'
      '  F_RECEIVED_PARTICIPANTS_LIST,'
      '  F_SENT_COURSE,'
      '  F_INSTRUCTOR,'
      '  F_SES_DAYS,'
      '  F_CONFIRMATION_REPORT_SENT,'
      '  F_ENROLMENTS_WEB,'
      '  F_COMMENT,'
      '  F_VIRTUAL_SESSION,'
      '  F_COOPERATION_ID,'
      '  F_COOPERATION_NAME,'
      ' F_EPYC,'
      ' F_EXAMINATOR,'
      ' F_ATTESTS_SENT,'
      ' F_SCANNING'
      'FROM'
      '  V_SES_SESSION_WITH_ORGANISATION')
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROGRAM_NAME: TStringField
      FieldName = 'F_PROGRAM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PROGRAM_TYPE_NAME: TStringField
      FieldName = 'F_PROGRAM_TYPE_NAME'
      Size = 40
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_DURATION: TSmallintField
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_DURATION_DAYS: TSmallintField
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ANA_SLEUTEL: TStringField
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_CLERK: TFloatField
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CONSTRUCT_COMMENT: TStringField
      DisplayLabel = 'Construct commentaar'
      FieldName = 'F_CONSTRUCT_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_SES_AANW_PRINTED: TBooleanField
      FieldName = 'F_SES_AANW_PRINTED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ALL_CERT_PRINTED: TBooleanField
      FieldName = 'F_ALL_CERT_PRINTED'
      ProviderFlags = []
    end
    object adoqryListF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_SESSIONGROUP_NAME: TStringField
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_RECEIVED_PARTICIPANTS_LIST: TBooleanField
      FieldName = 'F_RECEIVED_PARTICIPANTS_LIST'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_WEEK: TStringField
      FieldName = 'F_WEEK'
      ProviderFlags = []
      ReadOnly = True
      Size = 15
    end
    object adoqryListF_SENT_COURSE: TBooleanField
      FieldName = 'F_SENT_COURSE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INSTRUCTOR: TStringField
      FieldName = 'F_INSTRUCTOR'
      ProviderFlags = []
      ReadOnly = True
      Size = 255
    end
    object adoqryListF_SES_DAYS: TStringField
      FieldName = 'F_SES_DAYS'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryListF_CONFIRMATION_REPORT_SENT: TBooleanField
      FieldName = 'F_CONFIRMATION_REPORT_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ENROLMENTS_WEB: TIntegerField
      DisplayLabel = 'N Inschrijvingen via Web'
      FieldName = 'F_ENROLMENTS_WEB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = []
      Size = 255
    end
    object adoqryListF_VIRTUAL_SESSION: TBooleanField
      FieldName = 'F_VIRTUAL_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COOPERATION_ID: TIntegerField
      FieldName = 'F_COOPERATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_COOPERATION_NAME: TStringField
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_EPYC: TBooleanField
      FieldName = 'F_EPYC'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_EXAMINATOR: TBooleanField
      FieldName = 'F_EXAMINATOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ATTESTS_SENT: TBooleanField
      FieldName = 'F_ATTESTS_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SCANNING: TBooleanField
      FieldName = 'F_SCANNING'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = []
    end
    object adoqryListF_CODE_EPYC: TStringField
      FieldName = 'F_CODE_EPYC'
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    ParamCheck = False
    SQL.Strings = (
      'SELECT '
      '  F_SESSION_ID, '
      '  F_PROGRAM_ID, '
      '  F_PROGRAM_NAME, '
      '  F_PROGRAM_TYPE,'
      '  F_PROGRAM_TYPE_NAME,'
      '  F_USER_ID, '
      '  F_COMPLETE_USERNAME, '
      '  F_SESSIONGROUP_ID, '
      '  F_SESSIONGROUP_NAME, '
      '  F_NAME, '
      '  F_CODE, '
      '  F_CODE_EPYC,'
      '  F_DURATION, '
      '  F_DURATION_DAYS, '
      '  F_MINIMUM, '
      '  F_MAXIMUM, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_STATUS_ID, '
      '  F_STATUS_NAME, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_COMMENT, '
      '  F_START_HOUR, '
      '  F_PRICING_SCHEME, '
      '/*  F_PRICE_SESSION, '
      '  F_PRICE_DAY,  */'
      '  F_PRICE_WORKER, '
      '  F_PRICE_CLERK,'
      '  F_PRICE_OTHER,'
      '  F_INFRASTRUCTURE_ID, '
      '  F_INFRASTRUCTURE_NAME,'
      '  F_ANA_SLEUTEL,'
      '  F_SES_DAYS,'
      '  F_PRICE_FIXED,'
      '  F_TK_FVB,'
      '  F_TK_CEVORA,'
      '  F_BT_FVB,'
      '  F_BT_CEVORA,'
      '  F_OCR,'
      '  F_INDIRECT_COST,'
      '  F_WEB,'
      '  F_EXTRA_INFO,'
      '  F_VIRTUAL_SESSION,'
      '  F_COOPERATION_ID,'
      '  F_COOPERATION_NAME,'
      ' F_EPYC,'
      ' F_EXAMINATOR,'
      ' F_ATTESTS_SENT,'
      ' F_SCANNING'
      ''
      'FROM '
      '  V_SES_SESSION')
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PROGRAM_NAME: TStringField
      FieldName = 'F_PROGRAM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
    end
    object adoqryRecordF_PROGRAM_TYPE_NAME: TStringField
      FieldName = 'F_PROGRAM_TYPE_NAME'
      Size = 40
    end
    object adoqryRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object adoqryRecordF_SESSIONGROUP_ID: TIntegerField
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSIONGROUP_NAME: TStringField
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DURATION: TSmallintField
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_DURATION_DAYS: TSmallintField
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_START_HOUR: TStringField
      FieldName = 'F_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ANA_SLEUTEL: TStringField
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryRecordF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_CLERK: TFloatField
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TK_FVB: TFloatField
      FieldName = 'F_TK_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TK_CEVORA: TFloatField
      FieldName = 'F_TK_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_BT_FVB: TFloatField
      FieldName = 'F_BT_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_BT_CEVORA: TFloatField
      FieldName = 'F_BT_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_FIXED: TFloatField
      FieldName = 'F_PRICE_FIXED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SES_DAYS: TMemoField
      FieldName = 'F_SES_DAYS'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_OCR: TStringField
      FieldName = 'F_OCR'
      ProviderFlags = []
      Size = 100
    end
    object adoqryRecordF_INDIRECT_COST: TBCDField
      FieldName = 'F_INDIRECT_COST'
      ProviderFlags = [pfInUpdate]
      Precision = 5
      Size = 2
    end
    object adoqryRecordF_WEB: TBooleanField
      FieldName = 'F_WEB'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EXTRA_INFO: TStringField
      FieldName = 'F_EXTRA_INFO'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object adoqryRecordF_VIRTUAL_SESSION: TBooleanField
      FieldName = 'F_VIRTUAL_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COOPERATION_ID: TIntegerField
      FieldName = 'F_COOPERATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COOPERATION_NAME: TStringField
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_EPYC: TBooleanField
      FieldName = 'F_EPYC'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_EXAMINATOR: TBooleanField
      FieldName = 'F_EXAMINATOR'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_ATTESTS_SENT: TBooleanField
      FieldName = 'F_ATTESTS_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SCANNING: TBooleanField
      FieldName = 'F_SCANNING'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CODE_EPYC: TStringField
      FieldName = 'F_CODE_EPYC'
    end
  end
  object adospSES_SESSION: TFVBFFCStoredProc
    ProcedureName = 'SP_SES_SESSION'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ACTION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PRICE_WORKER'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@PRICE_CLERK'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@PRICE_OTHER'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@SESSION_DATE'
        DataType = ftDateTime
        Value = Null
      end>
    AutoOpen = False
    Left = 240
    Top = 24
  end
  object prvSES_SESSION: TFVBFFCDataSetProvider
    DataSet = adospSES_SESSION
    Left = 240
    Top = 72
  end
  object adoqryUpdate: TADOQuery
    Connection = rdtmEDUMain.adocnnMain
    Parameters = <
      item
        Name = 'F_SESSION_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'update T_SES_SESSION'
      'set F_SES_AANW_PRINTED = 1'
      'where F_SESSION_ID = :F_SESSION_ID')
    Left = 64
    Top = 152
  end
  object adoP_CREATE_OCR: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_CREATE_OCR;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ASessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 240
    Top = 136
  end
end
