{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  <T_SES_SESSION>.

  @Name       Remote_SesSession
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/09/2007   ivdbossche           Added CreateOCR procedure
  11/09/2006   sLesage              Added fields for 'Tussenkomst' and
                                    'Boetes'.
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_SesSession;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmSesSession = class(TrdtmEduBase, IrdtmSesSession)
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_PROGRAM_ID: TIntegerField;
    adoqryListF_PROGRAM_NAME: TStringField;
    adoqryListF_NAME: TStringField;
    adoqryListF_CODE: TStringField;
    adoqryListF_DURATION: TSmallintField;
    adoqryListF_DURATION_DAYS: TSmallintField;
    adoqryListF_MINIMUM: TSmallintField;
    adoqryListF_MAXIMUM: TSmallintField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_END_DATE: TDateTimeField;
    adoqryListF_STATUS_ID: TIntegerField;
    adoqryListF_STATUS_NAME: TStringField;
    adoqryListF_LANGUAGE_ID: TIntegerField;
    adoqryListF_LANGUAGE_NAME: TStringField;
    adoqryListF_PRICING_SCHEME: TIntegerField;
    adoqryListF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryListF_INFRASTRUCTURE_NAME: TStringField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_PROGRAM_ID: TIntegerField;
    adoqryRecordF_PROGRAM_NAME: TStringField;
    adoqryRecordF_USER_ID: TIntegerField;
    adoqryRecordF_COMPLETE_USERNAME: TStringField;
    adoqryRecordF_SESSIONGROUP_ID: TIntegerField;
    adoqryRecordF_SESSIONGROUP_NAME: TStringField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_CODE: TStringField;
    adoqryRecordF_DURATION: TSmallintField;
    adoqryRecordF_DURATION_DAYS: TSmallintField;
    adoqryRecordF_MINIMUM: TSmallintField;
    adoqryRecordF_MAXIMUM: TSmallintField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_STATUS_ID: TIntegerField;
    adoqryRecordF_STATUS_NAME: TStringField;
    adoqryRecordF_LANGUAGE_ID: TIntegerField;
    adoqryRecordF_LANGUAGE_NAME: TStringField;
    adoqryRecordF_COMMENT: TMemoField;
    adoqryRecordF_START_HOUR: TStringField;
    adoqryRecordF_PRICING_SCHEME: TIntegerField;
    adoqryRecordF_INFRASTRUCTURE_ID: TIntegerField;
    adoqryRecordF_INFRASTRUCTURE_NAME: TStringField;
    adospSES_SESSION: TFVBFFCStoredProc;
    prvSES_SESSION: TFVBFFCDataSetProvider;
    adoqryRecordF_PRICE_OTHER: TFloatField;
    adoqryListF_PRICE_OTHER: TFloatField;
    adoqryListF_ANA_SLEUTEL: TStringField;
    adoqryRecordF_ANA_SLEUTEL: TStringField;
    adoqryListF_PRICE_WORKER: TFloatField;
    adoqryListF_PRICE_CLERK: TFloatField;
    adoqryRecordF_PRICE_WORKER: TFloatField;
    adoqryRecordF_PRICE_CLERK: TFloatField;
    adoqryListF_CONSTRUCT_COMMENT: TStringField;
    adoqryRecordF_TK_FVB: TFloatField;
    adoqryRecordF_TK_CEVORA: TFloatField;
    adoqryRecordF_BT_FVB: TFloatField;
    adoqryRecordF_BT_CEVORA: TFloatField;
    adoqryRecordF_PRICE_FIXED: TFloatField;
    adoqryRecordF_SES_DAYS: TMemoField;
    adoqryUpdate: TADOQuery;
    adoqryListF_SES_AANW_PRINTED: TBooleanField;
    adoqryListF_ALL_CERT_PRINTED: TBooleanField;
    adoqryListF_NO_PARTICIPANTS: TIntegerField;
    adoqryRecordF_OCR: TStringField;
    adoP_CREATE_OCR: TFVBFFCStoredProc;
    adoqryRecordF_INDIRECT_COST: TBCDField;
    adoqryRecordF_WEB: TBooleanField;
    adoqryListF_SESSIONGROUP_NAME: TStringField;
    adoqryListF_RECEIVED_PARTICIPANTS_LIST: TBooleanField;
    adoqryRecordF_EXTRA_INFO: TStringField;
    adoqryListF_WEEK: TStringField;
    adoqryListF_SENT_COURSE: TBooleanField;
    adoqryListF_INSTRUCTOR: TStringField;
    adoqryListF_SES_DAYS: TStringField;
    adoqryListF_CONFIRMATION_REPORT_SENT: TBooleanField;
    adoqryListF_ENROLMENTS_WEB: TIntegerField;
    adoqryListF_COMMENT: TStringField;
    adoqryListF_VIRTUAL_SESSION: TBooleanField;
    adoqryRecordF_VIRTUAL_SESSION: TBooleanField;
    adoqryListF_COOPERATION_ID: TIntegerField;
    adoqryListF_COOPERATION_NAME: TStringField;
    adoqryRecordF_COOPERATION_ID: TIntegerField;
    adoqryRecordF_COOPERATION_NAME: TStringField;
    adoqryListF_EPYC: TBooleanField;
    adoqryListF_EXAMINATOR: TBooleanField;
    adoqryListF_ATTESTS_SENT: TBooleanField;
    adoqryListF_SCANNING: TBooleanField;
    adoqryRecordF_EPYC: TBooleanField;
    adoqryRecordF_EXAMINATOR: TBooleanField;
    adoqryRecordF_ATTESTS_SENT: TBooleanField;
    adoqryRecordF_SCANNING: TBooleanField;
    adoqryListF_PROGRAM_TYPE: TIntegerField;
    adoqryListF_CODE_EPYC: TStringField;
    adoqryRecordF_CODE_EPYC: TStringField;
    adoqryListF_PROGRAM_TYPE_NAME: TStringField;
    adoqryRecordF_PROGRAM_TYPE: TIntegerField;
    adoqryRecordF_PROGRAM_TYPE_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    procedure FilterForOrganisation(OrganisationId: Integer); safecall;
    procedure MarkAsAanwezigheidslijstPrinted(const aSessionList: WideString);
      safecall;
    procedure CreateOCR(ASessionID: Integer); safecall;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofSesSession : TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmSesSession.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmSesSession.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmSesSession.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmSesSession.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmSesSession.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmSesSession.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmSesSession.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmSesSession.GetTableName: String;
begin
  Result := 'T_SES_SESSION';
end;

function TrdtmSesSession.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

procedure TrdtmSesSession.FilterForOrganisation(OrganisationId: Integer);
begin

end;

procedure TrdtmSesSession.MarkAsAanwezigheidslijstPrinted(
  const aSessionList: WideString);
begin
  with adoqryUpdate do
  begin
    SQL.Text := 'update T_SES_SESSION set F_SES_AANW_PRINTED = 1' +
      ' where F_SESSION_ID in (' + aSessionList + ')';
    ExecSQL;
  end;
end;

{*****************************************************************************
  Updates field F_OCR (T_SES_SESSION) using stored procedure P_CREATE_OCR
  @Name       TrdtmSesSession.CreateOCR
  @author     Ivan Van den Bossche
  @param      ASessionID
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TrdtmSesSession.CreateOCR(ASessionID: Integer);
begin
  with adoP_CREATE_OCR do begin
    Parameters.ParamByName('@ASessionId').Value := ASessionID;
    ExecProc;
  end;

end;

initialization
  ofSesSession := TComponentFactory.Create(ComServer, TrdtmSesSession,
    Class_rdtmSesSession, ciInternal, tmApartment);
end.
