inherited rdtmConfirmationReports: TrdtmConfirmationReports
  OldCreateOrder = True
  Height = 697
  Width = 1059
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '[F_CONF_ID],'
      '[F_SESSION_ID],'
      '[F_EXTRA_COMPANIES],'
      '[F_EXTRA_INFRASTRUCTURE],'
      '[F_EXTRA_ORGANISATION]'
      'FROM '
      '  V_REP_CONFIRMATION_HEADER')
    object adoqryRecordF_CONF_ID: TAutoIncField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordF_EXTRA_COMPANIES: TMemoField
      FieldName = 'F_EXTRA_COMPANIES'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_EXTRA_INFRASTRUCTURE: TMemoField
      FieldName = 'F_EXTRA_INFRASTRUCTURE'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object adoqryRecordF_EXTRA_ORGANISATION: TMemoField
      FieldName = 'F_EXTRA_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    ResolveToDataSet = True
  end
  object adoqryRecordCompaniesSchools: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '1'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE],'
      '  [F_SESSION_ID],'
      '  [F_PRINT_CONFIRMATION],'
      '  [F_ORGTYPE_ID],'
      '  [F_ORGANISATION_ID],'
      '  [F_MAIL_ATTACHMENT],'
      '  [F_PARTNER_WINTER_NAME],'
      '  [F_PARTNER_WINTER_ID],'
      '  [F_ISCCENABLED],'
      '  [F_PARTNER_WINTER_CONTACT1],'
      '  [F_PARTNER_WINTER_CONTACT1_EMAIL],'
      '  [F_PARTNER_WINTER_CONTACT2],'
      '  [F_PARTNER_WINTER_CONTACT2_EMAIL],'
      '  [F_LANGUAGE_ID]'
      ''
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 0 AND F_SESSION_ID = :1')
    AutoOpen = False
    Left = 288
    Top = 136
    object adoqryRecordCompaniesSchoolsF_CONF_SUB_ID: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordCompaniesSchoolsF_CONF_ID: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION_TYPE: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT1_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT2_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_CONFIRMATION_DATE: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordCompaniesSchoolsF_PRINT_CONFIRMATION: TBooleanField
      FieldName = 'F_PRINT_CONFIRMATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = []
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object adoqryRecordCompaniesSchoolsF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = []
      Size = 512
    end
    object adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_NAME: TStringField
      FieldName = 'F_PARTNER_WINTER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_ID: TIntegerField
      FieldName = 'F_PARTNER_WINTER_ID'
      ProviderFlags = []
    end
    object adoqryRecordCompaniesSchoolsF_ISCCENABLED: TBooleanField
      FieldName = 'F_ISCCENABLED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT1'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1_EMAIL: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT1_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT2'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2_EMAIL: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT2_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRecordCompaniesSchoolsF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object prvRecordOrganisation: TFVBFFCDataSetProvider
    DataSet = adoqryRecordOrganisation
    ResolveToDataSet = True
    Left = 152
    Top = 360
  end
  object adoqryRecordOrganisation: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '1'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE],'
      '  [F_SESSION_ID],'
      '  [F_PRINT_CONFIRMATION],'
      '  [F_INSTRUCTOR_NAME],'
      '  [F_ORG_INSTRUCTOR_NAME],'
      '  [F_INSTRUCTOR_ID],'
      '  [F_MAIL_ATTACHMENT],'
      '  [F_LANGUAGE_ID]'
      ''
      ''
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 2 AND F_SESSION_ID = :1')
    AutoOpen = False
    Left = 152
    Top = 296
    object IntegerField4: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object IntegerField5: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = []
    end
    object StringField4: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = []
      Size = 64
    end
    object SmallintField2: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = []
    end
    object IntegerField6: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = []
    end
    object StringField5: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField6: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = []
    end
    object adoqryRecordOrganisationF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object adoqryRecordOrganisationF_PRINT_CONFIRMATION: TBooleanField
      FieldName = 'F_PRINT_CONFIRMATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordOrganisationF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordOrganisationF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordOrganisationF_INSTRUCTOR_NAME: TStringField
      FieldName = 'F_INSTRUCTOR_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 129
    end
    object adoqryRecordOrganisationF_ORG_INSTRUCTOR_NAME: TStringField
      FieldName = 'F_ORG_INSTRUCTOR_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordOrganisationF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = []
    end
    object adoqryRecordOrganisationF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = []
      Size = 512
    end
    object adoqryRecordOrganisationF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object prvRecordCompaniesSchools: TFVBFFCDataSetProvider
    DataSet = adoqryRecordCompaniesSchools
    ResolveToDataSet = True
    Left = 288
    Top = 184
  end
  object adoqryRecordInfrastructure: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    DataSource = adosrcRecord
    Parameters = <
      item
        Name = '1'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE],'
      '  [F_SESSION_ID],'
      '  [F_PRINT_CONFIRMATION],'
      '  [F_LOCATION],'
      '  [F_INFRASTRUCTURE_ID],'
      '  [F_LANGUAGE_ID]'
      ''
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 1 AND F_SESSION_ID = :1')
    AutoOpen = False
    Left = 776
    Top = 136
    object adoqryRecordInfrastructureF_CONF_SUB_ID: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordInfrastructureF_CONF_ID: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordInfrastructureF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordInfrastructureF_ORGANISATION_TYPE: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = []
    end
    object adoqryRecordInfrastructureF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordInfrastructureF_CONFIRMATION_DATE: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordInfrastructureF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object adoqryRecordInfrastructureF_PRINT_CONFIRMATION: TBooleanField
      FieldName = 'F_PRINT_CONFIRMATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordInfrastructureF_ORG_CONTACT1_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordInfrastructureF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordInfrastructureF_ORG_CONTACT2_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordInfrastructureF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordInfrastructureF_LOCATION: TStringField
      FieldName = 'F_LOCATION'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryRecordInfrastructureF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = []
    end
    object adoqryRecordInfrastructureF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object prvRecordInfrastructure: TFVBFFCDataSetProvider
    DataSet = adoqryRecordInfrastructure
    ResolveToDataSet = True
    Left = 776
    Top = 184
  end
  object adosrcRecord: TFVBFFCDataSource
    DataSet = adoqryRecord
    Left = 136
    Top = 128
  end
  object adoqryFieldValueForSession: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@F_SESSION_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT dbo.UDF_REP_TRANSLATETEKST('#39'ExamenVCA'#39',F_LANGUAGE_ID) as ' +
        'TX_ExamenVCA,'
      '  F_PROGRAM_NAME,'
      '  F_PROGRAM_TYPE,'
      '  F_CODE,'
      '  F_SESSION_ID,'
      '  F_SESSIONGROUP_ID,'
      '  F_START_DATE'
      'FROM'
      '  V_SES_SESSION'
      'WHERE'
      '  F_SESSION_ID = :@F_SESSION_ID'
      '')
    AutoOpen = False
    Left = 296
    Top = 360
    object adoqryFieldValueForSessionF_PROGRAM_NAME: TStringField
      FieldName = 'F_PROGRAM_NAME'
      Size = 64
    end
    object adoqryFieldValueForSessionF_CODE: TStringField
      FieldName = 'F_CODE'
    end
    object adoqryFieldValueForSessionF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
    end
    object adoqryFieldValueForSessionF_SESSIONGROUP_ID: TIntegerField
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = []
    end
    object adoqryFieldValueForSessionF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object adoqryFieldValueForSessionF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
    end
    object adoqryFieldValueForSessionTX_ExamenVCA: TStringField
      FieldName = 'TX_ExamenVCA'
      ReadOnly = True
      Size = 255
    end
  end
  object adocmdTemp: TFVBFFCCommand
    Connection = rdtmEDUMain.adocnnMain
    Parameters = <>
    Left = 520
    Top = 296
  end
  object adospConfirmationCalcRolegroups: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_CONFIRMATION_CALC_ROLEGROUPS;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@TotalParticipants'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@OrganisationId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PartnerWinterId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 520
    Top = 360
  end
  object adop_update_contact_company: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_UPDATE_CONTACT_COMPANY;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@AOrganisation_Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ANoContact'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@AContactname'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
        Value = Null
      end
      item
        Name = '@AContactEmail'
        Attributes = [paNullable]
        DataType = ftString
        Size = 128
        Value = Null
      end
      item
        Name = '@ConfSubId'
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@IsCCEnabled'
        DataType = ftBoolean
        Value = Null
      end>
    AutoOpen = False
    Left = 560
    Top = 136
  end
  object adop_update_contact_infrastruct: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_UPDATE_CONTACT_INFRASTRUCT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@AInfrastructure_Id'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ANoContact'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@AContactname'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
        Value = Null
      end
      item
        Name = '@AContactEmail'
        Attributes = [paNullable]
        DataType = ftString
        Size = 128
        Value = Null
      end>
    AutoOpen = False
    Left = 560
    Top = 192
  end
  object adop_update_contact_instructororg: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_UPDATE_CONTACT_INSTRUCTORORG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@AInstructorId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ANoContact'
        Attributes = [paNullable]
        DataType = ftSmallint
        Precision = 5
        Value = Null
      end
      item
        Name = '@AContactname'
        Attributes = [paNullable]
        DataType = ftString
        Size = 64
        Value = Null
      end
      item
        Name = '@AContactEmail'
        Attributes = [paNullable]
        DataType = ftString
        Size = 128
        Value = Null
      end>
    AutoOpen = False
    Left = 560
    Top = 88
  end
  object adoqryMailingSignature: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@language'
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT '
      
        '  dbo.UDF_REP_TRANSLATETEKST('#39'MailingConfirm'#39' , :@language) +F_S' +
        'IGNATURE as F_SIGNATURE'
      'FROM V_REP_MAILING_SIGNATURE')
    AutoOpen = False
    Left = 288
    Top = 440
    object adoqryMailingSignatureF_SIGNATURE: TStringField
      FieldName = 'F_SIGNATURE'
      ReadOnly = True
      Size = 1000
    end
  end
  object P_MARK_CONFREP_SENT: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_MARK_CONFREP_SENT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ACONF_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 560
    Top = 240
  end
  object adospAttestRegistered: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'SP_SES_ATTEST_REGISTERED;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@SessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 672
    Top = 376
  end
  object prvAttestRegistered: TFVBFFCDataSetProvider
    DataSet = adospAttestRegistered
    Left = 672
    Top = 424
  end
  object adoqryMailingBedrijfVCASignature: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@language1'
        Size = -1
        Value = Null
      end
      item
        Name = '@language2'
        Size = -1
        Value = Null
      end
      item
        Name = '@language3'
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT '
      
        '  dbo.UDF_REP_TRANSLATETEKST('#39'MailingConfirmVCA1'#39' , :@language1)' +
        ' +dbo.UDF_REP_TRANSLATETEKST('#39'MailingConfirmVCA2'#39' , :@language2)' +
        ' +dbo.UDF_REP_TRANSLATETEKST('#39'MailingConfirmVCA3'#39' , :@language3)' +
        ' +F_SIGNATURE as F_SIGNATURE FROM V_REP_MAILING_VCA_SIGNATURE'
      '')
    AutoOpen = False
    Left = 288
    Top = 496
    object StringField1: TStringField
      FieldName = 'F_SIGNATURE'
      ReadOnly = True
      Size = 1000
    end
  end
  object adoqryMailingVCASignature: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = '@language'
        Size = -1
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT '
      
        '  dbo.UDF_REP_TRANSLATETEKST('#39'MailingConfirmVCA'#39' , :@language) +' +
        'F_SIGNATURE as F_SIGNATURE'
      'FROM V_REP_MAILING_VCA_SIGNATURE')
    AutoOpen = False
    Left = 288
    Top = 552
    object StringField2: TStringField
      FieldName = 'F_SIGNATURE'
      ReadOnly = True
      Size = 1000
    end
  end
end
