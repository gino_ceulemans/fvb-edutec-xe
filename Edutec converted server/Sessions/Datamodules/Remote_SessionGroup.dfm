inherited rdtmSessionGroup: TrdtmSessionGroup
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_SESSIONGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_SESSIONGROUP_NAME '
      'FROM'
      '  V_SES_GROUP'
      '')
    object adoqryListF_SESSIONGROUP_ID: TIntegerField
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryListF_SESSIONGROUP_NAME: TStringField
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_SESSIONGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_SESSIONGROUP_NAME '
      'FROM'
      '  V_SES_GROUP'
      '')
    object adoqryRecordF_SESSIONGROUP_ID: TIntegerField
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordF_SESSIONGROUP_NAME: TStringField
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
end
