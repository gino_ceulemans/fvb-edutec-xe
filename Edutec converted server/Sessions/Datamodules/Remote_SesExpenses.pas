{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SES_EXPENSES record.

  @Name       Remote_SesExpenses
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_SesExpenses;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TdtmRdtmSesExpenses = class(TrdtmEduBase, IrdtmSesExpenses)
    adoqryListF_EXPENSE_ID: TIntegerField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_SESSION_NAME: TStringField;
    adoqryListF_EXPENSE_TYPE: TIntegerField;
    adoqryListF_EXPENSE_DESC: TMemoField;
    adoqryListF_EXPENSE_AMOUNT: TFloatField;
    adoqryRecordF_EXPENSE_ID: TIntegerField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_SESSION_NAME: TStringField;
    adoqryRecordF_EXPENSE_TYPE: TIntegerField;
    adoqryRecordF_EXPENSE_DESC: TMemoField;
    adoqryRecordF_EXPENSE_AMOUNT: TFloatField;
    adoqryRecordF_EXPENSE_DATE: TDateTimeField;
    adoqryListF_EXPENSE_DATE: TDateTimeField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    { Public declarations }
  end;

var
  ofSesExpenses : TComponentFactory;
  
implementation

{$R *.DFM}

class procedure TdtmRdtmSesExpenses.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TdtmRdtmSesExpenses.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TdtmRdtmSesExpenses.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TdtmRdtmSesExpenses.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TdtmRdtmSesExpenses.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TdtmRdtmSesExpenses.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TdtmRdtmSesExpenses.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TdtmRdtmSesExpenses.GetTableName: String;
begin
  Result := 'T_SES_EXPENSE';
end;

initialization
  ofSesExpenses := TComponentFactory.Create(ComServer, TdtmRdtmSesExpenses,
    Class_RdtmSesExpenses, ciInternal, tmApartment);
end.
