{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_SES_ENROL.

  @Name       Remote_Enrolment
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  10/11/2005   sLesage              Changes for Attestation.
  30/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Enrolment;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmEnrolment = class(TrdtmEduBase, IrdtmEnrolment)
    adoqryListF_ENROL_ID: TIntegerField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_SESSION_NAME: TStringField;
    adoqryListF_PERSON_ID: TIntegerField;
    adoqryListF_LASTNAME: TStringField;
    adoqryListF_FIRSTNAME: TStringField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_STATUS_ID: TIntegerField;
    adoqryListF_STATUS_NAME: TStringField;
    adoqryListF_EVALUATION: TBooleanField;
    adoqryListF_CERTIFICATE: TBooleanField;
    adoqryListF_INVOICED: TBooleanField;
    adoqryListF_LAST_MINUTE: TBooleanField;
    adoqryRecordF_ENROL_ID: TIntegerField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryRecordF_SESSION_NAME: TStringField;
    adoqryRecordF_PERSON_ID: TIntegerField;
    adoqryRecordF_LASTNAME: TStringField;
    adoqryRecordF_FIRSTNAME: TStringField;
    adoqryRecordF_SOCSEC_NR: TStringField;
    adoqryRecordF_STATUS_ID: TIntegerField;
    adoqryRecordF_STATUS_NAME: TStringField;
    adoqryRecordF_EVALUATION: TBooleanField;
    adoqryRecordF_CERTIFICATE: TBooleanField;
    adoqryRecordF_COMMENT: TStringField;
    adoqryRecordF_ENROLMENT_DATE: TDateTimeField;
    adoqryRecordF_INVOICED: TBooleanField;
    adoqryRecordF_LAST_MINUTE: TBooleanField;
    adoqryRecordF_HOURS: TIntegerField;
    adoqryRecordF_FVB_FILE_ID: TIntegerField;
    adoqryRecordF_HOURS_PRESENT: TSmallintField;
    adoqryRecordF_HOURS_ABSENT_JUSTIFIED: TSmallintField;
    adoqryRecordF_HOURS_ABSENT_ILLEGIT: TSmallintField;
    adoqryListF_HOURS: TIntegerField;
    adoqryListF_HOURS_PRESENT: TSmallintField;
    adoqryListF_HOURS_ABSENT_JUSTIFIED: TSmallintField;
    adoqryListF_HOURS_ABSENT_ILLEGIT: TSmallintField;
    adoqryListF_PRICING_SCHEME: TIntegerField;
    adoqryRecordF_PRICING_SCHEME: TIntegerField;
    adoqryListF_TOTAL_SESSION_HOURS: TSmallintField;
    adoqryRecordF_TOTAL_SESSION_HOURS: TSmallintField;
    adospSES_SESSION: TFVBFFCStoredProc;
    prvSES_SESSION: TFVBFFCDataSetProvider;
    adoqryListF_ROLE_ID: TIntegerField;
    adoqryRecordF_ROLE_ID: TIntegerField;
    adoqryRecordF_NAME: TStringField;
    adoqryListF_NAME: TStringField;
    adoqryListF_PRICE_WORKER: TFloatField;
    adoqryListF_PRICE_CLERK: TFloatField;
    adoqryListF_PRICE_OTHER: TFloatField;
    adoqryRecordF_PRICE_WORKER: TFloatField;
    adoqryRecordF_PRICE_CLERK: TFloatField;
    adoqryRecordF_PRICE_OTHER: TFloatField;
    adoqryUpdateCertificateFields: TADOQuery;
    adoqryListF_CERTIFICATE_PRINT_DATE: TDateTimeField;
    adoqryRecordF_CERTIFICATE_PRINT_DATE: TDateTimeField;
    adoqryListF_SELECTED_PRICE: TFloatField;
    adoqryRecordF_SELECTED_PRICE: TFloatField;
    adospMoveToSession: TFVBFFCStoredProc;
    prvMoveToSession: TFVBFFCDataSetProvider;
    adoqryListF_ROLEGROUP_NAME: TStringField;
    qryGetDefaultPrice: TFVBFFCQuery;
    qryGetDefaultPriceF_PRICE_WORKER: TFloatField;
    qryGetDefaultPriceF_PRICE_CLERK: TFloatField;
    qryGetDefaultPriceF_PRICE_OTHER: TFloatField;
    adoqryListF_START_DATE: TDateTimeField;
    adoqryListF_NO_PARTICIPANTS: TIntegerField;
    adoqryListF_CODE: TStringField;
    adoqryListF_CONSTRUCT: TBooleanField;
    adoqryRecordF_PARTNER_WINTER_ID: TIntegerField;
    adoqryRecordF_PARTNER_WINTER_NAME: TStringField;
    adoqryListF_PARTNER_WINTER_ID: TIntegerField;
    adoqryListF_PARTNER_WINTER_NAME: TStringField;
    adoqryListF_ORGTYPE_ID: TIntegerField;
    adoqryListF_SCORE: TIntegerField;
    adoqryListF_MAX_SCORE: TIntegerField;
    adoqryRecordF_SCORE: TIntegerField;
    adoqryRecordF_MAX_SCORE: TIntegerField;
    adoqryRecordF_MAX_PUNTEN: TIntegerField;
    adoqryListF_MAX_PUNTEN: TIntegerField;
    adoqryListF_PROGRAM_TYPE: TIntegerField;
    adoqryRecordF_PROGRAM_TYPE: TIntegerField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    procedure MarkAsCertificatePrinted(const EnrollmentList: WideString);
      safecall;
    function GetDefaultPrice(TussenKomstType, SessieID: SYSINT): Double;
      safecall;
    { Public declarations }
  end;

var
  ofEnrolment : TComponentFactory;
  
implementation

uses Remote_EduMain;

{$R *.DFM}

class procedure TrdtmEnrolment.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmEnrolment.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmEnrolment.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmEnrolment.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmEnrolment.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmEnrolment.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmEnrolment.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmEnrolment.GetTableName: String;
begin
  Result := 'T_SES_ENROL';
end;

function TrdtmEnrolment.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

{*****************************************************************************
  Name           : MarkAsCertificatePrinted
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : For the given enrollments set the F_CERTIFICATE and
                   F_CERTIFICATE_PRINT_DATE fields.
                   EnrollmentList should be a ,-separated list of ID's
  History        :

  Date         By                   Description
  ----         --                   -----------
  08/06/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
procedure TrdtmEnrolment.MarkAsCertificatePrinted(
  const EnrollmentList: WideString);
begin
  with adoqryUpdateCertificateFields do
  begin
    SQL.Text := 'update T_SES_ENROL set F_CERTIFICATE = 1, F_CERTIFICATE_PRINT_DATE=getdate()' +
      ' where F_ENROL_ID in (' + EnrollmentList + ')';
    ExecSQL;
  end;
end;

//Voor een gegeven tussenkomst geeft het factuurbedrag terug voor de inschrijving en de gegeven sessie
function TrdtmEnrolment.GetDefaultPrice(TussenKomstType,
  SessieID: SYSINT): Double;
begin
  result := 0;
  with qryGetDefaultPrice do
  begin
    close;
    Parameters.ParamByName('F_SESSION_ID').Value := SessieID;
    open;
    if (not Eof) then
    begin
      case TussenKomstType of
        0: //geen tussenkomst
          result := qryGetDefaultPriceF_PRICE_OTHER.AsFloat;
        1: //tussenkomst FVB
          result := qryGetDefaultPriceF_PRICE_WORKER.AsFloat;
        2: //tussenkomst Cevora
          result := qryGetDefaultPriceF_PRICE_CLERK.AsFloat;
      end;
    end;
    close;
  end;
end;

initialization
  ofEnrolment := TComponentFactory.Create(ComServer, TrdtmEnrolment,
    Class_rdtmEnrolment, ciInternal, tmApartment);
end.
