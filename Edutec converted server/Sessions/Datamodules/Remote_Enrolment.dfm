inherited rdtmEnrolment: TrdtmEnrolment
  OldCreateOrder = True
  Height = 466
  Width = 596
  inherited adoqryList: TFVBFFCQuery
    AutoCalcFields = False
    CursorType = ctStatic
    ParamCheck = False
    Prepared = True
    SQL.Strings = (
      'SELECT'
      '  F_ENROL_ID,'
      '  F_SESSION_ID,'
      '  F_SESSION_NAME,'
      '  F_PERSON_ID,'
      '  F_LASTNAME,'
      '  F_FIRSTNAME,'
      '  [F_NAME],'
      '  F_SOCSEC_NR,'
      '  F_STATUS_ID,'
      '  F_STATUS_NAME,'
      '  F_EVALUATION,'
      '  F_CERTIFICATE,'
      '  F_INVOICED,'
      '  F_LAST_MINUTE,'
      '  F_PRICING_SCHEME,'
      '/*  F_PRICE_SESSION,'
      '  F_PRICE_DAY,'
      '  F_PRICE_ENROL, */'
      '  F_PRICE_WORKER,'
      '  F_PRICE_CLERK,'
      '  F_PRICE_OTHER,'
      '  F_HOURS,'
      '  [F_TOTAL_SESSION_HOURS],'
      '  F_HOURS_PRESENT,'
      '  F_HOURS_ABSENT_JUSTIFIED,'
      '  F_HOURS_ABSENT_ILLEGIT,'
      ' F_SCORE,'
      ' F_MAX_SCORE,'
      '  F_ROLE_ID,'
      '  F_CERTIFICATE_PRINT_DATE,'
      '  F_SELECTED_PRICE,'
      '  F_ROLEGROUP_NAME,'
      '  F_START_DATE,'
      '  F_NO_PARTICIPANTS,'
      '  F_CODE,'
      '  F_CONSTRUCT,'
      '  F_PARTNER_WINTER_ID,'
      '  F_PARTNER_WINTER_NAME,'
      '  F_ORGTYPE_ID,'
      '  F_MAX_PUNTEN,'
      '  F_PROGRAM_TYPE'
      'FROM'
      '  V_SES_ENROL')
    Left = 48
    object adoqryListF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryListF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_EVALUATION: TBooleanField
      FieldName = 'F_EVALUATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CERTIFICATE: TBooleanField
      FieldName = 'F_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_INVOICED: TBooleanField
      FieldName = 'F_INVOICED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_LAST_MINUTE: TBooleanField
      FieldName = 'F_LAST_MINUTE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_HOURS: TIntegerField
      FieldName = 'F_HOURS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_TOTAL_SESSION_HOURS: TSmallintField
      FieldName = 'F_TOTAL_SESSION_HOURS'
      ProviderFlags = []
      Required = True
    end
    object adoqryListF_HOURS_PRESENT: TSmallintField
      FieldName = 'F_HOURS_PRESENT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_HOURS_ABSENT_JUSTIFIED: TSmallintField
      FieldName = 'F_HOURS_ABSENT_JUSTIFIED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_HOURS_ABSENT_ILLEGIT: TSmallintField
      FieldName = 'F_HOURS_ABSENT_ILLEGIT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object adoqryListF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
    end
    object adoqryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_CLERK: TFloatField
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_CERTIFICATE_PRINT_DATE: TDateTimeField
      FieldName = 'F_CERTIFICATE_PRINT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SELECTED_PRICE: TFloatField
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ReadOnly = True
      Size = 64
    end
    object adoqryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object adoqryListF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = []
    end
    object adoqryListF_CONSTRUCT: TBooleanField
      FieldName = 'F_CONSTRUCT'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryListF_PARTNER_WINTER_ID: TIntegerField
      FieldName = 'F_PARTNER_WINTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_PARTNER_WINTER_NAME: TStringField
      FieldName = 'F_PARTNER_WINTER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = []
    end
    object adoqryListF_SCORE: TIntegerField
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MAX_SCORE: TIntegerField
      FieldName = 'F_MAX_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_MAX_PUNTEN: TIntegerField
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = []
    end
    object adoqryListF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = []
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    ParamCheck = False
    SQL.Strings = (
      'SELECT '
      '  F_ENROL_ID, '
      '  F_SESSION_ID, '
      '  F_SESSION_NAME, '
      '  F_PERSON_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME,'
      '  [F_NAME], '
      '  F_SOCSEC_NR, '
      '  F_STATUS_ID, '
      '  F_STATUS_NAME, '
      '  F_EVALUATION, '
      '  F_CERTIFICATE, '
      '  F_COMMENT, '
      '  F_ENROLMENT_DATE, '
      '  F_INVOICED, '
      '  F_LAST_MINUTE, '
      '  F_HOURS, '
      '  [F_TOTAL_SESSION_HOURS],'
      '  F_HOURS_PRESENT,'
      '  F_HOURS_ABSENT_JUSTIFIED,'
      '  F_HOURS_ABSENT_ILLEGIT,'
      '  F_FVB_FILE_ID, '
      '  F_PRICING_SCHEME, '
      '/*  F_PRICE_SESSION, '
      '  F_PRICE_DAY, '
      '  F_PRICE_ENROL, */'
      '  F_PRICE_WORKER, '
      '  F_PRICE_CLERK, '
      '  F_PRICE_OTHER,'
      ' F_SCORE,'
      ' F_MAX_SCORE,'
      ' F_ROLE_ID,'
      '   F_CERTIFICATE_PRINT_DATE,'
      '  F_SELECTED_PRICE,'
      '  F_PARTNER_WINTER_ID,'
      '  F_PARTNER_WINTER_NAME,'
      '  F_MAX_PUNTEN,'
      '  F_PROGRAM_TYPE'
      'FROM '
      '  V_SES_ENROL')
    object adoqryRecordF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryRecordF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_EVALUATION: TBooleanField
      FieldName = 'F_EVALUATION'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CERTIFICATE: TBooleanField
      FieldName = 'F_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRecordF_ENROLMENT_DATE: TDateTimeField
      FieldName = 'F_ENROLMENT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_INVOICED: TBooleanField
      FieldName = 'F_INVOICED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_LAST_MINUTE: TBooleanField
      FieldName = 'F_LAST_MINUTE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_FVB_FILE_ID: TIntegerField
      FieldName = 'F_FVB_FILE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_HOURS: TIntegerField
      FieldName = 'F_HOURS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_TOTAL_SESSION_HOURS: TSmallintField
      FieldName = 'F_TOTAL_SESSION_HOURS'
      ProviderFlags = []
      Required = True
    end
    object adoqryRecordF_HOURS_PRESENT: TSmallintField
      FieldName = 'F_HOURS_PRESENT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_HOURS_ABSENT_JUSTIFIED: TSmallintField
      FieldName = 'F_HOURS_ABSENT_JUSTIFIED'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_HOURS_ABSENT_ILLEGIT: TSmallintField
      FieldName = 'F_HOURS_ABSENT_ILLEGIT'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object adoqryRecordF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_CLERK: TFloatField
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_CERTIFICATE_PRINT_DATE: TDateTimeField
      FieldName = 'F_CERTIFICATE_PRINT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_SELECTED_PRICE: TFloatField
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = []
    end
    object adoqryRecordF_PARTNER_WINTER_ID: TIntegerField
      FieldName = 'F_PARTNER_WINTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_PARTNER_WINTER_NAME: TStringField
      FieldName = 'F_PARTNER_WINTER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SCORE: TIntegerField
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MAX_SCORE: TIntegerField
      FieldName = 'F_MAX_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordF_MAX_PUNTEN: TIntegerField
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = []
    end
    object adoqryRecordF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = []
    end
  end
  object adospSES_SESSION: TFVBFFCStoredProc
    ProcedureName = 'SP_SES_SESSION'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ACTION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@PRICE_WORKER'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@PRICE_CLERK'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@PRICE_OTHER'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 18
        Value = Null
      end
      item
        Name = '@SESSION_DATE'
        DataType = ftDateTime
        Value = Null
      end>
    AutoOpen = False
    Left = 240
    Top = 24
  end
  object prvSES_SESSION: TFVBFFCDataSetProvider
    DataSet = adospSES_SESSION
    Left = 240
    Top = 72
  end
  object adoqryUpdateCertificateFields: TADOQuery
    Connection = rdtmEDUMain.adocnnMain
    Parameters = <
      item
        Name = 'F_ENROL_ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'update T_SES_ENROL'
      'set F_CERTIFICATE = 1, F_CERTIFICATE_PRINT_DATE = getdate()'
      'where F_ENROL_ID = :F_ENROL_ID')
    Left = 64
    Top = 152
  end
  object adospMoveToSession: TFVBFFCStoredProc
    ProcedureName = 'SP_MOVE_ENR2SESSION'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@EnrollmnentID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@NewSessionID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    AutoOpen = False
    Left = 240
    Top = 120
  end
  object prvMoveToSession: TFVBFFCDataSetProvider
    DataSet = adospMoveToSession
    Left = 240
    Top = 176
  end
  object qryGetDefaultPrice: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'F_SESSION_ID'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select'
      '  F_PRICE_WORKER,'
      '  F_PRICE_CLERK,'
      '  F_PRICE_OTHER'
      'from'
      '  T_SES_SESSION'
      'where'
      '  F_SESSION_ID = :F_SESSION_ID')
    AutoOpen = False
    Left = 64
    Top = 216
    object qryGetDefaultPriceF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
    end
    object qryGetDefaultPriceF_PRICE_CLERK: TFloatField
      FieldName = 'F_PRICE_CLERK'
    end
    object qryGetDefaultPriceF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
    end
  end
end
