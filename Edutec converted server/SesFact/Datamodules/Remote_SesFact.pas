unit Remote_SesFact;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB, ActiveX;

type
  TrdtmSesFact = class(TrdtmEduBase, IrdtmSesFact)
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryListF_LAND: TStringField;
    adoqryListF_ALFA_CODE: TStringField;
    adoqryListF_STRAAT: TStringField;
    adoqryListF_HUISNUMMER: TStringField;
    adoqryListF_BUSNUMMER: TStringField;
    adoqryListF_WOONPLAATS: TStringField;
    adoqryListF_TAAL: TIntegerField;
    adoqryListF_TELEFOON_NUMMER: TStringField;
    adoqryListF_DOCUMENT_NUMMER: TIntegerField;
    adoqryListF_ONZE_REFERTE: TStringField;
    adoqryListF_REFERTE: TStringField;
    adoqryListF_BEDRAG_VALUTA: TFloatField;
    adoqryListF_VERKOOPREKENING: TIntegerField;
    adoqryListF_ANALYTISCHE_SLEUTEL_1: TStringField;
    adoqryListF_BOEKHOUDPERIODE: TIntegerField;
    adoqryListF_NAAM: TStringField;
    adoqryListF_DOCUMENT_DATUM: TDateTimeField;
    adoqryListF_VERVALDATUM: TDateTimeField;
    adoqryListF_KLANTNUMMER: TIntegerField;
    adoqryListF_POSTNUMMER: TIntegerField;

    adospSES_FACT: TFVBFFCStoredProc;
    prvSES_FACT: TFVBFFCDataSetProvider;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofSesFact : TComponentFactory;
  
implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain;

{$R *.DFM}

class procedure TrdtmSesFact.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmSesFact.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmSesFact.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmSesFact.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmSesFact.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmSesFact.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmSesFact.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmSesFact.GetTableName: String;
begin
  Result := 'T_SES_FACT';
end;

function TrdtmSesFact.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofSesFact := TComponentFactory.Create(ComServer, TrdtmSesFact,
    Class_RdtmSesFact, ciInternal, tmApartment);

end.
