inherited rdtmSesFact: TrdtmSesFact
  OldCreateOrder = True
  Height = 192
  Width = 349
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '[F_SESSION_ID], '
      '[F_ORGANISATION_ID], '
      '[F_KLANTNUMMER], '
      '[F_LAND], '
      '[F_ALFA_CODE], '
      '[F_NAAM], '
      '[F_STRAAT], '
      '[F_HUISNUMMER], '
      '[F_BUSNUMMER], '
      '[F_POSTNUMMER], '
      '[F_WOONPLAATS], '
      '[F_TAAL], '
      '[F_TELEFOON_NUMMER], '
      '[F_DOCUMENT_NUMMER], '
      '[F_DOCUMENT_DATUM], '
      '[F_VERVALDATUM], '
      '[F_ONZE_REFERTE], '
      '[F_REFERTE], '
      '[F_BEDRAG_VALUTA], '
      '[F_VERKOOPREKENING], '
      '[F_ANALYTISCHE_SLEUTEL_1], '
      '[F_BOEKHOUDPERIODE] '
      'FROM '
      '[dbo].[V_SES_FACT_CUMUL]')
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_LAND: TStringField
      FieldName = 'F_LAND'
      ProviderFlags = [pfInUpdate]
      Size = 3
    end
    object adoqryListF_ALFA_CODE: TStringField
      FieldName = 'F_ALFA_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_STRAAT: TStringField
      FieldName = 'F_STRAAT'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_HUISNUMMER: TStringField
      FieldName = 'F_HUISNUMMER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryListF_BUSNUMMER: TStringField
      FieldName = 'F_BUSNUMMER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryListF_WOONPLAATS: TStringField
      FieldName = 'F_WOONPLAATS'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_TAAL: TIntegerField
      FieldName = 'F_TAAL'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_TELEFOON_NUMMER: TStringField
      FieldName = 'F_TELEFOON_NUMMER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_DOCUMENT_NUMMER: TIntegerField
      FieldName = 'F_DOCUMENT_NUMMER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ONZE_REFERTE: TStringField
      FieldName = 'F_ONZE_REFERTE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_REFERTE: TStringField
      FieldName = 'F_REFERTE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_BEDRAG_VALUTA: TFloatField
      FieldName = 'F_BEDRAG_VALUTA'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_VERKOOPREKENING: TIntegerField
      FieldName = 'F_VERKOOPREKENING'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object adoqryListF_ANALYTISCHE_SLEUTEL_1: TStringField
      FieldName = 'F_ANALYTISCHE_SLEUTEL_1'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryListF_BOEKHOUDPERIODE: TIntegerField
      FieldName = 'F_BOEKHOUDPERIODE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_NAAM: TStringField
      FieldName = 'F_NAAM'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object adoqryListF_DOCUMENT_DATUM: TDateTimeField
      FieldName = 'F_DOCUMENT_DATUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_VERVALDATUM: TDateTimeField
      FieldName = 'F_VERVALDATUM'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_KLANTNUMMER: TIntegerField
      FieldName = 'F_KLANTNUMMER'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_POSTNUMMER: TIntegerField
      FieldName = 'F_POSTNUMMER'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  object adospSES_FACT: TFVBFFCStoredProc
    Connection = dtmEDUMainClient.adocnnMain
    ProcedureName = 'SP_SES_FACT'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ACTION'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@F_SESSION_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_ORGANISATION_ID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_SALE_NR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_DOC_NR'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@F_ACTIVE'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end>
    AutoOpen = False
    Left = 240
    Top = 24
  end
  object prvSES_FACT: TFVBFFCDataSetProvider
    DataSet = adospSES_FACT
    Left = 240
    Top = 72
  end
end
