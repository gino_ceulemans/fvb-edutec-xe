unit EdutecServerD10_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 31/05/2017 14:17:40 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Development\fvb-edutec-xe\Edutec converted server\EdutecServerD10 (1)
// LIBID: {4D14D735-36D3-49C7-A188-B46DE1C8E399}
// LCID: 0
// Helpfile:
// HelpString: EdutecServerD10 Library
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v1.0 Midas, (midas.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, Midas, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;



// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  EdutecServerD10MajorVersion = 1;
  EdutecServerD10MinorVersion = 0;

  LIBID_EdutecServerD10: TGUID = '{4D14D735-36D3-49C7-A188-B46DE1C8E399}';

  IID_IrdtmEDUMain: TGUID = '{744F6596-F9AD-49FD-AB1F-E3E0192BE904}';
  CLASS_rdtmEDUMain: TGUID = '{3664AA21-C536-4A8B-90C2-C80EBF4DE6FD}';
  IID_IrdtmEduBase: TGUID = '{45541AD4-8539-4F1E-9158-D20B58911AF7}';
  CLASS_rdtmEduBase: TGUID = '{DE8E3F87-CD1D-414D-9DE9-2DDBF4F2B6A8}';
  IID_IrdtmCountry: TGUID = '{B4594C5C-4189-45D6-B4F8-3D07716AFE14}';
  CLASS_rdtmCountry: TGUID = '{A14D7E28-406C-474B-BF24-654B3E9510C3}';
  IID_IrdtmCountryPart: TGUID = '{E386E179-9B56-4CB0-B61E-EB43340DAC86}';
  CLASS_rdtmCountryPart: TGUID = '{6CB15F85-0AFD-43BB-8DB9-090DC59BB6CA}';
  IID_IrdtmProvince: TGUID = '{E399C5B0-1409-4C3A-BD58-E5AB4B22AD50}';
  CLASS_rdtmProvince: TGUID = '{EE292D94-1AFA-4C73-A21A-5E8F5DAA6BFB}';
  IID_IrdtmLanguage: TGUID = '{AB266A9F-ABB7-4204-AB1A-F73A8F498215}';
  CLASS_rdtmLanguage: TGUID = '{28695141-B5B7-4680-8B9F-C101E85F63BA}';
  IID_IrdtmPostalCode: TGUID = '{1EEB71F2-C970-42E9-B9EE-B1302EC8672B}';
  CLASS_rdtmPostalCode: TGUID = '{F4016E4A-5A30-4CF1-BC79-EDFAC6B46F6E}';
  IID_IrdtmStatus: TGUID = '{38A619C6-16EB-41F9-9C15-5B63A0F4CC88}';
  CLASS_rdtmStatus: TGUID = '{4F02E63B-FFBF-4D7C-BF3F-19970AAACBEB}';
  IID_IrdtmDiploma: TGUID = '{A1911829-4DF0-4BDF-A02C-98630A6AAE7A}';
  CLASS_rdtmDiploma: TGUID = '{B38D9CB8-30EF-46C0-88D6-6A040240B6DB}';
  IID_IrdtmSessionGroup: TGUID = '{A02044EB-E8C3-48B1-9423-60D0AFAB494C}';
  CLASS_rdtmSessionGroup: TGUID = '{92245CC7-99B4-454B-88B7-9F889D7270A8}';
  IID_IrdtmRoleGroup: TGUID = '{D2410469-5394-43FE-A04F-C79398EEF997}';
  CLASS_rdtmRoleGroup: TGUID = '{38F2952A-2EA8-4F77-85F7-D02B4FA2CD74}';
  IID_IrdtmSchoolYear: TGUID = '{9C2668EA-3829-4983-BA24-5B8D60CC5E2B}';
  CLASS_rdtmSchoolYear: TGUID = '{9160F751-FDA8-41AA-ADCC-7BA0C8F57E15}';
  IID_IrdtmProgDiscipline: TGUID = '{E38C7789-3D39-4A41-AB15-38DE7DFFCECC}';
  CLASS_rdtmProgDiscipline: TGUID = '{F29FA6E1-238E-40FB-A2DF-87DD062C7EAA}';
  IID_IrdtmUser: TGUID = '{E4CBE81D-5385-4A97-8EEE-221FA3D3A924}';
  CLASS_rdtmUser: TGUID = '{649ED8D1-3A41-4D8C-9DED-075E12FBFE97}';
  IID_IrdtmProfile: TGUID = '{8E7B52D1-39DE-4B01-9B69-77F84BCEA91E}';
  CLASS_rdtmProfile: TGUID = '{AFDB5A2F-EE53-4922-ABE0-D333AF711D1D}';
  IID_IrdtmUserProfile: TGUID = '{E98ED30C-04A2-4F2F-B85E-FCFDA89026EB}';
  CLASS_rdtmUserProfile: TGUID = '{9FA1DF50-90E7-4402-991E-94BDB2CD18BE}';
  IID_IrdtmGender: TGUID = '{56F77B0E-7468-4A6E-96B5-C8F7C5E1BE8F}';
  CLASS_rdtmGender: TGUID = '{073D0B79-7591-4B96-A5FA-D8258E20FB2B}';
  IID_IrdtmTitle: TGUID = '{F9AB5FAC-2636-45F2-93EB-73ECADD1726A}';
  CLASS_rdtmTitle: TGUID = '{363F2D99-925A-4517-94C3-3451150F4CD3}';
  IID_IrdtmMedium: TGUID = '{2D70E93C-5B46-4F97-B963-57DD411FA3C8}';
  CLASS_rdtmMedium: TGUID = '{6439EFD5-BF99-468E-AA60-51EFAD421739}';
  IID_IrdtmOrganisationType: TGUID = '{27CCD308-5D71-4091-8364-F37186FB1AC1}';
  CLASS_rdtmOrganisationType: TGUID = '{E628847D-E5E5-4D56-8168-ED1C9525DD20}';
  IID_IrdtmOrganisation: TGUID = '{F477BE2C-E999-441A-8D95-BE0140644F40}';
  CLASS_rdtmOrganisation: TGUID = '{1091E046-6612-4E0C-9F06-B5FB3EB200AC}';
  IID_IrdtmPerson: TGUID = '{D52313BD-66F4-4EC8-8C27-FE3D21741D7A}';
  CLASS_rdtmPerson: TGUID = '{DE7178CC-532F-4FCE-9B47-AE87C1872B38}';
  IID_IrdtmInstructor: TGUID = '{BECD895C-63C5-4677-985C-480EB3FA4D48}';
  CLASS_rdtmInstructor: TGUID = '{725ABE30-A376-4694-B958-F41853935BA6}';
  IID_IrdtmProgInstructor: TGUID = '{7483D231-473D-4FB1-A1F8-58E8987C6F47}';
  CLASS_rdtmProgInstructor: TGUID = '{43AD9DE4-DEA0-4B27-B404-406D16786A2D}';
  IID_IrdtmRole: TGUID = '{D22636E8-29D5-4FCD-8968-95EE0A55C8BE}';
  CLASS_rdtmRole: TGUID = '{5FBD2F87-05A8-49E6-AD93-8C0371AD1EAB}';
  IID_IrdtmProgProgram: TGUID = '{8E605826-4220-4CE7-972E-C37A283B6103}';
  CLASS_rdtmProgProgram: TGUID = '{810B7990-3DC3-4903-8726-82432E80FEDD}';
  IID_IrdtmSesSession: TGUID = '{ED23B4A9-A052-4C83-90B2-287EB412C209}';
  CLASS_rdtmSesSession: TGUID = '{F316634B-7E5E-4478-BF0C-B6D40EA64C9D}';
  IID_IrdtmCenInfrastructure: TGUID = '{D34D72B8-8BF9-4A42-830C-AA17B0AC73E3}';
  CLASS_rdtmCenInfrastructure: TGUID = '{D8E01ECA-FAAE-47E4-A572-35FFEBE84FE7}';
  IID_IrdtmSesExpenses: TGUID = '{65329748-D2B2-4882-8338-1CC4F1DFD989}';
  CLASS_rdtmSesExpenses: TGUID = '{789BF56A-82D5-4393-B286-46B4F1E5D6EF}';
  IID_IrdtmEnrolment: TGUID = '{A75B586D-57D9-4B22-B7DA-B12DA13AD333}';
  CLASS_rdtmEnrolment: TGUID = '{3194F8D9-0941-4978-BD0B-3C7430952479}';
  IID_IrdtmSesWizard: TGUID = '{EE509BD9-A4B5-46AE-88D7-7C59F2B43A23}';
  CLASS_rdtmSesWizard: TGUID = '{127DE282-B6BA-41B7-8682-92507D625C8E}';
  IID_IrdtmSesInstructorRoom: TGUID = '{7838DFC4-1474-42AA-BCC7-D92AE2A5C748}';
  CLASS_rdtmSesInstructorRoom: TGUID = '{596C8E0B-FDEB-4BD1-9DE8-48078FD76705}';
  IID_IrdtmCenRoom: TGUID = '{FD7C6DD8-FDD4-42D3-85FB-78C27464AE72}';
  CLASS_rdtmCenRoom: TGUID = '{C8DF9738-3F8E-47FF-844C-5CEDC1747167}';
  IID_IrdtmQueryGroup: TGUID = '{45C3EF4C-805F-4418-AEBF-A4C5380F27A9}';
  CLASS_rdtmQueryGroup: TGUID = '{867D9E1E-0DFF-489F-8FE4-F9C5504F9908}';
  IID_IrdtmQuery: TGUID = '{A55D9241-94D2-4C48-ADBD-38227DB869DB}';
  CLASS_rdtmQuery: TGUID = '{DDAB51C4-3EEB-47C6-A536-2A311FB74192}';
  IID_IrdtmParameter: TGUID = '{89A7A2BA-F452-49FC-88F4-3C11182C4748}';
  CLASS_rdtmParameter: TGUID = '{1EF45F62-7ED0-4A35-9120-D5C1368C218A}';
  IID_IrdtmParameterType: TGUID = '{46EC91D6-52BB-4E1F-BBBB-EE6F6CB0AE93}';
  CLASS_rdtmParameterType: TGUID = '{26ECD85D-CD5A-49C1-950A-E55A14AAA2D9}';
  IID_IrdtmLogHistory: TGUID = '{AF7AF5B9-C8AA-4198-AFE4-48B8EB22BA7B}';
  CLASS_rdtmLogHistory: TGUID = '{FDFA618A-3F97-4A16-BB19-F352ACBF576E}';
  IID_IrdtmTableField: TGUID = '{EF0E4931-FD09-425D-AFCF-AF1838E319A4}';
  CLASS_rdtmTableField: TGUID = '{C0FF3DD3-6DCA-4052-84F7-C0C514D6276B}';
  IID_IrdtmQueryWizard: TGUID = '{77D32143-D688-4C84-ABA5-39821213A754}';
  CLASS_rdtmQueryWizard: TGUID = '{77AA1E7D-45EB-48DA-A7CF-610CB04C9585}';
  IID_IrdtmReport: TGUID = '{A3FC3779-A79E-4770-878C-88E02C0F51E1}';
  CLASS_rdtmReport: TGUID = '{8865EF27-DE11-44B7-8FDF-B78141D6C4A5}';
  IID_IrdtmImportOverview: TGUID = '{EAF69794-2A48-44C0-8DA1-81031B631509}';
  CLASS_rdtmImportOverview: TGUID = '{15FD8567-0001-4892-B41F-B42A38EA0366}';
  IID_IrdtmDocCenter: TGUID = '{3B3D1409-7D05-4218-90CC-D9D91F7D30F1}';
  CLASS_rdtmDocCenter: TGUID = '{DF8A78F4-E117-47BB-87E0-B6360D0E209F}';
  IID_IrdtmSesFact: TGUID = '{B1D13108-6DAB-430D-9E6D-CB8ABC4A6CDE}';
  CLASS_rdtmSesFact: TGUID = '{9B0F5AF3-2FF9-41E1-951C-BB14F945D526}';
  IID_IrdtmInvoicing: TGUID = '{361C66D9-B14D-4955-9D41-F0275F60B6BB}';
  CLASS_rdtmInvoicing: TGUID = '{9905646C-9E27-433F-AB4D-8B24D59121A3}';
  IID_IrdtmImport: TGUID = '{7E2601A1-E669-4204-B22D-E2CD9C938445}';
  CLASS_rdtmImport: TGUID = '{0CFC6FC6-AECE-4E00-8951-ACC956B65A80}';
  IID_IrdtmConfirmationReports: TGUID = '{F910CAAF-AED4-4F01-BEAF-291B251B2BBD}';
  CLASS_rdtmConfirmationReports: TGUID = '{75AC01A0-08A4-411C-99AF-2BFEA659EE2E}';
  IID_IrdtmSessionWizard: TGUID = '{5675D53A-E56A-4B81-B6DA-68F8134AEBFB}';
  CLASS_rdtmSessionWizard: TGUID = '{68E7E6CE-C045-48BB-911C-97CF5D4B5874}';
  IID_IrdtmCooperation: TGUID = '{4028964F-DF94-488E-A807-588A1CD04BC3}';
  CLASS_rdtmCooperation: TGUID = '{23CB344F-632B-417A-8806-0F1DF3A53EB1}';
  IID_IrdtmKMO_PORT_Status: TGUID = '{71B729E2-6E86-4BEF-9764-5F1DA030814A}';
  CLASS_rdtmKMO_PORT_Status: TGUID = '{DE4879A7-BCB3-4F11-9823-4C9D58467A9A}';
  IID_IrdtmKMO_Portefeuille: TGUID = '{275703D8-CD09-40A4-AAE8-EFB480F06FAC}';
  CLASS_rdtmKMO_Portefeuille: TGUID = '{8B438B1F-4143-4AE2-9125-994BB4EBDB44}';
  IID_IrdtmKMO_PORT_Payment: TGUID = '{06A23DAB-0461-4994-A8EE-EC9055C459F3}';
  CLASS_rdtmKMO_PORT_Payment: TGUID = '{43E8F4F6-DD44-411B-B2C5-A24D98669D7E}';
  IID_IrdtmKMO_PORT_Link_Port_Payment: TGUID = '{EF936A41-A1BE-46DC-8846-A6E3150EAAB7}';
  CLASS_rdtmKMO_PORT_Link_Port_Payment: TGUID = '{B4E296D8-36EC-4E5C-9396-96D9253D80ED}';
  IID_IrdtmKMO_PORT_Link_Port_Session: TGUID = '{1FFF2FB5-15DA-477A-A40B-62CBC628EA22}';
  CLASS_rdtmKMO_PORT_Link_Port_Session: TGUID = '{1A788E40-E05F-4D80-9CAF-B63B7E0BCC2D}';
  IID_IrdtmImport_Excel: TGUID = '{29633E00-91AE-4C7A-A90A-228A32D728C7}';
  CLASS_rdtmImport_Excel: TGUID = '{F2CA0133-C12C-476D-B763-20E0CB830886}';
  IID_IrdtmProgramType: TGUID = '{A741EC78-9BA5-4196-9DE1-C7D7CBFF9FD5}';
  CLASS_rdtmProgramType: TGUID = '{04E0A44D-B497-4AE1-A0D1-061B801AB3DB}';
  IID_IData_Country: TGUID = '{4C96DE8E-3A5E-4313-9EDD-EDC923266678}';
  CLASS_Data_Country: TGUID = '{C426AAF0-A658-41EE-B3CD-78D5A8653E75}';
  IID_IRemote_Country_Test: TGUID = '{B529E1CF-C623-40C6-8167-BCF87770AC8E}';
  CLASS_Remote_Country_Test: TGUID = '{4912F5F8-D6D6-4407-B38D-1F8595693734}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library
// *********************************************************************//
// Constants for enum ENUM_LOGON
type
  ENUM_LOGON = TOleEnum;
const
  LOGON_OK = $00000000;
  LOGON_CONNECTION_FAILED = $00000001;
  LOGON_APPLICATION_ROLE_FAILED = $00000002;
  LOGON_UNKNOWN_USER = $00000003;
  LOGON_USER_LOCKED = $00000004;
  LOGON_INVALID_PASSWORD = $00000005;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IrdtmEDUMain = interface;
  IrdtmEDUMainDisp = dispinterface;
  IrdtmEduBase = interface;
  IrdtmEduBaseDisp = dispinterface;
  IrdtmCountry = interface;
  IrdtmCountryDisp = dispinterface;
  IrdtmCountryPart = interface;
  IrdtmCountryPartDisp = dispinterface;
  IrdtmProvince = interface;
  IrdtmProvinceDisp = dispinterface;
  IrdtmLanguage = interface;
  IrdtmLanguageDisp = dispinterface;
  IrdtmPostalCode = interface;
  IrdtmPostalCodeDisp = dispinterface;
  IrdtmStatus = interface;
  IrdtmStatusDisp = dispinterface;
  IrdtmDiploma = interface;
  IrdtmDiplomaDisp = dispinterface;
  IrdtmSessionGroup = interface;
  IrdtmSessionGroupDisp = dispinterface;
  IrdtmRoleGroup = interface;
  IrdtmRoleGroupDisp = dispinterface;
  IrdtmSchoolYear = interface;
  IrdtmSchoolYearDisp = dispinterface;
  IrdtmProgDiscipline = interface;
  IrdtmProgDisciplineDisp = dispinterface;
  IrdtmUser = interface;
  IrdtmUserDisp = dispinterface;
  IrdtmProfile = interface;
  IrdtmProfileDisp = dispinterface;
  IrdtmUserProfile = interface;
  IrdtmUserProfileDisp = dispinterface;
  IrdtmGender = interface;
  IrdtmGenderDisp = dispinterface;
  IrdtmTitle = interface;
  IrdtmTitleDisp = dispinterface;
  IrdtmMedium = interface;
  IrdtmMediumDisp = dispinterface;
  IrdtmOrganisationType = interface;
  IrdtmOrganisationTypeDisp = dispinterface;
  IrdtmOrganisation = interface;
  IrdtmOrganisationDisp = dispinterface;
  IrdtmPerson = interface;
  IrdtmPersonDisp = dispinterface;
  IrdtmInstructor = interface;
  IrdtmInstructorDisp = dispinterface;
  IrdtmProgInstructor = interface;
  IrdtmProgInstructorDisp = dispinterface;
  IrdtmRole = interface;
  IrdtmRoleDisp = dispinterface;
  IrdtmProgProgram = interface;
  IrdtmProgProgramDisp = dispinterface;
  IrdtmSesSession = interface;
  IrdtmSesSessionDisp = dispinterface;
  IrdtmCenInfrastructure = interface;
  IrdtmCenInfrastructureDisp = dispinterface;
  IrdtmSesExpenses = interface;
  IrdtmSesExpensesDisp = dispinterface;
  IrdtmEnrolment = interface;
  IrdtmEnrolmentDisp = dispinterface;
  IrdtmSesWizard = interface;
  IrdtmSesWizardDisp = dispinterface;
  IrdtmSesInstructorRoom = interface;
  IrdtmSesInstructorRoomDisp = dispinterface;
  IrdtmCenRoom = interface;
  IrdtmCenRoomDisp = dispinterface;
  IrdtmQueryGroup = interface;
  IrdtmQueryGroupDisp = dispinterface;
  IrdtmQuery = interface;
  IrdtmQueryDisp = dispinterface;
  IrdtmParameter = interface;
  IrdtmParameterDisp = dispinterface;
  IrdtmParameterType = interface;
  IrdtmParameterTypeDisp = dispinterface;
  IrdtmLogHistory = interface;
  IrdtmLogHistoryDisp = dispinterface;
  IrdtmTableField = interface;
  IrdtmTableFieldDisp = dispinterface;
  IrdtmQueryWizard = interface;
  IrdtmQueryWizardDisp = dispinterface;
  IrdtmReport = interface;
  IrdtmReportDisp = dispinterface;
  IrdtmImportOverview = interface;
  IrdtmImportOverviewDisp = dispinterface;
  IrdtmDocCenter = interface;
  IrdtmDocCenterDisp = dispinterface;
  IrdtmSesFact = interface;
  IrdtmSesFactDisp = dispinterface;
  IrdtmInvoicing = interface;
  IrdtmInvoicingDisp = dispinterface;
  IrdtmImport = interface;
  IrdtmImportDisp = dispinterface;
  IrdtmConfirmationReports = interface;
  IrdtmConfirmationReportsDisp = dispinterface;
  IrdtmSessionWizard = interface;
  IrdtmSessionWizardDisp = dispinterface;
  IrdtmCooperation = interface;
  IrdtmCooperationDisp = dispinterface;
  IrdtmKMO_PORT_Status = interface;
  IrdtmKMO_PORT_StatusDisp = dispinterface;
  IrdtmKMO_Portefeuille = interface;
  IrdtmKMO_PortefeuilleDisp = dispinterface;
  IrdtmKMO_PORT_Payment = interface;
  IrdtmKMO_PORT_PaymentDisp = dispinterface;
  IrdtmKMO_PORT_Link_Port_Payment = interface;
  IrdtmKMO_PORT_Link_Port_PaymentDisp = dispinterface;
  IrdtmKMO_PORT_Link_Port_Session = interface;
  IrdtmKMO_PORT_Link_Port_SessionDisp = dispinterface;
  IrdtmImport_Excel = interface;
  IrdtmImport_ExcelDisp = dispinterface;
  IrdtmProgramType = interface;
  IrdtmProgramTypeDisp = dispinterface;
  IData_Country = interface;
  IData_CountryDisp = dispinterface;
  IRemote_Country_Test = interface;
  IRemote_Country_TestDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  rdtmEDUMain = IrdtmEDUMain;
  rdtmEduBase = IrdtmEduBase;
  rdtmCountry = IrdtmCountry;
  rdtmCountryPart = IrdtmCountryPart;
  rdtmProvince = IrdtmProvince;
  rdtmLanguage = IrdtmLanguage;
  rdtmPostalCode = IrdtmPostalCode;
  rdtmStatus = IrdtmStatus;
  rdtmDiploma = IrdtmDiploma;
  rdtmSessionGroup = IrdtmSessionGroup;
  rdtmRoleGroup = IrdtmRoleGroup;
  rdtmSchoolYear = IrdtmSchoolYear;
  rdtmProgDiscipline = IrdtmProgDiscipline;
  rdtmUser = IrdtmUser;
  rdtmProfile = IrdtmProfile;
  rdtmUserProfile = IrdtmUserProfile;
  rdtmGender = IrdtmGender;
  rdtmTitle = IrdtmTitle;
  rdtmMedium = IrdtmMedium;
  rdtmOrganisationType = IrdtmOrganisationType;
  rdtmOrganisation = IrdtmOrganisation;
  rdtmPerson = IrdtmPerson;
  rdtmInstructor = IrdtmInstructor;
  rdtmProgInstructor = IrdtmProgInstructor;
  rdtmRole = IrdtmRole;
  rdtmProgProgram = IrdtmProgProgram;
  rdtmSesSession = IrdtmSesSession;
  rdtmCenInfrastructure = IrdtmCenInfrastructure;
  rdtmSesExpenses = IrdtmSesExpenses;
  rdtmEnrolment = IrdtmEnrolment;
  rdtmSesWizard = IrdtmSesWizard;
  rdtmSesInstructorRoom = IrdtmSesInstructorRoom;
  rdtmCenRoom = IrdtmCenRoom;
  rdtmQueryGroup = IrdtmQueryGroup;
  rdtmQuery = IrdtmQuery;
  rdtmParameter = IrdtmParameter;
  rdtmParameterType = IrdtmParameterType;
  rdtmLogHistory = IrdtmLogHistory;
  rdtmTableField = IrdtmTableField;
  rdtmQueryWizard = IrdtmQueryWizard;
  rdtmReport = IrdtmReport;
  rdtmImportOverview = IrdtmImportOverview;
  rdtmDocCenter = IrdtmDocCenter;
  rdtmSesFact = IrdtmSesFact;
  rdtmInvoicing = IrdtmInvoicing;
  rdtmImport = IrdtmImport;
  rdtmConfirmationReports = IrdtmConfirmationReports;
  rdtmSessionWizard = IrdtmSessionWizard;
  rdtmCooperation = IrdtmCooperation;
  rdtmKMO_PORT_Status = IrdtmKMO_PORT_Status;
  rdtmKMO_Portefeuille = IrdtmKMO_Portefeuille;
  rdtmKMO_PORT_Payment = IrdtmKMO_PORT_Payment;
  rdtmKMO_PORT_Link_Port_Payment = IrdtmKMO_PORT_Link_Port_Payment;
  rdtmKMO_PORT_Link_Port_Session = IrdtmKMO_PORT_Link_Port_Session;
  rdtmImport_Excel = IrdtmImport_Excel;
  rdtmProgramType = IrdtmProgramType;
  Data_Country = IData_Country;
  Remote_Country_Test = IRemote_Country_Test;


// *********************************************************************//
// Declaration of structures, unions and aliases.
// *********************************************************************//
  PWideString1 = ^WideString; {*}


// *********************************************************************//
// Interface: IrdtmEDUMain
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {744F6596-F9AD-49FD-AB1F-E3E0192BE904}
// *********************************************************************//
  IrdtmEDUMain = interface(IAppServer)
    ['{744F6596-F9AD-49FD-AB1F-E3E0192BE904}']
    function GetNewRecordID(const aTableName: WideString): SYSINT; safecall;
    function Get_rdtmCountry: IrdtmEduBase; safecall;
    function Get_rdtmCountryPart: IrdtmEduBase; safecall;
    function GetErrorMessageFromConstraint(const Value: WideString): OleVariant; safecall;
    function Get_rdtmProvince: IrdtmEduBase; safecall;
    function Get_rdtmLanguage: IrdtmEduBase; safecall;
    function Get_rdtmPostalCode: IrdtmEduBase; safecall;
    function Get_rdtmStatus: IrdtmEduBase; safecall;
    function Get_rdtmDiploma: IrdtmEduBase; safecall;
    function Get_rdtmSessionGroup: IrdtmEduBase; safecall;
    function Get_rdtmRoleGroup: IrdtmEduBase; safecall;
    function Get_rdtmSchoolYear: IrdtmEduBase; safecall;
    function Get_rdtmProgDiscipline: IrdtmEduBase; safecall;
    function Get_rdtmUser: IrdtmEduBase; safecall;
    function Get_rdtmProfile: IrdtmEduBase; safecall;
    function Get_rdtmUserProfile: IrdtmEduBase; safecall;
    function Get_rdtmGender: IrdtmEduBase; safecall;
    function Get_rdtmTitle: IrdtmEduBase; safecall;
    function Get_rdtmMedium: IrdtmEduBase; safecall;
    function Get_rdtmOrganisationType: IrdtmEduBase; safecall;
    function Get_rdtmOrganisation: IrdtmEduBase; safecall;
    function Get_rdtmPerson: IrdtmEduBase; safecall;
    function Get_rdtmInstructor: IrdtmEduBase; safecall;
    function Get_rdtmProgInstructor: IrdtmEduBase; safecall;
    function Get_rdtmRole: IrdtmEduBase; safecall;
    function Get_rdtmProgProgram: IrdtmEduBase; safecall;
    function Get_rdtmSesSession: IrdtmEduBase; safecall;
    function Get_rdtmCenInfrastructure: IrdtmEduBase; safecall;
    function Get_rdtmSesExpenses: IrdtmEduBase; safecall;
    function Get_rdtmEnrolment: IrdtmEduBase; safecall;
    function Get_rdtmSesWizard: IrdtmEduBase; safecall;
    function Get_rdtmSesInstructorRoom: IrdtmEduBase; safecall;
    function Get_rdtmCenRoom: IrdtmEduBase; safecall;
    function Get_rdtmQueryGroup: IrdtmEduBase; safecall;
    function Get_rdtmQuery: IrdtmEduBase; safecall;
    function Get_rdtmParameter: IrdtmEduBase; safecall;
    function Get_rdtmParameterType: IrdtmEduBase; safecall;
    function Get_rdtmLogHistory: IrdtmEduBase; safecall;
    function Login(const Username: WideString; const Password: WideString): OleVariant; safecall;
    function Get_rdtmTableField: IrdtmEduBase; safecall;
    function Get_rdtmQueryWizard: IrdtmEduBase; safecall;
    function Get_rdtmReport: IrdtmEduBase; safecall;
    function GetErrorMessageFromNumber(Value: SYSINT): OleVariant; safecall;
    function GetNameForState(StatusID: Integer): WideString; safecall;
    function Get_rdtmImportOverview: IrdtmEduBase; safecall;
    function Get_rdtmDocCenter: IrdtmEduBase; safecall;
    function Get_rdtmSesFact: IrdtmEduBase; safecall;
    function GetServername: WideString; safecall;
    function GetDatabasename: WideString; safecall;
    function GetUserID: WideString; safecall;
    function GetPassword: WideString; safecall;
    function ReportRecordCount(const ViewName: WideString; const DistinctFields: WideString;
                               const KeyField: WideString; const KeyValues: WideString): SYSINT; safecall;
    function Get_rdtmInvoicing: IrdtmEduBase; safecall;
    function Get_rdtmImport: IrdtmEduBase; safecall;
    function GetEnrolIdsFromSessionId(const ASessionId: WideString): WideString; safecall;
    function GetPrinterCertificates: WideString; safecall;
    function GetPrinterLetters: WideString; safecall;
    function Get_rdtmConfirmationReports: IrdtmEduBase; safecall;
    procedure PrepareConfirmationReports(const ASessionId: WideString); safecall;
    function GetCurUser: WideString; safecall;
    function Get_rdtmSessionWizard: IrdtmEduBase; safecall;
    function GetPrinterEvaluationDoc: WideString; safecall;
    function GetPathCRReports: WideString; safecall;
    function GetConfirmationPathCurUser: WideString; safecall;
    function GetEmailPublFolder: WideString; safecall;
    function getUSERinfo_UserName: WideString; safecall;
    function getUSERinfo_DBName: WideString; safecall;
    function getUSERinfo_SPID: Integer; safecall;
    function getUSERinfo_UserID: Integer; safecall;
    function Get_rdtmCooperation: IrdtmEduBase; safecall;
    function GetNameForCooperation(CooperationID: Integer): WideString; safecall;
    function getUSERinfo_ServerName: WideString; safecall;
    function Get_rdtmKMO_PORT_Status: IrdtmEduBase; safecall;
    function Get_rdtmKMO_Portefeuille: IrdtmEduBase; safecall;
    function GetNameForKMO_Portefeuille_State(StatusID: Integer): WideString; safecall;
    function Get_rdtmKMO_PORT_Payment: IrdtmEduBase; safecall;
    function Get_rdtmKMO_PORT_Link_Port_Payment: IrdtmEduBase; safecall;
    function Get_rdtmKMO_PORT_Link_Port_Session: IrdtmEduBase; safecall;
    function Get_rdtmImport_Excel: IrdtmEduBase; safecall;
    function Get_rdtmProgramType: IrdtmEduBase; safecall;
    property rdtmCountry: IrdtmEduBase read Get_rdtmCountry;
    property rdtmCountryPart: IrdtmEduBase read Get_rdtmCountryPart;
    property rdtmProvince: IrdtmEduBase read Get_rdtmProvince;
    property rdtmLanguage: IrdtmEduBase read Get_rdtmLanguage;
    property rdtmPostalCode: IrdtmEduBase read Get_rdtmPostalCode;
    property rdtmStatus: IrdtmEduBase read Get_rdtmStatus;
    property rdtmDiploma: IrdtmEduBase read Get_rdtmDiploma;
    property rdtmSessionGroup: IrdtmEduBase read Get_rdtmSessionGroup;
    property rdtmRoleGroup: IrdtmEduBase read Get_rdtmRoleGroup;
    property rdtmSchoolYear: IrdtmEduBase read Get_rdtmSchoolYear;
    property rdtmProgDiscipline: IrdtmEduBase read Get_rdtmProgDiscipline;
    property rdtmUser: IrdtmEduBase read Get_rdtmUser;
    property rdtmProfile: IrdtmEduBase read Get_rdtmProfile;
    property rdtmUserProfile: IrdtmEduBase read Get_rdtmUserProfile;
    property rdtmGender: IrdtmEduBase read Get_rdtmGender;
    property rdtmTitle: IrdtmEduBase read Get_rdtmTitle;
    property rdtmMedium: IrdtmEduBase read Get_rdtmMedium;
    property rdtmOrganisationType: IrdtmEduBase read Get_rdtmOrganisationType;
    property rdtmOrganisation: IrdtmEduBase read Get_rdtmOrganisation;
    property rdtmPerson: IrdtmEduBase read Get_rdtmPerson;
    property rdtmInstructor: IrdtmEduBase read Get_rdtmInstructor;
    property rdtmProgInstructor: IrdtmEduBase read Get_rdtmProgInstructor;
    property rdtmRole: IrdtmEduBase read Get_rdtmRole;
    property rdtmProgProgram: IrdtmEduBase read Get_rdtmProgProgram;
    property rdtmSesSession: IrdtmEduBase read Get_rdtmSesSession;
    property rdtmCenInfrastructure: IrdtmEduBase read Get_rdtmCenInfrastructure;
    property rdtmSesExpenses: IrdtmEduBase read Get_rdtmSesExpenses;
    property rdtmEnrolment: IrdtmEduBase read Get_rdtmEnrolment;
    property rdtmSesWizard: IrdtmEduBase read Get_rdtmSesWizard;
    property rdtmSesInstructorRoom: IrdtmEduBase read Get_rdtmSesInstructorRoom;
    property rdtmCenRoom: IrdtmEduBase read Get_rdtmCenRoom;
    property rdtmQueryGroup: IrdtmEduBase read Get_rdtmQueryGroup;
    property rdtmQuery: IrdtmEduBase read Get_rdtmQuery;
    property rdtmParameter: IrdtmEduBase read Get_rdtmParameter;
    property rdtmParameterType: IrdtmEduBase read Get_rdtmParameterType;
    property rdtmLogHistory: IrdtmEduBase read Get_rdtmLogHistory;
    property rdtmTableField: IrdtmEduBase read Get_rdtmTableField;
    property rdtmQueryWizard: IrdtmEduBase read Get_rdtmQueryWizard;
    property rdtmReport: IrdtmEduBase read Get_rdtmReport;
    property rdtmImportOverview: IrdtmEduBase read Get_rdtmImportOverview;
    property rdtmDocCenter: IrdtmEduBase read Get_rdtmDocCenter;
    property rdtmSesFact: IrdtmEduBase read Get_rdtmSesFact;
    property rdtmInvoicing: IrdtmEduBase read Get_rdtmInvoicing;
    property rdtmImport: IrdtmEduBase read Get_rdtmImport;
    property rdtmConfirmationReports: IrdtmEduBase read Get_rdtmConfirmationReports;
    property rdtmSessionWizard: IrdtmEduBase read Get_rdtmSessionWizard;
    property rdtmCooperation: IrdtmEduBase read Get_rdtmCooperation;
    property rdtmKMO_PORT_Status: IrdtmEduBase read Get_rdtmKMO_PORT_Status;
    property rdtmKMO_Portefeuille: IrdtmEduBase read Get_rdtmKMO_Portefeuille;
    property rdtmKMO_PORT_Payment: IrdtmEduBase read Get_rdtmKMO_PORT_Payment;
    property rdtmKMO_PORT_Link_Port_Payment: IrdtmEduBase read Get_rdtmKMO_PORT_Link_Port_Payment;
    property rdtmKMO_PORT_Link_Port_Session: IrdtmEduBase read Get_rdtmKMO_PORT_Link_Port_Session;
    property rdtmImport_Excel: IrdtmEduBase read Get_rdtmImport_Excel;
    property rdtmProgramType: IrdtmEduBase read Get_rdtmProgramType;
  end;

// *********************************************************************//
// DispIntf:  IrdtmEDUMainDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {744F6596-F9AD-49FD-AB1F-E3E0192BE904}
// *********************************************************************//
  IrdtmEDUMainDisp = dispinterface
    ['{744F6596-F9AD-49FD-AB1F-E3E0192BE904}']
    function GetNewRecordID(const aTableName: WideString): SYSINT; dispid 301;
    property rdtmCountry: IrdtmEduBase readonly dispid 302;
    property rdtmCountryPart: IrdtmEduBase readonly dispid 303;
    function GetErrorMessageFromConstraint(const Value: WideString): OleVariant; dispid 304;
    property rdtmProvince: IrdtmEduBase readonly dispid 305;
    property rdtmLanguage: IrdtmEduBase readonly dispid 306;
    property rdtmPostalCode: IrdtmEduBase readonly dispid 307;
    property rdtmStatus: IrdtmEduBase readonly dispid 308;
    property rdtmDiploma: IrdtmEduBase readonly dispid 309;
    property rdtmSessionGroup: IrdtmEduBase readonly dispid 310;
    property rdtmRoleGroup: IrdtmEduBase readonly dispid 311;
    property rdtmSchoolYear: IrdtmEduBase readonly dispid 312;
    property rdtmProgDiscipline: IrdtmEduBase readonly dispid 313;
    property rdtmUser: IrdtmEduBase readonly dispid 314;
    property rdtmProfile: IrdtmEduBase readonly dispid 315;
    property rdtmUserProfile: IrdtmEduBase readonly dispid 316;
    property rdtmGender: IrdtmEduBase readonly dispid 317;
    property rdtmTitle: IrdtmEduBase readonly dispid 318;
    property rdtmMedium: IrdtmEduBase readonly dispid 319;
    property rdtmOrganisationType: IrdtmEduBase readonly dispid 320;
    property rdtmOrganisation: IrdtmEduBase readonly dispid 321;
    property rdtmPerson: IrdtmEduBase readonly dispid 322;
    property rdtmInstructor: IrdtmEduBase readonly dispid 323;
    property rdtmProgInstructor: IrdtmEduBase readonly dispid 324;
    property rdtmRole: IrdtmEduBase readonly dispid 325;
    property rdtmProgProgram: IrdtmEduBase readonly dispid 326;
    property rdtmSesSession: IrdtmEduBase readonly dispid 327;
    property rdtmCenInfrastructure: IrdtmEduBase readonly dispid 328;
    property rdtmSesExpenses: IrdtmEduBase readonly dispid 329;
    property rdtmEnrolment: IrdtmEduBase readonly dispid 330;
    property rdtmSesWizard: IrdtmEduBase readonly dispid 331;
    property rdtmSesInstructorRoom: IrdtmEduBase readonly dispid 332;
    property rdtmCenRoom: IrdtmEduBase readonly dispid 333;
    property rdtmQueryGroup: IrdtmEduBase readonly dispid 334;
    property rdtmQuery: IrdtmEduBase readonly dispid 335;
    property rdtmParameter: IrdtmEduBase readonly dispid 336;
    property rdtmParameterType: IrdtmEduBase readonly dispid 337;
    property rdtmLogHistory: IrdtmEduBase readonly dispid 338;
    function Login(const Username: WideString; const Password: WideString): OleVariant; dispid 339;
    property rdtmTableField: IrdtmEduBase readonly dispid 340;
    property rdtmQueryWizard: IrdtmEduBase readonly dispid 341;
    property rdtmReport: IrdtmEduBase readonly dispid 342;
    function GetErrorMessageFromNumber(Value: SYSINT): OleVariant; dispid 343;
    function GetNameForState(StatusID: Integer): WideString; dispid 344;
    property rdtmImportOverview: IrdtmEduBase readonly dispid 345;
    property rdtmDocCenter: IrdtmEduBase readonly dispid 346;
    property rdtmSesFact: IrdtmEduBase readonly dispid 347;
    function GetServername: WideString; dispid 348;
    function GetDatabasename: WideString; dispid 349;
    function GetUserID: WideString; dispid 350;
    function GetPassword: WideString; dispid 351;
    function ReportRecordCount(const ViewName: WideString; const DistinctFields: WideString;
                               const KeyField: WideString; const KeyValues: WideString): SYSINT; dispid 352;
    property rdtmInvoicing: IrdtmEduBase readonly dispid 353;
    property rdtmImport: IrdtmEduBase readonly dispid 354;
    function GetEnrolIdsFromSessionId(const ASessionId: WideString): WideString; dispid 355;
    function GetPrinterCertificates: WideString; dispid 356;
    function GetPrinterLetters: WideString; dispid 357;
    property rdtmConfirmationReports: IrdtmEduBase readonly dispid 358;
    procedure PrepareConfirmationReports(const ASessionId: WideString); dispid 359;
    function GetCurUser: WideString; dispid 360;
    property rdtmSessionWizard: IrdtmEduBase readonly dispid 361;
    function GetPrinterEvaluationDoc: WideString; dispid 362;
    function GetPathCRReports: WideString; dispid 363;
    function GetConfirmationPathCurUser: WideString; dispid 364;
    function GetEmailPublFolder: WideString; dispid 365;
    function getUSERinfo_UserName: WideString; dispid 366;
    function getUSERinfo_DBName: WideString; dispid 367;
    function getUSERinfo_SPID: Integer; dispid 368;
    function getUSERinfo_UserID: Integer; dispid 369;
    property rdtmCooperation: IrdtmEduBase readonly dispid 370;
    function GetNameForCooperation(CooperationID: Integer): WideString; dispid 371;
    function getUSERinfo_ServerName: WideString; dispid 372;
    property rdtmKMO_PORT_Status: IrdtmEduBase readonly dispid 373;
    property rdtmKMO_Portefeuille: IrdtmEduBase readonly dispid 374;
    function GetNameForKMO_Portefeuille_State(StatusID: Integer): WideString; dispid 375;
    property rdtmKMO_PORT_Payment: IrdtmEduBase readonly dispid 376;
    property rdtmKMO_PORT_Link_Port_Payment: IrdtmEduBase readonly dispid 377;
    property rdtmKMO_PORT_Link_Port_Session: IrdtmEduBase readonly dispid 378;
    property rdtmImport_Excel: IrdtmEduBase readonly dispid 379;
    property rdtmProgramType: IrdtmEduBase readonly dispid 380;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmEduBase
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {45541AD4-8539-4F1E-9158-D20B58911AF7}
// *********************************************************************//
  IrdtmEduBase = interface(IAppServer)
    ['{45541AD4-8539-4F1E-9158-D20B58911AF7}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; safecall;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; safecall;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; safecall;
    function GetNewRecordID: SYSINT; safecall;
    procedure Set_ADOConnection(Param1: Integer); safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); safecall;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; safecall;
    property ADOConnection: Integer write Set_ADOConnection;
    property rdtmEDUMain: IrdtmEDUMain write Set_rdtmEDUMain;
  end;

// *********************************************************************//
// DispIntf:  IrdtmEduBaseDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {45541AD4-8539-4F1E-9158-D20B58911AF7}
// *********************************************************************//
  IrdtmEduBaseDisp = dispinterface
    ['{45541AD4-8539-4F1E-9158-D20B58911AF7}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmCountry
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B4594C5C-4189-45D6-B4F8-3D07716AFE14}
// *********************************************************************//
  IrdtmCountry = interface(IrdtmEduBase)
    ['{B4594C5C-4189-45D6-B4F8-3D07716AFE14}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmCountryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B4594C5C-4189-45D6-B4F8-3D07716AFE14}
// *********************************************************************//
  IrdtmCountryDisp = dispinterface
    ['{B4594C5C-4189-45D6-B4F8-3D07716AFE14}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmCountryPart
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E386E179-9B56-4CB0-B61E-EB43340DAC86}
// *********************************************************************//
  IrdtmCountryPart = interface(IrdtmEduBase)
    ['{E386E179-9B56-4CB0-B61E-EB43340DAC86}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmCountryPartDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E386E179-9B56-4CB0-B61E-EB43340DAC86}
// *********************************************************************//
  IrdtmCountryPartDisp = dispinterface
    ['{E386E179-9B56-4CB0-B61E-EB43340DAC86}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmProvince
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E399C5B0-1409-4C3A-BD58-E5AB4B22AD50}
// *********************************************************************//
  IrdtmProvince = interface(IrdtmEduBase)
    ['{E399C5B0-1409-4C3A-BD58-E5AB4B22AD50}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmProvinceDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E399C5B0-1409-4C3A-BD58-E5AB4B22AD50}
// *********************************************************************//
  IrdtmProvinceDisp = dispinterface
    ['{E399C5B0-1409-4C3A-BD58-E5AB4B22AD50}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmLanguage
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AB266A9F-ABB7-4204-AB1A-F73A8F498215}
// *********************************************************************//
  IrdtmLanguage = interface(IrdtmEduBase)
    ['{AB266A9F-ABB7-4204-AB1A-F73A8F498215}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmLanguageDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AB266A9F-ABB7-4204-AB1A-F73A8F498215}
// *********************************************************************//
  IrdtmLanguageDisp = dispinterface
    ['{AB266A9F-ABB7-4204-AB1A-F73A8F498215}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmPostalCode
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1EEB71F2-C970-42E9-B9EE-B1302EC8672B}
// *********************************************************************//
  IrdtmPostalCode = interface(IrdtmEduBase)
    ['{1EEB71F2-C970-42E9-B9EE-B1302EC8672B}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmPostalCodeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1EEB71F2-C970-42E9-B9EE-B1302EC8672B}
// *********************************************************************//
  IrdtmPostalCodeDisp = dispinterface
    ['{1EEB71F2-C970-42E9-B9EE-B1302EC8672B}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmStatus
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {38A619C6-16EB-41F9-9C15-5B63A0F4CC88}
// *********************************************************************//
  IrdtmStatus = interface(IrdtmEduBase)
    ['{38A619C6-16EB-41F9-9C15-5B63A0F4CC88}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmStatusDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {38A619C6-16EB-41F9-9C15-5B63A0F4CC88}
// *********************************************************************//
  IrdtmStatusDisp = dispinterface
    ['{38A619C6-16EB-41F9-9C15-5B63A0F4CC88}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmDiploma
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A1911829-4DF0-4BDF-A02C-98630A6AAE7A}
// *********************************************************************//
  IrdtmDiploma = interface(IrdtmEduBase)
    ['{A1911829-4DF0-4BDF-A02C-98630A6AAE7A}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmDiplomaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A1911829-4DF0-4BDF-A02C-98630A6AAE7A}
// *********************************************************************//
  IrdtmDiplomaDisp = dispinterface
    ['{A1911829-4DF0-4BDF-A02C-98630A6AAE7A}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSessionGroup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A02044EB-E8C3-48B1-9423-60D0AFAB494C}
// *********************************************************************//
  IrdtmSessionGroup = interface(IrdtmEduBase)
    ['{A02044EB-E8C3-48B1-9423-60D0AFAB494C}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmSessionGroupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A02044EB-E8C3-48B1-9423-60D0AFAB494C}
// *********************************************************************//
  IrdtmSessionGroupDisp = dispinterface
    ['{A02044EB-E8C3-48B1-9423-60D0AFAB494C}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmRoleGroup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D2410469-5394-43FE-A04F-C79398EEF997}
// *********************************************************************//
  IrdtmRoleGroup = interface(IrdtmEduBase)
    ['{D2410469-5394-43FE-A04F-C79398EEF997}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmRoleGroupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D2410469-5394-43FE-A04F-C79398EEF997}
// *********************************************************************//
  IrdtmRoleGroupDisp = dispinterface
    ['{D2410469-5394-43FE-A04F-C79398EEF997}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSchoolYear
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9C2668EA-3829-4983-BA24-5B8D60CC5E2B}
// *********************************************************************//
  IrdtmSchoolYear = interface(IrdtmEduBase)
    ['{9C2668EA-3829-4983-BA24-5B8D60CC5E2B}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmSchoolYearDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9C2668EA-3829-4983-BA24-5B8D60CC5E2B}
// *********************************************************************//
  IrdtmSchoolYearDisp = dispinterface
    ['{9C2668EA-3829-4983-BA24-5B8D60CC5E2B}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmProgDiscipline
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E38C7789-3D39-4A41-AB15-38DE7DFFCECC}
// *********************************************************************//
  IrdtmProgDiscipline = interface(IrdtmEduBase)
    ['{E38C7789-3D39-4A41-AB15-38DE7DFFCECC}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmProgDisciplineDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E38C7789-3D39-4A41-AB15-38DE7DFFCECC}
// *********************************************************************//
  IrdtmProgDisciplineDisp = dispinterface
    ['{E38C7789-3D39-4A41-AB15-38DE7DFFCECC}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmUser
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E4CBE81D-5385-4A97-8EEE-221FA3D3A924}
// *********************************************************************//
  IrdtmUser = interface(IrdtmEduBase)
    ['{E4CBE81D-5385-4A97-8EEE-221FA3D3A924}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmUserDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E4CBE81D-5385-4A97-8EEE-221FA3D3A924}
// *********************************************************************//
  IrdtmUserDisp = dispinterface
    ['{E4CBE81D-5385-4A97-8EEE-221FA3D3A924}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmProfile
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8E7B52D1-39DE-4B01-9B69-77F84BCEA91E}
// *********************************************************************//
  IrdtmProfile = interface(IrdtmEduBase)
    ['{8E7B52D1-39DE-4B01-9B69-77F84BCEA91E}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmProfileDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8E7B52D1-39DE-4B01-9B69-77F84BCEA91E}
// *********************************************************************//
  IrdtmProfileDisp = dispinterface
    ['{8E7B52D1-39DE-4B01-9B69-77F84BCEA91E}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmUserProfile
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E98ED30C-04A2-4F2F-B85E-FCFDA89026EB}
// *********************************************************************//
  IrdtmUserProfile = interface(IrdtmEduBase)
    ['{E98ED30C-04A2-4F2F-B85E-FCFDA89026EB}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmUserProfileDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {E98ED30C-04A2-4F2F-B85E-FCFDA89026EB}
// *********************************************************************//
  IrdtmUserProfileDisp = dispinterface
    ['{E98ED30C-04A2-4F2F-B85E-FCFDA89026EB}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmGender
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {56F77B0E-7468-4A6E-96B5-C8F7C5E1BE8F}
// *********************************************************************//
  IrdtmGender = interface(IrdtmEduBase)
    ['{56F77B0E-7468-4A6E-96B5-C8F7C5E1BE8F}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmGenderDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {56F77B0E-7468-4A6E-96B5-C8F7C5E1BE8F}
// *********************************************************************//
  IrdtmGenderDisp = dispinterface
    ['{56F77B0E-7468-4A6E-96B5-C8F7C5E1BE8F}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmTitle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F9AB5FAC-2636-45F2-93EB-73ECADD1726A}
// *********************************************************************//
  IrdtmTitle = interface(IrdtmEduBase)
    ['{F9AB5FAC-2636-45F2-93EB-73ECADD1726A}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmTitleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F9AB5FAC-2636-45F2-93EB-73ECADD1726A}
// *********************************************************************//
  IrdtmTitleDisp = dispinterface
    ['{F9AB5FAC-2636-45F2-93EB-73ECADD1726A}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmMedium
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2D70E93C-5B46-4F97-B963-57DD411FA3C8}
// *********************************************************************//
  IrdtmMedium = interface(IrdtmEduBase)
    ['{2D70E93C-5B46-4F97-B963-57DD411FA3C8}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmMediumDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2D70E93C-5B46-4F97-B963-57DD411FA3C8}
// *********************************************************************//
  IrdtmMediumDisp = dispinterface
    ['{2D70E93C-5B46-4F97-B963-57DD411FA3C8}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmOrganisationType
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {27CCD308-5D71-4091-8364-F37186FB1AC1}
// *********************************************************************//
  IrdtmOrganisationType = interface(IrdtmEduBase)
    ['{27CCD308-5D71-4091-8364-F37186FB1AC1}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmOrganisationTypeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {27CCD308-5D71-4091-8364-F37186FB1AC1}
// *********************************************************************//
  IrdtmOrganisationTypeDisp = dispinterface
    ['{27CCD308-5D71-4091-8364-F37186FB1AC1}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmOrganisation
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F477BE2C-E999-441A-8D95-BE0140644F40}
// *********************************************************************//
  IrdtmOrganisation = interface(IrdtmEduBase)
    ['{F477BE2C-E999-441A-8D95-BE0140644F40}']
    function JoinOrganisations(var OrganisationList: WideString; CopyToOrganisationID: SYSINT): SYSINT; safecall;
    function JoinPersons(OrganisationID: SYSINT): SYSINT; safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmOrganisationDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F477BE2C-E999-441A-8D95-BE0140644F40}
// *********************************************************************//
  IrdtmOrganisationDisp = dispinterface
    ['{F477BE2C-E999-441A-8D95-BE0140644F40}']
    function JoinOrganisations(var OrganisationList: WideString; CopyToOrganisationID: SYSINT): SYSINT; dispid 401;
    function JoinPersons(OrganisationID: SYSINT): SYSINT; dispid 402;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmPerson
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D52313BD-66F4-4EC8-8C27-FE3D21741D7A}
// *********************************************************************//
  IrdtmPerson = interface(IrdtmEduBase)
    ['{D52313BD-66F4-4EC8-8C27-FE3D21741D7A}']
    procedure JoinPersons(const APersonList: WideString; ACopyToPersonID: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmPersonDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D52313BD-66F4-4EC8-8C27-FE3D21741D7A}
// *********************************************************************//
  IrdtmPersonDisp = dispinterface
    ['{D52313BD-66F4-4EC8-8C27-FE3D21741D7A}']
    procedure JoinPersons(const APersonList: WideString; ACopyToPersonID: Integer); dispid 401;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmInstructor
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BECD895C-63C5-4677-985C-480EB3FA4D48}
// *********************************************************************//
  IrdtmInstructor = interface(IrdtmEduBase)
    ['{BECD895C-63C5-4677-985C-480EB3FA4D48}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmInstructorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BECD895C-63C5-4677-985C-480EB3FA4D48}
// *********************************************************************//
  IrdtmInstructorDisp = dispinterface
    ['{BECD895C-63C5-4677-985C-480EB3FA4D48}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmProgInstructor
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7483D231-473D-4FB1-A1F8-58E8987C6F47}
// *********************************************************************//
  IrdtmProgInstructor = interface(IrdtmEduBase)
    ['{7483D231-473D-4FB1-A1F8-58E8987C6F47}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmProgInstructorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7483D231-473D-4FB1-A1F8-58E8987C6F47}
// *********************************************************************//
  IrdtmProgInstructorDisp = dispinterface
    ['{7483D231-473D-4FB1-A1F8-58E8987C6F47}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmRole
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D22636E8-29D5-4FCD-8968-95EE0A55C8BE}
// *********************************************************************//
  IrdtmRole = interface(IrdtmEduBase)
    ['{D22636E8-29D5-4FCD-8968-95EE0A55C8BE}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmRoleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D22636E8-29D5-4FCD-8968-95EE0A55C8BE}
// *********************************************************************//
  IrdtmRoleDisp = dispinterface
    ['{D22636E8-29D5-4FCD-8968-95EE0A55C8BE}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmProgProgram
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8E605826-4220-4CE7-972E-C37A283B6103}
// *********************************************************************//
  IrdtmProgProgram = interface(IrdtmEduBase)
    ['{8E605826-4220-4CE7-972E-C37A283B6103}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmProgProgramDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8E605826-4220-4CE7-972E-C37A283B6103}
// *********************************************************************//
  IrdtmProgProgramDisp = dispinterface
    ['{8E605826-4220-4CE7-972E-C37A283B6103}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSesSession
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {ED23B4A9-A052-4C83-90B2-287EB412C209}
// *********************************************************************//
  IrdtmSesSession = interface(IrdtmEduBase)
    ['{ED23B4A9-A052-4C83-90B2-287EB412C209}']
    procedure MarkAsAanwezigheidslijstPrinted(const aSessionList: WideString); safecall;
    procedure CreateOCR(ASessionId: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmSesSessionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {ED23B4A9-A052-4C83-90B2-287EB412C209}
// *********************************************************************//
  IrdtmSesSessionDisp = dispinterface
    ['{ED23B4A9-A052-4C83-90B2-287EB412C209}']
    procedure MarkAsAanwezigheidslijstPrinted(const aSessionList: WideString); dispid 401;
    procedure CreateOCR(ASessionId: Integer); dispid 402;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmCenInfrastructure
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D34D72B8-8BF9-4A42-830C-AA17B0AC73E3}
// *********************************************************************//
  IrdtmCenInfrastructure = interface(IrdtmEduBase)
    ['{D34D72B8-8BF9-4A42-830C-AA17B0AC73E3}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmCenInfrastructureDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D34D72B8-8BF9-4A42-830C-AA17B0AC73E3}
// *********************************************************************//
  IrdtmCenInfrastructureDisp = dispinterface
    ['{D34D72B8-8BF9-4A42-830C-AA17B0AC73E3}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSesExpenses
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {65329748-D2B2-4882-8338-1CC4F1DFD989}
// *********************************************************************//
  IrdtmSesExpenses = interface(IrdtmEduBase)
    ['{65329748-D2B2-4882-8338-1CC4F1DFD989}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmSesExpensesDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {65329748-D2B2-4882-8338-1CC4F1DFD989}
// *********************************************************************//
  IrdtmSesExpensesDisp = dispinterface
    ['{65329748-D2B2-4882-8338-1CC4F1DFD989}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmEnrolment
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A75B586D-57D9-4B22-B7DA-B12DA13AD333}
// *********************************************************************//
  IrdtmEnrolment = interface(IrdtmEduBase)
    ['{A75B586D-57D9-4B22-B7DA-B12DA13AD333}']
    procedure MarkAsCertificatePrinted(const EnrollmentList: WideString); safecall;
    function GetDefaultPrice(TussenkomstType: SYSINT; SessieID: SYSINT): Double; safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmEnrolmentDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A75B586D-57D9-4B22-B7DA-B12DA13AD333}
// *********************************************************************//
  IrdtmEnrolmentDisp = dispinterface
    ['{A75B586D-57D9-4B22-B7DA-B12DA13AD333}']
    procedure MarkAsCertificatePrinted(const EnrollmentList: WideString); dispid 401;
    function GetDefaultPrice(TussenkomstType: SYSINT; SessieID: SYSINT): Double; dispid 402;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSesWizard
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EE509BD9-A4B5-46AE-88D7-7C59F2B43A23}
// *********************************************************************//
  IrdtmSesWizard = interface(IrdtmEduBase)
    ['{EE509BD9-A4B5-46AE-88D7-7C59F2B43A23}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmSesWizardDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EE509BD9-A4B5-46AE-88D7-7C59F2B43A23}
// *********************************************************************//
  IrdtmSesWizardDisp = dispinterface
    ['{EE509BD9-A4B5-46AE-88D7-7C59F2B43A23}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSesInstructorRoom
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7838DFC4-1474-42AA-BCC7-D92AE2A5C748}
// *********************************************************************//
  IrdtmSesInstructorRoom = interface(IrdtmEduBase)
    ['{7838DFC4-1474-42AA-BCC7-D92AE2A5C748}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmSesInstructorRoomDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7838DFC4-1474-42AA-BCC7-D92AE2A5C748}
// *********************************************************************//
  IrdtmSesInstructorRoomDisp = dispinterface
    ['{7838DFC4-1474-42AA-BCC7-D92AE2A5C748}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmCenRoom
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FD7C6DD8-FDD4-42D3-85FB-78C27464AE72}
// *********************************************************************//
  IrdtmCenRoom = interface(IrdtmEduBase)
    ['{FD7C6DD8-FDD4-42D3-85FB-78C27464AE72}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmCenRoomDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FD7C6DD8-FDD4-42D3-85FB-78C27464AE72}
// *********************************************************************//
  IrdtmCenRoomDisp = dispinterface
    ['{FD7C6DD8-FDD4-42D3-85FB-78C27464AE72}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmQueryGroup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {45C3EF4C-805F-4418-AEBF-A4C5380F27A9}
// *********************************************************************//
  IrdtmQueryGroup = interface(IrdtmEduBase)
    ['{45C3EF4C-805F-4418-AEBF-A4C5380F27A9}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmQueryGroupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {45C3EF4C-805F-4418-AEBF-A4C5380F27A9}
// *********************************************************************//
  IrdtmQueryGroupDisp = dispinterface
    ['{45C3EF4C-805F-4418-AEBF-A4C5380F27A9}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmQuery
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A55D9241-94D2-4C48-ADBD-38227DB869DB}
// *********************************************************************//
  IrdtmQuery = interface(IrdtmEduBase)
    ['{A55D9241-94D2-4C48-ADBD-38227DB869DB}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmQueryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A55D9241-94D2-4C48-ADBD-38227DB869DB}
// *********************************************************************//
  IrdtmQueryDisp = dispinterface
    ['{A55D9241-94D2-4C48-ADBD-38227DB869DB}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmParameter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {89A7A2BA-F452-49FC-88F4-3C11182C4748}
// *********************************************************************//
  IrdtmParameter = interface(IrdtmEduBase)
    ['{89A7A2BA-F452-49FC-88F4-3C11182C4748}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmParameterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {89A7A2BA-F452-49FC-88F4-3C11182C4748}
// *********************************************************************//
  IrdtmParameterDisp = dispinterface
    ['{89A7A2BA-F452-49FC-88F4-3C11182C4748}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmParameterType
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {46EC91D6-52BB-4E1F-BBBB-EE6F6CB0AE93}
// *********************************************************************//
  IrdtmParameterType = interface(IrdtmEduBase)
    ['{46EC91D6-52BB-4E1F-BBBB-EE6F6CB0AE93}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmParameterTypeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {46EC91D6-52BB-4E1F-BBBB-EE6F6CB0AE93}
// *********************************************************************//
  IrdtmParameterTypeDisp = dispinterface
    ['{46EC91D6-52BB-4E1F-BBBB-EE6F6CB0AE93}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmLogHistory
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AF7AF5B9-C8AA-4198-AFE4-48B8EB22BA7B}
// *********************************************************************//
  IrdtmLogHistory = interface(IrdtmEduBase)
    ['{AF7AF5B9-C8AA-4198-AFE4-48B8EB22BA7B}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmLogHistoryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {AF7AF5B9-C8AA-4198-AFE4-48B8EB22BA7B}
// *********************************************************************//
  IrdtmLogHistoryDisp = dispinterface
    ['{AF7AF5B9-C8AA-4198-AFE4-48B8EB22BA7B}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmTableField
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF0E4931-FD09-425D-AFCF-AF1838E319A4}
// *********************************************************************//
  IrdtmTableField = interface(IrdtmEduBase)
    ['{EF0E4931-FD09-425D-AFCF-AF1838E319A4}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmTableFieldDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF0E4931-FD09-425D-AFCF-AF1838E319A4}
// *********************************************************************//
  IrdtmTableFieldDisp = dispinterface
    ['{EF0E4931-FD09-425D-AFCF-AF1838E319A4}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmQueryWizard
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {77D32143-D688-4C84-ABA5-39821213A754}
// *********************************************************************//
  IrdtmQueryWizard = interface(IrdtmEduBase)
    ['{77D32143-D688-4C84-ABA5-39821213A754}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmQueryWizardDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {77D32143-D688-4C84-ABA5-39821213A754}
// *********************************************************************//
  IrdtmQueryWizardDisp = dispinterface
    ['{77D32143-D688-4C84-ABA5-39821213A754}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmReport
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A3FC3779-A79E-4770-878C-88E02C0F51E1}
// *********************************************************************//
  IrdtmReport = interface(IrdtmEduBase)
    ['{A3FC3779-A79E-4770-878C-88E02C0F51E1}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmReportDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A3FC3779-A79E-4770-878C-88E02C0F51E1}
// *********************************************************************//
  IrdtmReportDisp = dispinterface
    ['{A3FC3779-A79E-4770-878C-88E02C0F51E1}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmImportOverview
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EAF69794-2A48-44C0-8DA1-81031B631509}
// *********************************************************************//
  IrdtmImportOverview = interface(IrdtmEduBase)
    ['{EAF69794-2A48-44C0-8DA1-81031B631509}']
    function ProcessSingleRecord(EducationIdentity: Integer): WideString; safecall;
    function GetDescriptionForStatus(const Status: WideString): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmImportOverviewDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EAF69794-2A48-44C0-8DA1-81031B631509}
// *********************************************************************//
  IrdtmImportOverviewDisp = dispinterface
    ['{EAF69794-2A48-44C0-8DA1-81031B631509}']
    function ProcessSingleRecord(EducationIdentity: Integer): WideString; dispid 401;
    function GetDescriptionForStatus(const Status: WideString): WideString; dispid 402;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmDocCenter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3B3D1409-7D05-4218-90CC-D9D91F7D30F1}
// *********************************************************************//
  IrdtmDocCenter = interface(IrdtmEduBase)
    ['{3B3D1409-7D05-4218-90CC-D9D91F7D30F1}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmDocCenterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3B3D1409-7D05-4218-90CC-D9D91F7D30F1}
// *********************************************************************//
  IrdtmDocCenterDisp = dispinterface
    ['{3B3D1409-7D05-4218-90CC-D9D91F7D30F1}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSesFact
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B1D13108-6DAB-430D-9E6D-CB8ABC4A6CDE}
// *********************************************************************//
  IrdtmSesFact = interface(IrdtmEduBase)
    ['{B1D13108-6DAB-430D-9E6D-CB8ABC4A6CDE}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmSesFactDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B1D13108-6DAB-430D-9E6D-CB8ABC4A6CDE}
// *********************************************************************//
  IrdtmSesFactDisp = dispinterface
    ['{B1D13108-6DAB-430D-9E6D-CB8ABC4A6CDE}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmInvoicing
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {361C66D9-B14D-4955-9D41-F0275F60B6BB}
// *********************************************************************//
  IrdtmInvoicing = interface(IrdtmEduBase)
    ['{361C66D9-B14D-4955-9D41-F0275F60B6BB}']
    function GetInvoicePathCurUser: WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmInvoicingDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {361C66D9-B14D-4955-9D41-F0275F60B6BB}
// *********************************************************************//
  IrdtmInvoicingDisp = dispinterface
    ['{361C66D9-B14D-4955-9D41-F0275F60B6BB}']
    function GetInvoicePathCurUser: WideString; dispid 401;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmImport
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7E2601A1-E669-4204-B22D-E2CD9C938445}
// *********************************************************************//
  IrdtmImport = interface(IrdtmEduBase)
    ['{7E2601A1-E669-4204-B22D-E2CD9C938445}']
    function ProcessSingleRecord(EducationIdentity: Integer; SessionIdentity: Integer): WideString; safecall;
    procedure UpdatePersonConstructId(APersonId: Integer; AConstructId: Integer;
                                      AImportId: Integer; const AMsgType: WideString); safecall;
    procedure XmlEducationSetProcessed(AImportId: Integer; const AMsgType: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmImportDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {7E2601A1-E669-4204-B22D-E2CD9C938445}
// *********************************************************************//
  IrdtmImportDisp = dispinterface
    ['{7E2601A1-E669-4204-B22D-E2CD9C938445}']
    function ProcessSingleRecord(EducationIdentity: Integer; SessionIdentity: Integer): WideString; dispid 401;
    procedure UpdatePersonConstructId(APersonId: Integer; AConstructId: Integer;
                                      AImportId: Integer; const AMsgType: WideString); dispid 402;
    procedure XmlEducationSetProcessed(AImportId: Integer; const AMsgType: WideString); dispid 403;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmConfirmationReports
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F910CAAF-AED4-4F01-BEAF-291B251B2BBD}
// *********************************************************************//
  IrdtmConfirmationReports = interface(IrdtmEduBase)
    ['{F910CAAF-AED4-4F01-BEAF-291B251B2BBD}']
    function GetProgramNameFromSessionId(SessionId: Integer): WideString; safecall;
    procedure MarkConfirmationRptAsPrinted(conf_id: Integer; conf_sub_id: Integer); safecall;
    procedure RecalcRolegroups(SessionId: Integer; TotalParticipants: Integer;
                               OrganisationID: Integer; PartnerWinterID: Integer); safecall;
    function GetCodeFromSessionId(SessionId: Integer): WideString; safecall;
    procedure SetToBePrinted(ConfId: Integer); safecall;
    function GetSessionGroupIdFromSessionId(SessionId: Integer): SYSINT; safecall;
    procedure UpdateContactCompany(AOrganisationId: Integer; ANoContact: Integer;
                                   const AContactName: WideString; const AContactEmail: WideString;
                                   AConfSubId: Integer; AIsCCEnabled: Integer); safecall;
    procedure UpdateContactInfrastruct(AInfrastructure_Id: Integer; ANoContact: Integer;
                                       const AContactName: WideString;
                                       const AContactEmail: WideString); safecall;
    procedure UpdateContactInstructorOrg(AInstructorId: Integer; ANoContact: Integer;
                                         const AContactName: WideString;
                                         const AContactEmail: WideString); safecall;
    function GetStartdateFromSessionId(SessionId: Integer): WideString; safecall;
    function GetMailingSignature(Language: SYSINT): WideString; safecall;
    procedure MarkConfrepSent(AConfId: Integer); safecall;
    function GetProgramTypeFromSessionId(SessionId: Integer): SYSINT; safecall;
    function GetMailingVCASignature(Language: SYSINT): WideString; safecall;
    function GetMailingBedrijfVCASignature(Language: SYSINT): WideString; safecall;
    function GetTextFromSessionId(SessionId: Integer): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmConfirmationReportsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F910CAAF-AED4-4F01-BEAF-291B251B2BBD}
// *********************************************************************//
  IrdtmConfirmationReportsDisp = dispinterface
    ['{F910CAAF-AED4-4F01-BEAF-291B251B2BBD}']
    function GetProgramNameFromSessionId(SessionId: Integer): WideString; dispid 362;
    procedure MarkConfirmationRptAsPrinted(conf_id: Integer; conf_sub_id: Integer); dispid 363;
    procedure RecalcRolegroups(SessionId: Integer; TotalParticipants: Integer;
                               OrganisationID: Integer; PartnerWinterID: Integer); dispid 364;
    function GetCodeFromSessionId(SessionId: Integer): WideString; dispid 401;
    procedure SetToBePrinted(ConfId: Integer); dispid 402;
    function GetSessionGroupIdFromSessionId(SessionId: Integer): SYSINT; dispid 403;
    procedure UpdateContactCompany(AOrganisationId: Integer; ANoContact: Integer;
                                   const AContactName: WideString; const AContactEmail: WideString;
                                   AConfSubId: Integer; AIsCCEnabled: Integer); dispid 404;
    procedure UpdateContactInfrastruct(AInfrastructure_Id: Integer; ANoContact: Integer;
                                       const AContactName: WideString;
                                       const AContactEmail: WideString); dispid 405;
    procedure UpdateContactInstructorOrg(AInstructorId: Integer; ANoContact: Integer;
                                         const AContactName: WideString;
                                         const AContactEmail: WideString); dispid 406;
    function GetStartdateFromSessionId(SessionId: Integer): WideString; dispid 407;
    function GetMailingSignature(Language: SYSINT): WideString; dispid 408;
    procedure MarkConfrepSent(AConfId: Integer); dispid 409;
    function GetProgramTypeFromSessionId(SessionId: Integer): SYSINT; dispid 410;
    function GetMailingVCASignature(Language: SYSINT): WideString; dispid 411;
    function GetMailingBedrijfVCASignature(Language: SYSINT): WideString; dispid 412;
    function GetTextFromSessionId(SessionId: Integer): WideString; dispid 413;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmSessionWizard
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5675D53A-E56A-4B81-B6DA-68F8134AEBFB}
// *********************************************************************//
  IrdtmSessionWizard = interface(IrdtmEduBase)
    ['{5675D53A-E56A-4B81-B6DA-68F8134AEBFB}']
    function GetNewOrganisationID: SYSINT; safecall;
    function GetNewRoleID: SYSINT; safecall;
    function EnrolPerson(ASessionId: Integer; ARoleID: Integer; ARoleGroupID: Integer): SYSINT; safecall;
    procedure RemoveEnrolment(AEnrolID: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmSessionWizardDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5675D53A-E56A-4B81-B6DA-68F8134AEBFB}
// *********************************************************************//
  IrdtmSessionWizardDisp = dispinterface
    ['{5675D53A-E56A-4B81-B6DA-68F8134AEBFB}']
    function GetNewOrganisationID: SYSINT; dispid 401;
    function GetNewRoleID: SYSINT; dispid 402;
    function EnrolPerson(ASessionId: Integer; ARoleID: Integer; ARoleGroupID: Integer): SYSINT; dispid 403;
    procedure RemoveEnrolment(AEnrolID: Integer); dispid 404;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmCooperation
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4028964F-DF94-488E-A807-588A1CD04BC3}
// *********************************************************************//
  IrdtmCooperation = interface(IrdtmEduBase)
    ['{4028964F-DF94-488E-A807-588A1CD04BC3}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmCooperationDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4028964F-DF94-488E-A807-588A1CD04BC3}
// *********************************************************************//
  IrdtmCooperationDisp = dispinterface
    ['{4028964F-DF94-488E-A807-588A1CD04BC3}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmKMO_PORT_Status
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {71B729E2-6E86-4BEF-9764-5F1DA030814A}
// *********************************************************************//
  IrdtmKMO_PORT_Status = interface(IrdtmEduBase)
    ['{71B729E2-6E86-4BEF-9764-5F1DA030814A}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmKMO_PORT_StatusDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {71B729E2-6E86-4BEF-9764-5F1DA030814A}
// *********************************************************************//
  IrdtmKMO_PORT_StatusDisp = dispinterface
    ['{71B729E2-6E86-4BEF-9764-5F1DA030814A}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmKMO_Portefeuille
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {275703D8-CD09-40A4-AAE8-EFB480F06FAC}
// *********************************************************************//
  IrdtmKMO_Portefeuille = interface(IrdtmEduBase)
    ['{275703D8-CD09-40A4-AAE8-EFB480F06FAC}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmKMO_PortefeuilleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {275703D8-CD09-40A4-AAE8-EFB480F06FAC}
// *********************************************************************//
  IrdtmKMO_PortefeuilleDisp = dispinterface
    ['{275703D8-CD09-40A4-AAE8-EFB480F06FAC}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmKMO_PORT_Payment
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {06A23DAB-0461-4994-A8EE-EC9055C459F3}
// *********************************************************************//
  IrdtmKMO_PORT_Payment = interface(IrdtmEduBase)
    ['{06A23DAB-0461-4994-A8EE-EC9055C459F3}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmKMO_PORT_PaymentDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {06A23DAB-0461-4994-A8EE-EC9055C459F3}
// *********************************************************************//
  IrdtmKMO_PORT_PaymentDisp = dispinterface
    ['{06A23DAB-0461-4994-A8EE-EC9055C459F3}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmKMO_PORT_Link_Port_Payment
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF936A41-A1BE-46DC-8846-A6E3150EAAB7}
// *********************************************************************//
  IrdtmKMO_PORT_Link_Port_Payment = interface(IrdtmEduBase)
    ['{EF936A41-A1BE-46DC-8846-A6E3150EAAB7}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmKMO_PORT_Link_Port_PaymentDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {EF936A41-A1BE-46DC-8846-A6E3150EAAB7}
// *********************************************************************//
  IrdtmKMO_PORT_Link_Port_PaymentDisp = dispinterface
    ['{EF936A41-A1BE-46DC-8846-A6E3150EAAB7}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmKMO_PORT_Link_Port_Session
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1FFF2FB5-15DA-477A-A40B-62CBC628EA22}
// *********************************************************************//
  IrdtmKMO_PORT_Link_Port_Session = interface(IrdtmEduBase)
    ['{1FFF2FB5-15DA-477A-A40B-62CBC628EA22}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmKMO_PORT_Link_Port_SessionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {1FFF2FB5-15DA-477A-A40B-62CBC628EA22}
// *********************************************************************//
  IrdtmKMO_PORT_Link_Port_SessionDisp = dispinterface
    ['{1FFF2FB5-15DA-477A-A40B-62CBC628EA22}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmImport_Excel
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {29633E00-91AE-4C7A-A90A-228A32D728C7}
// *********************************************************************//
  IrdtmImport_Excel = interface(IrdtmEduBase)
    ['{29633E00-91AE-4C7A-A90A-228A32D728C7}']
    procedure ProcessValidateKeys; safecall;
    procedure XLSSetProcessed(AImportId: Integer); safecall;
    function ProcessSingleRecord(EducationIdentity: Integer; SessionIdentity: Integer): WideString; safecall;
    procedure XLSAutCreate(AImportId: Integer); safecall;
    procedure XLSSetSession(AImportId: Integer; ASession: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IrdtmImport_ExcelDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {29633E00-91AE-4C7A-A90A-228A32D728C7}
// *********************************************************************//
  IrdtmImport_ExcelDisp = dispinterface
    ['{29633E00-91AE-4C7A-A90A-228A32D728C7}']
    procedure ProcessValidateKeys; dispid 401;
    procedure XLSSetProcessed(AImportId: Integer); dispid 402;
    function ProcessSingleRecord(EducationIdentity: Integer; SessionIdentity: Integer): WideString; dispid 403;
    procedure XLSAutCreate(AImportId: Integer); dispid 404;
    procedure XLSSetSession(AImportId: Integer; ASession: Integer); dispid 405;
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IrdtmProgramType
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A741EC78-9BA5-4196-9DE1-C7D7CBFF9FD5}
// *********************************************************************//
  IrdtmProgramType = interface(IrdtmEduBase)
    ['{A741EC78-9BA5-4196-9DE1-C7D7CBFF9FD5}']
  end;

// *********************************************************************//
// DispIntf:  IrdtmProgramTypeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {A741EC78-9BA5-4196-9DE1-C7D7CBFF9FD5}
// *********************************************************************//
  IrdtmProgramTypeDisp = dispinterface
    ['{A741EC78-9BA5-4196-9DE1-C7D7CBFF9FD5}']
    function ApplyFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 303;
    function ApplyDetailFilter(const aProvider: WideString; const aFilter: WideString): OleVariant; dispid 304;
    function ApplyOrderBy(const aProvider: WideString; const aOrderBy: WideString): OleVariant; dispid 305;
    function GetNewRecordID: SYSINT; dispid 306;
    property ADOConnection: Integer writeonly dispid 301;
    property rdtmEDUMain: IrdtmEDUMain writeonly dispid 302;
    function SetSQLStatement(const aProvider: WideString; const aSQL: WideString): OleVariant; dispid 307;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IData_Country
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4C96DE8E-3A5E-4313-9EDD-EDC923266678}
// *********************************************************************//
  IData_Country = interface(IAppServer)
    ['{4C96DE8E-3A5E-4313-9EDD-EDC923266678}']
  end;

// *********************************************************************//
// DispIntf:  IData_CountryDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4C96DE8E-3A5E-4313-9EDD-EDC923266678}
// *********************************************************************//
  IData_CountryDisp = dispinterface
    ['{4C96DE8E-3A5E-4313-9EDD-EDC923266678}']
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// Interface: IRemote_Country_Test
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B529E1CF-C623-40C6-8167-BCF87770AC8E}
// *********************************************************************//
  IRemote_Country_Test = interface(IAppServer)
    ['{B529E1CF-C623-40C6-8167-BCF87770AC8E}']
  end;

// *********************************************************************//
// DispIntf:  IRemote_Country_TestDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B529E1CF-C623-40C6-8167-BCF87770AC8E}
// *********************************************************************//
  IRemote_Country_TestDisp = dispinterface
    ['{B529E1CF-C623-40C6-8167-BCF87770AC8E}']
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: SYSINT;
                             out ErrorCount: SYSINT; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: SYSINT; out RecsOut: SYSINT;
                           Options: SYSINT; const CommandText: WideString; var Params: OleVariant;
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: SYSINT;
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString;
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CordtmEDUMain provides a Create and CreateRemote method to
// create instances of the default interface IrdtmEDUMain exposed by
// the CoClass rdtmEDUMain. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmEDUMain = class
    class function Create: IrdtmEDUMain;
    class function CreateRemote(const MachineName: string): IrdtmEDUMain;
  end;

// *********************************************************************//
// The Class CordtmEduBase provides a Create and CreateRemote method to
// create instances of the default interface IrdtmEduBase exposed by
// the CoClass rdtmEduBase. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmEduBase = class
    class function Create: IrdtmEduBase;
    class function CreateRemote(const MachineName: string): IrdtmEduBase;
  end;

// *********************************************************************//
// The Class CordtmCountry provides a Create and CreateRemote method to
// create instances of the default interface IrdtmCountry exposed by
// the CoClass rdtmCountry. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmCountry = class
    class function Create: IrdtmCountry;
    class function CreateRemote(const MachineName: string): IrdtmCountry;
  end;

// *********************************************************************//
// The Class CordtmCountryPart provides a Create and CreateRemote method to
// create instances of the default interface IrdtmCountryPart exposed by
// the CoClass rdtmCountryPart. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmCountryPart = class
    class function Create: IrdtmCountryPart;
    class function CreateRemote(const MachineName: string): IrdtmCountryPart;
  end;

// *********************************************************************//
// The Class CordtmProvince provides a Create and CreateRemote method to
// create instances of the default interface IrdtmProvince exposed by
// the CoClass rdtmProvince. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmProvince = class
    class function Create: IrdtmProvince;
    class function CreateRemote(const MachineName: string): IrdtmProvince;
  end;

// *********************************************************************//
// The Class CordtmLanguage provides a Create and CreateRemote method to
// create instances of the default interface IrdtmLanguage exposed by
// the CoClass rdtmLanguage. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmLanguage = class
    class function Create: IrdtmLanguage;
    class function CreateRemote(const MachineName: string): IrdtmLanguage;
  end;

// *********************************************************************//
// The Class CordtmPostalCode provides a Create and CreateRemote method to
// create instances of the default interface IrdtmPostalCode exposed by
// the CoClass rdtmPostalCode. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmPostalCode = class
    class function Create: IrdtmPostalCode;
    class function CreateRemote(const MachineName: string): IrdtmPostalCode;
  end;

// *********************************************************************//
// The Class CordtmStatus provides a Create and CreateRemote method to
// create instances of the default interface IrdtmStatus exposed by
// the CoClass rdtmStatus. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmStatus = class
    class function Create: IrdtmStatus;
    class function CreateRemote(const MachineName: string): IrdtmStatus;
  end;

// *********************************************************************//
// The Class CordtmDiploma provides a Create and CreateRemote method to
// create instances of the default interface IrdtmDiploma exposed by
// the CoClass rdtmDiploma. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmDiploma = class
    class function Create: IrdtmDiploma;
    class function CreateRemote(const MachineName: string): IrdtmDiploma;
  end;

// *********************************************************************//
// The Class CordtmSessionGroup provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSessionGroup exposed by
// the CoClass rdtmSessionGroup. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSessionGroup = class
    class function Create: IrdtmSessionGroup;
    class function CreateRemote(const MachineName: string): IrdtmSessionGroup;
  end;

// *********************************************************************//
// The Class CordtmRoleGroup provides a Create and CreateRemote method to
// create instances of the default interface IrdtmRoleGroup exposed by
// the CoClass rdtmRoleGroup. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmRoleGroup = class
    class function Create: IrdtmRoleGroup;
    class function CreateRemote(const MachineName: string): IrdtmRoleGroup;
  end;

// *********************************************************************//
// The Class CordtmSchoolYear provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSchoolYear exposed by
// the CoClass rdtmSchoolYear. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSchoolYear = class
    class function Create: IrdtmSchoolYear;
    class function CreateRemote(const MachineName: string): IrdtmSchoolYear;
  end;

// *********************************************************************//
// The Class CordtmProgDiscipline provides a Create and CreateRemote method to
// create instances of the default interface IrdtmProgDiscipline exposed by
// the CoClass rdtmProgDiscipline. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmProgDiscipline = class
    class function Create: IrdtmProgDiscipline;
    class function CreateRemote(const MachineName: string): IrdtmProgDiscipline;
  end;

// *********************************************************************//
// The Class CordtmUser provides a Create and CreateRemote method to
// create instances of the default interface IrdtmUser exposed by
// the CoClass rdtmUser. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmUser = class
    class function Create: IrdtmUser;
    class function CreateRemote(const MachineName: string): IrdtmUser;
  end;

// *********************************************************************//
// The Class CordtmProfile provides a Create and CreateRemote method to
// create instances of the default interface IrdtmProfile exposed by
// the CoClass rdtmProfile. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmProfile = class
    class function Create: IrdtmProfile;
    class function CreateRemote(const MachineName: string): IrdtmProfile;
  end;

// *********************************************************************//
// The Class CordtmUserProfile provides a Create and CreateRemote method to
// create instances of the default interface IrdtmUserProfile exposed by
// the CoClass rdtmUserProfile. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmUserProfile = class
    class function Create: IrdtmUserProfile;
    class function CreateRemote(const MachineName: string): IrdtmUserProfile;
  end;

// *********************************************************************//
// The Class CordtmGender provides a Create and CreateRemote method to
// create instances of the default interface IrdtmGender exposed by
// the CoClass rdtmGender. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmGender = class
    class function Create: IrdtmGender;
    class function CreateRemote(const MachineName: string): IrdtmGender;
  end;

// *********************************************************************//
// The Class CordtmTitle provides a Create and CreateRemote method to
// create instances of the default interface IrdtmTitle exposed by
// the CoClass rdtmTitle. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmTitle = class
    class function Create: IrdtmTitle;
    class function CreateRemote(const MachineName: string): IrdtmTitle;
  end;

// *********************************************************************//
// The Class CordtmMedium provides a Create and CreateRemote method to
// create instances of the default interface IrdtmMedium exposed by
// the CoClass rdtmMedium. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmMedium = class
    class function Create: IrdtmMedium;
    class function CreateRemote(const MachineName: string): IrdtmMedium;
  end;

// *********************************************************************//
// The Class CordtmOrganisationType provides a Create and CreateRemote method to
// create instances of the default interface IrdtmOrganisationType exposed by
// the CoClass rdtmOrganisationType. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmOrganisationType = class
    class function Create: IrdtmOrganisationType;
    class function CreateRemote(const MachineName: string): IrdtmOrganisationType;
  end;

// *********************************************************************//
// The Class CordtmOrganisation provides a Create and CreateRemote method to
// create instances of the default interface IrdtmOrganisation exposed by
// the CoClass rdtmOrganisation. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmOrganisation = class
    class function Create: IrdtmOrganisation;
    class function CreateRemote(const MachineName: string): IrdtmOrganisation;
  end;

// *********************************************************************//
// The Class CordtmPerson provides a Create and CreateRemote method to
// create instances of the default interface IrdtmPerson exposed by
// the CoClass rdtmPerson. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmPerson = class
    class function Create: IrdtmPerson;
    class function CreateRemote(const MachineName: string): IrdtmPerson;
  end;

// *********************************************************************//
// The Class CordtmInstructor provides a Create and CreateRemote method to
// create instances of the default interface IrdtmInstructor exposed by
// the CoClass rdtmInstructor. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmInstructor = class
    class function Create: IrdtmInstructor;
    class function CreateRemote(const MachineName: string): IrdtmInstructor;
  end;

// *********************************************************************//
// The Class CordtmProgInstructor provides a Create and CreateRemote method to
// create instances of the default interface IrdtmProgInstructor exposed by
// the CoClass rdtmProgInstructor. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmProgInstructor = class
    class function Create: IrdtmProgInstructor;
    class function CreateRemote(const MachineName: string): IrdtmProgInstructor;
  end;

// *********************************************************************//
// The Class CordtmRole provides a Create and CreateRemote method to
// create instances of the default interface IrdtmRole exposed by
// the CoClass rdtmRole. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmRole = class
    class function Create: IrdtmRole;
    class function CreateRemote(const MachineName: string): IrdtmRole;
  end;

// *********************************************************************//
// The Class CordtmProgProgram provides a Create and CreateRemote method to
// create instances of the default interface IrdtmProgProgram exposed by
// the CoClass rdtmProgProgram. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmProgProgram = class
    class function Create: IrdtmProgProgram;
    class function CreateRemote(const MachineName: string): IrdtmProgProgram;
  end;

// *********************************************************************//
// The Class CordtmSesSession provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSesSession exposed by
// the CoClass rdtmSesSession. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSesSession = class
    class function Create: IrdtmSesSession;
    class function CreateRemote(const MachineName: string): IrdtmSesSession;
  end;

// *********************************************************************//
// The Class CordtmCenInfrastructure provides a Create and CreateRemote method to
// create instances of the default interface IrdtmCenInfrastructure exposed by
// the CoClass rdtmCenInfrastructure. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmCenInfrastructure = class
    class function Create: IrdtmCenInfrastructure;
    class function CreateRemote(const MachineName: string): IrdtmCenInfrastructure;
  end;

// *********************************************************************//
// The Class CordtmSesExpenses provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSesExpenses exposed by
// the CoClass rdtmSesExpenses. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSesExpenses = class
    class function Create: IrdtmSesExpenses;
    class function CreateRemote(const MachineName: string): IrdtmSesExpenses;
  end;

// *********************************************************************//
// The Class CordtmEnrolment provides a Create and CreateRemote method to
// create instances of the default interface IrdtmEnrolment exposed by
// the CoClass rdtmEnrolment. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmEnrolment = class
    class function Create: IrdtmEnrolment;
    class function CreateRemote(const MachineName: string): IrdtmEnrolment;
  end;

// *********************************************************************//
// The Class CordtmSesWizard provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSesWizard exposed by
// the CoClass rdtmSesWizard. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSesWizard = class
    class function Create: IrdtmSesWizard;
    class function CreateRemote(const MachineName: string): IrdtmSesWizard;
  end;

// *********************************************************************//
// The Class CordtmSesInstructorRoom provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSesInstructorRoom exposed by
// the CoClass rdtmSesInstructorRoom. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSesInstructorRoom = class
    class function Create: IrdtmSesInstructorRoom;
    class function CreateRemote(const MachineName: string): IrdtmSesInstructorRoom;
  end;

// *********************************************************************//
// The Class CordtmCenRoom provides a Create and CreateRemote method to
// create instances of the default interface IrdtmCenRoom exposed by
// the CoClass rdtmCenRoom. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmCenRoom = class
    class function Create: IrdtmCenRoom;
    class function CreateRemote(const MachineName: string): IrdtmCenRoom;
  end;

// *********************************************************************//
// The Class CordtmQueryGroup provides a Create and CreateRemote method to
// create instances of the default interface IrdtmQueryGroup exposed by
// the CoClass rdtmQueryGroup. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmQueryGroup = class
    class function Create: IrdtmQueryGroup;
    class function CreateRemote(const MachineName: string): IrdtmQueryGroup;
  end;

// *********************************************************************//
// The Class CordtmQuery provides a Create and CreateRemote method to
// create instances of the default interface IrdtmQuery exposed by
// the CoClass rdtmQuery. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmQuery = class
    class function Create: IrdtmQuery;
    class function CreateRemote(const MachineName: string): IrdtmQuery;
  end;

// *********************************************************************//
// The Class CordtmParameter provides a Create and CreateRemote method to
// create instances of the default interface IrdtmParameter exposed by
// the CoClass rdtmParameter. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmParameter = class
    class function Create: IrdtmParameter;
    class function CreateRemote(const MachineName: string): IrdtmParameter;
  end;

// *********************************************************************//
// The Class CordtmParameterType provides a Create and CreateRemote method to
// create instances of the default interface IrdtmParameterType exposed by
// the CoClass rdtmParameterType. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmParameterType = class
    class function Create: IrdtmParameterType;
    class function CreateRemote(const MachineName: string): IrdtmParameterType;
  end;

// *********************************************************************//
// The Class CordtmLogHistory provides a Create and CreateRemote method to
// create instances of the default interface IrdtmLogHistory exposed by
// the CoClass rdtmLogHistory. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmLogHistory = class
    class function Create: IrdtmLogHistory;
    class function CreateRemote(const MachineName: string): IrdtmLogHistory;
  end;

// *********************************************************************//
// The Class CordtmTableField provides a Create and CreateRemote method to
// create instances of the default interface IrdtmTableField exposed by
// the CoClass rdtmTableField. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmTableField = class
    class function Create: IrdtmTableField;
    class function CreateRemote(const MachineName: string): IrdtmTableField;
  end;

// *********************************************************************//
// The Class CordtmQueryWizard provides a Create and CreateRemote method to
// create instances of the default interface IrdtmQueryWizard exposed by
// the CoClass rdtmQueryWizard. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmQueryWizard = class
    class function Create: IrdtmQueryWizard;
    class function CreateRemote(const MachineName: string): IrdtmQueryWizard;
  end;

// *********************************************************************//
// The Class CordtmReport provides a Create and CreateRemote method to
// create instances of the default interface IrdtmReport exposed by
// the CoClass rdtmReport. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmReport = class
    class function Create: IrdtmReport;
    class function CreateRemote(const MachineName: string): IrdtmReport;
  end;

// *********************************************************************//
// The Class CordtmImportOverview provides a Create and CreateRemote method to
// create instances of the default interface IrdtmImportOverview exposed by
// the CoClass rdtmImportOverview. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmImportOverview = class
    class function Create: IrdtmImportOverview;
    class function CreateRemote(const MachineName: string): IrdtmImportOverview;
  end;

// *********************************************************************//
// The Class CordtmDocCenter provides a Create and CreateRemote method to
// create instances of the default interface IrdtmDocCenter exposed by
// the CoClass rdtmDocCenter. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmDocCenter = class
    class function Create: IrdtmDocCenter;
    class function CreateRemote(const MachineName: string): IrdtmDocCenter;
  end;

// *********************************************************************//
// The Class CordtmSesFact provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSesFact exposed by
// the CoClass rdtmSesFact. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSesFact = class
    class function Create: IrdtmSesFact;
    class function CreateRemote(const MachineName: string): IrdtmSesFact;
  end;

// *********************************************************************//
// The Class CordtmInvoicing provides a Create and CreateRemote method to
// create instances of the default interface IrdtmInvoicing exposed by
// the CoClass rdtmInvoicing. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmInvoicing = class
    class function Create: IrdtmInvoicing;
    class function CreateRemote(const MachineName: string): IrdtmInvoicing;
  end;

// *********************************************************************//
// The Class CordtmImport provides a Create and CreateRemote method to
// create instances of the default interface IrdtmImport exposed by
// the CoClass rdtmImport. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmImport = class
    class function Create: IrdtmImport;
    class function CreateRemote(const MachineName: string): IrdtmImport;
  end;

// *********************************************************************//
// The Class CordtmConfirmationReports provides a Create and CreateRemote method to
// create instances of the default interface IrdtmConfirmationReports exposed by
// the CoClass rdtmConfirmationReports. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmConfirmationReports = class
    class function Create: IrdtmConfirmationReports;
    class function CreateRemote(const MachineName: string): IrdtmConfirmationReports;
  end;

// *********************************************************************//
// The Class CordtmSessionWizard provides a Create and CreateRemote method to
// create instances of the default interface IrdtmSessionWizard exposed by
// the CoClass rdtmSessionWizard. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmSessionWizard = class
    class function Create: IrdtmSessionWizard;
    class function CreateRemote(const MachineName: string): IrdtmSessionWizard;
  end;

// *********************************************************************//
// The Class CordtmCooperation provides a Create and CreateRemote method to
// create instances of the default interface IrdtmCooperation exposed by
// the CoClass rdtmCooperation. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmCooperation = class
    class function Create: IrdtmCooperation;
    class function CreateRemote(const MachineName: string): IrdtmCooperation;
  end;

// *********************************************************************//
// The Class CordtmKMO_PORT_Status provides a Create and CreateRemote method to
// create instances of the default interface IrdtmKMO_PORT_Status exposed by
// the CoClass rdtmKMO_PORT_Status. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmKMO_PORT_Status = class
    class function Create: IrdtmKMO_PORT_Status;
    class function CreateRemote(const MachineName: string): IrdtmKMO_PORT_Status;
  end;

// *********************************************************************//
// The Class CordtmKMO_Portefeuille provides a Create and CreateRemote method to
// create instances of the default interface IrdtmKMO_Portefeuille exposed by
// the CoClass rdtmKMO_Portefeuille. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmKMO_Portefeuille = class
    class function Create: IrdtmKMO_Portefeuille;
    class function CreateRemote(const MachineName: string): IrdtmKMO_Portefeuille;
  end;

// *********************************************************************//
// The Class CordtmKMO_PORT_Payment provides a Create and CreateRemote method to
// create instances of the default interface IrdtmKMO_PORT_Payment exposed by
// the CoClass rdtmKMO_PORT_Payment. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmKMO_PORT_Payment = class
    class function Create: IrdtmKMO_PORT_Payment;
    class function CreateRemote(const MachineName: string): IrdtmKMO_PORT_Payment;
  end;

// *********************************************************************//
// The Class CordtmKMO_PORT_Link_Port_Payment provides a Create and CreateRemote method to
// create instances of the default interface IrdtmKMO_PORT_Link_Port_Payment exposed by
// the CoClass rdtmKMO_PORT_Link_Port_Payment. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmKMO_PORT_Link_Port_Payment = class
    class function Create: IrdtmKMO_PORT_Link_Port_Payment;
    class function CreateRemote(const MachineName: string): IrdtmKMO_PORT_Link_Port_Payment;
  end;

// *********************************************************************//
// The Class CordtmKMO_PORT_Link_Port_Session provides a Create and CreateRemote method to
// create instances of the default interface IrdtmKMO_PORT_Link_Port_Session exposed by
// the CoClass rdtmKMO_PORT_Link_Port_Session. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmKMO_PORT_Link_Port_Session = class
    class function Create: IrdtmKMO_PORT_Link_Port_Session;
    class function CreateRemote(const MachineName: string): IrdtmKMO_PORT_Link_Port_Session;
  end;

// *********************************************************************//
// The Class CordtmImport_Excel provides a Create and CreateRemote method to
// create instances of the default interface IrdtmImport_Excel exposed by
// the CoClass rdtmImport_Excel. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmImport_Excel = class
    class function Create: IrdtmImport_Excel;
    class function CreateRemote(const MachineName: string): IrdtmImport_Excel;
  end;

// *********************************************************************//
// The Class CordtmProgramType provides a Create and CreateRemote method to
// create instances of the default interface IrdtmProgramType exposed by
// the CoClass rdtmProgramType. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CordtmProgramType = class
    class function Create: IrdtmProgramType;
    class function CreateRemote(const MachineName: string): IrdtmProgramType;
  end;

// *********************************************************************//
// The Class CoData_Country provides a Create and CreateRemote method to
// create instances of the default interface IData_Country exposed by
// the CoClass Data_Country. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoData_Country = class
    class function Create: IData_Country;
    class function CreateRemote(const MachineName: string): IData_Country;
  end;

// *********************************************************************//
// The Class CoRemote_Country_Test provides a Create and CreateRemote method to
// create instances of the default interface IRemote_Country_Test exposed by
// the CoClass Remote_Country_Test. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoRemote_Country_Test = class
    class function Create: IRemote_Country_Test;
    class function CreateRemote(const MachineName: string): IRemote_Country_Test;
  end;

implementation

uses System.Win.ComObj;

class function CordtmEDUMain.Create: IrdtmEDUMain;
begin
  Result := CreateComObject(CLASS_rdtmEDUMain) as IrdtmEDUMain;
end;

class function CordtmEDUMain.CreateRemote(const MachineName: string): IrdtmEDUMain;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmEDUMain) as IrdtmEDUMain;
end;

class function CordtmEduBase.Create: IrdtmEduBase;
begin
  Result := CreateComObject(CLASS_rdtmEduBase) as IrdtmEduBase;
end;

class function CordtmEduBase.CreateRemote(const MachineName: string): IrdtmEduBase;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmEduBase) as IrdtmEduBase;
end;

class function CordtmCountry.Create: IrdtmCountry;
begin
  Result := CreateComObject(CLASS_rdtmCountry) as IrdtmCountry;
end;

class function CordtmCountry.CreateRemote(const MachineName: string): IrdtmCountry;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmCountry) as IrdtmCountry;
end;

class function CordtmCountryPart.Create: IrdtmCountryPart;
begin
  Result := CreateComObject(CLASS_rdtmCountryPart) as IrdtmCountryPart;
end;

class function CordtmCountryPart.CreateRemote(const MachineName: string): IrdtmCountryPart;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmCountryPart) as IrdtmCountryPart;
end;

class function CordtmProvince.Create: IrdtmProvince;
begin
  Result := CreateComObject(CLASS_rdtmProvince) as IrdtmProvince;
end;

class function CordtmProvince.CreateRemote(const MachineName: string): IrdtmProvince;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmProvince) as IrdtmProvince;
end;

class function CordtmLanguage.Create: IrdtmLanguage;
begin
  Result := CreateComObject(CLASS_rdtmLanguage) as IrdtmLanguage;
end;

class function CordtmLanguage.CreateRemote(const MachineName: string): IrdtmLanguage;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmLanguage) as IrdtmLanguage;
end;

class function CordtmPostalCode.Create: IrdtmPostalCode;
begin
  Result := CreateComObject(CLASS_rdtmPostalCode) as IrdtmPostalCode;
end;

class function CordtmPostalCode.CreateRemote(const MachineName: string): IrdtmPostalCode;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmPostalCode) as IrdtmPostalCode;
end;

class function CordtmStatus.Create: IrdtmStatus;
begin
  Result := CreateComObject(CLASS_rdtmStatus) as IrdtmStatus;
end;

class function CordtmStatus.CreateRemote(const MachineName: string): IrdtmStatus;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmStatus) as IrdtmStatus;
end;

class function CordtmDiploma.Create: IrdtmDiploma;
begin
  Result := CreateComObject(CLASS_rdtmDiploma) as IrdtmDiploma;
end;

class function CordtmDiploma.CreateRemote(const MachineName: string): IrdtmDiploma;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmDiploma) as IrdtmDiploma;
end;

class function CordtmSessionGroup.Create: IrdtmSessionGroup;
begin
  Result := CreateComObject(CLASS_rdtmSessionGroup) as IrdtmSessionGroup;
end;

class function CordtmSessionGroup.CreateRemote(const MachineName: string): IrdtmSessionGroup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSessionGroup) as IrdtmSessionGroup;
end;

class function CordtmRoleGroup.Create: IrdtmRoleGroup;
begin
  Result := CreateComObject(CLASS_rdtmRoleGroup) as IrdtmRoleGroup;
end;

class function CordtmRoleGroup.CreateRemote(const MachineName: string): IrdtmRoleGroup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmRoleGroup) as IrdtmRoleGroup;
end;

class function CordtmSchoolYear.Create: IrdtmSchoolYear;
begin
  Result := CreateComObject(CLASS_rdtmSchoolYear) as IrdtmSchoolYear;
end;

class function CordtmSchoolYear.CreateRemote(const MachineName: string): IrdtmSchoolYear;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSchoolYear) as IrdtmSchoolYear;
end;

class function CordtmProgDiscipline.Create: IrdtmProgDiscipline;
begin
  Result := CreateComObject(CLASS_rdtmProgDiscipline) as IrdtmProgDiscipline;
end;

class function CordtmProgDiscipline.CreateRemote(const MachineName: string): IrdtmProgDiscipline;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmProgDiscipline) as IrdtmProgDiscipline;
end;

class function CordtmUser.Create: IrdtmUser;
begin
  Result := CreateComObject(CLASS_rdtmUser) as IrdtmUser;
end;

class function CordtmUser.CreateRemote(const MachineName: string): IrdtmUser;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmUser) as IrdtmUser;
end;

class function CordtmProfile.Create: IrdtmProfile;
begin
  Result := CreateComObject(CLASS_rdtmProfile) as IrdtmProfile;
end;

class function CordtmProfile.CreateRemote(const MachineName: string): IrdtmProfile;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmProfile) as IrdtmProfile;
end;

class function CordtmUserProfile.Create: IrdtmUserProfile;
begin
  Result := CreateComObject(CLASS_rdtmUserProfile) as IrdtmUserProfile;
end;

class function CordtmUserProfile.CreateRemote(const MachineName: string): IrdtmUserProfile;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmUserProfile) as IrdtmUserProfile;
end;

class function CordtmGender.Create: IrdtmGender;
begin
  Result := CreateComObject(CLASS_rdtmGender) as IrdtmGender;
end;

class function CordtmGender.CreateRemote(const MachineName: string): IrdtmGender;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmGender) as IrdtmGender;
end;

class function CordtmTitle.Create: IrdtmTitle;
begin
  Result := CreateComObject(CLASS_rdtmTitle) as IrdtmTitle;
end;

class function CordtmTitle.CreateRemote(const MachineName: string): IrdtmTitle;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmTitle) as IrdtmTitle;
end;

class function CordtmMedium.Create: IrdtmMedium;
begin
  Result := CreateComObject(CLASS_rdtmMedium) as IrdtmMedium;
end;

class function CordtmMedium.CreateRemote(const MachineName: string): IrdtmMedium;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmMedium) as IrdtmMedium;
end;

class function CordtmOrganisationType.Create: IrdtmOrganisationType;
begin
  Result := CreateComObject(CLASS_rdtmOrganisationType) as IrdtmOrganisationType;
end;

class function CordtmOrganisationType.CreateRemote(const MachineName: string): IrdtmOrganisationType;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmOrganisationType) as IrdtmOrganisationType;
end;

class function CordtmOrganisation.Create: IrdtmOrganisation;
begin
  Result := CreateComObject(CLASS_rdtmOrganisation) as IrdtmOrganisation;
end;

class function CordtmOrganisation.CreateRemote(const MachineName: string): IrdtmOrganisation;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmOrganisation) as IrdtmOrganisation;
end;

class function CordtmPerson.Create: IrdtmPerson;
begin
  Result := CreateComObject(CLASS_rdtmPerson) as IrdtmPerson;
end;

class function CordtmPerson.CreateRemote(const MachineName: string): IrdtmPerson;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmPerson) as IrdtmPerson;
end;

class function CordtmInstructor.Create: IrdtmInstructor;
begin
  Result := CreateComObject(CLASS_rdtmInstructor) as IrdtmInstructor;
end;

class function CordtmInstructor.CreateRemote(const MachineName: string): IrdtmInstructor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmInstructor) as IrdtmInstructor;
end;

class function CordtmProgInstructor.Create: IrdtmProgInstructor;
begin
  Result := CreateComObject(CLASS_rdtmProgInstructor) as IrdtmProgInstructor;
end;

class function CordtmProgInstructor.CreateRemote(const MachineName: string): IrdtmProgInstructor;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmProgInstructor) as IrdtmProgInstructor;
end;

class function CordtmRole.Create: IrdtmRole;
begin
  Result := CreateComObject(CLASS_rdtmRole) as IrdtmRole;
end;

class function CordtmRole.CreateRemote(const MachineName: string): IrdtmRole;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmRole) as IrdtmRole;
end;

class function CordtmProgProgram.Create: IrdtmProgProgram;
begin
  Result := CreateComObject(CLASS_rdtmProgProgram) as IrdtmProgProgram;
end;

class function CordtmProgProgram.CreateRemote(const MachineName: string): IrdtmProgProgram;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmProgProgram) as IrdtmProgProgram;
end;

class function CordtmSesSession.Create: IrdtmSesSession;
begin
  Result := CreateComObject(CLASS_rdtmSesSession) as IrdtmSesSession;
end;

class function CordtmSesSession.CreateRemote(const MachineName: string): IrdtmSesSession;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSesSession) as IrdtmSesSession;
end;

class function CordtmCenInfrastructure.Create: IrdtmCenInfrastructure;
begin
  Result := CreateComObject(CLASS_rdtmCenInfrastructure) as IrdtmCenInfrastructure;
end;

class function CordtmCenInfrastructure.CreateRemote(const MachineName: string): IrdtmCenInfrastructure;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmCenInfrastructure) as IrdtmCenInfrastructure;
end;

class function CordtmSesExpenses.Create: IrdtmSesExpenses;
begin
  Result := CreateComObject(CLASS_rdtmSesExpenses) as IrdtmSesExpenses;
end;

class function CordtmSesExpenses.CreateRemote(const MachineName: string): IrdtmSesExpenses;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSesExpenses) as IrdtmSesExpenses;
end;

class function CordtmEnrolment.Create: IrdtmEnrolment;
begin
  Result := CreateComObject(CLASS_rdtmEnrolment) as IrdtmEnrolment;
end;

class function CordtmEnrolment.CreateRemote(const MachineName: string): IrdtmEnrolment;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmEnrolment) as IrdtmEnrolment;
end;

class function CordtmSesWizard.Create: IrdtmSesWizard;
begin
  Result := CreateComObject(CLASS_rdtmSesWizard) as IrdtmSesWizard;
end;

class function CordtmSesWizard.CreateRemote(const MachineName: string): IrdtmSesWizard;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSesWizard) as IrdtmSesWizard;
end;

class function CordtmSesInstructorRoom.Create: IrdtmSesInstructorRoom;
begin
  Result := CreateComObject(CLASS_rdtmSesInstructorRoom) as IrdtmSesInstructorRoom;
end;

class function CordtmSesInstructorRoom.CreateRemote(const MachineName: string): IrdtmSesInstructorRoom;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSesInstructorRoom) as IrdtmSesInstructorRoom;
end;

class function CordtmCenRoom.Create: IrdtmCenRoom;
begin
  Result := CreateComObject(CLASS_rdtmCenRoom) as IrdtmCenRoom;
end;

class function CordtmCenRoom.CreateRemote(const MachineName: string): IrdtmCenRoom;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmCenRoom) as IrdtmCenRoom;
end;

class function CordtmQueryGroup.Create: IrdtmQueryGroup;
begin
  Result := CreateComObject(CLASS_rdtmQueryGroup) as IrdtmQueryGroup;
end;

class function CordtmQueryGroup.CreateRemote(const MachineName: string): IrdtmQueryGroup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmQueryGroup) as IrdtmQueryGroup;
end;

class function CordtmQuery.Create: IrdtmQuery;
begin
  Result := CreateComObject(CLASS_rdtmQuery) as IrdtmQuery;
end;

class function CordtmQuery.CreateRemote(const MachineName: string): IrdtmQuery;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmQuery) as IrdtmQuery;
end;

class function CordtmParameter.Create: IrdtmParameter;
begin
  Result := CreateComObject(CLASS_rdtmParameter) as IrdtmParameter;
end;

class function CordtmParameter.CreateRemote(const MachineName: string): IrdtmParameter;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmParameter) as IrdtmParameter;
end;

class function CordtmParameterType.Create: IrdtmParameterType;
begin
  Result := CreateComObject(CLASS_rdtmParameterType) as IrdtmParameterType;
end;

class function CordtmParameterType.CreateRemote(const MachineName: string): IrdtmParameterType;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmParameterType) as IrdtmParameterType;
end;

class function CordtmLogHistory.Create: IrdtmLogHistory;
begin
  Result := CreateComObject(CLASS_rdtmLogHistory) as IrdtmLogHistory;
end;

class function CordtmLogHistory.CreateRemote(const MachineName: string): IrdtmLogHistory;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmLogHistory) as IrdtmLogHistory;
end;

class function CordtmTableField.Create: IrdtmTableField;
begin
  Result := CreateComObject(CLASS_rdtmTableField) as IrdtmTableField;
end;

class function CordtmTableField.CreateRemote(const MachineName: string): IrdtmTableField;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmTableField) as IrdtmTableField;
end;

class function CordtmQueryWizard.Create: IrdtmQueryWizard;
begin
  Result := CreateComObject(CLASS_rdtmQueryWizard) as IrdtmQueryWizard;
end;

class function CordtmQueryWizard.CreateRemote(const MachineName: string): IrdtmQueryWizard;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmQueryWizard) as IrdtmQueryWizard;
end;

class function CordtmReport.Create: IrdtmReport;
begin
  Result := CreateComObject(CLASS_rdtmReport) as IrdtmReport;
end;

class function CordtmReport.CreateRemote(const MachineName: string): IrdtmReport;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmReport) as IrdtmReport;
end;

class function CordtmImportOverview.Create: IrdtmImportOverview;
begin
  Result := CreateComObject(CLASS_rdtmImportOverview) as IrdtmImportOverview;
end;

class function CordtmImportOverview.CreateRemote(const MachineName: string): IrdtmImportOverview;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmImportOverview) as IrdtmImportOverview;
end;

class function CordtmDocCenter.Create: IrdtmDocCenter;
begin
  Result := CreateComObject(CLASS_rdtmDocCenter) as IrdtmDocCenter;
end;

class function CordtmDocCenter.CreateRemote(const MachineName: string): IrdtmDocCenter;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmDocCenter) as IrdtmDocCenter;
end;

class function CordtmSesFact.Create: IrdtmSesFact;
begin
  Result := CreateComObject(CLASS_rdtmSesFact) as IrdtmSesFact;
end;

class function CordtmSesFact.CreateRemote(const MachineName: string): IrdtmSesFact;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSesFact) as IrdtmSesFact;
end;

class function CordtmInvoicing.Create: IrdtmInvoicing;
begin
  Result := CreateComObject(CLASS_rdtmInvoicing) as IrdtmInvoicing;
end;

class function CordtmInvoicing.CreateRemote(const MachineName: string): IrdtmInvoicing;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmInvoicing) as IrdtmInvoicing;
end;

class function CordtmImport.Create: IrdtmImport;
begin
  Result := CreateComObject(CLASS_rdtmImport) as IrdtmImport;
end;

class function CordtmImport.CreateRemote(const MachineName: string): IrdtmImport;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmImport) as IrdtmImport;
end;

class function CordtmConfirmationReports.Create: IrdtmConfirmationReports;
begin
  Result := CreateComObject(CLASS_rdtmConfirmationReports) as IrdtmConfirmationReports;
end;

class function CordtmConfirmationReports.CreateRemote(const MachineName: string): IrdtmConfirmationReports;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmConfirmationReports) as IrdtmConfirmationReports;
end;

class function CordtmSessionWizard.Create: IrdtmSessionWizard;
begin
  Result := CreateComObject(CLASS_rdtmSessionWizard) as IrdtmSessionWizard;
end;

class function CordtmSessionWizard.CreateRemote(const MachineName: string): IrdtmSessionWizard;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmSessionWizard) as IrdtmSessionWizard;
end;

class function CordtmCooperation.Create: IrdtmCooperation;
begin
  Result := CreateComObject(CLASS_rdtmCooperation) as IrdtmCooperation;
end;

class function CordtmCooperation.CreateRemote(const MachineName: string): IrdtmCooperation;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmCooperation) as IrdtmCooperation;
end;

class function CordtmKMO_PORT_Status.Create: IrdtmKMO_PORT_Status;
begin
  Result := CreateComObject(CLASS_rdtmKMO_PORT_Status) as IrdtmKMO_PORT_Status;
end;

class function CordtmKMO_PORT_Status.CreateRemote(const MachineName: string): IrdtmKMO_PORT_Status;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmKMO_PORT_Status) as IrdtmKMO_PORT_Status;
end;

class function CordtmKMO_Portefeuille.Create: IrdtmKMO_Portefeuille;
begin
  Result := CreateComObject(CLASS_rdtmKMO_Portefeuille) as IrdtmKMO_Portefeuille;
end;

class function CordtmKMO_Portefeuille.CreateRemote(const MachineName: string): IrdtmKMO_Portefeuille;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmKMO_Portefeuille) as IrdtmKMO_Portefeuille;
end;

class function CordtmKMO_PORT_Payment.Create: IrdtmKMO_PORT_Payment;
begin
  Result := CreateComObject(CLASS_rdtmKMO_PORT_Payment) as IrdtmKMO_PORT_Payment;
end;

class function CordtmKMO_PORT_Payment.CreateRemote(const MachineName: string): IrdtmKMO_PORT_Payment;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmKMO_PORT_Payment) as IrdtmKMO_PORT_Payment;
end;

class function CordtmKMO_PORT_Link_Port_Payment.Create: IrdtmKMO_PORT_Link_Port_Payment;
begin
  Result := CreateComObject(CLASS_rdtmKMO_PORT_Link_Port_Payment) as IrdtmKMO_PORT_Link_Port_Payment;
end;

class function CordtmKMO_PORT_Link_Port_Payment.CreateRemote(const MachineName: string): IrdtmKMO_PORT_Link_Port_Payment;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmKMO_PORT_Link_Port_Payment) as IrdtmKMO_PORT_Link_Port_Payment;
end;

class function CordtmKMO_PORT_Link_Port_Session.Create: IrdtmKMO_PORT_Link_Port_Session;
begin
  Result := CreateComObject(CLASS_rdtmKMO_PORT_Link_Port_Session) as IrdtmKMO_PORT_Link_Port_Session;
end;

class function CordtmKMO_PORT_Link_Port_Session.CreateRemote(const MachineName: string): IrdtmKMO_PORT_Link_Port_Session;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmKMO_PORT_Link_Port_Session) as IrdtmKMO_PORT_Link_Port_Session;
end;

class function CordtmImport_Excel.Create: IrdtmImport_Excel;
begin
  Result := CreateComObject(CLASS_rdtmImport_Excel) as IrdtmImport_Excel;
end;

class function CordtmImport_Excel.CreateRemote(const MachineName: string): IrdtmImport_Excel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmImport_Excel) as IrdtmImport_Excel;
end;

class function CordtmProgramType.Create: IrdtmProgramType;
begin
  Result := CreateComObject(CLASS_rdtmProgramType) as IrdtmProgramType;
end;

class function CordtmProgramType.CreateRemote(const MachineName: string): IrdtmProgramType;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_rdtmProgramType) as IrdtmProgramType;
end;

class function CoData_Country.Create: IData_Country;
begin
  Result := CreateComObject(CLASS_Data_Country) as IData_Country;
end;

class function CoData_Country.CreateRemote(const MachineName: string): IData_Country;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Data_Country) as IData_Country;
end;

class function CoRemote_Country_Test.Create: IRemote_Country_Test;
begin
  Result := CreateComObject(CLASS_Remote_Country_Test) as IRemote_Country_Test;
end;

class function CoRemote_Country_Test.CreateRemote(const MachineName: string): IRemote_Country_Test;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Remote_Country_Test) as IRemote_Country_Test;
end;

end.
