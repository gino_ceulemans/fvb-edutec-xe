object rdtmEduBase: TrdtmEduBase
  OldCreateOrder = False
  Height = 342
  Width = 524
  object adoqryList: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    Parameters = <>
    AutoOpen = False
    Left = 40
    Top = 24
  end
  object adoqryRecord: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    Parameters = <>
    AutoOpen = False
    Left = 136
    Top = 24
  end
  object prvList: TFVBFFCDataSetProvider
    DataSet = adoqryList
    OnGetTableName = prvListGetTableName
    Left = 40
    Top = 72
  end
  object prvRecord: TFVBFFCDataSetProvider
    DataSet = adoqryRecord
    OnGetTableName = prvListGetTableName
    Left = 136
    Top = 72
  end
end
