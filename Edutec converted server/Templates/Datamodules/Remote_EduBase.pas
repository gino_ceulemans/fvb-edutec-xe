{*****************************************************************************
  This unit will contain the Base Remote DataModule from which all other
  Remote DataModules in the Edutec Project should inherit.
  
  @Name       Remote_EduBase
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  06/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_EduBase;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, Provider, Unit_FVBFFCDBComponents,
  DB, ADODB, ActiveX;

type
  TrdtmEduBase = class(TRemoteDataModule, IrdtmEduBase)
    adoqryList: TFVBFFCQuery;
    adoqryRecord: TFVBFFCQuery;
    prvList: TFVBFFCDataSetProvider;
    prvRecord: TFVBFFCDataSetProvider;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
  private
    { Private declarations }
  protected
    FrdtmEDUMain : IrdtmEDUMain;
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function ApplyDetailFilter(const aProvider, aFilter: WideString): Integer;
      virtual; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      virtual; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      virtual; safecall;

    function GetProviderByName( aProviderName : String ) : TFVBFFCDataSetProvider; virtual;
    function GetTableName                                : string;                 virtual; abstract;
    function Get_ADOConnection: Integer; safecall;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      safecall;
    function GetNewRecordID: SYSINT; virtual; safecall;
    procedure Set_ADOConnection(Param1: Integer); virtual; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); virtual; safecall;
  public
    { Public declarations }
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Remote_EduMain,
  Unit_FVBFFCInterfaces;

{$R *.DFM}

class procedure TrdtmEduBase.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

{*****************************************************************************
  This method will be used to apply a ( Permanent ) System Filter to an ADO
  Query component.

  @Name       TrdtmEduBase.ApplyDetailFilter
  @author     slesage
  @param      aProvider   The Provider associated to the Query for which we
                          want to apply a Filter.
              aFilter     The SQL Where Clause which should be added to the
                          Query.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEduBase.ApplyDetailFilter(const aProvider,
  aFilter: WideString): Integer;
var
  aDataSetProvider : TFVBFFCDataSetProvider;
  aFVBFFCDataSet   : IFVBFFCDataSetForFilterAndSort;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'ApplyDetailFilter' );
  {$ENDIF}

  try
    aDataSetProvider := GetProviderByName( aProvider );

    if ( Assigned( aDataSetProvider ) ) and
       ( Assigned( aDataSetProvider.DataSet ) ) and
       ( Supports( aDataSetProvider.DataSet, IFVBFFCDataSetForFilterAndSort, aFVBFFCDataSet ) ) then
    begin
      aFVBFFCDataSet.ApplyDetailFilter( aFilter );
    end;
    Result := 0;
  Except
    Result := -1;
  End;
  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'ApplyDetailFilter' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to apply a User Filter to an ADO Query component.

  @Name       TrdtmEduBase.ApplyFilter
  @author     slesage
  @param      aProvider   The Provider associated to the Query for which we
                          want to apply a Filter.
              aFilter     The SQL Where Clause which should be added to the
                          Query.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEduBase.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
var
  aDataSetProvider : TFVBFFCDataSetProvider;
  aFVBFFCDataSet   : IFVBFFCDataSetForFilterAndSort;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'ApplyFilter' );
  {$ENDIF}
  try
    aDataSetProvider := GetProviderByName( aProvider );

    if ( Assigned( aDataSetProvider ) ) and
       ( Assigned( aDataSetProvider.DataSet ) ) and
       ( Supports( aDataSetProvider.DataSet, IFVBFFCDataSetForFilterAndSort, aFVBFFCDataSet ) ) then
    begin
      aFVBFFCDataSet.ApplyFilter( aFilter );
    end;
    Result := 0;
  Except
    Result := -1;
  End;
  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'ApplyFilter' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to apply an Order By to an ADO Query component.

  @Name       TrdtmEduBase.ApplyOrderBy
  @author     slesage
  @param      aProvider   The Provider associated to the Query for which we
                          want to apply a Filter.
              aOrderBy    The SQL ORDER BY Clause which should be added to the
                          Query.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEduBase.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
var
  aDataSetProvider : TFVBFFCDataSetProvider;
  aFVBFFCDataSet   : IFVBFFCDataSetForFilterAndSort;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'ApplyOrderBy' );
  {$ENDIF}

  try
    aDataSetProvider := GetProviderByName( aProvider );

    if ( Assigned( aDataSetProvider ) ) and
       ( Assigned( aDataSetProvider.DataSet ) ) and
       ( Supports( aDataSetProvider.DataSet, IFVBFFCDataSetForFilterAndSort, aFVBFFCDataSet ) ) then
    begin
      aFVBFFCDataSet.ApplyOrderBy( aOrderBy );
    end;
    Result := 0;
  Except
    Result := -1;
  End;
  
  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'ApplyOrderBy' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to fetch a new RecordID.

  @Name       TrdtmEduBase.GetNewRecordID
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEduBase.GetNewRecordID: SYSINT;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetNewRecordID' );
  {$ENDIF}

  Result := FrdtmEDUMain.GetNewRecordID( GetTableName );
  
  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'GetNewRecordID' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to set the ADO Connection for all ADO Components
  based on the connection in the Main Remote DataModule.

  @Name       TrdtmEduBase.Set_ADOConnection
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TrdtmEduBase.Set_ADOConnection(Param1: Integer);
var
  idxComponent : Integer;
begin
  for idxComponent := 0 to Pred( ComponentCount ) do
  begin
    if ( Components[ idxComponent ] is TCustomADODataSet ) then
    begin
      TCustomADODataSet( Components[ idxComponent ] ).Connection := TADOConnection( Param1 );
    end
    else if ( Components[ idxComponent ] is TADOCommand ) then
    begin
      TADOCommand( Components[ idxComponent ] ).Connection := TADOConnection( Param1 );
    end;
  end;
//  adoqryList.Connection := TADOConnection( Param1 );
//  adoqryRecord.Connection := TADOConnection( Param1 );
end;

{*****************************************************************************
  Property Setter for the rdtmEDUMain property.

  @Name       TrdtmEduBase.Set_rdtmEDUMain
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TrdtmEduBase.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  FrdtmEDUMain := Param1;
end;

{*****************************************************************************
  This function will be used to find a Provider component with the given Name.

  @Name       TrdtmEduBase.GetProviderByName
  @author     slesage
  @param      aProviderName   The Name of the Provider Component we are looking
                              for.
  @return     Returns the provider component which has the given aProviderName,
              if no provider was found with that name, it returns nil;
  @Exception  None
  @See        None
******************************************************************************}

function TrdtmEduBase.GetProviderByName(
  aProviderName: String): TFVBFFCDataSetProvider;
var
  aComponent : TComponent;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'GetProviderByName' );
  csFVBFFCServer.SendString( 'aProviderName', aProviderName );
  {$ENDIF}

  Result := Nil;
  aComponent := FindComponent( aProviderName );

  if ( ( Assigned( aComponent ) ) and
       ( aComponent is TFVBFFCDataSetProvider ) ) then
  begin
    Result := TFVBFFCDataSetProvider( aComponent );
  end;

  {$IFDEF CODESITE}
  csFVBFFCServer.SendObject( 'Result', Result );
  csFVBFFCServer.ExitMethod( Self, 'GetProviderByName' );
  {$ENDIF}
end;

{*****************************************************************************
  This method is executed when a resolver initializes its information about
  the table to which it applies updates.

  @Name       TrdtmEduBase.prvListGetTableName
  @author     slesage
  @param      Sender      The provider that needs the table name for applying
                          updates.
  @param      DataSet     The dataset to which updates should be applied. This
                          may be the providerís source dataset or a nested
                          detail dataset.
  @param      TableName   Returns the name of the target table. This is the
                          name used in generated SQL statements that insert,
                          delete, or modify records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TrdtmEduBase.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'prvListGetTableName' );
  csFVBFFCServer.SendObject( 'Sender', Sender );
  csFVBFFCServer.SendObject( 'DataSet', DataSet );
  {$ENDIF}
  TableName := GetTableName;
  {$IFDEF CODESITE}
  csFVBFFCServer.SendString( 'TableName', TableName );
  csFVBFFCServer.ExitMethod( Self, 'prvListGetTableName' );
  {$ENDIF}
end;

function TrdtmEduBase.Get_ADOConnection: Integer;
begin
//  Result := Integer( adoqryList.Connection ); 
end;

function TrdtmEduBase.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
var
  aDataSetProvider : TFVBFFCDataSetProvider;
  aFVBFFCDataSet   : IFVBFFCDataSetForFilterAndSort;
begin
  {$IFDEF CODESITE}
  csFVBFFCServer.EnterMethod( Self, 'SetSQLStatement' );
  {$ENDIF}
  try
    aDataSetProvider := GetProviderByName( aProvider );

    if ( Assigned( aDataSetProvider ) ) and
       ( Assigned( aDataSetProvider.DataSet ) ) and
       ( Supports( aDataSetProvider.DataSet, IFVBFFCDataSetForFilterAndSort, aFVBFFCDataSet ) ) then
    begin
      aFVBFFCDataSet.SetSQLStatement( aSQL );
    end;
    Result := 0;
  Except
    Result := -1;
  End;
  {$IFDEF CODESITE}
  csFVBFFCServer.ExitMethod( Self, 'SetSQLStatement' );
  {$ENDIF}
end;

initialization
  TComponentFactory.Create(ComServer, TrdtmEduBase,
    Class_rdtmEduBase, ciInternal, tmApartment);
end.
