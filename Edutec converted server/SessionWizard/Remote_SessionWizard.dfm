inherited rdtm_SessionWizard: Trdtm_SessionWizard
  OldCreateOrder = True
  inherited adoqryList: TFVBFFCQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'SessionId'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '       F_ENROL_ID,'
      '       F_SESSION_ID,'
      '       F_PERSON_ID,'
      '       F_PERSON_NAME,'
      '       F_PERSON_ADDRESS,'
      '       F_SOCSEC_NR,'
      '       F_ORGANISATION,'
      '       F_ORGANISATION_ID,'
      '       F_STATUS_NAME,'
      '       F_PRICING_SCHEME,'
      '       F_SELECTED_PRICE,'
      '       F_ROLEGROUP_NAME,'
      '       F_CONSTRUCT'
      'FROM V_SES_WIZARD_ENROL'
      'WHERE F_SESSION_ID = :SessionId')
    object adoqryListF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object adoqryListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = []
    end
    object adoqryListF_PERSON_NAME: TStringField
      FieldName = 'F_PERSON_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 129
    end
    object adoqryListF_PERSON_ADDRESS: TStringField
      FieldName = 'F_PERSON_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 354
    end
    object adoqryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryListF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object adoqryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_SELECTED_PRICE: TFloatField
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryListF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryListF_CONSTRUCT: TBooleanField
      FieldName = 'F_CONSTRUCT'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  inherited adoqryRecord: TFVBFFCQuery
    CursorType = ctStatic
    Left = 192
    Top = 192
    object adoqryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = []
    end
    object adoqryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    Left = 192
    Top = 240
  end
  object adoqryOrganisation: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Organisation'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'Postalcode1'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Postalcode2'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Postalcode3'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'City'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 128
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'#9
      '  o.F_ORGANISATION_ID, '
      '  o.F_ORGANISATION, '
      '  o.F_STREET,'
      '  o.F_NUMBER, '
      '  o.F_MAILBOX, '
      '  o.F_POSTALCODE_ID, '
      '  o.F_POSTALCODE,'
      '  o.F_CITY_NAME,'
      '  o.F_COUNTRY_ID,'
      '  o.F_COUNTRY_NAME,'
      '  o.F_ORGTYPE_ID,'
      '  o.F_ORGTYPE_NAME,'
      '  o.F_CONTACT_DATA,'
      '  o.F_CONFIRMATION_CONTACT1,'
      '  o.F_CONFIRMATION_CONTACT1_EMAIL,'
      '  o.F_CONFIRMATION_CONTACT2,'
      '  o.F_CONFIRMATION_CONTACT2_EMAIL,'
      '  o.F_ACTIVE,'
      '  o.F_MANUALLY_ADDED'
      '  '
      'FROM V_SES_WIZARD_ORGANISATION o'
      'WHERE (F_ORGANISATION like '#39'%'#39' + :Organisation + '#39'%'#39' )'
      
        '  AND (    (F_POSTALCODE_ID =  :Postalcode1 AND :Postalcode2 > 0' +
        ') '
      
        '              or (:Postalcode3 = 0 AND (F_POSTALCODE_ID IS NULL ' +
        'or F_POSTALCODE_ID IS NOT NULL))'
      '           )'
      
        '  AND  ((F_CITY_NAME like '#39'%'#39' + :City + '#39'%'#39') or (F_CITY_NAME IS ' +
        'NULL))')
    AutoOpen = False
    Left = 120
    Top = 24
    object adoqryOrganisationF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryOrganisationF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryOrganisationF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryOrganisationF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryOrganisationF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryOrganisationF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryOrganisationF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryOrganisationF_CONTACT_DATA: TStringField
      FieldName = 'F_CONTACT_DATA'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object adoqryOrganisationF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryOrganisationF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryOrganisationF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryOrganisationF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryOrganisationF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryOrganisationF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryOrganisationF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryOrganisationF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
    object adoqryOrganisationF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryOrganisationF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryOrganisationF_MANUALLY_ADDED: TBooleanField
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvOrganisation: TFVBFFCDataSetProvider
    DataSet = adoqryOrganisation
    OnGetTableName = prvOrganisationGetTableName
    Left = 120
    Top = 72
  end
  object adoqryRoles: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'OrganisationID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      '  r.F_ROLE_ID, '
      '  r.F_PERSON_ID, '
      '  r.F_FIRSTNAME,'
      '  r.F_LASTNAME,'
      '  r.F_ORGANISATION_ID, '
      '  r.F_NAME,'
      '  r.F_ROLEGROUP_ID, '
      '  r.F_ROLEGROUP_NAME,'
      '  r.F_START_DATE, '
      '  r.F_END_DATE,'
      '  r.F_STREET,'
      '  r.F_NUMBER,'
      '  r.F_MAILBOX,'
      '  r.F_POSTALCODE,'
      '  r.F_POSTALCODE_ID,'
      '  r.F_CITY_NAME,'
      '  r.F_COUNTRY_ID,'
      '  r.F_COUNTRY_NAME,'
      '  r.F_GENDER_ID,'
      '  r.F_GENDER_NAME,'
      '  r.F_DATEBIRTH,'
      '  r.F_DIPLOMA_ID,'
      '  r.F_DIPLOMA_NAME'
      '  '
      'FROM V_SES_WIZARD_ROLE r'
      'WHERE F_ORGANISATION_ID = :OrganisationID'
      'ORDER BY r.F_LASTNAME ASC')
    AutoOpen = False
    Left = 216
    Top = 24
    object adoqryRolesF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object adoqryRolesF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRolesF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRolesF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRolesF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRolesF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object adoqryRolesF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object adoqryRolesF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object adoqryRolesF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryRolesF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object adoqryRolesF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRolesF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object adoqryRolesF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRolesF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  object prvRoles: TFVBFFCDataSetProvider
    DataSet = adoqryRoles
    OnGetTableName = prvRolesGetTableName
    Left = 212
    Top = 72
  end
  object adop_SES_WIZARD_ENROL: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_SES_WIZARD_ENROL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@ASessionId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@ARoleId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@APersonAlreadyEnrolled'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end
      item
        Name = '@ARolegroupId'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 336
    Top = 120
  end
  object adoP_SES_WIZARD_REMOVE_ENROLMENT: TFVBFFCStoredProc
    Connection = rdtmEDUMain.adocnnMain
    ProcedureName = 'P_SES_WIZARD_REMOVE_ENROLMENT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@AEnrolID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    AutoOpen = False
    Left = 336
    Top = 168
  end
  object adoqryLookupPerson: TFVBFFCQuery
    Connection = rdtmEDUMain.adocnnMain
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Lastname'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'Firstname'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      #9'pe.F_PERSON_ID,'
      #9'pe.F_SOCSEC_NR,'
      #9'pe.F_LASTNAME,'
      #9'pe.F_FIRSTNAME,'
      #9'pe.F_PERSON_ADDRESS,'
      #9'pe.F_ROLE_ID,'
      #9'pe.F_ORGANISATION_NAME,'
      #9'pe.F_ROLEGROUP_NAME,'
      #9'pe.F_DATEBIRTH,'
      #9'pe.F_GENDER_NAME,'
      #9'pe.F_GENDER_ID,'
      #9'pe.F_ROLEGROUP_ID,'
      #9'pe.F_STREET,'
      #9'pe.F_NUMBER,'
      #9'pe.F_MAILBOX,'
      #9'pe.F_POSTALCODE_ID,'
      #9'pe.F_POSTALCODE,'
      #9'pe.F_CITY_NAME,'
      #9'pe.F_COUNTRY_NAME,'
      #9'pe.F_COUNTRY_ID,'
      '                pe.F_DIPLOMA_ID,'
      '                pe.F_DIPLOMA_NAME'#9#9
      ''
      'FROM V_SES_WIZARD_LOOKUP_PERSON pe'
      'WHERE (pe.F_LASTNAME like '#39'%'#39' + :Lastname + '#39'%'#39')'
      '               and (pe.F_FIRSTNAME like '#39'%'#39' + :Firstname + '#39'%'#39')')
    AutoOpen = False
    Left = 40
    Top = 144
    object adoqryLookupPersonF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object adoqryLookupPersonF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryLookupPersonF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryLookupPersonF_PERSON_ADDRESS: TStringField
      FieldName = 'F_PERSON_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 354
    end
    object adoqryLookupPersonF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object adoqryLookupPersonF_ORGANISATION_NAME: TStringField
      FieldName = 'F_ORGANISATION_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryLookupPersonF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryLookupPersonF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryLookupPersonF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = []
      Size = 128
    end
    object adoqryLookupPersonF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = []
      Size = 5
    end
    object adoqryLookupPersonF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = []
      Size = 10
    end
    object adoqryLookupPersonF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object adoqryLookupPersonF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 128
    end
    object adoqryLookupPersonF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object adoqryLookupPersonF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = []
    end
    object adoqryLookupPersonF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  object prvLookupPerson: TFVBFFCDataSetProvider
    DataSet = adoqryLookupPerson
    OnGetTableName = prvLookupPersonGetTableName
    Left = 40
    Top = 200
  end
end
