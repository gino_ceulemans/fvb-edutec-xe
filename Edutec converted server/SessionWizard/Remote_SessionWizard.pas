{*****************************************************************************
  This unit contains the Remote DataModule used for Session Wizard

  @Name       Remote_SessionWizard
  @Author     Ivan Van den Bossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  03/10/2007   ivdbossche           Initial creation of the Unit.
******************************************************************************}

unit Remote_SessionWizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Remote_EduBase, Remote_EduMain, Provider, Unit_FVBFFCDBComponents, DB, DBClient, ADODB, VCLCom, EdutecServerD10_TLB, ActiveX;

type
  Trdtm_SessionWizard = class(TrdtmEduBase, IrdtmSessionWizard)
    adoqryListF_ENROL_ID: TIntegerField;
    adoqryListF_SESSION_ID: TIntegerField;
    adoqryListF_PERSON_ID: TIntegerField;
    adoqryListF_PERSON_NAME: TStringField;
    adoqryListF_PERSON_ADDRESS: TStringField;
    adoqryListF_SOCSEC_NR: TStringField;
    adoqryListF_ORGANISATION: TStringField;
    adoqryListF_STATUS_NAME: TStringField;
    adoqryListF_PRICING_SCHEME: TIntegerField;
    adoqryListF_SELECTED_PRICE: TFloatField;
    adoqryListF_ROLEGROUP_NAME: TStringField;
    adoqryListF_ORGANISATION_ID: TIntegerField;
    adoqryRecordF_START_DATE: TDateTimeField;
    adoqryRecordF_END_DATE: TDateTimeField;
    adoqryRecordF_NAME: TStringField;
    adoqryRecordF_SESSION_ID: TIntegerField;
    adoqryOrganisation: TFVBFFCQuery;
    prvOrganisation: TFVBFFCDataSetProvider;
    adoqryOrganisationF_ORGANISATION_ID: TIntegerField;
    adoqryOrganisationF_STREET: TStringField;
    adoqryOrganisationF_NUMBER: TStringField;
    adoqryOrganisationF_MAILBOX: TStringField;
    adoqryOrganisationF_POSTALCODE_ID: TIntegerField;
    adoqryOrganisationF_COUNTRY_ID: TIntegerField;
    adoqryOrganisationF_ORGTYPE_ID: TIntegerField;
    adoqryOrganisationF_CONTACT_DATA: TStringField;
    adoqryOrganisationF_CONFIRMATION_CONTACT1: TStringField;
    adoqryOrganisationF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    adoqryOrganisationF_CONFIRMATION_CONTACT2: TStringField;
    adoqryOrganisationF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    adoqryOrganisationF_ORGANISATION: TStringField;
    adoqryOrganisationF_POSTALCODE: TStringField;
    adoqryOrganisationF_COUNTRY_NAME: TStringField;
    adoqryOrganisationF_ORGTYPE_NAME: TStringField;
    adoqryOrganisationF_CITY_NAME: TStringField;
    adoqryRoles: TFVBFFCQuery;
    prvRoles: TFVBFFCDataSetProvider;
    adoqryRolesF_ROLE_ID: TIntegerField;
    adoqryRolesF_PERSON_ID: TIntegerField;
    adoqryRolesF_FIRSTNAME: TStringField;
    adoqryRolesF_LASTNAME: TStringField;
    adoqryRolesF_ORGANISATION_ID: TIntegerField;
    adoqryRolesF_NAME: TStringField;
    adoqryRolesF_ROLEGROUP_ID: TIntegerField;
    adoqryRolesF_ROLEGROUP_NAME: TStringField;
    adoqryRolesF_START_DATE: TDateTimeField;
    adoqryRolesF_END_DATE: TDateTimeField;
    adoqryRolesF_STREET: TStringField;
    adoqryRolesF_NUMBER: TStringField;
    adoqryRolesF_MAILBOX: TStringField;
    adoqryRolesF_POSTALCODE: TStringField;
    adoqryRolesF_POSTALCODE_ID: TIntegerField;
    adoqryRolesF_CITY_NAME: TStringField;
    adoqryRolesF_COUNTRY_ID: TIntegerField;
    adoqryRolesF_COUNTRY_NAME: TStringField;
    adoqryRolesF_GENDER_ID: TIntegerField;
    adoqryRolesF_GENDER_NAME: TStringField;
    adoqryRolesF_DATEBIRTH: TDateTimeField;
    adoqryOrganisationF_ACTIVE: TBooleanField;
    adoqryOrganisationF_MANUALLY_ADDED: TBooleanField;
    adop_SES_WIZARD_ENROL: TFVBFFCStoredProc;
    adoP_SES_WIZARD_REMOVE_ENROLMENT: TFVBFFCStoredProc;
    adoqryLookupPerson: TFVBFFCQuery;
    prvLookupPerson: TFVBFFCDataSetProvider;
    adoqryLookupPersonF_PERSON_ID: TIntegerField;
    adoqryLookupPersonF_SOCSEC_NR: TStringField;
    adoqryLookupPersonF_LASTNAME: TStringField;
    adoqryLookupPersonF_FIRSTNAME: TStringField;
    adoqryLookupPersonF_PERSON_ADDRESS: TStringField;
    adoqryLookupPersonF_ROLE_ID: TIntegerField;
    adoqryLookupPersonF_ORGANISATION_NAME: TStringField;
    adoqryLookupPersonF_ROLEGROUP_NAME: TStringField;
    adoqryLookupPersonF_DATEBIRTH: TDateTimeField;
    adoqryLookupPersonF_GENDER_NAME: TStringField;
    adoqryLookupPersonF_GENDER_ID: TIntegerField;
    adoqryLookupPersonF_ROLEGROUP_ID: TIntegerField;
    adoqryLookupPersonF_STREET: TStringField;
    adoqryLookupPersonF_NUMBER: TStringField;
    adoqryLookupPersonF_MAILBOX: TStringField;
    adoqryLookupPersonF_POSTALCODE_ID: TIntegerField;
    adoqryLookupPersonF_POSTALCODE: TStringField;
    adoqryLookupPersonF_CITY_NAME: TStringField;
    adoqryLookupPersonF_COUNTRY_NAME: TStringField;
    adoqryLookupPersonF_COUNTRY_ID: TIntegerField;
    adoqryListF_CONSTRUCT: TBooleanField;
    adoqryRolesF_DIPLOMA_ID: TIntegerField;
    adoqryRolesF_DIPLOMA_NAME: TStringField;
    adoqryLookupPersonF_DIPLOMA_ID: TIntegerField;
    adoqryLookupPersonF_DIPLOMA_NAME: TStringField;
//GCXE    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet; var TableName: String);
    procedure prvOrganisationGetTableName(Sender: TObject;
      DataSet: TDataSet; var TableName: String);
    procedure prvRolesGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure prvLookupPersonGetTableName(Sender: TObject;
      DataSet: TDataSet; var TableName: String);
  private
    { Private declarations }
  protected
    function GetNewOrganisationID: SYSINT; safecall;
    function GetNewRoleID: SYSINT; safecall;
    function EnrolPerson(ASessionID: Integer; ARoleID: Integer; ARolegroupID: Integer): SYSINT; safecall;
    procedure RemoveEnrolment(AEnrolID: Integer); safecall;
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    { Public declarations }
  end;

{var
  rdtm_SessionWizard: Trdtm_SessionWizard;
  }
var
  ofSessionWizard:TComponentFactory;

implementation

{$R *.dfm}

uses DataBkr, ComServ, ComObj;


{ Trdtm_SessionWizard }

class procedure Trdtm_SessionWizard.UpdateRegistry(Register: Boolean;
  const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

{GCXE
procedure Trdtm_SessionWizard.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  //inherited;
  TableName := 'V_SES_WIZARD_ENROL';

end;
}
procedure Trdtm_SessionWizard.prvOrganisationGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
    TableName := 'V_SES_WIZARD_ORGANISATION';

end;

procedure Trdtm_SessionWizard.prvRolesGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
    TableName := 'V_SES_WIZARD_ROLE';

end;

function Trdtm_SessionWizard.GetNewOrganisationID: SYSINT;
begin
  Result := self.FrdtmEDUMain.GetNewRecordID('T_OR_ORGANISATION');
end;

function Trdtm_SessionWizard.GetNewRoleID: SYSINT;
begin
  Result := self.FrdtmEDUMain.GetNewRecordID('T_RO_ROLE');
end;

function Trdtm_SessionWizard.EnrolPerson(ASessionID,
  ARoleID: Integer; ARolegroupID: Integer): SYSINT;
begin
  adop_SES_WIZARD_ENROL.Parameters.ParamByName('@ASessionId').Value := ASessionID;
  adop_SES_WIZARD_ENROL.Parameters.ParamByName('@ARoleID').Value := ARoleID;
  adop_SES_WIZARD_ENROL.Parameters.ParamByName('@ARolegroupID').Value := ARolegroupID;
  adop_SES_WIZARD_ENROL.ExecProc;

  Result := adop_SES_WIZARD_ENROL.Parameters.ParamByName('@APersonAlreadyEnrolled').Value;

end;

procedure Trdtm_SessionWizard.RemoveEnrolment(AEnrolID: Integer);
begin
  adoP_SES_WIZARD_REMOVE_ENROLMENT.Parameters.ParamByName('@AEnrolID').Value := AEnrolID;
  adoP_SES_WIZARD_REMOVE_ENROLMENT.ExecProc;
  
end;

procedure Trdtm_SessionWizard.prvLookupPersonGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
    //TableName := 'V_SES_WIZARD_LOOKUP_PERSON';
end;

initialization
  ofSessionWizard := TComponentFactory.Create(ComServer, Trdtm_SessionWizard,
    CLASS_rdtmSessionWizard, ciInternal, tmApartment);


end.
