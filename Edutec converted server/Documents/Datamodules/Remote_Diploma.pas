{*****************************************************************************
  This unit contains the Remote DataModule used for the maintenace of
  T_DOC_DIPLOMA
  
  @Name       Remote_Diploma
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  08/09/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Remote_Diploma;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, ComServ, ComObj, VCLCom, DataBkr,
  DBClient, EdutecServerD10_TLB, StdVcl, ActiveX, Remote_EduBase, Provider,
  Unit_FVBFFCDBComponents, DB, ADODB;

type
  TrdtmDiploma = class(TrdtmEduBase, IrdtmDiploma)
    adoqryListF_DIPLOMA_ID: TIntegerField;
    adoqryListF_NAME_NL: TStringField;
    adoqryListF_NAME_FR: TStringField;
    adoqryListF_DIPLOMA_NAME: TStringField;
    adoqryRecordF_DIPLOMA_ID: TIntegerField;
    adoqryRecordF_NAME_NL: TStringField;
    adoqryRecordF_NAME_FR: TStringField;
    adoqryRecordF_DIPLOMA_NAME: TStringField;
  private
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
    function ApplyDetailFilter(const aProvider,
      aFilter: WideString): Integer; //override; safecall;
    function ApplyFilter(const aProvider, aFilter: WideString): Integer;
      //override; safecall;
    function ApplyOrderBy(const aProvider, aOrderBy: WideString): Integer;
      //override; safecall;
    function GetNewRecordID: SYSINT; override; safecall;
    procedure Set_ADOConnection(Param1: Integer); override; safecall;
    procedure Set_rdtmEDUMain(const Param1: IrdtmEDUMain); override; safecall;
    function GetTableName : String; override;
    function SetSQLStatement(const aProvider, aSQL: WideString): Integer;
      //safecall;
    { Public declarations }
  end;

var
  ofDiploma: TComponentFactory;

implementation

{$R *.DFM}

class procedure TrdtmDiploma.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

function TrdtmDiploma.ApplyDetailFilter(const aProvider,
  aFilter: WideString): integer;
begin
  Result := Inherited ApplyDetailFilter( aProvider, aFilter );
end;

function TrdtmDiploma.ApplyFilter(const aProvider,
  aFilter: WideString): Integer;
begin
  Result := Inherited ApplyFilter( aProvider, aFilter );
end;

function TrdtmDiploma.ApplyOrderBy(const aProvider,
  aOrderBy: WideString): Integer;
begin
  Result := Inherited ApplyOrderBy( aProvider, aOrderBy );
end;

function TrdtmDiploma.GetNewRecordID: SYSINT;
begin
  Result := Inherited GetNewRecordID;
end;

procedure TrdtmDiploma.Set_ADOConnection(Param1: Integer);
begin
  Inherited Set_ADOConnection( Param1 );
end;

procedure TrdtmDiploma.Set_rdtmEDUMain(const Param1: IrdtmEDUMain);
begin
  Inherited Set_rdtmEDUMain( Param1 );
end;

function TrdtmDiploma.GetTableName: String;
begin
  Result := 'T_DOC_DIPLOMA';
end;

function TrdtmDiploma.SetSQLStatement(const aProvider,
  aSQL: WideString): Integer;
begin

end;

initialization
  ofDiploma := TComponentFactory.Create(ComServer, TrdtmDiploma,
    Class_rdtmDiploma, ciInternal, tmApartment);
    
end.
