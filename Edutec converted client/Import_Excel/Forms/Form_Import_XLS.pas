unit Form_Import_XLS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, DB, Unit_FVBFFCDBComponents, DBClient,
  Unit_PPWFrameWorkComponents, Provider, ADODB, MConnect, Grids, DBGrids,
  ComObj, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, Buttons, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator; //, DBTables;

type
  TFormImportXLS = class(TForm)
    srcMain: TFVBFFCDataSource;
    PanelBottom: TPanel;
    BtnImport: TButton;
    OpenDialog: TOpenDialog;
    BitBtnSelectAll: TBitBtn;
    BitBtnUnselectAll: TBitBtn;
    cxGridImportXLS: TcxGrid;
    cxGridImportXLSDBTableView1: TcxGridDBTableView;
    cxGridImportXLSDBTableView1F_VCA_IMPORT_ID: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_VCA_EXAM_NR: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_PERSON_LASTNAME: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_PERSON_FIRSTNAME: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_DATE_OF_BIRTH: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_PLACE_OF_BIRTH: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_MAILBOX: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_REEKS: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_SCORE: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_ORG_NAME: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_ADDRESS: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_POSTAL_CODE: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_COUNTRY: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_CITY: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_EXTRA: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_INSERT_DATE: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_UPDATE_DATE: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_FLAG: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_COMMENT: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_PERSON_ID: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_ORGANISATION_ID: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_PROGRAM_ID: TcxGridDBColumn;
    cxGridImportXLSDBTableView1F_SESSION_ID: TcxGridDBColumn;
    cxGridImportXLSLevel1: TcxGridLevel;
    QueryCheckAlreadyImported: TFVBFFCQuery;
    cxGridImportXLSDBTableView1F_FUNCTIE: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure BtnImportClick(Sender: TObject);
    procedure BitBtnSelectAllClick(Sender: TObject);
    procedure BitBtnUnselectAllClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormImportXLS: TFormImportXLS ;

implementation

uses Data_Import_XLS, Data_EduMainClient, unit_EdutecInterfaces;

{$R *.dfm}

procedure TFormImportXLS.FormShow(Sender: TObject);
var
  Cols: integer;
  Rows: integer;
  Excel, XLSheet: Variant;
  ColumnName : TStringList;
  Index : Integer;
  BlnAlreadyImported : Boolean;
begin
  inherited;
  OpenDialog.InitialDir := GetCurrentDir;
  if OpenDialog.Execute then
  begin
    dtmImport_XLS.cdsXLS.Close;
    dtmImport_XLS.cdsXLS.Open;
    Excel:=CreateOleObject('Excel.Application');
    Excel.Visible := True;
    Excel.workbooks.open(OpenDialog.FileName);
    XLSheet := Excel.worksheets[1];
    Cols := XLSheet.UsedRange.Columns.Count;
    Rows := XLSheet.UsedRange.Rows.Count;
    ColumnName := TStringList.Create;
    for Index := 1 to Cols do
    begin
      ColumnName.Add(Excel.Cells[1,Index].Value);
    end;
    Cols := XLSheet.UsedRange.Columns.Count;
    while Rows > 1 do
    begin
      dtmImport_XLS.cdsXLS.Insert;
      while Cols > 0 do
      begin
        if ColumnName[Cols-1] = 'Type' then
          dtmImport_XLS.cdsXLS.FieldByName('F_VCA_EXAM_NR').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Voornaam' then
          dtmImport_XLS.cdsXLS.FieldByName('F_PERSON_FIRSTNAME').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Naam' then
          dtmImport_XLS.cdsXLS.FieldByName('F_PERSON_LASTNAME').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Geboortedatum' then
          dtmImport_XLS.cdsXLS.FieldByName('F_DATE_OF_BIRTH').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Geboorteplaats' then
          dtmImport_XLS.cdsXLS.FieldByName('F_PLACE_OF_BIRTH').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Reeks' then
          dtmImport_XLS.cdsXLS.FieldByName('F_REEKS').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'E-mail' then
          dtmImport_XLS.cdsXLS.FieldByName('F_MAILBOX').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Adres' then
          dtmImport_XLS.cdsXLS.FieldByName('F_ADDRESS').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Postcode' then
          dtmImport_XLS.cdsXLS.FieldByName('F_POSTAL_CODE').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Stad' then
          dtmImport_XLS.cdsXLS.FieldByName('F_CITY').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Land' then
          dtmImport_XLS.cdsXLS.FieldByName('F_COUNTRY').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Firma' then
          dtmImport_XLS.cdsXLS.FieldByName('F_ORG_NAME').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Extra' then
          dtmImport_XLS.cdsXLS.FieldByName('F_EXTRA').AsString := Excel.Cells[Rows,Cols].Value
        else if ColumnName[Cols-1] = 'Functie' then
          dtmImport_XLS.cdsXLS.FieldByName('F_FUNCTIE').AsString := Excel.Cells[Rows,Cols].Value;
        dec(Cols);
      end;
      dtmImport_XLS.cdsXLS.FieldByName('F_INSERT_DATE').AsDateTime := date;
      dtmImport_XLS.cdsXLS.FieldByName('F_UPDATE_DATE').AsDateTime := date;
      dtmImport_XLS.cdsXLS.FieldByName('F_FLAG').AsString := 'I';
      Cols := XLSheet.UsedRange.Columns.Count;
    dec(Rows);
    end;
    Excel.workbooks.close;
    Excel.Quit;
    Excel:=Unassigned;
    //remove already imported lines
    BlnAlreadyImported := False;
    dtmImport_XLS.cdsXLS.First;
    while not dtmImport_XLS.cdsXLS.Eof do
    begin
      QueryCheckAlreadyImported.Close;
      QueryCheckAlreadyImported.Parameters.ParamByName('EXAM_NR').Value := dtmImport_XLS.cdsXLS.FieldByName('F_VCA_EXAM_NR').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('LASTNAME').Value := dtmImport_XLS.cdsXLS.FieldByName('F_PERSON_LASTNAME').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('FIRSTNAME').Value := dtmImport_XLS.cdsXLS.FieldByName('F_PERSON_FIRSTNAME').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('BIRTHDATE').Value := dtmImport_XLS.cdsXLS.FieldByName('F_DATE_OF_BIRTH').AsDateTime;
      QueryCheckAlreadyImported.Parameters.ParamByName('BIRTHPLACE').Value := dtmImport_XLS.cdsXLS.FieldByName('F_PLACE_OF_BIRTH').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('MAILBOX').Value := dtmImport_XLS.cdsXLS.FieldByName('F_MAILBOX').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('ORGNAME').Value := dtmImport_XLS.cdsXLS.FieldByName('F_ORG_NAME').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('ADDRESS').Value := dtmImport_XLS.cdsXLS.FieldByName('F_ADDRESS').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('POSTALCODE').Value := dtmImport_XLS.cdsXLS.FieldByName('F_POSTAL_CODE').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('COUNTRY').Value := dtmImport_XLS.cdsXLS.FieldByName('F_COUNTRY').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('CITY').Value := dtmImport_XLS.cdsXLS.FieldByName('F_CITY').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('EXTRA').Value := dtmImport_XLS.cdsXLS.FieldByName('F_EXTRA').AsString;
      QueryCheckAlreadyImported.Parameters.ParamByName('FUNCTIE').Value := dtmImport_XLS.cdsXLS.FieldByName('F_FUNCTIE').AsString;
      QueryCheckAlreadyImported.Open;
      if not QueryCheckAlreadyImported.IsEmpty then
      begin
        dtmImport_XLS.cdsXLS.Delete;
        BlnAlreadyImported := True;
      end
      else
        dtmImport_XLS.cdsXLS.Next;
    end;
    if BlnAlreadyImported then
      ShowMessage('Reeds geimporteerde lijen verwijderd');
  end;
  if Assigned(dtmImport_XLS.cdsXLS) and (dtmImport_XLS.cdsXLS.IsEmpty) then
    PanelBottom.Enabled := False
  else
    PanelBottom.Enabled := True;
end;

procedure TFormImportXLS.BtnImportClick(Sender: TObject);
var
  i, j : integer;
  rowindex : integer;
  rowrecordindex : integer;
  blnselected : boolean;
  SLRowsToDelete : TStringList;
begin
  //delete not selected lines in CDS to allow easy update
  SLRowsToDelete := TStringList.Create;
  for i := cxGridImportXLSDBTableView1.DataController.GetRowCount -1 downto 0 do
  begin
    rowrecordindex := cxGridImportXLSDBTableView1.DataController.GetRowId(i);
    blnselected := false;
    for j := 0 to cxGridImportXLSDBTableView1.Controller.SelectedRowCount -1 do
    begin
      rowindex := cxGridImportXLSDBTableView1.Controller.SelectedRecords[j].RecordIndex;
      if rowindex = rowrecordindex then
        blnselected := true;
    end;
    if not blnselected then
    begin
      SLRowsToDelete.Add(IntToStr(rowrecordindex));
    end;
  end;

  //commit to database
  dtmImport_XLS.DeleteAndCommit(SLRowsToDelete);
  Self.Close;
end;

procedure TFormImportXLS.BitBtnSelectAllClick(Sender: TObject);
begin
  cxGridImportXLSDBTableView1.BeginUpdate();
  cxGridImportXLSDBTableView1.Controller.SelectAll;
  cxGridImportXLSDBTableView1.EndUpdate();
end;

procedure TFormImportXLS.BitBtnUnselectAllClick(Sender: TObject);
begin
  cxGridImportXLSDBTableView1.BeginUpdate();
  cxGridImportXLSDBTableView1.Controller.ClearSelection;
  cxGridImportXLSDBTableView1.EndUpdate();
end;

end.
