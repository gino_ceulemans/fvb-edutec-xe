object FormImportXLS: TFormImportXLS
  Left = 382
  Top = 180
  Caption = 'Import XLS'
  ClientHeight = 711
  ClientWidth = 1289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBottom: TPanel
    Left = 0
    Top = 668
    Width = 1289
    Height = 43
    Align = alBottom
    TabOrder = 0
    object BtnImport: TButton
      Left = 240
      Top = 10
      Width = 200
      Height = 25
      Caption = 'Import Selected Lines'
      TabOrder = 0
      OnClick = BtnImportClick
    end
    object BitBtnSelectAll: TBitBtn
      Left = 16
      Top = 10
      Width = 105
      Height = 25
      Caption = 'Selecteer alles'
      TabOrder = 1
      OnClick = BitBtnSelectAllClick
    end
    object BitBtnUnselectAll: TBitBtn
      Left = 128
      Top = 10
      Width = 105
      Height = 25
      Caption = 'Deselecteer alles'
      TabOrder = 2
      OnClick = BitBtnUnselectAllClick
    end
  end
  object cxGridImportXLS: TcxGrid
    Left = 0
    Top = 0
    Width = 1289
    Height = 668
    Align = alClient
    TabOrder = 1
    object cxGridImportXLSDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = srcMain
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      object cxGridImportXLSDBTableView1F_VCA_IMPORT_ID: TcxGridDBColumn
        Caption = 'VCA Import ID'
        DataBinding.FieldName = 'F_VCA_IMPORT_ID'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_VCA_EXAM_NR: TcxGridDBColumn
        Caption = 'VCA Examen nummer'
        DataBinding.FieldName = 'F_VCA_EXAM_NR'
      end
      object cxGridImportXLSDBTableView1F_PERSON_LASTNAME: TcxGridDBColumn
        Caption = 'Naam'
        DataBinding.FieldName = 'F_PERSON_LASTNAME'
      end
      object cxGridImportXLSDBTableView1F_PERSON_FIRSTNAME: TcxGridDBColumn
        Caption = 'Voornaam'
        DataBinding.FieldName = 'F_PERSON_FIRSTNAME'
      end
      object cxGridImportXLSDBTableView1F_DATE_OF_BIRTH: TcxGridDBColumn
        Caption = 'Geboortedatum'
        DataBinding.FieldName = 'F_DATE_OF_BIRTH'
      end
      object cxGridImportXLSDBTableView1F_PLACE_OF_BIRTH: TcxGridDBColumn
        Caption = 'Geboorteplaats'
        DataBinding.FieldName = 'F_PLACE_OF_BIRTH'
      end
      object cxGridImportXLSDBTableView1F_MAILBOX: TcxGridDBColumn
        Caption = 'E-mail'
        DataBinding.FieldName = 'F_MAILBOX'
      end
      object cxGridImportXLSDBTableView1F_REEKS: TcxGridDBColumn
        Caption = 'Reeks'
        DataBinding.FieldName = 'F_REEKS'
      end
      object cxGridImportXLSDBTableView1F_SCORE: TcxGridDBColumn
        DataBinding.FieldName = 'F_SCORE'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_ORG_NAME: TcxGridDBColumn
        Caption = 'Firma'
        DataBinding.FieldName = 'F_ORG_NAME'
      end
      object cxGridImportXLSDBTableView1F_ADDRESS: TcxGridDBColumn
        Caption = 'Adres'
        DataBinding.FieldName = 'F_ADDRESS'
      end
      object cxGridImportXLSDBTableView1F_POSTAL_CODE: TcxGridDBColumn
        Caption = 'Postcode'
        DataBinding.FieldName = 'F_POSTAL_CODE'
      end
      object cxGridImportXLSDBTableView1F_COUNTRY: TcxGridDBColumn
        Caption = 'Land'
        DataBinding.FieldName = 'F_COUNTRY'
      end
      object cxGridImportXLSDBTableView1F_CITY: TcxGridDBColumn
        Caption = 'Stad'
        DataBinding.FieldName = 'F_CITY'
      end
      object cxGridImportXLSDBTableView1F_EXTRA: TcxGridDBColumn
        Caption = 'Extra'
        DataBinding.FieldName = 'F_EXTRA'
      end
      object cxGridImportXLSDBTableView1F_FUNCTIE: TcxGridDBColumn
        Caption = 'Functie'
        DataBinding.FieldName = 'F_FUNCTIE'
        Width = 200
      end
      object cxGridImportXLSDBTableView1F_INSERT_DATE: TcxGridDBColumn
        DataBinding.FieldName = 'F_INSERT_DATE'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_UPDATE_DATE: TcxGridDBColumn
        DataBinding.FieldName = 'F_UPDATE_DATE'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_FLAG: TcxGridDBColumn
        DataBinding.FieldName = 'F_FLAG'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_COMMENT: TcxGridDBColumn
        DataBinding.FieldName = 'F_COMMENT'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_PERSON_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_PERSON_ID'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_ORGANISATION_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_ORGANISATION_ID'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_PROGRAM_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_PROGRAM_ID'
        Visible = False
      end
      object cxGridImportXLSDBTableView1F_SESSION_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_SESSION_ID'
        Visible = False
      end
    end
    object cxGridImportXLSLevel1: TcxGridLevel
      GridView = cxGridImportXLSDBTableView1
    end
  end
  object srcMain: TFVBFFCDataSource
    AutoEdit = False
    DataSet = dtmImport_XLS.cdsXLS
    Left = 24
    Top = 72
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '*.XLS'
    Left = 88
    Top = 72
  end
  object QueryCheckAlreadyImported: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    Parameters = <
      item
        Name = 'EXAM_NR'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'LASTNAME'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'FIRSTNAME'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'BIRTHDATE'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'BIRTHPLACE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'MAILBOX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'ORGNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'ADDRESS'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 128
        Value = Null
      end
      item
        Name = 'POSTALCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'COUNTRY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'CITY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 64
        Value = Null
      end
      item
        Name = 'EXTRA'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 128
        Value = Null
      end
      item
        Name = 'FUNCTIE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'select * from T_XLS_VCA_IMPORT '
      'where F_VCA_EXAM_NR = :EXAM_NR'
      'and F_PERSON_LASTNAME = :LASTNAME'
      'and F_PERSON_FIRSTNAME = :FIRSTNAME'
      'and F_DATE_OF_BIRTH = :BIRTHDATE'
      'and F_PLACE_OF_BIRTH = :BIRTHPLACE'
      'and F_MAILBOX = :MAILBOX'
      'and F_ORG_NAME = :ORGNAME'
      'and F_ADDRESS = :ADDRESS'
      'and F_POSTAL_CODE = :POSTALCODE'
      'and F_COUNTRY = :COUNTRY'
      'and F_CITY = :CITY'
      'and F_EXTRA = :EXTRA'
      'and F_FUNCTIE = :FUNCTIE')
    AutoOpen = False
    Left = 296
    Top = 104
  end
end
