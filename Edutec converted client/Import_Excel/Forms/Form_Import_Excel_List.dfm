inherited frmImport_Excel_List: TfrmImport_Excel_List
  Left = 722
  Top = 180
  Width = 833
  Height = 639
  Caption = 'Import_Excel'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 571
    Width = 825
    DesignSize = (
      825
      41)
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 609
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 721
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 817
    Height = 563
    inherited cxgrdList: TcxGrid
      Top = 199
      Width = 817
      Height = 321
      PopupMenu = FVBFFCBaseListView.dxbpmnGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = True
        OptionsData.Deleting = True
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_VCA_IMPORT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_VCA_IMPORT_ID'
          Visible = False
        end
        object cxgrdtblvListF_VCA_EXAM_NR: TcxGridDBColumn
          Caption = 'VCA Examennummer'
          DataBinding.FieldName = 'F_VCA_EXAM_NR'
          Width = 113
        end
        object cxgrdtblvListF_PERSON_LASTNAME: TcxGridDBColumn
          Caption = 'Naam'
          DataBinding.FieldName = 'F_PERSON_LASTNAME'
        end
        object cxgrdtblvListF_PERSON_FIRSTNAME: TcxGridDBColumn
          Caption = 'Voornaam'
          DataBinding.FieldName = 'F_PERSON_FIRSTNAME'
          Width = 77
        end
        object cxgrdtblvListF_DATE_OF_BIRTH: TcxGridDBColumn
          Caption = 'Geboortedatum'
          DataBinding.FieldName = 'F_DATE_OF_BIRTH'
        end
        object cxgrdtblvListF_PLACE_OF_BIRTH: TcxGridDBColumn
          Caption = 'Geboorteplaats'
          DataBinding.FieldName = 'F_PLACE_OF_BIRTH'
        end
        object cxgrdtblvListF_MAILBOX: TcxGridDBColumn
          Caption = 'E-mail'
          DataBinding.FieldName = 'F_MAILBOX'
        end
        object cxgrdtblvListF_REEKS: TcxGridDBColumn
          Caption = 'Reeks'
          DataBinding.FieldName = 'F_REEKS'
        end
        object cxgrdtblvListF_SCORE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SCORE'
          Visible = False
        end
        object cxgrdtblvListF_ORG_NAME: TcxGridDBColumn
          Caption = 'Firma'
          DataBinding.FieldName = 'F_ORG_NAME'
        end
        object cxgrdtblvListF_ADDRESS: TcxGridDBColumn
          Caption = 'Adres'
          DataBinding.FieldName = 'F_ADDRESS'
        end
        object cxgrdtblvListF_POSTAL_CODE: TcxGridDBColumn
          Caption = 'Postcode'
          DataBinding.FieldName = 'F_POSTAL_CODE'
        end
        object cxgrdtblvListF_COUNTRY: TcxGridDBColumn
          Caption = 'Land'
          DataBinding.FieldName = 'F_COUNTRY'
        end
        object cxgrdtblvListF_CITY: TcxGridDBColumn
          Caption = 'Stad'
          DataBinding.FieldName = 'F_CITY'
        end
        object cxgrdtblvListF_EXTRA: TcxGridDBColumn
          Caption = 'Extra'
          DataBinding.FieldName = 'F_EXTRA'
        end
        object cxgrdtblvListF_INSERT_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_INSERT_DATE'
          Visible = False
        end
        object cxgrdtblvListF_UPDATE_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_UPDATE_DATE'
          Visible = False
        end
        object cxgrdtblvListF_FLAG: TcxGridDBColumn
          DataBinding.FieldName = 'F_FLAG'
          Visible = False
        end
        object cxgrdtblvListF_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_COMMENT'
          Visible = False
        end
        object cxgrdtblvListF_PERSON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_ID'
          Visible = False
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Visible = False
        end
        object cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_ID'
          Visible = False
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_NAME'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_CODE'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_START_DATE'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_END_DATE'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 165
      Width = 817
      Height = 30
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 195
      Width = 817
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 817
      Height = 165
      ExpandedHeight = 165
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 132
        Width = 815
        Height = 32
        DesignSize = (
          815
          32)
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 647
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 735
        end
      end
      object cxlblF_NAME2: TFVBFFCLabel
        Left = 256
        Top = 34
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Voornaam'
        FocusControl = cxdbteF_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME1: TFVBFFCDBTextEdit
        Left = 328
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_PERSON_FIRSTNAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 2
        Width = 137
      end
      object FVBFFCLabel2: TFVBFFCLabel
        Left = 8
        Top = 34
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam persoon'
        FocusControl = FVBFFCDBTextEdit1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
        Left = 112
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_PERSON_LASTNAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 4
        Width = 137
      end
      object FVBFFCLabel3: TFVBFFCLabel
        Left = 8
        Top = 58
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Bedrijf'
        FocusControl = FVBFFCDBTextEdit2
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object FVBFFCDBTextEdit2: TFVBFFCDBTextEdit
        Left = 112
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_ORG_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 6
        Width = 137
      end
    end
    object PanelImportXLS: TPanel
      Left = 0
      Top = 520
      Width = 817
      Height = 43
      Align = alBottom
      TabOrder = 4
      object ButtonImportXLS: TFVBFFCButton
        Left = 8
        Top = 8
        Width = 120
        Height = 25
        Caption = 'Import XLS'
        TabOrder = 0
        OnClick = ButtonImportXLSClick
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 825
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 567
    Width = 825
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 821
    Height = 563
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 563
  end
  inherited srcMain: TFVBFFCDataSource
    AutoEdit = False
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxbbDelete: TdxBarButton
      ShortCut = 16430
    end
    object dxBarButton1: TdxBarButton
      Action = acLinkToSession
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = acMarkAsProcessed
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = acAutCreate
      Category = 0
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton1
        Visible = True
      end
      item
        Item = dxBarButton4
        Visible = True
      end
      item
        Item = dxBarButton2
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
    OnPopup = dxbpmnGridPopup
  end
  object alImportXLS: TFVBFFCActionList
    Left = 276
    Top = 388
    object acLinkToSession: TAction
      Caption = 'Schrijf in op sessie'
      OnExecute = acLinkToSessionExecute
    end
    object acLinkPersonConstruct2Edgard: TAction
      Caption = 'Koppel Construct Persoon aan Edgard Persoon'
    end
    object acMarkAsProcessed: TAction
      Caption = 'Als verwerkt markeren'
      OnExecute = acMarkAsProcessedExecute
    end
    object acAutCreate: TAction
      Caption = 'Aut creatie pers/org'
      OnExecute = acAutCreateExecute
    end
  end
  object srcDefaultSearchCriteria: TFVBFFCDataSource
    Left = 584
    Top = 38
  end
end
