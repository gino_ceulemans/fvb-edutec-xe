unit Form_Import_Excel_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  ActnList, Unit_FVBFFCComponents, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxDBEdit, cxContainer, cxLabel, Unit_PPWFrameWorkInterfaces,
  Grids;

resourcestring
  rsConfirmDeletionImportRecords='Bent u zeker dat u de gekozen Import Records wilt verwijderen?';
  rsDifferentSessionsSelected='U heeft verschillende soorten sessies geselecteerd of dezelfde soort sessies maar met een verschillende startdatum.';
  rsConfirmMarkAsProcessed='U heeft gekozen om personen te markeren als zijnde verwerkt.  Bent u zeker dat u dit wilt doen?';
  rsConfirmCreate='U heeft gekozen om personen te creeren.  Bent u zeker dat u dit wilt doen?';

type
  TfrmImport_Excel_List = class(TEDUListView, IIPWFrameWorkFormDeleteMultiSelect)
    alImportXLS: TFVBFFCActionList;
    acLinkToSession: TAction;
    dxBarButton1: TdxBarButton;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME1: TFVBFFCDBTextEdit;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    FVBFFCLabel3: TFVBFFCLabel;
    FVBFFCDBTextEdit2: TFVBFFCDBTextEdit;
    srcDefaultSearchCriteria: TFVBFFCDataSource;
    
    dxBarButton2: TdxBarButton;
    acMarkAsProcessed: TAction;
    cxgrdtblvListF_VCA_IMPORT_ID: TcxGridDBColumn;
    cxgrdtblvListF_VCA_EXAM_NR: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_DATE_OF_BIRTH: TcxGridDBColumn;
    cxgrdtblvListF_PLACE_OF_BIRTH: TcxGridDBColumn;
    cxgrdtblvListF_MAILBOX: TcxGridDBColumn;
    cxgrdtblvListF_REEKS: TcxGridDBColumn;
    cxgrdtblvListF_SCORE: TcxGridDBColumn;
    cxgrdtblvListF_ORG_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ADDRESS: TcxGridDBColumn;
    cxgrdtblvListF_POSTAL_CODE: TcxGridDBColumn;
    cxgrdtblvListF_COUNTRY: TcxGridDBColumn;
    cxgrdtblvListF_CITY: TcxGridDBColumn;
    cxgrdtblvListF_EXTRA: TcxGridDBColumn;
    cxgrdtblvListF_INSERT_DATE: TcxGridDBColumn;
    cxgrdtblvListF_UPDATE_DATE: TcxGridDBColumn;
    cxgrdtblvListF_FLAG: TcxGridDBColumn;
    cxgrdtblvListF_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn;
    PanelImportXLS: TPanel;
    ButtonImportXLS: TFVBFFCButton;
    acAutCreate: TAction;
    dxBarButton4: TdxBarButton;
    procedure ButtonImportXLSClick(Sender: TObject);
    procedure acLinkToSessionExecute(Sender: TObject);
    procedure acMarkAsProcessedExecute(Sender: TObject);
    procedure FVBFFCListViewShow(Sender: TObject);
    procedure acAutCreateExecute(Sender: TObject);
    procedure dxbpmnGridPopup(Sender: TObject);
    procedure cxgrdtblvListCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);

  private
    FSessionID: Integer;
    FMaxParticipants: integer;
    FNbrParticipants: integer;
    procedure SetSessionId;
    procedure DeleteRecord;
    procedure AutoCreate;
    procedure SetProcessed;
    procedure DeleteSelectedRecords;

  public
    function GetFilterString : String; override;
  end;

implementation

uses Data_Import_Excel, Form_Import_XLS, 
  Data_EduMainClient, cxGridDBDataDefinitions, unit_EdutecInterfaces, DBClient;


{$R *.dfm}

procedure TfrmImport_Excel_List.acLinkToSessionExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
  aDataModule     : IEDUImport_ExcelDataModule;
  aDataSet        : TClientDataSet;
  lcv             : Integer;
  aDefaultSearchCriteria : TClientDataSet;
  aSession        : String;
  aStartSessionDate : TDateTime;
begin
  if ( Supports( FrameWorkDataModule, IEDUImport_ExcelDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aDataSet := TClientDataSet.Create( Self );

    try
      aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
      aDataController := TcxGridDBDataController( ActiveGridView.DataController );

      if(aViewController.SelectedRecordCount <> 0) then
      begin
          // Initialize default search criteria client dataset
          aDefaultSearchCriteria := aDataModule.DefaultSearchCriteria; // Turn search criteria on by default
          try
              // Check for different session names and start dates
              { Loop over each selected record in the grid }
              for lcv := 0 to Pred( aViewController.SelectedRecordCount ) do
              begin
                srcMain.DataSet.Bookmark := aDataController.GetSelectedBookmark( lcv );

                if lcv=0 then
                begin
                  aSession := srcMain.DataSet.FieldByName('F_VCA_EXAM_NR').AsString;
                  aStartSessionDate := srcMain.DataSet.FieldByName('F_SESSION_START_DATE').AsDateTime;
                end else begin
                  if (aSession <> srcMain.DataSet.FieldByName('F_VCA_EXAM_NR').AsString)
                      or (aStartSessionDate <> srcMain.DataSet.FieldByName('F_SESSION_START_DATE').AsDateTime)
                  then
                  begin
                    // Since different import record sessions have been selected, please don't filter
                    aDefaultSearchCriteria := nil;
                    MessageDlg(rsDifferentSessionsSelected, mtWarning, [mbOk], 0);
                    Refresh;
                    break;
                  end;
                end;
              end;

              // Allow the user to select a session id and filter records temporary when aDefaultSearchCriteria <> nil
              //if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesSession', 'F_STATUS_ID IN ( 1, 4, 6, 7, 8, 9, 10, 11 )'
              if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesSession', 'F_STATUS_ID IN ( 1, 4, 6, 7, 8, 9, 11 )'
                    , aDataSet, aDefaultSearchCriteria, False ) = mrOk ) then
              begin
                // Assign selected session id to selected import records
                FSessionID := aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
                FMaxParticipants := aDataSet.FieldByName( 'F_MAXIMUM' ).AsInteger;
                FNbrParticipants := aDataSet.FieldByName( 'F_NO_PARTICIPANTS' ).AsInteger;
                LoopThroughSelectedRecords(SetSessionId);
                aDataModule.ProcessValidateKeys;
                RefreshData;
                cxgrdtblvList.DataController.ClearSelection;
              end;

          finally
            // Close Default search criteria clientdataset
            aDataModule.CloseDefSearchCriteriaDS;
          end;
      end;
    finally
      FreeAndNil( aDataSet );
    end;
  end;
  cxgrdtblvList.DataController.DataSource.DataSet.Refresh;
end;

procedure TfrmImport_Excel_List.DeleteRecord;
var
    oldTag: Integer;
begin
    assert (self.srcMain.DataSet <> nil);

    oldTag := srcMain.Dataset.Tag;
    try
      srcMain.DataSet.Tag := 1;
      srcMain.DataSet.Delete;
    finally
      srcMain.Dataset.Tag := oldTag;
    end;
end;

procedure TfrmImport_Excel_List.DeleteSelectedRecords;
var
  oldcursor: TCursor;
begin
  if MessageDlg(rsConfirmDeletionImportRecords, mtConfirmation, [mbYes,mbNo], 0)=mrYes then
  begin
    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;
      LoopThroughSelectedRecords(DeleteRecord);
      cxgrdtblvList.DataController.ClearSelection;
    finally
      Screen.Cursor := oldcursor;
    end;
  end;
end;

function TfrmImport_Excel_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the Condition for the Person Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PERSON_LASTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_PERSON_LASTNAME', FSearchCriteriaDataSet.FieldByName( 'F_PERSON_LASTNAME' ).AsString );
  end;

  { Add the Condition for the Person Firstname }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PERSON_FIRSTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_PERSON_FIRSTNAME', FSearchCriteriaDataSet.FieldByName( 'F_PERSON_FIRSTNAME' ).AsString );
  end;

  { Add the Condition for the Company Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ORG_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_ORG_NAME', FSearchCriteriaDataSet.FieldByName( 'F_ORG_NAME' ).AsString );
  end;

  Result := aFilterString;
end;

procedure TfrmImport_Excel_List.SetSessionId;
var
  aDataModule     : IEDUImport_ExcelDataModule;
begin
  // Assign selected session id to import record(s)
  if FMaxParticipants > FNbrParticipants then
  begin
    if ( Supports( FrameWorkDataModule, IEDUImport_ExcelDataModule, aDataModule ) ) and
       ( ActiveGridView.Controller is TcxCustomGridTableController ) and
       ( ActiveGridView.DataController is TcxGridDBDataController ) then
    begin
      aDataModule.XLSSetSession(srcMain.DataSet.FieldByName('F_VCA_IMPORT_ID').AsInteger,FSessionID);
    end;
    Inc(FNbrParticipants);
  end;
end;

procedure TfrmImport_Excel_List.ButtonImportXLSClick(Sender: TObject);
var
  frmImportXLS: TFormImportXLS;
begin
  inherited;
  frmImportXLS := TFormImportXLS.Create(nil);
  try
    frmImportXLS.ShowModal;
  finally
    freeAndNil(frmImportXLS);
  end;
  cxgrdtblvList.DataController.DataSource.DataSet.Refresh;
end;

procedure TfrmImport_Excel_List.acMarkAsProcessedExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataModule     : IEDUImport_ExcelDataModule;
  oldcursor: TCursor;
begin
  if ( Supports( FrameWorkDataModule, IEDUImport_ExcelDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
    if MessageDlg(rsConfirmMarkAsProcessed, mtConfirmation, [mbYes,mbNo], 0)=mrYes then
    begin
      oldcursor := Screen.Cursor;
      try
        Screen.Cursor := crSQLWait;
        LoopThroughSelectedRecords(SetProcessed);
        RefreshData;
        cxgrdtblvList.DataController.ClearSelection;
      finally
        Screen.Cursor := oldcursor;
      end;
    end;
  end;
end;

procedure TfrmImport_Excel_List.SetProcessed;
var
  aDataModule     : IEDUImport_ExcelDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUImport_ExcelDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aDataModule.XLSSetProcessed(srcMain.DataSet.FieldByName('F_VCA_IMPORT_ID').AsInteger);
  end;
end;

procedure TfrmImport_Excel_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;
  dxbbEdit.Visible := ivNever;
  dxbbView.Visible := ivNever;
  dxbbAdd.Visible := ivNever;
  dxbbDelete.Visible := ivNever;
end;

procedure TfrmImport_Excel_List.acAutCreateExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataModule     : IEDUImport_ExcelDataModule;
  oldcursor: TCursor;
begin
  if ( Supports( FrameWorkDataModule, IEDUImport_ExcelDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
    if MessageDlg(rsConfirmCreate, mtConfirmation, [mbYes,mbNo], 0)=mrYes then
    begin
      oldcursor := Screen.Cursor;
      try
        Screen.Cursor := crSQLWait;
        LoopThroughSelectedRecords(AutoCreate);
        aDataModule.ProcessValidateKeys;
        RefreshData;
        cxgrdtblvList.DataController.ClearSelection;
      finally
        Screen.Cursor := oldcursor;
      end;
    end;
  end;
end;

procedure TfrmImport_Excel_List.AutoCreate;
var
  aDataModule     : IEDUImport_ExcelDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUImport_ExcelDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aDataModule.XLSAutCreate(srcMain.DataSet.FieldByName('F_VCA_IMPORT_ID').AsInteger);
  end;
end;


procedure TfrmImport_Excel_List.dxbpmnGridPopup(Sender: TObject);
begin
  inherited;
  assert(srcMain.DataSet.Active);
end;

procedure TfrmImport_Excel_List.cxgrdtblvListCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  //no detail possible
  Exit;
end;

end.
