unit Data_Import_Excel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_EDUDataModule, DB, DBClient, MConnect, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Provider, ADODB, unit_EdutecInterfaces,
  Unit_PPWFrameWorkActions;

type
  TdtmImport_Excel = class(TEDUDataModule, IEDUImport_ExcelDataModule)
    cdsSearchCriteriaF_PERSON_FIRSTNAME: TStringField;
    cdsSearchCriteriaF_PERSON_LASTNAME: TStringField;
    cdsSearchCriteriaF_ORG_NAME: TStringField;
    cdsDefaultSearchCriteria: TFVBFFCClientDataSet;
    cdsDefaultSearchCriteriaF_CODE: TStringField;
    cdsListF_VCA_IMPORT_ID: TIntegerField;
    cdsListF_VCA_EXAM_NR: TStringField;
    cdsListF_PERSON_LASTNAME: TStringField;
    cdsListF_PERSON_FIRSTNAME: TStringField;
    cdsListF_DATE_OF_BIRTH: TDateTimeField;
    cdsListF_PLACE_OF_BIRTH: TStringField;
    cdsListF_MAILBOX: TStringField;
    cdsListF_REEKS: TStringField;
    cdsListF_SCORE: TStringField;
    cdsListF_ORG_NAME: TStringField;
    cdsListF_ADDRESS: TStringField;
    cdsListF_POSTAL_CODE: TStringField;
    cdsListF_COUNTRY: TStringField;
    cdsListF_CITY: TStringField;
    cdsListF_EXTRA: TStringField;
    cdsListF_INSERT_DATE: TDateTimeField;
    cdsListF_UPDATE_DATE: TDateTimeField;
    cdsListF_FLAG: TStringField;
    cdsListF_COMMENT: TStringField;
    cdsListF_PERSON_ID: TIntegerField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_PROGRAM_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_SESSION_NAME: TStringField;
    cdsListF_SESSION_CODE: TStringField;
    cdsListF_SESSION_START_DATE: TDateTimeField;
    cdsListF_SESSION_END_DATE: TDateTimeField;
    cdsListF_FUNCTIE: TStringField;
  private
    { Private declarations }
  protected
    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
    procedure XLSSetProcessed(AImportId: Integer);
    procedure XLSSetSession(AImportId, ASession: Integer);
    procedure XLSAutCreate(AImportId: Integer);
    procedure CloseDefSearchCriteriaDS;
    procedure ProcessValidateKeys;
    function GetDefaultSearchCriteria: TClientDataSet;

  public
    { Public declarations }
    property DefaultSearchCriteria: TClientDataSet read GetDefaultSearchCriteria;

  end;

var
  dtmImport_Excel: TdtmImport_Excel;

implementation

{$R *.dfm}

{ TdtmImport_Excel }

function TdtmImport_Excel.ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
begin
  Result := ssckData.AppServer.ProcessSingleRecord(EducationIdentity, SessionIdentity);
end;

procedure TdtmImport_Excel.XLSSetProcessed(AImportId: Integer);
begin
  ssckData.AppServer.XLSSetProcessed(AImportId);
end;

procedure TdtmImport_Excel.XLSSetSession(AImportId, ASession: Integer);
begin
  ssckData.AppServer.XLSSetSession(AImportId, ASession);
end;

procedure TdtmImport_Excel.XLSAutCreate(AImportId: Integer);
begin
  ssckData.AppServer.XLSAutCreate(AImportId);
end;

procedure TdtmImport_Excel.ProcessValidateKeys;
begin
  ssckData.AppServer.ProcessValidateKeys;
end;

procedure TdtmImport_Excel.CloseDefSearchCriteriaDS;
begin
  if cdsDefaultSearchCriteria.Active then
    cdsDefaultSearchCriteria.Close;
end;

function TdtmImport_Excel.GetDefaultSearchCriteria: TClientDataSet;
begin
  if not cdsDefaultSearchCriteria.Active then
  begin
    cdsDefaultSearchCriteria.CreateDataSet;
    cdsDefaultSearchCriteria.Append;
  end;

  if not (cdsDefaultSearchCriteria.State in [dsInsert, dsEdit]) then
    cdsDefaultSearchCriteria.Edit;

  cdsDefaultSearchCriteria.FieldByName('F_CODE').Value := cdsListF_VCA_EXAM_NR.AsString;

  cdsDefaultSearchCriteria.Post;

  Result := cdsDefaultSearchCriteria;
end;


end.
