object dtmImport_XLS: TdtmImport_XLS
  OldCreateOrder = False
  Left = 611
  Top = 251
  Height = 251
  Width = 501
  object ssckData: TFVBFFCSharedConnection
    ParentConnection = dtmEDUMainClient.sckcnnMain
    ChildName = 'rdtmImport_Excel'
    Left = 48
    Top = 32
  end
  object qryXLS: TFVBFFCQuery
    Connection = dtmFVBFFCMainClient.adocnnMain
    Parameters = <>
    SQL.Strings = (
      'SELECT F_VCA_IMPORT_ID'
      '      ,F_VCA_EXAM_NR'
      '      ,F_PERSON_LASTNAME'
      '      ,F_PERSON_FIRSTNAME'
      '      ,F_DATE_OF_BIRTH'
      '      ,F_PLACE_OF_BIRTH'
      '      ,F_MAILBOX'
      '      ,F_REEKS'
      '      ,F_SCORE'
      '      ,F_ORG_NAME'
      '      ,F_ADDRESS'
      '      ,F_POSTAL_CODE'
      '      ,F_COUNTRY'
      '      ,F_CITY'
      '      ,F_EXTRA'
      '      ,F_INSERT_DATE'
      '      ,F_UPDATE_DATE'
      '      ,F_FLAG'
      '      ,F_COMMENT'
      '      ,F_PERSON_ID'
      '      ,F_ORGANISATION_ID'
      '      ,F_PROGRAM_ID'
      '      ,F_SESSION_ID'
      '  FROM T_XLS_VCA_IMPORT'
      '  WHERE F_FLAG = '#39'I'#39)
    AutoOpen = False
    Left = 104
    Top = 32
  end
  object prvXLS: TFVBFFCDataSetProvider
    DataSet = qryXLS
    UpdateMode = upWhereAll
    Left = 104
    Top = 80
  end
  object cdsXLS: TFVBFFCClientDataSet
    Tag = 1
    Aggregates = <>
    PacketRecords = 25
    Params = <>
    ProviderName = 'prvXLS'
    RemoteServer = ssckData
    OnReconcileError = cdsXLSReconcileError
    Left = 104
    Top = 136
    object cdsXLSF_VCA_IMPORT_ID: TAutoIncField
      FieldName = 'F_VCA_IMPORT_ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsXLSF_VCA_EXAM_NR: TStringField
      FieldName = 'F_VCA_EXAM_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsXLSF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsXLSF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsXLSF_DATE_OF_BIRTH: TDateTimeField
      FieldName = 'F_DATE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object cdsXLSF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsXLSF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsXLSF_REEKS: TStringField
      FieldName = 'F_REEKS'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsXLSF_SCORE: TStringField
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsXLSF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsXLSF_ADDRESS: TStringField
      FieldName = 'F_ADDRESS'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsXLSF_POSTAL_CODE: TStringField
      FieldName = 'F_POSTAL_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsXLSF_COUNTRY: TStringField
      FieldName = 'F_COUNTRY'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsXLSF_CITY: TStringField
      FieldName = 'F_CITY'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsXLSF_FUNCTIE: TStringField
      FieldName = 'F_FUNCTIE'
      Size = 50
    end
    object cdsXLSF_EXTRA: TStringField
      FieldName = 'F_EXTRA'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsXLSF_INSERT_DATE: TDateTimeField
      FieldName = 'F_INSERT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsXLSF_UPDATE_DATE: TDateTimeField
      FieldName = 'F_UPDATE_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsXLSF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object cdsXLSF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsXLSF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = []
    end
    object cdsXLSF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object cdsXLSF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = []
    end
    object cdsXLSF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
  end
end
