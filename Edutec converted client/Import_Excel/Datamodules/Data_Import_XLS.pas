unit Data_Import_XLS;

interface

uses
  SysUtils, Classes, DB, DBClient, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, ADODB, MConnect,
  Data_EDUDataModule, unit_EdutecInterfaces, recerror;

type
  TdtmImport_XLS = class(TDataModule)
    ssckData: TFVBFFCSharedConnection;
    qryXLS: TFVBFFCQuery;
    prvXLS: TFVBFFCDataSetProvider;
    cdsXLS: TFVBFFCClientDataSet;
    cdsXLSF_VCA_IMPORT_ID: TAutoIncField;
    cdsXLSF_VCA_EXAM_NR: TStringField;
    cdsXLSF_PERSON_LASTNAME: TStringField;
    cdsXLSF_PERSON_FIRSTNAME: TStringField;
    cdsXLSF_DATE_OF_BIRTH: TDateTimeField;
    cdsXLSF_PLACE_OF_BIRTH: TStringField;
    cdsXLSF_MAILBOX: TStringField;
    cdsXLSF_REEKS: TStringField;
    cdsXLSF_SCORE: TStringField;
    cdsXLSF_ORG_NAME: TStringField;
    cdsXLSF_ADDRESS: TStringField;
    cdsXLSF_POSTAL_CODE: TStringField;
    cdsXLSF_COUNTRY: TStringField;
    cdsXLSF_CITY: TStringField;
    cdsXLSF_EXTRA: TStringField;
    cdsXLSF_INSERT_DATE: TDateTimeField;
    cdsXLSF_UPDATE_DATE: TDateTimeField;
    cdsXLSF_FLAG: TStringField;
    cdsXLSF_COMMENT: TStringField;
    cdsXLSF_PERSON_ID: TIntegerField;
    cdsXLSF_ORGANISATION_ID: TIntegerField;
    cdsXLSF_PROGRAM_ID: TIntegerField;
    cdsXLSF_SESSION_ID: TIntegerField;
    cdsXLSF_FUNCTIE: TStringField;
    procedure cdsXLSReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
  private
    { Private declarations }
  protected
    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
    procedure XLSSetProcessed(AImportId: Integer);
    procedure XLSSetSession(AImportId, ASession: Integer);
    procedure XLSAutCreate(AImportId: Integer);
    procedure CloseDefSearchCriteriaDS;
    function GetDefaultSearchCriteria: TClientDataSet;

  public
    { Public declarations }
    procedure DeleteAndCommit(SLRowsToDelete : TStringList);
  end;

var
  dtmImport_XLS: TdtmImport_XLS ;

implementation

{$R *.dfm}

function TdtmImport_XLS.ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
begin
end;

procedure TdtmImport_XLS.XLSSetProcessed(AImportId: Integer);
begin
end;

procedure TdtmImport_XLS.XLSSetSession(AImportId, ASession: Integer);
begin
end;

procedure TdtmImport_XLS.XLSAutCreate(AImportId: Integer);
begin
end;

procedure TdtmImport_XLS.CloseDefSearchCriteriaDS;
begin
end;

function TdtmImport_XLS.GetDefaultSearchCriteria: TClientDataSet;
begin
end;

procedure TdtmImport_XLS.cdsXLSReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
  HandleReconcileError(Dataset, UpdateKind, E)
end;

procedure TdtmImport_XLS.DeleteAndCommit(SLRowsToDelete : TStringList);
var
  i : integer;
begin
  for i := 0 to SLRowsToDelete.Count-1 do
  begin
    cdsXLS.RecNo := StrToInt(SLRowsToDelete[i])+1;
      cdsXLS.Delete;
  end;
  cdsXLS.ApplyUpdates(-1);
  ssckData.AppServer.ProcessValidateKeys;
end;

end.
