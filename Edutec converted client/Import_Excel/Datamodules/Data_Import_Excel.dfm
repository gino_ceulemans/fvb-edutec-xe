inherited dtmImport_Excel: TdtmImport_Excel
  KeyFields = 'F_VCA_IMPORT_ID'
  ListViewClass = 'TfrmImport_Excel_List'
  Registered = True
  Left = 550
  Top = 285
  inherited qryList: TFVBFFCQuery
    SQL.Strings = (
      'SELECT'
      '       F_VCA_IMPORT_ID'
      '      ,F_VCA_EXAM_NR'
      '      ,F_PERSON_LASTNAME'
      '      ,F_PERSON_FIRSTNAME'
      '      ,F_DATE_OF_BIRTH'
      '      ,F_PLACE_OF_BIRTH'
      '      ,F_MAILBOX'
      '      ,F_REEKS'
      '      ,F_SCORE'
      '      ,F_ORG_NAME'
      '      ,F_ADDRESS'
      '      ,F_POSTAL_CODE'
      '      ,F_COUNTRY'
      '      ,F_CITY'
      '      ,F_EXTRA'
      '      ,F_INSERT_DATE'
      '      ,F_UPDATE_DATE'
      '      ,F_FLAG'
      '      ,F_COMMENT'
      '      ,F_PERSON_ID'
      '      ,F_ORGANISATION_ID'
      '      ,F_PROGRAM_ID'
      '      ,F_NAME'
      '      ,F_SESSION_ID'
      '      ,F_SESSION_NAME'
      '      ,F_SESSION_CODE'
      '      ,F_SESSION_START_DATE'
      '      ,F_SESSION_END_DATE'
      '  FROM'
      '       dbo.V_XLS_VCA_IMPORT')
  end
  inherited cdsList: TFVBFFCClientDataSet
    Tag = 1
    RemoteServer = ssckData
    BeforePost = nil
    BeforeDelete = nil
    OnNewRecord = nil
    object cdsListF_VCA_IMPORT_ID: TIntegerField
      FieldName = 'F_VCA_IMPORT_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_VCA_EXAM_NR: TStringField
      FieldName = 'F_VCA_EXAM_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsListF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_DATE_OF_BIRTH: TDateTimeField
      FieldName = 'F_DATE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsListF_REEKS: TStringField
      FieldName = 'F_REEKS'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsListF_SCORE: TStringField
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsListF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_ADDRESS: TStringField
      FieldName = 'F_ADDRESS'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_POSTAL_CODE: TStringField
      FieldName = 'F_POSTAL_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsListF_COUNTRY: TStringField
      FieldName = 'F_COUNTRY'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_CITY: TStringField
      FieldName = 'F_CITY'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_EXTRA: TStringField
      FieldName = 'F_EXTRA'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_FUNCTIE: TStringField
      FieldName = 'F_FUNCTIE'
      Size = 50
    end
    object cdsListF_INSERT_DATE: TDateTimeField
      FieldName = 'F_INSERT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_UPDATE_DATE: TDateTimeField
      FieldName = 'F_UPDATE_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object cdsListF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_SESSION_CODE: TStringField
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
    end
    object cdsSearchCriteriaF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
    end
    object cdsSearchCriteriaF_ORG_NAME: TStringField
      FieldName = 'F_ORG_NAME'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmImport_Excel'
  end
  object cdsSessionsForProgram: TFVBFFCClientDataSet
    Tag = 1
    Aggregates = <>
    AutoCalcFields = False
    FetchOnDemand = False
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@ATransfertId'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@AMsgType'
        ParamType = ptInput
        Size = 3
      end
      item
        DataType = ftDateTime
        Name = '@AStartDate'
        ParamType = ptInput
      end>
    ProviderName = 'prvGetSessionsForProgram'
    RemoteServer = ssckData
    AfterPost = cdsListAfterPost
    AfterDelete = cdsListAfterDelete
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 280
    Top = 56
  end
  object cdsDefaultSearchCriteria: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    AutoOpen = False
    Left = 280
    Top = 216
    object cdsDefaultSearchCriteriaF_CODE: TStringField
      FieldName = 'F_CODE'
      Size = 255
    end
  end
end
