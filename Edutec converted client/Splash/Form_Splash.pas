unit Form_Splash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxProgressBar, Unit_FVBFFCDevExpress, jpeg, cxControls,
  cxContainer, cxEdit, cxImage, ExtCtrls, Unit_FVBFFCFoldablePanel;

type
  TfrmSplash = class(TForm)
    pnlFVBFFCPanel1: TFVBFFCPanel;
    FVBFFCImage1: TFVBFFCImage;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure WaitaTick(NrOfSeconds : integer = 0;NrOfMilliSeconds : integer =1);
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;

implementation

{$R *.dfm}

{ TfrmSplash }

procedure TfrmSplash.WaitaTick(NrOfSeconds : integer = 0;NrOfMilliSeconds : integer =1);
// Deze procedure laat de applicatie een X-aantal Ms wachten
var
  aMs : integer;
  frequency, start, stop : int64;
begin
  aMs := NrOfSeconds * 1000 + NrOfMilliSeconds;
  try
    QueryPerformanceFrequency(frequency);
    QueryPerformanceCounter(Start);
    QueryPerformanceCounter(Stop);

    while ((1000 * (stop - start)) div frequency) < aMs do
    begin
      Application.ProcessMessages;
      QueryPerformanceCounter(Stop);
    end;
  except
  end;
end;

procedure TfrmSplash.FormCreate(Sender: TObject);
begin
  // Default setting ...
end;

end.
