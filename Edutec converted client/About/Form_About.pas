unit Form_About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLabel, Unit_FVBFFCDevExpress, jpeg, cxControls, cxContainer,
  cxEdit, cxImage, ExtCtrls, Unit_FVBFFCFoldablePanel, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, cxGraphics, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue;

type
  TfrmAbout = class(TForm)
    pnlLogo: TFVBFFCPanel;
    pnlDetail: TFVBFFCPanel;
    imgLogo: TFVBFFCImage;
    cxlblFileVersion: TFVBFFCLabel;
    cxlblVerFileVersion: TFVBFFCLabel;
    cxlblCompanyName: TFVBFFCLabel;
    cxlblVerCompanyName: TFVBFFCLabel;
    cxlblFileDescription: TFVBFFCLabel;
    cxlblVerFileDescription: TFVBFFCLabel;
    cxlblFileDate: TFVBFFCLabel;
    cxlblVerFileDate: TFVBFFCLabel;
    cxlblFileSize: TFVBFFCLabel;
    cxlblVerFileSize: TFVBFFCLabel;
    cxlblWindowsVersion: TFVBFFCLabel;
    cxlblVerWindowsVersion: TFVBFFCLabel;
    cxlblComputerName: TFVBFFCLabel;
    cxlblVerComputerName: TFVBFFCLabel;
    cxlblUsername: TFVBFFCLabel;
    cxlblVerUserName: TFVBFFCLabel;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    cxlblSPID: TFVBFFCLabel;
    cxlblVerSPID: TFVBFFCLabel;
    cxlblDB: TFVBFFCLabel;
    cxlblVerDB: TFVBFFCLabel;
    cxlblServer: TFVBFFCLabel;
    cxlblVerServer: TFVBFFCLabel;
    cxlblUser: TFVBFFCLabel;
    cxlblVerUser: TFVBFFCLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

uses Data_EDUDataModule, Data_About, MaskUtils;

{$R *.dfm}

procedure TfrmAbout.FormCreate(Sender: TObject);
var
   aAbout : TclsAbout;
begin
  // Zorg ervoor dat we de nodige data gaan ophalen via TclsVersion
  aAbout := TclsAbout.Create( application );

  cxlblVerFileVersion.Caption := aAbout.FileVersion;
  cxlblVerCompanyName.Caption := aAbout.CompanyName;
  cxlblVerFileDescription.Caption := aAbout.FileDescription;
  cxlblVerFileDate.Caption := aAbout.FileDate;
  cxlblVerFileSize.Caption := FormatFloat( '##0.000', aAbout.Size / 1000000 ) + ' MB';
  cxlblVerWindowsVersion.Caption := aAbout.WindowsVersion;
  cxlblVerUserName.Caption := aAbout.UserName;
  cxlblVerComputerName.Caption := aAbout.ComputerName;
//  cxlblVerSPID.Caption := aAbout.SPID;
//  cxlblVerDB.Caption := aAbout.DBname;
//  cxlblVerServer.Caption := aAbout.SQLServer;
//  cxlblVerUser.Caption := aAbout.User;

  freeAndNil( aAbout );
end;

end.
