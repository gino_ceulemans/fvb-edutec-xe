{*****************************************************************************
  This unit contains the Session Wizard form.

  @Name       Form_SessionWizard
  @Author     Ivan Van den Bossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  16/03/2009   tclaesen             Mantis 5132: lblRecordNo: TLabel -> TFVBFFCLabel
  11/04/2008   ivdbossche           Removed field F_LANGUAGE_NAME in wizard,
                                    disabled OnOpenLanguageRole, added OnOpenDiploma,
                                    hidden gender related fields; (Mantis 2196).
  10/10/2007   ivdbossche           Initial creation of the Unit.

******************************************************************************}
unit Form_SessionWizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxDBLabel, Unit_FVBFFCDevExpress, cxControls, cxContainer,
  cxEdit, cxLabel, ExtCtrls, Unit_FVBFFCFoldablePanel, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxHyperLinkEdit, cxDBEdit,
  cxButtonEdit, cxGroupBox, cxTextEdit, cxMaskEdit, cxSpinEdit, Menus,
  cxLookAndFeelPainters, StdCtrls, cxButtons, cxDropDownEdit, cxCalendar,
  ActnList, ImgList, Unit_FVBFFCComponents, Unit_FVBFFCDBComponents,
  DBCtrls, cxImageComboBox, Grids, DBGrids, cxCurrencyEdit, cxCheckBox,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, System.Actions;

type
  TLoopThroughCallBack = procedure of object;

type
  TfrmSessionWizard = class(TForm)
    pnlRecordHeader: TFVBFFCPanel;
    pnlRecordFixedData: TFVBFFCPanel;
    cxlblF_SESSIONGROUP_ID1: TFVBFFCLabel;
    FVBFFCLabel1: TFVBFFCLabel;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCLabel3: TFVBFFCLabel;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    ActionListOrganisation: TActionList;
    acAppendOrg: TAction;
    acSearchOrg: TAction;
    srcList: TFVBFFCDataSource;
    cxlblSessionID: TFVBFFCLabel;
    cxlblF_NAME: TFVBFFCLabel;
    cxlblF_START_DATE: TFVBFFCLabel;
    cxlblF_END_DATE: TFVBFFCLabel;
    srcOrganisation: TFVBFFCDataSource;
    srcSearchOrganisation: TFVBFFCDataSource;
    srcRoles: TFVBFFCDataSource;
    acCancelModificationsOrg: TAction;
    ActionListRoles: TActionList;
    acAppendRole: TAction;
    acEnrolRole: TAction;
    acCancelModificiationsRole: TAction;
    PopupMenuEnrolments: TPopupMenu;
    Verwijderinschrijvingen1: TMenuItem;
    ActionListEnrolments: TActionList;
    acRemoveEnrolment: TAction;
    Panel1: TPanel;
    pnlEnrolments: TFVBFFCPanel;
    cxGridEnrol: TcxGrid;
    cxgrdtblvListEnrolments: TcxGridDBTableView;
    cxgrdtblvListEnrolmentsF_ENROL_ID: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_PERSON_NAME: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_PERSON_ADDRESS: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_ORGANISATION: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_ROLEGROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_PRICING_SCHEME: TcxGridDBColumn;
    cxgrdtblvListEnrolmentsF_SELECTED_PRICE: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    FVBFFCPanel1: TFVBFFCPanel;
    Panel4: TPanel;
    Panel2: TPanel;
    pnlRecordDetailTitle: TFVBFFCPanel;
    sbxOrganisation: TScrollBox;
    cxlblF_COUNTRY_ID1: TFVBFFCLabel;
    cxdbseF_ORGANISATION_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_ORGANISATION: TFVBFFCDBTextEdit;
    cxgbAddress: TFVBFFCGroupBox;
    cxlblF_STREET1: TFVBFFCLabel;
    cxdbteF_STREET1: TFVBFFCDBTextEdit;
    cxlblF_NUMBER1: TFVBFFCLabel;
    cxdbteF_NUMBER1: TFVBFFCDBTextEdit;
    cxlblF_MAILBOX1: TFVBFFCLabel;
    cxdbteF_MAILBOX1: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxdbbeF_POSTALCODE1: TFVBFFCDBButtonEdit;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdbteF_CITY_NAME1: TFVBFFCDBTextEdit;
    cxlblF_COUNTRY_NAME1: TFVBFFCLabel;
    cxdbbeF_COUNTRY_NAME1: TFVBFFCDBButtonEdit;
    FVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlblF_ORGTYPE_NAME1: TFVBFFCLabel;
    cxdbbeF_ORGTYPE_NAME1: TFVBFFCDBButtonEdit;
    FVBFFCButton1: TFVBFFCButton;
    FVBFFCButton3: TFVBFFCButton;
    DBNavigator1: TDBNavigator;
    Button1: TButton;
    Panel3: TPanel;
    FVBFFCPanel2: TFVBFFCPanel;
    sbxRoles: TScrollBox;
    cxgbGeneralInfo: TFVBFFCGroupBox;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdbteF_LASTNAME: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit;
    cxlblF_DATEBIRTH1: TFVBFFCLabel;
    cxdbdeF_DATEBIRTH: TFVBFFCDBDateEdit;
    cxlblF_GENDER_NAME1: TFVBFFCLabel;
    cxdbbeF_GENDER_NAME: TFVBFFCDBButtonEdit;
    cxlblF_DIPLOMA_NAME: TFVBFFCLabel;
    cxdbbeF_DIPLOMA_NAME: TFVBFFCDBButtonEdit;
    FVBFFCLabel10: TFVBFFCLabel;
    cxdbbeF_ROLEGROUP_NAME: TFVBFFCDBButtonEdit;
    cxdbdeF_START_DATE: TFVBFFCDBDateEdit;
    FVBFFCLabel11: TFVBFFCLabel;
    FVBFFCGroupBox2: TFVBFFCGroupBox;
    FVBFFCLabel4: TFVBFFCLabel;
    cxdbteF_STREET: TFVBFFCDBTextEdit;
    FVBFFCLabel6: TFVBFFCLabel;
    cxdbteF_MAILBOX: TFVBFFCDBTextEdit;
    FVBFFCLabel7: TFVBFFCLabel;
    cxdbbeF_POSTALCODE: TFVBFFCDBButtonEdit;
    FVBFFCLabel8: TFVBFFCLabel;
    cxdbteF_CITY_NAME: TFVBFFCDBTextEdit;
    FVBFFCLabel9: TFVBFFCLabel;
    cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit;
    FVBFFCLabel5: TFVBFFCLabel;
    cxdbteF_NUMBER: TFVBFFCDBTextEdit;
    FVBFFCButton4: TFVBFFCButton;
    FVBFFCButton5: TFVBFFCButton;
    Button2: TButton;
    cxgrdRoles: TcxGrid;
    cxgrdtblvListRoles: TcxGridDBTableView;
    cxgrdtblvListRolesF_ROLE_ID: TcxGridDBColumn;
    cxgrdtblvListRolesF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListRolesF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListRolesF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListRolesF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListRolesF_ROLEGROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListRolesF_ROLEGROUP_ID: TcxGridDBColumn;
    cxgrdtblvListRolesF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListRolesF_END_DATE: TcxGridDBColumn;
    cxgrdlvlList: TcxGridLevel;
    cxgrdtblvListEnrolmentsF_CONSTRUCT: TcxGridDBColumn;
    lblRecordNo: TFVBFFCLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxdbbeF_POSTALCODE1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_COUNTRY_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_ORGTYPE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acSearchOrgExecute(Sender: TObject);
    procedure srcRolesDataChange(Sender: TObject; Field: TField);
    procedure cxdbbeF_GENDER_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_POSTALCODEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acAppendOrgExecute(Sender: TObject);
    procedure srcOrganisationStateChange(Sender: TObject);
    procedure acCancelModificationsOrgExecute(Sender: TObject);
    procedure acAppendRoleExecute(Sender: TObject);
    procedure acEnrolRoleExecute(Sender: TObject);
    procedure acCancelModificiationsRoleExecute(Sender: TObject);
    procedure srcRolesStateChange(Sender: TObject);
    procedure cxdbbeF_ROLEGROUP_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure srcListDataChange(Sender: TObject; Field: TField);
    procedure acRemoveEnrolmentExecute(Sender: TObject);
    procedure PopupMenuEnrolmentsPopup(Sender: TObject);
    procedure OnLookupPerson(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cxgrdtblvListRolesSelectionChanged(
      Sender: TcxCustomGridTableView);
    procedure srcOrganisationDataChange(Sender: TObject; Field: TField);
    procedure cxdbbeF_POSTALCODE1Enter(Sender: TObject);
    procedure cxdbbeF_COUNTRY_NAME1Enter(Sender: TObject);
    procedure cxdbbeF_GENDER_NAMEEnter(Sender: TObject);
    procedure cxdbbeF_ROLEGROUP_NAMEEnter(Sender: TObject);
    procedure cxdbbeF_POSTALCODEEnter(Sender: TObject);
    procedure cxdbbeF_COUNTRY_NAMEEnter(Sender: TObject);
    procedure cxdbbeF_DIPLOMA_NAMEEnter(Sender: TObject);
    procedure cxdbbeF_DIPLOMA_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);

  private
    FSessionId: Integer;
    FSession: String;
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    FFilterModus: Boolean;
    FEditModusOrg: Boolean;
    FEditModusRole: Boolean;
    FPriceClerk: Double;
    FPriceOther: Double;
    FPriceWorker: Double;

    { Private declarations }
    procedure RetrieveSessionData;
    procedure SetFilter;
    procedure SetDataSet;
    procedure SetModus(AEditModus: Boolean; AIsOrganisationRecord: Boolean);
    procedure LoopThroughSelectedRecordsInGrid(AGrid:  TcxGridDBTableView; ACallback: TLoopThroughCallBack);
    procedure EnrolPerson;
    procedure RemoveEnrolment;
    procedure ShowNumberOfRecords;

    { Events }
    procedure OnOpenRoles;
    procedure OnCloseRoles;
    procedure OnOpenCountryOrganisation;
    //procedure OnOpenLanguageRole;
    procedure OnOpenDiploma;
    procedure OnOpenGenderRole;
    procedure OnOpenRolegroupRole;
    procedure OnOpenPostalcodeRole;
    procedure OnOpenCountryRole;
    procedure SetFilterModus(const Value: Boolean);
    property FilterModus: Boolean read FFilterModus write SetFilterModus;


  public
    { Public declarations }
    property SessionId: Integer read FSessionId write FSessionId;
    property Session: String read FSession write FSession;
    property StartDate: TDateTime read FStartDate write FStartDate;
    property EndDate: TDateTime read FEndDate write FEndDate;
    property PriceWorker: Double read FPriceWorker write FPriceWorker;
    property PriceClerk: Double read FPriceClerk write FPriceClerk;
    property PriceOther: Double read FPriceOther write FPriceOther;

  end;

var
  frmSessionWizard: TfrmSessionWizard;

implementation

{$R *.dfm}

uses Data_SessionWizard, DBClient;

procedure TfrmSessionWizard.FormCreate(Sender: TObject);
begin
  dtmSessionWizard := TdtmSessionWizard.Create(nil);

  // Assign events
  dtmSessionWizard.OnOpenRoles := OnOpenRoles;
  dtmSessionWizard.OnCloseRoles := OnCloseRoles;

  // Assign datasets to datasources
  srcList.DataSet := dtmSessionWizard.cdsList;
  srcOrganisation.DataSet := dtmSessionWizard.cdsOrganisation;
  srcSearchOrganisation.DataSet := dtmSessionWizard.cdsSearchOrganisation;
  srcRoles.DataSet := dtmSessionWizard.cdsRoles;

  SetFilter; // We guess users want to search for an organisation...

  FEditModusOrg := False;
  FEditModusRole := False;

end;

procedure TfrmSessionWizard.FormDestroy(Sender: TObject);
begin
  dtmSessionWizard.Free;

end;

procedure TfrmSessionWizard.FormShow(Sender: TObject);
begin
  RetrieveSessionData; // Retrieve session specific data

end;

procedure TfrmSessionWizard.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if dtmSessionWizard.ModifiedData then // Modified data?
  begin
    if MessageDlg(rsConfirmClosingWizard, mtConfirmation, [mbYes, mbNo], 0)=mrYes then
      CanClose := True
    else
      CanClose := False;
  end;

end;

procedure TfrmSessionWizard.srcRolesDataChange(Sender: TObject;
  Field: TField);
begin
  // Adjust width for each field after data has been changed
  { if srcRoles.DataSet.Active then
    cxgrdtblvListRoles.ApplyBestFit();
  }

end;

procedure TfrmSessionWizard.srcOrganisationDataChange(Sender: TObject;
  Field: TField);
begin
  ShowNumberOfRecords;
end;

procedure TfrmSessionWizard.srcListDataChange(Sender: TObject;
  Field: TField);
var
  aPrice: Double;
  aPricingScheme: Integer;
begin
  if srcList.DataSet.Active then
  begin
    if ( Field <> nil ) and ( Field.FieldName = 'F_PRICING_SCHEME' ) then
    begin
      // Pricing scheme has been changed, lookup corresponding price;
      // Lookup price for specified pricing scheme
      aPricingScheme := srcList.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger;
      aPrice := 0;
      
      case aPricingScheme of
        0: aPrice := PriceOther;
        1: aPrice := PriceWorker;
        2: aPrice := PriceClerk;
      end;

      srcList.DataSet.FieldByName('F_SELECTED_PRICE').AsFloat := aPrice;

    end;

    cxgrdtblvListEnrolments.ApplyBestFit();
  end;

end;

procedure TfrmSessionWizard.srcOrganisationStateChange(Sender: TObject);
begin
  if srcOrganisation.State in [dsInsert, dsEdit] then
    SetModus( True, True ) // Set Edit modus
  else
    SetModus( False, True ); // Set Browse modus

end;

procedure TfrmSessionWizard.srcRolesStateChange(Sender: TObject);
begin
    if srcRoles.State in [dsInsert, dsEdit] then
      SetModus( True, False ) // Set Edit modus
    else
      SetModus( False, False); // Set Browse modus

end;

procedure TfrmSessionWizard.LoopThroughSelectedRecordsInGrid(AGrid:  TcxGridDBTableView; ACallback: TLoopThroughCallBack);
var
  lcv, RecId, RecIdx : Integer;
  bookmarks: TStringList;
  gridMode: Boolean;
begin
  assert (assigned (ACallback), 'Callback unassigned in LoopThroughSelectedRecords !');
  {GCXE
  bookmarks := TStringList.Create;
  try
    // Check if there are selected records

    if ( AGrid.Controller.SelectedRecordCount <> 0 ) then
    begin
            gridMode := AGrid.DataController.DataModeController.GridMode;

            // Loop over each selected record in the grid
            for lcv := 0 to Pred( AGrid.Controller.SelectedRecordCount ) do
            begin
              if gridMode then
              begin
                bookmarks.Add (AGrid.DataController.GetSelectedBookmark( lcv ));
              end else
              begin
                RecIdx := AGrid.Controller.SelectedRecords[lcv].RecordIndex;
                RecID := AGrid.DataController.GetRecordId(RecIdx);
                bookmarks.Add( srcRoles.DataSet.Lookup( AGrid.DataController.KeyFieldNames, RecID, 'F_ROLE_ID') );
              end;
            end;

            // When we use a single loop for the bookmarks, even counting down it seems
            //  we might get a ListIndex of bounds while we are deleting rows
            for lcv := Pred (bookmarks.Count) downto 0 do
            begin
              if gridMode then
              begin
                AGrid.DataController.DataSource.DataSet.Bookmark := bookmarks[lcv];
                ACallback;
              end else
              begin
                if srcRoles.DataSet.Locate(AGrid.DataController.KeyFieldNames, bookmarks[lcv], []) then ACallback;
              end;

            end;
    end;

  finally
      bookmarks.free;
  end;
  }
end;


procedure TfrmSessionWizard.SetFilterModus(const Value: Boolean);
begin
  if Value = False then
  begin
    acSearchOrg.Caption := 'Filter';
    acAppendOrg.Visible := True;
  end else begin
    acSearchOrg.Caption := 'Zoek';

    acCancelModificationsOrg.Visible := False;
    acAppendOrg.Visible := False;
  end;

  FFilterModus := Value;
end;

procedure TfrmSessionWizard.SetModus(AEditModus: Boolean; AIsOrganisationRecord: Boolean);
var
  AActionAppend, AActionCancel: TAction;
begin
  if AIsOrganisationRecord then // Set modus for organisation?
  begin
    FEditModusOrg := AEditModus;
    AActionAppend := acAppendOrg;
    AActionCancel := acCancelModificationsOrg;

    // We dont want that a user could modify the selected role while editing an organisation
    acAppendRole.Visible := ( not AEditModus ) and ( srcOrganisation.DataSet.Active ) and ( srcOrganisation.DataSet.RecordCount > 0 ); // At least one organisation should be present!
    acSearchOrg.Visible := not AEditModus; // Filter button should not be visible when user edits organisation
    acCancelModificationsOrg.Visible := ( not AEditModus ) and ( FEditModusRole );
    acEnrolRole.Visible := ( not AEditModus ) and ( FEditModusRole );
    srcRoles.AutoEdit := ( not AEditModus ) {and ( FEditModusRole )};

  end else begin
    FEditModusRole := AEditModus;
    AActionAppend := acAppendRole;
    AActionCancel := acCancelModificiationsRole;
    acEnrolRole.Visible := ( not AEditModus ) and ( cxgrdtblvListRoles.Controller.SelectedRecordCount > 0 ); // button 'Inschrijven' should only be available in browse modus and user has selected one or more roles
    cxgrdRoles.Enabled := not AEditModus; // Roles grid should be only be available in browse modus

    // We dont want that a user could modify the selected organisation while editing role...
    acAppendOrg.Visible := not AEditModus;
    acCancelModificationsOrg.Visible := ( not AEditModus ) and ( FEditModusOrg );
    acSearchOrg.Visible := not AEditModus;
    srcOrganisation.AutoEdit := ( not AEditModus ) {and ( FEditModusOrg )};
    DBNavigator1.Visible := ( not AEditModus );

  end;

  if AEditModus then
    AActionAppend.Caption := 'Bewaar'
  else
    AActionAppend.Caption := 'Nieuw';

  AActionCancel.Visible := AEditModus;

end;

procedure TfrmSessionWizard.RetrieveSessionData;
var
  oldcursor: TCursor;
begin
  oldcursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;

    // Assign header info
    cxlblSessionID.Caption := IntToStr(FSessionId);
    cxlblF_NAME.Caption := FSession;
    cxlblF_START_DATE.Caption := FormatDateTime('dd/mm/yyyy', FStartDate);
    cxlblF_END_DATE.Caption := FormatDateTime('dd/mm/yyyy', FEndDate);

    // Retrieve enrolments for specified session
    dtmSessionWizard.RetrieveEnrolmentsForSession(FSessionId);

    dtmSessionWizard.SessionID := FSessionID;

  finally
    Screen.Cursor := oldcursor;
  end;

end;

procedure TfrmSessionWizard.cxdbbeF_POSTALCODE1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  // Only allow user to select Postalcode when organisation may be edited.
  if srcOrganisation.AutoEdit then
  begin
    if AButtonIndex=0 then
      dtmSessionWizard.SelectPostalCode( True, FFilterModus )
    else
      dtmSessionWizard.RemovePostalcode( FFilterModus );

  end;

end;

procedure TfrmSessionWizard.cxdbbeF_COUNTRY_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  OnOpenCountryOrganisation;

end;

procedure TfrmSessionWizard.cxdbbeF_ORGTYPE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  // Only allow user to select organisation type when organisation may be edited.
  if srcOrganisation.AutoEdit then
  begin
    if not FFilterModus then
      dtmSessionWizard.SelectOrgType;

  end;

end;

procedure TfrmSessionWizard.cxdbbeF_GENDER_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  OnOpenGenderRole;

end;

procedure TfrmSessionWizard.cxdbbeF_GENDER_NAMEEnter(Sender: TObject);
begin
  OnOpenGenderRole;

end;

procedure TfrmSessionWizard.cxdbbeF_POSTALCODEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  OnOpenPostalcodeRole;

end;

procedure TfrmSessionWizard.cxdbbeF_POSTALCODEEnter(Sender: TObject);
begin
  OnOpenPostalcodeRole;

end;


procedure TfrmSessionWizard.cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  OnOpenCountryRole;
  
end;

procedure TfrmSessionWizard.cxdbbeF_COUNTRY_NAMEEnter(Sender: TObject);
begin
  OnOpenCountryRole;

end;


procedure TfrmSessionWizard.cxdbbeF_ROLEGROUP_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  OnOpenRolegroupRole;

end;

procedure TfrmSessionWizard.cxdbbeF_ROLEGROUP_NAMEEnter(Sender: TObject);
begin
  OnOpenRolegroupRole;

end;

procedure TfrmSessionWizard.cxgrdtblvListRolesSelectionChanged(
  Sender: TcxCustomGridTableView);
begin
  if ( cxgrdtblvListRoles.Controller.SelectedRecordCount > 0 ) and ( not FEditModusRole) then
    acEnrolRole.Visible := True
  else
    acEnrolRole.Visible := False;
end;

procedure TfrmSessionWizard.cxdbbeF_POSTALCODE1Enter(Sender: TObject);
begin
  // Only allow user to select Postalcode when organisation may be edited.
  if srcOrganisation.AutoEdit then
    dtmSessionWizard.SelectPostalCode( True, FFilterModus );

end;

procedure TfrmSessionWizard.cxdbbeF_COUNTRY_NAME1Enter(Sender: TObject);
begin
  OnOpenCountryOrganisation;

end;

// Assign fields to 'Data' datasource
procedure TfrmSessionWizard.SetDataSet;
begin
    cxdbseF_ORGANISATION_ID.DataBinding.DataSource := srcOrganisation;
    cxdbteF_ORGANISATION.DataBinding.DataSource := srcOrganisation;
    cxdbteF_ORGANISATION.Style.Color := clWhite;

    cxdbbeF_ORGTYPE_NAME1.DataBinding.DataSource := srcOrganisation;
    //cxdbteF_ORGANISATION.Style.Color := clWhite;

    cxdbteF_STREET1.DataBinding.DataSource := srcOrganisation;
    cxdbteF_NUMBER1.DataBinding.DataSource := srcOrganisation;
    cxdbteF_MAILBOX1.DataBinding.DataSource := srcOrganisation;
    cxdbbeF_POSTALCODE1.DataBinding.DataSource := srcOrganisation;
    cxdbbeF_POSTALCODE1.Style.Color := clWhite;

    cxdbteF_CITY_NAME1.DataBinding.DataSource := srcOrganisation;
    cxdbbeF_COUNTRY_NAME1.DataBinding.DataSource := srcOrganisation;

    cxdbteF_CONFIRMATION_CONTACT1.DataBinding.DataSource := srcOrganisation;
    cxdbteF_CONFIRMATION_CONTACT1_EMAIL.DataBinding.DataSource := srcOrganisation;
    cxdbteF_CONFIRMATION_CONTACT2.DataBinding.DataSource := srcOrganisation;
    cxdbteF_CONFIRMATION_CONTACT1_EMAIL.DataBinding.DataSource := srcOrganisation;
    cxdbteF_CONFIRMATION_CONTACT2_EMAIL.DataBinding.DataSource := srcOrganisation;

    // Show Navigator buttons
    DBNavigator1.Visible := True;
    FilterModus := False;
    
end;

// Prepare fields used for filter purposes
procedure TfrmSessionWizard.SetFilter;
begin
    cxdbseF_ORGANISATION_ID.DataBinding.DataSource := nil;
    cxdbteF_ORGANISATION.DataBinding.DataSource := srcSearchOrganisation;
    cxdbteF_ORGANISATION.Style.Color := clMoneyGreen;

    //cxdbbeF_ORGTYPE_NAME1.DataBinding.DataSource := nil;
    cxdbbeF_ORGTYPE_NAME1.DataBinding.DataSource := nil;
    //cxdbteF_ORGANISATION.Style.Color := clMoneyGreen;

    cxdbteF_STREET1.DataBinding.DataSource := nil;
    cxdbteF_NUMBER1.DataBinding.DataSource := nil;
    cxdbteF_MAILBOX1.DataBinding.DataSource := nil;
    cxdbbeF_POSTALCODE1.DataBinding.DataSource := srcSearchOrganisation;
    cxdbbeF_POSTALCODE1.Style.Color := clMoneyGreen;

    cxdbteF_CITY_NAME1.DataBinding.DataSource := srcSearchOrganisation;
    cxdbbeF_COUNTRY_NAME1.DataBinding.DataSource := nil;

    cxdbteF_CONFIRMATION_CONTACT1.DataBinding.DataSource := nil;
    cxdbteF_CONFIRMATION_CONTACT1_EMAIL.DataBinding.DataSource := nil;
    cxdbteF_CONFIRMATION_CONTACT2.DataBinding.DataSource := nil;
    cxdbteF_CONFIRMATION_CONTACT1_EMAIL.DataBinding.DataSource := nil;
    cxdbteF_CONFIRMATION_CONTACT2_EMAIL.DataBinding.DataSource := nil;

    // Do not show Navigator buttons in filter modus
    DBNavigator1.Visible := False;
    FilterModus := True;

end;

procedure TfrmSessionWizard.acSearchOrgExecute(Sender: TObject);
var
  oldcursor: TCursor;
begin
  if FFilterModus then  // We are in filter modus?
  begin
    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;
      dtmSessionWizard.RetrieveOrganisation;
      SetDataSet;
    finally
        Screen.Cursor := oldcursor;
    end;
  end else begin
    SetFilter;
  end;
end;

procedure TfrmSessionWizard.acAppendOrgExecute(Sender: TObject);
var
  oldcursor: TCursor;
begin
  try
      oldcursor := Screen.Cursor;
      try
        Screen.Cursor := crSQLWait;

        if FEditModusOrg then // Editing record?
          dtmSessionWizard.SaveOrganisation // Save record
        else
          dtmSessionWizard.AddNewOrg; // Add new organisation

      finally
          Screen.Cursor := oldcursor;
      end;

  except
      on E: Exception do begin
          MessageDlg(E.Message, mtError, [mbOk], 0);
      end;
  end;

end;

procedure TfrmSessionWizard.acAppendRoleExecute(Sender: TObject);
var
  oldcursor: TCursor;
begin
  try      { TODO : aanpassen Save }
      oldcursor := Screen.Cursor;
      try
        Screen.Cursor := crSQLWait;

        if FEditModusRole then // Editing record?
        begin
          dtmSessionWizard.SaveRole;  // Save role

          // Enable Enrol button for last added role
          cxgrdtblvListRoles.DataController.SelectRows( cxgrdtblvListRoles.DataController.RowCount-1, cxgrdtblvListRoles.DataController.RowCount-1 );
        end else begin
          dtmSessionWizard.AddNewRole; // Add new role
        end;

      finally
          Screen.Cursor := oldcursor;
      end;

  except
      on E: Exception do begin
          MessageDlg(E.Message, mtError, [mbOk], 0);
      end;
  end;

end;

procedure TfrmSessionWizard.acCancelModificationsOrgExecute(Sender: TObject);
begin
  dtmSessionWizard.CancelModificationsOrganisation;

end;

procedure TfrmSessionWizard.acCancelModificiationsRoleExecute(
  Sender: TObject);
begin
  // Cancel modifications applied to role
  dtmSessionWizard.CancelModificationsRole;

end;

procedure TfrmSessionWizard.acEnrolRoleExecute(Sender: TObject);
var
  oldcursor : TCursor;
begin
  oldcursor := Screen.Cursor;
  try
    Screen.Cursor := crSQLWait;

    // Enrol selected person(s)
    LoopThroughSelectedRecordsInGrid(cxgrdtblvListRoles, EnrolPerson);

    cxgrdtblvListRoles.Controller.ClearSelection;
    dtmSessionWizard.RetrieveEnrolmentsForSession( SessionID );

  finally
      Screen.Cursor := oldcursor;
  end;

end;

procedure TfrmSessionWizard.EnrolPerson;
begin
  if dtmSessionWizard.EnrolPerson > 0 then // Person already enrolled?
    MessageDlg(Format(rsEnrollmentAlreadyExists, [srcRoles.DataSet.FieldByName('F_FIRSTNAME').AsString, srcRoles.DataSet.FieldByName('F_LASTNAME').AsString]), mtInformation, [mbOk], 0)

end;

procedure TfrmSessionWizard.RemoveEnrolment;
begin
  dtmSessionWizard.RemoveEnrolment;

end;

procedure TfrmSessionWizard.acRemoveEnrolmentExecute(Sender: TObject);
var
  oldcursor : TCursor;
begin
  if MessageDlg( rsConfirmRemEnrolment, mtConfirmation, [mbYes,mbNo], 0)=mrYes then
  begin
    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;

      // Enrol selected person(s)
      LoopThroughSelectedRecordsInGrid( cxgrdtblvListEnrolments, RemoveEnrolment );

      cxgrdtblvListEnrolments.Controller.ClearSelection;
      dtmSessionWizard.RetrieveEnrolmentsForSession( SessionID );

    finally
        Screen.Cursor := oldcursor;
    end;
  end;

end;

procedure TfrmSessionWizard.PopupMenuEnrolmentsPopup(Sender: TObject);
begin
  // Enrolments available?
  acRemoveEnrolment.Enabled := ( srcList.DataSet.Active ) and ( srcList.DataSet.RecordCount > 0 );
end;

{ Events which are triggered when cdsRoles has been closed/opened }
procedure TfrmSessionWizard.OnCloseRoles;
begin
  acAppendRole.Visible := False;
  acEnrolRole.Visible := False;

end;

procedure TfrmSessionWizard.OnOpenRoles;
begin
  acAppendRole.Visible := True;
  acEnrolRole.Visible := False; // Hide Enrol button after opening existing roles
  cxgrdtblvListRoles.DataController.ClearSelection; // Make sure that no roles are selected by default

end;

procedure TfrmSessionWizard.OnLookupPerson(Sender: TObject);
begin
  dtmSessionWizard.LookupPerson;

end;

procedure TfrmSessionWizard.OnOpenCountryOrganisation;
begin
    // Only allow user to select country when organisation may be edited.
  if srcOrganisation.AutoEdit then
  begin
    if not FFilterModus then
      dtmSessionWizard.SelectCountry(True);
  end;

end;

{
procedure TfrmSessionWizard.OnOpenLanguageRole;
begin
  // Only allow user to select language when role may be edited.
  if srcRoles.AutoEdit then
    dtmSessionWizard.SelectLanguage;

end;
}

procedure TfrmSessionWizard.OnOpenDiploma;
begin
  // Only allow user to select diploma when role may be edited.
  if srcRoles.AutoEdit then
    dtmSessionWizard.SelectDiploma;
    
end;


procedure TfrmSessionWizard.OnOpenGenderRole;
begin
  // Only allow user to select gender when role may be edited.
  if srcRoles.AutoEdit then
    dtmSessionWizard.SelectGender;

end;

procedure TfrmSessionWizard.OnOpenRolegroupRole;
begin
  // Only allow user to select rolegroup when role may be edited.
  if srcRoles.AutoEdit then
    dtmSessionWizard.SelectRoleGroup;

end;

procedure TfrmSessionWizard.OnOpenPostalcodeRole;
begin
  // Only allow user to select postalcode when role may be edited.
  if srcRoles.AutoEdit then
    dtmSessionWizard.SelectPostalCode( False );

end;

procedure TfrmSessionWizard.OnOpenCountryRole;
begin
  // Only allow user to select country when role may be edited.
  if srcRoles.AutoEdit then
    dtmSessionWizard.SelectCountry( False );

end;

procedure TfrmSessionWizard.cxdbbeF_DIPLOMA_NAMEEnter(Sender: TObject);
begin
  OnOpenDiploma;

end;

procedure TfrmSessionWizard.cxdbbeF_DIPLOMA_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  OnOpenDiploma;

end;

procedure TfrmSessionWizard.ShowNumberOfRecords;
begin
  // Show number of record / total number records
  if ( srcOrganisation.DataSet.Active ) and ( srcOrganisation.DataSet.RecNo >= 0 ) then
    lblRecordNo.Caption := Format('%d / %d', [srcOrganisation.DataSet.RecNo, srcOrganisation.DataSet.RecordCount])
  else
    lblRecordNo.Caption := ''; // Do not shown current recordno/total number of records when user creates new organisation to prevent -1/2 notation
end;

end.

