{*****************************************************************************
  This unit contains the form that will be used to display a list of
  T_PE_PERSON (including last role) records.

  @Name       Form_LookupPerson
  @Author     Ivan Van den Bossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  16/10/2007   ivdbossche           Initial creation of the Unit.

******************************************************************************}

unit Form_LookupPerson;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_SessionWizard, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, Unit_FVBFFCDevExpress, ExtCtrls,
  Unit_FVBFFCFoldablePanel, cxTextEdit, Grids, DBGrids, DBClient, Data_Lookupperson, Unit_FVBFFCDBComponents,
  ActnList, cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator;

type
  Tfrm_LookupPerson = class(TForm)
    sbxOrganisation: TScrollBox;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    srcLookupPerson: TDataSource;
    cxbtnCancel: TFVBFFCButton;
    cxGridPersonsDBTableView: TcxGridDBTableView;
    cxGridPersonsLevel1: TcxGridLevel;
    cxGridPersons: TcxGrid;
    cxGridPersonsDBTableViewF_PERSON_ID: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_SOCSEC_NR: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_LASTNAME: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_FIRSTNAME: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_PERSON_ADDRESS: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_ROLE_ID: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_ORGANISATION_NAME: TcxGridDBColumn;
    cxGridPersonsDBTableViewF_ROLEGROUP_NAME: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure srcLookupPersonDataChange(Sender: TObject; Field: TField);
  private
    FFirstName: String;
    FLastName: String;
    { Private declarations }
  public
    { Public declarations }
    property FirstName: String read FFirstName write FFirstName;
    property LastName: String read FLastName write FLastName;
    procedure GetPerson(ADest: TFVBFFCClientDataSet);
  end;

var
  frm_LookupPerson: Tfrm_LookupPerson;

implementation

{$R *.dfm}

procedure Tfrm_LookupPerson.FormCreate(Sender: TObject);
begin
  dmLookupPerson := nil;
  dmLookupPerson := TdmLookupPerson.Create(self);

end;

procedure Tfrm_LookupPerson.FormShow(Sender: TObject);
begin
  if Assigned ( dmLookupPerson ) then
    dmLookupPerson.LookupPerson(FLastName, FFirstName);

end;

procedure Tfrm_LookupPerson.FormDestroy(Sender: TObject);
begin
  if Assigned ( dmLookupPerson ) then
    dmLookupPerson.Free;

end;

{*****************************************************************************
  This method will be executed when the user has selected an existing person

  @Name       Tfrm_LookupPerson.GetPerson
  @author     Ivan Van den Bossche
  @param      ADest is the dataset which we want to use to copy the person data to.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure Tfrm_LookupPerson.GetPerson(ADest: TFVBFFCClientDataSet);
var
  bookmark: TBookmark;// String;
  I: Integer;
begin
  if cxGridPersonsDBTableView.Controller.SelectedRecordCount <> 0 then
  begin
      bookmark := cxGridPersonsDBTableView.DataController.GetSelectedBookmark(0);
      cxGridPersonsDBTableView.DataController.DataSource.DataSet.Bookmark := bookmark;

      if not (ADest.State in [dsInsert, dsEdit]) then
        ADest.Edit;

      // Copy all person data to ADest clientdataset
      for I:=0 to ADest.FieldCount-1 do
      begin
        if ADest.Fields[I].FieldName <> 'F_ROLE_ID' then // We don't want to make a copy of F_ROLE_ID since another value already has assigned
        begin
          if srcLookupPerson.DataSet.FindField(ADest.Fields[I].FieldName) <> nil then
          begin
            ADest.Fields[I].Value := srcLookupPerson.DataSet.FieldByName(ADest.Fields[I].FieldName).Value;
          end;
        end;
      end;

      srcLookupPerson.DataSet.FieldByName('F_PERSON_ID').AsInteger;
  end;

end;

procedure Tfrm_LookupPerson.srcLookupPersonDataChange(Sender: TObject;
  Field: TField);
begin
  cxGridPersonsDBTableView.ApplyBestFit();
end;

end.
