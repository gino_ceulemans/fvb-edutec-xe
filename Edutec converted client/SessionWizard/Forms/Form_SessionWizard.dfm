object frmSessionWizard: TfrmSessionWizard
  Left = 403
  Top = 43
  Caption = 'Session Wizard'
  ClientHeight = 0
  ClientWidth = 120
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlRecordHeader: TFVBFFCPanel
    Left = 0
    Top = 0
    Width = 120
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 0
    Caption = 'Session Wizard'
    ParentColor = True
    TabOrder = 0
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clHighlightText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clMenuHighlight
    StyleClientArea.BackColor2 = clMenuHighlight
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clHighlightText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitWidth = 790
  end
  object pnlRecordFixedData: TFVBFFCPanel
    Left = 0
    Top = 25
    Width = 120
    Height = 40
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 4
    ParentColor = True
    TabOrder = 1
    StyleBackGround.BackColor = 15914442
    StyleBackGround.BackColor2 = 15914442
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = 15914442
    StyleClientArea.BackColor2 = 15914442
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object cxlblF_SESSIONGROUP_ID1: TFVBFFCLabel
      Left = 8
      Top = 0
      HelpType = htKeyword
      HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
      RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
      Caption = 'Session ( ID )'
      ParentColor = False
      ParentFont = False
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
    end
    object FVBFFCLabel1: TFVBFFCLabel
      Left = 160
      Top = 0
      HelpType = htKeyword
      HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
      RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
      Caption = 'Session'
      ParentColor = False
      ParentFont = False
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
    end
    object FVBFFCLabel2: TFVBFFCLabel
      Left = 520
      Top = 0
      HelpType = htKeyword
      HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
      RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
      Caption = 'Startdatum'
      ParentColor = False
      ParentFont = False
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
    end
    object FVBFFCLabel3: TFVBFFCLabel
      Left = 632
      Top = 0
      HelpType = htKeyword
      HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
      RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
      Caption = 'Einddatum'
      ParentColor = False
      ParentFont = False
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
    end
    object cxlblSessionID: TFVBFFCLabel
      Left = 8
      Top = 16
      Caption = 'cxlblSessionID'
      ParentColor = False
      ParentFont = False
      Style.Color = 15914442
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxlblF_NAME: TFVBFFCLabel
      Left = 160
      Top = 15
      Caption = 'cxlblF_NAME'
      ParentColor = False
      ParentFont = False
      Style.Color = 15914442
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxlblF_START_DATE: TFVBFFCLabel
      Left = 520
      Top = 15
      Caption = 'cxlblF_START_DATE'
      ParentColor = False
      ParentFont = False
      Style.Color = 15914442
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
    object cxlblF_END_DATE: TFVBFFCLabel
      Left = 632
      Top = 15
      Caption = 'cxlblF_END_DATE'
      ParentColor = False
      ParentFont = False
      Style.Color = 15914442
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
    end
  end
  object pnlBottom: TFVBFFCPanel
    Left = 0
    Top = -33
    Width = 120
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 0
    ParentColor = True
    TabOrder = 2
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitTop = 598
    ExplicitWidth = 790
    object pnlBottomButtons: TFVBFFCPanel
      Left = 454
      Top = 0
      Width = 336
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      BorderWidth = 0
      ParentColor = True
      TabOrder = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object cxbtnOK: TFVBFFCButton
        Left = 225
        Top = 4
        Width = 104
        Height = 25
        Caption = 'Sluiten'
        ModalResult = 1
        OptionsImage.Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        OptionsImage.Margin = 10
        OptionsImage.NumGlyphs = 2
        OptionsImage.Spacing = -1
        TabOrder = 0
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 465
    Width = 120
    Height = 133
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 790
    object pnlEnrolments: TFVBFFCPanel
      Left = 0
      Top = 25
      Width = 790
      Height = 108
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 0
      StyleBackGround.BackColor = clAppWorkSpace
      StyleBackGround.BackColor2 = clAppWorkSpace
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clWindow
      StyleClientArea.BackColor2 = 12822429
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object cxGridEnrol: TcxGrid
        Left = 4
        Top = 4
        Width = 782
        Height = 100
        Align = alClient
        Constraints.MinHeight = 100
        PopupMenu = PopupMenuEnrolments
        TabOrder = 0
        object cxgrdtblvListEnrolments: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Buttons.First.Enabled = False
          Navigator.Buttons.PriorPage.Enabled = False
          Navigator.Buttons.Prior.Enabled = False
          Navigator.Buttons.Next.Enabled = False
          Navigator.Buttons.NextPage.Enabled = False
          Navigator.Buttons.Last.Enabled = False
          Navigator.Buttons.Insert.Enabled = False
          Navigator.Buttons.Insert.Visible = False
          Navigator.Buttons.Delete.Enabled = False
          Navigator.Buttons.Delete.Visible = False
          Navigator.Buttons.Edit.Enabled = False
          Navigator.Buttons.Edit.Visible = False
          Navigator.Buttons.Post.Enabled = False
          Navigator.Buttons.Post.Visible = False
          Navigator.Buttons.Cancel.Enabled = False
          Navigator.Buttons.Cancel.Visible = False
          Navigator.Buttons.Refresh.Enabled = False
          Navigator.Buttons.SaveBookmark.Enabled = False
          Navigator.Buttons.GotoBookmark.Enabled = False
          Navigator.Buttons.Filter.Enabled = False
          Navigator.Buttons.Filter.Visible = False
          DataController.DataModeController.GridMode = True
          DataController.DataSource = srcList
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderAutoHeight = True
          OptionsView.Indicator = True
          Styles.StyleSheet = dtmFVBFFCMainClient.GridTableViewStyleSheetDevExpress
          object cxgrdtblvListEnrolmentsF_ENROL_ID: TcxGridDBColumn
            DataBinding.FieldName = 'F_ENROL_ID'
            Visible = False
          end
          object cxgrdtblvListEnrolmentsF_SESSION_ID: TcxGridDBColumn
            DataBinding.FieldName = 'F_SESSION_ID'
            Visible = False
          end
          object cxgrdtblvListEnrolmentsF_PERSON_ID: TcxGridDBColumn
            DataBinding.FieldName = 'F_PERSON_ID'
            Visible = False
          end
          object cxgrdtblvListEnrolmentsF_PERSON_NAME: TcxGridDBColumn
            Caption = 'Naam'
            DataBinding.FieldName = 'F_PERSON_NAME'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Options.Editing = False
          end
          object cxgrdtblvListEnrolmentsF_PERSON_ADDRESS: TcxGridDBColumn
            Caption = 'Adres'
            DataBinding.FieldName = 'F_PERSON_ADDRESS'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Options.Editing = False
          end
          object cxgrdtblvListEnrolmentsF_SOCSEC_NR: TcxGridDBColumn
            Caption = 'Socsec'
            DataBinding.FieldName = 'F_SOCSEC_NR'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Options.Editing = False
          end
          object cxgrdtblvListEnrolmentsF_ORGANISATION: TcxGridDBColumn
            Caption = 'Organisatie'
            DataBinding.FieldName = 'F_ORGANISATION'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Options.Editing = False
          end
          object cxgrdtblvListEnrolmentsF_ORGANISATION_ID: TcxGridDBColumn
            DataBinding.FieldName = 'F_ORGANISATION_ID'
            Visible = False
          end
          object cxgrdtblvListEnrolmentsF_STATUS_NAME: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'F_STATUS_NAME'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
            Visible = False
            Options.Editing = False
          end
          object cxgrdtblvListEnrolmentsF_ROLEGROUP_NAME: TcxGridDBColumn
            Caption = 'Functie'
            DataBinding.FieldName = 'F_ROLEGROUP_NAME'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
          end
          object cxgrdtblvListEnrolmentsF_PRICING_SCHEME: TcxGridDBColumn
            Caption = 'Tussenkomst'
            DataBinding.FieldName = 'F_PRICING_SCHEME'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.ImmediatePost = True
            Properties.Items = <
              item
                Description = 'Geen tussenkomst'
                ImageIndex = 0
                Value = 0
              end
              item
                Description = 'FVB'
                Value = 1
              end
              item
                Description = 'CEVORA'
                Value = 2
              end>
            Width = 79
          end
          object cxgrdtblvListEnrolmentsF_SELECTED_PRICE: TcxGridDBColumn
            Caption = 'Prijs'
            DataBinding.FieldName = 'F_SELECTED_PRICE'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 35
          end
          object cxgrdtblvListEnrolmentsF_CONSTRUCT: TcxGridDBColumn
            Caption = 'Import vanuit Osiris'
            DataBinding.FieldName = 'F_CONSTRUCT'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ReadOnly = True
            Options.Editing = False
            Width = 72
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxgrdtblvListEnrolments
        end
      end
    end
    object FVBFFCPanel1: TFVBFFCPanel
      Left = 0
      Top = 0
      Width = 790
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 0
      Caption = 'Inschrijvingen'
      ParentColor = True
      TabOrder = 1
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clWhite
      StyleClientArea.BackColor2 = clGradientInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 65
    Width = 120
    Height = 400
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 425
      Height = 400
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object pnlRecordDetailTitle: TFVBFFCPanel
        Left = 0
        Top = 0
        Width = 425
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 0
        Caption = 'Organisatie'
        ParentColor = True
        TabOrder = 0
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clWhite
        StyleClientArea.BackColor2 = clGradientInactiveCaption
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object sbxOrganisation: TScrollBox
        Left = 0
        Top = 25
        Width = 425
        Height = 374
        HorzScrollBar.Style = ssFlat
        VertScrollBar.Style = ssFlat
        BevelInner = bvNone
        BevelOuter = bvNone
        BevelKind = bkFlat
        BorderStyle = bsNone
        Color = 15914442
        ParentColor = False
        TabOrder = 1
        object cxlblF_COUNTRY_ID1: TFVBFFCLabel
          Left = 8
          Top = 8
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_COUNTRY_ID'
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          Caption = 'Organisation ( ID )'
          FocusControl = cxdbseF_ORGANISATION_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdbseF_ORGANISATION_ID: TFVBFFCDBSpinEdit
          Left = 104
          Top = 8
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_ORGANISATION_ID'
          RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
          DataBinding.DataField = 'F_ORGANISATION_ID'
          DataBinding.DataSource = srcOrganisation
          ParentFont = False
          Properties.ReadOnly = True
          TabOrder = 1
          Width = 121
        end
        object cxlblF_NAME_NL1: TFVBFFCLabel
          Left = 8
          Top = 32
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_NAME_NL'
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          Caption = 'Naam'
          FocusControl = cxdbteF_ORGANISATION
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdbteF_ORGANISATION: TFVBFFCDBTextEdit
          Left = 88
          Top = 32
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_ORGANISATION'
          DataBinding.DataField = 'F_ORGANISATION'
          DataBinding.DataSource = srcOrganisation
          ParentFont = False
          Properties.ReadOnly = False
          Style.Color = clWindow
          TabOrder = 2
          Width = 321
        end
        object cxgbAddress: TFVBFFCGroupBox
          Left = 8
          Top = 80
          Caption = 'Adres'
          ParentColor = False
          ParentFont = False
          TabOrder = 4
          Transparent = True
          Height = 145
          Width = 401
          object cxlblF_STREET1: TFVBFFCLabel
            Left = 8
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_STREET'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Straat'
            FocusControl = cxdbteF_STREET1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_STREET1: TFVBFFCDBTextEdit
            Left = 80
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_STREET'
            DataBinding.DataField = 'F_STREET'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 1
            Width = 300
          end
          object cxlblF_NUMBER1: TFVBFFCLabel
            Left = 8
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_NUMBER'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Nummer'
            FocusControl = cxdbteF_NUMBER1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NUMBER1: TFVBFFCDBTextEdit
            Left = 80
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_NUMBER'
            DataBinding.DataField = 'F_NUMBER'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 2
            Width = 121
          end
          object cxlblF_MAILBOX1: TFVBFFCLabel
            Left = 208
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_MAILBOX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bus'
            FocusControl = cxdbteF_MAILBOX1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_MAILBOX1: TFVBFFCDBTextEdit
            Left = 256
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_MAILBOX'
            DataBinding.DataField = 'F_MAILBOX'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 3
            Width = 121
          end
          object cxlblF_POSTALCODE1: TFVBFFCLabel
            Left = 8
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Postcode'
            FocusControl = cxdbbeF_POSTALCODE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_POSTALCODE1: TFVBFFCDBButtonEdit
            Left = 80
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_POSTALCODE'
            DataBinding.DataField = 'F_POSTALCODE'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end
              item
                Caption = 'X'
                Kind = bkText
              end>
            Properties.OnButtonClick = cxdbbeF_POSTALCODE1PropertiesButtonClick
            Style.Edges = [bLeft, bTop, bRight, bBottom]
            Style.ButtonStyle = btsDefault
            TabOrder = 4
            OnEnter = cxdbbeF_POSTALCODE1Enter
            Width = 121
          end
          object cxlblF_CITY_NAME1: TFVBFFCLabel
            Left = 8
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CITY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gemeente'
            FocusControl = cxdbteF_CITY_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CITY_NAME1: TFVBFFCDBTextEdit
            Left = 80
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CITY_NAME'
            DataBinding.DataField = 'F_CITY_NAME'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 5
            Width = 300
          end
          object cxlblF_COUNTRY_NAME1: TFVBFFCLabel
            Left = 8
            Top = 112
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_COUNTRY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land'
            FocusControl = cxdbbeF_COUNTRY_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COUNTRY_NAME1: TFVBFFCDBButtonEdit
            Left = 80
            Top = 112
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_COUNTRY_NAME'
            DataBinding.DataField = 'F_COUNTRY_NAME'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.OnButtonClick = cxdbbeF_COUNTRY_NAME1PropertiesButtonClick
            TabOrder = 6
            OnEnter = cxdbbeF_COUNTRY_NAME1Enter
            Width = 300
          end
        end
        object FVBFFCGroupBox1: TFVBFFCGroupBox
          Left = 7
          Top = 224
          Caption = 'Contactgegevens bevestigingsrapporten'
          ParentColor = False
          ParentFont = False
          TabOrder = 8
          Transparent = True
          Height = 113
          Width = 402
          object cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel
            Left = 8
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Contactpersoon 1'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit
            Left = 120
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
            DataBinding.DataField = 'F_CONFIRMATION_CONTACT1'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 0
            Width = 273
          end
          object cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel
            Left = 8
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Contactpersoon 2'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit
            Left = 120
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
            DataBinding.DataField = 'F_CONFIRMATION_CONTACT2'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 2
            Width = 273
          end
          object cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel
            Left = 64
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'EMail'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit
            Left = 120
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriEMail
            DataBinding.DataField = 'F_CONFIRMATION_CONTACT1_EMAIL'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 1
            Width = 273
          end
          object cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel
            Left = 64
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'EMail'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit
            Left = 120
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriEMail
            DataBinding.DataField = 'F_CONFIRMATION_CONTACT2_EMAIL'
            DataBinding.DataSource = srcOrganisation
            ParentFont = False
            TabOrder = 3
            Width = 273
          end
        end
        object cxlblF_ORGTYPE_NAME1: TFVBFFCLabel
          Left = 8
          Top = 56
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisation_Record.F_ORGTYPE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          Caption = 'Org. Type'
          FocusControl = cxdbbeF_ORGTYPE_NAME1
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdbbeF_ORGTYPE_NAME1: TFVBFFCDBButtonEdit
          Left = 88
          Top = 56
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisation_Record.F_ORGTYPE_NAME'
          DataBinding.DataField = 'F_ORGTYPE_NAME'
          DataBinding.DataSource = srcOrganisation
          ParentFont = False
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = cxdbbeF_ORGTYPE_NAME1PropertiesButtonClick
          TabOrder = 3
          Width = 200
        end
        object FVBFFCButton1: TFVBFFCButton
          Left = 272
          Top = 344
          Width = 81
          Height = 25
          Action = acAppendOrg
          OptionsImage.Margin = 10
          OptionsImage.Spacing = -1
          TabOrder = 10
        end
        object FVBFFCButton3: TFVBFFCButton
          Left = 352
          Top = 344
          Width = 57
          Height = 25
          Action = acSearchOrg
          OptionsImage.Margin = 10
          OptionsImage.Spacing = -1
          TabOrder = 11
        end
        object DBNavigator1: TDBNavigator
          Left = 8
          Top = 344
          Width = 112
          Height = 25
          DataSource = srcOrganisation
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
          TabOrder = 6
        end
        object Button1: TButton
          Left = 120
          Top = 344
          Width = 155
          Height = 25
          Action = acCancelModificationsOrg
          TabOrder = 9
        end
        object lblRecordNo: TFVBFFCLabel
          Left = 344
          Top = 8
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.IsFontAssigned = True
        end
      end
    end
    object Panel3: TPanel
      Left = 425
      Top = 0
      Width = 365
      Height = 400
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 1
      object FVBFFCPanel2: TFVBFFCPanel
        Left = 0
        Top = 0
        Width = 365
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 0
        Caption = 'Functies'
        ParentColor = True
        TabOrder = 0
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clWhite
        StyleClientArea.BackColor2 = clGradientInactiveCaption
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object sbxRoles: TScrollBox
        Left = 0
        Top = 25
        Width = 365
        Height = 375
        HorzScrollBar.Style = ssFlat
        VertScrollBar.Style = ssFlat
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = 15914442
        ParentColor = False
        TabOrder = 1
        object cxgbGeneralInfo: TFVBFFCGroupBox
          Left = 0
          Top = 112
          Caption = 'Algemeen'
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = True
          Height = 113
          Width = 572
          object cxlblF_LASTNAME1: TFVBFFCLabel
            Left = 8
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_LASTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Familienaam'
            FocusControl = cxdbteF_LASTNAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_LASTNAME: TFVBFFCDBTextEdit
            Left = 120
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_LASTNAME'
            DataBinding.DataField = 'F_LASTNAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 0
            Width = 256
          end
          object cxlblF_FIRSTNAME1: TFVBFFCLabel
            Left = 8
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_FIRSTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Voornaam'
            FocusControl = cxdbteF_FIRSTNAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit
            Left = 120
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_FIRSTNAME'
            DataBinding.DataField = 'F_FIRSTNAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 1
            OnExit = OnLookupPerson
            Width = 193
          end
          object cxlblF_DATEBIRTH1: TFVBFFCLabel
            Left = 8
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_DATEBIRTH'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Geboortedatum'
            FocusControl = cxdbdeF_DATEBIRTH
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_DATEBIRTH: TFVBFFCDBDateEdit
            Left = 120
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_DATEBIRTH'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_DATEBIRTH'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 5
            Width = 137
          end
          object cxlblF_GENDER_NAME1: TFVBFFCLabel
            Left = 320
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_GENDER_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Geslacht'
            FocusControl = cxdbbeF_GENDER_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbbeF_GENDER_NAME: TFVBFFCDBButtonEdit
            Left = 400
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_GENDER_NAME'
            DataBinding.DataField = 'F_GENDER_NAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.OnButtonClick = cxdbbeF_GENDER_NAMEPropertiesButtonClick
            TabOrder = 4
            Visible = False
            OnEnter = cxdbbeF_GENDER_NAMEEnter
            Width = 169
          end
          object cxlblF_DIPLOMA_NAME: TFVBFFCLabel
            Left = 8
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_DIPLOMA_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Opleidingsniveau'
            FocusControl = cxdbbeF_DIPLOMA_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_DIPLOMA_NAME: TFVBFFCDBButtonEdit
            Left = 120
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_DIPLOMA_NAME'
            DataBinding.DataField = 'F_DIPLOMA_NAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.OnButtonClick = cxdbbeF_DIPLOMA_NAMEPropertiesButtonClick
            TabOrder = 3
            OnEnter = cxdbbeF_DIPLOMA_NAMEEnter
            Width = 169
          end
          object FVBFFCLabel10: TFVBFFCLabel
            Left = 320
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_ROLEGROUP_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Functie'
            FocusControl = cxdbbeF_GENDER_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_ROLEGROUP_NAME: TFVBFFCDBButtonEdit
            Left = 400
            Top = 88
            HelpType = htKeyword
            DataBinding.DataField = 'F_ROLEGROUP_NAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.OnButtonClick = cxdbbeF_ROLEGROUP_NAMEPropertiesButtonClick
            TabOrder = 6
            OnEnter = cxdbbeF_ROLEGROUP_NAMEEnter
            Width = 169
          end
          object cxdbdeF_START_DATE: TFVBFFCDBDateEdit
            Left = 400
            Top = 40
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_START_DATE'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 2
            Width = 137
          end
          object FVBFFCLabel11: TFVBFFCLabel
            Left = 320
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_GENDER_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Startdatum'
            FocusControl = cxdbbeF_GENDER_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
        end
        object FVBFFCGroupBox2: TFVBFFCGroupBox
          Left = 0
          Top = 224
          TabStop = True
          Caption = 'Adres'
          ParentColor = False
          ParentFont = False
          TabOrder = 5
          Transparent = True
          Height = 113
          Width = 572
          object FVBFFCLabel4: TFVBFFCLabel
            Left = 8
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_STREET'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Straat'
            FocusControl = cxdbteF_STREET
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_STREET: TFVBFFCDBTextEdit
            Left = 80
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_STREET'
            DataBinding.DataField = 'F_STREET'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 1
            Width = 300
          end
          object FVBFFCLabel6: TFVBFFCLabel
            Left = 208
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_MAILBOX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bus'
            FocusControl = cxdbteF_MAILBOX
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_MAILBOX: TFVBFFCDBTextEdit
            Left = 240
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_MAILBOX'
            DataBinding.DataField = 'F_MAILBOX'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 3
            Width = 121
          end
          object FVBFFCLabel7: TFVBFFCLabel
            Left = 8
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Postcode'
            FocusControl = cxdbbeF_POSTALCODE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_POSTALCODE: TFVBFFCDBButtonEdit
            Left = 80
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_POSTALCODE'
            DataBinding.DataField = 'F_POSTALCODE'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.OnButtonClick = cxdbbeF_POSTALCODEPropertiesButtonClick
            TabOrder = 4
            OnEnter = cxdbbeF_POSTALCODEEnter
            Width = 121
          end
          object FVBFFCLabel8: TFVBFFCLabel
            Left = 208
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_CITY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gemeente'
            FocusControl = cxdbteF_CITY_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CITY_NAME: TFVBFFCDBTextEdit
            Left = 272
            Top = 64
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_CITY_NAME'
            DataBinding.DataField = 'F_CITY_NAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 9
            Width = 289
          end
          object FVBFFCLabel9: TFVBFFCLabel
            Left = 8
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_COUNTRY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land'
            FocusControl = cxdbbeF_COUNTRY_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit
            Left = 80
            Top = 88
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_COUNTRY_NAME'
            DataBinding.DataField = 'F_COUNTRY_NAME'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Glyph.Data = {
                  36030000424D3603000000000000360000002800000010000000100000000100
                  18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
                  FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                  FFFF00FFFF00FFFF00FFFF00FFAFAFAF999C9E999C9E999C9E999C9E999C9E99
                  9C9E999C9E999C9E999C9E999C9E999C9E999C9E999C9EFF00FFFF00FFAFAFAF
                  FEFEFDFEFEFDFEFEFDFBFBFAF8F8F7F5F5F4F1F1F0EEEEEDEBEBEAE8E8E7DEDE
                  DED2D2D2999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536DEDEDE999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF695
                  36E8E8E7636B73FF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F69536F69536636B732F76B3636B73FF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF636B
                  732F76B35ABDFF31A5FFFF00FFAFAFAFFEFEFDF69536F69536F69536F69536F6
                  9536F69536F69536F69536F695362F76B35ABDFF31A5FFFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFFFFFFFF69536CCA79DAD9383AD9383AD9383AD93834A42
                  3931A5FF999C9EFF00FFFF00FFAFAFAFFEFEFDF69536F69536F69536AD9383E7
                  D4AEF3E4B2F8E8B5E7D6AAAD9383AD9383FAFAF9999C9EFF00FFFF00FFAFAFAF
                  FEFEFDF69536FFFFFFCCA79DECDEADFEF0BBFFF1BCFFEFBBF9E8B6ECDDADAD93
                  83FDFDFC999C9EFF00FFFF00FFAFAFAFFEFEFDD4730AD4730AAD9383FBEBB7FF
                  F1BDFFF0BCFEEDBBF9E8B6EBDCACAD9383FEFEFD999C9EFF00FFFF00FFAFAFAF
                  FEFEFDD4730AD4730AAD9383F0E0B0FCECB9FBEEC8FCEBBAF4E5B7E2D4A7AD93
                  83FEFEFD999C9EFF00FFFF00FFAFAFAFFEFEFDFEFEFDFEFEFDCCA79DD7BE9EED
                  DEAEF5E5B4F6E9C2EEE1BADBC8A9CCA79DFEFEFD999C9EFF00FFFF00FFAFAFAF
                  999C9E999C9E999C9E999C9EAD9383DABEA2DFCAA6DDC9A6D8C3AEAD9383999C
                  9E999C9E999C9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
                  A79DAD9383AD9383CCA79DFF00FFFF00FFFF00FFFF00FFFF00FF}
                Kind = bkGlyph
              end>
            Properties.OnButtonClick = cxdbbeF_COUNTRY_NAMEPropertiesButtonClick
            TabOrder = 5
            OnEnter = cxdbbeF_COUNTRY_NAMEEnter
            Width = 300
          end
          object FVBFFCLabel5: TFVBFFCLabel
            Left = 8
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_NUMBER'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Nummer'
            FocusControl = cxdbteF_NUMBER
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NUMBER: TFVBFFCDBTextEdit
            Left = 80
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmPerson_Record.F_NUMBER'
            DataBinding.DataField = 'F_NUMBER'
            DataBinding.DataSource = srcRoles
            ParentFont = False
            TabOrder = 2
            Width = 121
          end
        end
        object FVBFFCButton4: TFVBFFCButton
          Left = 496
          Top = 344
          Width = 73
          Height = 25
          Action = acEnrolRole
          OptionsImage.Margin = 10
          OptionsImage.Spacing = -1
          TabOrder = 3
          TabStop = False
        end
        object FVBFFCButton5: TFVBFFCButton
          Left = 416
          Top = 344
          Width = 81
          Height = 25
          Action = acAppendRole
          OptionsImage.Margin = 10
          OptionsImage.Spacing = -1
          TabOrder = 2
          TabStop = False
        end
        object Button2: TButton
          Left = 264
          Top = 344
          Width = 155
          Height = 25
          Action = acCancelModificiationsRole
          TabOrder = 1
          TabStop = False
        end
        object cxgrdRoles: TcxGrid
          Left = 0
          Top = 0
          Width = 572
          Height = 113
          Align = alTop
          Constraints.MinHeight = 100
          TabOrder = 4
          object cxgrdtblvListRoles: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Enabled = False
            Navigator.Buttons.PriorPage.Enabled = False
            Navigator.Buttons.Prior.Enabled = False
            Navigator.Buttons.Next.Enabled = False
            Navigator.Buttons.NextPage.Enabled = False
            Navigator.Buttons.Last.Enabled = False
            Navigator.Buttons.Insert.Enabled = False
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Enabled = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Enabled = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Enabled = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Cancel.Enabled = False
            Navigator.Buttons.Cancel.Visible = False
            Navigator.Buttons.Refresh.Enabled = False
            Navigator.Buttons.SaveBookmark.Enabled = False
            Navigator.Buttons.GotoBookmark.Enabled = False
            Navigator.Buttons.Filter.Enabled = False
            Navigator.Buttons.Filter.Visible = False
            OnSelectionChanged = cxgrdtblvListRolesSelectionChanged
            DataController.DataSource = srcRoles
            DataController.DetailKeyFieldNames = 'F_ROLE_ID'
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.KeyFieldNames = 'F_ROLE_ID'
            DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderAutoHeight = True
            OptionsView.Indicator = True
            Styles.StyleSheet = dtmFVBFFCMainClient.GridTableViewStyleSheetDevExpress
            object cxgrdtblvListRolesF_ROLE_ID: TcxGridDBColumn
              DataBinding.FieldName = 'F_ROLE_ID'
              Visible = False
            end
            object cxgrdtblvListRolesF_PERSON_ID: TcxGridDBColumn
              DataBinding.FieldName = 'F_PERSON_ID'
              Visible = False
            end
            object cxgrdtblvListRolesF_ROLEGROUP_NAME: TcxGridDBColumn
              Caption = 'Functie'
              DataBinding.FieldName = 'F_ROLEGROUP_NAME'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
            end
            object cxgrdtblvListRolesF_FIRSTNAME: TcxGridDBColumn
              Caption = 'Voornaam'
              DataBinding.FieldName = 'F_FIRSTNAME'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              Options.Grouping = False
            end
            object cxgrdtblvListRolesF_LASTNAME: TcxGridDBColumn
              Caption = 'Achternaam'
              DataBinding.FieldName = 'F_LASTNAME'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
              Options.Grouping = False
              SortIndex = 0
              SortOrder = soAscending
            end
            object cxgrdtblvListRolesF_ORGANISATION_ID: TcxGridDBColumn
              DataBinding.FieldName = 'F_ORGANISATION_ID'
              Visible = False
            end
            object cxgrdtblvListRolesF_ROLEGROUP_ID: TcxGridDBColumn
              DataBinding.FieldName = 'F_ROLEGROUP_ID'
              Visible = False
            end
            object cxgrdtblvListRolesF_START_DATE: TcxGridDBColumn
              Caption = 'Startdatum'
              DataBinding.FieldName = 'F_START_DATE'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
            end
            object cxgrdtblvListRolesF_END_DATE: TcxGridDBColumn
              Caption = 'Einddatum'
              DataBinding.FieldName = 'F_END_DATE'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.ReadOnly = True
              Options.Editing = False
              Options.Filtering = False
            end
          end
          object cxgrdlvlList: TcxGridLevel
            GridView = cxgrdtblvListRoles
          end
        end
      end
    end
  end
  object ActionListOrganisation: TActionList
    Left = 248
    Top = 89
    object acAppendOrg: TAction
      Caption = 'Nieuw'
      OnExecute = acAppendOrgExecute
    end
    object acSearchOrg: TAction
      Caption = 'Zoek'
      OnExecute = acSearchOrgExecute
    end
    object acCancelModificationsOrg: TAction
      Caption = 'Wijzigingen ongedaan maken'
      OnExecute = acCancelModificationsOrgExecute
    end
  end
  object srcList: TFVBFFCDataSource
    DataSet = dtmSessionWizard.cdsList
    OnDataChange = srcListDataChange
    Left = 16
    Top = 552
  end
  object srcOrganisation: TFVBFFCDataSource
    DataSet = dtmSessionWizard.cdsOrganisation
    OnStateChange = srcOrganisationStateChange
    OnDataChange = srcOrganisationDataChange
    Left = 320
    Top = 152
  end
  object srcSearchOrganisation: TFVBFFCDataSource
    DataSet = dtmSessionWizard.cdsSearchOrganisation
    Left = 320
    Top = 232
  end
  object srcRoles: TFVBFFCDataSource
    DataSet = dtmSessionWizard.cdsRoles
    OnStateChange = srcRolesStateChange
    OnDataChange = srcRolesDataChange
    Left = 464
    Top = 120
  end
  object ActionListRoles: TActionList
    Left = 728
    Top = 120
    object acAppendRole: TAction
      Caption = 'Nieuw'
      Visible = False
      OnExecute = acAppendRoleExecute
    end
    object acEnrolRole: TAction
      Caption = 'Inschrijven'
      Visible = False
      OnExecute = acEnrolRoleExecute
    end
    object acCancelModificiationsRole: TAction
      Caption = 'Wijzigingen ongedaan maken'
      Visible = False
      OnExecute = acCancelModificiationsRoleExecute
    end
  end
  object PopupMenuEnrolments: TPopupMenu
    OnPopup = PopupMenuEnrolmentsPopup
    Left = 312
    Top = 568
    object Verwijderinschrijvingen1: TMenuItem
      Action = acRemoveEnrolment
    end
  end
  object ActionListEnrolments: TActionList
    Left = 424
    Top = 544
    object acRemoveEnrolment: TAction
      Caption = 'Verwijder inschrijving(en)'
      OnExecute = acRemoveEnrolmentExecute
    end
  end
end
