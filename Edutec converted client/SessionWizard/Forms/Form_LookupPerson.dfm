object frm_LookupPerson: Tfrm_LookupPerson
  Left = 2
  Top = 2
  Caption = 'Persoon opzoeken'
  ClientHeight = 0
  ClientWidth = 120
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sbxOrganisation: TScrollBox
    Left = 0
    Top = 0
    Width = 120
    Height = 678
    HorzScrollBar.Style = ssFlat
    VertScrollBar.Style = ssFlat
    Align = alClient
    BevelInner = bvNone
    BevelOuter = bvNone
    BevelKind = bkFlat
    BorderStyle = bsNone
    Color = 15914442
    ParentColor = False
    TabOrder = 0
    ExplicitWidth = 1014
    object cxGridPersons: TcxGrid
      Left = 0
      Top = 0
      Width = 1014
      Height = 678
      Align = alClient
      TabOrder = 0
      object cxGridPersonsDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataModeController.GridMode = True
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = srcLookupPerson
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupByBox = False
        object cxGridPersonsDBTableViewF_PERSON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_ID'
          Visible = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_LASTNAME: TcxGridDBColumn
          Caption = 'Achternaam'
          DataBinding.FieldName = 'F_LASTNAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Filtering = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_FIRSTNAME: TcxGridDBColumn
          Caption = 'Voornaam'
          DataBinding.FieldName = 'F_FIRSTNAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Filtering = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_PERSON_ADDRESS: TcxGridDBColumn
          Caption = 'Adres'
          DataBinding.FieldName = 'F_PERSON_ADDRESS'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Filtering = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_SOCSEC_NR: TcxGridDBColumn
          Caption = 'Socsec'
          DataBinding.FieldName = 'F_SOCSEC_NR'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Filtering = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_ORGANISATION_NAME: TcxGridDBColumn
          Caption = 'Organisatie'
          DataBinding.FieldName = 'F_ORGANISATION_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Filtering = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_ROLEGROUP_NAME: TcxGridDBColumn
          Caption = 'Functie'
          DataBinding.FieldName = 'F_ROLEGROUP_NAME'
          Options.Filtering = False
          Options.Moving = False
        end
        object cxGridPersonsDBTableViewF_ROLE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROLE_ID'
          Visible = False
        end
      end
      object cxGridPersonsLevel1: TcxGridLevel
        GridView = cxGridPersonsDBTableView
      end
    end
  end
  object pnlBottom: TFVBFFCPanel
    Left = 0
    Top = -33
    Width = 120
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 0
    ParentColor = True
    TabOrder = 1
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitTop = 678
    ExplicitWidth = 1014
    object pnlBottomButtons: TFVBFFCPanel
      Left = 678
      Top = 0
      Width = 336
      Height = 33
      Align = alRight
      BevelOuter = bvNone
      BorderWidth = 0
      ParentColor = True
      TabOrder = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object cxbtnOK: TFVBFFCButton
        Left = 113
        Top = 4
        Width = 104
        Height = 25
        Caption = 'Selecteer'
        ModalResult = 1
        OptionsImage.Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        OptionsImage.Margin = 10
        OptionsImage.NumGlyphs = 2
        OptionsImage.Spacing = -1
        TabOrder = 0
      end
      object cxbtnCancel: TFVBFFCButton
        Left = 223
        Top = 4
        Width = 104
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        OptionsImage.Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        OptionsImage.Margin = 10
        OptionsImage.NumGlyphs = 2
        OptionsImage.Spacing = -1
        TabOrder = 1
      end
    end
  end
  object srcLookupPerson: TDataSource
    OnDataChange = srcLookupPersonDataChange
    Left = 280
    Top = 160
  end
end
