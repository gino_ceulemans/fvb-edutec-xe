object dmLookupPerson: TdmLookupPerson
  OldCreateOrder = False
  Left = 358
  Top = 331
  Height = 244
  Width = 327
  object cdsLookupPerson: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Precision = 255
        NumericScale = 255
        Name = 'Lastname'
        ParamType = ptInput
        Size = 64
      end
      item
        DataType = ftString
        Precision = 255
        NumericScale = 255
        Name = 'Firstname'
        ParamType = ptInput
        Size = 64
      end>
    ProviderName = 'prvLookupPerson'
    ReadOnly = True
    RemoteServer = ssckData
    Left = 32
    Top = 8
    object cdsLookupPersonF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
    end
    object cdsLookupPersonF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsLookupPersonF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_PERSON_ADDRESS: TStringField
      FieldName = 'F_PERSON_ADDRESS'
      ReadOnly = True
      Size = 354
    end
    object cdsLookupPersonF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ReadOnly = True
    end
    object cdsLookupPersonF_ORGANISATION_NAME: TStringField
      FieldName = 'F_ORGANISATION_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
    object cdsLookupPersonF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
    end
    object cdsLookupPersonF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object cdsLookupPersonF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
    end
    object cdsLookupPersonF_STREET: TStringField
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsLookupPersonF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsLookupPersonF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsLookupPersonF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
    end
    object cdsLookupPersonF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsLookupPersonF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ReadOnly = True
      Size = 128
    end
    object cdsLookupPersonF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
    end
  end
  object ssckData: TFVBFFCSharedConnection
    ParentConnection = dtmEDUMainClient.sckcnnMain
    ChildName = 'rdtmSessionWizard'
    Left = 32
    Top = 56
  end
end
