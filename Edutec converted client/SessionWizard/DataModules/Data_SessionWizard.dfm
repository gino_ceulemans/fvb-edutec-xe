object dtmSessionWizard: TdtmSessionWizard
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 307
  Top = 157
  Height = 314
  Width = 322
  object ssckData: TFVBFFCSharedConnection
    ParentConnection = dtmEDUMainClient.sckcnnMain
    ChildName = 'rdtmSessionWizard'
    Left = 16
    Top = 56
  end
  object cdsList: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = 'SessionId'
        ParamType = ptInput
        Size = 4
      end>
    ProviderName = 'prvList'
    RemoteServer = ssckData
    AfterPost = cdsListAfterPost
    OnReconcileError = OnReconcileError
    Left = 88
    Top = 56
    object cdsListF_ENROL_ID: TIntegerField
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = []
    end
    object cdsListF_PERSON_NAME: TStringField
      FieldName = 'F_PERSON_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 129
    end
    object cdsListF_PERSON_ADDRESS: TStringField
      FieldName = 'F_PERSON_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 354
    end
    object cdsListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object cdsListF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object cdsListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object cdsListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SELECTED_PRICE: TFloatField
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object cdsListF_CONSTRUCT: TBooleanField
      FieldName = 'F_CONSTRUCT'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  object cdsRecord: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = 'SessionId'
        ParamType = ptInput
        Size = 4
      end>
    ProviderName = 'prvRecord'
    RemoteServer = ssckData
    OnReconcileError = OnReconcileError
    Left = 152
    Top = 56
    object cdsRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object cdsRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = []
    end
    object cdsRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
  end
  object cdsSearchOrganisation: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <>
    Left = 232
    Top = 144
    object IntegerField2: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object StringField9: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField13: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsSearchOrganisationF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
    end
    object cdsSearchOrganisationF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
    end
    object cdsSearchOrganisationF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
    end
  end
  object cdsOrganisation: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <
      item
        DataType = ftString
        Precision = 255
        NumericScale = 255
        Name = 'Organisation'
        ParamType = ptInput
        Size = 64
      end
      item
        DataType = ftInteger
        Name = 'Postalcode1'
        ParamType = ptInput
        Size = 4
      end
      item
        DataType = ftInteger
        Name = 'Postalcode2'
        ParamType = ptInput
        Size = 4
      end
      item
        DataType = ftInteger
        Name = 'Postalcode3'
        ParamType = ptInput
        Size = 4
      end
      item
        DataType = ftString
        Precision = 255
        NumericScale = 255
        Name = 'City'
        ParamType = ptInput
        Size = 128
      end>
    ProviderName = 'prvOrganisation'
    RemoteServer = ssckData
    BeforePost = cdsOrganisationBeforePost
    OnNewRecord = cdsOrganisationNewRecord
    OnReconcileError = OnReconcileError
    Left = 128
    Top = 144
    object cdsOrganisationF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsOrganisationF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsOrganisationF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object cdsOrganisationF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsOrganisationF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsOrganisationF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsOrganisationF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsOrganisationF_CONTACT_DATA: TStringField
      FieldName = 'F_CONTACT_DATA'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsOrganisationF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsOrganisationF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsOrganisationF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsOrganisationF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsOrganisationF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsOrganisationF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object cdsOrganisationF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsOrganisationF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
    object cdsOrganisationF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object cdsOrganisationF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsOrganisationF_MANUALLY_ADDED: TBooleanField
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
  end
  object cdsRoles: TFVBFFCClientDataSet
    Aggregates = <>
    IndexFieldNames = 'F_ORGANISATION_ID'
    MasterFields = 'F_ORGANISATION_ID'
    MasterSource = srcOrganisation
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = 'OrganisationID'
        ParamType = ptInput
        Size = 4
      end>
    ProviderName = 'prvRoles'
    RemoteServer = ssckData
    AfterOpen = cdsRolesAfterOpen
    AfterClose = cdsRolesAfterClose
    AfterInsert = cdsRolesAfterInsert
    BeforePost = cdsRolesBeforePost
    OnNewRecord = cdsRolesNewRecord
    OnReconcileError = OnReconcileError
    Left = 40
    Top = 216
    object cdsRolesF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsRolesF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_FIRSTNAME: TStringField
      DisplayWidth = 25
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRolesF_LASTNAME: TStringField
      DisplayWidth = 25
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRolesF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRolesF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_ROLEGROUP_NAME: TStringField
      DisplayWidth = 25
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRolesF_END_DATE: TDateTimeField
      DisplayWidth = 10
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRolesF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object cdsRolesF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsRolesF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object cdsRolesF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object cdsRolesF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRolesF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRolesF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_START_DATE: TDateTimeField
      DisplayWidth = 10
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRolesF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  object srcOrganisation: TFVBFFCDataSource
    DataSet = cdsOrganisation
    OnDataChange = srcOrganisationDataChange
    Left = 40
    Top = 144
  end
  object cdsLookupPerson: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Precision = 255
        NumericScale = 255
        Name = 'Lastname'
        ParamType = ptInput
        Size = 64
      end
      item
        DataType = ftString
        Precision = 255
        NumericScale = 255
        Name = 'Firstname'
        ParamType = ptInput
        Size = 64
      end>
    ProviderName = 'prvLookupPerson'
    ReadOnly = True
    RemoteServer = ssckData
    Left = 152
    Top = 216
    object cdsLookupPersonF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
    end
    object cdsLookupPersonF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsLookupPersonF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_PERSON_ADDRESS: TStringField
      FieldName = 'F_PERSON_ADDRESS'
      ReadOnly = True
      Size = 354
    end
    object cdsLookupPersonF_ROLE_ID: TIntegerField
      FieldName = 'F_ROLE_ID'
      ReadOnly = True
    end
    object cdsLookupPersonF_ORGANISATION_NAME: TStringField
      FieldName = 'F_ORGANISATION_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
    end
    object cdsLookupPersonF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object cdsLookupPersonF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
    end
    object cdsLookupPersonF_STREET: TStringField
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsLookupPersonF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsLookupPersonF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsLookupPersonF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
    end
    object cdsLookupPersonF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsLookupPersonF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ReadOnly = True
      Size = 128
    end
    object cdsLookupPersonF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsLookupPersonF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
    end
    object cdsLookupPersonF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
    end
    object cdsLookupPersonF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      Size = 64
    end
  end
end
