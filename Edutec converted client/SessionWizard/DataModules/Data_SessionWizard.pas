{***************************************************************************************************
  This DataModule will be used for the Session wizard

  @Name       Data_SessionWizard
  @Author     Ivan Van den Bossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  11/04/2008   ivdbossche           Disabled SelectLanguage and added SelectDiploma. (Mantis 2196).
  10/10/2007   ivdbossche           Initial creation of the Unit.
****************************************************************************************************}
unit Data_SessionWizard;

interface

uses
  SysUtils, Classes, Controls, Data_EDUDataModule, DB, DBClient,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, MConnect;

  resourcestring
    rsUnableToUpdateDB='Wijzigingen kunnen niet opgeslagen worden: %s';
    rsOrganisationStreetEmpty='Gelieve de straat van de organisatie in te voeren.';
    rsOrganisationNumberEmpty='Gelieve de huisnr van de organisatie in te voeren.';
    rsOrganisationNameEmpty='Gelieve de naam van de organisatie in te voeren.';
    rsOrganisationTypeEmpty='Gelieve het organisatietype te selecteren.';
    rsEnrollmentAlreadyExists='%s %s is reeds ingeschreven.';
    rsRoleLastname='Gelieve de achternaam van de persoon in te voeren.';
    rsRoleFirstname='Gelieve de voornaam van de persoon in te voeren.';
    rsRoleStartdate='Gelieve de startdatum van de persoon op te geven.';
    rsRoleRolegroup='Gelieve de functie van de persoon op te geven.';
    rsConfirmRemEnrolment='Bent u zeker dat u de geselecteerde inschrijving(en) wilt verwijderen voor deze sessie?';
    rsConfirmClosingWizard='U heeft data gewijzigd zonder deze eerst op te slaan.  Bent u zeker dat u de Wizard toch wil sluiten?';
    rsNotAppliable='Niet van toepassing';

  // Organisation types permitted in Session Wizard
  const
    ORG_IS_SCHOOL=4;
    ORG_IS_COMPANY=10;

  type
    OnClose=procedure of object;
    OnOpen=procedure of object;

type
  TdtmSessionWizard = class(TDataModule)
    ssckData: TFVBFFCSharedConnection;
    cdsList: TFVBFFCClientDataSet;
    cdsListF_ENROL_ID: TIntegerField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_PERSON_ID: TIntegerField;
    cdsListF_PERSON_NAME: TStringField;
    cdsListF_PERSON_ADDRESS: TStringField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_ORGANISATION: TStringField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_STATUS_NAME: TStringField;
    cdsListF_PRICING_SCHEME: TIntegerField;
    cdsListF_SELECTED_PRICE: TFloatField;
    cdsListF_ROLEGROUP_NAME: TStringField;
    cdsRecord: TFVBFFCClientDataSet;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsSearchOrganisation: TFVBFFCClientDataSet;
    IntegerField2: TIntegerField;
    StringField9: TStringField;
    StringField13: TStringField;
    cdsOrganisation: TFVBFFCClientDataSet;
    cdsOrganisationF_ORGANISATION_ID: TIntegerField;
    cdsOrganisationF_STREET: TStringField;
    cdsOrganisationF_NUMBER: TStringField;
    cdsOrganisationF_MAILBOX: TStringField;
    cdsOrganisationF_POSTALCODE_ID: TIntegerField;
    cdsOrganisationF_COUNTRY_ID: TIntegerField;
    cdsOrganisationF_ORGTYPE_ID: TIntegerField;
    cdsOrganisationF_CONTACT_DATA: TStringField;
    cdsOrganisationF_CONFIRMATION_CONTACT1: TStringField;
    cdsOrganisationF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    cdsOrganisationF_CONFIRMATION_CONTACT2: TStringField;
    cdsOrganisationF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    cdsOrganisationF_ORGANISATION: TStringField;
    cdsOrganisationF_POSTALCODE: TStringField;
    cdsOrganisationF_COUNTRY_NAME: TStringField;
    cdsOrganisationF_ORGTYPE_NAME: TStringField;
    cdsOrganisationF_CITY_NAME: TStringField;
    cdsRoles: TFVBFFCClientDataSet;
    cdsRolesF_ROLE_ID: TIntegerField;
    cdsRolesF_PERSON_ID: TIntegerField;
    cdsRolesF_FIRSTNAME: TStringField;
    cdsRolesF_LASTNAME: TStringField;
    cdsRolesF_ORGANISATION_ID: TIntegerField;
    cdsRolesF_NAME: TStringField;
    cdsRolesF_ROLEGROUP_ID: TIntegerField;
    cdsRolesF_ROLEGROUP_NAME: TStringField;
    cdsRolesF_END_DATE: TDateTimeField;
    srcOrganisation: TFVBFFCDataSource;
    cdsRolesF_STREET: TStringField;
    cdsRolesF_NUMBER: TStringField;
    cdsRolesF_MAILBOX: TStringField;
    cdsRolesF_POSTALCODE: TStringField;
    cdsRolesF_POSTALCODE_ID: TIntegerField;
    cdsRolesF_CITY_NAME: TStringField;
    cdsRolesF_COUNTRY_ID: TIntegerField;
    cdsRolesF_COUNTRY_NAME: TStringField;
    cdsRolesF_GENDER_ID: TIntegerField;
    cdsRolesF_GENDER_NAME: TStringField;
    cdsRolesF_DATEBIRTH: TDateTimeField;
    cdsOrganisationF_ACTIVE: TBooleanField;
    cdsOrganisationF_MANUALLY_ADDED: TBooleanField;
    cdsSearchOrganisationF_POSTALCODE: TStringField;
    cdsLookupPerson: TFVBFFCClientDataSet;
    cdsLookupPersonF_PERSON_ID: TIntegerField;
    cdsLookupPersonF_SOCSEC_NR: TStringField;
    cdsLookupPersonF_LASTNAME: TStringField;
    cdsLookupPersonF_FIRSTNAME: TStringField;
    cdsLookupPersonF_PERSON_ADDRESS: TStringField;
    cdsLookupPersonF_ROLE_ID: TIntegerField;
    cdsLookupPersonF_ORGANISATION_NAME: TStringField;
    cdsLookupPersonF_ROLEGROUP_NAME: TStringField;
    cdsLookupPersonF_DATEBIRTH: TDateTimeField;
    cdsLookupPersonF_GENDER_NAME: TStringField;
    cdsLookupPersonF_GENDER_ID: TIntegerField;
    cdsLookupPersonF_ROLEGROUP_ID: TIntegerField;
    cdsLookupPersonF_STREET: TStringField;
    cdsLookupPersonF_NUMBER: TStringField;
    cdsLookupPersonF_MAILBOX: TStringField;
    cdsLookupPersonF_POSTALCODE_ID: TIntegerField;
    cdsLookupPersonF_POSTALCODE: TStringField;
    cdsLookupPersonF_CITY_NAME: TStringField;
    cdsLookupPersonF_COUNTRY_NAME: TStringField;
    cdsLookupPersonF_COUNTRY_ID: TIntegerField;
    cdsRolesF_START_DATE: TDateTimeField;
    cdsSearchOrganisationF_ORGTYPE_ID: TIntegerField;
    cdsSearchOrganisationF_ORGTYPE_NAME: TStringField;
    cdsListF_CONSTRUCT: TBooleanField;
    cdsRolesF_DIPLOMA_ID: TIntegerField;
    cdsRolesF_DIPLOMA_NAME: TStringField;
    cdsLookupPersonF_DIPLOMA_ID: TIntegerField;
    cdsLookupPersonF_DIPLOMA_NAME: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure srcOrganisationDataChange(Sender: TObject; Field: TField);

    procedure OnReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsOrganisationNewRecord(DataSet: TDataSet);
    procedure cdsOrganisationBeforePost(DataSet: TDataSet);
    procedure cdsRolesNewRecord(DataSet: TDataSet);
    procedure cdsRolesBeforePost(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
    procedure cdsRolesAfterClose(DataSet: TDataSet);
    procedure cdsRolesAfterOpen(DataSet: TDataSet);
    procedure cdsRolesAfterInsert(DataSet: TDataSet);
  private
    FSessionID: Integer;
    FOnCloseRoles: OnClose;
    FOnOpenRoles: OnOpen;

    { Private declarations }
    function GetNewOrganisationID: Integer;
    function GetNewRoleID: Integer;

  public
    { Public declarations }
    procedure RetrieveEnrolmentsForSession(const ASessionId: Integer);
    procedure RetrieveOrganisation;
    procedure SelectPostalCode(AIsOrganisation: Boolean; AFilter: Boolean=False);
    procedure SelectCountry(AIsOrganisation: Boolean);
    procedure SelectOrgType;
    procedure SelectGender;
    //procedure SelectLanguage;
    procedure SelectDiploma;
    procedure SelectRoleGroup;

    procedure AddNewOrg;
    procedure AddNewRole;
    procedure SaveOrganisation;
    procedure SaveRole;
    procedure CancelModificationsOrganisation;
    procedure CancelModificationsRole;
    function EnrolPerson: Integer;
    procedure RemoveEnrolment;
    procedure RemovePostalcode( AFilter: Boolean );
    function LookupPerson: Boolean;

    property SessionID: Integer read FSessionID write FSessionID;
    property OnCloseRoles: OnClose read FOnCloseRoles write FOnCloseRoles;
    property OnOpenRoles: OnOpen read FOnOpenRoles write FOnOpenRoles;
    function ModifiedData: Boolean;
  end;

var
  dtmSessionWizard: TdtmSessionWizard;

implementation

{$R *.dfm}

uses Forms, Dialogs, Data_Postalcode, Data_Country, Data_Gender, Data_Language, Data_OrganisationType, Data_RoleGroup, Data_Diploma, Unit_PPWFrameWorkClasses, Form_LookupPerson;

{ TdtmSessionWizard }

// Private

procedure TdtmSessionWizard.DataModuleCreate(Sender: TObject);
begin
  cdsSearchOrganisation.CreateDataSet;

end;

function TdtmSessionWizard.GetNewOrganisationID: Integer;
begin
  Result := ssckData.AppServer.GetNewOrganisationID;

end;

function TdtmSessionWizard.GetNewRoleID: Integer;
begin
  Result := ssckData.AppServer.GetNewRoleID;

end;

/////////////////////////////////////////////////////////////////////////////////
// Dialog boxes
procedure TdtmSessionWizard.SelectCountry(AIsOrganisation: Boolean);
var
  aDataSet: TDataSet;
begin
  if ( AIsOrganisation ) then // Working on Organisation dataset?
    aDataSet := cdsOrganisation
  else
    aDataSet := cdsRoles;

  if ( aDataSet.Active ) and ( ( ( cdsOrganisation.Active ) and ( cdsOrganisation.RecordCount > 0 )
                                and ( not AIsOrganisation ) ) or (AIsOrganisation)) then  // At least one organisation should be visible when user tries to modify role
    TdtmCountry.SelectCountry( aDataSet );

end;

procedure TdtmSessionWizard.SelectGender;
begin
  if ( cdsRoles.Active ) and ( cdsOrganisation.Active ) and ( cdsOrganisation.RecordCount > 0 ) then // At least one organisation should be visible
    TdtmGender.SelectGender( cdsRoles );

end;

{
procedure TdtmSessionWizard.SelectLanguage;
begin
  if ( cdsRoles.Active ) and ( cdsOrganisation.Active ) and ( cdsOrganisation.RecordCount > 0 ) then // At least one organisation should be visible
    TdtmLanguage.SelectLanguage( cdsRoles );

end;
}

procedure TdtmSessionWizard.SelectDiploma;
begin
  if ( cdsRoles.Active ) and ( cdsOrganisation.Active ) and ( cdsOrganisation.RecordCount > 0 ) then // At least one organisation should be visible
    TdtmDiploma.SelectDiploma( cdsRoles );

end;


procedure TdtmSessionWizard.SelectOrgType;
begin
  if ( cdsOrganisation.Active ) then
    TdtmOrganisationType.SelectOrganisationType( cdsOrganisation, 'F_ORGTYPE_ID in (4, 10)' ); // Only company and school is allowed to select

end;

procedure TdtmSessionWizard.SelectPostalCode(AIsOrganisation: Boolean; AFilter: Boolean=False);
var
  aDataSet: TDataSet;
begin
  if ( AFilter ) then // Filter modus?
    aDataSet := cdsSearchOrganisation
  else if ( AIsOrganisation ) then  // Working on Organisation dataset?
    aDataSet := cdsOrganisation
  else
    aDataSet := cdsRoles;

  if ( aDataSet.Active ) and ( ( ( cdsOrganisation.Active )
                                  and ( cdsOrganisation.RecordCount > 0 )
                                  and ( not AIsOrganisation ) ) or (AIsOrganisation) or (AFilter) ) then  // At least one organisation should be visible when user tries to modify role
    TdtmPostalcode.SelectPostalCode( aDataSet );

end;

procedure TdtmSessionWizard.SelectRoleGroup;
var
  AWhere: String;
begin
  if ( cdsRoles.Active ) and ( cdsOrganisation.Active ) and ( cdsOrganisation.RecordCount > 0 ) then // At least one organisation should be visible
  begin
    if cdsOrganisation.FieldByName('F_ORGTYPE_ID').AsInteger = ORG_IS_SCHOOL then
      AWhere := 'F_ROLEGROUP_ID in ( 11, 20, 18)'  // Only students, teachers, overig roles allowed (Mantis 4544: overig should be available as well)
    else
      AWhere := 'F_ROLEGROUP_ID in ( 2, 3, 16, 18 )'; // Arbeider PC 124, Bediende PC 218, IBO, overig

    // Determine type of selected organisation
    TdtmRoleGroup.SelectRoleGroup( cdsRoles, AWhere );

  end;
end;

procedure TdtmSessionWizard.srcOrganisationDataChange(Sender: TObject;
  Field: TField);
begin
  // Lookup roles for selected organisation id
  if ( cdsOrganisation.Active ) and ( cdsOrganisation.State in [dsBrowse] ) then
  begin
        cdsRoles.DisableControls;
        try
          cdsRoles.Close;
          if not cdsOrganisation.FieldByName('F_ORGANISATION_ID').IsNull then
          begin
            cdsRoles.Params.ParamByName('OrganisationID').Value := cdsOrganisation.FieldByName('F_ORGANISATION_ID').AsInteger;
            cdsRoles.Open;
          end;
        finally
          cdsRoles.EnableControls;
        end;
  end;

end;

// Retrieve enrolments for selected session
procedure TdtmSessionWizard.RetrieveEnrolmentsForSession(
  const ASessionId: Integer);
begin
  cdsList.DisableControls;
  try
     cdsList.Close;
     cdsList.Params.ParamByName('SessionId').Value := ASessionId;
     cdsList.Open;
  finally
    cdsList.EnableControls;
  end;
end;

// Search for organisation using conditions
procedure TdtmSessionWizard.RetrieveOrganisation;
var
  AOrganisation: String;
  ACity: String;
  APostalcodeId: Integer;
begin
  Assert( cdsSearchOrganisation.Active );

  if cdsSearchOrganisation.FieldByName('F_POSTALCODE_ID').IsNull then
    APostalcodeId := 0
  else
    APostalcodeId := cdsSearchOrganisation.FieldByName('F_POSTALCODE_ID').AsInteger;

  if not cdsSearchOrganisation.FieldByName('F_CITY_NAME').IsNull then
    ACity := cdsSearchOrganisation.FieldByName('F_CITY_NAME').AsString;

  if not cdsSearchOrganisation.FieldByName('F_ORGANISATION').IsNull then
    AOrganisation := cdsSearchOrganisation.FieldByName('F_ORGANISATION').AsString;

 // if not cdsSearchOrganisation.FieldByName('F_ORGTYPE_ID').IsNull then
 // AOrgtypeId := cdsSearchOrganisation.FieldByName('F_ORGTYPE_ID').AsInteger;

  cdsOrganisation.DisableControls;
  try
    cdsOrganisation.Close;
    cdsOrganisation.Params.ParamByName('Organisation').Value := AOrganisation;
    cdsOrganisation.Params.ParamByName('City').Value := ACity;
    cdsOrganisation.Params.ParamByName('Postalcode1').Value := APostalCodeId;
    cdsOrganisation.Params.ParamByName('Postalcode2').Value := APostalCodeId;
    cdsOrganisation.Params.ParamByName('Postalcode3').Value := APostalCodeId;
   { cdsOrganisation.Params.ParamByName('OrgtypeId').Value := AOrgtypeId;
    cdsOrganisation.Params.ParamByName('OrgtypeId2').Value := AOrgtypeId;
   }
    cdsOrganisation.Open;
  finally
    cdsOrganisation.EnableControls;
  end;

end;

procedure TdtmSessionWizard.SaveOrganisation;
begin
    cdsOrganisation.DisableControls;
    try
      if cdsOrganisation.State in [dsInsert, dsEdit] then
        cdsOrganisation.Post;

      if cdsOrganisation.ChangeCount > 0 then
        cdsOrganisation.ApplyUpdates(0);
    finally
        cdsOrganisation.EnableControls;
    end;

end;

procedure TdtmSessionWizard.SaveRole;
begin
  cdsRoles.DisableControls;
  try
    if cdsRoles.State in [dsInsert, dsEdit] then
      cdsRoles.Post;

    if cdsRoles.ChangeCount > 0 then
    begin
      cdsRoles.ApplyUpdates( 0 );
      cdsRoles.Last;
    end;
  finally
    cdsRoles.EnableControls;
  end;

end;


procedure TdtmSessionWizard.CancelModificationsOrganisation;
begin
  cdsOrganisation.Cancel;

end;

procedure TdtmSessionWizard.CancelModificationsRole;
begin
  cdsRoles.Cancel;

end;

procedure TdtmSessionWizard.AddNewOrg;
begin
  cdsOrganisation.Append;

end;

procedure TdtmSessionWizard.AddNewRole;
begin
  cdsRoles.Append;

end;


procedure TdtmSessionWizard.OnReconcileError(DataSet: TCustomClientDataSet;
  E: EReconcileError; UpdateKind: TUpdateKind;
  var Action: TReconcileAction);
begin
  DataSet.CancelUpdates;
  Action := raCancel;
  raise Exception.Create(Format(rsUnableToUpdateDB, [E.Message]));

end;

procedure TdtmSessionWizard.cdsOrganisationNewRecord(DataSet: TDataSet);
begin
  // Initialize some fields for new Organisation
  DataSet.FieldByName('F_ORGANISATION_ID').Value := GetNewOrganisationID;
  DataSet.FieldByName('F_ACTIVE').Value := 1;
  DataSet.FieldByName('F_MANUALLY_ADDED').Value := 1;

end;

procedure TdtmSessionWizard.cdsRolesNewRecord(DataSet: TDataSet);
begin
  // Initialize some fields
  DataSet.FieldByName('F_ROLE_ID').Value := GetNewRoleID;

  DataSet.FieldByName('F_COUNTRY_ID').Value := 1; // Belgium
  DataSet.FieldByName('F_COUNTRY_NAME').Value := 'Belgie';

  DataSet.FieldByName('F_PERSON_ID').Value := 0; // TODO --- nakijken

end;

procedure TdtmSessionWizard.cdsOrganisationBeforePost(DataSet: TDataSet);
begin
  // Validation
  if DataSet.FieldByName( 'F_STREET' ).IsNull or
    ( Trim(DataSet.FieldByName( 'F_STREET' ).AsString) = '' ) then
        raise Exception.Create( rsOrganisationStreetEmpty );

  if DataSet.FieldByName( 'F_ORGANISATION' ).IsNull  or
    ( Trim(DataSet.FieldByName( 'F_ORGANISATION' ).AsString) = '' ) then
        raise Exception.Create( rsOrganisationNameEmpty );

  if DataSet.FieldByName( 'F_NUMBER' ).IsNull  or
    ( Trim(DataSet.FieldByName( 'F_NUMBER' ).AsString) = '' ) then
        raise Exception.Create( rsOrganisationNumberEmpty );

  if DataSet.FieldByName( 'F_ORGTYPE_ID' ).IsNull  or
    ( Trim(DataSet.FieldByName( 'F_NUMBER' ).AsString) = '' ) then
        raise Exception.Create( rsOrganisationTypeEmpty );

end;

procedure TdtmSessionWizard.cdsRolesBeforePost(DataSet: TDataSet);
begin
  // Validation
  if DataSet.FieldByName('F_LASTNAME').IsNull or
    ( Trim(DataSet.FieldByName('F_LASTNAME').AsString) = '' ) then
      raise Exception.Create( rsRoleLastname );

  if DataSet.FieldByName('F_FIRSTNAME').IsNull or
    ( Trim(DataSet.FieldByName('F_FIRSTNAME').AsString) = '' ) then
      raise Exception.Create( rsRoleFirstname );

  if DataSet.FieldByName('F_START_DATE').IsNull then
    raise Exception.Create( rsRoleStartDate );

  if DataSet.FieldByName('F_ROLEGROUP_ID').IsNull then
    raise Exception.Create( rsRoleRolegroup );

end;

procedure TdtmSessionWizard.cdsListAfterPost(DataSet: TDataSet);
begin
  if cdsList.ChangeCount > 0 then
    cdsList.ApplyUpdates( 0 ); // Apply immediately the changes to the database

end;

procedure TdtmSessionWizard.cdsRolesAfterClose(DataSet: TDataSet);
begin
  if Assigned ( FOnCloseRoles ) then
    FOnCloseRoles;

end;

procedure TdtmSessionWizard.cdsRolesAfterOpen(DataSet: TDataSet);
begin
  if Assigned ( FOnOpenRoles ) then
    FOnOpenRoles;

end;

function TdtmSessionWizard.EnrolPerson: Integer;
begin
  Result := 0;

  if ( cdsRoles.Active ) and ( cdsRoles.RecordCount > 0 ) then // At least one role should be available
  begin
    SaveRole;
    Result := ssckData.AppServer.EnrolPerson( SessionID,
                                              cdsRoles.FieldByName('F_ROLE_ID').AsInteger,
                                              cdsRoles.FieldByName('F_ROLEGROUP_ID').AsInteger );
  end;

end;

procedure TdtmSessionWizard.RemoveEnrolment;
begin
  if ( cdsList.Active ) and ( cdsList.RecordCount > 0 ) then // At least one enrolment should be available
  begin
    ssckData.AppServer.RemoveEnrolment( cdsList.FieldByName('F_ENROL_ID').AsInteger );
  end;

end;

procedure TdtmSessionWizard.RemovePostalcode(AFilter: Boolean);
var
  aDataSet: TClientDataSet;
begin
  if AFilter then // Currently filtering?
      aDataSet := cdsSearchOrganisation
  else
      aDataSet := cdsOrganisation;

  if ( aDataSet.Active ) and ( (not AFilter) and (aDataSet.RecordCount > 0) or (AFilter) ) then // When in edit modus, dataset should contain at least one record
  begin
      if not (aDataSet.State in [dsInsert, dsEdit]) then
        aDataSet.Edit;

      aDataSet.FieldByName('F_POSTALCODE').Value := '';
      aDataSet.FieldByName('F_POSTALCODE_ID').Value := 0;
      aDataSet.FieldByName('F_CITY_NAME').Value := '';
  end;

end;

// Search for person with the same name and display form when found so user can select existing person.
function TdtmSessionWizard.LookupPerson: Boolean;
var
  oldcursor: TCursor;
begin
  Result := False;

  // To lookup a person we want to make sure that both firstname and lastname are not empty
  if ( cdsRoles.Active ) and ( cdsRoles.State in [dsInsert, dsEdit] ) then
  begin
    if (not cdsRoles.FieldByName('F_FIRSTNAME').IsNull) and (Trim(cdsRoles.FieldByName('F_FIRSTNAME').AsString) <> '') then
    begin
      if (not cdsRoles.FieldByName('F_LASTNAME').IsNull) and (Trim(cdsRoles.FieldByName('F_LASTNAME').AsString) <> '') then
      begin
        cdsLookupPerson.Close;
        cdsLookupPerson.Params.ParamByName('Lastname').Value := cdsRoles.FieldByName('F_LASTNAME').AsString;
        cdsLookupPerson.Params.ParamByName('Firstname').Value := cdsRoles.FieldByName('F_FIRSTNAME').AsString;
        cdsLookupPerson.Open;
        try
          oldcursor := Screen.Cursor;
          try
            Screen.Cursor := crSQLWait;
            cdsLookupPerson.Open;
          finally
            Screen.Cursor := oldcursor;
          end;

          if cdsLookupPerson.RecordCount > 0 then
          begin

            with Tfrm_LookupPerson.Create(nil) do
            begin
                try
                    FirstName := cdsRoles.FieldByName('F_FIRSTNAME').AsString;
                    LastName := cdsRoles.FieldByName('F_LASTNAME').AsString;
                    if ShowModal=mrOK then // Existing person selected?
                      GetPerson( cdsRoles );  // Get person' data --> result will be stored in cdsRoles.

                finally
                  Free;
                end;
            end;

        end;

        finally
          cdsLookupPerson.Close;
        end;
      end;
    end;
  end;

end;


// Checks whether some data has been changed or not
function TdtmSessionWizard.ModifiedData: Boolean;
begin
  Result := False;

  if ( cdsList.Active ) and ( ( cdsList.State in [dsInsert, dsEdit] ) or ( cdsList.ChangeCount > 0 ) ) then
    Result := True;

  if ( cdsOrganisation.Active ) and ( ( cdsOrganisation.State in [dsInsert, dsEdit] ) or ( cdsOrganisation.ChangeCount > 0 ) ) then
    Result := True;

  if ( cdsRoles.Active ) and ( ( cdsRoles.State in [dsInsert, dsEdit] ) or ( cdsRoles.ChangeCount > 0 ) ) then
    Result := True;

end;

procedure TdtmSessionWizard.cdsRolesAfterInsert(DataSet: TDataSet);
begin
  // ***************************************************************
  DataSet.FieldByName('F_GENDER_ID').Value := 1; // Set gender to man by default. (Mantis 2196).
  DataSet.FieldByName('F_DIPLOMA_ID').Value := 0; // Set Education default to 'Niet van toepassing'
  DataSet.FieldByName('F_DIPLOMA_NAME').Value := rsNotAppliable;
end;

end.
