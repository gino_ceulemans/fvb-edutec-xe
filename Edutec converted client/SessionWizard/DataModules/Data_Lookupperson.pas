unit Data_Lookupperson;

interface

uses
  SysUtils, Classes, Data_EDUDataModule, Data_EduMainClient, DB, DBClient, MConnect,
  Unit_FVBFFCDBComponents, Unit_PPWFrameWorkComponents;

type
  TdmLookupPerson = class(TDataModule)
    cdsLookupPerson: TFVBFFCClientDataSet;
    cdsLookupPersonF_PERSON_ID: TIntegerField;
    cdsLookupPersonF_SOCSEC_NR: TStringField;
    cdsLookupPersonF_LASTNAME: TStringField;
    cdsLookupPersonF_FIRSTNAME: TStringField;
    cdsLookupPersonF_PERSON_ADDRESS: TStringField;
    cdsLookupPersonF_ROLE_ID: TIntegerField;
    cdsLookupPersonF_ORGANISATION_NAME: TStringField;
    cdsLookupPersonF_ROLEGROUP_NAME: TStringField;
    ssckData: TFVBFFCSharedConnection;
    cdsLookupPersonF_LANGUAGE_ID: TIntegerField;
    cdsLookupPersonF_LANGUAGE_NAME: TStringField;
    cdsLookupPersonF_DATEBIRTH: TDateTimeField;
    cdsLookupPersonF_GENDER_NAME: TStringField;
    cdsLookupPersonF_GENDER_ID: TIntegerField;
    cdsLookupPersonF_ROLEGROUP_ID: TIntegerField;
    cdsLookupPersonF_STREET: TStringField;
    cdsLookupPersonF_NUMBER: TStringField;
    cdsLookupPersonF_MAILBOX: TStringField;
    cdsLookupPersonF_POSTALCODE_ID: TIntegerField;
    cdsLookupPersonF_POSTALCODE: TStringField;
    cdsLookupPersonF_CITY_NAME: TStringField;
    cdsLookupPersonF_COUNTRY_NAME: TStringField;
    cdsLookupPersonF_COUNTRY_ID: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LookupPerson( const ALastName, AFirstName: String );
  end;

var
  dmLookupPerson: TdmLookupPerson;

implementation

{$R *.dfm}

{ TdmLookupPerson }

procedure TdmLookupPerson.LookupPerson( const ALastName, AFirstName: String );
begin
  cdsLookupPerson.DisableControls;
  try
    cdsLookupPerson.Close;
    cdsLookupPerson.Params.ParamByName('Lastname').Value := ALastName;
    cdsLookupPerson.Params.ParamByName('Firstname').Value := AFirstName;
    cdsLookupPerson.Open;
  finally
    cdsLookupPerson.EnableControls;
  end;

end;

end.
