inherited frmProgProgram_List: TfrmProgProgram_List
  Left = 224
  Top = 0
  Width = 560
  Height = 480
  ActiveControl = cxdbseF_PROGRAM_ID
  Caption = 'Opleidingsaanbod'
  Constraints.MinHeight = 480
  Constraints.MinWidth = 560
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 412
    Width = 552
  end
  inherited pnlList: TFVBFFCPanel
    Width = 544
    Height = 404
    inherited cxgrdList: TcxGrid
      Top = 190
      Width = 544
      Height = 214
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_ID'
          Width = 147
        end
        object cxgrdtblvListF_PROGRAM_TYPE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_TYPE_NAME'
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 200
        end
        object cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_NAME'
          Width = 200
        end
        object cxgrdtblvListF_PROFESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROFESSION_NAME'
          Width = 200
        end
        object cxgrdtblvListF_MAX_PUNTEN: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAX_PUNTEN'
        end
        object cxgrdtblvListF_DURATION: TcxGridDBColumn
          DataBinding.FieldName = 'F_DURATION'
        end
        object cxgrdtblvListF_DURATION_DAYS: TcxGridDBColumn
          DataBinding.FieldName = 'F_DURATION_DAYS'
          Width = 132
        end
        object cxgrdtblvListF_SHORT_DESC: TcxGridDBColumn
          DataBinding.FieldName = 'F_SHORT_DESC'
          Width = 200
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Width = 75
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 165
      Width = 544
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 161
      Width = 544
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 544
      Height = 161
      ExpandedHeight = 161
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 130
        Width = 542
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 374
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 462
        end
      end
      object cxlblF_PROGRAM_ID2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Programma ( ID )'
        FocusControl = cxdbseF_PROGRAM_ID
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbseF_PROGRAM_ID: TFVBFFCDBSpinEdit
        Left = 128
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_ID'
        DataBinding.DataField = 'F_PROGRAM_ID'
        DataBinding.DataSource = srcSearchCriteria
        Properties.ReadOnly = False
        TabOrder = 2
        Width = 120
      end
      object cxlblF_NAME2: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Programma'
        FocusControl = cxdbteF_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME: TFVBFFCDBTextEdit
        Left = 128
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_NAME'
        DataBinding.DataField = 'F_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 5
        Width = 400
      end
      object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Taal'
        FocusControl = cxdbbeF_LANGUAGE_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
        Left = 128
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_LANGUAGE_NAME'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
        TabOrder = 7
        Width = 400
      end
      object cxlblF_PROFESSION_NAME1: TFVBFFCLabel
        Left = 8
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_PROFESSION_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Discipline'
        FocusControl = cxdbbeF_PROFESSION_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_PROFESSION_NAME: TFVBFFCDBButtonEdit
        Left = 128
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmProgProgram_Record.F_PROFESSION_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_PROFESSION_NAME'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_PROFESSION_NAMEPropertiesButtonClick
        TabOrder = 9
        Width = 400
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 256
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 328
        Top = 32
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Items = <>
        TabOrder = 4
        Width = 200
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 552
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 408
    Width = 552
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 548
    Height = 404
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 404
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 296
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    Left = 216
    Top = 296
  end
end
