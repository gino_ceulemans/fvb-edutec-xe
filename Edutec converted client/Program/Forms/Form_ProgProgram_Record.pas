{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_PROG_PROGRAM record.

  @Name       Form_ProgProgram_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2008   ivdbossche           Enabled Active field (Mantis 3138)
  11/09/2006   sLesage              Added the necessary fields for Boetes and
                                    Tussenkomsten.
  10/11/2005   sLesage              Added and EditBox which allows the user to
                                    modify the Construct Eduction ID for a
                                    Program.
  26/07/2005   sLesage              Added functionality to show all Instructors
                                    to which the Program  is Linked.
  07/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_ProgProgram_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxControls,
  cxContainer, cxEdit, cxLabel, cxDropDownEdit, cxCalc, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxImageComboBox, cxMemo, cxCalendar,
  cxButtonEdit, cxSpinEdit, cxCurrencyEdit, Unit_PPWFrameWorkInterfaces,
  Menus, cxGraphics, cxSplitter, cxGroupBox, cxCheckBox;

type
  TfrmProgProgram_Record = class(TEDURecordView)
    cxlblF_PROGRAM_ID1: TFVBFFCLabel;
    cxdblblF_PROGRAM_ID1: TFVBFFCDBLabel;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    pnlPriceSchemeInfoSeperator: TFVBFFCPanel;
    cxlblF_PROGRAM_ID2: TFVBFFCLabel;
    cxdbseF_PROGRAM_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel;
    cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit;
    cxlblF_DURATION1: TFVBFFCLabel;
    cxdbseF_DURATION: TFVBFFCDBSpinEdit;
    cxlblF_DURATION_DAYS1: TFVBFFCLabel;
    cxdbseF_DURATION_DAYS: TFVBFFCDBSpinEdit;
    cxlblF_MINIMUM1: TFVBFFCLabel;
    cxdbseF_MINIMUM: TFVBFFCDBSpinEdit;
    cxlblF_MAXIMUM1: TFVBFFCLabel;
    cxdbseF_MAXIMUM: TFVBFFCDBSpinEdit;
    cxlblF_SHORT_DESC1: TFVBFFCLabel;
    cxdbteF_SHORT_DESC: TFVBFFCDBTextEdit;
    cxlblF_LONG_DESC1: TFVBFFCLabel;
    cxdbmmoF_LONG_DESC: TFVBFFCDBMemo;
    cxlblF_ENROLMENT_DATE1: TFVBFFCLabel;
    cxdbseF_ENROLMENT_DATE: TFVBFFCDBSpinEdit;
    cxlblF_CONTROL_DATE1: TFVBFFCLabel;
    cxdbseF_CONTROL_DATE: TFVBFFCDBSpinEdit;
    cxlblF_PROFESSION_NAME1: TFVBFFCLabel;
    cxdbbeF_PROFESSION_NAME: TFVBFFCDBButtonEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_COMMENT1: TFVBFFCLabel;
    cxdbmmoF_COMMENT: TFVBFFCDBMemo;
    cxlblF_ACTIVE1: TFVBFFCLabel;
    csdbcbF_ACTIVE: TFVBFFCDBCheckBox;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE: TFVBFFCDBDateEdit;
    sbxAdditionalInfo: TScrollBox;
    cxdbmmoInfrastructure: TFVBFFCDBMemo;
    cxlblVeilidgheidsuitrusting: TFVBFFCLabel;
    cxlblGereedschap: TFVBFFCLabel;
    cxdbmmoInstructor: TFVBFFCDBMemo;
    cxlblDidactischeHulpmiddelen: TFVBFFCLabel;
    cxdbmmoOrganisation: TFVBFFCDBMemo;
    acShowAdditionalInfo: TAction;
    dxnbiBijkomendeInfo: TdxNavBarItem;
    acShowSessions: TAction;
    dxnbiSessies: TdxNavBarItem;
    acShowInstructors: TAction;
    dxnbiInstructors: TdxNavBarItem;
    cxlblF_CODE1: TFVBFFCLabel;
    cxdbteF_CODE1: TFVBFFCDBTextEdit;
    acShowHistory: TAction;
    dxnbiShowHistory: TdxNavBarItem;
    cxlblF_EDUCATION_ID1: TFVBFFCLabel;
    cxlblF_TK_FVB2: TFVBFFCLabel;
    F_TK_FVB1: TFVBFFCDBCurrencyEdit;
    cxlblF_TK_CEVORA2: TFVBFFCLabel;
    F_TK_CEVORA1: TFVBFFCDBCurrencyEdit;
    cxlblF_BT_FVB2: TFVBFFCLabel;
    F_BT_FVB1: TFVBFFCDBCurrencyEdit;
    cxlblF_BT_CEVORA2: TFVBFFCLabel;
    F_BT_CEVORA1: TFVBFFCDBCurrencyEdit;
    cxlblF_START_HOUR1: TFVBFFCLabel;
    cxdbteF_START_HOUR: TFVBFFCDBTextEdit;
    cxlblF_PRICE_WORKER: TFVBFFCLabel;
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    cxlblF_PRICE_FIXED1: TFVBFFCLabel;
    FVBFFCDBCurrencyEdit1: TFVBFFCDBCurrencyEdit;
    FVBFFCDBCurrencyEdit2: TFVBFFCDBCurrencyEdit;
    FVBFFCDBCurrencyEdit3: TFVBFFCDBCurrencyEdit;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    cxdbbeF_PROGRAM_TYPE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_PROGRAM_TYPE: TFVBFFCLabel;
    cxlblF_MAX_PUNTEN: TFVBFFCLabel;
    cxdbseF_MAX_PUNTEN: TFVBFFCDBSpinEdit;
    procedure cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_PROGRAM_TYPE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_PROFESSION_NAME1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_COMPLETE_USERNAME1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure acShowAdditionalInfoExecute(Sender: TObject);
    procedure acShowRecordDetailExecute(Sender: TObject);
    procedure acShowSessionsExecute(Sender: TObject);
    procedure acShowInstructorsExecute(Sender: TObject);
    procedure acShowHistoryExecute(Sender: TObject);
    procedure srcMainDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_ProgProgram, Data_EDUMainClient, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, unit_EdutecInterfaces,
  Form_FVBFFCBaseRecordView;

//  resourcestring
//  rsRecalculate = 'Huidige basisprijs verschilt van berekende basisprijs,' +
//                  ' wilt u de huidige basisprijs herrekenen ?';

{ TfrmProgProgram_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmProgProgram_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmProgProgram_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption;
end;

{*****************************************************************************
  This method will be executed when the user clicks one of the Buttons in the
  ButtonEdit.  Depending on the clicked button it will execute the ShowXXX or
  SelectXXX methods.

  @Name       TfrmProgProgram_Record.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProgProgram_Record.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProgProgramDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUProgProgramDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectLanguage( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmProgProgram_Record.cxdbbeF_PROGRAM_TYPE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProgProgramDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUProgProgramDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectProgramType( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks one of the Buttons in the
  ButtonEdit.  Depending on the clicked button it will execute the ShowXXX or
  SelectXXX methods.

  @Name       TfrmProgProgram_Record.cxdbbeF_PROFESSION_NAME1PropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProgProgram_Record.cxdbbeF_PROFESSION_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProgProgramDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUProgProgramDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowDiscipline( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectDiscipline( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks one of the Buttons in the
  ButtonEdit.  Depending on the clicked button it will execute the ShowXXX or
  SelectXXX methods.

  @Name       TfrmProgProgram_Record.cxdbbeF_COMPLETE_USERNAME1PropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProgProgram_Record.cxdbbeF_COMPLETE_USERNAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProgProgramDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUProgProgramDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowUser( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectUser( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmProgProgram_Record.acShowAdditionalInfoExecute(
  Sender: TObject);
begin
  ShowRecordDetail( Sender );
  sbxMain.Visible := False;
  inherited;
  sbxAdditionalInfo.Visible := True;
end;

procedure TfrmProgProgram_Record.acShowRecordDetailExecute(
  Sender: TObject);
begin
  sbxAdditionalInfo.Visible := False;
  inherited;
  sbxMain.Visible := True;
end;

procedure TfrmProgProgram_Record.acShowSessionsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Sender, 'TdtmSesSession', pnlRecordDetail );
end;

procedure TfrmProgProgram_Record.acShowInstructorsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Sender, 'TdtmProgInstructor', pnlRecordDetail );
end;

procedure TfrmProgProgram_Record.acShowHistoryExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Sender, 'TdtmLogHistory', pnlRecordDetail );
end;

procedure TfrmProgProgram_Record.srcMainDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Field <> nil then
  begin
    if Field.FieldName = 'F_PROGRAM_TYPE' then
    begin
      if Field.AsInteger = 1 then
      begin
        srcMain.DataSet.FieldByName('F_DURATION').Required := True;
        srcMain.DataSet.FieldByName('F_DURATION_DAYS').Required := True;
        srcMain.DataSet.FieldByName('F_MAX_PUNTEN').Required := False;
      end else
      begin
        srcMain.DataSet.FieldByName('F_DURATION').Required := False;
        srcMain.DataSet.FieldByName('F_DURATION_DAYS').Required := False;
        srcMain.DataSet.FieldByName('F_MAX_PUNTEN').Required := True;
      end;
    end;
  end;
end;

end.
