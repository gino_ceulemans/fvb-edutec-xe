{*****************************************************************************
  This unit contains the form that will be used to display a list of
  T_PROG_PROGRAM records.

  @Name       Form_ProgProgram_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  07/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_ProgProgram_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxButtonEdit, cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit, cxContainer,
  cxLabel, Menus, cxDropDownEdit, cxImageComboBox;

type
  TfrmProgProgram_List = class(TEDUListView)
    cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_DURATION: TcxGridDBColumn;
    cxgrdtblvListF_DURATION_DAYS: TcxGridDBColumn;
    cxgrdtblvListF_SHORT_DESC: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxgrdtblvListF_PROFESSION_NAME: TcxGridDBColumn;
    cxlblF_PROGRAM_ID2: TFVBFFCLabel;
    cxdbseF_PROGRAM_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_PROFESSION_NAME1: TFVBFFCLabel;
    cxdbbeF_PROFESSION_NAME: TFVBFFCDBButtonEdit;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
    cxgrdtblvListF_MAX_PUNTEN: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM_TYPE_NAME: TcxGridDBColumn;
    procedure cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_PROFESSION_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);  private
    { Private declarations }
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

var
  frmProgProgram_List: TfrmProgProgram_List;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ProgProgram, Data_EDUMainClient, Unit_FVBFFCInterfaces,
  Form_FVBFFCBaseListView, unit_EdutecInterfaces;

{ TfrmProgProgram_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmProgProgram_List.GetFilterString
  @author     slesage
  @param      None
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmProgProgram_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the condition for the Program ID }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PROGRAM_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_PROGRAM_ID', FSearchCriteriaDataSet.FieldByName( 'F_PROGRAM_ID' ).AsInteger );
  end;

  { Add the condition for the Program Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_NAME', FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).AsString );
  end;

  { Add the condition for the Language }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_LANGUAGE_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_LANGUAGE_ID', srcSearchCriteria.DataSet.FieldByName( 'F_LANGUAGE_ID' ).AsInteger );
  end;

  { Add the condition for the Profession }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_PROFESSION_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_PROFESSION_ID', srcSearchCriteria.DataSet.FieldByName( 'F_PROFESSION_ID' ).AsInteger );
  end;

  { Add the condition for the Active }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddBlnEqualCondition( aFilterString, 'F_ACTIVE', srcSearchCriteria.DataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;

  Result := aFilterString;
end;

{*****************************************************************************
  This method will be executed when the user clicks the Select Language button.

  @Name       TfrmProgProgram_List.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProgProgram_List.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProgProgramDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUProgProgramDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectLanguage( FSearchCriteriaDataSet );
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks the Select Discipline button.

  @Name       TfrmProgProgram_List.cxdbbeF_PROFESSION_NAMEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProgProgram_List.cxdbbeF_PROFESSION_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProgProgramDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUProgProgramDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectDiscipline( FSearchCriteriaDataSet );
  end;
end;

end.