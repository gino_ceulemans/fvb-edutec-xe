inherited frmProgProgram_Record: TfrmProgProgram_Record
  Left = 343
  Top = 219
  Width = 1017
  Height = 602
  Caption = 'Opleidingsaanbod'
  Constraints.MinHeight = 507
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 543
    Width = 1009
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 673
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 1009
    Height = 543
    inherited pnlRecord: TFVBFFCPanel
      Width = 847
      Height = 543
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 847
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 847
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 847
        object cxlblF_PROGRAM_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Programma ( ID )'
          FocusControl = cxdblblF_PROGRAM_ID1
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROGRAM_ID1: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROGRAM_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 105
        end
        object cxlblF_NAME1: TFVBFFCLabel
          Left = 120
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgProgram_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Programma'
          FocusControl = cxdblblF_NAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 120
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgProgram_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 847
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 539
        Width = 847
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 847
        Height = 469
        Caption = '`'
        object sbxAdditionalInfo: TScrollBox [0]
          Left = 0
          Top = 25
          Width = 847
          Height = 440
          HorzScrollBar.Style = ssFlat
          VertScrollBar.Style = ssFlat
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15914442
          ParentColor = False
          TabOrder = 3
          Visible = False
          object cxdbmmoInfrastructure: TFVBFFCDBMemo
            Left = 8
            Top = 216
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_REMARKS_INFRASTRUCTURE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 4
            Height = 161
            Width = 401
          end
          object cxlblVeilidgheidsuitrusting: TFVBFFCLabel
            Left = 8
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Infrastructuur'
            FocusControl = cxdbmmoInfrastructure
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblGereedschap: TFVBFFCLabel
            Left = 8
            Top = 0
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Lesgever'
            FocusControl = cxdbmmoInstructor
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoInstructor: TFVBFFCDBMemo
            Left = 8
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_REMARKS_INSTRUCTOR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 0
            Height = 161
            Width = 401
          end
          object cxlblDidactischeHulpmiddelen: TFVBFFCLabel
            Left = 416
            Top = 0
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bedrijf/School'
            FocusControl = cxdbmmoOrganisation
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoOrganisation: TFVBFFCDBMemo
            Left = 416
            Top = 16
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_REMARKS_ORGANISATION'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Height = 161
            Width = 401
          end
        end
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 847
        end
        inherited sbxMain: TScrollBox
          Width = 847
          Height = 440
          object cxdbmmoF_COMMENT: TFVBFFCDBMemo
            Left = 8
            Top = 328
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_COMMENT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 45
            Height = 89
            Width = 529
          end
          object cxlblF_PROGRAM_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programma ( ID )'
            FocusControl = cxdbseF_PROGRAM_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PROGRAM_ID: TFVBFFCDBSpinEdit
            Left = 136
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PROGRAM_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 0
            Width = 121
          end
          object cxlblF_NAME2: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programma'
            FocusControl = cxdbteF_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME: TFVBFFCDBTextEdit
            Left = 136
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_NAME'
            DataBinding.DataField = 'F_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 400
          end
          object cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_COMPLETE_USERNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Verantwoordelijke'
            FocusControl = cxdbbeF_COMPLETE_USERNAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_COMPLETE_USERNAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_COMPLETE_USERNAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COMPLETE_USERNAME1PropertiesButtonClick
            TabOrder = 3
            Width = 400
          end
          object cxlblF_DURATION1: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_DURATION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Duur'
            FocusControl = cxdbseF_DURATION
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_DURATION: TFVBFFCDBSpinEdit
            Left = 136
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_DURATION'
            DataBinding.DataField = 'F_DURATION'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 5
            Width = 121
          end
          object cxlblF_DURATION_DAYS1: TFVBFFCLabel
            Left = 288
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_DURATION_DAYS'
            Caption = 'Duur ( Dagen )'
            FocusControl = cxdbseF_DURATION_DAYS
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_DURATION_DAYS: TFVBFFCDBSpinEdit
            Left = 416
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_DURATION_DAYS'
            DataBinding.DataField = 'F_DURATION_DAYS'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 6
            Width = 121
          end
          object cxlblF_MINIMUM1: TFVBFFCLabel
            Left = 144
            Top = 296
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_MINIMUM'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Minimum'
            FocusControl = cxdbseF_MINIMUM
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbseF_MINIMUM: TFVBFFCDBSpinEdit
            Left = 296
            Top = 296
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_MINIMUM'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 7
            Visible = False
            Width = 121
          end
          object cxlblF_MAXIMUM1: TFVBFFCLabel
            Left = 144
            Top = 320
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_MAXIMUM'
            Caption = 'Maximum'
            FocusControl = cxdbseF_MAXIMUM
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbseF_MAXIMUM: TFVBFFCDBSpinEdit
            Left = 296
            Top = 320
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_MAXIMUM'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 8
            Visible = False
            Width = 121
          end
          object cxlblF_SHORT_DESC1: TFVBFFCLabel
            Left = 8
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_SHORT_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Korte Omschrijving'
            FocusControl = cxdbteF_SHORT_DESC
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_SHORT_DESC: TFVBFFCDBTextEdit
            Left = 136
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_SHORT_DESC'
            DataBinding.DataField = 'F_SHORT_DESC'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 10
            Width = 401
          end
          object cxlblF_LONG_DESC1: TFVBFFCLabel
            Left = 8
            Top = 196
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Omschrijving'
            FocusControl = cxdbmmoF_LONG_DESC
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_LONG_DESC: TFVBFFCDBMemo
            Left = 8
            Top = 216
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_LONG_DESC'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 30
            Height = 89
            Width = 529
          end
          object cxlblF_ENROLMENT_DATE1: TFVBFFCLabel
            Left = 544
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_ENROLMENT_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Inschrijvingsdatum'
            FocusControl = cxdbseF_ENROLMENT_DATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_ENROLMENT_DATE: TFVBFFCDBSpinEdit
            Left = 664
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_ENROLMENT_DATE'
            DataBinding.DataField = 'F_ENROLMENT_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 15
            Width = 152
          end
          object cxlblF_CONTROL_DATE1: TFVBFFCLabel
            Left = 544
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_CONTROL_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Controledatum'
            FocusControl = cxdbseF_CONTROL_DATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_CONTROL_DATE: TFVBFFCDBSpinEdit
            Left = 664
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_CONTROL_DATE'
            DataBinding.DataField = 'F_CONTROL_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 16
            Width = 152
          end
          object cxlblF_PROFESSION_NAME1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_PROFESSION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Discipline'
            FocusControl = cxdbbeF_PROFESSION_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_PROFESSION_NAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_PROFESSION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PROFESSION_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PROFESSION_NAME1PropertiesButtonClick
            TabOrder = 4
            Width = 400
          end
          object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
            Left = 544
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Taal'
            FocusControl = cxdbbeF_LANGUAGE_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
            Left = 624
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_LANGUAGE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
            TabOrder = 13
            Width = 193
          end
          object cxlblF_COMMENT1: TFVBFFCLabel
            Left = 8
            Top = 306
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Commentaar'
            FocusControl = cxdbmmoF_COMMENT
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_ACTIVE1: TFVBFFCLabel
            Left = 544
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_ACTIVE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Actief'
            FocusControl = csdbcbF_ACTIVE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object csdbcbF_ACTIVE: TFVBFFCDBCheckBox
            Left = 624
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_ACTIVE'
            Caption = 'csdbcbF_ACTIVE'
            DataBinding.DataField = 'F_ACTIVE'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            Properties.NullStyle = nssInactive
            Properties.ReadOnly = False
            TabOrder = 11
            Width = 21
          end
          object cxlblF_END_DATE1: TFVBFFCLabel
            Left = 544
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Eind Datum'
            FocusControl = cxdbdeF_END_DATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_END_DATE: TFVBFFCDBDateEdit
            Left = 624
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_END_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 12
            Width = 193
          end
          object cxlblF_CODE1: TFVBFFCLabel
            Left = 544
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Analytische sleutel'
            FocusControl = cxdbteF_CODE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CODE1: TFVBFFCDBTextEdit
            Left = 664
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_CODE'
            DataBinding.DataField = 'F_CODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 17
            Width = 153
          end
          object cxlblF_EDUCATION_ID1: TFVBFFCLabel
            Left = 544
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_EDUCATION_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'External code'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_TK_FVB2: TFVBFFCLabel
            Left = 544
            Top = 280
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Tussenkomst FVB (1u)'
            FocusControl = F_TK_FVB1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_TK_FVB1: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 280
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_TK_FVB'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 22
            Width = 121
          end
          object cxlblF_TK_CEVORA2: TFVBFFCLabel
            Left = 544
            Top = 304
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Tussenkomst Cevora (4u)'
            FocusControl = F_TK_CEVORA1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_TK_CEVORA1: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 304
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_TK_CEVORA'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 23
            Width = 121
          end
          object cxlblF_BT_FVB2: TFVBFFCLabel
            Left = 544
            Top = 328
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Boete FVB (4u)'
            FocusControl = F_BT_FVB1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_BT_FVB1: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 328
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_BT_FVB'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 24
            Width = 121
          end
          object cxlblF_BT_CEVORA2: TFVBFFCLabel
            Left = 544
            Top = 352
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Boete Cevora (4u)'
            FocusControl = F_BT_CEVORA1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_BT_CEVORA1: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 352
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_BT_CEVORA'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 25
            Width = 121
          end
          object cxlblF_START_HOUR1: TFVBFFCLabel
            Left = 544
            Top = 179
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_START_HOUR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Start/Eind uur'
            FocusControl = cxdbteF_START_HOUR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_START_HOUR: TFVBFFCDBTextEdit
            Left = 632
            Top = 179
            HelpType = htKeyword
            HelpKeyword = 'frmProgramRecord.F_START_HOUR'
            DataBinding.DataField = 'F_START_HOUR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 18
            Width = 185
          end
          object cxlblF_PRICE_WORKER: TFVBFFCLabel
            Left = 544
            Top = 208
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessieprijs (arbeider)'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblFVBFFCLabel2: TFVBFFCLabel
            Left = 544
            Top = 232
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_PRICE_SESSION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessieprijs (overige)'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_PRICE_FIXED1: TFVBFFCLabel
            Left = 544
            Top = 256
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_PRICE_FIXED'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie Prijs FVB'
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object FVBFFCDBCurrencyEdit1: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 256
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_PRICE_FIXED'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 21
            Width = 121
          end
          object FVBFFCDBCurrencyEdit2: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 232
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_PRICE_OTHER'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 20
            Width = 121
          end
          object FVBFFCDBCurrencyEdit3: TFVBFFCDBCurrencyEdit
            Left = 696
            Top = 208
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_PRICE_WORKER'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 19
            Width = 121
          end
          object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
            Left = 661
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_CODE'
            DataBinding.DataField = 'externalcode1'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.MaxLength = 6
            TabOrder = 14
            Width = 153
          end
          object cxdbbeF_PROGRAM_TYPE_NAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_PROGRAM_TYPE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PROGRAM_TYPE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PROGRAM_TYPE_NAMEPropertiesButtonClick
            TabOrder = 2
            Width = 193
          end
          object cxlblF_PROGRAM_TYPE: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programmatype'
            FocusControl = cxdbbeF_PROGRAM_TYPE_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_MAX_PUNTEN: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_DURATION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Max punten'
            FocusControl = cxdbseF_MAX_PUNTEN
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_MAX_PUNTEN: TFVBFFCDBSpinEdit
            Left = 136
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_MAX_PUNTEN'
            DataBinding.DataField = 'F_MAX_PUNTEN'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 9
            Width = 121
          end
        end
        object pnlPriceSchemeInfoSeperator: TFVBFFCPanel
          Left = 0
          Top = 465
          Width = 847
          Height = 4
          Align = alBottom
          BevelOuter = bvNone
          BorderWidth = 4
          ParentColor = True
          TabOrder = 2
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clInactiveCaption
          StyleClientArea.BackColor2 = clInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 1005
      Height = 543
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 543
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end
          item
            Item = dxnbiBijkomendeInfo
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiSessies
          end
          item
            Item = dxnbiInstructors
          end
          item
            Item = dxnbiShowHistory
          end>
      end
      object dxnbiBijkomendeInfo: TdxNavBarItem
        Action = acShowAdditionalInfo
      end
      object dxnbiSessies: TdxNavBarItem
        Action = acShowSessions
      end
      object dxnbiInstructors: TdxNavBarItem
        Action = acShowInstructors
      end
      object dxnbiShowHistory: TdxNavBarItem
        Action = acShowHistory
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 543
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowAdditionalInfo: TAction
      Caption = 'Bijkomende Info'
      OnExecute = acShowAdditionalInfoExecute
    end
    object acShowSessions: TAction
      Caption = 'Sessies'
      OnExecute = acShowSessionsExecute
    end
    object acShowInstructors: TAction
      Caption = 'Lesgevers'
      OnExecute = acShowInstructorsExecute
    end
    object acShowHistory: TAction
      Caption = 'Historiek'
      OnExecute = acShowHistoryExecute
    end
  end
end
