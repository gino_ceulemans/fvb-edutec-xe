inherited dtmProgProgram: TdtmProgProgram
  AutoOpenDataSets = False
  KeyFields = 'F_PROGRAM_ID'
  ListViewClass = 'TfrmProgProgram_List'
  RecordViewClass = 'TfrmProgProgram_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Sessies'
      ForeignKeys = 'F_PROGRAM_ID'
      DataModuleClass = 'TdtmSesSession'
      AutoCreate = False
    end
    item
      Caption = 'Lesgevers'
      ForeignKeys = 'F_PROGRAM_ID'
      DataModuleClass = 'TdtmProgInstructor'
      AutoCreate = False
    end
    item
      Caption = 'Historiek'
      ForeignKeys = 'F_PROGRAM_ID'
      DataModuleClass = 'TdtmLogHistory'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_DURATION: TSmallintField
      DisplayLabel = 'Duur'
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_DURATION_DAYS: TSmallintField
      DisplayLabel = 'Duur ( Dagen )'
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SHORT_DESC: TStringField
      DisplayLabel = 'Korte Omschrijving'
      FieldName = 'F_SHORT_DESC'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_PROFESSION_NAME: TStringField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_USER_ID: TIntegerField
      DisplayLabel = 'Verantwoordelijke ( ID )'
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_COMPLETE_USERNAME: TStringField
      DisplayLabel = 'Verantwoordelijke'
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object cdsListF_ENROLMENT_DATE: TIntegerField
      DisplayLabel = 'Inschrijvingsdatum'
      FieldName = 'F_ENROLMENT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CONTROL_DATE: TIntegerField
      DisplayLabel = 'Controle Datum'
      FieldName = 'F_CONTROL_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_CODE: TStringField
      DisplayLabel = 'Analytische sleutel'
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_SESSION: TFloatField
      FieldName = 'F_PRICE_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_DAY: TFloatField
      FieldName = 'F_PRICE_DAY'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_TK_FVB: TFloatField
      FieldName = 'F_TK_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_TK_CEVORA: TFloatField
      FieldName = 'F_TK_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_BT_FVB: TFloatField
      FieldName = 'F_BT_FVB'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_BT_CEVORA: TFloatField
      FieldName = 'F_BT_CEVORA'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_FIXED: TFloatField
      FieldName = 'F_PRICE_FIXED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_START_HOUR: TStringField
      FieldName = 'F_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListexternalcode1: TStringField
      FieldName = 'externalcode1'
      ProviderFlags = [pfInUpdate]
      Size = 6
    end
    object cdsListF_PROGRAM_TYPE: TIntegerField
      DisplayLabel = 'Programmatype'
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_MAX_PUNTEN: TIntegerField
      DisplayLabel = 'Max punten'
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PROGRAM_TYPE_NAME: TStringField
      DisplayLabel = 'Programmatypenaam'
      FieldName = 'F_PROGRAM_TYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    AfterOpen = cdsRecordAfterOpen
    object cdsRecordF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
    end
    object cdsRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      Size = 232
    end
    object cdsRecordF_DURATION: TSmallintField
      DisplayLabel = 'Duur'
      FieldName = 'F_DURATION'
      Required = True
    end
    object cdsRecordF_DURATION_DAYS: TSmallintField
      DisplayLabel = 'Duur ( Dagen )'
      FieldName = 'F_DURATION_DAYS'
      Required = True
    end
    object cdsRecordF_SHORT_DESC: TStringField
      DisplayLabel = 'Korte Omschrijving'
      FieldName = 'F_SHORT_DESC'
      Size = 128
    end
    object cdsRecordF_LONG_DESC: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_LONG_DESC'
      BlobType = ftMemo
    end
    object cdsRecordF_ENROLMENT_DATE: TIntegerField
      DisplayLabel = 'Inschrijvingsdatum'
      FieldName = 'F_ENROLMENT_DATE'
    end
    object cdsRecordF_CONTROL_DATE: TIntegerField
      DisplayLabel = 'Controledatum'
      FieldName = 'F_CONTROL_DATE'
    end
    object cdsRecordF_PROFESSION_ID: TIntegerField
      DisplayLabel = 'Discipline ( ID )'
      FieldName = 'F_PROFESSION_ID'
    end
    object cdsRecordF_PROFESSION_NAME: TStringField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_NAME'
      Size = 64
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsRecordF_COMMENT: TMemoField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_COMMENT'
      BlobType = ftMemo
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Aktief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsRecordF_EDUCATION_ID: TIntegerField
      FieldName = 'F_EDUCATION_ID'
    end
    object cdsRecordF_CODE: TStringField
      DisplayLabel = 'Analytische sleutel'
      FieldName = 'F_CODE'
      Size = 50
    end
    object cdsRecordF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
      Visible = False
    end
    object cdsRecordF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
      Visible = False
    end
    object cdsRecordF_TK_FVB: TFloatField
      DisplayLabel = 'Tussenkomst FVB'
      FieldName = 'F_TK_FVB'
      Required = True
    end
    object cdsRecordF_TK_CEVORA: TFloatField
      DisplayLabel = 'Tussenkomst Cevora'
      FieldName = 'F_TK_CEVORA'
      Required = True
    end
    object cdsRecordF_BT_FVB: TFloatField
      DisplayLabel = 'Boete FVB'
      FieldName = 'F_BT_FVB'
      Required = True
    end
    object cdsRecordF_BT_CEVORA: TFloatField
      DisplayLabel = 'Boete Cevora'
      FieldName = 'F_BT_CEVORA'
      Required = True
    end
    object cdsRecordF_PRICE_SESSION: TFloatField
      FieldName = 'F_PRICE_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_START_HOUR: TStringField
      FieldName = 'F_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsRecordF_PRICE_FIXED: TFloatField
      FieldName = 'F_PRICE_FIXED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object cdsRecordF_PRICE_DAY: TFloatField
      FieldName = 'F_PRICE_DAY'
    end
    object cdsRecordF_REMARKS_INSTRUCTOR: TMemoField
      FieldName = 'F_REMARKS_INSTRUCTOR'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_REMARKS_ORGANISATION: TMemoField
      FieldName = 'F_REMARKS_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_REMARKS_INFRASTRUCTURE: TMemoField
      FieldName = 'F_REMARKS_INFRASTRUCTURE'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordexternalcode1: TStringField
      FieldName = 'externalcode1'
      ProviderFlags = [pfInUpdate]
      Size = 6
    end
    object cdsRecordF_PROGRAM_TYPE: TIntegerField
      DisplayLabel = 'Programmatype'
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_MAX_PUNTEN: TIntegerField
      DisplayLabel = 'Max punten'
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PROGRAM_TYPE_NAME: TStringField
      DisplayLabel = 'Programmatype naam'
      FieldName = 'F_PROGRAM_TYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsSearchCriteriaF_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_PROFESSION_ID: TIntegerField
      DisplayLabel = 'Discipline ( ID )'
      FieldName = 'F_PROFESSION_ID'
    end
    object cdsSearchCriteriaF_PROFESSION_NAME: TStringField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
    end
    object cdsSearchCriteriaF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_PRICING_SCHEME: TIntegerField
      DisplayLabel = 'Prijsbepaling'
      FieldName = 'F_PRICING_SCHEME'
    end
    object cdsSearchCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmProgProgram'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmProgProgram'
  end
end
