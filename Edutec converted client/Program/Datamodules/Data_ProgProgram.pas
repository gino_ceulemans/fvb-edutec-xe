{*****************************************************************************
  This DataModule will be used for the Maintenance of T_PROG_PROGRAM records.

  @Name       Data_ProgProgram
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2008   ivdbossche           Added F_PRICE_WORKER, F_START_HOUR,... (Mantis 3197)
  11/09/2006   sLesage              Added the necessary fields for Boetes and
                                    Tussenkomsten.
  26/07/2005   sLesage              Fixed a bug in the SelectProgram method.
                                    Added a DetailDataModule for ProgInstructor.
                                    Added functionality to link a program to
                                    some Instructors.
  07/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_ProgProgram;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect,
  Data.FMTBcd, Data.SqlExpr;

type
  TdtmProgProgram = class(TEDUDataModule, IEDUProgProgramDataModule)
    cdsRecordF_PROGRAM_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_USER_ID: TIntegerField;
    cdsRecordF_COMPLETE_USERNAME: TStringField;
    cdsRecordF_DURATION: TSmallintField;
    cdsRecordF_DURATION_DAYS: TSmallintField;
    cdsRecordF_SHORT_DESC: TStringField;
    cdsRecordF_LONG_DESC: TMemoField;
    cdsRecordF_ENROLMENT_DATE: TIntegerField;
    cdsRecordF_CONTROL_DATE: TIntegerField;
    cdsRecordF_PROFESSION_ID: TIntegerField;
    cdsRecordF_PROFESSION_NAME: TStringField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_COMMENT: TMemoField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_EDUCATION_ID: TIntegerField;
    cdsSearchCriteriaF_PROGRAM_ID: TIntegerField;
    cdsSearchCriteriaF_NAME: TStringField;
    cdsSearchCriteriaF_PROFESSION_ID: TIntegerField;
    cdsSearchCriteriaF_PROFESSION_NAME: TStringField;
    cdsSearchCriteriaF_LANGUAGE_ID: TIntegerField;
    cdsSearchCriteriaF_LANGUAGE_NAME: TStringField;
    cdsSearchCriteriaF_PRICING_SCHEME: TIntegerField;
    cdsRecordF_CODE: TStringField;
    cdsListF_PROGRAM_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_DURATION: TSmallintField;
    cdsListF_DURATION_DAYS: TSmallintField;
    cdsListF_SHORT_DESC: TStringField;
    cdsListF_PROFESSION_NAME: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsListF_USER_ID: TIntegerField;
    cdsListF_COMPLETE_USERNAME: TStringField;
    cdsListF_ENROLMENT_DATE: TIntegerField;
    cdsListF_CONTROL_DATE: TIntegerField;
    cdsListF_LANGUAGE_ID: TIntegerField;
    cdsListF_LANGUAGE_NAME: TStringField;
    cdsSearchCriteriaF_ACTIVE: TBooleanField;
    cdsListF_CODE: TStringField;
    cdsRecordF_MINIMUM: TSmallintField;
    cdsRecordF_MAXIMUM: TSmallintField;
    cdsRecordF_TK_FVB: TFloatField;
    cdsRecordF_TK_CEVORA: TFloatField;
    cdsRecordF_BT_FVB: TFloatField;
    cdsRecordF_BT_CEVORA: TFloatField;
    cdsListF_MINIMUM: TSmallintField;
    cdsListF_MAXIMUM: TSmallintField;
    cdsListF_PRICING_SCHEME: TIntegerField;
    cdsListF_PRICE_SESSION: TFloatField;
    cdsListF_PRICE_DAY: TFloatField;
    cdsListF_PRICE_OTHER: TFloatField;
    cdsListF_TK_FVB: TFloatField;
    cdsListF_TK_CEVORA: TFloatField;
    cdsListF_BT_FVB: TFloatField;
    cdsListF_BT_CEVORA: TFloatField;
    cdsRecordF_PRICE_SESSION: TFloatField;
    cdsRecordF_PRICE_OTHER: TFloatField;
    cdsRecordF_PRICE_WORKER: TFloatField;
    cdsRecordF_START_HOUR: TStringField;
    cdsRecordF_PRICE_FIXED: TFloatField;
    cdsListF_PRICE_WORKER: TFloatField;
    cdsListF_PRICE_FIXED: TFloatField;
    cdsListF_START_HOUR: TStringField;
    cdsRecordF_PRICING_SCHEME: TIntegerField;
    cdsRecordF_PRICE_DAY: TFloatField;
    cdsRecordF_REMARKS_INSTRUCTOR: TMemoField;
    cdsRecordF_REMARKS_ORGANISATION: TMemoField;
    cdsRecordF_REMARKS_INFRASTRUCTURE: TMemoField;
    cdsListexternalcode1: TStringField;
    cdsRecordexternalcode1: TStringField;
    cdsListF_PROGRAM_TYPE: TIntegerField;
    cdsListF_MAX_PUNTEN: TIntegerField;
    cdsRecordF_PROGRAM_TYPE: TIntegerField;
    cdsRecordF_MAX_PUNTEN: TIntegerField;
    cdsListF_PROGRAM_TYPE_NAME: TStringField;
    cdsRecordF_PROGRAM_TYPE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
    procedure cdsRecordAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SelectDiscipline ( aDataSet : TDataSet ); virtual;
    procedure SelectLanguage   ( aDataSet : TDataSet ); virtual;
    procedure SelectProgramType( aDataSet : TDataSet ); virtual;
    procedure SelectUser       ( aDataSet : TDataSet ); virtual;
    procedure ShowDiscipline   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowProgramType  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowUser         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectProgram (
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure LinkProgramsToInstructor( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False ); virtual;
    class procedure ShowProgram   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

  procedure CopyProgramFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault  );

var
  dtmProgProgram: TdtmProgProgram;

implementation

uses Data_Language, Data_ProgDiscipline, Data_User, Data_EDUMainClient, Data_ProgramType;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy CopyProgramFieldValues related fields from one DataSet
  to another DataSet.

  @Name       CopyUserFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyProgramFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault  );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_PROGRAM_ID' ).AsInteger :=
        aSource.FieldByName( 'F_PROGRAM_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      critProgramToSessionSearchCriteria :
      begin
        aDestination.FieldByName( 'F_PROGRAM_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end;
      critProgramToSession :
      begin
        // Mantis 3197 (04/07/2008 Ivan Van den Bossche)
        aDestination.FieldByName('F_PRICE_WORKER').Value := aSource.FieldByName('F_PRICE_WORKER').Value;
        aDestination.FieldByName('F_PRICE_OTHER').Value := aSource.FieldByName('F_PRICE_OTHER').Value;
        aDestination.FieldByName('F_PRICE_FIXED').Value := aSource.FieldByName('F_PRICE_FIXED').Value;
        aDestination.FieldByName('F_START_HOUR').Value := aSource.FieldByName('F_START_HOUR').Value;

        aDestination.FieldByName( 'F_PROGRAM_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
//        if Assigned( aSource.FindField( 'F_MINIMUM' ) ) then
//        begin
//          if aSource.FieldByName( 'F_MINIMUM' ).IsNull then
//          begin
//            aDestination.FieldByName( 'F_MINIMUM' ).AsInteger := 0;
//          end
//          else
//          begin
//            aDestination.FieldByName( 'F_MINIMUM' ).AsInteger :=
//              aSource.FieldByName( 'F_MINIMUM' ).AsInteger;
//          end;
//        end;
//        if Assigned( aSource.FindField( 'F_MAXIMUM' ) ) then
//        begin
//          if aSource.FieldByName( 'F_MAXIMUM' ).IsNull then
//          begin
//            aDestination.FieldByName( 'F_MAXIMUM' ).AsInteger := 9999;
//          end
//          else
//          begin
//            aDestination.FieldByName( 'F_MAXIMUM' ).AsInteger :=
//              aSource.FieldByName( 'F_MAXIMUM' ).AsInteger;
//          end;
//        end;
        if aSource.FieldByName( 'F_DURATION' ).AsInteger > 0 then
        begin
          aDestination.FieldByName( 'F_DURATION' ).AsInteger :=
            aSource.FieldByName( 'F_DURATION' ).AsInteger;
        end;
        if aSource.FieldByName( 'F_DURATION_DAYS' ).AsInteger > 0 then
        begin
          aDestination.FieldByName( 'F_DURATION_DAYS' ).AsInteger :=
            aSource.FieldByName( 'F_DURATION_DAYS' ).AsInteger;
        end;
        aDestination.FieldByName( 'F_LANGUAGE_ID' ).AsInteger :=
          aSource.FieldByName( 'F_LANGUAGE_ID' ).AsInteger;
        aDestination.FieldByName( 'F_LANGUAGE_NAME' ).AsString :=
          aSource.FieldByName( 'F_LANGUAGE_NAME' ).AsString;
        aDestination.FieldByName( 'F_USER_ID' ).AsInteger :=
          aSource.FieldByName( 'F_USER_ID' ).AsInteger;
        aDestination.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
          aSource.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
        { Initialise the Final Enrollment date to the start date + the
          enrolment date offset }
        if ( not aSource.FieldByName( 'F_ENROLMENT_DATE' ).IsNull ) and
           ( not aDestination.FieldByName( 'F_START_DATE' ).IsNull ) and
           ( aDestination.FieldByName( 'F_ENROLMENT_DATE' ).IsNull ) then
        begin
          aDestination.FieldByName( 'F_ENROLMENT_DATE' ).AsDateTime :=
            aDestination.FieldByName( 'F_START_DATE' ).AsDateTime +
            aSource.FieldByName( 'F_ENROLMENT_DATE' ).AsInteger;
        end;
        { Initialise the Final Control date to the start date + the
          control date offset }
        if ( not aSource.FieldByName( 'F_CONTROL_DATE' ).IsNull ) and
           ( not aDestination.FieldByName( 'F_START_DATE' ).IsNull ) and
           ( aDestination.FieldByName( 'F_CONTROL_DATE' ).IsNull ) then
        begin
          aDestination.FieldByName( 'F_CONTROL_DATE' ).AsDateTime :=
            aDestination.FieldByName( 'F_START_DATE' ).AsDateTime +
            aSource.FieldByName( 'F_CONTROL_DATE' ).AsInteger;
        end;
        // Analytisch rekeningnummer
        aDestination.FieldByName( 'F_ANA_SLEUTEL' ).Value :=
          aSource.FieldByName( 'F_CODE' ).Value;
        // Tussenkomsten
        aDestination.FieldByName( 'F_TK_FVB' ).Value :=
          aSource.FieldByName( 'F_TK_FVB' ).Value;
        aDestination.FieldByName( 'F_TK_CEVORA' ).Value :=
          aSource.FieldByName( 'F_TK_CEVORA' ).Value;
        // Boetes
        aDestination.FieldByName( 'F_BT_FVB' ).Value :=
          aSource.FieldByName( 'F_BT_FVB' ).Value;
        aDestination.FieldByName( 'F_BT_CEVORA' ).Value :=
          aSource.FieldByName( 'F_BT_CEVORA' ).Value;
      end;
      critProgramToSesWizard :
      begin
        aDestination.FieldByName( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName( 'F_DURATION' ).AsInteger :=
          aSource.FieldByName( 'F_DURATION' ).AsInteger;
        aDestination.FieldByName( 'F_DURATION_DAYS' ).AsInteger :=
          aSource.FieldByName( 'F_DURATION_DAYS' ).AsInteger;
        aDestination.FieldByName( 'F_LANGUAGE_ID' ).AsInteger :=
          aSource.FieldByName( 'F_LANGUAGE_ID' ).AsInteger;
        aDestination.FieldByName( 'F_LANGUAGE_NAME' ).AsString :=
          aSource.FieldByName( 'F_LANGUAGE_NAME' ).AsString;
        aDestination.FieldByName( 'F_USER_ID' ).AsInteger :=
          aSource.FieldByName( 'F_USER_ID' ).AsInteger;
        aDestination.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
          aSource.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
      end;
      critProgramToProgInstructor :
      begin
        aDestination.FieldByName( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end;
      else
      begin
        aDestination.FieldByName( 'F_PROGRAM_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName( 'F_LASTNAME' ).AsString :=
          aSource.FieldByName( 'F_LASTNAME' ).AsString;
        aDestination.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
          aSource.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to link one or more programs to an Instructor.

  @Name       TdtmProgProgram.LinkProgramsToInstructor
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        Parameter indicating what the Destination of the
                             Selection is ( is used to copy some fields ).
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgProgram.LinkProgramsToInstructor(
  aDataSet: TDataSet; aWhere: String; aMultiSelect: Boolean);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the Country ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmProgProgram', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      aSelectedRecords.First;
      while not aSelectedRecords.Eof do
      begin
        aDataSet.Tag := 1;
        aDataSet.Insert;
        CopyProgramFieldValues( aSelectedRecords, aDataSet, True, critProgramToProgInstructor );
        aDataSet.Post;
        aSelectedRecords.Next;
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmProgProgram.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_PROG_PROGRAM';
end;

{*****************************************************************************
  This method will be used to select one or more Discipline Records.

  @Name       TdtmProgProgram.SelectDiscipline
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.SelectDiscipline(aDataSet: TDataSet);
begin
  TdtmProgDiscipline.SelectDiscipline( aDataSet );
end;

{*****************************************************************************
  This method will be used to select one or more Language Records.

  @Name       TdtmProgProgram.SelectLanguage
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.SelectLanguage(aDataSet: TDataSet);
begin
  TdtmLanguage.SelectLanguage( aDataSet );
end;

procedure TdtmProgProgram.SelectProgramType(aDataSet: TDataSet);
begin
  TdtmProgramType.SelectProgramType( aDataSet );
end;

{*****************************************************************************
  This method will be used to select one or more Programs.

  @Name       TdtmProgProgram.SelectProgram
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        Parameter indicating what the Destination of the
                             Selection is ( is used to copy some fields ).
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgProgram.SelectProgram(
  aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False
  );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the Country ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmProgProgram', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyProgramFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more users.

  @Name       TdtmLanguage.SelectUser
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.SelectUser(aDataSet: TDataSet);
begin
  TdtmUser.SelectUser( aDataSet, '', False, critUserToProgProgram );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Discipline
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmLanguage.ShowDiscipline
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.ShowDiscipline(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_PROFESSION_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmProgDiscipline.ShowDiscipline( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Language
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmLanguage.ShowLanguage
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode );
  end;
end;

procedure TdtmProgProgram.ShowProgramType(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_PROGRAMTYPE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmProgramtype.ShowProgramType( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Program
  Record based on the PK Field values found in the given aDataSet.


  @Name       TdtmProgProgram.ShowUser
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           Parameter indicating what the Destination of the
                                Selection is ( is used to copy some fields ).
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgProgram.ShowProgram(
  aDataSet : TDataSet;
  aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView;
  aCopyTo : TCopyRecordInformationTo = critDefault );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView for TdtmUser.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmProgProgram', 'F_PROGRAM_ID = ' + aDataSet.FieldByName( 'F_PROGRAM_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyProgramFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single User
  Record based on the PK Field values found in the given aDataSet.


  @Name       TdtmProgProgram.ShowUser
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.ShowUser(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_USER_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmUser.ShowUser( aDataSet, aRecordViewMode, critUserToProgProgram );
  end;
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmProgProgram.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgProgram.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PROGRAM_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

procedure TdtmProgProgram.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_MINIMUM' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_MAXIMUM' ).AsInteger := 9999;
end;

procedure TdtmProgProgram.cdsSearchCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsSearchCriteria.FieldByName( 'F_ACTIVE' ).Value := True;

end;

procedure TdtmProgProgram.cdsRecordAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if cdsRecordF_PROGRAM_TYPE.AsInteger = 1 then
  begin
    cdsRecordF_DURATION.Required := True;
    cdsRecordF_DURATION_DAYS.Required := True;
    cdsRecordF_MAX_PUNTEN.Required := False;
  end
  else
  begin
    cdsRecordF_DURATION.Required := False;
    cdsRecordF_DURATION_DAYS.Required := False;
    cdsRecordF_MAX_PUNTEN.Required := True;
  end
end;

end.
