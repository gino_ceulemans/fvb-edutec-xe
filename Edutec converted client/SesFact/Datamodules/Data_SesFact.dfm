inherited dtmSesFact: TdtmSesFact
  KeyFields = 'F_SESSION_ID;F_ORGANISATION_ID;F_KLANTNUMMER;F_VERKOOPREKENING'
  ListViewClass = 'TfrmSesFact_List'
  RecordViewClass = 'TfrmSesFact_Record'
  Registered = True
  Height = 263
  Width = 494
  inherited qryList: TFVBFFCQuery
    Connection = nil
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_LAND: TStringField
      FieldName = 'F_LAND'
      ProviderFlags = [pfInUpdate]
      Size = 3
    end
    object cdsListF_ALFA_CODE: TStringField
      FieldName = 'F_ALFA_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_STRAAT: TStringField
      FieldName = 'F_STRAAT'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_HUISNUMMER: TStringField
      FieldName = 'F_HUISNUMMER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object cdsListF_BUSNUMMER: TStringField
      FieldName = 'F_BUSNUMMER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object cdsListF_WOONPLAATS: TStringField
      FieldName = 'F_WOONPLAATS'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_TAAL: TIntegerField
      FieldName = 'F_TAAL'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_TELEFOON_NUMMER: TStringField
      FieldName = 'F_TELEFOON_NUMMER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_DOCUMENT_NUMMER: TIntegerField
      FieldName = 'F_DOCUMENT_NUMMER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ONZE_REFERTE: TStringField
      FieldName = 'F_ONZE_REFERTE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_REFERTE: TStringField
      FieldName = 'F_REFERTE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_BEDRAG_VALUTA: TFloatField
      FieldName = 'F_BEDRAG_VALUTA'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_VERKOOPREKENING: TIntegerField
      FieldName = 'F_VERKOOPREKENING'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_ANALYTISCHE_SLEUTEL_1: TStringField
      FieldName = 'F_ANALYTISCHE_SLEUTEL_1'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsListF_BOEKHOUDPERIODE: TIntegerField
      FieldName = 'F_BOEKHOUDPERIODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_NAAM: TStringField
      FieldName = 'F_NAAM'
      Size = 50
    end
    object cdsListF_DOCUMENT_DATUM: TDateTimeField
      FieldName = 'F_DOCUMENT_DATUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_VERVALDATUM: TDateTimeField
      FieldName = 'F_VERVALDATUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_KLANTNUMMER: TIntegerField
      FieldName = 'F_KLANTNUMMER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_POSTNUMMER: TIntegerField
      FieldName = 'F_POSTNUMMER'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmSesFact'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmSesFact'
  end
  object cdsSesFact: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvSES_FACT'
    RemoteServer = dspConnection
    AutoOpen = False
    Left = 370
    Top = 64
  end
end
