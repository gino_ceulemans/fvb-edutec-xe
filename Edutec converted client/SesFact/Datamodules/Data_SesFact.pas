{*****************************************************************************
  This DataModule will be used for the maintenance of <T_SES_FACT>.

  @Name       Data_SesFact
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  06/02/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_SesFact;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, Unit_PPWFrameWorkActions, MConnect, unit_EdutecInterfaces,
  Datasnap.DSConnect;

type
  TdtmSesFact = class(TEDUDataModule, IEDUSesFactDatamodule)
    cdsSesFact: TFVBFFCClientDataSet;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_LAND: TStringField;
    cdsListF_ALFA_CODE: TStringField;
    cdsListF_STRAAT: TStringField;
    cdsListF_HUISNUMMER: TStringField;
    cdsListF_BUSNUMMER: TStringField;
    cdsListF_WOONPLAATS: TStringField;
    cdsListF_TAAL: TIntegerField;
    cdsListF_TELEFOON_NUMMER: TStringField;
    cdsListF_DOCUMENT_NUMMER: TIntegerField;
    cdsListF_ONZE_REFERTE: TStringField;
    cdsListF_REFERTE: TStringField;
    cdsListF_BEDRAG_VALUTA: TFloatField;
    cdsListF_VERKOOPREKENING: TIntegerField;
    cdsListF_ANALYTISCHE_SLEUTEL_1: TStringField;
    cdsListF_BOEKHOUDPERIODE: TIntegerField;
    cdsListF_NAAM: TStringField;
    cdsListF_DOCUMENT_DATUM: TDateTimeField;
    cdsListF_VERVALDATUM: TDateTimeField;
    cdsListF_KLANTNUMMER: TIntegerField;
    cdsListF_POSTNUMMER: TIntegerField;
  private
    { Private declarations }
    function WriteRecordToDatabase(
             aSessionId, aOrganisationId, aSaleNr, aDocNr : integer) : integer;
  protected
    { Protected declarations }
    function IsListViewActionAllowed (
             aAction : TPPWFrameWorkListViewAction ) : Boolean; override;
    function RetrieveOldMaximum      : integer; virtual;
    function WriteRecordsToDatabase  ( aDataSet : TDataSet ) : integer; virtual;
    procedure StartNewBookYear       ( aFactNr  : integer ); virtual;


  public
    { Public declarations }
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, DateUtils, Unit_PPWFrameWorkDataModule;

{$R *.dfm}

function TdtmSesFact.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
var
  aAllowed    : Boolean;
begin
  if ( aAction is TPPWFrameWorkListViewGridExportToHTML ) or
     ( aAction is TPPWFrameWorkListViewGridExportToXML  ) or
     ( aAction is TPPWFrameWorkListViewGridExportToXLS  )
  then
  begin
    aAllowed := True;
  end
  else
  begin
    aAllowed := False;
  end;

  Result := aAllowed;
end;

function TdtmSesFact.RetrieveOldMaximum : integer;
begin
  Result := 0;
  with cdsSesFact do
  begin
    Close;
    FetchParams;
    if Params.Count > 0 then
    begin
      cdsSesFact.Params.ParamByName( '@ACTION' ).Value := 'M';
      Execute;
      FetchParams;
      Result := Params.ParamByName( '@RETURN_VALUE' ).Value;
    end;
  end;
end;

procedure TdtmSesFact.StartNewBookYear( aFactNr  : integer );
begin
  with cdsSesFact do
  begin
    Close;
    FetchParams;
    if Params.Count > 0 then
    begin
      cdsSesFact.Params.ParamByName( '@F_DOC_NR' ).Value := aFactNr;
      cdsSesFact.Params.ParamByName( '@ACTION' ).Value := 'R';
      Execute;
    end;
  end;
end;

function TdtmSesFact.WriteRecordsToDatabase(aDataSet: TDataSet): integer;
var
  aResult : integer;
begin
  aResult := 0;
  if aDataSet.RecordCount > 0 then
  begin
    aDataSet.First;
    aResult := WriteRecordToDatabase
      (
      aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger,
      aDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger,
      aDataSet.FieldByName( 'F_VERKOOPREKENING' ).AsInteger,
      aDataSet.FieldByName( 'F_DOCUMENT_NUMMER' ).AsInteger
      );
    while not aDataset.Eof do
    begin
      aDataSet.Next;
      aResult := WriteRecordToDatabase
        (
        aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger,
        aDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger,
        aDataSet.FieldByName( 'F_VERKOOPREKENING' ).AsInteger,
        aDataSet.FieldByName( 'F_DOCUMENT_NUMMER' ).AsInteger
        );
    end;
  end;
  Result := aResult;
end;

function TdtmSesFact.WriteRecordToDatabase(aSessionId, aOrganisationId,
  aSaleNr, aDocNr: integer): integer;
begin
  with cdsSesFact do
  begin
    Close;
    FetchParams;
    Params.ParamByName( '@ACTION' ).Value := 'W';
    Params.ParamByName( '@F_SESSION_ID' ).Value := aSessionId;
    Params.ParamByName( '@F_ORGANISATION_ID' ).Value := aOrganisationId;
    Params.ParamByName( '@F_SALE_NR' ).Value := aSaleNr;
    Params.ParamByName( '@F_DOC_NR' ).Value := aDocNr;
    Params.ParamByName( '@F_ACTIVE' ).Value := 1;
    Execute;
    FetchParams;
    Result := Params.ParamByName( '@RETURN_VALUE' ).Value;
  end;
end;

end.
