{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <T_SES_FACT> records.


  @Name       Form_SesFact_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  06/02/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_SesFact_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxPC, cxControls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxMemo, cxDBEdit,
  cxSpinEdit, cxMaskEdit, cxButtonEdit, cxTextEdit, cxContainer, cxLabel,
  cxCurrencyEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, cxCalendar,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Menus, cxSplitter, Form_EDUListView,
  //dxExEdtr, dxTL, dxDBCtrl, dxCntner, dxDBTL,
  dxBar, cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter;

type
  TfrmSesFact_List = class(TEDUListView)
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_LAND: TcxGridDBColumn;
    cxgrdtblvListF_ALFA_CODE: TcxGridDBColumn;
    cxgrdtblvListF_NAAM: TcxGridDBColumn;
    cxgrdtblvListF_STRAAT: TcxGridDBColumn;
    cxgrdtblvListF_HUISNUMMER: TcxGridDBColumn;
    cxgrdtblvListF_BUSNUMMER: TcxGridDBColumn;
    cxgrdtblvListF_POSTNUMMER: TcxGridDBColumn;
    cxgrdtblvListF_WOONPLAATS: TcxGridDBColumn;
    cxgrdtblvListF_TAAL: TcxGridDBColumn;
    cxgrdtblvListF_TELEFOON_NUMMER: TcxGridDBColumn;
    cxgrdtblvListF_DOCUMENT_NUMMER: TcxGridDBColumn;
    cxgrdtblvListF_DOCUMENT_DATUM: TcxGridDBColumn;
    cxgrdtblvListF_VERVALDATUM: TcxGridDBColumn;
    cxgrdtblvListF_ONZE_REFERTE: TcxGridDBColumn;
    cxgrdtblvListF_REFERTE: TcxGridDBColumn;
    cxgrdtblvListF_BEDRAG_VALUTA: TcxGridDBColumn;
    cxgrdtblvListF_VERKOOPREKENING: TcxGridDBColumn;
    cxgrdtblvListF_ANALYTISCHE_SLEUTEL_1: TcxGridDBColumn;
    cxgrdtblvListF_BOEKHOUDPERIODE: TcxGridDBColumn;
    cxgrdtblvListF_KLANTNUMMER: TcxGridDBColumn;
    procedure FVBFFCListViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxgrdtblvListF_DOCUMENT_NUMMERPropertiesValidate(
      Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
      var Error: Boolean);
  private
    { Private declarations }
//    fOldMaximum : integer;

  protected
    { Protected declarations }
    function ExportGridContents(
              aFileExt, aFilter, aFileName : String;
              aMethod : TPPWFrameWorkSaveGridMethod ) : boolean; override;

  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SesFact, Data_EduMainClient, DateUtils, Unit_EdutecInterfaces,
  Unit_PPWFrameWorkForm;

{ TfrmSesFact_List }


function TfrmSesFact_List.ExportGridContents(aFileExt, aFilter,
  aFileName: String; aMethod: TPPWFrameWorkSaveGridMethod) : boolean;
var
  aDataModule : IEDUSesFactDataModule;
  aResult : boolean;
begin
  // Tempvariable to know when this button is pushed
  // but we still need to ask a question to the user
  // concerning the export because cancel could have been
  // clicked anyway ! ( confusing - i know )
  // After this question we need to export all the records to T_SES_FACT
  // so we can recreate the corresponding lines with the same DOC_NMBR's
  aResult := Inherited ExportGridContents(
            aFileExt, aFilter, aFileName,
            aMethod );
  if aResult then
  begin
    if ( Supports( FrameWorkDataModule, IEDUSesFactDataModule, aDataModule ) ) then
    begin
      aDataModule.WriteRecordsToDatabase( aDataModule.Dataset );
    end;

    Self.Close;
  end;

  Result := aResult;

end;

procedure TfrmSesFact_List.FVBFFCListViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
// var
//   aDataModule : IEDUSesFactDataModule;
begin
  inherited;
{
  if ( Supports( aFrameWorkDataModule, IEDUSesFactDataModule, aDataModule ) ) then
  begin
    fOldMaximum := aDataModule.RetrieveOldMaximum( aDataModule.Dataset );
  end;

  TcxSpinEditProperties(cxgrdtblvListF_DOCUMENT_NUMMER.Properties).MinValue :=
    YearOf(date()) * 10000;
}
end;

procedure TfrmSesFact_List.cxgrdtblvListF_DOCUMENT_NUMMERPropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
{
//  inherited;
  if error then
  begin
    DisplayValue := fOldMaximum;
//      TcxSpinEditProperties(cxgrdtblvListF_DOCUMENT_NUMMER.Properties).MinValue;
    error := False;
  end;
}
end;

end.
