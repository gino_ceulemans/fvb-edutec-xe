inherited frmSesFact_List: TfrmSesFact_List
  Caption = '(Pre)Facturatie'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    ExplicitTop = 385
    ExplicitWidth = 512
  end
  inherited pnlList: TFVBFFCPanel
    ExplicitWidth = 504
    ExplicitHeight = 377
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        OptionsData.Editing = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.IncSearch = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Inactive = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        Styles.Selection = nil
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_KLANTNUMMER: TcxGridDBColumn
          DataBinding.FieldName = 'F_KLANTNUMMER'
          PropertiesClassName = 'TcxSpinEditProperties'
          Width = 105
        end
        object cxgrdtblvListF_LAND: TcxGridDBColumn
          DataBinding.FieldName = 'F_LAND'
          Options.Editing = False
          Options.Focusing = False
          Width = 50
        end
        object cxgrdtblvListF_ALFA_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ALFA_CODE'
          Options.Editing = False
          Options.Focusing = False
          Width = 136
        end
        object cxgrdtblvListF_NAAM: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAAM'
          Options.Editing = False
          Options.Focusing = False
          Width = 52
        end
        object cxgrdtblvListF_STRAAT: TcxGridDBColumn
          DataBinding.FieldName = 'F_STRAAT'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_HUISNUMMER: TcxGridDBColumn
          DataBinding.FieldName = 'F_HUISNUMMER'
          Options.Editing = False
          Options.Focusing = False
          Width = 100
        end
        object cxgrdtblvListF_BUSNUMMER: TcxGridDBColumn
          DataBinding.FieldName = 'F_BUSNUMMER'
          Options.Editing = False
          Options.Focusing = False
          Width = 92
        end
        object cxgrdtblvListF_POSTNUMMER: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTNUMMER'
          PropertiesClassName = 'TcxSpinEditProperties'
          Options.Editing = False
          Options.Focusing = False
          Width = 99
        end
        object cxgrdtblvListF_WOONPLAATS: TcxGridDBColumn
          DataBinding.FieldName = 'F_WOONPLAATS'
          Options.Editing = False
          Options.Focusing = False
          Width = 100
        end
        object cxgrdtblvListF_TAAL: TcxGridDBColumn
          DataBinding.FieldName = 'F_TAAL'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_TELEFOON_NUMMER: TcxGridDBColumn
          DataBinding.FieldName = 'F_TELEFOON_NUMMER'
          Options.Editing = False
          Options.Focusing = False
          Width = 134
        end
        object cxgrdtblvListF_DOCUMENT_NUMMER: TcxGridDBColumn
          DataBinding.FieldName = 'F_DOCUMENT_NUMMER'
          Options.Editing = False
          Options.Focusing = False
          Width = 141
        end
        object cxgrdtblvListF_DOCUMENT_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_DOCUMENT_DATUM'
          Options.Editing = False
          Options.Focusing = False
          Width = 133
        end
        object cxgrdtblvListF_VERVALDATUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_VERVALDATUM'
          Options.Editing = False
          Options.Focusing = False
          Width = 105
        end
        object cxgrdtblvListF_ONZE_REFERTE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ONZE_REFERTE'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_REFERTE: TcxGridDBColumn
          DataBinding.FieldName = 'F_REFERTE'
          Options.Editing = False
          Options.Focusing = False
          Width = 69
        end
        object cxgrdtblvListF_BEDRAG_VALUTA: TcxGridDBColumn
          DataBinding.FieldName = 'F_BEDRAG_VALUTA'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Options.Editing = False
          Options.Focusing = False
          Width = 120
        end
        object cxgrdtblvListF_VERKOOPREKENING: TcxGridDBColumn
          DataBinding.FieldName = 'F_VERKOOPREKENING'
          PropertiesClassName = 'TcxSpinEditProperties'
          Options.Editing = False
          Options.Focusing = False
          Width = 135
        end
        object cxgrdtblvListF_ANALYTISCHE_SLEUTEL_1: TcxGridDBColumn
          DataBinding.FieldName = 'F_ANALYTISCHE_SLEUTEL_1'
          Options.Editing = False
          Options.Focusing = False
          Width = 170
        end
        object cxgrdtblvListF_BOEKHOUDPERIODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_BOEKHOUDPERIODE'
          Options.Editing = False
          Options.Focusing = False
          Width = 137
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
      ExplicitTop = 0
      ExplicitWidth = 504
      ExplicitHeight = 153
      inherited pnlSearchCriteriaButtons: TPanel
        ExplicitTop = 122
        ExplicitWidth = 502
      end
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
end
