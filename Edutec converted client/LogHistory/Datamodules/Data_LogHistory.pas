{*****************************************************************************
  This DataModule will be used for the maintenance of lOGHISTORY.

  @Name       Data_LogHistory
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  19/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_LogHistory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, DB, MConnect, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Provider, ADODB, unit_EdutecInterfaces,
  Datasnap.DSConnect;

type
  TdtmLogHistory = class(TEDUdatamodule, IEduLogHistoryDatamodule)
    cdsListUserName: TStringField;
    cdsListModificationStart: TDateTimeField;
    cdsListModificationEnd: TDateTimeField;
    cdsListChanges: TStringField;
    procedure FVBFFCDataModuleFilterDetail(Sender: TObject; Fields: String;
      Values: Variant; Expression: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses Data_EduMainClient, Data_FVBFFCMainClient;

{$R *.dfm}


procedure TdtmLogHistory.FVBFFCDataModuleFilterDetail(Sender: TObject;
  Fields: String; Values: Variant; Expression: String);
{begin
  inherited;
  // Hier moet er onderscheid gemaakt gaan worden m.b.t de Parameters die doorgegeven
  // moeten worden ...
}var
  aCursorRestorer : IPPWRestorer;
begin
  aCursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

//  ssckData.AppServer.ApplyDetailFilter( cdsList.ProviderName, Expression );
//  cdsList.Open;

// { wordt vervangen door }

  cdsList.Close;
  // Indien "master"module aanwezig dan ff controleren of deze van het juiste type is ...
  if ( Assigned( MasterDataModule ) ) then
  begin
    cdsList.FetchParams;
    // Controle van het type gebeurt hier door functionaliteit van Support aan te roepen ...
    if ( Supports( MasterDataModule, IEDUEnrolmentDataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'ENR';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_ENROL_ID').AsInteger;
    end
    else if ( Supports( MasterDataModule, IEDUSessionDataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'SES';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_SESSION_ID').AsInteger;
    end
    else if ( Supports( MasterDataModule, IEDUOrganisationDataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'ORG';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_ORGANISATION_ID').AsInteger;
    end
    else if ( Supports( MasterDataModule, IEDUPersonDataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'PE';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_PERSON_ID').AsInteger;
    end
    else if ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'PRG';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_PROGRAM_ID').AsInteger;
    end
    else if ( Supports( MasterDataModule, IEDUCenInfrastructureDataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'CEN';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_INFRASTRUCTURE_ID').AsInteger;
    end;
{
    // Moest niet worden geimplementeerd ...
    else if ( Supports( MasterDataModule, IEDU???doc???DataModule ) ) then
    begin
      cdsList.Params.ParamByName('@entity').AsString := 'DOC';
      cdsList.Params.ParamByName('@entityid').AsInteger := MasterDatamodule.RecordDataSet.FieldByName('F_???DOC???_ID').AsInteger;
    end;
}
    cdsList.Open;
  end;

end;

end.
