inherited dtmLogHistory: TdtmLogHistory
  KeyFields = 'UserName'
  ListViewClass = 'TfrmLogHistory_List'
  RecordViewClass = 'TfrmLogHistory_Record'
  Registered = True
  inherited cdsList: TFVBFFCClientDataSet
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftString
        Name = '@entity'
        ParamType = ptInput
        Size = 3
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@entityid'
        ParamType = ptInput
      end>
    object cdsListUserName: TStringField
      DisplayLabel = 'Gebruiker'
      FieldName = 'UserName'
      Size = 129
    end
    object cdsListModificationStart: TDateTimeField
      DisplayLabel = 'Start aanpassing'
      FieldName = 'ModificationStart'
    end
    object cdsListModificationEnd: TDateTimeField
      DisplayLabel = 'Einde aanpassing'
      FieldName = 'ModificationEnd'
    end
    object cdsListChanges: TStringField
      DisplayLabel = 'Aanpassingen'
      FieldName = 'Changes'
      Size = 4096
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmLogHistory'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmLogHistory'
  end
end
