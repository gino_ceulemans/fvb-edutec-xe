inherited frmLogHistory_List: TfrmLogHistory_List
  Caption = 'Historisch overzicht'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListUserName: TcxGridDBColumn
          DataBinding.FieldName = 'UserName'
          Width = 200
        end
        object cxgrdtblvListModificationStart: TcxGridDBColumn
          DataBinding.FieldName = 'ModificationStart'
          Width = 120
        end
        object cxgrdtblvListModificationEnd: TcxGridDBColumn
          DataBinding.FieldName = 'ModificationEnd'
          Width = 120
        end
        object cxgrdtblvListChanges: TcxGridDBColumn
          DataBinding.FieldName = 'Changes'
          Width = 2400
        end
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmLogHistory.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmLogHistory.cdsSearchCriteria
  end
end
