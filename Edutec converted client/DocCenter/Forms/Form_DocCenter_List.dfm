inherited frmDocCenter_List: TfrmDocCenter_List
  Caption = 'Sessie - Lesgevers'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_SES_INSTRUCTOR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SES_INSTRUCTOR_ID'
          Width = 77
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Width = 82
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 198
        end
        object cxgrdtblvListF_CODE: TcxGridDBColumn
          Caption = 'Referentie'
          DataBinding.FieldName = 'F_CODE'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvListF_INSTRUCTOR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INSTRUCTOR_ID'
          Width = 92
        end
        object cxgrdtblvListF_FULLNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FULLNAME'
          Width = 152
        end
        object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SOCSEC_NR'
          Width = 117
        end
        object cxgrdtblvListF_PHONE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE'
        end
        object cxgrdtblvListF_FAX: TcxGridDBColumn
          DataBinding.FieldName = 'F_FAX'
        end
        object cxgrdtblvListF_GSM: TcxGridDBColumn
          DataBinding.FieldName = 'F_GSM'
        end
        object cxgrdtblvListF_EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'F_EMAIL'
          Width = 82
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmDocCenter.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
