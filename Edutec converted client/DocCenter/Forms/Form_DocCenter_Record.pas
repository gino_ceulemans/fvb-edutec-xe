{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <T_DOC_CENTER> records.


  @Name       Form_DocCenter_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/02/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_DocCenter_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxCurrencyEdit, cxDBEdit,
  cxDropDownEdit, cxImageComboBox, cxCalendar, cxCheckBox, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, cxMemo, Menus, cxGraphics, cxSplitter, cxGroupBox;

type
  TfrmDocCenter_Record = class(TEDURecordview)
    cxdbseF_SES_INSTRUCTOR_ID: TFVBFFCDBSpinEdit;
    cxlblF_INFRASTRUCTURE_ID: TFVBFFCLabel;
    cxdbbeF_NAME: TFVBFFCDBButtonEdit;
    cxlblF_INSTRUCTOR_ID1: TFVBFFCLabel;
    cxdbbeF_FULLNAME: TFVBFFCDBButtonEdit;
    cxlblF_SES_INSTRUCTOR_ID: TFVBFFCLabel;
    cxdblblF_SES_INSTRUCTOR_ID: TFVBFFCDBLabel;
    cxlblF_SOCSEC_NR1: TFVBFFCLabel;
    cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit;
    cxdbteF_PHONE: TFVBFFCDBTextEdit;
    cxlblF_PHONE1: TFVBFFCLabel;
    cxlblF_GSM1: TFVBFFCLabel;
    cxdbteF_GSM: TFVBFFCDBTextEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    cxlblF_FAX1: TFVBFFCLabel;
    cxdbteF_FAX: TFVBFFCDBTextEdit;
    cxdbteF_EMAIL: TFVBFFCDBTextEdit;
    cxlblF_NAME: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxlblF_FULLNAME: TFVBFFCLabel;
    cxdblblF_FULLNAME: TFVBFFCDBLabel;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_FULLNAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_DocCenter, Data_EduMainClient,
  unit_EdutecInterfaces, Form_FVBFFCBaseRecordView, Unit_PPWFrameWorkRecordView;

{ TfrmDocCenter_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmDocCenter_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmDocCenter_Record.GetDetailName: String;
begin
  Result := cxdblblF_SES_INSTRUCTOR_ID.Caption + ' ' +
  cxdblblF_NAME.Caption + ' ' + cxdblblF_FULLNAME.Caption;
end;

{*****************************************************************************
  Procedure to enable/disable "master"controls so we can't change the relation
  here.

  @Name       TfrmDocCenter_Record.FVBFFCRecordViewInitialise
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmDocCenter_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  // Controleer hier welke mastermodule er is en zet juiste btnedit af !
  if Assigned( aFrameWorkDataModule.MasterDataModule ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUDocInstructorDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_FULLNAME );
    end
    else if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUSessionDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_NAME );
    end;
  end;

  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbteF_SOCSEC_NR );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbteF_PHONE );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbteF_FAX );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbteF_GSM );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbteF_EMAIL );

end;

{*****************************************************************************
  Procedure to select or view/edit a selected instructor.

  @Name       TfrmDocCenter_Record.cxdbbeF_FULLNAMEPropertiesButtonClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmDocCenter_Record.cxdbbeF_FULLNAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDocCenterDataModule : IEDUDocCenterDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocCenterDataModule, aDocCenterDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aDocCenterDataModule.ShowInstructor( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aDocCenterDataModule.SelectInstructor( srcMain.DataSet );
        end;
      end;
    end;
  end;
end;

{*****************************************************************************
  Procedure to select/view(edit) a selected Session.

  @Name       TfrmDocCenter_Record.cxdbbeF_NAMEPropertiesButtonClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmDocCenter_Record.cxdbbeF_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDocCenterDataModule : IEDUDocCenterDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
  ( Supports( FrameWorkDataModule, IEDUDocCenterDataModule, aDocCenterDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDocCenterDataModule.ShowSession( srcMain.DataSet, rvmEdit );
      end;
      else
      begin
        aDocCenterDataModule.SelectSession( srcMain.DataSet );
      end;
    end;
  end;
end;

end.