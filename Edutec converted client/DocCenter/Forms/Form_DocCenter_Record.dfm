inherited frmDocCenter_Record: TfrmDocCenter_Record
  Caption = 'Sessie - Lesgever'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_SES_INSTRUCTOR_ID: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          Caption = 'Sessie - Lesgever (ID)'
          FocusControl = cxdbseF_SES_INSTRUCTOR_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SES_INSTRUCTOR_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_SES_INSTRUCTOR_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 89
        end
        object cxlblF_NAME: TFVBFFCLabel
          Left = 160
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmDocCenter_Record.F_CENTER_ID'
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          Caption = 'Sessie'
          FocusControl = cxdbseF_SES_INSTRUCTOR_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 160
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmDocCenter_Record.F_CENTER_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 233
        end
        object cxlblF_FULLNAME: TFVBFFCLabel
          Left = 392
          Top = 0
          HelpType = htKeyword
          RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
          Caption = 'Lesgever'
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_FULLNAME: TFVBFFCDBLabel
          Left = 392
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmDocCenter_Record.F_CENTER_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_FULLNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 233
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_SES_INSTRUCTOR_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie - Lesgever (ID)'
            FocusControl = cxdbseF_SES_INSTRUCTOR_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_SES_INSTRUCTOR_ID: TFVBFFCDBSpinEdit
            Left = 160
            Top = 8
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_SES_INSTRUCTOR_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 121
          end
          object cxlblF_INFRASTRUCTURE_ID: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie'
            FocusControl = cxdbbeF_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_NAME: TFVBFFCDBButtonEdit
            Left = 160
            Top = 32
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_NAMEPropertiesButtonClick
            TabOrder = 3
            Width = 465
          end
          object cxlblF_INSTRUCTOR_ID1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmDocCenter_Record.F_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Lesgever'
            FocusControl = cxdbbeF_FULLNAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_FULLNAME: TFVBFFCDBButtonEdit
            Left = 160
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmDocCenter_Record.F_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_FULLNAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_FULLNAMEPropertiesButtonClick
            TabOrder = 5
            Width = 465
          end
          object cxlblF_SOCSEC_NR1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rijksregisternr.'
            FocusControl = cxdbteF_SOCSEC_NR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit
            Left = 160
            Top = 80
            HelpType = htKeyword
            DataBinding.DataField = 'F_SOCSEC_NR'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 7
            Width = 160
          end
          object cxdbteF_PHONE: TFVBFFCDBTextEdit
            Left = 160
            Top = 104
            HelpType = htKeyword
            DataBinding.DataField = 'F_PHONE'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 8
            Width = 160
          end
          object cxlblF_PHONE1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Telefoon'
            FocusControl = cxdbteF_PHONE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_GSM1: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'GSM'
            FocusControl = cxdbteF_GSM
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_GSM: TFVBFFCDBTextEdit
            Left = 160
            Top = 128
            HelpType = htKeyword
            DataBinding.DataField = 'F_GSM'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 11
            Width = 160
          end
          object cxlblF_EMAIL1: TFVBFFCLabel
            Left = 328
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Email'
            FocusControl = cxdbteF_EMAIL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_FAX1: TFVBFFCLabel
            Left = 328
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_FAX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Fax'
            FocusControl = cxdbteF_FAX
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FAX: TFVBFFCDBTextEdit
            Left = 464
            Top = 104
            HelpType = htKeyword
            DataBinding.DataField = 'F_FAX'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 14
            Width = 160
          end
          object cxdbteF_EMAIL: TFVBFFCDBTextEdit
            Left = 464
            Top = 128
            HelpType = htKeyword
            DataBinding.DataField = 'F_EMAIL'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 15
            Width = 160
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmDocCenter.cdsRecord
  end
end
