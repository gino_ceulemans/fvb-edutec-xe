{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <T_DOC_CENTER> records.


  @Name       Form_DocCenter_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/02/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_DocCenter_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxTextEdit;

type
  TfrmDocCenter_List = class(TEDUListview)
    cxgrdtblvListF_SES_INSTRUCTOR_ID: TcxGridDBColumn;
    cxgrdtblvListF_INSTRUCTOR_ID: TcxGridDBColumn;
    cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListF_PHONE: TcxGridDBColumn;
    cxgrdtblvListF_FAX: TcxGridDBColumn;
    cxgrdtblvListF_GSM: TcxGridDBColumn;
    cxgrdtblvListF_EMAIL: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_FULLNAME: TcxGridDBColumn;
    cxgrdtblvListF_CODE: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_DocCenter;

end.