{*****************************************************************************
  This DataModule will be used for the maintenance of <T_SES_INSTRUCTOR>.

  @Name       Data_DocCenter
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/07/2008   Ivan Van den Bossche Added F_CODE to cdsList.
  01/02/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_DocCenter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, Unit_PPWFrameWorkActions, MConnect, unit_EdutecInterfaces,
  Datasnap.DSConnect;

type
  TdtmDocCenter = class(TEDUDataModule, IEDUDocCenterDataModule)
    cdsListF_SES_INSTRUCTOR_ID: TIntegerField;
    cdsListF_INSTRUCTOR_ID: TIntegerField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_GSM: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_FULLNAME: TStringField;
    cdsListF_PROGRAM_ID: TIntegerField;
    cdsRecordF_SES_INSTRUCTOR_ID: TIntegerField;
    cdsRecordF_INSTRUCTOR_ID: TIntegerField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_GSM: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_FULLNAME: TStringField;
    cdsRecordF_PROGRAM_ID: TIntegerField;
    cdsListF_CODE: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SelectInstructor ( aDataSet : TDataSet ); virtual;
    procedure SelectSession    ( aDataSet : TDataSet ); virtual;
    procedure ShowInstructor   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowSession      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

  public
    { Public declarations }

  end;

implementation

uses Data_Instructor, Data_SesSession;

{$R *.dfm}


{*****************************************************************************
  Procedure to make sure updates are restricted to this table and only this.

  @Name       TdtmDocCenter.prvListGetTableName
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmDocCenter.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_SES_INSTRUCTOR';
end;

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmEnrolment.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     cheuten
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmDocCenter.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_SES_INSTRUCTOR_ID' ).AsInteger :=
    ssckData.AppServer.GetNewRecordID;
end;

procedure TdtmDocCenter.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUSessionDataModule ) ) then
    begin
      CopySessionFieldValues( MasterDataModule.RecordDataset,
                                        aDataSet,
                                        False,
                                        critSessionToSesInstructor );
    end
    else if ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) then
    begin
      CopyInstructorFieldValues( MasterDataModule.RecordDataset,
                                 aDataSet,
                                 False,
                                 critInstructorToSesInstructor );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Session Records.

  @Name       TdtmDocCenter.SelectSession
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TdtmDocCenter.SelectSession(aDataSet: TDataSet);
var
  aIDField : TField;
  aInstField : TField;
  aWhere   : String;
  aWhereInst   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aCopyTo := critSessionToSesInstructor;
  aIDField := aDataSet.FindField( 'F_SESSION_ID' );
  aInstField := aDataSet.FindField( 'F_INSTRUCTOR_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_SESSION_ID <> ' + aIDField.AsString;
  end;
  if ( Assigned( aInstField ) ) and
     ( not ( aInstField.IsNull ) ) then
  begin
    aWhereInst := 'F_PROGRAM_ID IN ( SELECT DISTINCT(F_PROGRAM_ID) ' +
                  'FROM T_PROG_INSTRUCTOR ' +
                  'WHERE F_INSTRUCTOR_ID = ' + aInstField.AsString + ')';
  end;

  if aWhere = '' then
  begin
    aWhere := aWhereInst;
  end
  else
  begin
    aWhere := aWhere + ' AND ' + aWhereInst;
  end;

  TdtmSesSession.SelectSession( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Select an Instructor.

  @Name       TdtmDocCenter.SelectInstructor
  @author     cheuten
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TdtmDocCenter.SelectInstructor(aDataSet: TDataSet);
var
  aIDField : TField;
  aProgField : TField;
  aWhere, aWhereProg   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critInstructorToSesInstructor;

  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and
     not ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) ) then
  begin
    { Get the ID Field }
    aIDField := aDataSet.FindField( 'F_INSTRUCTOR_ID' );
    aProgField := aDataSet.FindField( 'F_PROGRAM_ID' );
    aWhere   := '';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Diploma isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_INSTRUCTOR_ID <> ' + aIDField.AsString;
    end;

    if ( Assigned( aProgField ) ) and
       ( not ( aProgField.IsNull ) ) then
    begin
      aWhereProg := ' F_INSTRUCTOR_ID IN ( ' +
      ' SELECT F_INSTRUCTOR_ID FROM T_PROG_INSTRUCTOR '+
      ' WHERE F_PROGRAM_ID  = ' + aProgField.AsString + ')';
    end;

    if aWhere = '' then
    begin
      aWhere := aWhereProg;
    end
    else
    begin
      aWhere := aWhere + ' AND ' + aWhereProg;
    end;

    TdtmInstructor.SelectInstructor( aDataSet, aWhere, True, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Session.

  @Name       TdtmDocCenter.ShowSession
  @author     cheuten
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TdtmDocCenter.ShowSession(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_SESSION_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmSesSession.ShowSession( aDataSet,
                               aRecordViewMode,
                               critSessionToSesInstructor
                               );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show an Instructor.

  @Name       TdtmDocCenter.ShowInstructor
  @author     cheuten
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmDocCenter.ShowInstructor(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and
     not ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) ) then
  begin
    { Get the ID Field }
    aIDField := aDataSet.FindField( 'F_INSTRUCTOR_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmInstructor.ShowInstructor( aDataSet,
                                     aRecordViewMode,
                                     critInstructorToSesInstructor
                                     );
    end;
  end;
end;

procedure TdtmDocCenter.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataset.FieldByName( 'F_START_DATE' ).AsDateTime := Date;
end;

end.
