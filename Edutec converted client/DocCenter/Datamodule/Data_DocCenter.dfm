inherited dtmDocCenter: TdtmDocCenter
  KeyFields = 'F_SES_INSTRUCTOR_ID'
  ListViewClass = 'TfrmDocCenter_List'
  RecordViewClass = 'TfrmDocCenter_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_SES_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_SES_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever (ID)'
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'RSZ nr'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object cdsListF_PHONE: TStringField
      DisplayLabel = 'Tel'
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object cdsListF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object cdsListF_GSM: TStringField
      DisplayLabel = 'Gsm'
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object cdsListF_EMAIL: TStringField
      DisplayLabel = 'e-mail'
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie (ID)'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_FULLNAME: TStringField
      DisplayLabel = 'Lesgever'
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object cdsListF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma (ID)'
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = []
    end
    object cdsListF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = []
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_SES_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_SES_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsRecordF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object cdsRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object cdsRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object cdsRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object cdsRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object cdsRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = []
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmDocCenter'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmDocCenter'
  end
end
