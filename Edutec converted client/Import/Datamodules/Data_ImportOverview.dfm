inherited dtmImportOverview: TdtmImportOverview
  AutoOpenDataSets = False
  ListViewClass = 'TfrmImportOverview_List'
  RecordViewClass = 'TfrmImportOverview_Record'
  Registered = True
  Height = 319
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_EDUCATION_IDENTITY: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'F_EDUCATION_IDENTITY'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsListF_SENT: TBooleanField
      FieldName = 'F_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object cdsListF_FLAG_DESCRIPTION: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_FLAG_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      Size = 32
    end
    object cdsListF_ERROR_MESSAGE: TStringField
      DisplayLabel = 'Foutbericht'
      FieldName = 'F_ERROR_MESSAGE'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsListF_HOURS_TOT: TIntegerField
      DisplayLabel = '# uren'
      FieldName = 'F_HOURS_TOT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_EXT_EDUCATION_ID: TIntegerField
      DisplayLabel = 'OpleidingsID'
      FieldName = 'F_EXT_EDUCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_EDUCATION_DESCRIPTION: TStringField
      DisplayLabel = 'Opleiding'
      FieldName = 'F_EDUCATION_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      Size = 75
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Startdatum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Einddatum'
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PERSON_IDENTITY: TAutoIncField
      DisplayLabel = 'Person ID'
      FieldName = 'F_PERSON_IDENTITY'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
    end
    object cdsListF_PERSON_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_PERSON_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'RR nr'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object cdsListF_COMPANY_IDENTITY: TAutoIncField
      DisplayLabel = 'BedrijfID'
      FieldName = 'F_COMPANY_IDENTITY'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
    end
    object cdsListF_COM_NAME: TStringField
      DisplayLabel = 'Bedrijf'
      FieldName = 'F_COM_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_EDUCATION_IDENTITY: TAutoIncField
      FieldName = 'F_EDUCATION_IDENTITY'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsRecordF_SENT: TBooleanField
      FieldName = 'F_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object cdsRecordF_FLAG_DESCRIPTION: TStringField
      FieldName = 'F_FLAG_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
      Size = 32
    end
    object cdsRecordF_ERROR_MESSAGE: TStringField
      FieldName = 'F_ERROR_MESSAGE'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsRecordF_HOURS_TOT: TIntegerField
      FieldName = 'F_HOURS_TOT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_EXT_EDUCATION_ID: TIntegerField
      FieldName = 'F_EXT_EDUCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_EDUCATION_DESCRIPTION: TStringField
      FieldName = 'F_EDUCATION_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      Size = 75
    end
    object cdsRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PERSON_IDENTITY: TAutoIncField
      FieldName = 'F_PERSON_IDENTITY'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
    end
    object cdsRecordF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object cdsRecordF_COMPANY_IDENTITY: TAutoIncField
      FieldName = 'F_COMPANY_IDENTITY'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
    end
    object cdsRecordF_COM_NAME: TStringField
      FieldName = 'F_COM_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_ERROR_DESCRIPTION: TStringField
      FieldName = 'F_ERROR_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      Size = 200
    end
    object cdsSearchCriteriaF_FLAG: TStringField
      FieldName = 'F_FLAG'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmImportOverview'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmImportOverview'
  end
  object cdsStatusLookup: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvStatus'
    RemoteServer = dspConnection
    Left = 392
    Top = 160
    object cdsStatusLookupF_FLAG: TStringField
      FieldName = 'F_FLAG'
      FixedChar = True
      Size = 1
    end
    object cdsStatusLookupF_DESCRIPTION: TStringField
      FieldName = 'F_DESCRIPTION'
      Size = 32
    end
  end
end
