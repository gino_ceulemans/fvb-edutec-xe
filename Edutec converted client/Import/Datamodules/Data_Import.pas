unit Data_Import;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_EDUDataModule, DB, DBClient, MConnect, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Provider, ADODB, unit_EdutecInterfaces,
  Unit_PPWFrameWorkActions, Datasnap.DSConnect;

type
  TdtmImport = class(TEDUDataModule, IEDUImportDataModule)
    cdsSearchCriteriaF_START_DATE: TDateTimeField;
    cdsSearchCriteriaF_END_DATE: TDateTimeField;
    cdsSearchCriteriaF_PERSON_IDENTITY: TIntegerField;
    cdsSearchCriteriaF_DATE_ADDED: TDateTimeField;
    cdsSearchCriteriaF_PERSON_CONSTRUCT_ID: TIntegerField;
    cdsSearchCriteriaF_ORG_CONSTRUCT_ID: TIntegerField;
    cdsSearchCriteriaF_PERSON_FIRSTNAME: TStringField;
    cdsSearchCriteriaF_PERSON_LASTNAME: TStringField;
    cdsSearchCriteriaF_COM_NAME: TStringField;
    cdsListF_SESSION_START_DATE: TDateTimeField;
    cdsListF_SESSION_END_DATE: TDateTimeField;
    cdsListF_SESSION_DURATION_HOURS: TIntegerField;
    cdsListF_COMPANY_NAME: TStringField;
    cdsListF_COMPANY_PHONE: TStringField;
    cdsListF_COMPANY_FAX: TStringField;
    cdsListF_COMPANY_EMAIL: TStringField;
    cdsListF_PERSON_DATEBIRTH: TDateTimeField;
    cdsListF_MSG: TStringField;
    cdsListF_EDUCATION_DESCRIPTION: TStringField;
    cdsListF_EDGARD_PROCESS_COMMENT: TStringField;
    cdsListF_EDGARD_SESSION_ID: TIntegerField;
    cdsListF_IMPORT_ID: TIntegerField;
    cdsDefaultSearchCriteria: TFVBFFCClientDataSet;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    cdsDefaultSearchCriteriaF_NAME: TStringField;
    cdsListF_COMPANY_ADDRESS: TStringField;
    cdsListF_PERSON_ADDRESS: TStringField;
    cdsListF_PERSON_NAME: TStringField;
    cdsListF_PERSON_FUNCTION: TStringField;
    cdsSessionsForProgram: TFVBFFCClientDataSet;
    cdsSessionsForProgramF_SESSION_ID: TIntegerField;
    cdsListF_PERSON_CONSTRUCT_ID: TIntegerField;
    cdsListF_PERSON_LASTNAME: TStringField;
    cdsListF_PERSON_FIRSTNAME: TStringField;
    cdsListF_EDGARD_PROCESS_STATUS: TStringField;
    procedure cdsListAfterDelete(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
    
    procedure XmlEducationSetProcessed(AImportId: Integer; const AMsgType: WideString);

    procedure CloseDefSearchCriteriaDS;
    function GetDefaultSearchCriteria: TClientDataSet;
    procedure GetSessionCandidateForImpRecord(const AImportId: Integer; const AMsgType: string; const AStartDate: TDateTime; var ASessionId: string);
    function IsListViewActionAllowed( aAction : TPPWFrameWorkListViewAction ) : Boolean; override;

  public
    { Public declarations }
    property DefaultSearchCriteria: TClientDataSet read GetDefaultSearchCriteria;

  end;

var
  dtmImport: TdtmImport;

implementation

{$R *.dfm}

{ TdtmImport }

function TdtmImport.IsListViewActionAllowed(aAction: TPPWFrameWorkListViewAction): Boolean;
var
  aAllowed    : Boolean;
begin
  aAllowed := Inherited IsListViewActionAllowed( aAction );

  if ( Assigned( aAction ) ) then
    if ( {( aAction is TPPWFrameWorkListViewDeleteAction ) or }
          // Disabled by Ivan Van den Bossche on 12-SEP-2007
          // We would like to cancel import records.
          
         ( aAction is TPPWFrameWorkListViewEditAction ) or
         ( aAction is TPPWFrameWorkListViewConsultAction ) or
         ( aAction is TPPWFrameWorkListViewAddAction ) )
    then
    begin
      aAllowed := False;
    end;

  Result := aAllowed;
end;

function TdtmImport.ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
begin
  Result := ssckData.AppServer.ProcessSingleRecord(EducationIdentity, SessionIdentity);
end;

procedure TdtmImport.XmlEducationSetProcessed(AImportId: Integer;
  const AMsgType: WideString);
begin
  ssckData.AppServer.XmlEducationSetProcessed(AImportId, AMsgType);
end;

procedure TdtmImport.CloseDefSearchCriteriaDS;
begin
  if cdsDefaultSearchCriteria.Active then
    cdsDefaultSearchCriteria.Close;
end;

function TdtmImport.GetDefaultSearchCriteria: TClientDataSet;
begin
  if not cdsDefaultSearchCriteria.Active then
  begin
    cdsDefaultSearchCriteria.CreateDataSet;
    cdsDefaultSearchCriteria.Append;
  end;

  if not (cdsDefaultSearchCriteria.State in [dsInsert, dsEdit]) then
    cdsDefaultSearchCriteria.Edit;

  cdsDefaultSearchCriteria.FieldByName('F_NAME').Value := cdsListF_EDUCATION_DESCRIPTION.AsString;
  cdsDefaultSearchCriteria.FieldByName('F_START_DATE').Value := cdsListF_SESSION_START_DATE.AsDateTime;

  cdsDefaultSearchCriteria.Post;

  Result := cdsDefaultSearchCriteria;
end;

procedure TdtmImport.cdsListAfterDelete(DataSet: TDataSet);
begin
  inherited;
    cdsList.Tag := 1;

end;

procedure TdtmImport.cdsListAfterPost(DataSet: TDataSet);
begin
  cdsList.Tag := 1;
  inherited;

end;


// Look for session candidates suited to be linked with selected import record.
// Returned sessions will be used to discover suitable non-construct (manually registered by Edutec) persons
procedure TdtmImport.GetSessionCandidateForImpRecord(const AImportId: Integer; const AMsgType: string;
const AStartDate: TDateTime; var ASessionId: string);
var
  aList: TStringList;
begin
  cdsSessionsForProgram.Params.Clear;
  cdsSessionsForProgram.FetchParams;
  cdsSessionsForProgram.Params.ParamByName('@ATransfertId').Value := AImportId;
  cdsSessionsForProgram.Params.ParamByName('@AMsgType').Value := AMsgType;
  cdsSessionsForProgram.Params.ParamByName('@AStartDate').Value := AStartDate;
  cdsSessionsForProgram.Open;
  try

    if cdsSessionsForProgram.RecordCount > 0 then
    begin

      aList := TStringList.Create;
      try
        aList.Delimiter := ',';

        while not cdsSessionsForProgram.Eof do
        begin
          aList.Add( cdsSessionsForProgram.FieldByName('F_SESSION_ID').AsString );
          cdsSessionsForProgram.Next;
        end;

        // Assign delimited text to ASessionId
        ASessionId := aList.DelimitedText;

      finally
          aList.Free;
      end;

    end;

  finally
    cdsSessionsForProgram.Close;
  end;

end;

end.
