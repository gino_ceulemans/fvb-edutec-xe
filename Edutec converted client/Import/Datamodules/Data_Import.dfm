inherited dtmImport: TdtmImport
  KeyFields = 'F_EDUCATION_IDENTITY'
  ListViewClass = 'TfrmImport_List'
  Registered = True
  inherited cdsList: TFVBFFCClientDataSet
    Tag = 1
    BeforePost = nil
    BeforeDelete = nil
    OnNewRecord = nil
    object cdsListF_SESSION_START_DATE: TDateTimeField
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_END_DATE: TDateTimeField
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_DURATION_HOURS: TIntegerField
      FieldName = 'F_SESSION_DURATION_HOURS'
      ProviderFlags = []
    end
    object cdsListF_COMPANY_NAME: TStringField
      DisplayWidth = 80
      FieldName = 'F_COMPANY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_COMPANY_PHONE: TStringField
      FieldName = 'F_COMPANY_PHONE'
      ProviderFlags = []
    end
    object cdsListF_COMPANY_FAX: TStringField
      FieldName = 'F_COMPANY_FAX'
      ProviderFlags = []
    end
    object cdsListF_COMPANY_EMAIL: TStringField
      DisplayWidth = 20
      FieldName = 'F_COMPANY_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_PERSON_DATEBIRTH: TDateTimeField
      FieldName = 'F_PERSON_DATEBIRTH'
      ProviderFlags = []
    end
    object cdsListF_MSG: TStringField
      FieldName = 'F_MSG'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
      Size = 3
    end
    object cdsListF_EDUCATION_DESCRIPTION: TStringField
      DisplayWidth = 50
      FieldName = 'F_EDUCATION_DESCRIPTION'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_EDGARD_PROCESS_COMMENT: TStringField
      DisplayWidth = 50
      FieldName = 'F_EDGARD_PROCESS_COMMENT'
      ProviderFlags = []
      Size = 1024
    end
    object cdsListF_EDGARD_SESSION_ID: TIntegerField
      FieldName = 'F_EDGARD_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_IMPORT_ID: TIntegerField
      FieldName = 'F_IMPORT_ID'
    end
    object cdsListF_COMPANY_ADDRESS: TStringField
      DisplayWidth = 50
      FieldName = 'F_COMPANY_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 294
    end
    object cdsListF_PERSON_ADDRESS: TStringField
      DisplayWidth = 50
      FieldName = 'F_PERSON_ADDRESS'
      ProviderFlags = []
      ReadOnly = True
      Size = 282
    end
    object cdsListF_PERSON_NAME: TStringField
      DisplayWidth = 50
      FieldName = 'F_PERSON_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 80
    end
    object cdsListF_PERSON_FUNCTION: TStringField
      FieldName = 'F_PERSON_FUNCTION'
      ProviderFlags = []
      ReadOnly = True
      Size = 8
    end
    object cdsListF_PERSON_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_PERSON_CONSTRUCT_ID'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object cdsListF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object cdsListF_EDGARD_PROCESS_STATUS: TStringField
      FieldName = 'F_EDGARD_PROCESS_STATUS'
      ProviderFlags = []
      ReadOnly = True
      Size = 1
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
    end
    object cdsSearchCriteriaF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object cdsSearchCriteriaF_DATE_ADDED: TDateTimeField
      FieldName = 'F_DATE_ADDED'
    end
    object cdsSearchCriteriaF_PERSON_IDENTITY: TIntegerField
      FieldName = 'F_PERSON_IDENTITY'
    end
    object cdsSearchCriteriaF_PERSON_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_PERSON_CONSTRUCT_ID'
    end
    object cdsSearchCriteriaF_ORG_CONSTRUCT_ID: TIntegerField
      FieldName = 'F_ORG_CONSTRUCT_ID'
    end
    object cdsSearchCriteriaF_PERSON_FIRSTNAME: TStringField
      FieldName = 'F_PERSON_FIRSTNAME'
    end
    object cdsSearchCriteriaF_PERSON_LASTNAME: TStringField
      FieldName = 'F_PERSON_LASTNAME'
    end
    object cdsSearchCriteriaF_COM_NAME: TStringField
      FieldName = 'F_COM_NAME'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmImport'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmImport'
  end
  object cdsDefaultSearchCriteria: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    AutoOpen = False
    Left = 280
    Top = 216
    object DateTimeField1: TDateTimeField
      FieldName = 'F_START_DATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object cdsDefaultSearchCriteriaF_NAME: TStringField
      FieldName = 'F_NAME'
      Size = 255
    end
  end
  object cdsSessionsForProgram: TFVBFFCClientDataSet
    Tag = 1
    Aggregates = <>
    AutoCalcFields = False
    FetchOnDemand = False
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@ATransfertId'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@AMsgType'
        ParamType = ptInput
        Size = 3
      end
      item
        DataType = ftDateTime
        Name = '@AStartDate'
        ParamType = ptInput
      end>
    ProviderName = 'prvGetSessionsForProgram'
    RemoteServer = dspConnection
    AfterPost = cdsListAfterPost
    AfterDelete = cdsListAfterDelete
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 392
    Top = 56
    object cdsSessionsForProgramF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
  end
end
