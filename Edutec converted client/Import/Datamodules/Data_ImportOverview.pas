{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_ImportOverview
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  22/11/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_ImportOverview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule, DB,
  MConnect, Unit_FVBFFCDBComponents, Unit_PPWFrameWorkComponents, Provider,
  ADODB, Unit_PPWFrameWorkActions, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmImportOverview = class(TEDUDatamodule, IEDUImportOverviewModule)
    cdsSearchCriteriaF_START_DATE: TDateTimeField;
    cdsSearchCriteriaF_END_DATE: TDateTimeField;
    cdsSearchCriteriaF_ERROR_DESCRIPTION: TStringField;
    cdsListF_EDUCATION_IDENTITY: TAutoIncField;
    cdsListF_SENT: TBooleanField;
    cdsListF_FLAG: TStringField;
    cdsListF_FLAG_DESCRIPTION: TStringField;
    cdsListF_ERROR_MESSAGE: TStringField;
    cdsListF_HOURS_TOT: TIntegerField;
    cdsListF_EXT_EDUCATION_ID: TIntegerField;
    cdsListF_EDUCATION_DESCRIPTION: TStringField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_PERSON_IDENTITY: TAutoIncField;
    cdsListF_PERSON_FIRSTNAME: TStringField;
    cdsListF_PERSON_LASTNAME: TStringField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_COMPANY_IDENTITY: TAutoIncField;
    cdsListF_COM_NAME: TStringField;
    cdsRecordF_EDUCATION_IDENTITY: TAutoIncField;
    cdsRecordF_SENT: TBooleanField;
    cdsRecordF_FLAG: TStringField;
    cdsRecordF_FLAG_DESCRIPTION: TStringField;
    cdsRecordF_ERROR_MESSAGE: TStringField;
    cdsRecordF_HOURS_TOT: TIntegerField;
    cdsRecordF_EXT_EDUCATION_ID: TIntegerField;
    cdsRecordF_EDUCATION_DESCRIPTION: TStringField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_PERSON_IDENTITY: TAutoIncField;
    cdsRecordF_PERSON_FIRSTNAME: TStringField;
    cdsRecordF_PERSON_LASTNAME: TStringField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_COMPANY_IDENTITY: TAutoIncField;
    cdsRecordF_COM_NAME: TStringField;
    cdsStatusLookup: TFVBFFCClientDataSet;
    cdsStatusLookupF_FLAG: TStringField;
    cdsStatusLookupF_DESCRIPTION: TStringField;
    cdsSearchCriteriaF_FLAG: TStringField;
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    function IsListViewActionAllowed ( aAction: TPPWFrameWorkListViewAction ) : Boolean; override;
    function ProcessSingleRecord(EducationIdentity: Integer): String;
  public
    function GetStatusLookupDataset: TClientDataset;
    function GetDescriptionForStatus (const status: String): String;
  end;

const
  ERROR_FLAG = 'F';
  RECEIVED_FLAG = 'O';
  PROCESSED_FLAG = 'P';

implementation

{$R *.dfm}

{*****************************************************************************
  Set default filtering on last days
  @Name       TdtmImportOverview.cdsSearchCriteriaNewRecord
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TdtmImportOverview.cdsSearchCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;
  Dataset.FieldByName('F_END_DATE').AsDateTime := Now;
  Dataset.FieldByName('F_START_DATE').AsDateTime := now - 3;
  Dataset.FieldByName('F_FLAG').AsString := ERROR_FLAG;
end;

function TdtmImportOverview.GetDescriptionForStatus(
  const status: String): String;
begin
  result := ssckData.AppServer.GetDescriptionForStatus (status);
end;

function TdtmImportOverview.GetStatusLookupDataset: TClientDataset;
begin
  result := cdsStatusLookup;
end;

function TdtmImportOverview.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
begin
  result := inherited IsListViewActionAllowed (aAction);
  if (aAction is TPPWFrameWorkListViewConsultAction) or
     (aAction is TPPWFrameWorkListViewAddAction) or
     (aAction is TPPWFrameWorkListViewDeleteAction) or
     (aAction is TPPWFrameWorkListViewEditAction) then
  begin
    result := false;
  end;
end;

function TdtmImportOverview.ProcessSingleRecord(
  EducationIdentity: Integer): String;
begin
  result := ssckData.AppServer.ProcessSingleRecord(EducationIdentity);
end;

end.
