{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_ImportOverview_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  22/11/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_ImportOverview_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView, Menus,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, cxControls, cxSplitter, Unit_FVBFFCDevExpress,
  dxNavBarCollns, dxNavBarBase, dxNavBar, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons;

type
  TfrmImportOverview_Record = class(TEDURecordView)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ImportOverview;

{ TfrmImportOverview_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmImportOverview_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmImportOverview_Record.GetDetailName: String;
begin
  Result := Inherited GetDetailName;
end;

end.