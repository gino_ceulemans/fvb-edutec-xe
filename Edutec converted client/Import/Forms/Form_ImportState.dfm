object frmImportState: TfrmImportState
  Left = 273
  Top = 329
  Width = 407
  Height = 231
  Caption = 'Import status overzicht'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object FVBFFCPanel1: TFVBFFCPanel
    Left = 0
    Top = 0
    Width = 399
    Height = 172
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 0
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clWindow
    StyleClientArea.BackColor2 = 15254445
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object cxlblFVBFFCLabel1: TFVBFFCLabel
      Left = 24
      Top = 93
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Aantal niet-correct verwerkt (fouten)'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
    object seNotCorrect: TFVBFFCSpinEdit
      Left = 240
      Top = 91
      TabStop = False
      ParentFont = False
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescReadOnly
      TabOrder = 1
      Width = 121
    end
    object seCorrect: TFVBFFCSpinEdit
      Left = 240
      Top = 51
      TabStop = False
      ParentFont = False
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescReadOnly
      TabOrder = 0
      Width = 121
    end
    object cxlblFVBFFCLabel2: TFVBFFCLabel
      Left = 24
      Top = 53
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Aantal correct verwerkt'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
  end
  object pnlBottom: TFVBFFCPanel
    Left = 0
    Top = 172
    Width = 399
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 0
    ParentColor = True
    TabOrder = 1
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object pnlBottomButtons: TFVBFFCPanel
      Left = 63
      Top = 0
      Width = 336
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      BorderWidth = 0
      ParentColor = True
      TabOrder = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object cxbtnOK: TFVBFFCButton
        Left = 113
        Top = 4
        Width = 104
        Height = 25
        Action = acOk
        Cancel = True
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object cxbtnCancel: TFVBFFCButton
        Left = 225
        Top = 2
        Width = 104
        Height = 25
        Action = acCancel
        Cancel = True
        Default = True
        ModalResult = 2
        TabOrder = 1
        OnClick = cxbtnCancelClick
      end
    end
  end
  object alButtons: TFVBFFCActionList
    Left = 192
    Top = 16
    object acOk: TAction
      Caption = '&Ok'
      OnExecute = acOkExecute
    end
    object acCancel: TAction
      Caption = '&Cancel'
    end
  end
end
