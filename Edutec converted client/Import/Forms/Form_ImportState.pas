unit Form_ImportState;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents,
  StdCtrls, cxButtons, Unit_FVBFFCDevExpress, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxControls, cxContainer, cxEdit, cxLabel, ExtCtrls,
  Unit_FVBFFCFoldablePanel;

type
  TfrmImportState = class(TForm)
    FVBFFCPanel1: TFVBFFCPanel;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    seNotCorrect: TFVBFFCSpinEdit;
    seCorrect: TFVBFFCSpinEdit;
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    cxbtnCancel: TFVBFFCButton;
    alButtons: TFVBFFCActionList;
    acOk: TAction;
    acCancel: TAction;
    procedure acOkExecute(Sender: TObject);
    procedure cxbtnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportState: TfrmImportState;

implementation

{$R *.dfm}

procedure TfrmImportState.acOkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmImportState.cxbtnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
