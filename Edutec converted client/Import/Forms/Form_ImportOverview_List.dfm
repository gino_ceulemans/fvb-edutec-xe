inherited frmImportOverview_List: TfrmImportOverview_List
  Left = 248
  Top = 203
  Width = 766
  Height = 527
  Caption = 'Import overzicht'
  OnCreate = FVBFFCListViewCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 459
    Width = 758
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 542
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 654
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 750
    Height = 451
    inherited cxgrdList: TcxGrid
      Top = 166
      Width = 750
      Height = 285
      inherited cxgrdtblvList: TcxGridDBTableView
        OptionsSelection.MultiSelect = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_EDUCATION_IDENTITY: TcxGridDBColumn
          DataBinding.FieldName = 'F_EDUCATION_IDENTITY'
        end
        object cxgrdtblvListF_FLAG_DESCRIPTION: TcxGridDBColumn
          DataBinding.FieldName = 'F_FLAG_DESCRIPTION'
          Width = 90
        end
        object cxgrdtblvListF_ERROR_MESSAGE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ERROR_MESSAGE'
          Width = 200
        end
        object cxgrdtblvListF_HOURS_TOT: TcxGridDBColumn
          DataBinding.FieldName = 'F_HOURS_TOT'
        end
        object cxgrdtblvListF_EXT_EDUCATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_EXT_EDUCATION_ID'
          Width = 95
        end
        object cxgrdtblvListF_EDUCATION_DESCRIPTION: TcxGridDBColumn
          DataBinding.FieldName = 'F_EDUCATION_DESCRIPTION'
          Width = 200
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
        end
        object cxgrdtblvListF_PERSON_IDENTITY: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_IDENTITY'
          Width = 90
        end
        object cxgrdtblvListF_PERSON_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_FIRSTNAME'
          Width = 150
        end
        object cxgrdtblvListF_PERSON_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_LASTNAME'
          Width = 150
        end
        object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SOCSEC_NR'
          Width = 80
        end
        object cxgrdtblvListF_COMPANY_IDENTITY: TcxGridDBColumn
          DataBinding.FieldName = 'F_COMPANY_IDENTITY'
          Width = 90
        end
        object cxgrdtblvListF_COM_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COM_NAME'
          Width = 200
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 141
      Width = 750
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 137
      Width = 750
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 750
      Height = 137
      ExpandedHeight = 137
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 106
        Width = 748
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 580
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 668
        end
      end
      object cxlblF_END_DATE1: TFVBFFCLabel
        Left = 16
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Datums tussen'
        FocusControl = cxdbdeF_END_DATE1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_START_DATE1: TFVBFFCDBDateEdit
        Left = 120
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_START_DATE'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 2
        Width = 136
      end
      object cxlblF_START_DATE1: TFVBFFCLabel
        Left = 272
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'en'
        FocusControl = cxdbdeF_START_DATE1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_END_DATE1: TFVBFFCDBDateEdit
        Left = 302
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_END_DATE'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 4
        Width = 136
      end
      object FVBFFCLabel1: TFVBFFCLabel
        Left = 16
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        FocusControl = cxdbdeF_END_DATE1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object FVBFFCLabel2: TFVBFFCLabel
        Left = 16
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Errorbericht'
        FocusControl = cxdbdeF_END_DATE1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
        Left = 120
        Top = 80
        DataBinding.DataField = 'F_ERROR_DESCRIPTION'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 7
        Width = 319
      end
      object FVBFFCDBLookupComboBox1: TFVBFFCDBLookupComboBox
        Left = 120
        Top = 56
        DataBinding.DataField = 'F_FLAG'
        DataBinding.DataSource = srcSearchCriteria
        Properties.KeyFieldNames = 'F_FLAG'
        Properties.ListColumns = <
          item
            FieldName = 'F_DESCRIPTION'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = srcStatusLookup
        TabOrder = 8
        Width = 137
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 758
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 455
    Width = 758
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 754
    Height = 451
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 451
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarButton1: TdxBarButton
      Action = acRetry
      Category = 0
      Hint = 'Probeer geselecteerde fouten opnieuw'
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton1
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
  end
  object alImportOverview: TFVBFFCActionList
    Left = 248
    Top = 352
    object acRetry: TAction
      Caption = 'Probeer geselecteerde fouten opnieuw'
      OnExecute = acRetryExecute
    end
  end
  object srcStatusLookup: TFVBFFCDataSource
    DataSet = dtmImportOverview.cdsStatusLookup
    Left = 332
    Top = 396
  end
end
