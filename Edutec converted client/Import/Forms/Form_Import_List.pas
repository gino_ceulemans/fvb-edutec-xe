unit Form_Import_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  ActnList, Unit_FVBFFCComponents, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxDBEdit, cxContainer, cxLabel, Unit_PPWFrameWorkInterfaces;

resourcestring
  rsDifferentSessionsSelected='U heeft verschillende soorten sessies geselecteerd of dezelfde soort sessies maar met een verschillende startdatum.';
  rsConfirmDeletionImportRecords='Bent u zeker dat u de gekozen Import Records wilt verwijderen?';
  //rsError2beLinkable1='geen sessie gevonden met correcte status';
  //rsError2beLinkable2='sessie gevonden maar reeds volzet';
  rsErrorNoSession='Er is geen geschikte sessie gevonden.';
  
  rsConfirmMarkAsProcessed='U heeft gekozen om persoon %s te markeren als zijnde verwerkt.  Bent u zeker dat u dit wilt doen?';

type
  TfrmImport_List = class(TEDUListView, IIPWFrameWorkFormDeleteMultiSelect)
    alImport: TFVBFFCActionList;
    acLinkToSession: TAction;
    dxBarButton1: TdxBarButton;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE1: TFVBFFCDBDateEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE1: TFVBFFCDBDateEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME1: TFVBFFCDBTextEdit;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    FVBFFCLabel3: TFVBFFCLabel;
    FVBFFCDBTextEdit2: TFVBFFCDBTextEdit;
    cxgrdtblvListF_IMPORT_ID: TcxGridDBColumn;
    cxgrdtblvListF_EDUCATION_DESCRIPTION: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_HOURS: TcxGridDBColumn;
    cxgrdtblvListF_EDGARD_PROCESS_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_COMPANY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_COMPANY_PHONE: TcxGridDBColumn;
    cxgrdtblvListF_COMPANY_FAX: TcxGridDBColumn;
    cxgrdtblvListF_COMPANY_EMAIL: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_DATEBIRTH: TcxGridDBColumn;
    srcDefaultSearchCriteria: TFVBFFCDataSource;
    cxgrdtblvListF_COMPANY_ADDRESS: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_ADDRESS: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_FUNCTION: TcxGridDBColumn;
    
    dxBarButton2: TdxBarButton;
    acMarkAsProcessed: TAction;
    procedure acLinkToSessionExecute(Sender: TObject);
    
    procedure dxbpmnGridPopup(Sender: TObject);
    procedure acMarkAsProcessedExecute(Sender: TObject);

  private
    { Private declarations }
    FSessionID: Integer;
    FPersonID: Integer;
    FMaxParticipants: integer;
    FNbrParticipants: integer;

    procedure SetSessionId;
    procedure DeleteRecord;
    procedure DeleteSelectedRecords;

  public
    function GetFilterString : String; override;
  end;

var
  frmImport_List: TfrmImport_List;

implementation

uses
  Data_Import, Data_EduMainClient, Unit_PPWFrameWorkForm, DBClient,
  Form_FVBFFCBaseListView, Data_EDUDataModule, unit_EdutecInterfaces,
  cxGridDBDataDefinitions, Unit_PPWFrameWorkListView, Form_ImportState;

{$R *.dfm}

procedure TfrmImport_List.acLinkToSessionExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
  aDataModule     : IEDUImportDataModule;
  aDataSet        : TClientDataSet;
  lcv             : Integer;
  aDefaultSearchCriteria : TClientDataSet;
  aSession        : String;
  aStartSessionDate : TDateTime;
begin
  if ( Supports( FrameWorkDataModule, IEDUImportDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aDataSet := TClientDataSet.Create( Self );

    try
      aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
      aDataController := TcxGridDBDataController( ActiveGridView.DataController );

      if(aViewController.SelectedRecordCount <> 0) then
      begin
          // Initialize default search criteria client dataset
          aDefaultSearchCriteria := aDataModule.DefaultSearchCriteria; // Turn search criteria on by default
          try
              // Check for different session names and start dates
              { Loop over each selected record in the grid }
              for lcv := 0 to Pred( aViewController.SelectedRecordCount ) do
              begin
                srcMain.DataSet.Bookmark := aDataController.GetSelectedBookmark( lcv );

                if lcv=0 then
                begin
                  aSession := srcMain.DataSet.FieldByName('F_EDUCATION_DESCRIPTION').AsString;
                  aStartSessionDate := srcMain.DataSet.FieldByName('F_SESSION_START_DATE').AsDateTime;
                end else begin
                  if (aSession <> srcMain.DataSet.FieldByName('F_EDUCATION_DESCRIPTION').AsString)
                      or (aStartSessionDate <> srcMain.DataSet.FieldByName('F_SESSION_START_DATE').AsDateTime)
                  then
                  begin
                    // Since different import record sessions have been selected, please don't filter
                    aDefaultSearchCriteria := nil;
                    MessageDlg(rsDifferentSessionsSelected, mtWarning, [mbOk], 0);
                    Refresh;
                    break;
                  end;
                end;
              end;

              // Allow the user to select a session id and filter records temporary when aDefaultSearchCriteria <> nil
              //if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesSession', 'F_STATUS_ID IN ( 1, 4, 6, 7, 8, 9, 10, 11 )'
              if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesSession', 'F_STATUS_ID IN ( 1, 4, 6, 7, 8, 9, 11 )'
                    , aDataSet, aDefaultSearchCriteria, False ) = mrOk ) then
              begin
                // Assign selected session id to selected import records
                FSessionID := aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
                FMaxParticipants := aDataSet.FieldByName( 'F_MAXIMUM' ).AsInteger;
                FNbrParticipants := aDataSet.FieldByName( 'F_NO_PARTICIPANTS' ).AsInteger;
                LoopThroughSelectedRecords(SetSessionId);

                RefreshData;
                cxgrdtblvList.DataController.ClearSelection;

              end;

          finally
            // Close Default search criteria clientdataset
            aDataModule.CloseDefSearchCriteriaDS;
          end;
      end;
    finally
      FreeAndNil( aDataSet );
    end;
  end;
end;

{*****************************************************************************
  @Name       TfrmImport_List.acMarkAsProcessedExecute
  @author     Ivan Van den Bossche (06/08/2013)
  @param      None
  @return     None
  @Exception  None
  @See        None
  @Descr      Procedure updates selected import record state as processed.
  @History      Author                      Description
  06/08/2013    Ivan Van den Bossche        Initial creation.
******************************************************************************}
procedure TfrmImport_List.acMarkAsProcessedExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
  aDataModule     : IEDUImportDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUImportDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
      aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
      aDataController := TcxGridDBDataController( ActiveGridView.DataController );

      if(aViewController.SelectedRecordCount = 1) then
      begin
        if MessageDlg(Format( rsConfirmMarkAsProcessed, [ srcMain.DataSet.FieldByName('F_PERSON_NAME').AsString ]),
                                                          mtConfirmation, [mbYes,mbNo], 0) = mrYes then
        begin
          // Mark as processed
          aDataModule.XmlEducationSetProcessed(srcMain.DataSet.FieldByName('F_IMPORT_ID').AsInteger,
                                               srcMain.DataSet.FieldByName('F_MSG').AsString);

          RefreshData;
          cxgrdtblvList.DataController.ClearSelection;
        end;
      end;
  end;
end;

{*****************************************************************************
  @Name       TfrmImport_List.DeleteSelectedRecords
  @author     Ivan Van den Bossche (12/09/2007)
  @param      None
  @return     None
  @Exception  None
  @See        None
  @Descr      Function deletes all selected records from Grid & database
******************************************************************************}
procedure TfrmImport_List.DeleteRecord;
var
    oldTag: Integer;
begin
    assert (self.srcMain.DataSet <> nil);

    oldTag := srcMain.Dataset.Tag;
    try
      srcMain.DataSet.Tag := 1;
      srcMain.DataSet.Delete;
    finally
      srcMain.Dataset.Tag := oldTag;
    end;
end;

procedure TfrmImport_List.DeleteSelectedRecords;
var
  oldcursor: TCursor;
begin
  if MessageDlg(rsConfirmDeletionImportRecords, mtConfirmation, [mbYes,mbNo], 0)=mrYes then
  begin
    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;
      LoopThroughSelectedRecords(DeleteRecord);
      cxgrdtblvList.DataController.ClearSelection;
    finally
      Screen.Cursor := oldcursor;
    end;
  end;
end;

function TfrmImport_List.GetFilterString: String;
var
  aFilterString : String;
  aDateString   : String;
  startdatefilter, enddatefilter: TDatetime;
begin
  startdatefilter := EncodeDate (1900, 1, 1);
  enddatefilter   := EncodeDate (2039, 12,31);

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).IsNull ) then startdatefilter := FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).AsDateTime;
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).IsNull ) then enddatefilter := FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).AsDateTime;

  AddDateBetweenCondition (aDateString, 'F_SESSION_START_DATE', startdatefilter, enddatefilter);
  AddFilterCondition( aFilterString, aDateString, 'AND' );

  { Add the Condition for the Person Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PERSON_LASTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_PERSON_LASTNAME', FSearchCriteriaDataSet.FieldByName( 'F_PERSON_LASTNAME' ).AsString );
  end;

  { Add the Condition for the Person Firstname }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PERSON_FIRSTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_PERSON_FIRSTNAME', FSearchCriteriaDataSet.FieldByName( 'F_PERSON_FIRSTNAME' ).AsString );
  end;

  { Add the Condition for the Company Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_COM_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_COMPANY_NAME', FSearchCriteriaDataSet.FieldByName( 'F_COM_NAME' ).AsString );
  end;

  Result := aFilterString;
end;

procedure TfrmImport_List.SetSessionId;
begin
  // Assign selected session id to import record(s)
  if FMaxParticipants > FNbrParticipants then
  begin
    srcMain.DataSet.Edit;
    srcMain.DataSet.FieldByName('F_EDGARD_SESSION_ID').Value := FSessionID;
    srcMain.DataSet.Post;
    Inc(FNbrParticipants);
  end;
end;

procedure TfrmImport_List.dxbpmnGridPopup(Sender: TObject);
begin
  inherited;

    assert(srcMain.DataSet.Active);

    if TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount = 1 then
    begin
      acMarkAsProcessed.Enabled := True;
    end else begin
      acMarkAsProcessed.Enabled := False;
    end;
end;

end.
