{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_ImportOverview_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  22/11/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_ImportOverview_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxDropDownEdit, cxImageComboBox, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxCalendar, cxContainer, cxLabel, ActnList, Unit_FVBFFCComponents,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox;

type
  TfrmImportOverview_List = class(TEDUListView)
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE1: TFVBFFCDBDateEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE1: TFVBFFCDBDateEdit;
    FVBFFCLabel1: TFVBFFCLabel;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    alImportOverview: TFVBFFCActionList;
    acRetry: TAction;
    dxBarButton1: TdxBarButton;
    cxgrdtblvListF_EDUCATION_IDENTITY: TcxGridDBColumn;
    cxgrdtblvListF_FLAG_DESCRIPTION: TcxGridDBColumn;
    cxgrdtblvListF_ERROR_MESSAGE: TcxGridDBColumn;
    cxgrdtblvListF_HOURS_TOT: TcxGridDBColumn;
    cxgrdtblvListF_EXT_EDUCATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_EDUCATION_DESCRIPTION: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_IDENTITY: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListF_COMPANY_IDENTITY: TcxGridDBColumn;
    cxgrdtblvListF_COM_NAME: TcxGridDBColumn;
    FVBFFCDBLookupComboBox1: TFVBFFCDBLookupComboBox;
    srcStatusLookup: TFVBFFCDataSource;
    procedure acRetryExecute(Sender: TObject);
    procedure FVBFFCListViewCreate(Sender: TObject);
  private
    procedure RetrySelectedRecords;
  public
    function GetFilterString : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ImportOverview, Form_FVBFFCBaseListView, unit_EdutecInterfaces, Unit_PPWFrameWorkClasses;

{ TfrmImportOverview_List }

function TfrmImportOverview_List.GetFilterString: String;
var
  aFilterString : String;
  aDateString   : String;
  startDate, endDate: TDateTime;
begin
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ERROR_DESCRIPTION' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_ERROR_MESSAGE', FSearchCriteriaDataSet.FieldByName( 'F_ERROR_DESCRIPTION' ).AsString );
  end;

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).IsNull ) or
     not ( FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).IsNull ) then
  begin //TODO: it must be possible to do this in a more efficient way !
    if FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).IsNull then
      startDate := EncodeDate(1990, 1, 1)
    else
      startDate := FSearchCriteriaDataset.FieldByName ('F_START_DATE').AsDateTime;

    if FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).IsNull then
      endDate := EncodeDate(2049, 12, 31)
    else
      endDate := FSearchCriteriaDataset.FieldByName ('F_END_DATE').AsDateTime;

    AddDateBetweenCondition ( aDateString, 'F_START_DATE',
                              startDate, endDate );
    AddDateBetweenCondition ( aDateString, 'F_END_DATE',
                              startDate, endDate );
    AddFilterCondition( aFilterString, aDateString, 'AND' );
  end;

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_FLAG').IsNull ) then
  begin
    AddStringEqualFilterCondition(aFilterString, 'F_FLAG', FSearchCriteriaDataset.FieldByName( 'F_FLAG' ).AsString );
  end;

  Result := aFilterString;
end;

procedure TfrmImportOverview_List.acRetryExecute(Sender: TObject);
var
  aCursor : IPPWRestorer;
begin
  inherited;
  aCursor := TPPWCursorRestorer.Create( crSQLWait );
  LoopThroughSelectedRecords (RetrySelectedRecords);
end;

procedure TfrmImportOverview_List.RetrySelectedRecords;
var
  oldTag: Integer;
  aDatamodule: IEDUImportOverviewModule;
  flag: String;
begin
  assert (srcMain.Dataset <> nil);
  if not supports (FrameworkDatamodule, IEDUImportOverviewModule, aDatamodule) then
  begin
    assert (false);
  end;

  oldTag := srcMain.Dataset.Tag;
  try

    //Set the state to 'O' again so that we can reprocess again
    if (srcMain.DataSet.FieldByName('F_FLAG').AsString = ERROR_FLAG) then
    begin
      srcMain.Dataset.Tag := 1;
      srcMain.Dataset.Edit;
      srcMain.Dataset.FieldByName('F_FLAG').AsString := RECEIVED_FLAG;
      srcMain.Dataset.Post;
    end;

    //Proces the record again
    flag := aDatamodule.ProcessSingleRecord(srcMain.DataSet.FieldByName('F_EDUCATION_IDENTITY').AsInteger);

    //We set the status to the status returned by the stored procedure
    //without doing ApplyUpdates
    if trim(flag) <> '' then
    begin
      srcMain.Dataset.Tag := 0;
      srcMain.Dataset.Edit;
      srcMain.Dataset.FieldByName('F_FLAG').AsString := flag;
      srcMain.Dataset.FieldByName('F_FLAG_DESCRIPTION').AsString := aDataModule.GetDescriptionForStatus (flag);
      if flag = PROCESSED_FLAG then
      begin
        srcMain.Dataset.FieldByName('F_ERROR_MESSAGE').AsString := '';
      end;
      srcMain.Dataset.Post;
    end;
    
  finally
    srcMain.Dataset.Tag := oldTag;
  end;
end;

procedure TfrmImportOverview_List.FVBFFCListViewCreate(Sender: TObject);
var
  aDatamodule: IEDUImportOverviewModule;
begin
  inherited;
  if not supports (FrameworkDatamodule, IEDUImportOverviewModule, aDatamodule) then
  begin
    assert (false);
  end;

  srcStatusLookup.DataSet := aDatamodule.GetStatusLookupDataset;
  srcStatusLookup.Dataset.Open;
end;

end.
