inherited frmImport_List: TfrmImport_List
  Left = 535
  Top = 80
  Width = 833
  Height = 639
  Caption = 'Import'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 571
    Width = 825
    DesignSize = (
      825
      41)
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 609
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 721
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 817
    Height = 563
    inherited cxgrdList: TcxGrid
      Top = 199
      Width = 817
      Height = 364
      PopupMenu = FVBFFCBaseListView.dxbpmnGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = True
        OptionsData.Deleting = True
        OptionsSelection.MultiSelect = True
        OptionsView.GroupByBox = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_IMPORT_ID: TcxGridDBColumn
          Caption = 'ImportID'
          DataBinding.FieldName = 'F_IMPORT_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
        end
        object cxgrdtblvListF_EDUCATION_DESCRIPTION: TcxGridDBColumn
          Caption = 'Sessie'
          DataBinding.FieldName = 'F_EDUCATION_DESCRIPTION'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 160
        end
        object cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn
          Caption = 'Startdatum'
          DataBinding.FieldName = 'F_SESSION_START_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Width = 79
        end
        object cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn
          Caption = 'Einddatum'
          DataBinding.FieldName = 'F_SESSION_END_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Width = 79
        end
        object cxgrdtblvListF_SESSION_HOURS: TcxGridDBColumn
          Caption = 'Aantal uren'
          DataBinding.FieldName = 'F_SESSION_DURATION_HOURS'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Width = 42
        end
        object cxgrdtblvListF_EDGARD_PROCESS_COMMENT: TcxGridDBColumn
          Caption = 'Foutmelding'
          DataBinding.FieldName = 'F_EDGARD_PROCESS_COMMENT'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 160
        end
        object cxgrdtblvListF_COMPANY_NAME: TcxGridDBColumn
          Caption = 'Bedrijfsnaam'
          DataBinding.FieldName = 'F_COMPANY_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 244
        end
        object cxgrdtblvListF_COMPANY_ADDRESS: TcxGridDBColumn
          Caption = 'Adres Bedrijf'
          DataBinding.FieldName = 'F_COMPANY_ADDRESS'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 216
        end
        object cxgrdtblvListF_COMPANY_PHONE: TcxGridDBColumn
          Caption = 'Telefoon'
          DataBinding.FieldName = 'F_COMPANY_PHONE'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 91
        end
        object cxgrdtblvListF_COMPANY_FAX: TcxGridDBColumn
          Caption = 'Fax'
          DataBinding.FieldName = 'F_COMPANY_FAX'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 86
        end
        object cxgrdtblvListF_COMPANY_EMAIL: TcxGridDBColumn
          Caption = 'Email'
          DataBinding.FieldName = 'F_COMPANY_EMAIL'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 147
        end
        object cxgrdtblvListF_PERSON_NAME: TcxGridDBColumn
          Caption = 'Naam Persoon'
          DataBinding.FieldName = 'F_PERSON_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 153
        end
        object cxgrdtblvListF_PERSON_ADDRESS: TcxGridDBColumn
          Caption = 'Adres Persoon'
          DataBinding.FieldName = 'F_PERSON_ADDRESS'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 208
        end
        object cxgrdtblvListF_PERSON_DATEBIRTH: TcxGridDBColumn
          Caption = 'Geboortedatum'
          DataBinding.FieldName = 'F_PERSON_DATEBIRTH'
          PropertiesClassName = 'TcxDateEditProperties'
        end
        object cxgrdtblvListF_PERSON_FUNCTION: TcxGridDBColumn
          Caption = 'Functie Persoon'
          DataBinding.FieldName = 'F_PERSON_FUNCTION'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 121
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 165
      Width = 817
      Height = 30
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 195
      Width = 817
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 817
      Height = 165
      ExpandedHeight = 165
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 132
        Width = 815
        Height = 32
        DesignSize = (
          815
          32)
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 647
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 735
        end
      end
      object cxlblF_END_DATE1: TFVBFFCLabel
        Left = 8
        Top = 34
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Opleiding tussen'
        FocusControl = cxdbdeF_END_DATE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_START_DATE1: TFVBFFCDBDateEdit
        Left = 112
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_START_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 2
        Width = 136
      end
      object cxlblF_START_DATE1: TFVBFFCLabel
        Left = 256
        Top = 34
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'En'
        FocusControl = cxdbdeF_START_DATE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_END_DATE1: TFVBFFCDBDateEdit
        Left = 329
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_END_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 4
        Width = 136
      end
      object cxlblF_NAME2: TFVBFFCLabel
        Left = 256
        Top = 58
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Voornaam'
        FocusControl = cxdbteF_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME1: TFVBFFCDBTextEdit
        Left = 328
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_PERSON_FIRSTNAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 6
        Width = 137
      end
      object FVBFFCLabel2: TFVBFFCLabel
        Left = 8
        Top = 58
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam persoon'
        FocusControl = FVBFFCDBTextEdit1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
        Left = 112
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_PERSON_LASTNAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 8
        Width = 137
      end
      object FVBFFCLabel3: TFVBFFCLabel
        Left = 8
        Top = 82
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Bedrijf'
        FocusControl = FVBFFCDBTextEdit2
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object FVBFFCDBTextEdit2: TFVBFFCDBTextEdit
        Left = 112
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_COM_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 10
        Width = 137
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 825
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 567
    Width = 825
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 821
    Height = 563
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 563
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxbbDelete: TdxBarButton
      ShortCut = 16430
    end
    object dxBarButton1: TdxBarButton
      Action = acLinkToSession
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = acMarkAsProcessed
      Category = 0
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton1
        Visible = True
      end
      item
        Item = dxBarButton2
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
    OnPopup = dxbpmnGridPopup
  end
  object alImport: TFVBFFCActionList
    Left = 276
    Top = 388
    object acLinkToSession: TAction
      Caption = 'Schrijf in op sessie'
      OnExecute = acLinkToSessionExecute
    end
    object acLinkPersonConstruct2Edgard: TAction
      Caption = 'Koppel Construct Persoon aan Edgard Persoon'
    end
    object acMarkAsProcessed: TAction
      Caption = 'Als verwerkt markeren'
      OnExecute = acMarkAsProcessedExecute
    end
  end
  object srcDefaultSearchCriteria: TFVBFFCDataSource
    DataSet = dtmImport.cdsDefaultSearchCriteria
    Left = 144
    Top = 454
  end
end
