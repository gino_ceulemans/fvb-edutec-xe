{*****************************************************************************
  This unit contains the Main DataModule used by the FVB FFC Client
  Applications.
  
  @Name       Data_MainClient
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  15/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_FVBFFCMainClient;

interface

uses
  Classes, DB, ADODB, AppEvnts, cxEditRepositoryItems,
  cxExtEditRepositoryItems, cxEdit, Unit_FVBFFCDevExpress, ImgList,
  Controls, Unit_FVBFFCComponents, cxContainer, StdActns, ExtActns,
  Unit_PPWFrameWorkActions, ActnList, cxClasses, cxStyles, cxGridTableView,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, System.ImageList, System.Actions;

type
  TdtmFVBFFCMainClient = class(TDataModule)
    cxlfMain: TFVBFFCLookAndFeelController;
    cxsrMain: TFVBFFCStyleRepository;
    cxsBackground: TcxStyle;
    cxsContent: TcxStyle;
    cxsContentEven: TcxStyle;
    cxsContentOdd: TcxStyle;
    cxsFilterBox: TcxStyle;
    cxsFooter: TcxStyle;
    cxsGroup: TcxStyle;
    cxsGroupByBox: TcxStyle;
    cxsHeader: TcxStyle;
    cxsInactive: TcxStyle;
    cxsIncSearch: TcxStyle;
    cxsIndicator: TcxStyle;
    cxsPreview: TcxStyle;
    cxsSelection: TcxStyle;
    GridTableViewStyleSheetDevExpress: TcxGridTableViewStyleSheet;
    alMain: TFVBFFCActionList;
    acListViewAdd: TPPWFrameWorkListViewAddAction;
    acListViewDelete: TPPWFrameWorkListViewDeleteAction;
    acListViewEdit: TPPWFrameWorkListViewEditAction;
    acListViewConsult: TPPWFrameWorkListViewConsultAction;
    acListViewRefresh: TPPWFrameWorkListViewRefreshAction;
    acGridPrint: TPPWFrameWorkListViewGridPrint;
    acGridExportHTML: TPPWFrameWorkListViewGridExportToHTML;
    acGridExportXLS: TPPWFrameWorkListViewGridExportToXLS;
    acGridExportXML: TPPWFrameWorkListViewGridExportToXML;
    acGridCustomiseColumns: TPPWFrameWorkListViewGridCustomiseColumns;
    acListViewApplyFilter: TPPWFrameWorkListViewFilterAction;
    acInformation: TAction;
    acHelp: TAction;
    acWindowCascade1: TWindowCascade;
    acWindowTileHorizontal1: TWindowTileHorizontal;
    acWindowTileVertical1: TWindowTileVertical;
    acWindowMinimizeAll1: TWindowMinimizeAll;
    acWindowArrange1: TWindowArrange;
    acWindowClose1: TWindowClose;
    acListViewCancelFilter: TAction;
    acRichEditBold1: TRichEditBold;
    acRichEditItalic1: TRichEditItalic;
    acRichEditUnderline1: TRichEditUnderline;
    acRichEditStrikeOut1: TRichEditStrikeOut;
    acRichEditBullets1: TRichEditBullets;
    acRichEditAlignLeft1: TRichEditAlignLeft;
    acRichEditAlignRight1: TRichEditAlignRight;
    acRichEditAlignCenter1: TRichEditAlignCenter;
    FileExit1: TFileExit;
    cxdedtscMain: TFVBFFCDefaultEditStyleController;
    cxescMainCheckBox: TFVBFFCEditStyleController;
    cxescReadOnly: TFVBFFCEditStyleController;
    cxescRequired: TFVBFFCEditStyleController;
    ilGlobalSmall: TFVBFFCImageList;
    ilGlobalLarge: TFVBFFCImageList;
    cxescSearchCriteria: TFVBFFCEditStyleController;
    cserMain: TFVBFFCEditRepository;
    cxeriRecordHeader: TcxEditRepositoryLabel;
    cxeriBooleanCenterAlign: TcxEditRepositoryCheckBoxItem;
    cxeriSearchCriteriaLabels: TcxEditRepositoryLabel;
    cxeriPassWord: TcxEditRepositoryTextItem;
    cxeriShowSelectAddLookup: TcxEditRepositoryButtonItem;
    cxeriShowSelectLookup: TcxEditRepositoryButtonItem;
    cxeriSelectLookup: TcxEditRepositoryButtonItem;
    cxeriDateTime: TcxEditRepositoryDateItem;
    cxeriDate: TcxEditRepositoryDateItem;
    cxeriBooleanLeftAlign: TcxEditRepositoryCheckBoxItem;
    cxeriMemo: TcxEditRepositoryMemoItem;
    cxeriRichEdit: TcxEditRepositoryRichItem;
    cxeriRichEditReadOnly: TcxEditRepositoryRichItem;
    aeApplicationEvents1: TApplicationEvents;
    adocnnMain: TADOConnection;
    cxediPrimaryKey: TcxEditRepositorySpinItem;
    cseriPrimaryKey: TcxEditRepositoryTextItem;
    cserMainTextItem1: TcxEditRepositoryTextItem;
    cserMainTextItem2: TcxEditRepositoryTextItem;
    cxeriShowSelectLookupReadOnly: TcxEditRepositoryButtonItem;
    cxeriURL: TcxEditRepositoryHyperLinkItem;
    cxeriURLReadOnly: TcxEditRepositoryHyperLinkItem;
    cxeriEMail: TcxEditRepositoryHyperLinkItem;
    cxeriEMailReadOnly: TcxEditRepositoryHyperLinkItem;
    cxeriBedrag: TcxEditRepositoryCurrencyItem;
    cxeriBedragReadOnly: TcxEditRepositoryCurrencyItem;
    cxeriMemoReadOnly: TcxEditRepositoryMemoItem;
    procedure acListViewCancelFilterExecute(Sender: TObject);
    procedure acListViewEditAddDeleteConsultUpdate(Sender: TObject);
  end;

var
  dtmFVBFFCMainClient: TdtmFVBFFCMainClient;

implementation

uses
  Unit_FVBFFCInterfaces, Unit_PPWFrameworkController, SysUtils;

{$R *.dfm}

{ TdtmFVBFFCMainClient }

procedure TdtmFVBFFCMainClient.acListViewCancelFilterExecute(Sender: TObject);
var
  aFVBFFCListView : IFVBFFCListView;
begin
  if ( Assigned( PPWController.ActiveListViewForm ) and
       Supports( PPWController.ActiveListViewForm, IFVBFFCListView, aFVBFFCListView ) ) then
  begin
    aFVBFFCListView.ClearFilterCriteria;
  end;
end;

{*****************************************************************************
  @Name       TdtmFVBFFCMainClient.acListViewEditAddDeleteUpdate
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TdtmFVBFFCMainClient.acListViewEditAddDeleteConsultUpdate(
  Sender: TObject);
var
  aDataModule : IFVBFFCDataModule;
  aAction     : TPPWFrameWorkListViewAction;
begin
  if ( Sender is TPPWFrameWorkListViewAction ) then
  begin
    aAction := TPPWFrameWorkListViewAction( Sender );
    aAction.Enabled := Assigned( PPWController.ActiveDataModule ) and
                       Supports( PPWController.ActiveDataModule, IFVBFFCDataModule, aDataModule ) and
                       aDataModule.IsListViewActionAllowed( aAction );
  end;
end;

end.

















