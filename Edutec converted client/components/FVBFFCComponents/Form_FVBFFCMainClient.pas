unit Form_FVBFFCMainClient;

interface

uses
  Windows, cxGraphics, dxBarExtItems, dxBar, Classes, Controls, cxControls,
  dxStatusBar, Unit_FVBFFCDevExpress, Forms, Data_FVBFFCMainClient,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxStatusBarPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, cxClasses;

type
  TfrmFVBFFCMainClient = class(TForm)
    dxBarManager1: TdxBarManager;
    dxbrsiFile: TdxBarSubItem;
    dxbbEdit: TdxBarButton;
    dxbbView: TdxBarButton;
    dxbbAdd: TdxBarButton;
    dxbbDelete: TdxBarButton;
    dxbbRefreshData: TdxBarButton;
    dxbbCustomiseColumns: TdxBarButton;
    dxbbPrintList: TdxBarButton;
    dxbbExportXLS: TdxBarButton;
    dxbbExportHTML: TdxBarButton;
    dxbbExportXML: TdxBarButton;
    dxbrsiData: TdxBarSubItem;
    dxbrsiGeneral: TdxBarSubItem;
    dxbbWindowClose: TdxBarButton;
    dxbbTileHorizontally: TdxBarButton;
    dxbbTileVertically: TdxBarButton;
    dxbbCascade: TdxBarButton;
    dxbbArrangeAll: TdxBarButton;
    dxbbMinimizeAll: TdxBarButton;
    dxbrsiWindow: TdxBarSubItem;
    sbrMain: TFVBFFCStatusBar;
    dxBarListWindows: TdxBarListItem;
    Maintenance: TdxBarSubItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarListWindowsGetData(Sender: TObject);
    procedure dxBarListWindowsClick(Sender: TObject);
  private
    { Private declarations }
  protected
      procedure LinkComponentsToActions; virtual;
      function GetMainDataModule : TdtmFVBFFCMainClient; virtual;
  public
    constructor Create (aOwner: TComponent); override;
    property MainDataModule : TdtmFVBFFCMainClient read GetMainDataModule;
  end;

var
  frmFVBFFCMainClient: TfrmFVBFFCMainClient;

implementation

uses
  Unit_FVBFFCInterfaces;

{$R *.dfm}

procedure TfrmFVBFFCMainClient.FormClose(Sender: TObject; var Action: TCloseAction);
var
  lcv : Integer;
begin
  for lcv := Pred( MDIChildCount ) downto 0 do
  begin
    MDIChildren[ lcv ].Close;
    Application.ProcessMessages;
  end;
end;

procedure TfrmFVBFFCMainClient.dxBarListWindowsGetData(Sender: TObject);
begin
  with dxBarListWindows do
  begin
    ItemIndex := Items.IndexOfObject(ActiveMDIChild);
  end;
end;

procedure TfrmFVBFFCMainClient.dxBarListWindowsClick(Sender: TObject);
begin
  with dxBarListWindows do
  begin
    TCustomForm(Items.Objects[ItemIndex]).Show;
  end;
end;


{*****************************************************************************
  Name           : TfrmFVBFFCMainClient.Create
  Author         : Wim Lambrechts
  Arguments      : aOwner: TComponent
  Return Values  : None
  Exceptions     : None
  Description    : Links the visual components to the actions because it seems
                   they sometimes disappear                                            
  History        :

  Date         By             Description
  ----         --             -----------
  14-jul-2005  WL             Initial creation
 *****************************************************************************}
constructor TfrmFVBFFCMainClient.Create(aOwner: TComponent);
begin
  inherited;
  LinkComponentsToActions;
end;

{*****************************************************************************
  Name           : TfrmFVBFFCMainClient.LinkComponentsToActions
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    :                                            
  History        :

  Date         By             Description
  ----         --             -----------
  14-jul-2005  WL             Initial creation
 *****************************************************************************}
procedure TfrmFVBFFCMainClient.LinkComponentsToActions;
begin
  { Link the BarManager }
(*  dxBarManager1.LargeImages   := MainDataModule.ilGlobalLarge;
  dxBarManager1.Images        := MainDataModule.ilGlobalSmall;

  { Link shortcut buttons to correct actions }
  dxbbEdit.action             := MainDataModule.acListViewEdit;
  dxbbView.action             := MainDataModule.acListViewConsult;
  dxbbAdd.action              := MainDataModule.acListViewAdd;
  dxbbDelete.action           := MainDataModule.acListViewDelete;
  dxbbRefreshData.Action      := MainDatamodule.acListViewRefresh;
  dxbbCustomiseColumns.action := MainDataModule.acGridCustomiseColumns;
  dxbbPrintList.action        := MainDataModule.acGridPrint;
  dxbbExportXLS.action        := MainDataModule.acGridExportXLS;
  dxbbExportHTML.action       := MainDataModule.acGridExportHTML;
  dxbbExportXML.action        := MainDataModule.acGridExportXML;
*)
end;

{*****************************************************************************
  Name           : TfrmFVBFFCMainClient.GetMainDataModule
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    :                                            
  History        :

  Date         By             Description
  ----         --             -----------
  14-jul-2005  WL             Initial creation
 *****************************************************************************}
function TfrmFVBFFCMainClient.GetMainDataModule: TdtmFVBFFCMainClient;
begin
   Result := dtmFVBFFCMainClient;
end;

end.

