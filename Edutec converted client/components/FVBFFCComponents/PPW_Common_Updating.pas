unit PPW_Common_Updating;

interface

uses Classes;

type
  ///Simple class to implement recursive behavior of updating
  TPPWUpdating = class(TObject)
  private
    FUpdating : Integer;
    FOnChanged : TNotifyEvent;
    FSender : TObject;
    FCallChanged : Boolean;
  private
    function GetUpdating: Boolean;
  protected
    procedure DoCallChanged;
  public
    constructor Create(aSender : TObject);
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure Changed;
  public
    property  Sender : TObject          read  FSender     write FSender;
    property  Updating : Boolean        read  GetUpdating;
    property  OnChanged : TNotifyEvent  read  FOnChanged  write FOnChanged;
  end;


implementation

{ TPPWUpdating }

procedure TPPWUpdating.BeginUpdate;
begin
  Inc(FUpdating);
end;

procedure TPPWUpdating.Changed;
begin
  DoCallChanged;
end;

constructor TPPWUpdating.Create(aSender: TObject);
begin
  inherited Create;
  FSender := aSender;
end;

procedure TPPWUpdating.DoCallChanged;
begin
  if FUpdating = 0 then begin
    if assigned(FOnChanged) then begin
      FOnChanged(FSender);
    end;
    FCallChanged := False;
  end else begin
    FCallChanged := True;
  end;
end;

procedure TPPWUpdating.EndUpdate;
begin
  if FUpdating > 0 then begin
    Dec(FUpdating);
    if FUpdating = 0 then DoCallChanged;
  end;
end;

function TPPWUpdating.GetUpdating: Boolean;
begin
  Result := FUpdating > 0;
end;

end.
