unit Data_About;

interface

uses
  Classes, SysUtils, Windows;

type
  TclsAbout = class(TComponent)
  private
    FCompanyName: string;
    FFileDescription: string;
    FFileVersion: string;
    FFileDate : string;
    FFileSize : integer;
    FInternalName: string;
    FLegalCopyright: string;
    FLegalTradeMarks: string;
    FOriginalFilename: string;
    FProductName: string;
    FProductVersion: string;
    FComments: string;
    fWindowsVersion : string;
    fUserName : string;
    fComputerName : string;
    function Execute(const AFilename: string): Boolean;
    function GetFileSize(const FileName: string): LongInt;
    function GetWindowsVersion: string;

  public
    constructor Create(AOwner: TComponent); override;

    property CompanyName: string read FCompanyName;
    property FileDescription: string read FFileDescription;
    property FileVersion: string read FFileVersion;
    property FileDate : string read FFileDate;
    property Size : integer read fFileSize;
    property InternalName: string read FInternalName;
    property LegalCopyright: string read FLegalCopyright;
    property LegalTradeMarks: string read FLegalTradeMarks;
    property OriginalFilename: string read FOriginalFilename;
    property ProductName: string read FProductName;
    property ProductVersion: string read FProductVersion;
    property Comments: string read FComments;
    property WindowsVersion : string read fWindowsVersion;
    property UserName: String read fUserName;
    property ComputerName: String read fComputerName;

  end;

implementation

{ TclsVersion }

constructor TclsAbout.Create(AOwner: TComponent);
var
  buffer : array[0..255] of char;
  size : dword;
begin
  inherited Create(AOwner);
  Execute(ParamStr(0));
  fFileDate := FormatDateTime('c', FileDateToDateTime(FileAge(ParamStr(0))));
  fFileSize := GetFileSize(ParamStr(0));
  fWindowsVersion := GetWindowsVersion;
  // Username
  size := 256;
  GetUserName(buffer, size);
  fUserName := buffer;
  // Computername
  size := MAX_COMPUTERNAME_LENGTH + 1;
  GetComputerName(buffer, size);
  fComputerName := buffer;
end;

function TclsAbout.GetWindowsVersion: string;
begin
  Result := '9X';
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    case Win32MajorVersion of
      3..4: Result := 'NT';
      5: case Win32MinorVersion of
           0: Result := '2K';
           1: Result := 'XP';
         end;
    end;
end;

// Open in Binary mode to use FileSize function
function TclsAbout.GetFileSize(const FileName: string): LongInt;
var
    FileModeSave: BYTE;
    RawFile : FILE OF Byte;
begin
  try
    FileModeSave := System.FileMode;
    System.FileMode := fmOpenRead + fmShareDenyNone;
    AssignFile(RawFile, FileName);
    Reset(RawFile);
    Result := FileSize(RawFile);
    CloseFile(RawFile);
    System.FileMode := FileModeSave
  except
     // avoid compiler warning
    // ShowMessage('Fatal Error: Cannot open ' + FileName + ' (binary)');
    raise
  end
end {GetFileSize};

function TclsAbout.Execute(const AFilename: string): Boolean;
var
  MyHandle: Cardinal;
  MyVersionInfoBuffer: Pointer;
  MyVersionInfoSize: Cardinal;

  function GetVersionInfo(const Key: string): string;

    function SwapLong(L: LongInt): LongInt; assembler;
      asm rol eax, 16;
    end;

  var
    MyVersionValueSize: Cardinal;
    MyVersionValueBuffer: Pointer;
  begin
    VerQueryValue(MyVersionInfoBuffer, '\VarFileInfo\Translation', MyVersionValueBuffer, MyVersionValueSize);
    if VerQueryValue(MyVersionInfoBuffer, PChar(Format('\StringFileInfo\%.8x\%s', [SwapLong(LongInt(MyVersionValueBuffer^)), '\' + Key])), MyVersionValueBuffer, MyVersionValueSize) then
      Result := StrPas(PChar(MyVersionValueBuffer))
    else
      Result := ''
  end;

begin
  FCompanyName := '';
  FFileDescription := '';
  FFileVersion := '';
  FInternalName := '';
  FLegalCopyright := '';
  FLegalTradeMarks := '';
  FOriginalFilename := '';
  FProductName := '';
  FProductVersion := '';
  FComments := '';
  MyVersionInfoSize := GetFileVersionInfoSize(PChar(AFilename), MyHandle);
  Result := MyVersionInfoSize > 0;
  if Result then
  begin
    GetMem(MyVersionInfoBuffer, MyVersionInfoSize);
    if GetFileVersionInfo(PChar(AFilename), MyHandle, MyVersionInfoSize, MyVersionInfoBuffer) then
    begin
      FCompanyName := GetVersionInfo('CompanyName');
      FFileDescription := GetVersionInfo('FileDescription');
      FFileVersion := GetVersionInfo('FileVersion');
      FInternalName := GetVersionInfo('InternalName');
      FLegalCopyright := GetVersionInfo('LegalCopyright');
      FLegalTradeMarks := GetVersionInfo('LegalTradeMarks');
      FOriginalFilename := GetVersionInfo('OriginalFilename');
      FProductName := GetVersionInfo('ProductName');
      FProductVersion := GetVersionInfo('ProductVersion');
      FComments := GetVersionInfo('Comments')
    end;
    FreeMem(MyVersionInfoBuffer, MyVersionInfoSize)
  end
end;

end.
