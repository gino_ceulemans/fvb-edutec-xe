unit Unit_FVBFFCRecordView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  cxEdit, Unit_PPWFrameWorkClasses,
  Dialogs, Unit_PPWFrameWorkRecordView, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkInterfaces;

type
  TFVBFFCRecordView = class( TPPWFrameWorkRecordView, IFVBFFCRecordView, IFVBFFCForm )
  private
    { Private declarations }
    FOnInitialise: TInitialiseFormEvent;
  protected
    function GetLabelStyleController: TcxEditStyleController; virtual;
    function GetReadOnlyEditStyleController: TcxEditStyleController; virtual;
    function GetRequiredEditStyleController: TcxEditStyleController; virtual;

    { IEDUForm Methods }
    procedure DoInitialiseForm( aFrameWorkDataModule : IPPWFrameWorkDataModule );
    procedure CreateFormButtonOnToolbar; virtual;
    procedure DestroyFormButtonOnToolbar; virtual;
    procedure AddOrUpdateWindowMenuItem; virtual;
    procedure RemoveWindowMenuItem; virtual;

    { IEDURecordView Methods }
    function GetActiveDetailForm           : IPPWFrameWorkForm;
    procedure ActivateControl( aControl : TWinControl ); virtual;
    procedure UpdateControlColor( Control : TControl ); override;
    procedure ShowDetailListView( Sender : TObject;
                                  aDataModuleClassName : String;
                                  aDockSite            : TWinControl ); virtual;
    procedure ShowDetailRecordView( aDataModuleClassName : String;
                                    aDockSite            : TWinControl ); virtual;
    procedure ShowRecordDetail( Sender : TObject ); virtual;
    procedure FocusActiveDetailForm;
    procedure HideHeaderAndButtonPanel;
    function VisibleRecordView: IFVBFFCRecordView;
  public
    procedure AddReadOnlyComponent( aComponent : TComponent; aHandled : Boolean = False ); override;
    procedure RemoveReadOnlyComponent( aComponent : TComponent; aHandled : Boolean = False ); override;

    procedure UpdateControls( Container : TWinControl ); override;
    procedure UpdateControlLayout( Control : TControl; ControlOptions : TPPWFrameWorkControlOptions ); override;

    property ActiveDetailForm        : IPPWFrameWorkForm             read GetActiveDetailForm;
    property ReadOnlyStyleController : TcxEditStyleController        read GetReadOnlyEditStyleController;
    property RequiredStyleController : TcxEditStyleController        read GetRequiredEditStyleController;
    property LabelStyleController    : TcxEditStyleController        read GetLabelStyleController;
    { Public declarations }
  published
    property OnInitialise : TInitialiseFormEvent   read  FOnInitialise
                                                   write FOnInitialise;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  cxLabel, cxMemo, Unit_FVBFFCDevExpress, 
  TypInfo, cxDBEdit, cxDropDownEdit, Unit_PPWFrameWorkController;

{$R *.dfm}

{ TFVBFFCRecordView }

procedure TFVBFFCRecordView.ActivateControl(aControl: TWinControl);
begin
  if ( Assigned( aControl ) ) and
     ( Mode in [ rvmAdd, rvmEdit ] ) then
  begin
    ActiveControl := aControl;
  end
  else
  begin
    ActiveControl := Nil;
  end;
end;

procedure TFVBFFCRecordView.AddOrUpdateWindowMenuItem;
begin
end;

procedure TFVBFFCRecordView.AddReadOnlyComponent(aComponent: TComponent;
  aHandled: Boolean);
var
  acxCustomEditProperties : TcxCustomEditProperties;
begin
  {$IFDEF CODESITE}
  csFVBFFCControlLayout.EnterMethod( Self, 'AddReadOnlyComponent' );
  csFVBFFCControlLayout.SendObject( 'aComponent', aComponent );
  {$ENDIF}

  Inherited AddReadOnlyComponent( aComponent, aHandled );

  if ( Assigned( aComponent ) ) and
     ( aComponent is TcxCustomEdit ) then
  begin
    if ( IsPublishedProp( aComponent, 'Properties' ) ) then
    begin
      acxCustomEditProperties := TcxCustomEditProperties( GetObjectProp( aComponent, 'Properties' ) );
      if ( Assigned( acxCustomEditProperties ) ) then
      begin
        acxCustomEditProperties.ReadOnly := True;
      end;
    end;

    if not ( aComponent is TcxCustomMemo ) then
    begin
      TcxCustomEdit( aComponent ).Enabled := False;
    end;
  end;

  {$IFDEF CODESITE}
  csFVBFFCControlLayout.SendObject( 'aComponent', aComponent );
  csFVBFFCControlLayout.ExitMethod( Self, 'AddReadOnlyComponent' );
  {$ENDIF}
end;

procedure TFVBFFCRecordView.CreateFormButtonOnToolbar;
begin

end;

procedure TFVBFFCRecordView.DestroyFormButtonOnToolbar;
begin

end;

procedure TFVBFFCRecordView.DoInitialiseForm(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  if ( Assigned( FOnInitialise ) ) then
  begin
    FOnInitialise( aFrameWorkDataModule );
  end;
end;

{*****************************************************************************
  This method will be used to Focus the ActiveDetailForm if one was Available.

  @Name       TFVBFFCRecordView.FocusActiveDetailForm
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCRecordView.FocusActiveDetailForm;
begin
  if ( Assigned( ActiveDetailForm ) ) then
  begin
    ActiveDetailForm.SetFocus;
  end;
end;

{*****************************************************************************
  Property Getter for the ActiveDetailForm property.

  @Name       TFVBFFCRecordView.GetActiveDetailForm
  @author     slesage
  @param      None
  @return     Returns a reference to the currently active detail form.  This
              could be a ListView or a RecordView, so the returned reference
              will be a IPPWFrameWorkForm.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCRecordView.GetActiveDetailForm: IPPWFrameWorkForm;
var
  aFormIndex  : Integer;
  aActiveForm : IPPWFrameWorkForm;
  aForm       : IPPWFrameWorkForm;
begin
  aActiveForm := Nil;

  { Loop over all Detail ListViews to determine if one of them is currently
    visible }
  if ( Not( Assigned( aActiveForm ) ) ) then
  begin
    for aFormIndex := 0 to Pred( DetailListViews.Count ) do
    begin
      if ( Supports( DetailListViews.Items[ aFormIndex ], IPPWFrameWorkForm, aForm ) ) and
         ( Assigned( aForm ) ) and
         ( aForm.Visible ) then
      begin
        aActiveForm := aForm;
        Break;
      end;
    end;
  end;

  { Loop over all Detail ListViews to determine if one of them is currently
    visible }
  if ( Not( Assigned( aActiveForm ) ) ) then
  begin
    for aFormIndex := 0 to Pred( DockedRecordViews.Count ) do
    begin
      if ( Supports( DockedRecordViews.Items[ aFormIndex ], IPPWFrameWorkForm, aForm ) ) and
         ( Assigned( aForm ) ) and
         ( aForm.Visible ) then
      begin
        aActiveForm := aForm;
        Break;
      end;
    end;
  end;

  Result := aActiveForm;
end;

{*****************************************************************************
  This funciton will be used to return the EditStyleController for ReadOnly
  controls.

  @Name       TFVBFFCRecordView.GetReadOnlyEditStyleController
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCRecordView.GetLabelStyleController: TcxEditStyleController;
begin
  Result := DefaultEditStyleController;
end;

function TFVBFFCRecordView.GetReadOnlyEditStyleController: TcxEditStyleController;
begin
  Result := DefaultEditStyleController;
end;

{*****************************************************************************
  This funciton will be used to return the EditStyleController for Required
  controls.

  @Name       TFVBFFCRecordView.GetRequiredEditStyleController
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCRecordView.GetRequiredEditStyleController: TcxEditStyleController;
begin
  Result := DefaultEditStyleController;
end;

procedure TFVBFFCRecordView.HideHeaderAndButtonPanel;
begin

end;

procedure TFVBFFCRecordView.RemoveReadOnlyComponent(aComponent: TComponent;
  aHandled: Boolean);
var
  acxCustomEditProperties : TcxCustomEditProperties;
begin
  {$IFDEF CODESITE}
  csFVBFFCControlLayout.EnterMethod( Self, 'RemoveReadOnlyComponent' );
  csFVBFFCControlLayout.SendObject( 'aComponent', aComponent );
  {$ENDIF}

  Inherited RemoveReadOnlyComponent( aComponent );

  if ( Assigned( aComponent ) ) and
     ( aComponent is TcxCustomEdit ) then
  begin
    if ( IsPublishedProp( aComponent, 'Properties' ) ) then
    begin
      acxCustomEditProperties := TcxCustomEditProperties( GetObjectProp( aComponent, 'Properties' ) );
      if ( Assigned( acxCustomEditProperties ) ) then
      begin
        acxCustomEditProperties.ReadOnly := False;
      end;
    end;

    if not ( aComponent is TcxCustomMemo ) then
    begin
      TcxCustomEdit( aComponent ).Enabled := True;
    end;
  end;

  {$IFDEF CODESITE}
  csFVBFFCControlLayout.ExitMethod( Self, 'RemoveReadOnlyComponent' );
  {$ENDIF}
end;

procedure TFVBFFCRecordView.RemoveWindowMenuItem;
begin

end;

procedure TFVBFFCRecordView.ShowDetailListView( Sender : TObject;
  aDataModuleClassName: String; aDockSite: TWinControl);
var
  aListView      : IPPWFrameWorkListView;
  aListViewIndex : integer;
begin
  { First check if the ListView is already docked in the form, if that is
    the case, simply show it }
  for aListViewIndex := 0 to Pred( Self.DetailListViews.Count ) do
  begin
    if ( Supports( DetailListViews.Items[ aListViewIndex ], IPPWFrameWorkListView, aListView ) ) then
    begin
      if aListView.FrameWorkDataModule.FrameWorkDatamoduleClassName = aDataModuleClassName then
      begin
        aListView.Visible := True;
        aListView.BringToFront;
        aListView.SetFocus;
        Break;
      end
      else
      begin
        aListView := Nil;
      end;
    end;
  end;

  { If we didn't find the ListView for the given Detail DataModule, then
    we should create it, and dock it into the given DockSite }
  if not ( Assigned( aListView ) ) then
  begin
    aListView := PPWController.CreateDetailListView( RecordViewDatamodule, aDataModuleClassName, Self );
    aListView.MasterRecordView := Self;
    aListView.Align := alClient;
    aListView.BorderStyle := bsNone;
    aListView.ManualDock( aDockSite );
    DetailListViews.Add( aListView );
    aListView.Show;
  end;
end;

procedure TFVBFFCRecordView.ShowDetailRecordView(
  aDataModuleClassName: String; aDockSite: TWinControl);
begin
//
end;

procedure TFVBFFCRecordView.ShowRecordDetail( Sender : TObject );
var
  aListViewIndex   : Integer;
  aListView        : IPPWFrameWorkListView;
  aRecordViewIndex : Integer;
  aRecordView      : IPPWFrameWorkRecordView;
begin
  for aListViewIndex := 0 to Pred( Self.DetailListViews.Count ) do
  begin
    if ( Supports( DetailListViews.Items[ aListViewIndex ], IPPWFrameWorkListView, aListView ) ) then
    begin
      aListView.Visible := False;
    end;
  end;

  for aRecordViewIndex := 0 to Pred( Self.DockedRecordViews.Count ) do
  begin
    if ( Supports( DockedRecordViews.Items[ aRecordViewIndex ], IPPWFrameWorkRecordView, aRecordView ) ) then
    begin
      aRecordView.Visible := False;
    end;
  end;
end;

procedure TFVBFFCRecordView.UpdateControlColor(Control: TControl);
var
  aDataBinding : TcxDBEditDataBinding;
  aOptions     : TPPWFrameWorkControlOptions;
//  aParentColorAccess : ICMBParentColorAccess;
//  acxCustomEditProperties : TcxCustomEditProperties;
begin
  aOptions := [];

  if ( ( Control is TcxCustomEdit ) and not
       ( Control is TcxCustomLabel ) )  then
  begin
    if IsPublishedProp( Control, 'DataBinding' ) then
    begin
      aDataBinding := TcxDBEditDataBinding( GetObjectProp( Control, 'DataBinding' ) );
    end
    else
    begin
      aDataBinding := Nil;
    end;

    { First check if the Control was set to ReadOnly at Design Time, if
      that is the case, we kan skip all other checks }
    if ( IsPublishedProp( Control, 'Properties' ) ) then
    begin
      if ( TcxCustomEdit( Control ).ActiveProperties.ReadOnly ) then
      begin
        aOptions := aOptions + [ coControlReadOnly ];
      end;
    end;

    if not ( coControlReadOnly in aOptions ) then
    begin
    { If the Form is in Delete or Edit Mode add the coDataSetReadOnly to the
      Options }
    if ( Mode = rvmDelete ) or ( Mode = rvmView ) then
    begin
      aOptions := aOptions + [ coDataSetReadOnly ];
      AddReadOnlyComponent( Control );
    end
    { If the Field is ReadOnly add the coFieldReadOnly to the Options }
    else if ( Assigned( aDataBinding ) and
              Assigned( aDataBinding.DataLink ) and
              Assigned( aDataBinding.DataLink.Field ) and
              aDataBinding.DataLink.Field.ReadOnly ) then
    begin
      aOptions := aOptions + [ coFieldReadOnly ];
      AddReadOnlyComponent( Control );
    end
    else if ( Assigned( aDataBinding ) and
              Assigned( aDataBinding.DataLink ) and
              Assigned( aDataBinding.DataLink.Field ) and
              aDataBinding.DataLink.Field.Required ) then
    begin
      aOptions := aOptions + [ coFieldRequired ];
    end;
    end;

    UpdateControlLayout( Control, aOptions );
  end;

//  if ( Supports( Control, ICMBParentColorAccess, aParentColorAccess ) ) then
//  begin
//    aParentColorAccess.ParentColor := True;
//  end;
end;

procedure TFVBFFCRecordView.UpdateControlLayout(Control: TControl;
  ControlOptions: TPPWFrameWorkControlOptions);
const
  cMySet = [coDataSetReadOnly, coFieldReadOnly, coControlDisabled];
begin
  { Only update the Layout for cxEdit Controls, excluding cxLabel Controls }
  if     ( Control is TcxCustomEdit  ) and
     not ( Control is TcxCustomLabel ) then
  begin
    if ( coControlReadOnly in ControlOptions ) or
       ( coFieldReadOnly   in ControlOptions ) or
       ( coDataSetReadOnly in ControlOptions ) then
    begin
      TcxCustomEdit( Control ).Style.StyleController := ReadOnlyStyleController;

      if ( Control is TcxCustomMemo ) then
      begin
        TcxCustomMemo( Control ).Properties.ReadOnly := True;
        TcxCustomMemo( Control ).Enabled  := PPWController.EnableWhenReadOnly;
      end
      else
      begin
        TcxCustomEdit( Control ).Enabled  := PPWController.EnableWhenReadOnly;
      end;
    end
    else if ( coFieldRequired in ControlOptions ) then
    begin
      TcxCustomEdit( Control ).Style.StyleController := RequiredStyleController;
    end
    else
    begin
      TcxCustomEdit( Control ).Style.StyleController := DefaultEditStyleController;
    end;
  end;
end;

procedure TFVBFFCRecordView.UpdateControls(Container: TWinControl);
var
  lcv         : Integer;
begin
  {$IFDEF CODESITE}
  csFVBFFCControlLayout.EnterMethod( Self, 'UpdateControls' );
  csFVBFFCControlLayout.SendObject( 'Container', Container );
  {$ENDIF}

  for lcv := 0 to Pred( Container.ControlCount ) do
  begin
    UpdateControlColor( Container.Controls[ lcv ] );

    if ( Container.Controls[ lcv ] is TWinControl ) and
       ( TWinControl( Container.Controls[ lcv ] ).ControlCount <> 0 ) then
    begin
      UpdateControls( TWinControl( Container.Controls[ lcv ] ) );
    end;
  end;

  {$IFDEF CODESITE}
  csFVBFFCControlLayout.ExitMethod( Self, 'UpdateControls' );
  {$ENDIF}
end;

function TFVBFFCRecordView.VisibleRecordView: IFVBFFCRecordView;
var
  aRecordView    : IFVBFFCRecordView;
  aRecordViewIndex : Integer;
begin
  { First we need to find the ListView / DataModule }
  for aRecordViewIndex := 0 to Pred( DockedRecordViews.Count ) do
  begin
    if ( Supports( DockedRecordViews.Items[ aRecordViewIndex ], IFVBFFCRecordView, aRecordView ) ) and
       ( aRecordView.Visible ) then
    begin
      Result := aRecordView;
      Break;
    end;
  end;

  if ( not ( Assigned( Result ) ) ) then
  begin
    Result := Self;
  end;
end;

end.
