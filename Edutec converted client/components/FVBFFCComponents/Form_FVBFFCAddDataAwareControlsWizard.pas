{*****************************************************************************
  This unit contains a descentant of the AddDataAwareControls Wizard.
  
  @Name       Form_FVBFCCAddDataAwareControlsWizard
  @Author     slesage
  @Copyright  (c) 2004 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  23/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_FVBFFCAddDataAwareControlsWizard;

interface

uses
  Form_PPWFrameWorkAddDataAwareControlsWizard, DB, Classes;

type
  TFVBFFCAddDataAwareControlsWizard = class( TfrmPPWFrameWorkAddDataAwareControlsWizard )
  protected
    function GetDefaultDBAwareComponentClassNames : String; override;
    function GetDefaultDBAwareComponentClassName( aField : TField ) : String; override;
  public
    function GetDBAwareComponentClass( aField : TField ) : TComponentClass; override;
  end;

implementation

uses
  Unit_FVBFFCDevExpress;

{ TFVBFFCAddDataAwareControlsWizard }

{*****************************************************************************
  This function will be used to get the ComponentClass that will represent
  the given aField.

  @Name       TFVBFFCAddDataAwareControlsWizard.GetDBAwareComponentClass
  @author     slesage
  @param      None
  @return     Returns the DBAware Component Class that should be used to
              represent the given aField.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCAddDataAwareControlsWizard.GetDBAwareComponentClass(
  aField: TField): TComponentClass;
begin
  if ( cdsRecordFields.Locate( 'F_FIELD_NAME', aField.FieldName, [] ) ) then
  begin
    Result := TComponentClass( FindClass( cdsRecordFieldsF_FIELD_COMPONENT.AsString ) );
  end
  else
  begin
    Result := TFVBFFCDBTextEdit;
  end;
end;

{*****************************************************************************
  This function will be used to get the ClassName of the Default DBAware
  Component that should be used ( depending in the aField ).

  @Name       TFVBFFCAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassName
  @author     slesage
  @param      None
  @return     Returns the Default DBAwareComponent ClassName that should be
              used to represent the given aField.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassName(
  aField: TField): String;
begin
  if ( Assigned( aField ) ) then
  begin
    case aField.DataType of
      ftString, ftAutoInc, ftFixedChar, ftWideString : Result := 'TFVBFFCDBTextEdit';
      ftSmallint, ftInteger, ftWord, ftLargeint      : Result := 'TFVBFFCDBSpinEdit';
      ftBoolean                                      : Result := 'TFVBFFCDBCheckBox';
      ftFloat, ftBCD                                 : Result := 'TFVBFFCDBCalcEdit';
      ftCurrency                                     : Result := 'TFVBFFCDBCurrencyEdit';
      ftDate, ftDateTime                             : Result := 'TFVBFFCDBDateEdit';
      ftTime                                         : Result := 'TFVBFFCDBTimeEdit';
      ftBlob                                         : Result := 'TFVBFFCDBBlobEdit';
      ftMemo                                         : Result := 'TFVBFFCDBMemo';
      ftGraphic                                      : Result := 'TFVBFFCDBImage';
      ftFmtMemo                                      : Result := 'TFVBFFCDBRichEdit';
      ftGuid                                         : Result := 'TFVBFFCDBButtonEdit';
      else Result := 'TFVBFFCtecDBTextEdit';
    end;
  end;
end;

{*****************************************************************************
  This function will be used to return a list of DBAwareComponent Class Names
  that can be used by the Wizard.

  @Name       TFVBFFCAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassNames
  @author     slesage
  @param      None
  @return     Returns a string containing all the ClassNames of the DBAware
              Components that can be used by the Wizard.  Each ClassName will
              be seperated from the previous une using a
              CariageReturn and LineFeed.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassNames: String;
begin
  Result := 'TFVBFFCDBBlobEdit' + #13#10 +
            'TFVBFFCDBButtonEdit' + #13#10 +
            'TFVBFFCDBCalcEdit' + #13#10 +
            'TFVBFFCDBCheckBox' + #13#10 +
            'TFVBFFCDBCheckGroup' + #13#10 +
            'TFVBFFCDBComboBox' + #13#10 +
            'TFVBFFCDBCurrencyEdit' + #13#10 +
            'TFVBFFCDBDateEdit' + #13#10 +
            'TFVBFFCDBHyperLinkEdit' + #13#10 +
            'TFVBFFCDBImage' + #13#10 +
            'TFVBFFCDBImageComboBox' + #13#10 +
            'TFVBFFCDBLabel' + #13#10 +
            'TFVBFFCDBLookupComboBox' + #13#10 +
            'TFVBFFCDBMaskEdit' + #13#10 +
            'TFVBFFCDBMemo' + #13#10 +
            'TFVBFFCDBProgressBar' + #13#10 +
            'TFVBFFCDBRadioGroup' + #13#10 +
            'TFVBFFCDBRichEdit' + #13#10 +
            'TFVBFFCDBSpinEdit' + #13#10 +
            'TFVBFFCDBTextEdit' + #13#10 +
            'TFVBFFCDBTimeEdit';
end;

end.
