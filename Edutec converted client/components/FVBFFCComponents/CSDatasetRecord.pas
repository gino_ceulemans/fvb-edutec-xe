{===============================================================================
  CSDatasetRecord - CodeSite Dataset Record Unit

  Copyright � 2001 by Raize Software, Inc.
===============================================================================}

unit CSDatasetRecord;

interface

uses
  DB, CSIntf;

const
  csmDatasetRecord = csmReserved + 4;

type
  TCSDatasetRecordFormatter = class( TCSFormatter )
  public
    function InspectorType: TCSInspectorType; override;
    procedure FormatData( var Data ); override;
  end;

procedure CSSendDatasetRecord( const Msg: string; Dataset: TDataset );

implementation

uses
  SysUtils;

{=======================================}
{== TCSDatasetRecordFormatter Methods ==}
{=======================================}

function TCSDatasetRecordFormatter.InspectorType: TCSInspectorType;
begin
  Result := itStockGrid;
end;


procedure TCSDatasetRecordFormatter.FormatData( var Data );
var
  Dataset: TDataset;
  I: Integer;
begin
  Dataset := TDataset( Data );

  for I := 0 to Dataset.FieldCount - 1 do
  begin
    AddNameValuePair( Dataset.FieldList[ I ].DisplayName, Dataset.Fields[ I ].AsString );
  end;
end;


{=====================}
{== Support Methods ==}
{=====================}

procedure CSSendDatasetRecord( const Msg: string; Dataset: TDataset );
begin
  CodeSite.SendCustomData( csmDatasetRecord, Msg, Dataset );
end;


initialization
  // Custom formatter classes are registered with the CodeSite Object Manager,
  // CSObjectManager, so that all TCodeSite objects will have access to the
  // custom formatter classes.

  CSObjectManager.RegisterCustomFormatter( csmDatasetRecord, TCSDatasetRecordFormatter );

end.


