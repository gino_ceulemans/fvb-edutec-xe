{*****************************************************************************
  This unit will contain various Utility Routines and Functions.

  @Name       Unit_FVBFFCUtils
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  16/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCUtils;

interface

uses
  Controls;

  function ApplicationRegistryKey   : String;
  function ApplicationIniFileName   : String;
  function ApplicationPath          : String;
  function ApplicationVersionString : String;
  function LocalSettingsIniFileName : String;

  function ControlIsChildOf( aControl, aParent : TWinControl ) : Boolean;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  SysUtils, Forms, Types, Windows, shlobj, Data_About;


{*****************************************************************************
  Function copied from http://www.cryer.co.uk/brian/delphi/howto_get_temp.htm
  @Name       GetTempDirectory
  @author     wlambrec
  @param      None
  @return

  @Exception  None
  @See        None
******************************************************************************}
function GetTempDirectory: String;
var
  tempFolder: array[0..MAX_PATH] of Char;
begin
  GetTempPath(MAX_PATH, @tempFolder);
  result := StrPas(tempFolder);
end;

function GetAppDataDirectory: string;
var
   buffer: array[0..MAX_PATH] of Char;
begin
   if (ShGetSpecialFolderPath(0, @buffer, CSIDL_APPDATA, False)) then
     Result := buffer
   else
     Result := GetTempDirectory;
end;

{*****************************************************************************
  @Name       ApplicationIniFileName
  @author     slesage
  @param      None
  @return     Returns the FileName of the Application's INI File.
              The ini-file was now made version dependant: WL
  @Exception  None
  @See        None
******************************************************************************}
function ApplicationIniFileName : String;
begin
  result := ChangeFileExt( Application.ExeName, '.INI' );
end;

function LocalSettingsIniFileName: String;
var
  dir: String;
  aAbout: TclsAbout;
  filename: String;
begin
  aAbout := TClsAbout.Create(application);
  try
    filename := ExtractFileName(Application.EXeName);
    filename := Copy (filename, 1, length (filename) - length(ExtractFileExt(filename)));
    filename := filename + aAbout.FileVersion + '.INI';
  finally
    FreeAndNil (aAbout);
  end;
  dir := IncludeTrailingBackslash(GetAppDataDirectory) + 'PeopleWare\';
  dir := dir + ChangeFileExt (ExtractFileName(Application.EXEName), '') + '\';
  ForceDirectories(dir);
  result :=  dir + filename;
end;

{*****************************************************************************
  This function will be used to return the Path where the EXE resides.

  @Name       ApplicationPath
  @author     slesage
  @param      None
  @return     Returns the Path where the Executable Resides.
  @Exception  None
  @See        None
******************************************************************************}

function ApplicationPath : String;
begin
  Result := ExtractFilePath( Application.ExeName );
end;

function ApplicationVersionString : String;
var
  aFileName : String;
  VersionInfoSize,
  VersionInfoValueSize,
  Zero             : DWord;
  VersionInfo,
  VersionInfoValue : Pointer;
begin
  aFileName := Application.ExeName;
  Result := '';
  { Obtain size of version info structure }
  VersionInfoSize := GetFileVersionInfoSize(PChar(aFileName), Zero);
  if VersionInfoSize = 0 then exit;
  { Allocate memory for the version info structure }
  { This could raise an EOutOfMemory exception }
  GetMem(VersionInfo, VersionInfoSize);
  try
    if GetFileVersionInfo(PChar(aFileName), 0, VersionInfoSize, VersionInfo) and
       VerQueryValue(VersionInfo, '\' { root block }, VersionInfoValue, VersionInfoValueSize) and
       (0 <> LongInt(VersionInfoValueSize)) then begin
       with TVSFixedFileInfo(VersionInfoValue^) do begin
          Result :=                IntToStr(HiWord(dwFileVersionMS));
          Result := Result + '.' + IntToStr(LoWord(dwFileVersionMS));
          Result := Result + '.' + IntToStr(HiWord(dwFileVersionLS));
          Result := Result + '.' + IntToStr(LoWord(dwFileVersionLS));
       end; { with }
    end; { then }
  finally
    FreeMem(VersionInfo);
  end; { try }
end;

function ApplicationRegistryKey : String;
begin
  Result := 'Software\PeopleWare\FVBFFC\';
end;

{*****************************************************************************
  This function can be used to check if a Given control is a direct child of
  another control.

  @Name       ControlIsChildOf
  @author     slesage
  @param      aControl
  @param      aParent
  @return     Returns a boolean indicating if aControl is a direct child of
              the aParent control.
  @Exception  None
  @See        None
******************************************************************************}

function ControlIsChildOf(aControl, aParent: TWinControl): Boolean;
var
  idxControl : Integer;
begin
  Result := False;
  if ( Assigned( aParent ) ) and
     ( Assigned( aControl  ) ) then
  begin
    for idxControl := 0 to Pred( aParent.ControlCount ) do
    begin
      if ( aParent.Controls[ idxControl ] = aControl ) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;
end;

end.
