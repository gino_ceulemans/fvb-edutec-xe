unit Unit_FVBFFCListView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkListView, Unit_FVBFFCInterfaces, Unit_PPWFrameWorkInterfaces,
  cxGrid, cxGridCustomView, dxPSCore, DBClient, Unit_PPWFrameWorkClasses;

type
  TFVBFFCListView = class( TPPWFrameWorkListView, IFVBFFCListView, IFVBFFCForm, IPIFDevExpressListView )
  private
    { Private declarations }
    FAllowMultiSelect : Boolean;
    FOnInitialise: TInitialiseFormEvent;
  protected
    { IPIFDevExpressListView Property Getters }
    function GetAllowMultiSelect    : Boolean; virtual;
    function GetRegistryKey         : String; override;

    { IEDUListView Property Getters }
    function GetActiveGridView   : TcxCustomGridView; virtual;
    function GetFilterString     : String; virtual; abstract;
    function GetOrderByString    : String; virtual; abstract;
    function GetGrid             : TcxGrid; virtual; abstract;
    function GetComponentPrinter : TdxComponentPrinter; virtual; abstract;

    { IPIFDevExpressListView Property Setters }
    procedure SetAllowMultiSelect ( const Value : Boolean ); virtual;

    { ICMBForm Mehtods }
    procedure CreateFormButtonOnToolbar; virtual;
    procedure DestroyFormButtonOnToolbar; virtual;
    procedure AddOrUpdateWindowMenuItem; virtual;
    procedure RemoveWindowMenuItem; virtual;

    { IPIFDevExpressListView Methods }
    procedure CloneSelectedRecords( aSource, aDestination : TClientDataSet ); virtual;
    procedure CustomiseColumns; virtual;
    procedure ExportGridContentsToHTML( Sender : TObject ); virtual;
    procedure ExportGridContentsToXML( Sender : TObject ); virtual;
    procedure ExportGridContentsToXLS( Sender : TObject ); virtual;
    procedure PrintGrid( aPrintPreview : Boolean = True ); virtual;
    procedure SaveGridContents( aFileExt, aFilter, aFileName : String;
      aMethod : TPPWFrameWorkSaveGridMethod ); virtual;
    function ExportGridContents(aFileExt, aFilter, aFileName: String;
      aMethod: TPPWFrameWorkSaveGridMethod): boolean; virtual;


    { IEDUListView }
    procedure ClearFilterCriteria; virtual;
    procedure StoreActiveViewToIni; virtual;
    procedure RestoreActiveViewFromIni; virtual;

    { IEDUForm }
    procedure DoInitialiseForm( aFrameWorkDataModule : IPPWFrameWorkDataModule ); virtual;

    property ActiveGridView  : TcxCustomGridView   read GetActiveGridView;
    property Grid            : TcxGrid             read GetGrid;
    property ComponentPrinter: TdxComponentPrinter read GetComponentPrinter;
  public
    { Public declarations }
  published
    property OnInitialise : TInitialiseFormEvent   read FOnInitialise
                                                   write FOnInitialise;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Unit_PPWFrameWorkUtils,
  cxGridDBDataDefinitions, cxGridDBTableView, cxGridExportLink,
  cxGridCustomTableView;

{$R *.dfm}

{ TFVBFFCListView }

{*****************************************************************************
  This method will be used to clear the Filter Criteria in our ListView.

  @Name       TFVBFFCListView.ClearFilterCriteria
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.AddOrUpdateWindowMenuItem;
begin

end;

procedure TFVBFFCListView.ClearFilterCriteria;
begin
  //
end;

{*****************************************************************************
  This method will be used to Copy all the selected records in the Grid from
  the Source DataSet to the Destination DataSet.

  @Name       TFVBFFCListView.CloneSelectedRecords
  @author     slesage
  @param      aSource       The Source DataSet.
  @param      aDestination  The Destination DataSet.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.CloneSelectedRecords(aSource,
  aDestination: TClientDataSet);
var
  lcv : Integer;
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
begin
  if ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
    aDataController := TcxGridDBDataController( ActiveGridView.DataController );

    { Check if there are selected records }
    if ( aViewController. SelectedRecordCount <> 0 ) then
    begin
      { Loop over each selected record in the grid }
      for lcv := 0 to Pred( aViewController.SelectedRecordCount ) do
      begin
        aSource.Bookmark := aDataController.GetSelectedBookmark( lcv );
        CloneRecord( aSource, aDestination );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to show the Column Customizing Form.

  @Name       TFVBFFCListView.CustomiseColumns
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.CreateFormButtonOnToolbar;
begin

end;

procedure TFVBFFCListView.CustomiseColumns;
begin
  if ( ActiveGridView.Controller is TcxCustomGridTableController ) then
  begin
    TcxCustomGridTableController( ActiveGridView.Controller ).Customization := True;
  end;
end;

{*****************************************************************************
  This method will be used to save the current contents of a grid to a HTML file.
  Internally it will call the SaveGridContents method.

  @Name       TFVBFFCListView.ExportGridContentsToHTML
  @author     slesage
  @param      Sender   The Object from which the method is invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.DestroyFormButtonOnToolbar;
begin

end;

procedure TFVBFFCListView.DoInitialiseForm(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  if ( Assigned( FOnInitialise ) ) then
  begin
    FOnInitialise( aFrameWorkDataModule );
  end;
end;

procedure TFVBFFCListView.ExportGridContentsToHTML(Sender: TObject);
begin
  SaveGridContents( 'htm', 'HTML File (*.htm; *.html)|*.htm',
                    Caption + '.htm', Nil );
end;

{*****************************************************************************
  This method will be used to save the current contents of a grid to a XLS file.
  Internally it will call the SaveGridContents method.

  @Name       TFVBFFCListView.ExportGridContentsToXLS
  @author     slesage
  @param      Sender   The Object from which the method is invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.ExportGridContentsToXLS(Sender: TObject);
begin
  SaveGridContents( 'xls', 'Microsoft Excel Worksheet (*.xls)|*.xls',
                    Caption + '.xls', Nil );
end;

{*****************************************************************************
  This method will be used to save the current contents of a grid to a XML file.
  Internally it will call the SaveGridContents method.

  @Name       TFVBFFCListView.ExportGridContentsToXML
  @author     slesage
  @param      Sender   The Object from which the method is invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.ExportGridContentsToXML(Sender: TObject);
begin
  SaveGridContents( 'xml', 'XML File (*.xml)|*.xml',
                    Self.Caption + '.xml', Nil );
end;

{*****************************************************************************
  Property Getter for the ActiveGridView propety.

  @Name       TFVBFFCListView.GetActiveGridView
  @author     slesage
  @param      None
  @return     Returns the Active View in the cxGrid.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCListView.GetActiveGridView: TcxCustomGridView;
begin
  Result := Grid.ActiveView;
end;

{*****************************************************************************
  Property Getter for the AllowMultiSelect Property.

  @Name       TFVBFFCListView.GetAllowMultiSelect
  @author     slesage
  @param      None
  @return     Returns the current value of the AllowMultiSelect Property.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCListView.GetAllowMultiSelect: Boolean;
begin
  Result := FAllowMultiSelect;
end;

{*****************************************************************************
  Overridden GetRegistryKey method.
  If the form is used as a detail of another RecordView, then the RegistryKey
  should refer to the MasterRecordView, and a SubKey for the ListView should
  be made.

  @Name       TFVBFFCListView.GetRegistryKey
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCListView.GetRegistryKey: String;
var
  aPIFListView : IPPWFrameWorkListView;
begin
  if ( ( AsDetail ) and
       ( Supports( Self , IPPWFrameWorkListView, aPIFListView ) ) ) then
  begin
    Result := aPIFListView.MasterRecordView.RegistryKey + '\' + Self.ClassName;
  end
  else
  begin
    Result := Inherited GetRegistryKey;
  end;
end;

{*****************************************************************************
  This method will be used to Print the the Grid.

  @Name       TFVBFFCListView.PrintGrid
  @author     slesage
  @param      aPrintPreview   Booleaen flag indicating if the PrintPreview
                              should be shown.
  @return     None
  @Exception  None
  @See        None
  @History

  Date         By                   Description
  ----         --                   -----------
  25/05/2005   Tony Claesen         Set the CurrentLink to the link of the grid
                                    because the Preview and Print command will
                                    be executed with the link in the CurrentLink
                                    property. So the CurrentLink can have an
                                    other link to a grid than you expect.
******************************************************************************}

procedure TFVBFFCListView.PrintGrid(aPrintPreview: Boolean);
begin
  if ( Assigned( ComponentPrinter ) ) then
  begin
    if Assigned( MasterRecordView ) then
    begin
      ComponentPrinter.PrintTitle := Self.Caption + ' for ' + MasterRecordView.GetDetailName;
    end
    else
    begin
      ComponentPrinter.PrintTitle := Self.Caption;
    end;

    if Assigned( ComponentPrinter.FindLinkByComponent( Grid ) ) then
      ComponentPrinter.CurrentLink := ComponentPrinter.FindLinkByComponent( Grid );

//    ComponentPrinter.FindLinkByComponent( Grid ).ReportTitle.Text := ComponentPrinter.PrintTitle;
    ComponentPrinter.CurrentLink.ReportTitle.Text := ComponentPrinter.PrintTitle;

    if ( aPrintPreview ) then
    begin
      ComponentPrinter.Preview;
    end
    else
    begin
      ComponentPrinter.Print( True, Nil )
    end;
  end;
end;

{*****************************************************************************
  This method will be used to Remove the form's Item from the Window Menu.

  @Name       TFVBFFCListView.RemoveWindowMenuItem
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.RemoveWindowMenuItem;
begin
  //
end;

{*****************************************************************************
  This method will be used to restore the layout of the List from an Ini File.

  @Name       TFVBFFCListView.RestoreActiveViewFromIni
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.RestoreActiveViewFromIni;
begin
  //
end;

{*****************************************************************************
  This method will be used to actually save the contents of the Grid.

  @Name       TFVBFFCListView.SaveGridContents
  @author     slesage
  @param      aFileExt   The Default FileExtention that should be used in the
                         SaveDialog
              aFilter    The Default filter that should be used in the
                         Save Dialog.
              aFileName  The Default FileName that should be used in the
                         Save Dialog.
              aMethod    The Actual method that will be used for exporting the
                         Grid.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.SaveGridContents(aFileExt, aFilter,
  aFileName: String; aMethod: TPPWFrameWorkSaveGridMethod);
begin
  ExportGridContents(aFileExt, aFilter, aFileName, aMethod);
end;


{*****************************************************************************
  Function to make it possible to know if the export has been a success.

  @Name       TFVBFFCListView.ExportGridContents
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCListView.ExportGridContents(aFileExt, aFilter,
  aFileName: String; aMethod: TPPWFrameWorkSaveGridMethod) : boolean;
var
  lcv: integer;
begin

  Result := False;

  if ActiveGridView.InheritsFrom(TcxGridDBTableView) then
  begin
    for lcv := 0 to TcxGridDBTableView(ActiveGridView).ColumnCount - 1 do
    begin
      if TcxGridDBTableView(ActiveGridView).Columns[lcv].Hidden then
      begin
        TcxGridDBTableView(ActiveGridView).Columns[lcv].Visible := True;
        TcxGridDBTableView(ActiveGridView).Columns[lcv].Tag := 1;
      end;
    end;
  end;
  {
  30/01/2006 : cheuten
  Bugfix begin.
  Problem : Instead of aFileName use FileName because else userinput is ignored.
  }
  with TSaveDialog.Create( Self ) do
  begin
    DefaultExt := aFileExt;
    Filter     := aFilter;
    FileName   := aFileName;
    if ( Execute ) then
    begin
      if ( aFileExt = 'htm' ) then
      begin
        try
          ExportGridToHTML( FileName, Grid );
          Result := True;
        except
          Result := False;
        end;
      end
      else if ( aFileExt = 'xls' ) then
      begin
        try
          ExportGridToExcel( FileName, Grid );
          Result := True;
        except
          Result := False;
        end;
      end
      else if ( aFileExt = 'xml' ) then
      begin
        try
          ExportGridToXML( FileName, Grid );
          Result := True;
        except
          Result := False;
        end;
      end
      else
      begin
        try
          ExportGridToText( FileName, Grid );
          Result := True;
        except
          Result := False;
        end;
      end;
    end;
    Free;
  end;
  {
  30/01/2006 : cheuten
  Bugfix ended.
  }

  if ActiveGridView.InheritsFrom(TcxGridDBTableView) then
  begin
    for lcv := 0 to TcxGridDBTableView(ActiveGridView).ColumnCount - 1 do
    begin
      if (TcxGridDBTableView(ActiveGridView).Columns[lcv].Tag = 1) then
      begin
        TcxGridDBTableView(ActiveGridView).Columns[lcv].Visible := False;
        TcxGridDBTableView(ActiveGridView).Columns[lcv].Tag := 0;
      end;
    end;
  end;
end;

{*****************************************************************************
  Property Setter for the AllowMultiSelect property.

  @Name       TFVBFFCListView.SetAllowMultiSelect
  @author     slesage
  @param      Value   The new Value for the AllowMultiSelect property.
  @return     None
  @Exception  None
  @See        None

  Bug Fix: WL 11/09/2007
  - check on whether Value <> FAllowMultiSelect removed. Why ?
  when a listvies has Multiselect = TRUE (to be operated as a standalone screen)
  and this listview is also used in a SelectRecords (multiselect FALSE) the condition
  would always result in FALSE so that the multiselect property of the tableview remains
  unchanged and thus = TRUE (whether we would need a FALSE)
******************************************************************************}
procedure TFVBFFCListView.SetAllowMultiSelect(const Value: Boolean);
begin
//  if ( Value <> FAllowMultiSelect ) then
  begin
    FAllowMultiSelect := Value;

    if ( Assigned( ActiveGridView ) ) and
       ( ActiveGridView is TcxGridDBTableView ) then
    begin
      TcxGridDBTableView( ActiveGridView ).OptionsSelection.MultiSelect := Value;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to store the layout of the List to an Ini File.

  @Name       TFVBFFCListView.StoreActiveViewToIni
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCListView.StoreActiveViewToIni;
begin

end;

end.
