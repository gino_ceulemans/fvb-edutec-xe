unit PPW_Components_Common_CollapsablePanelIcons;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList;

type
  TCollapsebleIconKind = (icMin, icMax, icNormal, icClose);
  TButtonState = (bsNormal, bsOver, bsDown);

  TdtmCollapsableIcons = class(TDataModule)
    ImgCollapse: TImageList;
    imgHeader: TImageList;
  private
    class procedure LoadBitmaps;
  public
    class function ToBitmap(aState : Boolean) : TBitmap;
    class function ToBitmapForm(aKind : TCollapsebleIconKind;
                                aState : TButtonState) : TBitmap;
  end;

implementation

{$R *.dfm}

{ TdtmCollapsableIcons }
var
  TdtmCollapsableIconsFBitmaps : array[Boolean] of TBitmap;
  TdtmCollapsableIconsFButtons : array[0..8] of TBitmap;
  TdtmCollapsableIconsFLoaded : Boolean;

class procedure TdtmCollapsableIcons.LoadBitmaps;
var
  I : Boolean;
  X : Integer;
begin
  TdtmCollapsableIconsFLoaded := True;
  with TdtmCollapsableIcons.Create(nil) do try
    for I := low(TdtmCollapsableIconsFBitmaps) to High(TdtmCollapsableIconsFBitmaps) do begin
      TdtmCollapsableIconsFBitmaps[I] := TBitmap.Create;
      ImgCollapse.GetBitmap(ord(I), TdtmCollapsableIconsFBitmaps[I]);
      TdtmCollapsableIconsFBitmaps[I].PixelFormat := pf24Bit;
      TdtmCollapsableIconsFBitmaps[I].Transparent := True;
    end;
    for X := low(TdtmCollapsableIconsFButtons) to High(TdtmCollapsableIconsFButtons) do begin
      TdtmCollapsableIconsFButtons[X] := TBitmap.Create;
      ImgHeader.GetBitmap(X, TdtmCollapsableIconsFButtons[X]);
      TdtmCollapsableIconsFButtons[X].PixelFormat := pf24Bit;
      TdtmCollapsableIconsFButtons[X].Transparent := True;
    end;
  finally
    Free;
  end;
end;

class function TdtmCollapsableIcons.ToBitmap(aState: Boolean): TBitmap;
begin
  if not TdtmCollapsableIconsFloaded then LoadBitmaps;
  Result := TdtmCollapsableIconsFBitmaps[aState];
end;

class function TdtmCollapsableIcons.ToBitmapForm
  ( aKind : TCollapsebleIconKind; aState : TButtonState) : TBitmap;
begin
  if not TdtmCollapsableIconsFloaded then LoadBitmaps;
  Result := TdtmCollapsableIconsFButtons[ord(aKind) + (ord(aState) * 3)];
end;

procedure FreeBitmaps;
var
  I : Boolean;
  X : Integer;
begin
  if not TdtmCollapsableIconsFLoaded then Exit;
  for I := Low(TdtmCollapsableIconsFBitmaps) to High(TdtmCollapsableIconsFBitmaps) do begin
    TdtmCollapsableIconsFBitmaps[I].Free;
  end;
  for X := Low(TdtmCollapsableIconsFButtons) to High(TdtmCollapsableIconsFButtons) do begin
    TdtmCollapsableIconsFButtons[X].Free;
  end;
end;

initialization
  // nothing

finalization
  FreeBitmaps;

end.
