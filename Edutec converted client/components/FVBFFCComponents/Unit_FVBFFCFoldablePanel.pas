{*****************************************************************************
  This unit contains the Panel descendats used in the FVB / FFC project.

  @Name       Unit_FVBFFCFoldablePanel
  @Author     slesage
  @Copyright  (c) 2004 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/04/2005   Amuylaert            Added property visible  to TFVBFFCFoldablePanelStyle
  14/12/2004   sLesage              Modified the painting of the EDU panel so
                                    it draws a background ( size = borderwidth )
                                    and then an inner ClientArea with rounded
                                    corners before painting the Caption.
  10/12/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCFoldablePanel;

interface

uses
  ExtCtrls, Classes, Types, dxNavBar, Messages, Controls, Windows, Graphics,
  dxNavBarStyles, Math;

{$DEFINE CUSTOMBUFFERED}

type
  ARGB = DWORD;
  TFVBFFCFoldablePanelChangeType = ( doRecreate, doRecalc, doRedraw, doRedrawHeader, doRedrawClient, doRedrawHeaderAndClient );
  TFVBFFCFoldablePanelChangeEvent = procedure (Sender: TObject; AType: TFVBFFCFoldablePanelChangeType) of object;

  TFVBFFCFoldablePanelStyle = class( TPersistent )
  private
    FOwner : TPersistent;
    FAlphaBlending2: Byte;
    FAlphaBlending: Byte;
    FVisible : Boolean;
    FBackColor2: TColor;
    FBackColor: TColor;
    FGradientMode: TdxBarStyleGradientMode;
    FFont: TFont;
    FOnChange: TFVBFFCFoldablePanelChangeEvent;
    procedure SetAlphaBlending(const Value: Byte);
    procedure SetAlphaBlending2(const Value: Byte);
    procedure SetBackColor(const Value: TColor);
    procedure SetBackColor2(const Value: TColor);
    procedure SetFont(const Value: TFont);
    procedure SetGradientMode(const Value: TdxBarStyleGradientMode);
  protected
    function GetOwner: TPersistent; override;

    procedure DoFontChanged( Sender : TObject ); virtual;

    procedure Changed(AType: TFVBFFCFoldablePanelChangeType); virtual;
  public
    constructor Create( AOwner : TPersistent ); virtual;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property AlphaBlending  : Byte                     read FAlphaBlending
                                                       write SetAlphaBlending default 255;
    property AlphaBlending2 : Byte                     read FAlphaBlending2
                                                       write SetAlphaBlending2 default 255;
    property BackColor      : TColor                   read FBackColor
                                                       write SetBackColor;
    property BackColor2     : TColor                   read FBackColor2
                                                       write SetBackColor2;
    property GradientMode   : TdxBarStyleGradientMode  read FGradientMode
                                                       write SetGradientMode default gmHorizontal;
    property Font           : TFont                    read FFont
                                                       write SetFont;
    property OnChange       : TFVBFFCFoldablePanelChangeEvent read FOnChange
                                                       write FOnChange;
    property  Visible : Boolean                        read FVisible
                                                       write FVisible
                                                       default True;
  end;

  { TPanel descendant which implements Gradient Background Painting }
  TFVBFFCPanel         = class( TPanel )
  private
    FRedrawBackGround : Boolean;

    FStyleBackGround: TFVBFFCFoldablePanelStyle;
    FStyleClientArea: TFVBFFCFoldablePanelStyle;
    {$IFDEF CUSTOMBUFFERED}
    FCache : TBitmap;
    FCacheValid : Boolean;
    function GetBorderWidth: Integer;
    procedure SetBorderWidth(const Value: Integer);
    {$ENDIF}
  protected
    procedure SetStyleBackGround(const Value: TFVBFFCFoldablePanelStyle); virtual;
    procedure SetStyleClientArea(const Value: TFVBFFCFoldablePanelStyle); virtual;



    procedure DoStyleChanged(Sender: TObject; AType: TFVBFFCFoldablePanelChangeType); virtual;

    procedure InitialiseStyles; virtual;
    procedure InvalidateAll( aType : TFVBFFCFoldablePanelChangeType ); virtual;
    procedure InvalidateViewInfo(AType: TFVBFFCFoldablePanelChangeType); virtual;
    procedure Resize; override;
    {$IFDEF CUSTOMBUFFERED}
    procedure InvalidateCache;
    procedure WndProc(var Message: TMessage); override;
    {$ENDIF}
  private
    procedure PaintToCanvas(const aRect : TRect; aCanvas : TCanvas);
  public
    constructor Create( AOwner : TComponent ); override;
    destructor  Destroy; override;

    procedure Paint; override;

    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property Font;
    property Color;
  published
    property BorderWidth : Integer read GetBorderWidth write SetBorderWidth stored True;
    property StyleBackGround : TFVBFFCFoldablePanelStyle read  FStyleBackGround
                                                      write SetStyleBackGround;
    property StyleClientArea : TFVBFFCFoldablePanelStyle read  FStyleClientArea
                                                      write SetStyleClientArea;
  end;

  { TCustom descendant which implements folding, unfolding and style painting }
  TFVBFFCFoldablePanel = class( TCustomPanel )
  private
    FTabStops          : TStringList;
    FCaptionRect       : TRect;
    FCaptionTextRect   : TRect;
    FCaptionSignRect   : TRect;
    FCaptionImageRect  : TRect;
    FFoldableAreaRect  : TRect;
    FRect              : TRect;
    FReCalculateBounds : Boolean;
    FExpanded          : Boolean;
    FExpandedHeight    : Integer;
    FMouseInControl    : Boolean;

    FRedrawBackGround    : Boolean;
    FRedrawHeaderSign    : Boolean;
    FRedrawHeaderCaption : Boolean;
    FRedrawHeaderArea    : Boolean;
    FRedrawClientArea    : Boolean;

    FOnExpand: TNotifyEvent;
    FOnCollapse: TNotifyEvent;

    FStyleFoldArea: TFVBFFCFoldablePanelStyle;
    FStyleBackGround: TFVBFFCFoldablePanelStyle;
    FStyleHeader: TFVBFFCFoldablePanelStyle;

  protected
    function GetBorderWidth : Integer; virtual;
    function GetCaptionHeight: Integer; virtual;
    function GetCaptionSignSize: Integer; virtual;
    function GetCaptionImageIndent: Integer; virtual;
    function GetCaptionImageSize: Integer; virtual;
    function GetCaptionTextIndent: Integer; virtual;
    function GetHeight: Integer; virtual;
    function GetMinimumHeight: Integer; virtual;
    function GetMouseInHeader: Boolean; virtual;
    function GetState: TdxNavBarObjectStates; virtual;

    procedure SetBorderWidth( const Value : Integer ); virtual;
    procedure SetExpandedHeight(const Value: Integer); virtual;
    procedure SetHeight(const Value: Integer); virtual;
    procedure SetStyleBackGround(const Value: TFVBFFCFoldablePanelStyle); virtual;
    procedure SetStyleFoldArea(const Value: TFVBFFCFoldablePanelStyle); virtual;
    procedure SetStyleHeader(const Value: TFVBFFCFoldablePanelStyle); virtual;

    procedure CheckChildTabStops; virtual;
    procedure GetChildTabStops; virtual;
    procedure SetChildTabStops; virtual;
    procedure ClearChildTabStops; virtual;

    procedure DoOnCollapse; virtual;
    procedure DoOnExpand; virtual;

    procedure SetExpanded(const Value: Boolean); virtual;

    procedure AdjustClientRect(var Rect: TRect); override;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMSetFocus); message WM_KILLFOCUS;
    procedure InvalidateAll( aType : TFVBFFCFoldablePanelChangeType ); virtual;
    procedure InvalidateViewInfo(AType: TFVBFFCFoldablePanelChangeType); virtual;
  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;

    procedure DoStyleChanged(Sender: TObject; AType: TFVBFFCFoldablePanelChangeType); virtual;
    procedure DrawControlBackGround; virtual;
    procedure DrawHeaderArea; virtual;
    procedure DrawClientArea; virtual;
    procedure InitialiseStyles; virtual;

    procedure CalculateBounds; virtual;
    procedure ClearRects; virtual;
    procedure Paint; override;
    procedure Click; override;
    procedure Loaded; override;

    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

    property CaptionHeight      : Integer read GetCaptionHeight;
    property CaptionImageIndent : Integer read GetCaptionImageIndent;
    property CaptionImageSize   : Integer read GetCaptionImageSize;
    property CaptionSignSize    : Integer read GetCaptionSignSize;
    property CaptionTextIndent  : Integer read GetCaptionTextIndent;
    property MinimumHeight      : Integer read GetMinimumHeight;
    property State              : TdxNavBarObjectStates read GetState;
    property MouseInHeader      : Boolean read GetMouseInHeader;
    property Height             : Integer read GetHeight write SetHeight;
  published
    property Align;
    property BorderWidth    : Integer read GetBorderWidth write SetBorderWidth;
    property Caption;
    property Expanded       : Boolean read FExpanded       write SetExpanded;
    property ExpandedHeight : Integer read FExpandedHeight write SetExpandedHeight;
    property ParentFont;
    property ParentColor;
    property StyleBackGround : TFVBFFCFoldablePanelStyle read FStyleBackGround
                                                      write SetStyleBackGround;
    property StyleHeader     : TFVBFFCFoldablePanelStyle read FStyleHeader
                                                      write SetStyleHeader;
    property StyleFoldArea   : TFVBFFCFoldablePanelStyle read FStyleFoldArea
                                                      write SetStyleFoldArea;
    property Visible;

    property OnCollapse : TNotifyEvent read FOnCollapse write FOnCollapse;
    property OnExpand   : TNotifyEvent read FOnExpand   write FOnExpand;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Forms, SysUtils,
  dxNavBarGraphics,
  dxNavBarExplorerViews;

{ TFVBFFCFoldablePanel }

{*****************************************************************************
  This method will be used to calculate the bounds of the different Rets.
  Depending on the Size of the control, border settings, and other properties
  the different rects used to paint the control need to be recalculated.

  @Name       TFVBFFCFoldablePanel.CalculateBounds
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.CalculateBounds;
var
  Delta : Integer;
begin
  { First Clear the existing Rectangles }
  ClearRects;

  FCaptionRect.Left  := BorderWidth;
  FCaptionRect.Top   := BorderWidth;
  FCaptionRect.Right := ClientWidth - BorderWidth;
  FCaptionRect.Bottom := BorderWidth + GetCaptionHeight;
  FCaptionTextRect := FCaptionRect;
  InflateRect(FCaptionTextRect, -2, -1);

  FCaptionImageRect.Left := FCaptionRect.Left + CaptionImageIndent;
  FCaptionImageRect.Bottom := FCaptionRect.Bottom;
  FCaptionImageRect.Right := FCaptionImageRect.Left + CaptionImageSize;
  FCaptionImageRect.Top := FCaptionImageRect.Bottom - CaptionImageSize;
  Delta := (FCaptionRect.Bottom - FCaptionRect.Top) div 2 - CaptionImageSize div 2;
  OffsetRect(FCaptionImageRect, 0, -Delta);
  FCaptionTextRect.Left := FCaptionImageRect.Right + CaptionTextIndent;

  Delta := (FCaptionRect.Bottom - FCaptionRect.Top - CaptionSignSize) div 2;
  FCaptionSignRect.Top := FCaptionRect.Top + Delta + 1;
  FCaptionSignRect.Bottom := FCaptionSignRect.Top + CaptionSignSize;
  FCaptionSignRect.Right := FCaptionRect.Right - Delta;
  FCaptionSignRect.Left := FCaptionSignRect.Right - CaptionSignSize;
  FCaptionTextRect.Right := FCaptionSignRect.Left - Delta;//TODO

//  csPhoenixComponents.EnterMethod( Self, 'CalculateBounds' );

  FFoldableAreaRect.Left := FCaptionRect.Left;
  FFoldableAreaRect.Top := FCaptionRect.Bottom;
  FFoldableAreaRect.Right := FCaptionRect.Right;
  FFoldableAreaRect.Bottom := Height - BorderWidth;

//  csPhoenixComponents.SendRect( 'FFoldableAreaRect', FFoldableAreaRect );
//  csPhoenixComponents.ExitMethod( Self, 'CalculateBounds' );
end;

{*****************************************************************************
  This method will reset all the Rects used for Painting the Control.

  @Name       TFVBFFCFoldablePanel.ClearRects
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.ClearRects;
begin
  FCaptionRect      := Rect( 0, 0, 0, 0 );
  FCaptionTextRect  := Rect( 0, 0, 0, 0 );
  FCaptionSignRect  := Rect( 0, 0, 0, 0 );
  FCaptionImageRect := Rect( 0, 0, 0, 0 );
  FFoldableAreaRect := Rect( 0, 0, 0, 0 );
  FRect             := Rect( 0, 0, Width, Height );
end;

{*****************************************************************************
  Overridden constructor in which we will initialise some variables.

  @Name       TFVBFFCFoldablePanel.Create
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TFVBFFCFoldablePanel.Create(AOwner: TComponent);
begin
  FStyleFoldArea  := TFVBFFCFoldablePanelStyle.Create( Self );
  FStyleFoldArea.OnChange := DoStyleChanged;
  FStyleBackGround:= TFVBFFCFoldablePanelStyle.Create( Self );
  FStyleBackGround.OnChange := DoStyleChanged;
  FStyleHeader    := TFVBFFCFoldablePanelStyle.Create( Self );
  FStyleHeader.OnChange := DoStyleChanged;
  CreateAdvExplorerBarColors;

  inherited Create( AOwner );

  { Initialise the Colors / Images }
  Expanded       := True;
  ExpandedHeight := 200;
  Height := 200;

  BevelInner  := bvNone;
  BevelOuter  := bvNone;
  BorderStyle := bsNone;
  DoubleBuffered := True;
  BorderWidth := 4;
  ControlStyle := ControlStyle + [ csOpaque ];


  InitialiseStyles;

  FRecalculateBounds   := True;
  FReDrawBackGround    := True;
  FRedrawHeaderSign    := True;
  FRedrawHeaderCaption := True;
  FRedrawHeaderArea    := True;
  FRedrawClientArea    := True;
end;

{*****************************************************************************
  Returns the Height of the Caption Area.

  @Name       TFVBFFCFoldablePanel.GetCaptionHeight
  @author     slesage
  @param      None
  @return     Returns the Height of the Caption Area.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetCaptionHeight: Integer;
  function GetFontHeight(AFont : TFont) : Integer;
  var
    DC : HDC;
    SaveFont : HFont;
    Metrics : TTextMetric;
  begin
    DC := GetDC(0);
    SaveFont := SelectObject(DC, AFont.Handle);
    GetTextMetrics(DC, Metrics);
    SelectObject(DC, SaveFont);
    ReleaseDC(0, DC);
    Result := Metrics.tmHeight;
  end;
begin
  Result := 6 + GetFontHeight( StyleHeader.Font ) + 6;
end;

{*****************************************************************************
  Returns the Size of the Caption Image.

  @Name       TFVBFFCFoldablePanel.GetCaptionImageSize
  @author     slesage
  @param      None
  @return     Returns the Size of the Caption Image.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetCaptionImageSize: Integer;
begin
  Result := 16;
end;

{*****************************************************************************
  Returns the size of the Caption Sign.

  @Name       TFVBFFCFoldablePanel.GetCaptionSignSize
  @author     slesage
  @param      None
  @return     Returns the size of the Caption Sign.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetCaptionSignSize: Integer;
begin
  Result := 22;
end;

{*****************************************************************************
  Returns the number of pixes used to indent the Caption Image.

  @Name       TFVBFFCFoldablePanel.GetCaptionImageIndent
  @author     slesage
  @param      None
  @return     Returns the number of pixes used to indent the Caption Image.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetCaptionImageIndent: Integer;
begin
  Result := 4;
end;

{*****************************************************************************
  Property setter of the Expanded propert.  Depending on the Expanded proeprty
  some parts of the control should be recalculated / repainted.

  @Name       TFVBFFCFoldablePanel.SetExpanded
  @author     slesage
  @param      Value   New Value for the Expanded Property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.SetExpanded(const Value: Boolean);
begin
  if ( Value <> FExpanded ) then
  begin
    FExpanded := Value;

    if ( Expanded ) then
    begin
      inherited Height := ExpandedHeight;
      DoOnExpand;
    end
    else
    begin
      inherited Height := MinimumHeight;
      DoOnCollapse;
    end;

    FRedrawHeaderSign := True;
    FRedrawHeaderCaption := True;
    FRedrawClientArea := True;

    InvalidateAll( doRedraw );
    CheckChildTabStops;
  end;
end;

{*****************************************************************************
  Returns the number of pixels used to indent the Caption.

  @Name       TFVBFFCFoldablePanel.GetCaptionTextIndent
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetCaptionTextIndent: Integer;
begin
  Result := 4;
end;

{*****************************************************************************
  Overridden Paint Method.  Instead of using the default Paint method, we will
  use some DevExpress Painters to draw all parts of our control.

  @Name       TFVBFFCFoldablePanel.Paint
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.Paint;
begin
  if ( FRecalculateBounds ) then
  begin
    CalculateBounds;
  end;

  DrawControlBackGround;
  DrawHeaderArea;
  DrawClientArea;

  FRecalculateBounds   := False;
  FReDrawBackGround    := False;
  FRedrawHeaderSign    := False;
  FRedrawHeaderCaption := False;
  FRedrawHeaderArea    := False;
  FRedrawClientArea    := False;
end;

{*****************************************************************************
  Returns the State of the Control.  This state depends on different things
  and will be used by the Painting Mechanism.

  @Name       TFVBFFCFoldablePanel.GetState
  @author     slesage
  @param      None
  @return     Returns the State of the Control.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetState: TdxNavBarObjectStates;
begin
  Result := [];

  if ( Expanded ) then
  begin
    Result := Result + [ sExpanded ];
  end;
  if ( FMouseInControl ) then
  begin
    Result := Result + [ sHotTracked ];
  end;
end;

{*****************************************************************************
  Sets the Left, Top, Width, and Height properties all at once.

  @Name       TFVBFFCFoldablePanel.SetBounds
  @author     slesage
  @param      ALeft    The new value for the Left property.
              ATop     The new value for the Top property.
              AWidth   The new value for the Width property.
              AHeight  The new value for the Height property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.SetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  if ( aHeight <= MinimumHeight ) or
     ( not Expanded ) then
  begin
    aHeight := MinimumHeight;
  end;

  inherited SetBounds(ALeft, ATop, AWidth, AHeight);

  if ( Expanded ) then
  begin
    FExpandedHeight := aHeight;
  end;

  FRect.Top    := 0;
  FRect.Bottom := Height;
  FRect.Right  := Width;
  FRect.Left   := 0;

  if HandleAllocated then
  begin
    InvalidateAll( doRecalc );
  end;
end;

{*****************************************************************************
  Returns the minimum height of the control.  The minimum Height is the
  height of the Caption with the size of the borders added to it.

  @Name       TFVBFFCFoldablePanel.GetMinimumHeight
  @author     slesage
  @param      None
  @return     Returns the minimum height of the control
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetMinimumHeight: Integer;
begin
  Result := CaptionHeight + ( 2 * BorderWidth );
end;

{*****************************************************************************
  This method will be executed when the Mouse pointer enters the control.

  @Name       TFVBFFCFoldablePanel.CMMouseEnter
  @author     slesage
  @param      None
  @return     Message   The CM_MOUSEENTER message.
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.CMMouseEnter(var Message: TMessage);
var
  aMouseInControl : Boolean;
begin
  aMouseInControl := FMouseInControl;
  FMouseInControl := True;

  if ( aMouseInControl <> FMouseInControl ) then
  begin
    InvalidateAll( doRedrawHeader );
  end;
end;

{*****************************************************************************
  This method will be executed when the Mouse pointer leaves the control.

  @Name       TFVBFFCFoldablePanel.CMMouseLeave
  @author     slesage
  @return     Message   The CM_MOUSELEAVE message.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.CMMouseLeave(var Message: TMessage);
var
  aMouseInControl : Boolean;
begin
  aMouseInControl := FMouseInControl;
  FMouseInControl := False;

  if ( aMouseInControl <> FMouseInControl ) then
  begin
    InvalidateAll( doRedrawHeader );
  end;
end;

{*****************************************************************************
  Returns a boolean indicating if the mouse pointer is currently located
  in the Header area of the component.

  @Name       TFVBFFCFoldablePanel.GetMouseInHeader
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCFoldablePanel.GetMouseInHeader: Boolean;
var
  P: TPoint;
  R: TRect;
begin
  GetCursorPos(P);
  P := ScreenToClient(P);
  R := FCaptionRect;
  Result := PtInRect(R, P);
end;

{*****************************************************************************
  Overridden Click method in which we will check if the Mouse Pointer was
  currently in the Header area.  If that was the case we should Collapse /
  Expand the control accordingly.

  @Name       TFVBFFCFoldablePanel.Click
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.Click;
begin
  If ( MouseInHeader ) then
  begin
    Expanded := not Expanded;
  end;
  inherited Click;
end;

{*****************************************************************************
  This method will be used to paint the BackGround of the Control.

  @Name       TFVBFFCFoldablePanel.DrawControlBackGround
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.DrawControlBackGround;
begin
{GCXE
  TdxNavBarBackgroundPainter.DrawBackground( Canvas, FRect, Nil, True,
                                             clNone, StyleBackGround.BackColor,
                                             StyleBackGround.BackColor2,
                                             StyleBackGround.AlphaBlending,
                                             StyleBackGround.AlphaBlending2,
                                             StyleBackGround.GradientMode );
  }
end;

{*****************************************************************************
  This method will be used to paint the Header Area of the Control

  @Name       TFVBFFCFoldablePanel.DrawHeaderArea
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
  function CalcTextColor(AColor : TColor; AState : TdxNavBarObjectStates) : TColor;
    function CalcColor(d:Integer) : TColor;
      function GetRed(color : ARGB) : BYTE;
      begin
        result := Byte(color shr 16);
      end;
      function GetGreen(color : ARGB) : BYTE;
      begin
        result := Byte(color shr 8);
      end;
      function GetBlue(color : ARGB) : BYTE;
      begin
        result := Byte(color shr 0);
      end;
      function ColorRefToARGB(rgb : COLORREF) : ARGB;
        function MakeColor(a,r,g,b : Byte) : ARGB;
        begin
          result := ((DWORD(b) shl 0) or
                     (DWORD(g) shl 8) or
                     (DWORD(r) shl 16) or
                     (DWORD(a) shl 24));
        end;
      begin
        result := MakeColor(255,GetRValue(rgb),GetGValue(rgb),GetBValue(rgb));
      end;
    var
      clrColor : TColor;
      r,g,b : Integer;
      AMax, ADelta, AMaxDelta : Integer;
    begin
      clrColor := clHighlight;
      r := GetRed(clrColor);
      g := GetGreen(clrColor);
      b := GetBlue(clrColor);
      AMax := Max(Max(r,g),b);
      ADelta := $23 + d;
      AMaxDelta := (255 - (AMax + ADelta));
      if AMaxDelta > 0 then AMaxDelta := 0;
      Inc(r, (ADelta + AMaxDelta + 5));
      Inc(g, (ADelta + AMaxDelta));
      Inc(b, (ADelta + AMaxDelta));
      if r > 255 then r := 255;
      if g > 255 then r := 255;
      if b > 255 then r := 255;
      Result := RGB(abs(r),abs(g),abs(b));
      Result := ColorRefToARGB(Result);
    end;
  begin
    if AColor = clNone then
      Result := CalcColor(-50)
    else  Result := AColor;

    if (sDisabled in AState) then
      Result := LightLightColor(Result)
    else  if (sSelected in AState) or (SHotTracked in AState) or (sPressed in AState) then
      Result := LightColor(Result);
  end;


procedure TFVBFFCFoldablePanel.DrawHeaderArea;
var
  Flags : Integer;
begin
  { Draw the Header Button }
  TdxNavBarAdvExplorerButtonPainter.DrawButton( Canvas, FCaptionRect, nil,
                                                FStyleHeader.BackColor,
                                                FStyleHeader.BackColor2,
                                                FStyleHeader.AlphaBlending,
                                                FStyleHeader.AlphaBlending2,
                                                FStyleHeader.GradientMode,
                                                clGreen, [ ] );

  { Draw the Caption }
  Flags := DT_CENTER or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS;

  Canvas.Brush.Style := bsClear;
  Canvas.Font := FStyleHeader.Font;
  Canvas.Font.Color := CalcTextColor( FStyleHeader.Font.Color { clNone }, State);
  DrawText(Canvas.Handle, PChar(Caption), Length(Caption), FCaptionTextRect, Flags);

  { Draw the Expand / Collapse Sign }
  TdxNavBarAdvExplorerBarSignPainter.DrawSign(Canvas, FCaptionSignRect, Canvas.Font.Color , clWindow, LightLightColor(clHighlight), State );
  TdxNavBarAdvExplorerBarSignPainter.DrawSign( Canvas, FCaptionSignRect, CalcTextColor(clNone, State), clWindow, LightLightColor(clHighlight), State );

  FRedrawHeaderArea    := False;
  FRedrawHeaderSign    := False;
  FRedrawHeaderCaption := False;
end;

{*****************************************************************************
  This method will be used to draw the Client Area of the Control.

  @Name       TFVBFFCFoldablePanel.DrawClientArea
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.DrawClientArea;
begin
  if ( Expanded ) then
  begin
    {GCXE
    TdxNavBarGroupBackgroundPainter.DrawBackground(Canvas, FFoldableAreaRect, Nil,
        StyleFoldArea.BackColor, StyleFoldArea.BackColor2,
        StyleFoldArea.AlphaBlending, StyleFoldArea.AlphaBlending2,
        StyleFoldArea.GradientMode );
//        LightLightColor(clInactiveCaption), LightLightColor(clInactiveCaption), 255, 255, gmHorizontal);
    }
    TdxNavBarExplorerBarBorderPainter.DrawBorder(Canvas, FFoldableAreaRect, clWhite, True);
  end
  else
  begin
    {GCXE
    TdxNavBarGroupBackgroundPainter.DrawBackground(Canvas, FFoldableAreaRect, Nil,
        StyleBackGround.BackColor, StyleBackGround.BackColor2,
        StyleBackGround.AlphaBlending, StyleBackGround.AlphaBlending2,
        StyleBackGround.GradientMode );
    }
//        FBgColor, FBgColor2, FBgAlphaBlending, FBgAlphaBlending2, gmHorizontal);
  end;
end;

{*****************************************************************************
  Adjusts the ClientRect property for the idiosyncrasies of the window.

  @Name       TFVBFFCFoldablePanel.AdjustClientRect
  @author     slesage
  @param      Rect   The rectangle representing the Client Area of the cotnrol.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.AdjustClientRect(var Rect: TRect);
var
  aBordersWidth : Integer;
begin
  inherited AdjustClientRect( Rect );

  if ( Expanded ) then
  begin
    aBordersWidth := {BorderWidth} + 1;
    InflateRect( Rect, - aBordersWidth, - aBordersWidth );
    Rect.Top := Rect.Top + CaptionHeight -1;// + 1;
  end
  else
  begin
    Rect := Classes.Rect( 0,0,0,0 );
  end;
end;

procedure TFVBFFCFoldablePanel.InvalidateAll(
  aType: TFVBFFCFoldablePanelChangeType);
begin
  if csDestroying in  ComponentState then exit;
  InvalidateViewInfo(AType);
  if HandleAllocated then Invalidate;
end;

{*****************************************************************************
  This method will be used to indicate which parts of the controls should be
  repainted, depending on the AType passed to the method.

  @Name       TFVBFFCFoldablePanel.InvalidateViewInfo
  @author     slesage
  @param      AType   The UpdateType for the control.  Depending on this
                      variable the different Painting flags will be enabled
                      or disabled.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.InvalidateViewInfo(
  AType: TFVBFFCFoldablePanelChangeType);
begin
  FRedrawBackGround    := True;
  case AType of
    doRecreate :
    begin
      FReCalculateBounds   := True;
      FRedrawClientArea    := True;
      FRedrawHeaderArea    := True;
      FRedrawHeaderCaption := True;
      FRedrawHeaderSign    := True;
    end;
    doRecalc :
    begin
      FReCalculateBounds   := True;
      FRedrawClientArea    := True;
      FRedrawHeaderArea    := True;
      FRedrawHeaderCaption := True;
      FRedrawHeaderSign    := True;
    end;
    doRedraw :
    begin
      FRedrawClientArea    := True;
      FRedrawHeaderArea    := True;
      FRedrawHeaderCaption := True;
      FRedrawHeaderSign    := True;
    end;
    doRedrawHeader :
    begin
      FRedrawBackGround    := False;
      FRedrawHeaderArea    := True;
      FRedrawHeaderCaption := True;
      FRedrawHeaderSign    := True;
    end;
    doRedrawClient :
    begin
      FRedrawBackGround    := False;
      FRedrawClientArea    := True;
    end;
    doRedrawHeaderAndClient :
    begin
      FRedrawBackGround    := False;
      FRedrawClientArea    := True;
      FRedrawHeaderArea    := True;
      FRedrawHeaderCaption := True;
      FRedrawHeaderSign    := True;
    end;
  end;
end;

{*****************************************************************************
  Overridden Loaded Method.

  @Name       TFVBFFCFoldablePanel.Loaded
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.Loaded;
begin
  inherited;
  InvalidateAll(doRecreate);
end;

{*****************************************************************************
  Sets or gets the TabStop value of child controls depending on the value of Collapsed.
  When Collapsed is True, calls GetChildTabStops.
  When Collapsed is False, calls SetChildTabStops.

  @Name       TFVBFFCFoldablePanel.CheckChildTabStops
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.CheckChildTabStops;
begin
  if not ( csDesigning in ComponentState ) then
  begin
    if Expanded then
      SetChildTabStops
    else
      GetChildTabStops;
  end;
end;

{*****************************************************************************
  Clears the internal list with children TabStop values *without* restoring
  the childrens TabStop values first.
  Normally, you don't need to call this method.

  @Name       TFVBFFCFoldablePanel.ClearChildTabStops
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.ClearChildTabStops;
begin
  FreeAndNil( FTabStops );
end;

{*****************************************************************************
  Checks the TabStop value of all child controls and adds the control to
  an internal list if TabStop is True. TabStop is then set to False.
  This is done to disable tabbing into the child control when the rollout
  is collapsed. Normally, you don't need to call this method.

  @Name       TFVBFFCFoldablePanel.GetChildTabStops
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.GetChildTabStops;
var
  I: Integer;
begin
  if FTabStops = nil then
  begin
    FTabStops := TStringList.Create;
    FTabStops.Sorted := True;
  end;
  for I := 0 to ControlCount - 1 do
  begin
    if (Controls[I] is TWinControl) and (TWinControl(Controls[I]).TabStop) then
    begin
      FTabStops.AddObject(Controls[I].Name, Controls[I]);
      TWinControl(Controls[I]).TabStop := False;
    end;
  end;
end;

{*****************************************************************************
  Resets the TabStop value of child controls to True if they where added to
  an internal list with a previous call to GetTabStops.
  Does nothing if GetChildTabStops hasn't been called.
  Normally, you don't need to call this method.

  @Name       TFVBFFCFoldablePanel.SetChildTabStops
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.SetChildTabStops;
var
  I: Integer;
begin
  if FTabStops <> nil then
  begin
    for I := 0 to FTabStops.Count - 1 do
    begin
      if FindChildControl(FTabStops[I]) <> nil then
      begin
        TWinControl(FTabStops.Objects[I]).TabStop := True;
      end;
    end;
    FreeAndNil(FTabStops);
  end;
end;

{*****************************************************************************
  Overridden Destructor in which we will do some CleanUp.

  @Name       TFVBFFCFoldablePanel.Destroy
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

destructor TFVBFFCFoldablePanel.Destroy;
begin
  { Initialise the Colors / Images }
  ReleaseAdvExplorerBarColors;

  FreeAndNil( FStyleFoldArea );
  FreeAndNil( FStyleBackGround );
  FreeAndNil( FStyleHeader );

  FreeAndNil( FTabStops );
  inherited;
end;

procedure TFVBFFCFoldablePanel.WMKillFocus(var Message: TWMSetFocus);
begin
  CheckChildTabStops;
  inherited;
  Invalidate;
end;

procedure TFVBFFCFoldablePanel.WMSetFocus(var Message: TWMSetFocus);
begin
  CheckChildTabStops;
  inherited;
  Invalidate;
end;

procedure TFVBFFCFoldablePanel.SetExpandedHeight(const Value: Integer);
begin
  if ( Value <> FExpandedHeight ) then
  begin
    FExpandedHeight := Value;

    if ( Expanded ) then
    begin
      inherited Height := FExpandedHeight;
    end;
  end;
end;

function TFVBFFCFoldablePanel.GetHeight: Integer;
begin
  Result := Inherited Height;
end;

procedure TFVBFFCFoldablePanel.SetHeight(const Value: Integer);
begin
  ExpandedHeight := Value;
end;

{*****************************************************************************
  OnCollapse Event Dispatcher.

  @Name       TFVBFFCFoldablePanel.DoOnCollapse
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.DoOnCollapse;
begin
  if ( Assigned( FOnCollapse ) ) then
  begin
    FOnCollapse( Self );
  end;
end;

{*****************************************************************************
  OnExpand Event Dispatcher.

  @Name       TFVBFFCFoldablePanel.DoOnExpand
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCFoldablePanel.DoOnExpand;
begin
  if ( Assigned( FOnExpand ) ) then
  begin
    FOnExpand( Self );
  end;
end;


{ TFVBFFCFoldablePanelStyle }

procedure TFVBFFCFoldablePanelStyle.Assign(Source: TPersistent);
begin
  if Source is TFVBFFCFoldablePanelStyle then
  begin
    FAlphaBlending := TFVBFFCFoldablePanelStyle(Source).AlphaBlending;
    FAlphaBlending2 := TFVBFFCFoldablePanelStyle(Source).AlphaBlending2;
    FBackColor := TFVBFFCFoldablePanelStyle(Source).BackColor;
    FBackColor2 := TFVBFFCFoldablePanelStyle(Source).BackColor2;
    FGradientMode := TFVBFFCFoldablePanelStyle(Source).GradientMode;
    FFont.Assign(TFVBFFCFoldablePanelStyle(Source).Font);
  end
  else inherited Assign(Source);
end;

procedure TFVBFFCFoldablePanelStyle.Changed(
  AType: TFVBFFCFoldablePanelChangeType);
begin
  if ( Assigned( FOnChange ) ) then
  begin
    FOnChange( Self, AType );
  end;
end;

constructor TFVBFFCFoldablePanelStyle.Create(AOwner: TPersistent);
begin
  Inherited Create;
  FOwner := aOwner;

  FFont := TFont.Create;
  FFont.Name := 'Verdana';
  FFont.OnChange := DoFontChanged;
  FBackColor := clWhite;
  FBackColor2 := clWhite;
  FAlphaBlending := 255;
  FAlphaBlending2 := 255;
  FGradientMode := gmHorizontal;
  FVisible := True;
end;

destructor TFVBFFCFoldablePanelStyle.Destroy;
begin
  FreeAndNil( FFont );
  inherited;
end;

procedure TFVBFFCFoldablePanelStyle.DoFontChanged(Sender: TObject);
begin
  Changed( doRedraw );
end;

function TFVBFFCFoldablePanelStyle.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

procedure TFVBFFCFoldablePanelStyle.SetAlphaBlending(const Value: Byte);
begin
  if FAlphaBlending <> Value then
  begin
    FAlphaBlending := Value;
    Changed(doRedraw);
  end;
end;

procedure TFVBFFCFoldablePanelStyle.SetAlphaBlending2(const Value: Byte);
begin
  if FAlphaBlending2 <> Value then
  begin
    FAlphaBlending2 := Value;
    Changed(doRedraw);
  end;
end;

procedure TFVBFFCFoldablePanelStyle.SetBackColor(const Value: TColor);
begin
  if FBackColor <> Value then
  begin
    FBackColor := Value;
    Changed(doRedraw);
  end;
end;

procedure TFVBFFCFoldablePanelStyle.SetBackColor2(const Value: TColor);
begin
  if FBackColor2 <> Value then
  begin
    FBackColor2 := Value;
    Changed(doRedraw);
  end;
end;

procedure TFVBFFCFoldablePanelStyle.SetFont(const Value: TFont);
begin
  FFont.Assign(Value);
  Changed(doRecalc);
end;

procedure TFVBFFCFoldablePanelStyle.SetGradientMode(
  const Value: TdxBarStyleGradientMode);
begin
  if FGradientMode <> Value then
  begin
    FGradientMode := Value;
    Changed(doRedraw);
  end;
end;

procedure TFVBFFCFoldablePanel.SetStyleBackGround(
  const Value: TFVBFFCFoldablePanelStyle);
begin
  FStyleBackGround.Assign( Value );
end;

procedure TFVBFFCFoldablePanel.SetStyleFoldArea(
  const Value: TFVBFFCFoldablePanelStyle);
begin
  FStyleFoldArea.Assign( Value );
end;

procedure TFVBFFCFoldablePanel.SetStyleHeader(
  const Value: TFVBFFCFoldablePanelStyle);
begin
  FStyleHeader.Assign( Value );
end;

procedure TFVBFFCFoldablePanel.DoStyleChanged(Sender: TObject;
  AType: TFVBFFCFoldablePanelChangeType);
begin
  InvalidateAll( AType );
end;

procedure TFVBFFCFoldablePanel.InitialiseStyles;
begin
  FStyleBackGround.BackColor := clAppWorkSpace;
  FStyleBackGround.BackColor2:= clAppWorkSpace;
  FStyleHeader.Font.Style := FStyleHeader.Font.Style + [ fsBold ];
  FStyleHeader.BackColor := clWindow;
  FStyleHeader.BackColor2 := LightLightColor(clHighlight);
  FStyleFoldArea.BackColor := LightLightColor(clInactiveCaption);
  FStyleFoldArea.BackColor2 := LightLightColor(clInactiveCaption);
end;

{ TFVBFFCPanel }

{*****************************************************************************
  Overridden constructor in which we will initialise some things.

  @Name       TFVBFFCPanel.Create
  @author     slesage
  @param      aOwner   The Owner of the Panel.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TFVBFFCPanel.Create(AOwner: TComponent);
begin
  FStyleBackGround:= TFVBFFCFoldablePanelStyle.Create( Self );
  FStyleBackGround.OnChange := DoStyleChanged;

  FStyleClientArea := TFVBFFCFoldablePanelStyle.Create( Self );
  FStyleClientArea.OnChange := DoStyleChanged;
  CreateAdvExplorerBarColors;

  inherited Create( AOwner );

  BevelInner  := bvNone;
  BevelOuter  := bvNone;
  BorderStyle := bsNone;
  BorderWidth := 4;
  ControlStyle := ControlStyle + [ csOpaque ];
  InitialiseStyles;

  {$IFDEF CUSTOMBUFFERED}
  FCache := TBitmap.Create;
  FCacheValid := False;
  DoubleBuffered := False;
  {$ELSE}
  DoubleBuffered := True;
  {$ENDIF}
end;

{*****************************************************************************
  Overridden destructor in which we will do all the necessary CleanUp.

  @Name       TFVBFFCPanel.Destroy
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

destructor TFVBFFCPanel.Destroy;
begin
  FreeAndNil( FStyleBackGround );
  FreeAndNil( FStyleClientArea );
  {$IFDEF CUSTOMBUFFERED}
  FreeAndNil( FCache );
  {$ENDIF}
  inherited;
end;

{*****************************************************************************
  StyleChanged event dispatcher which will be used to invalidate the control
  when something in one of the styles gets changed.

  @Name       TFVBFFCPanel.DoStyleChanged
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCPanel.DoStyleChanged(Sender: TObject;
  AType: TFVBFFCFoldablePanelChangeType);
begin
  InvalidateAll( AType );
end;

function TFVBFFCPanel.GetBorderWidth: Integer;
begin
  Result := Inherited BorderWidth;
end;

{*****************************************************************************
  This method will be used to initialise some Styles and Colors.

  @Name       TFVBFFCPanel.InitialiseStyles
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCPanel.InitialiseStyles;
begin
  FStyleBackGround.BackColor := clAppWorkSpace;
  FStyleBackGround.BackColor2:= clAppWorkSpace;

  FStyleClientArea.Font.Style := FStyleClientArea.Font.Style + [ fsBold ];
  FStyleClientArea.BackColor := clWindow;
  FStyleClientArea.BackColor2 := LightLightColor(clHighlight);
end;

procedure TFVBFFCPanel.InvalidateAll(aType: TFVBFFCFoldablePanelChangeType);
begin
  if csDestroying in  ComponentState then exit;
  InvalidateViewInfo(AType);
  {$IFDEF CUSTOMBUFFERED}
  InvalidateCache;
  {$ENDIF}
  if HandleAllocated then Invalidate;
end;

procedure TFVBFFCPanel.InvalidateViewInfo(AType: TFVBFFCFoldablePanelChangeType);
begin
  FRedrawBackGround    := True;
end;

procedure TFVBFFCPanel.PaintToCanvas(const aRect : TRect; aCanvas : TCanvas);
  function CalcTextColor(AColor : TColor; AState : TdxNavBarObjectStates) : TColor;
    function CalcColor(d:Integer) : TColor;
      function GetRed(color : ARGB) : BYTE;
      begin
        result := Byte(color shr 16);
      end;
      function GetGreen(color : ARGB) : BYTE;
      begin
        result := Byte(color shr 8);
      end;
      function GetBlue(color : ARGB) : BYTE;
      begin
        result := Byte(color shr 0);
      end;
      function ColorRefToARGB(rgb : COLORREF) : ARGB;
        function MakeColor(a,r,g,b : Byte) : ARGB;
        begin
          result := ((DWORD(b) shl 0) or
                     (DWORD(g) shl 8) or
                     (DWORD(r) shl 16) or
                     (DWORD(a) shl 24));
        end;
      begin
        result := MakeColor(255,GetRValue(rgb),GetGValue(rgb),GetBValue(rgb));
      end;
    var
      clrColor : TColor;
      r,g,b : Integer;
      AMax, ADelta, AMaxDelta : Integer;
    begin
      clrColor := clHighlight;
      r := GetRed(clrColor);
      g := GetGreen(clrColor);
      b := GetBlue(clrColor);
      AMax := Max(Max(r,g),b);
      ADelta := $23 + d;
      AMaxDelta := (255 - (AMax + ADelta));
      if AMaxDelta > 0 then AMaxDelta := 0;
      Inc(r, (ADelta + AMaxDelta + 5));
      Inc(g, (ADelta + AMaxDelta));
      Inc(b, (ADelta + AMaxDelta));
      if r > 255 then r := 255;
      if g > 255 then r := 255;
      if b > 255 then r := 255;
      Result := RGB(abs(r),abs(g),abs(b));
      Result := ColorRefToARGB(Result);
    end;
  begin
    if AColor = clNone then
      Result := CalcColor(-50)
    else  Result := AColor;

    if (sDisabled in AState) then
      Result := LightLightColor(Result)
    else  if (sSelected in AState) or (SHotTracked in AState) or (sPressed in AState) then
      Result := LightColor(Result);
  end;
var
  aPanelRect       : TRect;
  aCaptionRect     : TRect;
  aCaptionTextRect : TRect;
  Flags: Longint;
begin
  aCaptionRect.Left  := BorderWidth;
  aCaptionRect.Top   := BorderWidth;
  aCaptionRect.Right := (aRect.Right - aRect.Left) - BorderWidth;
  aCaptionRect.Bottom := (aRect.Bottom - aRect.Top) - BorderWidth;
  aCaptionTextRect := aCaptionRect;

  aPanelRect := aRect;

  { Drawn the Controls Background }
  if StyleBackGround.Visible then
  begin
    InflateRect( aCaptionTextRect, -2, -1 );
    {GCXE
    TdxNavBarBackgroundPainter.DrawBackground( aCanvas, aPanelRect, Nil, True,
                                             clNone, StyleBackGround.BackColor,
                                             StyleBackGround.BackColor2,
                                             StyleBackGround.AlphaBlending,
                                             StyleBackGround.AlphaBlending2,
                                             StyleBackGround.GradientMode );
    }
  end;

  if StyleClientArea.Visible then
  begin
    { Draw the Controls Client Area }
    TdxNavBarAdvExplorerButtonPainter.DrawButton( aCanvas, aCaptionRect, nil,
                                                  StyleClientArea.BackColor,
                                                  StyleClientArea.BackColor2,
                                                  StyleClientArea.AlphaBlending,
                                                  StyleClientArea.AlphaBlending2,
                                                  StyleClientArea.GradientMode,
                                                  clGreen, [ ] );
  end;

  Flags := DT_CENTER or DT_VCENTER or DT_SINGLELINE or DT_END_ELLIPSIS;
  aCanvas.Brush.Style := bsClear;
  aCanvas.Font := StyleClientArea.Font;
  aCanvas.Font.Color := CalcTextColor(StyleClientArea.Font.Color, [ ] );
  DrawText(aCanvas.Handle, PChar(Caption), Length(Caption), aCaptionTextRect, Flags);
end;

procedure TFVBFFCPanel.SetBorderWidth(const Value: Integer);
begin
  Inherited BorderWidth := Value;
end;

{*****************************************************************************
  Overridden Paint method in which we will take care of all the painting
  ourselves.

  @Name       TFVBFFCPanel.Paint
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCPanel.Paint;
var
  R : TRect;
begin
  R := GetClientRect;
  {$IFDEF CUSTOMBUFFERED}
  if not FCacheValid then
  begin
    FCache.Width := R.Right - R.Left;
    FCache.Height := R.Bottom - R.Top;
    PaintToCanvas(R, FCache.Canvas);
    FCacheValid := True;
  end;
  Canvas.Draw( 0, 0, FCache);
  {$ELSE}
  PaintToCanvas(R, Canvas);
  {$ENDIF}
end;

{*****************************************************************************
  Property Setter for the StyleBackGround property.

  @Name       TFVBFFCPanel.SetStyleBackGround
  @author     slesage
  @param      New Value for the StyleBackGround property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCPanel.SetStyleBackGround(
  const Value: TFVBFFCFoldablePanelStyle);
begin
  FStyleBackGround.Assign( Value );
end;

{*****************************************************************************
  Property Setter for the StyleClientArea property.

  @Name       TFVBFFCPanel.SetStyleClientArea
  @author     slesage
  @param      New Value for the StyleClientArea Property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCPanel.SetStyleClientArea(
  const Value: TFVBFFCFoldablePanelStyle);
begin
  FStyleClientArea.Assign( Value );
end;

{*****************************************************************************
  When the control is resized, all inherited controls need to be repainted to

  @Name       TFVBFFCPanel.Resize
  @author     amuylaer
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}


procedure TFVBFFCPanel.Resize;
var
  I : Integer;
begin
  inherited;
  {$IFDEF CUSTOMBUFFERED}
  InvalidateCache;
  {$ENDIF}
  {
  When the control resizes, all children should invalidate.
  Otherwise transparent labels are painted incorrectly
  }
  for I := ControlCount - 1 downto 0 do
  begin
    Controls[I].Invalidate;
  end;
end;

{$IFDEF CUSTOMBUFFERED}
{*****************************************************************************
  Invalidates the cache

  @Name       TFVBFFCPanel.InvalidateCache
  @author     amuylaer
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TFVBFFCPanel.InvalidateCache;
begin
  FCacheValid := False;
  if HandleAllocated then Invalidate;
end;
{$ENDIF}

{$IFDEF CUSTOMBUFFERED}
{*****************************************************************************
  This procedure handles certain events and forces the cache to update itself

  @Name       TFVBFFCPanel.WndProc
  @author     amuylaer
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCPanel.WndProc(var Message: TMessage);
begin
  inherited;
  if (Message.Msg = CM_FOCUSCHANGED) or
     (Message.Msg = CM_PARENTFONTCHANGED) or
     (Message.Msg = CM_PARENTCOLORCHANGED) or
     (Message.Msg = CM_VISIBLECHANGED) or
     (Message.Msg = CM_ENABLEDCHANGED) or
     (Message.Msg = CM_COLORCHANGED) or
     (Message.Msg = CM_FONTCHANGED) or
     (Message.Msg = CM_CTL3DCHANGED) or
     (Message.Msg = CM_PARENTCTL3DCHANGED) or
     (Message.Msg = CM_TEXTCHANGED) or
     (Message.Msg = CM_SYSCOLORCHANGE) or
     (Message.Msg = CM_FONTCHANGE) then begin
    InvalidateCache;
  end;
end;
{$ENDIF}

function TFVBFFCFoldablePanel.GetBorderWidth: Integer;
begin
  Result := Inherited BorderWidth;
end;

procedure TFVBFFCFoldablePanel.SetBorderWidth(const Value: Integer);
begin
  Inherited BorderWidth := Value;
end;


end.
