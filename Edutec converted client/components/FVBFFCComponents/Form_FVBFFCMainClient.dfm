object frmFVBFFCMainClient: TfrmFVBFFCMainClient
  Left = 195
  Top = 123
  Caption = 'frmFVBFFCMainClient'
  ClientHeight = 270
  ClientWidth = 783
  Color = clAppWorkSpace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  FormStyle = fsMDIForm
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object sbrMain: TFVBFFCStatusBar
    Left = 0
    Top = 250
    Width = 783
    Height = 20
    Panels = <>
    PaintStyle = stpsOffice11
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ExplicitTop = -20
    ExplicitWidth = 120
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      'Bestand'
      'Data'
      'General'
      'Window'
      'Menus')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True)
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 24
    Top = 128
    DockControlHeights = (
      0
      0
      137
      0)
    object dxBarManager1Bar1: TdxBar
      AllowQuickCustomizing = False
      Caption = 'Main Menu'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 0
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      IsMainMenu = True
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrsiFile'
        end
        item
          Visible = True
          ItemName = 'dxbrsiData'
        end
        item
          Visible = True
          ItemName = 'dxbrsiGeneral'
        end
        item
          Visible = True
          ItemName = 'dxbrsiWindow'
        end>
      MultiLine = True
      OldName = 'Main Menu'
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = True
    end
    object dxBarManager1Bar2: TdxBar
      AllowClose = False
      AllowQuickCustomizing = False
      Caption = 'Main Toolbar'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 81
      DockingStyle = dsTop
      FloatLeft = 475
      FloatTop = 252
      FloatClientWidth = 68
      FloatClientHeight = 186
      ItemLinks = <>
      OldName = 'Main Toolbar'
      OneOnRow = True
      Row = 2
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object dxBarManager1Bar3: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      Caption = 'ScreenActionsToolbar'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 24
      DockingStyle = dsTop
      FloatLeft = 625
      FloatTop = 185
      FloatClientWidth = 40
      FloatClientHeight = 455
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbEdit'
        end
        item
          Visible = True
          ItemName = 'dxbbView'
        end
        item
          Visible = True
          ItemName = 'dxbbAdd'
        end
        item
          Visible = True
          ItemName = 'dxbbDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbRefreshData'
        end
        item
          Visible = True
          ItemName = 'dxbbCustomiseColumns'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbPrintList'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbExportXLS'
        end
        item
          Visible = True
          ItemName = 'dxbbExportHTML'
        end
        item
          Visible = True
          ItemName = 'dxbbExportXML'
        end>
      MultiLine = True
      OldName = 'ScreenActionsToolbar'
      OneOnRow = True
      Row = 1
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object dxBarManager1Bar4: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      Caption = 'OpenWindows'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 109
      DockingStyle = dsTop
      FloatLeft = 276
      FloatTop = 216
      FloatClientWidth = 23
      FloatClientHeight = 22
      ItemLinks = <>
      OldName = 'OpenWindows'
      OneOnRow = True
      Row = 3
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object dxbbEdit: TdxBarButton
      Caption = 'Edit'
      Category = 2
      Hint = 'Edit|Edit the current Record'
      Visible = ivAlways
      ImageIndex = 2
      PaintStyle = psCaptionGlyph
      ShortCut = 113
    end
    object dxbbView: TdxBarButton
      Caption = 'View'
      Category = 2
      Hint = 'View|View the current Record'
      Visible = ivAlways
      ImageIndex = 3
      PaintStyle = psCaptionGlyph
    end
    object dxbbAdd: TdxBarButton
      Caption = 'Add'
      Category = 2
      Hint = 'Add|Add a new Record'
      Visible = ivAlways
      ImageIndex = 0
      PaintStyle = psCaptionGlyph
      ShortCut = 45
    end
    object dxbbDelete: TdxBarButton
      Caption = 'Delete'
      Category = 2
      Hint = 'Delete|Delete the current Record'
      Visible = ivAlways
      ImageIndex = 1
      PaintStyle = psCaptionGlyph
      ShortCut = 46
    end
    object dxbbRefreshData: TdxBarButton
      Caption = 'Refresh Data'
      Category = 2
      Hint = 
        'Refresh Data|Refreshes the current data with the data from the D' +
        'B'
      Visible = ivAlways
      ImageIndex = 4
      PaintStyle = psCaptionGlyph
      ShortCut = 116
    end
    object dxbbCustomiseColumns: TdxBarButton
      Caption = 'Customise Columns'
      Category = 2
      Hint = 'Customise Columns|Show and Hide columns'
      Visible = ivAlways
      ImageIndex = 11
      PaintStyle = psCaptionGlyph
    end
    object dxbbPrintList: TdxBarButton
      Caption = 'Print List'
      Category = 2
      Hint = 'Print Grid|Print the Contents of the Grid'
      Visible = ivAlways
      ImageIndex = 7
      PaintStyle = psCaptionGlyph
    end
    object dxbbExportXLS: TdxBarButton
      Caption = 'Export XLS'
      Category = 2
      Hint = 'Export to Excel|Export the contents of the grid to an Excel File'
      Visible = ivAlways
      ImageIndex = 8
      PaintStyle = psCaptionGlyph
    end
    object dxbbExportHTML: TdxBarButton
      Caption = 'Export HTML'
      Category = 2
      Hint = 'Export to HTML|Export the contents of the grid to a HTML File'
      Visible = ivAlways
      ImageIndex = 9
      PaintStyle = psCaptionGlyph
    end
    object dxbbExportXML: TdxBarButton
      Caption = 'Export XML'
      Category = 2
      Hint = 'Export to XML|Export the contents of the grid to an XML File'
      Visible = ivAlways
      ImageIndex = 10
      PaintStyle = psCaptionGlyph
    end
    object dxbbWindowClose: TdxBarButton
      Caption = 'C&lose'
      Category = 4
      Enabled = False
      Hint = 'Close'
      Visible = ivAlways
      ImageIndex = 16
    end
    object dxbbTileHorizontally: TdxBarButton
      Caption = 'Tile &Horizontally'
      Category = 4
      Enabled = False
      Hint = 'Tile Horizontal'
      Visible = ivAlways
      ImageIndex = 19
    end
    object dxbbTileVertically: TdxBarButton
      Caption = '&Tile Vertically'
      Category = 4
      Enabled = False
      Hint = 'Tile Vertical'
      Visible = ivAlways
      ImageIndex = 15
    end
    object dxbbCascade: TdxBarButton
      Caption = '&Cascade'
      Category = 4
      Enabled = False
      Hint = 'Cascade'
      Visible = ivAlways
      ImageIndex = 20
    end
    object dxbbArrangeAll: TdxBarButton
      Caption = '&Arrange'
      Category = 4
      Enabled = False
      Visible = ivAlways
    end
    object dxbbMinimizeAll: TdxBarButton
      Caption = '&Minimize All'
      Category = 4
      Enabled = False
      Hint = 'Minimize All'
      Visible = ivAlways
      ImageIndex = 18
    end
    object dxBarListWindows: TdxBarListItem
      Caption = 'List of Open Windows'
      Category = 4
      Visible = ivAlways
      OnClick = dxBarListWindowsClick
      OnGetData = dxBarListWindowsGetData
    end
    object dxbrsiFile: TdxBarSubItem
      Caption = '&Bestand'
      Category = 5
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxbrsiData: TdxBarSubItem
      Caption = '&Record'
      Category = 5
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbEdit'
        end
        item
          Visible = True
          ItemName = 'dxbbView'
        end
        item
          Visible = True
          ItemName = 'dxbbAdd'
        end
        item
          Visible = True
          ItemName = 'dxbbDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbRefreshData'
        end
        item
          Visible = True
          ItemName = 'dxbbCustomiseColumns'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbPrintList'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbExportXLS'
        end
        item
          Visible = True
          ItemName = 'dxbbExportHTML'
        end
        item
          Visible = True
          ItemName = 'dxbbExportXML'
        end>
    end
    object dxbrsiGeneral: TdxBarSubItem
      Caption = '&Hoofdsystemen'
      Category = 5
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxbrsiWindow: TdxBarSubItem
      Caption = '&Window'
      Category = 5
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbWindowClose'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbTileHorizontally'
        end
        item
          Visible = True
          ItemName = 'dxbbTileVertically'
        end
        item
          Visible = True
          ItemName = 'dxbbCascade'
        end
        item
          Visible = True
          ItemName = 'dxbbArrangeAll'
        end
        item
          Visible = True
          ItemName = 'dxbbMinimizeAll'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarListWindows'
        end>
    end
    object Maintenance: TdxBarSubItem
      Caption = 'Beheerschermen'
      Category = 5
      Visible = ivAlways
      ItemLinks = <>
    end
  end
end
