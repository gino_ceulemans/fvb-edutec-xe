{*****************************************************************************
  This unit contains the Base RecordView used in the Edutec Project.
  
  @Name       Form_EduRecordView
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  19/10/2005   slesage              If the DoPost succeeds it will now also
                                    call the UpdateFormCaption method.
  08/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_FVBFFCBaseRecordView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, DB, Unit_FVBFFCRecordView,
  dxNavBarCollns, dxNavBarBase, dxNavBar, Form_FVBFFCMainClient, 
  cxButtons, ExtCtrls, StdCtrls, Buttons,
  Unit_PPWFrameWorkInterfaces, Data_FVBFFCMainClient, 
  dxBar, Unit_PPWFrameWorkClasses, cxEdit, ActnList,
  Unit_FVBFFCComponents, Unit_FVBFFCDBComponents,
  Unit_FVBFFCDevExpress, Unit_FVBFFCFoldablePanel, cxControls, cxSplitter,
  Menus, cxGraphics, cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, System.Actions, cxClasses;

const
  sltView = 0;
  sltSelect = 1;
  sltAdd = 2;

type
  TFVBFFCBaseRecordView = class(TFVBFFCRecordView)
    bbtnBtnESCPressed: TBitBtn;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    cxbtnCancel: TFVBFFCButton;
    cxbtnApply: TFVBFFCButton;
    pnlTop: TFVBFFCPanel;
    pnlRecord: TFVBFFCPanel;
    sbxMain: TScrollBox;
    pnlRecordHeader: TFVBFFCPanel;
    pnlRecordHeaderTopSpacer: TFVBFFCPanel;
    pnlRecordFixedData: TFVBFFCPanel;
    pnlRecordDetailSeperator: TFVBFFCPanel;
    pnlRecordDetailTitle: TFVBFFCPanel;
    pnlEDUPanel1: TFVBFFCPanel;
    pnlRightSpacer: TFVBFFCPanel;
    dxnbNavBar: TFVBFFCNavBar;
    dxnbgGeneral: TdxNavBarGroup;
    dxnbgRelatedInfo: TdxNavBarGroup;
    dxnbiRecordDetail: TdxNavBarItem;
    srcMain: TFVBFFCDataSource;
    alRecordView: TFVBFFCActionList;
    pnlRecordDetail: TFVBFFCPanel;
    acShowRecordDetail: TAction;
    FVBFFCSplitter1: TFVBFFCSplitter;
    procedure srcMainDataChange(Sender: TObject; Field: TField);
    procedure srcMainStateChange(Sender: TObject);
    procedure FVBFFCRecordViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure FVBFFCRecordViewCloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure FVBFFCRecordViewCreate(Sender: TObject);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure bbtnBtnESCPressedClick(Sender: TObject);
    procedure cxbtnOKClick(Sender: TObject);
    procedure cxbtnCancelClick(Sender: TObject);
    procedure FVBFFCRecordViewShow(Sender: TObject);
    procedure acShowRecordDetailExecute(Sender: TObject);
  private
    { Private declarations }
    FPostFailed        : Boolean;
    FWindowBarButton   : TdxBarButton;
  protected
    function GetMainForm       : TfrmFVBFFCMainClient; virtual;
    function GetMainDataModule : TdtmFVBFFCMainClient; virtual;

    procedure SetFormStyle( const Value : TFormStyle ); override;
    procedure SetMode( const Value : TPPWFrameWorkRecordViewMode ); override;
    procedure SetCaption( const Value : TCaption ); override;

    procedure DoCancel; virtual;
    procedure DoPost; virtual;
    procedure DoDelete; virtual;
    procedure DoModeChanged; Override;

    function GetSettingsSaved: Boolean; override;

    function GetLabelStyleController: TcxEditStyleController; override;
    function GetReadOnlyEditStyleController: TcxEditStyleController; override;
    function GetRequiredEditStyleController: TcxEditStyleController; override;

    procedure AddOrUpdateWindowMenuItem; override;
    procedure CreateFormButtonOnToolbar; override;
    procedure DestroyFormButtonOnToolbar; override;
    procedure DoOnWindowBarButtonClick( Sender : TObject ); virtual;


    procedure ShowDetailListView( Sender : TObject;
                                  aDataModuleClassName : String;
                                  aDockSite            : TWinControl ); override;
    procedure ShowRecordDetail( Sender : TObject ); override;
    procedure UpdateRecordDetailTitle( aTitle : String ); virtual;

    property MainForm : TfrmFVBFFCMainClient read GetMainForm;
    property MainDataModule : TdtmFVBFFCMainClient read GetMainDataModule;
  public
    { Public declarations }
    procedure UpdateFormCaption; override;
    procedure AddReadOnlyComponent( aComponent : TComponent; aHandled : Boolean = False ); override;
    procedure RemoveReadOnlyComponent( aComponent : TComponent; aHandled : Boolean = False ); override;
  end;

var
  FVBFFCBaseRecordView: TFVBFFCBaseRecordView;

ResourceString
  rsOK     = 'OK';
  rsDelete = 'Delete';

implementation

uses
  DBClient, Registry, StrUtils,
  Unit_PPWFrameWorkForm, Unit_PPWFrameWorkUtils, Unit_FVBFFCInterfaces;

{$R *.dfm}

{ TFVBFFCRecordView }

{*****************************************************************************
  This method will be used to create a button on the Main Forms toolbar which
  can be used to show this form.

  @Name       TFVBFFCRecordView.CreateFormButtonOnToolbar
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.CreateFormButtonOnToolbar;
var
  aOpenWindowsBar : TdxBar;
  aWindowItemLink : TdxBarItemLink;
begin
  FWindowBarButton := TdxBarButton.Create( MainForm.dxBarManager1 );
  FWindowBarButton.Caption := Self.Caption;
  FWindowBarButton.OnClick := Self.DoOnWindowBarButtonClick;

  aOpenWindowsBar := MainForm.dxBarManager1.BarByOldName( 'OpenWindows' );

  aOpenWindowsBar.LockUpdate := True;
  try
    aWindowItemLink := aOpenWindowsBar.ItemLinks.Add;
    aWindowItemLink.BeginGroup := True;
    aWindowItemLink.Item := FWindowBarButton;
  finally
    aOpenWindowsBar.LockUpdate := False;
  end;
end;

{*****************************************************************************
  This method will be used to destroy the Button on the Toolbar if it was
  previously created.

  @Name       TFVBFFCRecordView.DestroyFormButtonOnToolbar
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.DestroyFormButtonOnToolbar;
var
  aWindowItemIndex : Integer;
begin
  if ( Assigned( MainForm ) ) and
     ( Assigned( MainForm.dxBarListWindows ) ) then
  begin
    aWindowItemIndex := MainForm.dxBarListWindows.Items.IndexOfObject( Self );

    if ( aWindowItemIndex <> -1 ) then
    begin
      MainForm.dxBarListWindows.Items.Delete( aWindowItemIndex );
    end;
  end;

  if ( Assigned( FWindowBarButton ) ) then
  begin
    FWindowBarButton.OnClick := Nil;
  end;
  FreeAndNil( FWindowBarButton );
end;

{*****************************************************************************
  This method will be used to cancel the changes in the Record.

  @Name       TFVBFFCRecordView.DoCancel
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.DoCancel;
begin
  if ( DataSource.State in dsEditModes ) then
  begin
    TClientDataset(Datasource.Dataset).CancelUpdates;
  end;
  FPostFailed := False;
end;

{*****************************************************************************
  This method will be used to delete the record in the RecordView.

  @Name       TFVBFFCRecordView.DoDelete
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.DoDelete;
begin
  Try
    Datasource.dataset.tag := 1;
    DataSource.DataSet.Delete;
    FPostFailed := False;
  except
    FPostFailed := True;
    Raise;
  end;
end;

{*****************************************************************************
  This method will be exuecuted when the Mode of the RecordView changes.

  @Name       TFVBFFCRecordView.DoModeChanged
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.DoModeChanged;
begin
  inherited DoModeChanged;

  srcMainStateChange( Self );
end;

{*****************************************************************************
  This method will be executed when the user clicks the Forms toolbutton.

  @Name       TFVBFFCRecordView.DoOnWindowBarButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.DoOnWindowBarButtonClick(Sender: TObject);
begin
  if ( WindowState = wsMinimized ) then
  begin
    WindowState := wsNormal;
  end;
  if not Visible then
  begin
    Show;
  end
  else
  begin
    BringToFront;
  end;
end;

{*****************************************************************************
  This method will be executed when the changes made in the RecordView should
  be posted to the DB.

  @Name       TFVBFFCRecordView.DoPost
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.DoPost;
begin
  { If the Contents of the DataSet has been modified, we should try to post
    the changes in the DataBase }
  if ( DataSource.DataSet.State in dsEditModes ) then
  begin
    Try
      DataSource.DataSet.Tag := 1;
      DataSource.DataSet.Post;

      { If the form was in Add mode, and the changes were posted, we should
        now put the form into Edit mode. }
      if ( Mode = rvmAdd ) {or ( Mode = rvmEdit )} then
      begin
        SetMode( rvmEdit );
      end;

      FPostFailed := False;
      UpdateFormCaption;
    Except
      FPostFailed := True;
      Raise;
    End;
  end;
end;

{*****************************************************************************
  Property Getter for the ReadOnlyEditStyleController property.

  @Name       TFVBFFCRecordView.GetReadOnlyEditStyleController
  @author     slesage
  @param      None
  @return     Returns the EditStyleController that should be used for Fields /
              Components that are set to ReadOnly.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCBaseRecordView.GetReadOnlyEditStyleController: TcxEditStyleController;
begin
  Result := MainDataModule.cxescReadOnly;
end;

{*****************************************************************************
  Property Getter for the RequiredEditStyleController property.

  @Name       TFVBFFCRecordView.GetRequiredEditStyleController
  @author     slesage
  @param      None
  @return     Returns the EditStyleController that should be used for Fields /
              Components that are set to Required.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCBaseRecordView.GetRequiredEditStyleController: TcxEditStyleController;
begin
  Result := MainDataModule.cxescRequired;
end;

{*****************************************************************************
  This funciton is used to check if the settings formt his form have already
  been saved.

  @Name       TFVBFFCRecordView.GetSettingsSaved
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCBaseRecordView.GetSettingsSaved: Boolean;
var
  reg: TRegistry;
begin
  reg := TRegistry.Create;
  try
      result := reg.OpenKey(RegistryKey + '\' + classname, false);
  finally
    reg.free;
  end;
end;

{*****************************************************************************
  Overridden SetCaption method in which we will also adjust the Caption of
  this forms Menu Item and ToolBar in the Main form.

  @Name       TFVBFFCRecordView.SetCaption
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.SetCaption(const Value: TCaption);
begin
  inherited SetCaption( Value );
  
  if not ( csLoading in ComponentState ) and
     not ( fsCreating in FormState ) and
         ( Assigned( FWindowBarButton ) ) then
  begin
    FWindowBarButton.Caption := GetDetailName;
    AddOrUpdateWindowMenuItem;
  end;
end;

{*****************************************************************************


  @Name       TFVBFFCRecordView.SetFormStyle
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.SetFormStyle(const Value: TFormStyle);
var
  aConstraints : TSizeConstraints;
begin
  aConstraints := Self.Constraints;

  inherited;

  Self.Constraints.Assign( aConstraints );
end;

{*****************************************************************************
  Property Setter for the ModeProperty, in which we will modify the caption
  of the OK button depending on the mode of the RecordView.

  @Name       TFVBFFCRecordView.SetMode
  @author     slesage
  @param      Value   New value for the Mode property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.SetMode(const Value: TPPWFrameWorkRecordViewMode);
begin
  inherited SetMode( Value );

  if ( Mode = rvmDelete ) then
  begin
    cxbtnOK.Caption := rsDelete;
    cxbtnOK.Enabled := True;
    cxbtnApply.Enabled := False;
  end
  else
  begin
    if ( Mode = rvmAdd ) then
    begin
      DataSource.DataSet.Append;
    end;
    cxbtnOK.Caption := rsOK;
  end;
end;

{*****************************************************************************


  @Name       TFVBFFCRecordView.ShowDetailListView
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.ShowDetailListView(Sender: TObject;
  aDataModuleClassName: String; aDockSite: TWinControl);
var
  aCursor : IPPWRestorer;
begin
  aCursor := TPPWCursorRestorer.Create( crSQLWait );

  Inherited ShowDetailListView( Sender, aDataModuleClassName, aDockSite );


  if ( Assigned( Sender ) ) then
  begin
    if ( Sender is TAction ) then
    begin
      UpdateRecordDetailTitle( TAction( Sender ).Caption );
    end;
    if ( Sender is TdxNavBarItem ) then
    begin
      UpdateRecordDetailTitle( TdxNavBarItem( Sender ).Caption );
    end;
  end;
end;

procedure TFVBFFCBaseRecordView.ShowRecordDetail(Sender: TObject);
begin
  Inherited ShowRecordDetail( Sender );


  if ( Assigned( Sender ) ) then
  begin
    if ( Sender is TAction ) then
    begin
      UpdateRecordDetailTitle( TAction( Sender ).Caption );
    end;
    if ( Sender is TdxNavBarItem ) then
    begin
      UpdateRecordDetailTitle( TdxNavBarItem( Sender ).Caption );
    end;
  end;
end;

{*****************************************************************************


  @Name       TFVBFFCRecordView.UpdateFormCaption
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.UpdateFormCaption;
begin
  Inherited UpdateFormCaption;
  PnlRecordHeader.Caption := Caption;
end;

procedure TFVBFFCBaseRecordView.srcMainDataChange(Sender: TObject; Field: TField);
begin
  if ( Assigned( Field ) ) then
  begin
    UpdateFormCaption;
  end;
end;

procedure TFVBFFCBaseRecordView.srcMainStateChange(Sender: TObject);
begin
  cxbtnApply.Enabled := ( Mode <> rvmDelete ) and
                       ( DataSource.State in [ dsEdit, dsInsert ] );
end;

{*****************************************************************************
  This method will be used to update the RecordDetail Title.

  @Name       TFVBFFCRecordView.UpdateRecordDetailTitle
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.UpdateRecordDetailTitle(aTitle: String);
begin
  inherited;
  pnlRecordDetailTitle.Caption := aTitle;
end;

procedure TFVBFFCBaseRecordView.FVBFFCRecordViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DoCancel;

  DestroyFormButtonOnToolbar;
  Action := caFree;
end;

procedure TFVBFFCBaseRecordView.FVBFFCRecordViewCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  BringToFront;

  CanClose := not ( FPostFailed );

  FPostFailed := False;
end;

procedure TFVBFFCBaseRecordView.FVBFFCRecordViewCreate(Sender: TObject);
begin
  CreateFormButtonOnToolbar;
end;

procedure TFVBFFCBaseRecordView.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  DataModule: IFVBFFCBaseDataModule;
begin
  UpdateFormCaption;
  AddOrUpdateWindowMenuItem;
  if aFrameWorkDataModule.QueryInterface(IFVBFFCBaseDataModule, DataModule) = 0 then
    DataModule.IsRecordViewDataModule := True;
end;

procedure TFVBFFCBaseRecordView.AddOrUpdateWindowMenuItem;
var
  aFormIndex : Integer;
begin
  inherited AddOrUpdateWindowMenuItem;

  if     ( Assigned( FrameWorkDataModule ) ) and
         ( Assigned( FrameWorkDataModule.ListViewDataModule ) ) and
     not ( Assigned( FrameWorkDataModule.ListViewDataModule.MasterDataModule ) ) then
  begin
    aFormIndex := MainForm.dxBarListWindows.Items.IndexOfObject( Self );
    if ( aFormIndex = -1 ) then
    begin
      MainForm.dxBarListWindows.Items.AddObject(Caption, Self);
    end
    else
    begin
      MainForm.dxBarListWindows.Items.Strings[ aFormIndex ] := Caption;
    end
  end;
end;

procedure TFVBFFCBaseRecordView.bbtnBtnESCPressedClick(Sender: TObject);
begin
  Close;
end;

{*****************************************************************************
  This method will be executed when the user clicks the OK or Apply button.

  @Name       TFVBFFCRecordView.cxbtnOKClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.cxbtnOKClick(Sender: TObject);
var
  aCursorRestorer : IPPWRestorer;
begin
  aCursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  inherited;

  Try

    { cheuten : Begin Bugfix Auto Enter }

    if not ( self.ActiveControl is TcxButton ) then
    begin
      if cxbtnOk.Visible then
      begin
        self.ActivateControl( cxbtnOk );
      end
      else if cxbtnApply.Visible then
      begin
        self.ActivateControl( cxbtnApply );
      end
      else if cxbtnCancel.Visible then
      begin
        self.ActivateControl( cxbtnCancel );
      end;
    end;

    { cheuten : Einde Bugfix auto Enter }

    if ( DataSource.State in dsEditModes ) and
       ( Mode in [ rvmAdd, rvmEdit ] ) then
    begin
      DoPost;
    end;

    if ( Mode in [ rvmDelete ] ) then
    begin
      DoDelete;
    end;


    if ( Sender is TBitBtn ) then
    begin
      ModalResult := TBitBtn( Sender ).ModalResult;
    end;

    if ( Sender = cxbtnOK ) and ( not ( fsModal in FormState ) ) then
    begin
      Close;
    end;
  except
    Raise;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks the Cancel Button.

  @Name       TFVBFFCRecordView.cxbtnCancelClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.cxbtnCancelClick(Sender: TObject);
begin
  FPostFailed := False;

  try
    if ( Not ( fsModal in FormState ) ) and
       ( TButton( Sender ).ModalResult <> mrNone ) then
    begin
      Close; { TODO : CancelClick also calls the OnClose which does the same }
    end
    else
      DoCancel;
  finally
  end;
end;

procedure TFVBFFCBaseRecordView.FVBFFCRecordViewShow(Sender: TObject);
begin
  AddOrUpdateWindowMenuItem;
  if ( WindowState = wsNormal ) then
  begin
    Width := Constraints.MinWidth;
  end;
end;

procedure TFVBFFCBaseRecordView.acShowRecordDetailExecute(Sender: TObject);
begin
  ShowRecordDetail( Sender );
end;

function TFVBFFCBaseRecordView.GetMainForm: TfrmFVBFFCMainClient;
begin
  Result := frmFVBFFCMainClient;
end;

function TFVBFFCBaseRecordView.GetMainDataModule: TdtmFVBFFCMainClient;
begin
  Result := dtmFVBFFCMainClient;
end;

{*****************************************************************************
  Overridden AddReadOnly mehtod in which we will also change the
  EditRepositoty for the aComponent if necessary.

  @Name       TFVBFFCBaseRecordView.AddReadOnlyComponent
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.AddReadOnlyComponent(
  aComponent: TComponent; aHandled: Boolean);
var
  aRIName : String;
  aRI     : TcxEditRepositoryItem;
begin
  inherited;

  if ( Assigned( aComponent ) ) and
     ( aComponent is TcxCustomEdit ) then
  begin
    if Assigned( TcxCustomEdit( aComponent ).RepositoryItem ) then
    begin
      aRIName := TcxCustomEdit( aComponent ).RepositoryItem.Name + 'ReadOnly';
      aRI := MainDataModule.cserMain.ItemByName( aRIName );
      if ( Assigned( aRI ) ) then
      begin
        TcxCustomEdit( aComponent ).RepositoryItem := aRI;
      end;
    end;
  end;
end;

{*****************************************************************************
  Overridden AddReadOnly mehtod in which we will also change the
  EditRepositoty for the aComponent if necessary.

  @Name       TFVBFFCBaseRecordView.RemoveReadOnlyComponent
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseRecordView.RemoveReadOnlyComponent(
  aComponent: TComponent; aHandled: Boolean);
var
  aRIName : String;
  aRI     : TcxEditRepositoryItem;
begin
  inherited;

  if ( Assigned( aComponent ) ) and
     ( aComponent is TcxCustomEdit ) then
  begin
    if ( Assigned( TcxCustomEdit( aComponent ).RepositoryItem ) ) and
       ( RightStr( TcxCustomEdit( aComponent ).RepositoryItem.Name, 8 ) = 'ReadOnly' ) then
    begin
      aRIName := TcxCustomEdit( aComponent ).RepositoryItem.Name;
      aRIName := LeftStr( aRIName, Length( aRIName ) - 8 );
      aRI := MainDataModule.cserMain.ItemByName( aRIName );
      if ( Assigned( aRI ) ) then
      begin
        TcxCustomEdit( aComponent ).RepositoryItem := aRI;
      end;
    end;
  end;

  inherited;
end;

function TFVBFFCBaseRecordView.GetLabelStyleController: TcxEditStyleController;
begin
  Result := MainDataModule.cxescSearchCriteria;
end;

end.
