{*****************************************************************************
  This unit contains the Base Edutec ListView with all its components.
  
  @Name       Form_EduListView
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  12/07/2006   tclaesen             Added overloaded procedure 
                                    LoopThroughSelectedRecords were you can
                                    deliver params.
  14/07/2005   sLesage              Added code which will link some components
                                    to the correct Actions.
  15/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_FVBFFCBaseListView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, Unit_FVBFFCListView,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxLookAndFeelPainters, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, StdCtrls, cxButtons,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, Unit_PPWFrameWorkInterfaces, DBClient, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, Data_FVBFFCMainClient,
  Form_FVBFFCMainClient, Menus, unit_FVBFFCInterfaces, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter,
  dxCore, PPW_Components_Common_CollapsablePanel;

type
  TFVBFFCBaseListView = class(TFVBFFCListView)
    pnlSelection: TPanel;
    pnlList: TFVBFFCPanel;
    cxgrdList: TcxGrid;
    cxgrdtblvList: TcxGridDBTableView;
    cxgrdlvlList: TcxGridLevel;
    pnlFormTitle: TFVBFFCPanel;
    pnlSearchCriteriaSpacer: TFVBFFCPanel;
    pnlListTopSpacer: TFVBFFCPanel;
    pnlListBottomSpacer: TFVBFFCPanel;
    pnlListRightSpacer: TFVBFFCPanel;
    pnlListLeftSpacer: TFVBFFCPanel;
    srcMain: TFVBFFCDataSource;
    dxcpGridPrinter: TFVBFFCComponentPrinter;
    dxgrptlGridLink: TdxGridReportLink;
    dxbmPopupMenu: TdxBarManager;
    dxbbEdit: TdxBarButton;
    dxbbView: TdxBarButton;
    dxbbAdd: TdxBarButton;
    dxbbDelete: TdxBarButton;
    dxbbRefresh: TdxBarButton;
    dxBBCustomizeColumns: TdxBarButton;
    dxbbPrintGrid: TdxBarButton;
    dxbbExportXLS: TdxBarButton;
    dxbbExportXML: TdxBarButton;
    dxbbExportHTML: TdxBarButton;
    dxbpmnGrid: TdxBarPopupMenu;
    srcSearchCriteria: TFVBFFCDataSource;
    cxbtnEDUButton1: TFVBFFCButton;
    cxbtnEDUButton2: TFVBFFCButton;
    pnlSearchCriteria: TPPWCollapsablePanel;
    pnlSearchCriteriaButtons: TPanel;
    cxbtnApplyFilter: TFVBFFCButton;
    cxbtnClearFilter: TFVBFFCButton;
    procedure FVBFFCListViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure FVBFFCListViewCloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure FVBFFCListViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure FVBFFCListViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxgrdtblvListKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxgrdtblvListDblClick(Sender: TObject);
    procedure cxbtnApplyFilterClick(Sender: TObject);
    procedure cxgrdtblvListCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FVBFFCListViewShow(Sender: TObject);
    procedure cxgrdtblvListDataControllerSortingChanged(Sender: TObject);
    procedure cxgrdtblvListCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    FWindowBarButton   : TdxBarButton;
    FCallBack: TLoopThroughCallBack;
    procedure LoopThroughSelectedRecordsCallBack (Params: array of TObject);
  protected
    FSearchCriteriaDataSet : TClientDataSet;

    { IWKBListView Property Getters }
    function GetFilterString     : String; override;
    function GetOrderByString    : String; override;

    function GetGrid             : TcxGrid; override;
    function GetComponentPrinter : TdxComponentPrinter; override;

    { TPPWFrameWorkListView Property Getters }
    procedure SetAsSelection(const Value: Boolean); override;
    procedure SetAsDetail( const Value : Boolean ); override;
    procedure SetCaption( const Value : TCaption); override;

    function GetMainDataModule : TdtmFVBFFCMainClient; virtual;
    function GetMainForm       : TfrmFVBFFCMainClient; virtual;

    procedure LinkComponentsToActions; virtual;


    { Other Methods }
    Procedure AddFilterCondition( var aFilterString : string; const aCondition : String; aAndOr : String = 'AND' ); virtual;

    Procedure AddLikeFilterCondition( var aFilterString : String;
                                      const aFieldName, aFieldCondition : String;
                                      aAndOr : String = 'AND' ); virtual;
    Procedure AddStringEqualFilterCondition( var aFilterString : String;
                                      const aFieldName, aFieldCondition : String;
                                      aAndOr : String = 'AND' ); virtual;
    Procedure AddIntEqualCondition( var aFilterString : String;
                                    const aFieldName  : String;
                                    const aFieldValue : Integer;
                                      aAndOr : String = 'AND' ); virtual;
    Procedure AddIntAtLeastCondition( var aFilterString : String;
                                      const aFieldName  : String;
                                      const aFieldValue : Integer;
                                      aAndOr : String = 'AND' ); virtual;
    Procedure AddIntAtMostCondition( var aFilterString : String;
                                     const aFieldName  : String;
                                     const aFieldValue : Integer;
                                      aAndOr : String = 'AND' ); virtual;
    Procedure AddIntBetweenCondition( var aFilterString : String;
                                      const aFieldName  : String;
                                      const aFieldValue1, aFieldValue2 : Integer;
                                      aAndOr : String = 'AND' ); virtual;
    Procedure AddDateBetweenCondition( var aFilterString : String;
                                       const aFieldName : String;
                                      const aFieldValue1, aFieldValue2 : TDateTime;
                                      aAndOr : String = 'AND' ); virtual;
    procedure AddBlnEqualCondition(var aFilterString: String;
                                      const aFieldName: String;
                                      const aFieldValue: Boolean;
                                      aAndOr: String = 'AND'); virtual;

    procedure CMChildKey(var Message: TCMChildKey); message CM_CHILDKEY;

    procedure CreateFormButtonOnToolbar; override;
    procedure ClearFilterCriteria; override;
    procedure DestroyFormButtonOnToolbar; override;
    procedure DoOnWindowBarButtonClick( Sender : TObject ); virtual;

    procedure StoreActiveViewToIni; override;
    procedure RestoreActiveViewFromIni; override;

    property MainDataModule : TdtmFVBFFCMainClient read GetMainDataModule;
    property MainForm       : TfrmFVBFFCMainClient read GetMainForm;
  public
    procedure LoopThroughSelectedRecords (callBack: TLoopThroughCallBack); overload; virtual;
    procedure LoopThroughSelectedRecords (callBack: TLoopThroughCallBackWithParams;
                                          Params: array of TObject); overload; virtual;
    destructor Destroy; override;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Unit_FVBFFCUtils, 
  Data_FVBFFCBaseDataModule,
  Unit_PPWFrameWorkListView, cxContainer, cxGridDBDataDefinitions;

{$R *.dfm}

{*****************************************************************************
  This method will be executed when the form is closed, and will be used to
  indicate what action should be taken ( Free, Minimize or Hide ).

  @Name       TFVBFFCListView.EdutecListViewClose
  @author     slesage
  @param      Sender   The Object from which the Method is invoked.
  @param      Action   Indicates the action that should be taken.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.FVBFFCListViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DestroyFormButtonOnToolbar;
  //StoreActiveViewToIni;
  Action := caFree;
end;

{*****************************************************************************
  This method will be executed before the Form is actually closed and will be
  used to check if closing the form is allowed.

  @Name       TFVBFFCListView.EdutecListViewCloseQuery
  @author     slesage
  @param      Sender     The Object from which the method is invoked<
  @param      CanClose   Determines whether a form is allowed to close.
                         Its default value is true.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.FVBFFCListViewCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := True;
end;

{*****************************************************************************
  This method will be executed when the form gets initialised.

  @Name       TFVBFFCListView.EdutecListViewInitialise
  @author     slesage
  @param      aDataModule   The FrameWorkDataModule that is linked to this
                            Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.FVBFFCListViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  aEduBaseDataModule : IFVBFFCBaseDataModule;
begin
  LinkComponentsToActions;
  RestoreActiveViewFromIni;
  CreateFormButtonOnToolbar;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Supports( aFrameWorkDataModule, IFVBFFCBaseDataModule, aEduBaseDataModule ) ) then
  begin
    FSearchCriteriaDataSet := aEduBaseDataModule.SearchCriteriaDataSet;;
    srcSearchCriteria.DataSet := FSearchCriteriaDataSet;
  end;
end;

{*****************************************************************************
  This method will be executed when a key is pressed on the form.

  @Name       TFVBFFCListView.EdutecListViewKeyDown
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.FVBFFCListViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if ( Key = VK_ESCAPE ) then
  begin
    if ( not AsSelection ) then
    begin
      Close;
    end
    else
    begin
      ModalResult := mrCancel;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when a key is pressed while the Grid has the
  focus.

  @Name       TFVBFFCListView.cxgrdtblvListKeyDown
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.cxgrdtblvListKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ( Key = VK_RETURN ) then
  begin
    if ( AsSelection ) {and
       ( DataSource.DataSet.RecordCount > 0 ) and
       ( dxdbgMain.SelectedCount <> 0 )} then
    begin
      ModalResult := mrOK;
    end
    else
    begin
      if ( not AsSelection ) and
         ( DataSource.DataSet.RecordCount > 0 ) then
      begin
        if ( MainDataModule.acListViewEdit.Enabled ) then
        begin
          MainDataModule.acListViewEdit.Execute;
        end
        else if ( MainDataModule.acListViewConsult.Enabled ) then
        begin
          MainDataModule.acListViewConsult.Execute;
        end;
      end;
    end;
  end;
end;

procedure TFVBFFCBaseListView.cxgrdtblvListDblClick(Sender: TObject);
begin
end;

{*****************************************************************************
  This method will be used to restore the layout of the List from an Ini File.

  @Name       TFVBFFCListView.RestoreActiveViewFromIni
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.RestoreActiveViewFromIni;
var
  aStorageName : String;
begin
  {$IFDEF CODESITE}
  csFVBFFCListView.EnterMethod( Self, 'RestoreActiveViewFromIni' );
  {$ENDIF}

  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  cxgrdList.ActiveView.RestoreFromIniFile( LocalSettingsIniFileName, True, False, [], aStorageName );
  
  {$IFDEF CODESITE}
  csFVBFFCListView.ExitMethod( Self, 'RestoreActiveViewFromIni' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to store the layout of the List to an Ini File.

  @Name       TFVBFFCListView.StoreActiveViewToIni
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.StoreActiveViewToIni;
var
  aStorageName : String;
begin
  {$IFDEF CODESITE}
  csFVBFFCListView.EnterMethod( Self, 'StoreActiveViewToIni' );
  {$ENDIF}

  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  cxgrdList.ActiveView.StoreToIniFile( LocalSettingsIniFileName, False, [], aStorageName );
  
  {$IFDEF CODESITE}
  csFVBFFCListView.ExitMethod( Self, 'StoreActiveViewToIni' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be executed when the user clicks the Filter button and will
  execute the ApplyFilter method.

  @Name       TFVBFFCListView.cxbtnApplyFilterClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.cxbtnApplyFilterClick(Sender: TObject);
begin
  Self.ApplyFilter;
end;

{*****************************************************************************
  This method will be used to add a Condition to a FilterString.

  @Name       TFVBFFCListView.AddFilterCondition
  @author     slesage
  @param      aFilterString   The String containing the Filter.
  @param      aCondition      The Filter Condition that should be added to the
                              FilterString.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddFilterCondition(var aFilterString: string;
  const aCondition: String; aAndOr : String = 'AND' );
begin
  if ( aFilterString <> '' ) then
  begin
    aFilterString := aFilterString + ' ' + aAndOr + ' ';
  end;
  aFilterString := aFilterString + '( ' + aCondition + ' )';
end;

{*****************************************************************************
  This method will be used to add an 'Field >= x' Filter Condition to a
  FitlerString

  @Name       TFVBFFCListView.AddIntAtLeastCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddIntAtLeastCondition(var aFilterString: String;
  const aFieldName: String; const aFieldValue: Integer; aAndOr : String = 'AND');
const
  aWhere : String = '%s >= %d';
begin
  if ( aFieldName <> '' ) then
  begin
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, aFieldValue ] ), aAndOr );
  end;
end;

{*****************************************************************************
  This method will be used to add an 'Field <= x' Filter Condition to a
  FitlerString

  @Name       TFVBFFCListView.AddIntAtMostCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddIntAtMostCondition(var aFilterString: String;
  const aFieldName: String; const aFieldValue: Integer; aAndOr : String = 'AND');
const
  aWhere : String = '%s <= %d';
begin
  if ( aFieldName <> '' ) then
  begin
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, aFieldValue ] ), aAndOr );
  end;
end;

{*****************************************************************************
  This method will be used to add an 'Field >= x AND Field <= Y' Filter
  Condition to a FitlerString

  @Name       TFVBFFCListView.AddIntBetweenCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddIntBetweenCondition(var aFilterString: String;
  const aFieldName: String; const aFieldValue1, aFieldValue2: Integer; aAndOr : String = 'AND' );
const
  aWhere : String = '%s >= %d and %s <= %d';
var
  aLowValue, aHighValue : Integer;
begin
  if ( aFieldName <> '' ) then
  begin
    if ( aFieldValue1 > aFieldValue2 ) then
    begin
      aLowValue := aFieldValue2;
      aHighValue := aFieldValue1;
    end
    else
    begin
      aLowValue := aFieldValue1;
      aHighValue := aFieldValue2;
    end;
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, aLowValue,
                                                         aFieldName, aHighValue  ] ), aAndOr );
  end;
end;

{*****************************************************************************
  This method will be used to add an 'Field = x ' Filter Condition to a
  FitlerString

  @Name       TFVBFFCListView.AddIntEqualCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddIntEqualCondition(var aFilterString: String;
  const aFieldName: String; const aFieldValue: Integer; aAndOr : String = 'AND');
const
  aWhere : String = '%s = %d';
begin
  if ( aFieldName <> '' ) then
  begin
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, aFieldValue ] ), aAndOr );
  end;
end;


{*****************************************************************************
  This method will be used to add an 'Field = True/False ' Filter Condition to a
  FilterString

  @Name       TFVBFFCListView.AddBlnEqualCondition
  @author     cheuten
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddBlnEqualCondition(var aFilterString: String;
  const aFieldName: String; const aFieldValue: Boolean; aAndOr : String = 'AND');
begin
  if ( aFieldName <> '' ) then
  begin
    if aFieldValue then
      AddFilterCondition( aFilterString, aFieldName + ' = 1', aAndOr )
    else
      AddFilterCondition( aFilterString, aFieldName + ' = 0', aAndOr );
  end;
end;

{*****************************************************************************
  This method will be used to add an 'Field LIKE %x%' Filter
  Condition to a FitlerString

  @Name       TFVBFFCListView.AddLikeFilterCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddLikeFilterCondition(var aFilterString: String;
  const aFieldName, aFieldCondition: String; aAndOr : String = 'AND');
const
  aWhere : String = '%s LIKE %s';
begin
  if ( aFieldName <> '' ) and
     ( aFieldCondition <> '' ) then
  begin
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, '''%' + aFieldCondition + '%''' ] ), aAndOr );
  end;
end;

{*****************************************************************************
  This method will be used to add an 'Field = "ddddd" ' Filter
  Condition to a FitlerString

  @Name       TFVBFFCListView.AddStringEqualFilterCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue     The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddStringEqualFilterCondition(
  var aFilterString: String; const aFieldName, aFieldCondition: String; aAndOr : String = 'AND');
const
  aWhere : String = '%s = ''%s''';
begin
  if ( aFieldName <> '' ) and
     ( aFieldCondition <> '' ) then
  begin
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, aFieldCondition ] ), aAndOr );
  end;
end;

{*****************************************************************************
  This method will be used to clear the FilterCriteria.

  @Name       TFVBFFCListView.ClearFilterCriteria
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.ClearFilterCriteria;
var
  aEduDataModule : IFVBFFCBaseDataModule;
begin
  inherited ClearFilterCriteria;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IFVBFFCBaseDataModule, aEduDataModule ) ) then
  begin
    aEduDataModule.SearchCriteriaDataSet.Close;
    aEduDataModule.OpenSearchCriteriaDataSet;
  end;
end;

{*****************************************************************************
  This method will be used to create a button on the Main Forms toolbar which
  can be used to show this form.

  @Name       TFVBFFCListView.CreateFormButtonOnToolbar
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.CreateFormButtonOnToolbar;
begin
  if ( not Assigned( MasterRecordView ) ) then
  begin
    FWindowBarButton := TdxBarButton.Create( MainForm.dxBarManager1 );
    FWindowBarButton.Caption := Self.Caption;
    FWindowBarButton.OnClick := Self.DoOnWindowBarButtonClick;

    with MainForm.dxBarManager1.BarByOldName( 'OpenWindows' ), ItemLinks do
    begin
      LockUpdate := True;
      with Add do
      begin
        BeginGroup := True;
        Item := FWindowBarButton;
      end;
      LockUpdate := False;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to destroy the Button on the Toolbar if it was
  previously created.

  @Name       TFVBFFCListView.DestroyFormButtonOnToolbar
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.DestroyFormButtonOnToolbar;
var
  aWindowItemIndex : Integer;
begin
  {$IFDEF CODESITE}
  csFVBFFCListView.EnterMethod( Self, 'DestroyFormButtonOnToolbar' );
  {$ENDIF}

  if ( not Assigned( MasterRecordView ) ) then
  begin
    if ( Assigned( MainForm ) ) and
       ( Assigned( MainForm.dxBarListWindows ) ) then
    begin
      aWindowItemIndex := MainForm.dxBarListWindows.Items.IndexOfObject( Self );

      if ( aWindowItemIndex <> -1 ) then
      begin
        MainForm.dxBarListWindows.Items.Delete( aWindowItemIndex );
      end;
    end;

    if ( Assigned( FWindowBarButton ) ) then
    begin
      FWindowBarButton.OnClick := Nil;
    end;
    FreeAndNil( FWindowBarButton );
  end;
  {$IFDEF CODESITE}
  csFVBFFCListView.ExitMethod( Self, 'DestroyFormButtonOnToolbar' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be executed when the user clicks the Form's Window Button
  in the Main Form's WindowBar.

  @Name       TFVBFFCListView.DoOnWindowBarButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.DoOnWindowBarButtonClick(Sender: TObject);
begin
  if ( WindowState = wsMinimized ) then
  begin
    WindowState := wsNormal;
  end;
  if not Visible then
  begin
    Show;
  end
  else
  begin
    BringToFront;
  end;
end;

{*****************************************************************************
  Property Getter for the ComponentPrinter property.

  @Name       TFVBFFCListView.GetComponentPrinter
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCBaseListView.GetComponentPrinter: TdxComponentPrinter;
begin
  Result := dxcpGridPrinter;
end;

{*****************************************************************************
  Property Getter for the Grid property.

  @Name       TFVBFFCListView.GetGrid
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCBaseListView.GetGrid: TcxGrid;
begin
  Result := cxgrdList;
end;

{*****************************************************************************
  Overridden property setter for the AsDetail Property.

  @Name       TFVBFFCListView.SetAsDetail
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.SetAsDetail(const Value: Boolean);
begin
  inherited SetAsDetail( Value );

  if ( AsDetail ) then
  begin
    Constraints.MinWidth  := 0;
    pnlFormTitle.Visible := False;
    pnlListTopSpacer.Visible := False;
    pnlListBottomSpacer.Visible := False;
    pnlListLeftSpacer.Visible := False;
    pnlListRightSpacer.Visible := False;
    pnlSearchCriteria.Visible := False;
    pnlSearchCriteriaSpacer.Visible := False;
    pnlSearchCriteriaButtons.Visible := False;
  end;
end;

procedure TFVBFFCBaseListView.SetAsSelection(const Value: Boolean);
begin
  if ( Value <> AsSelection ) then
  begin
    inherited SetAsSelection( Value );

    pnlSelection.Visible := Value;
  end;
end;

procedure TFVBFFCBaseListView.SetCaption(const Value: TCaption);
var
  aFormIndex : Integer;
begin
  inherited SetCaption( Value );

  pnlFormTitle.Caption := Caption;

  if not ( csLoading in ComponentState ) and
     not ( fsCreating in FormState ) and
     ( Assigned( FWindowBarButton ) ) then
  begin
    pnlFormTitle.Caption := Caption;
    FWindowBarButton.Caption := Caption;
    aFormIndex := MainForm.dxBarListWindows.Items.IndexOfObject( Self );
    if ( aFormIndex = -1 ) then
    begin
      MainForm.dxBarListWindows.Items.AddObject(Caption, Self);
    end
    else
    begin
      MainForm.dxBarListWindows.Items.Strings[ aFormIndex ] := Caption;
    end
  end;
end;

procedure TFVBFFCBaseListView.cxgrdtblvListCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  { Check if the form was shown for Selection Purposes }
  if ( AsSelection ) then
  begin
    { Return the correct Modal Result }
    if ( Assigned( DataSource ) ) and
       ( Assigned( DataSource.DataSet ) ) and
       ( Not ( ( DataSource.DataSet.BOF ) and ( DataSource.DataSet.EOF ) ) ) then
    begin
//      AHandled := True;
      ModalResult := mrOK;
    end
    else
    begin
//      AHandled := True;
      ModalResult := mrCancel;
    end;
  end
  else
  begin
    { Execute the Edit or View action }
//    if ( MainDataModule.acListViewEdit.Enabled ) then
//    begin
//      AHandled := True;
//      MainDataModule.acListViewEdit.Execute;
//    end
//    else if ( MainDataModule.acListViewConsult.Enabled ) then
//    begin
//      AHandled := True;
      MainDataModule.acListViewConsult.Execute;
//    end;
  end;
end;

procedure TFVBFFCBaseListView.FVBFFCListViewShow(Sender: TObject);
begin
  if not ( pnlSearchCriteria.Visible ) then
  begin
    cxgrdList.SetFocus;
  end;
//  if ( WindowState = wsNormal ) then
//  begin
//    Width := Constraints.MinWidth;
//  end;
end;

function TFVBFFCBaseListView.GetMainDataModule: TdtmFVBFFCMainClient;
begin
  Result := dtmFVBFFCMainClient;
end;

function TFVBFFCBaseListView.GetMainForm: TfrmFVBFFCMainClient;
begin
  Result := frmFVBFFCMainClient;
end;

{*****************************************************************************
  This method will be used to link some components to the correct actions.

  @Name       TFVBFFCBaseListView.LinkComponentsToActions
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.LinkComponentsToActions;
begin
  { Link the Popup Menu to the correct ImageLists }
  dxbmPopupMenu.Images      := MainDataModule.ilGlobalSmall;
  dxbmPopupMenu.LargeImages := MainDataModule.ilGlobalLarge;

  { Link the Popup Menu Items to the correct Action }
  dxbbAdd.Action              := MainDataModule.acListViewAdd;
  dxbbEdit.Action             := MainDataModule.acListViewEdit;
  dxbbDelete.Action           := MainDataModule.acListViewDelete;
  dxbbView.Action             := MainDataModule.acListViewConsult;
  dxbbRefresh.Action          := MainDataModule.acListViewRefresh;
  dxbbCustomizeColumns.Action := MainDataModule.acGridCustomiseColumns;
  dxbbPrintGrid.Action        := MainDataModule.acGridPrint;
  dxbbExportHTML.Action       := MainDataModule.acGridExportHTML;
  dxbbExportXLS.Action        := MainDataModule.acGridExportXLS;
  dxbbExportXML.Action        := MainDataModule.acGridExportXML;

  { Link the styles of the grid }
//  cxgrdtblvList.Styles.Background  := dtmCNVMainClient.cxsBackground;
//  cxgrdtblvList.Styles.Content     := dtmCNVMainClient.cxsContent;
//  cxgrdtblvList.Styles.ContentEven := dtmCNVMainClient.cxsContentEven;
//  cxgrdtblvList.Styles.ContentOdd  := dtmCNVMainClient.cxsContentOdd;
//  cxgrdtblvList.Styles.FilterBox   := dtmCNVMainClient.cxsFilterBox;
//  cxgrdtblvList.Styles.Footer      := dtmCNVMainClient.cxsFooter;
//  cxgrdtblvList.Styles.Group       := dtmCNVMainClient.cxsGroup;
//  cxgrdtblvList.Styles.GroupByBox  := dtmCNVMainClient.cxsGroupByBox;
//  cxgrdtblvList.Styles.Header      := dtmCNVMainClient.cxsHeader;
//  cxgrdtblvList.Styles.Inactive    := dtmCNVMainClient.cxsInactive;
//  cxgrdtblvList.Styles.IncSearch   := dtmCNVMainClient.cxsIncSearch;
//  cxgrdtblvList.Styles.Indicator   := dtmCNVMainClient.cxsIndicator;
//  cxgrdtblvList.Styles.Preview     := dtmCNVMainClient.cxsPreview;
//  cxgrdtblvList.Styles.Selection   := dtmCNVMainClient.cxsSelection;
  cxgrdtblvList.Styles.StyleSheet  := MainDataModule.GridTableViewStyleSheetDevExpress;

  { Link some buttons to the correct Actions }
  cxbtnClearFilter.Action     := MainDataModule.acListViewCancelFilter;
end;

{*****************************************************************************
  Borrowed from CMB project

  @Name       TFVBFFCBaseListView.GetFilterString
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
function TFVBFFCBaseListView.GetFilterString     : String;
begin
  Result := '';
end;

{*****************************************************************************
  Borrowed from CMB project

  @Name       TFVBFFCBaseListView.GetOrderByString
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
function TFVBFFCBaseListView.GetOrderByString: String;
var
  lcv : Integer;
begin
  Result := '';

  for lcv := 0 to Pred( cxgrdtblvList.SortedItemCount ) do
  begin
    if ( Result <> '' ) then
    begin
      Result := Result + ' , ';
    end;

    Result := Result + TcxGridDBColumn( cxgrdtblvList.SortedItems[ lcv ] ).DataBinding.FieldName;

    if ( cxgrdtblvList.SortedItems[ lcv ].SortOrder = soDescending ) then
    begin
      if ( TcxGridDBColumn( cxgrdtblvList.SortedItems[ lcv ] ).DataBinding.FieldName <> '' ) then
      begin
        Result := Result + ' DESC ';
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the User Changes the sorting.

  @Name       TFVBFFCBaseListView.cxgrdtblvListDataControllerSortingChanged
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.cxgrdtblvListDataControllerSortingChanged(
  Sender: TObject);
begin
  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    FrameWorkDataModule.DoSort( '' );
  end;
end;

{*****************************************************************************
  This method will be used to add an 'Field >= x AND Field <= Y' Filter
  Condition to a FitlerString

  @Name       TFVBFFCListView.AddDateBetweenCondition
  @author     slesage
  @param      aFilterString   The String containing the complete filter.
  @param      aFieldName      The Name of the field for which we want to add
                              a condition.
  @param      aFieldValue1    The Value which should be used in the condition.
  @param      aFieldValue2    The Value which should be used in the condition.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.AddDateBetweenCondition(
  var aFilterString: String; const aFieldName: String; const aFieldValue1,
  aFieldValue2: TDateTime; aAndOr: String);
const
  aWhere : String = '%s >= ''%s'' and %s <= ''%s''';
var
  aLowValue, aHighValue : TDateTime;
  aFmt : TFormatSettings;
begin
  aFmt.ShortDateFormat := 'yyyy-mm-dd';
  if ( aFieldName <> '' ) then
  begin
    if ( aFieldValue1 > aFieldValue2 ) then
    begin
      aLowValue := aFieldValue2;
      aHighValue := aFieldValue1;
    end
    else
    begin
      aLowValue := aFieldValue1;
      aHighValue := aFieldValue2;
    end;
    AddFilterCondition( aFilterString, Format( aWhere, [ aFieldName, DateToStr( aLowValue, aFmt ) ,
                                                         aFieldName, DateToStr( aHighValue, aFmt )  ] ), aAndOr );
  end;
end;

{*****************************************************************************
  This procedure will be triggered when a Key is pressed on one of the Child
  Components of the form.  In here we will check if the ENTER or ESCAPRE key
  was pressed on a Component which resides on our Search Panel.

  @Name       TFVBFFCBaseListView.CMChildKey
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseListView.CMChildKey(var Message: TCMChildKey);
var
  aInnerEdit : IcxContainerInnerControl;
  aControl : TWinControl;
begin
  if ( Supports( Message.Sender, IcxContainerInnerControl, aInnerEdit ) ) then
  begin
    aControl := aInnerEdit.ControlContainer;
  end
  else
  begin
    aControl := Message.Sender;
  end;

  if ( ControlIsChildOf( aControl, pnlSearchCriteria ) ) then
  begin
    case Message.CharCode of
      VK_RETURN : ApplyFilter;
    end;
  end;
end;

procedure TFVBFFCBaseListView.cxgrdtblvListCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  BGCol, FGCol : TcxGridDBColumn;
begin
  if AViewInfo.Selected then
    Exit;

  FGCol := (Sender as TcxGridDBTableView).GetColumnByFieldName('F_FOREGROUNDCOLOR');
  BGCol := (Sender as TcxGridDBTableView).GetColumnByFieldName('F_BACKGROUNDCOLOR');

  if Assigned(FGCol) and Assigned(BGCol) then
  begin
    if AViewInfo.GridRecord.Values[FGCol.Index] <> Null then
      ACanvas.Canvas.Font.Color := AViewInfo.GridRecord.Values[FGCol.Index];

    if AViewInfo.GridRecord.Values[BGCol.Index] <> Null then
      ACanvas.Canvas.Brush.Color := AViewInfo.GridRecord.Values[BGCol.Index];
  end;
end;

{*****************************************************************************
  Loops over all selected records and calls the callback procedure for each.
  It is part of the IFVBMultiSelectDeleteListView interface. Although the current
  class does implement this method, it does not implement the interface.
  Descendant classes might implement this interface and use this inherited method

  This method was initially in TfrmCNVBaseListView (Convenants only) but because
  we also needed it in the EduTec application it was moved to this super class.

  @Name       TfrmCNVBaseListView.LoopThroughSelectedRecords
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TFVBFFCBaseListView.LoopThroughSelectedRecords(callBack: TLoopThroughCallBack);
var Params: array of TObject;
begin
  FCallBack := callBack;
  LoopThroughSelectedRecords (LoopThroughSelectedRecordsCallBack, Params);
end;

procedure TFVBFFCBaseListView.LoopThroughSelectedRecordsCallBack (Params: array of TObject);
begin
  FCallBack();
end;

procedure TFVBFFCBaseListView.LoopThroughSelectedRecords(
  callBack: TLoopThroughCallBackWithParams; Params: array of TObject);
var
  lcv : Integer;
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
  bookmarks: TStringList;
  gridmode: Boolean;
begin
  assert (assigned (callback), 'Callback unassigned in LoopThroughSelectedRecords !');
  assert (assigned (srcMain.Dataset), 'srcMain.Dataset unassigned in LoopThroughSelectedRecords !');
{GCXE
  if ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
    aDataController := TcxGridDBDataController( ActiveGridView.DataController );

    gridmode := aDataController.DataModeController.GridMode;
    bookmarks := TStringList.Create;
    try
      // Check if there are selected records
      if ( aViewController. SelectedRecordCount <> 0 ) then
      begin
        // Loop over each selected record in the grid
        for lcv := 0 to Pred( aViewController.SelectedRecordCount ) do
        begin
          if gridmode then bookmarks.Add (aDataController.GetSelectedBookmark( lcv ))
          else bookmarks.Add (aDataController.Values[aViewController.SelectedRecords[lcv].RecordIndex, aDataController.GetItemByFieldName(aDataController.KeyFieldNames).Index]);
        end;

        // When we use a single loop for the bookmarks, even counting down it seems we might get a ListIndex of bounds while we are deleting rows
        for lcv := Pred (bookmarks.Count) downto 0 do
        begin
          if gridmode then
          begin
            if srcMain.DataSet.BookmarkValid(TBookmark(bookmarks[lcv])) then
            begin
              srcMain.DataSet.Bookmark := bookmarks[lcv];
              callback (Params);
            end;
          end
          else begin //no grid mode
            if srcMain.DataSet.Locate(aDataController.KeyFieldNames, bookmarks[lcv], []) then callback (Params);
          end;
        end;
      end;
    finally
        bookmarks.free;
    end;
  end;
  }
end;

destructor TFVBFFCBaseListView.Destroy;
begin
  StoreActiveViewToIni;
  inherited;
end;

end.

