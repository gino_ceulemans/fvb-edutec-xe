unit PPW_Components_Common_CollapsablePanel;

interface

uses Windows, Classes, ExtCtrls,
     Controls, Graphics, PPW_Common_Updating,
     Messages;

const
  cTitleHeight = 18;

type


  TPPWCollapsablePanel = class(TPanel)
  private
    FCollapsed: Boolean;
    FExpandedHeight: Integer;
    FUpdating : TPPWUpdating;
    FTitleHeight: Integer;
    FTitleColor: TColor;
    FTitleFontColor: TColor;
    FShowTitle: Boolean;
    FShowButton: Boolean;
    FOnCollapse: TNotifyEvent;
    FOnExpand: TNotifyEvent;
  private
    function StoreTitleFontColor: Boolean;
    function StoreTitleColor: Boolean;
    procedure SetShowButton(const Value: Boolean);
    procedure SetShowTitle(const Value: Boolean);
    procedure SetTitleFontColor(const Value: TColor);
    procedure SetTitleColor(const Value: TColor);
    procedure SetTitleHeight(const Value: Integer);
    procedure SetExpandedHeight(const Value: Integer);
    procedure SetCollapsed(const Value: Boolean);
    function GetExpanded: Boolean;
    procedure SetExpanded(const Value: Boolean);
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure Collapse;
    procedure Expand;
  private
    procedure PaintTitle(aCanvas : TCanvas; const aRct : TRect);
    procedure PaintPanel(aCanvas : TCanvas; const aRct : TRect);
  protected
    procedure Resize; override;
    procedure Loaded; override;
    procedure Paint; override;
    procedure AdjustClientRect(var Rect: TRect); override;
    procedure Click; override;
    procedure DoEnter; override;
    procedure DoExit; override;
  published
    property  Collapsed : Boolean       read  FCollapsed  write SetCollapsed default false;
    property  Expanded : Boolean        read  GetExpanded write SetExpanded stored false;
    property  OnExpand : TNotifyEvent   read  FOnExpand   write FOnExpand;
    property  OnCollapse : TNotifyEvent read  FOnCollapse write FOnCollapse;
  published
    property BevelOuter default bvNone;
    property ExpandedHeight : Integer   read  FExpandedHeight write SetExpandedHeight;
    property TitleHeight : Integer      read  FTitleHeight    write SetTitleHeight default cTitleHeight;
    property TitleColor : TColor        read  FTitleColor     write SetTitleColor stored StoreTitleColor;
    property TitleFontColor : TColor    read  FTitleFontColor write SetTitleFontColor stored StoreTitleFontColor;
    property ParentColor default True;
    property ParentFont default True;
    property ShowTitle : Boolean        read  FShowTitle      write SetShowTitle  default True;
    property ShowButton : Boolean       read  FShowButton     write SetShowButton default True;
  end;

implementation

uses PPW_Components_Common_CollapsablePanelIcons, Types, SysUtils;

{ TPPWCollapsablePanel }

procedure TPPWCollapsablePanel.AdjustClientRect(var Rect: TRect);
begin
  inherited;
  Inc(Rect.Top, TitleHeight);
end;

procedure TPPWCollapsablePanel.Click;
var
  P : TPoint;
begin
  if FShowButton and FShowTitle then begin
    P := ScreenToClient(Mouse.CursorPos);
    if (P.Y >= 0) and (P.Y <= TitleHeight) then begin
      Collapsed := not Collapsed;
    end;
    exit;
  end;
  inherited Click;
end;

procedure TPPWCollapsablePanel.Collapse;
begin
  if not FCollapsed then begin
    FUpdating.BeginUpdate;
    try
      FCollapsed := True;
      TabStop := false;
      Height := TitleHeight;
    finally
      FUpdating.EndUpdate;
    end;
    if assigned(FOnCollapse) then FOnCollapse(Self);
  end;
end;

constructor TPPWCollapsablePanel.Create(aOwner: TComponent);
begin
  inherited;
  FShowTitle := True;
  FShowButton := True;
  FCollapsed := False;
  FUpdating := TPPWUpdating.Create(Self);
  FTitleColor := clActiveCaption;
  FTitleFontColor := clWhite;      { clCaptionText }
  FTitleheight := cTitleHeight;
  FExpandedHeight := Height;
  BevelOuter := bvNone;
  ParentColor := True;
  ParentFont := True;
  DoubleBuffered := True;
  ControlStyle := ControlStyle + [csOpaque];
  ControlStyle := ControlStyle - [csParentBackground];
  Font.Name := 'Verdana';
end;

destructor TPPWCollapsablePanel.Destroy;
begin
  FUpdating.Free;
  inherited;
end;

procedure TPPWCollapsablePanel.DoEnter;
begin
  inherited;
end;

procedure TPPWCollapsablePanel.DoExit;
begin
  inherited;

end;

procedure TPPWCollapsablePanel.Expand;
begin
  if FCollapsed then begin
    FUpdating.BeginUpdate;
    try
      FCollapsed := False;
      if assigned(FOnExpand) then FOnExpand(Self);
      TabStop := True;
      Height := FExpandedHeight;
    finally
      FUpdating.EndUpdate;
    end;
  end;
end;

function TPPWCollapsablePanel.GetExpanded: Boolean;
begin
  Result := not Collapsed;
end;

procedure TPPWCollapsablePanel.Loaded;
begin
  inherited;
  if FCollapsed then begin
    FCollapsed := False;
    Collapse;
  end;
end;

procedure TPPWCollapsablePanel.PaintPanel(aCanvas: TCanvas; const aRct: TRect);
begin
  aCanvas.brush.Color := Color;
  aCanvas.brush.Style := bsSolid;
  if (csDesigning in ComponentState) then begin
    aCanvas.Pen.Color := clBlack;
    aCanvas.Pen.Style := psDash;
    aCanvas.Rectangle(aRct);
  end else begin
    aCanvas.FillRect(aRct);
  end;
end;

procedure TPPWCollapsablePanel.Paint;
var
  Rct : TRect;
  Cnv : TCanvas;
begin
  if FShowTitle then begin

    Rct := ClientRect;
    Cnv := Canvas;

    PaintPanel(Cnv, Rct);

    Rct.Top := Rct.Top + BorderWidth;
    Rct.Bottom := Rct.Top + TitleHeight;
    PaintTitle(Cnv, Rct);
  end else begin
    inherited Paint;
  end;
end;

procedure TPPWCollapsablePanel.PaintTitle(aCanvas: TCanvas; const aRct: TRect);
var
  R : TRect;
  Bmp : Tbitmap;
  X, Y : Integer;
begin
  aCanvas.brush.Color := TitleColor;
  aCanvas.brush.Style := bsSolid;
  aCanvas.FillRect(aRct);

  aCanvas.Font := Font;
  aCanvas.Font.Color := TitleFontColor;

  R := aRct;
  InflateRect(R, -2, 0);

  DrawText(aCanvas.Handle, PChar(Caption), Length(Caption), R, DT_SINGLELINE or DT_VCENTER or DT_LEFT);

  if FShowButton then begin
    Bmp := TdtmCollapsableIcons.ToBitmap(FCollapsed);
    if assigned(Bmp) and not Bmp.Empty then begin
      X := R.Right - Bmp.Width;
      Y := R.Top + (((R.Bottom - R.Top) - Bmp.height) div 2);
      aCanvas.Draw(X, Y, Bmp);
    end;
  end;
end;

procedure TPPWCollapsablePanel.Resize;
begin
  inherited;
  if not FUpdating.Updating and not FCollapsed then begin
    FExpandedHeight := Height;
  end;
end;

procedure TPPWCollapsablePanel.SetCollapsed(const Value: Boolean);
begin
  if FCollapsed <> Value then begin
    if (csLoading in ComponentState) then begin
      FCollapsed := True;
    end else begin
      if Value then Collapse else Expand;
    end;
  end;
end;

procedure TPPWCollapsablePanel.SetExpanded(const Value: Boolean);
begin
  Collapsed := not Value;
end;

procedure TPPWCollapsablePanel.SetExpandedHeight(const Value: Integer);
var
  V : Integer;
begin
  V := Value;
  if (Value < TitleHeight) then begin
   // if (csDesigning in ComponentState) then begin
   //   V := TitleHeight;
   // end else begin
   //   raise Exception.Create('Expanded height cannot be smaller then the titleheight');
   //end;
  end;
  if FExpandedHeight <> V then begin
    FExpandedHeight := V;
    if not FCollapsed then begin
      FUpdating.BeginUpdate;
      try
        Height := FExpandedHeight;
      finally
        FUpdating.EndUpdate;
      end;
    end;
  end;
end;

procedure TPPWCollapsablePanel.SetShowButton(const Value: Boolean);
begin
  if FShowButton <> Value then begin
    FShowButton := Value;
    Invalidate;
  end;
end;

procedure TPPWCollapsablePanel.SetShowTitle(const Value: Boolean);
begin
  if FShowTitle <> Value then begin
    FShowTitle := Value;
    Invalidate;
  end;
end;

procedure TPPWCollapsablePanel.SetTitleColor(const Value: TColor);
begin
  if FTitleColor <> Value then begin
    FTitleColor := Value;
    Invalidate;
  end;
end;

procedure TPPWCollapsablePanel.SetTitleFontColor(const Value: TColor);
begin
  if FTitleFontColor <> Value then begin
    FTitleFontColor := Value;
    Invalidate;
  end;
end;

procedure TPPWCollapsablePanel.SetTitleHeight(const Value: Integer);
begin
  if Value <= 0 then begin
    raise Exception.Create('Negative value not allowed');
  end;
  if FTitleHeight <> Value then begin
    FTitleHeight := Value;
    if FCollapsed then begin
      Height := FTitleheight;
    end else begin
      Realign;
      Invalidate;
    end;
  end;
end;

function TPPWCollapsablePanel.StoreTitleColor: Boolean;
begin
  Result := FTitleColor <> clActiveCaption;
end;

function TPPWCollapsablePanel.StoreTitleFontColor: Boolean;
begin
  Result := FTitleFontColor <> clCaptionText;
end;

end.
