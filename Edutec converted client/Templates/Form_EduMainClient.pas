{*****************************************************************************
  This unit contains the Main MDI Form used for the Edutec Client Application.

  @Name       Form_EduMainClient
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  20/07/2005   sLesage              Added the GetMainDataModule method.
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_EduMainClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_FVBFFCMainClient, cxGraphics, dxBarExtItems, dxBar,
  cxControls, dxStatusBar, Unit_FVBFFCDevExpress, Data_FVBFFCMainClient,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxStatusBarPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, cxClasses;

type
  TfrmEDUMainClient = class(TfrmFVBFFCMainClient)
    dxbbShowLanguages: TdxBarButton;
    dxbbShowCountries: TdxBarButton;
    dxbbShowCountryParts: TdxBarButton;
    dxbbShowProvinces: TdxBarButton;
    dxbbShowPostalCodes: TdxBarButton;
    dxbbShowDiplomas: TdxBarButton;
    dxbbShowSessionGroups: TdxBarButton;
    dxbbShowDisciplines: TdxBarButton;
    dxbbShowSchoolyears: TdxBarButton;
    dxbbShowUsers: TdxBarButton;
    dxbbShowProfiles: TdxBarButton;
    dxbbShowRoles: TdxBarButton;
    dxbbShowProgProgram: TdxBarButton;
    dxbbShowSesSession: TdxBarButton;
    dxbbShowDocInstructor: TdxBarButton;
    dxbbShowCenInfrastructure: TdxBarButton;
    dxbbOrgTypes: TdxBarButton;
    dxbbOrganisations: TdxBarButton;
    dxbbPersons: TdxBarButton;
    dxbbStatus: TdxBarButton;
    dxbbDxBarButton1: TdxBarButton;
    dxbbPersonen: TdxBarLargeButton;
    dxbbOrganisaties: TdxBarLargeButton;
    dxbbSessieWizardLeeg: TdxBarLargeButton;
    dxbsiDxBarSubItem1: TdxBarSubItem;
    dxbbOpleidingsaanbod: TdxBarLargeButton;
    dxbbSessies: TdxBarLargeButton;
    dxbbSessieWizardEmpty: TdxBarButton;
    dxbsiDxBarSubItem2: TdxBarSubItem;
    dxbbSessieWizardEmpty1: TdxBarButton;
    dxbbDxBarLargeButton1: TdxBarLargeButton;
    dxbbDxBarLargeButton2: TdxBarLargeButton;
    dxbbFileExit: TdxBarButton;
    dxbbDxBarLargeButton3: TdxBarLargeButton;
    dxbbDatabaseWizardEmpty1: TdxBarButton;
    dxbsiDxBarSubItem3: TdxBarSubItem;
    dxbsiDxBarSubItem4: TdxBarSubItem;
    dxbbDxBarButton3: TdxBarButton;
    dxbbDxBarButton4: TdxBarButton;
    dxbbDatabaseWizardEmpty: TdxBarLargeButton;
    dxbbDxBarButton2: TdxBarButton;
    dxbbShowQuery: TdxBarButton;
    dxbsiShowQueryGroup: TdxBarSubItem;
    dxbbShowQuerygroup: TdxBarButton;
    dxbbAbout: TdxBarButton;
    dxBarButton1: TdxBarButton;
    dxbbDxBarButton5: TdxBarButton;
    dxbbTbPrefacturatie: TdxBarLargeButton;
    dxbbMnuPrefacturatie: TdxBarButton;
    dxbbStartNewBookYear: TdxBarButton;
    dxbbDxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxbbCooperation: TdxBarButton;
    dxbbKMOPORTStatus: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarLargeButtonImportExcel: TdxBarLargeButton;
    procedure dxbbAboutClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  protected
    function GetMainDataModule : TdtmFVBFFCMainClient; override;
  public
    Constructor Create( AOwner : TComponent ); override;
    { Public declarations }
  end;

var
  frmEDUMainClient: TfrmEDUMainClient;

implementation

uses
  Data_EDUMainClient, Unit_PPWFrameWorkInterfaces, Unit_PPWFrameWorkClasses,
  Data_SesWizard, DBClient, Form_About, cxFormats;

{$R *.dfm}

{ TfrmEDUMainClient }

{*****************************************************************************
  This method will return a reference to the Project's Main DataModule.

  @Name       TfrmEDUMainClient.GetMainDataModule
  @author     slesage
  @param      None
  @return     Returns a reference to the Project's Main DataModule.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmEDUMainClient.GetMainDataModule: TdtmFVBFFCMainClient;
begin
  Result := dtmEDUMainClient;
end;

procedure TfrmEDUMainClient.dxbbAboutClick(Sender: TObject);
var
   aAbout : TfrmAbout;
begin
  inherited;
  aAbout := TfrmAbout.Create( Application );
  aAbout.ShowModal;
  freeAndNil( aAbout );
end;

constructor TfrmEDUMainClient.Create(AOwner: TComponent);
begin
  inherited;
  Self.Caption := Application.Title;
  sbrMain.Panels[0].Text := '';
  sbrMain.Panels[1].Text := TdtmEDUMainClient(MainDataModule).USERinfo_UserName
                          + ' ('
                          + inttostr(TdtmEDUMainClient(MainDataModule).USERinfo_UserID)
                          + ')';
  sbrMain.Panels[2].Text := TdtmEDUMainClient(MainDataModule).USERinfo_DBName;
  sbrMain.Panels[3].Text := TdtmEDUMainClient(MainDataModule).USERinfo_ServerName;
  sbrMain.Panels[4].Text := inttostr(TdtmEDUMainClient(MainDataModule).USERinfo_SPID);

  {$IFDEF DEBUG}
    self.Color := clRed;
  {$ENDIF}
end;

procedure TfrmEDUMainClient.FormCreate(Sender: TObject);
begin
  inherited;
  FormatSettings.DateSeparator := '/';
  FormatSettings.ShortDateFormat := 'dd/MM/yyyy';
  cxFormatController.UseDelphiDateTimeFormats := True;
  cxFormatController.NotifyListeners;
end;

procedure TfrmEDUMainClient.FormActivate(Sender: TObject);
begin
  inherited;
    {$IFDEF DEBUG}
      Color := clRed;
    {$ENDIF}
end;

end.
