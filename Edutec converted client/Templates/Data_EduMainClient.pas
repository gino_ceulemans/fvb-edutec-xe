{*****************************************************************************
  This unit contains the Main DataModule used by the Edutec Client Application.

  @Name       Data_EduMainClient
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/12/2007   ivdbossche           Added GetPrinterCRReports
  19/07/2007   ivdbossche           Added class TfrmConfirmationReports to RegisterFrameWorkClass
  18/07/2007   ivdbossche           Added functions GetPrinterCertificates and GetPrinterLetters
  17/07/2007   ivdbossche           Added procedure GetEnrolIdsFromSession
  26/07/2005   sLesage              Added Program Instructors.
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_EduMainClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_FVBFFCMainClient, DB, ADODB, AppEvnts, Unit_PPWFrameWorkInterfaces,
  cxEditRepositoryItems, cxExtEditRepositoryItems, cxEdit, DBClient,
  Unit_FVBFFCDevExpress, ImgList, Unit_FVBFFCComponents, cxContainer,
  StdActns, ExtActns, Unit_PPWFrameWorkActions, ActnList, cxClasses,
  cxStyles, cxGridTableView, cxLookAndFeels, Unit_PPWFrameWorkController,
  Unit_FVBFFCDBComponents, Unit_PPWFrameWorkClasses, MConnect, SConnect,
  Unit_FVBFFCInterfaces, unit_EdutecInterfaces, cxGridBandedTableView,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, System.ImageList, System.Actions, Data.DBXDataSnap,
  Data.DBXCommon, IPPeerClient, Data.SqlExpr, Data.FMTBcd, Datasnap.DSConnect;

type
  TStatus = (
            stUnknown = -1,
            stPlanned = 1,
            stCancelled = 2,
            stFull = 3,
            stMinEnrolments = 4,
            stClosed = 5,
            stSubscribed = 6,
            stMildlyInterested = 7,
            stInterested = 8,
            stWillFollow = 9,
            stAttested = 10,
            stSend = 11,
            stInvoiced = 12,
            stNotApplicable = 13,
            stWait = 14
            );

  TdtmEDUMainClient = class(TdtmFVBFFCMainClient)
    pfcMain: TPPWFrameWorkController;
    acShowLanguages: TPPWFrameWorkShowListViewAction;
    acShowCountry: TPPWFrameWorkShowListViewAction;
    acShowCountryPart: TPPWFrameWorkShowListViewAction;
    acShowProvince: TPPWFrameWorkShowListViewAction;
    acShowPostalCode: TPPWFrameWorkShowListViewAction;
    acShowSessionGroup: TPPWFrameWorkShowListViewAction;
    acShowRole: TPPWFrameWorkShowListViewAction;
    acShowSchoolYear: TPPWFrameWorkShowListViewAction;
    acShowDisciplines: TPPWFrameWorkShowListViewAction;
    acShowUsers: TPPWFrameWorkShowListViewAction;
    acShowProfiles: TPPWFrameWorkShowListViewAction;
    acShowDiploma: TPPWFrameWorkShowListViewAction;
    acShowProgProgram: TPPWFrameWorkShowListViewAction;
    acShowSesSession: TPPWFrameWorkShowListViewAction;
    cxeriPrijsBepaling: TcxEditRepositoryImageComboBoxItem;
    acShowDocInstructor: TPPWFrameWorkShowListViewAction;
    adospRecordID: TFVBFFCStoredProc;
    acShowCenInfrastructure: TPPWFrameWorkShowListViewAction;
    acShowOrgType: TPPWFrameWorkShowListViewAction;
    acShowOrganisations: TPPWFrameWorkShowListViewAction;
    acShowPersons: TPPWFrameWorkShowListViewAction;
    acShowStatus: TPPWFrameWorkShowListViewAction;
    cxeriPrijsBepalingReadOnly: TcxEditRepositoryImageComboBoxItem;
    acSessionWizardEmpty: TAction;
    cxeriExpenseOrigin: TcxEditRepositoryImageComboBoxItem;
    cxeriExpenseOriginReadOnly: TcxEditRepositoryImageComboBoxItem;
    sckcnnMainOrr: TFVBFFCSocketConnection;
    acShowQueryGroup: TPPWFrameWorkShowListViewAction;
    acShowQuery: TPPWFrameWorkShowListViewAction;
    acDatabaseWizard: TAction;
    cxeriActive: TcxEditRepositoryImageComboBoxItem;
    acInvoicing: TPPWFrameWorkShowListViewAction;
    acInputLastDocNumber: TAction;
    cseriFactuurType: TcxEditRepositoryImageComboBoxItem;
    cseriFactuurTypeReadOnly: TcxEditRepositoryImageComboBoxItem;
    cxsOrange: TcxStyle;
    cxsRed: TcxStyle;
    cxsGray: TcxStyle;
    cxsYellow: TcxStyle;
    acShowImport: TPPWFrameWorkShowListViewAction;
    qryTemp: TFVBFFCQuery;
    cxsBlue: TcxStyle;
    acShowCooperation: TPPWFrameWorkShowListViewAction;
    acShowKMO_PORT_Status: TPPWFrameWorkShowListViewAction;
    acShowKMOPortefeuille: TPPWFrameWorkShowListViewAction;
    cxsKMOPortefeuille: TcxStyle;
    acShowKMO_PORT_Payment: TPPWFrameWorkShowListViewAction;
    acImportExcel: TPPWFrameWorkShowListViewAction;
    sckcnnMain: TSQLConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure acSessionWizardEmptyExecute(Sender: TObject);
    procedure acListViewAddBeforeUpdate(Sender: TPPWFrameWorkAction;
      var DisableAction: Boolean);
    procedure acDatabaseWizardExecute(Sender: TObject);
    procedure acInputLastDocNumberExecute(Sender: TObject);
  private
    { Private declarations }
    FUserID: integer;
    FUserName: string;
    FDBName: string;
    FServerName: string;
    FSPID: integer;
    procedure CloneReposToReadOnlyRepos;
  public
    { Public declarations }
    function GetNewRecordID( aTableName : String ) : Integer;
    procedure Connect; virtual;
    function Login(Username, Password: string): Integer;
    function GetNameForState (const State: Integer): String;
    function GetNameForKMO_Portefeuille_State (const State: Integer): String;
    function GetNameForCooperation (const Cooperation: Integer): String;
    { Extra functionality to adjust layout - only on screen }
    procedure SetReadOnlyRepositoryItem(aCustomEdit: TcxCustomEdit);
    procedure ResetRepositoryItem(aCustomEdit: TcxCustomEdit);
    procedure ExecuteCrystal(const ReportName, ReportTitle: String;
      ParamNames, ParamValues: TStringList; const preview,
      portrait: Boolean; OnPreviewClosed: TNotifyEvent; const SelectedPrinter: string = '');overload;
    procedure ExecuteCrystal(const ReportName, ReportTitle: String;
      ParamNames, ParamValues: TStringList; portrait: Boolean;
      OnClosed: TNotifyEvent; const PdfOutputfile: string);overload;
    function ReportRecordCount (const ViewName: String; const DistinctFields: String;
      const KeyField: String; KeyValues: TStringList): Integer;

    procedure GetEnrolIdsFromSession(const ASessionId: string; AEnrolIds: TStringList);
    function GetPrinterLetters: String;
    function GetPrinterCertificates: String;
    function GetPrinterEvaluationDoc: String;
    function GetPathCRReports: String;
    function GetCurUser: String;
    function GetConfirmationPathCurUser: String;
    function GetEmailPublFolder: String;
    procedure PrepareConfirmationReports(const ASessionId: WideString);
    //
    property USERinfo_UserID: integer read FUserID;
    property USERinfo_UserName: string read FUserName;
    property USERinfo_DBName: string read FDBName;
    property USERinfo_SPID: integer read FSPID;
    property USERinfo_ServerName: string read FServerName;
  end;

var
  dtmEDUMainClient: TdtmEDUMainClient;

implementation

uses
  { Units for the Invoicing Entity }
  Data_Invoicing, Form_Invoicing_Record, Form_Invoicing_List,
  { Units for the SesFact Entity }
  Data_SesFact, Form_SesFact_Record, Form_SesFact_List,
  { Units for the DocCenter Entity }
  Data_DocCenter, Form_DocCenter_Record, Form_DocCenter_List,
  { Units for the ImportOverview Entity }
  Data_ImportOverview, Form_ImportOverview_Record, Form_ImportOverview_List,
  { Units for the Import Entity }
  Data_Import, Form_Import_List,
  { Units for the Import Excel Entity }
  Data_Import_Excel, Form_Import_Excel_List,
  { Units for the Report Entity }
  Data_Report, Form_Report_Record, Form_Report_List,
  { Units for the QueryWizard Entity }
  Data_QueryWizard, Form_QueryWizard_Record, Form_QueryWizard_List,
  { Units for the Tablefield Entity }
  Data_Tablefield, Form_Tablefield_Record, Form_Tablefield_List,
  { Units for the LogHistory Entity }
  Data_LogHistory, Form_LogHistory_Record, Form_LogHistory_List,
  { Units for the ParameterType Entity }
  Data_ParameterType, Form_ParameterType_Record, Form_ParameterType_List,
  { Units for the Parameter Entity }
  Data_Parameter, Form_Parameter_Record, Form_Parameter_List,
  { Units for the Query Entity }
  Data_Query, Form_Query_Record, Form_Query_List,
  { Units for the QueryGroup Entity }
  Data_QueryGroup, Form_QueryGroup_Record, Form_QueryGroup_List,
  { Units for the CenRoom Entity }
  Data_CenRoom, Form_CenRoom_Record, Form_CenRoom_List,
  { Units for the SesExpenses Entity }
  Data_SesExpenses, Form_SesExpenses_Record, Form_SesExpenses_List,
  { Units for the SesWizard Entity }
  Data_SesWizard, Form_SesWizard_Record, Form_SesWizard_List,
  { Units for the Enrolment Entity }
  Data_Enrolment, Form_Enrolment_Record, Form_Enrolment_List,
  { Units for the Status Entity }
  Data_Status, Form_Status_Record, Form_Status_List,
  { Units for the Role Entity }
  Data_Role, Form_Role_Record, Form_Role_List,
  { Units for the Person Entity }
  Data_Person, Form_Person_Record, Form_Person_List,
  { Units for the OrganisationType Entity }
  Data_OrganisationType, Form_OrganisationType_Record, Form_OrganisationType_List,
  { Units for the Organisation Entity }
  Data_Organisation, Form_Organisation_Record, Form_Organisation_List,
  { Units for the CenInfrastructure Entity }
  Data_CenInfrastructure, Form_CenInfrastructure_Record, Form_CenInfrastructure_List,
  { Units for the ProgInstructor Entity }
  Data_ProgInstructor, Form_ProgInstructor_Record, Form_ProgInstructor_List,
  { Units for the Instructor Entity }
  Data_Instructor, Form_Instructor_Record, Form_Instructor_List,
  { Units for the Medium Entity }
  Data_Medium, Form_Medium_Record, Form_Medium_List,
  { Units for the Title Entity }
  Data_Title, Form_Title_Record, Form_Title_List,
  { Units for the Gender Entity }
  Data_Gender, Form_Gender_Record, Form_Gender_List,
  { Units for the SesSession Entity }
  Data_SesSession, Form_SesSession_Record, Form_SesSession_List,
  { Units for the ProgProgram Entity }
  Data_ProgProgram, Form_ProgProgram_Record, Form_ProgProgram_List,
  { Units for the UserProfile Entity }
  Data_UserProfile, Form_UserProfile_Record, Form_UserProfile_List,
  { Units for the Profile Entity }
  Data_Profile, Form_Profile_Record, Form_Profile_List,
  { Units for the User Entity }
  Data_User, Form_User_Record, Form_User_List,
  { Units for the ProgDiscipline Entity }
  Data_ProgDiscipline, Form_ProgDiscipline_Record, Form_ProgDiscipline_List,
  { Units for the Schoolyear Entity }
  Data_Schoolyear, Form_Schoolyear_Record, Form_Schoolyear_List,
  { Units for the RoleGroup Entity }
  Data_RoleGroup, Form_RoleGroup_Record, Form_RoleGroup_List,
  { Units for the SessionGroup Entity }
  Data_SessionGroup, Form_SessionGroup_Record, Form_SessionGroup_List,
  { Units for the Diploma Entity }
  Data_Diploma, Form_Diploma_Record, Form_Diploma_List,
  { Units for the Postalcode Entity }
  Data_Postalcode, Form_Postalcode_Record, Form_Postalcode_List,
  { Units for the Province Entity }
  Data_Province, Form_Province_Record, Form_Province_List,
  { Units for the CountryPart Entity }
  Data_CountryPart, Form_CountryPart_Record, Form_CountryPart_List,
  { Units for the Country Entity }
  Data_Country, Form_Country_Record, Form_Country_List,
  { Units for the Language Entity }
  Data_Language, Form_Language_Record, Form_Language_List, Registry,
  { Units for the ProgramType Entity }
  Data_ProgramType, Form_ProgramType_Record, Form_ProgramType_List,
  { Units for the Confirmation Reports Entity }
  Data_ConfirmationReports, Form_Confirmation_Reports,
  { Units for the Cooperation Entity }
  Data_Cooperation, Form_Cooperation_Record, Form_Cooperation_List,
  { Units for the KMO Portefeuille }
  Data_KMO_PORT_Status, Form_KMO_PORT_Status_Record, Form_KMO_PORT_Status_List,
  Data_KMO_Portefeuille, Form_KMO_Portefeuille_Record, Form_KMO_Portefeuille_List,
  Data_KMO_PORT_Payment, Form_KMO_PORT_Payment_Record, Form_KMO_PORT_Payment_List,
  Data_KMO_PORT_Link_Port_Payment, Form_KMO_PORT_Link_Port_Payment_Record, Form_KMO_PORT_Link_Port_Payment_List,
  Data_KMO_PORT_Link_Port_Session, Form_KMO_PORT_Link_Port_Session_Record, Form_KMO_PORT_Link_Port_Session_List,

  Unit_FVBFFCUtils, INIFiles, Form_Splash, DateUtils;//, data_Crystal;

resourcestring
  rsInputNumber = 'Geef laatst gebruikt factuurnr in';
  rsDocNumber = 'Laatst gebruikt factuurnr : ';

{$R *.dfm}

{*****************************************************************************
  This method will be executed when the DataModule is created and will be used
  to register all FrameWorkClasses at RunTime.

  @Name       TdtmEDUMainClient.DataModuleCreate
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEDUMainClient.DataModuleCreate(Sender: TObject);
var
  aSplash : TfrmSplash;
  waitUntil: TDateTime;
begin
  // 172.21.129.23

  FUserID := 0;
  FUserName := '';
  FDBName := '';
  FServerName := '';
  FSPID := -1;

  aSplash := TfrmSplash.Create( self );

  aSplash.Show;

  waitUntil := IncSecond (Now, 1); //just wait a second for the splashscreen to be shown
  while (Now < waitUntil) do
  begin
    Application.ProcessMessages;
  end;

  { Get the Address of the Application Server }
  with TIniFile.Create( ApplicationIniFileName ) do
  begin
  //GCXE  sckcnnMain.Address := ReadString( 'Server', 'Address', '192.168.100.52' ); //TODO
    // Om er zeker van te zijn dat de gelezen waarde numeriek is,
    // deze even controleren ..
    Free;
  end;

  inherited;
  { PPWFrameWork Start RegisterClasses }
  { Register the FrameWorkDataModules }
  PPWController.RegisterFrameWorkClass( TdtmLanguage );
  PPWController.RegisterFrameWorkClass( TdtmProgramType );
  PPWController.RegisterFrameWorkClass( TdtmCountry );
  PPWController.RegisterFrameWorkClass( TdtmCountryPart );
  PPWController.RegisterFrameWorkClass( TdtmProvince );
  PPWController.RegisterFrameWorkClass( TdtmPostalcode );
  PPWController.RegisterFrameWorkClass( TdtmDiploma );
  PPWController.RegisterFrameWorkClass( TdtmSessionGroup );
  PPWController.RegisterFrameWorkClass( TdtmRoleGroup );
  PPWController.RegisterFrameWorkClass( TdtmSchoolyear );
  PPWController.RegisterFrameWorkClass( TdtmProgDiscipline );
  PPWController.RegisterFrameWorkClass( TdtmUser );
  PPWController.RegisterFrameWorkClass( TdtmProfile );
  PPWController.RegisterFrameWorkClass( TdtmUserProfile );
  PPWController.RegisterFrameWorkClass( TdtmProgProgram );
  PPWController.RegisterFrameWorkClass( TdtmSesSession );
  PPWController.RegisterFrameWorkClass( TdtmGender );
  PPWController.RegisterFrameWorkClass( TdtmTitle );
  PPWController.RegisterFrameWorkClass( TdtmMedium );
  PPWController.RegisterFrameWorkClass( TdtmInstructor );
  PPWController.RegisterFrameWorkClass( TdtmProgInstructor );
  PPWController.RegisterFrameWorkClass( TdtmCenInfrastructure );
  PPWController.RegisterFrameWorkClass( TdtmOrganisation );
  PPWController.RegisterFrameWorkClass( TdtmOrganisationType );
  PPWController.RegisterFrameWorkClass( TdtmPerson );
  PPWController.RegisterFrameWorkClass( TdtmRole );
  PPWController.RegisterFrameWorkClass( TdtmStatus );
  PPWController.RegisterFrameWorkClass( TdtmEnrolment );
  PPWController.RegisterFrameWorkClass( TdtmSesWizard );
  PPWController.RegisterFrameWorkClass( TdtmSesExpenses );
  PPWController.RegisterFrameWorkClass( TdtmCenRoom );
  PPWController.RegisterFrameWorkClass( TdtmQueryGroup );
  PPWController.RegisterFrameWorkClass( TdtmQuery );
  PPWController.RegisterFrameWorkClass( TdtmParameter );
  PPWController.RegisterFrameWorkClass( TdtmParameterType );
  PPWController.RegisterFrameWorkClass( TdtmLogHistory );
  PPWController.RegisterFrameWorkClass( TdtmTableField );
  PPWController.RegisterFrameWorkClass( TdtmQueryWizard );
  PPWController.RegisterFrameWorkClass( TdtmReport );
  PPWController.RegisterFrameWorkClass( TdtmImportOverview );
  PPWController.RegisterFrameWorkClass( TdtmImport );
  PPWController.RegisterFrameWorkClass( TdtmImport_Excel );
  PPWController.RegisterFrameWorkClass( TdtmDocCenter );
  PPWController.RegisterFrameWorkClass( TdtmSesFact );
  PPWController.RegisterFrameWorkClass( TdtmInvoicing );
  PPWController.RegisterFrameWorkClass( TdtmConfirmationReports );
  PPWController.RegisterFrameWorkClass( TdtmCooperation );
  PPWController.RegisterFrameWorkClass( TdtmKMO_PORT_Status );
  PPWController.RegisterFrameWorkClass( TdtmKMO_Portefeuille );
  PPWController.RegisterFrameWorkClass( TdtmKMO_PORT_Payment );
  PPWController.RegisterFrameWorkClass( TdtmKMO_PORT_Link_Port_Payment );
  PPWController.RegisterFrameWorkClass( TdtmKMO_PORT_Link_Port_Session );

  { Register the FrameWorkRecordViews }
  PPWController.RegisterFrameWorkClass( TfrmLanguage_Record );
  PPWController.RegisterFrameWorkClass( TfrmProgramType_Record );
  PPWController.RegisterFrameWorkClass( TfrmCountry_Record );
  PPWController.RegisterFrameWorkClass( TfrmCountryPart_Record );
  PPWController.RegisterFrameWorkClass( TfrmProvince_Record );
  PPWController.RegisterFrameWorkClass( TfrmPostalcode_Record );
  PPWController.RegisterFrameWorkClass( TfrmDiploma_Record );
  PPWController.RegisterFrameWorkClass( TfrmSessionGroup_Record );
  PPWController.RegisterFrameWorkClass( TfrmRoleGroup_Record );
  PPWController.RegisterFrameWorkClass( TfrmSchoolyear_Record );
  PPWController.RegisterFrameWorkClass( TfrmProgDiscipline_Record );
  PPWController.RegisterFrameWorkClass( TfrmUser_Record );
  PPWController.RegisterFrameWorkClass( TfrmProfile_Record );
  PPWController.RegisterFrameWorkClass( TfrmUserProfile_Record );
  PPWController.RegisterFrameWorkClass( TfrmProgProgram_Record );
  PPWController.RegisterFrameWorkClass( TfrmSesSession_Record );
  PPWController.RegisterFrameWorkClass( TfrmGender_Record );
  PPWController.RegisterFrameWorkClass( TfrmTitle_Record );
  PPWController.RegisterFrameWorkClass( TfrmMedium_Record );
  PPWController.RegisterFrameWorkClass( TfrmInstructor_Record );
  PPWController.RegisterFrameWorkClass( TfrmProgInstructor_Record );
  PPWController.RegisterFrameWorkClass( TfrmCenInfrastructure_Record );
  PPWController.RegisterFrameWorkClass( TfrmOrganisation_Record );
  PPWController.RegisterFrameWorkClass( TfrmOrganisationType_Record );
  PPWController.RegisterFrameWorkClass( TfrmPerson_Record );
  PPWController.RegisterFrameWorkClass( TfrmRole_Record );
  PPWController.RegisterFrameWorkClass( TfrmStatus_Record );
  PPWController.RegisterFrameWorkClass( TfrmEnrolment_Record );
  PPWController.RegisterFrameWorkClass( TfrmSesWizard_Record );
  PPWController.RegisterFrameWorkClass( TfrmSesExpenses_Record );
  PPWController.RegisterFrameWorkClass( TfrmCenRoom_Record );
  PPWController.RegisterFrameWorkClass( TfrmQuery_Record );
  PPWController.RegisterFrameWorkClass( TfrmQueryGroup_Record );
  PPWController.RegisterFrameWorkClass( TfrmParameterType_Record );
  PPWController.RegisterFrameWorkClass( TfrmParameter_Record );
  PPWController.RegisterFrameWorkClass( TfrmLogHistory_Record );
  PPWController.RegisterFrameWorkClass( TfrmTablefield_Record );
  PPWController.RegisterFrameWorkClass( TfrmReport_Record );
  PPWController.RegisterFrameWorkClass( TfrmQueryWizard_Record );
  PPWController.RegisterFrameWorkClass( TfrmImportOverview_Record );
  PPWController.RegisterFrameWorkClass( TfrmDocCenter_Record );
  PPWController.RegisterFrameWorkClass( TfrmSesFact_Record );
  PPWController.RegisterFrameWorkClass( TfrmInvoicing_Record );
  PPWController.RegisterFrameWorkClass( TfrmConfirmationReports );
  PPWController.RegisterFrameWorkClass( TfrmCooperation_Record );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Status_Record );
  PPWController.RegisterFrameWorkClass( TfrmKMO_Portefeuille_Record );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Payment_Record );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Link_Port_Payment_Record );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Link_Port_Session_Record );

  { Register the FrameWorkListViews }
  PPWController.RegisterFrameWorkClass( TfrmLanguage_List );
  PPWController.RegisterFrameWorkClass( TfrmProgramType_List );
  PPWController.RegisterFrameWorkClass( TfrmCountryPart_List );
  PPWController.RegisterFrameWorkClass( TfrmCountry_List );
  PPWController.RegisterFrameWorkClass( TfrmProvince_List );
  PPWController.RegisterFrameWorkClass( TfrmPostalcode_List );
  PPWController.RegisterFrameWorkClass( TfrmDiploma_List );
  PPWController.RegisterFrameWorkClass( TfrmSessionGroup_List );
  PPWController.RegisterFrameWorkClass( TfrmRoleGroup_List );
  PPWController.RegisterFrameWorkClass( TfrmSchoolyear_List );
  PPWController.RegisterFrameWorkClass( TfrmProgDiscipline_List );
  PPWController.RegisterFrameWorkClass( TfrmUser_List );
  PPWController.RegisterFrameWorkClass( TfrmProfile_List );
  PPWController.RegisterFrameWorkClass( TfrmUserProfile_List );
  PPWController.RegisterFrameWorkClass( TfrmProgProgram_List );
  PPWController.RegisterFrameWorkClass( TfrmSesSession_List );
  PPWController.RegisterFrameWorkClass( TfrmGender_List );
  PPWController.RegisterFrameWorkClass( TfrmTitle_list );
  PPWController.RegisterFrameWorkClass( TfrmMedium_List );
  PPWController.RegisterFrameWorkClass( TfrmInstructor_List );
  PPWController.RegisterFrameWorkClass( TfrmProgInstructor_List );
  PPWController.RegisterFrameWorkClass( TfrmCenInfrastructure_List );
  PPWController.RegisterFrameWorkClass( TfrmOrganisation_List );
  PPWController.RegisterFrameWorkClass( TfrmOrganisationType_List );
  PPWController.RegisterFrameWorkClass( TfrmPerson_List );
  PPWController.RegisterFrameWorkClass( TfrmRole_List );
  PPWController.RegisterFrameWorkClass( TfrmStatus_List );
  PPWController.RegisterFrameWorkClass( TfrmEnrolment_List );
  PPWController.RegisterFrameWorkClass( TfrmSesWizard_List );
  PPWController.RegisterFrameWorkClass( TfrmSesExpenses_List );
  PPWController.RegisterFrameWorkClass( TfrmCenRoom_List );
  PPWController.RegisterFrameWorkClass( TfrmQuery_List );
  PPWController.RegisterFrameWorkClass( TfrmQueryGroup_List );
  PPWController.RegisterFrameWorkClass( TfrmParameterType_List );
  PPWController.RegisterFrameWorkClass( TfrmTablefield_List );
  PPWController.RegisterFrameWorkClass( TfrmLogHistory_List );
  PPWController.RegisterFrameWorkClass( TfrmParameter_List );
  PPWController.RegisterFrameWorkClass( TfrmQueryWizard_List );
  PPWController.RegisterFrameWorkClass( TfrmImportOverview_List );
  PPWController.RegisterFrameWorkClass( TfrmImport_List );
  PPWController.RegisterFrameWorkClass( TfrmImport_Excel_List );
  PPWController.RegisterFrameWorkClass( TfrmDocCenter_List );
  PPWController.RegisterFrameWorkClass( TfrmSesFact_List );
  PPWController.RegisterFrameWorkClass( TfrmReport_List );
  PPWController.RegisterFrameWorkClass( TfrmInvoicing_List );
  PPWController.RegisterFrameWorkClass( TfrmCooperation_List );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Status_List );
  PPWController.RegisterFrameWorkClass( TfrmKMO_Portefeuille_List );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Payment_List );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Link_Port_Payment_List );
  PPWController.RegisterFrameWorkClass( TfrmKMO_PORT_Link_Port_Session_List );
  { PPWFrameWork End RegisterClasses }

  { Extra procedure to create all ReadOnly RepositoryItems at run-time
   except those darn Button-edits }
  CloneReposToReadOnlyRepos;

  aSplash.Close;
  freeandNil( aSplash );
end;

function TdtmEDUMainClient.GetNewRecordID(aTableName: String): Integer;
begin
  try
    adospRecordID.Parameters.ParamByName('@StrTableName').Value := aTableName;
    adospRecordID.ExecProc;
    Result := adospRecordID.Parameters.ParamByName('@nrintLatestValue').Value;
  except
    Raise;
  end;
end;

procedure TdtmEDUMainClient.acSessionWizardEmptyExecute(Sender: TObject);
//var
//  aDataModule    : IPPWFrameWorkDataModule;
//  aClientDataSet : TClientDataSet;
begin
  TdtmSesWizard.OpenWizard( Nil, False );
//  aClientDataSet := TClientDataSet.Create( Self );
//  try
//    pfcMain.ShowModalRecord( 'TdtmSesWizard', '', aClientDataSet, rvmAdd );
//  finally
//    FreeAndNil( aClientDataSet );
//  end;
end;

procedure TdtmEDUMainClient.acListViewAddBeforeUpdate(
  Sender: TPPWFrameWorkAction; var DisableAction: Boolean);
var
  dm: IFVBFFCDataModule;
begin
  inherited;

  if ( Assigned( Sender.FrameWorkDataModule ) ) and
     (
       ( Supports( Sender.FrameWorkDataModule, IEDULogHistoryDataModule ) ) Or
       ( Supports( Sender.FrameWorkDataModule, IEDUReportDataModule ) ) ) then
  begin
    DisableAction := True;
  end
  else
  begin
    if supports ( Sender.FrameWorkDataModule, IFVBFFCDataModule, dm) then
    begin
      DisableAction := not dm.isListViewActionAllowed ( ( Sender as TPPWFrameWorkListViewAction));
    end;
  end;
end;

procedure TdtmEDUMainClient.Connect;
begin
  sckcnnMain.Open;
end;

function TdtmEDUMainClient.Login(Username, Password: string): Integer;
var aServerMethod   : TSqlServerMethod;
begin
  aServerMethod :=  TSqlServerMethod.Create(Self);
  try
    aServerMethod.SQLConnection := dtmEDUMainClient.sckcnnMain;
    aServerMethod.ServerMethodName := Format('%s.%s', ['TrdtmEDUMain', 'Login']);
    aServerMethod.ParamByName('Username').AsString := Username;
    aServerMethod.ParamByName('Password').AsString := Password;
    aServerMethod.ExecuteMethod;
  finally
    aServerMethod.Free;
  end;
//  Result := sckcnnMain.AppServer.Login(Username, Password);

{
  if Result = 0 then
  begin
    FUserID     := sckcnnMain.AppServer.getUSERinfo_UserID;
    FUserName   := sckcnnMain.AppServer.getUSERinfo_UserName;
    FDBName     := sckcnnMain.AppServer.getUSERinfo_DBName;
    FServerName := sckcnnMain.AppServer.getUSERinfo_ServerName;
    FSPID       := sckcnnMain.AppServer.getUSERinfo_SPID;
  end;
}
end;

procedure TdtmEDUMainClient.acDatabaseWizardExecute(Sender: TObject);
begin
  TdtmQueryWizard.OpenWizard( Nil );
end;


{*****************************************************************************
  Returns the name of the state with the given Id.
  @Name       TdtmEDUMainClient.GetNameForState
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
function TdtmEDUMainClient.GetNameForState(const State: Integer): String;
begin
//GCXE  result := sckcnnMain.AppServer.GetNameForState (state);
end;

function TdtmEDUMainClient.GetNameForKMO_Portefeuille_State(const State: Integer): String;
begin
//GCXE  result := sckcnnMain.AppServer.GetNameForKMO_Portefeuille_State (state);
end;


{*****************************************************************************
  Procedure to auto create all readonly repositoryitems ...

  @Name       TdtmEDUMainClient.CloneReposToReadOnlyRepos
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEDUMainClient.CloneReposToReadOnlyRepos;
var
  i : integer;
  aRIName : string;
  aRI, cRi : TcxEditRepositoryItem;
begin
//
  for i:= cserMain.Count -1 downto 0 do
  begin
    if pos('ReadOnly', cserMain.Items[i].Name) = 0 then
    begin
      aRIName := cserMain.Items[i].Name + 'ReadOnly';
      aRI := cserMain.ItemByName( aRIName );
      // Indien deze niet bestaat dan even clonen ...
      if not ( Assigned( aRI ) ) then
      begin
        aRi := cserMain.Items[i];
        cRi := cserMain.CreateItem(TcxEditRepositoryItemClass(aRi.ClassType));
        cRi.Name := aRiName;
        cRi.Properties := aRi.Properties;
        cRi.Properties.ReadOnly := True;
      end;
    end;
  end;
end;

{*****************************************************************************
  Procedure to set the readonly repositoryitem at runtime.

  @Name       TdtmEDUMainClient.SetReadOnlyRepositoryItem
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEDUMainClient.SetReadOnlyRepositoryItem(
  aCustomEdit : TcxCustomEdit);
var
  aRIName : string;
  aRi : TcxEditRepositoryItem;
begin
  if Assigned( aCustomEdit.RepositoryItem ) then
  begin
    aRIName := aCustomEdit.RepositoryItem.Name + 'ReadOnly';
    aRi := cserMain.ItemByName( aRiName );
    if Assigned ( aRi ) then
    begin
      aCustomEdit.RepositoryItem := aRi;
    end;
  end
  else
  begin
    aCustomEdit.ActiveProperties.ReadOnly := True;
  end;
end;

{*****************************************************************************
  Proc to enable controls after Applying changes. ( Needed for BOB LINK support )

  @Name       TdtmEDUMainClient.ResetRepositoryItem
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEDUMainClient.ResetRepositoryItem(
  aCustomEdit: TcxCustomEdit);
var
  aRIName : string;
  aRi : TcxEditRepositoryItem;
begin
  if Assigned( aCustomEdit.RepositoryItem ) then
  begin
    aRIName := stringReplace(aCustomEdit.RepositoryItem.Name, 'ReadOnly', '',[RfIgnoreCase]);
    aRi := cserMain.ItemByName( aRiName );
    if Assigned ( aRi ) then
    begin
      aCustomEdit.RepositoryItem := aRi;
      aCustomEdit.TabStop := True;
    end;
  end
  else
  begin
    aCustomEdit.ActiveProperties.ReadOnly := False;
    aCustomEdit.TabStop := True;
  end;
end;

{*****************************************************************************
  Procedure to be used every time a new export has to be made

  @Name       TdtmEDUMainClient.acInputLastDocNumberExecute
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEDUMainClient.acInputLastDocNumberExecute(Sender: TObject);
var
  aDataModule          : IPPWFrameWorkDataModule;
  aSesFactDataModule   : IEDUSesFactDataModule;
  aFactNr, aNewNr      : integer;
begin
  Inherited;
{
  if ( rsStartNewBookYear,
      mtWarning, [ mbYes, mbNo ], 0 ) = mrYes ) then
  begin
}
  try
    aDataModule := pfcMain.CreateDataModule( Nil, 'TdtmSesFact', False );
    if ( Supports( aDataModule, IEDUSesFactDataModule, aSesFactDataModule ) ) then
    begin
      aFactNr := aSesFactDataModule.RetrieveOldMaximum;
      aNewNr := strToIntDef(
                InputBox(
                         rsInputNumber,
                         rsDocNumber,
                         inttostr(aFactNr)
                         )
                , 0);
      if ( aNewNr > 0 ) then
      begin
        aSesFactDataModule.StartNewBookYear( aNewNr );
      end;
    end;
  finally
  end;
end;



{*****************************************************************************
  Name           : ExecuteCrystal
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    :
  History        :

  Date         By                   Description
  ----         --                   -----------
  18/07/2007   Ivan Van den Bossche Added SelectedPrinter parameter.
  ??/??/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
procedure TdtmEDUMainClient.ExecuteCrystal(const ReportName, ReportTitle: String; ParamNames: TStringList;
  ParamValues: TStringList; const preview: Boolean; const portrait: Boolean; OnPreviewClosed: TNotifyEvent;
  const SelectedPrinter: string = '');
var
  filename, servername, dbname, userID, password: String;
begin
  //filename   := IncludeTrailingBackSlash (ExtractFilePath(ParamStr(0))) + ReportName;

  // Path to Crystal Reports could be setup on a per user basis.
  // Added by Ivan Van den Bossche (26/12/2007)
  fileName := IncludeTrailingPathDelimiter( dtmEDUMainClient.GetPathCRReports ) + ReportName;

{GCXE
  servername := sckcnnMain.AppServer.GetServerName;
  dbname     := sckcnnMain.AppServer.GetDatabaseName;
  userID     := sckcnnMain.AppServer.GetUserID;
  password   := sckcnnMain.AppServer.GetPassword;
  dtmCrystal.Execute ('Edgard', filename, ReportTitle, ParamNames, ParamValues, preview, portrait, servername, dbname, userId, password, OnPreviewClosed, SelectedPrinter);
}

end;

{*****************************************************************************
  Name           : ExecuteCrystal
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Export Crystal Report report to PDF file
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/07/2007   Ivan Van den Bossche Initial Creation.
 *****************************************************************************}
procedure TdtmEDUMainClient.ExecuteCrystal(const ReportName,
  ReportTitle: String; ParamNames, ParamValues: TStringList;
  portrait: Boolean; OnClosed: TNotifyEvent; const PdfOutputfile: string);
var
  filename, servername, dbname, userID, password: String;
begin
  //filename   := IncludeTrailingBackSlash (ExtractFilePath(ParamStr(0))) + ReportName;

  // Path to Crystal Reports could be setup on a per user basis.
  // Added by Ivan Van den Bossche (26/12/2007)
  fileName := IncludeTrailingPathDelimiter( dtmEDUMainClient.GetPathCRReports ) + ReportName;

{GCXE
  servername := sckcnnMain.AppServer.GetServerName;
  dbname     := sckcnnMain.AppServer.GetDatabaseName;
  userID     := sckcnnMain.AppServer.GetUserID;
  password   := sckcnnMain.AppServer.GetPassword;

  dtmCrystal.Execute('Edgard', filename, ReportTitle, ParamNames, ParamValues, portrait, servername, dbname, userid, password, OnClosed, PdfOutputfile);
}

end;

{*****************************************************************************
  Name           : ReportRecordCount
  Author         : Wim Lambrechts
  Arguments      : ViewName: The name of view being usied in the report
                   DistinctFields: The fields on which a distinct is to be done to determine
                    the number of records
                   KeyField / KeyValues: determines the WHERE clause
  Return Values  : None
  Exceptions     : None
  Description    : Counts the number of records that will be retrieved by the report,
                   so we can know, in advance, whether for the selected records
                   a decent report will be printed
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/06/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TdtmEDUMainClient.ReportRecordCount(const ViewName,
  DistinctFields, KeyField: String; KeyValues: TStringList): Integer;
begin
{GCXE
  result := sckcnnMain.AppServer.ReportRecordCount(ViewName, DistinctFields,
    KeyField, KeyValues.commaText);
}
end;


{*****************************************************************************
  Name           : GetEnrolIdsFromSession
  Author         : Ivan Van den Bossche
  Arguments      : ASessionId: Session id on which we retrieve enrol id's.
                   AEnrollIds: Storage for enrol id's.
  Return Values  : None
  Exceptions     : None
  Description    : Retrieves enrol id's for given Session Id.
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
procedure TdtmEDUMainClient.GetEnrolIdsFromSession(
  const ASessionId: string; AEnrolIds: TStringList);
begin
  assert((AEnrolIds <> nil), 'AEnrolIds not assigned to TStringList object!!!');
//GCXE  AEnrolIds.CommaText := sckcnnMain.AppServer.GetEnrolIdsFromSessionId(ASessionId);

end;

function TdtmEDUMainClient.GetPrinterCertificates: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetPrinterCertificates;
end;

function TdtmEDUMainClient.GetPrinterLetters: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetPrinterLetters;
end;

function TdtmEDUMainClient.GetPrinterEvaluationDoc: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetPrinterEvaluationDoc;

end;


{*****************************************************************************
  Name           : PrepareConfirmationReports
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Creates initial data in tables T_REP_CONFIRMATION_HEADER / _DETAILS
                   for given session id.  This procedure is used for confirmation reports
  History        :

  Date         By                   Description
  ----         --                   -----------
  23/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
procedure TdtmEDUMainClient.PrepareConfirmationReports(
  const ASessionId: WideString);
begin
//GCXE  sckcnnMain.AppServer.PrepareConfirmationReports(ASessionId);
end;

{*****************************************************************************
  Name           : GetCurUser
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Returns username for current connected user
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/08/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TdtmEDUMainClient.GetCurUser: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetCurUser; // voornaam + naam
  //Result := sckcnnMain.AppServer.getUSERinfo_UserName; // login-naam
end;

{*****************************************************************************
  Name           : GetPathCRReports
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Returns path to Crystal Report reports used within application
  History        :

  Date         By                   Description
  ----         --                   -----------
  26/12/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TdtmEDUMainClient.GetPathCRReports: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetPathCRReports;
end;

{*****************************************************************************
  Name           : GetConfirmationPathCurUser
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Returns path to confirmation letters.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/02/2008   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TdtmEDUMainClient.GetConfirmationPathCurUser: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetConfirmationPathCurUser;
end;

{*****************************************************************************
  Name           : GetEmailPublFolder
  Author         : Ivan Van den Bossche
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Gets Email public folder for current user
  History        :

  Date         By                   Description
  ----         --                   -----------
  15/03/2008   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
function TdtmEDUMainClient.GetEmailPublFolder: String;
begin
//GCXE  Result := sckcnnMain.AppServer.GetEmailPublFolder;
end;

{*****************************************************************************
  Returns the name of the cooperation with the given Id.
  @Name       TdtmEDUMainClient.GetNameForCooperation
  @author     tclaesen
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
function TdtmEDUMainClient.GetNameForCooperation(
  const Cooperation: Integer): String;
begin
//GCXE  result := sckcnnMain.AppServer.GetNameForCooperation (Cooperation);
end;


end.
