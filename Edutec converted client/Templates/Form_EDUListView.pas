{*****************************************************************************
  This Unit contains the Base PIF ListView used in the Edutec Client
  Application.

  
  @Name       Form_EDUListView
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_EDUListView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_FVBFFCBaseListView, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  Data_FVBFFCMainClient, Form_FVBFFCMainClient, Menus, Unit_PPWFrameWorkClasses,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter,
  PPW_Components_Common_CollapsablePanel;

type
  TEDUListView = class(TFVBFFCBaseListView)
    procedure cxgrdtblvListCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FVBFFCListViewCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function GetMainDataModule: TdtmFVBFFCMainClient; override;
    function GetMainForm       : TfrmFVBFFCMainClient; override;
  public
    { Public declarations }
  end;

var
  EDUListView: TEDUListView;

implementation

uses
  Data_EDUMainClient,
  Data_EDUDataModule,
  Form_EDUMainClient,
  cxFormats;

{$R *.dfm}

{ TfrmEmptyBaseListView }

{*****************************************************************************
  Overridden Property Getter for the MainDataModule property.

  @Name       TEDUListView.GetMainDataModule
  @author     slesage
  @param      None
  @return     Returns the Main DataModule for the Edutec Client.
  @Exception  None
  @See        None
******************************************************************************}

function TEDUListView.GetMainDataModule: TdtmFVBFFCMainClient;
begin
  Result := dtmEDUMainClient;
end;

{*****************************************************************************
  Overridden Property Getter for the MainForm property.

  @Name       TEDUListView.GetMainForm
  @author     slesage
  @param      None
  @return     Returns the Main Form for the Edutec Client.
  @Exception  None
  @See        None
******************************************************************************}

function TEDUListView.GetMainForm: TfrmFVBFFCMainClient;
begin
  Result := frmEDUMainClient;
end;

procedure TEDUListView.cxgrdtblvListCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  { Check if the form was shown for Selection Purposes }
  if ( AsSelection ) then
  begin
    { Return the correct Modal Result }
    if ( Assigned( DataSource ) ) and
       ( Assigned( DataSource.DataSet ) ) and
       ( Not ( ( DataSource.DataSet.BOF ) and ( DataSource.DataSet.EOF ) ) ) then
    begin
//      AHandled := True;
      ModalResult := mrOK;
    end
    else
    begin
//      AHandled := True;
      ModalResult := mrCancel;
    end;
  end
  else
  begin
    { Execute the Edit or View action }
    if ( MainDataModule.acListViewEdit.Enabled ) then
    begin
//      AHandled := True;
      MainDataModule.acListViewEdit.Execute;
    end
    else if ( MainDataModule.acListViewConsult.Enabled ) then
    begin
//      AHandled := True;
      MainDataModule.acListViewConsult.Execute;
    end;
  end;
end;

procedure TEDUListView.FVBFFCListViewCreate(Sender: TObject);
begin
  inherited;
  FormatSettings.DateSeparator := '/';
  FormatSettings.ShortDateFormat := 'dd/MM/yyyy';
  cxFormatController.UseDelphiDateTimeFormats := True;
  cxFormatController.NotifyListeners;
end;

end.
