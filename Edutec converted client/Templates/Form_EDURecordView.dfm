inherited EDURecordView: TEDURecordView
  Caption = 'EDURecordView'
  PixelsPerInch = 96
  TextHeight = 13
  inherited bbtnBtnESCPressed: TBitBtn
    TabStop = False
  end
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      Left = 158
      Width = 476
      ExplicitLeft = 158
      ExplicitWidth = 476
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 476
        ExplicitWidth = 476
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 476
        ExplicitWidth = 476
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 476
        ExplicitWidth = 476
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 476
        ExplicitWidth = 476
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 476
        ExplicitWidth = 476
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 476
        ExplicitWidth = 476
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 476
          ExplicitWidth = 476
        end
        inherited sbxMain: TScrollBox
          Width = 476
          ExplicitWidth = 476
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Width = 150
      ExplicitWidth = 150
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Left = 150
      ExplicitLeft = 150
    end
  end
end
