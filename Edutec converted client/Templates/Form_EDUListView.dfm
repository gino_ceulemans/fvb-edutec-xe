inherited EDUListView: TEDUListView
  Caption = 'EDUListView'
  OnCreate = FVBFFCListViewCreate
  ExplicitWidth = 320
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    DesignSize = (
      711
      41)
    inherited cxbtnEDUButton1: TFVBFFCButton
      Anchors = [akTop, akRight]
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Anchors = [akTop, akRight]
    end
  end
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.IncSearch = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Inactive = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        Styles.Selection = nil
      end
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
end
