inherited EDUDataModule: TEDUDataModule
  OldCreateOrder = True
  OnSort = FVBFFCDataModuleSort
  AllowEmptyFilter = True
  OnInitialiseAdditionalFields = FVBFFCDataModuleInitialiseAdditionalFields
  inherited cdsList: TFVBFFCClientDataSet
    PacketRecords = 25
    RemoteServer = dspConnection
    BeforePost = cdsListBeforePost
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = dspConnection
    BeforePost = cdsListBeforePost
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    RemoteServer = dspConnection
  end
  object ssckData: TFVBFFCSharedConnection
    ParentConnection = dtmEDUMainClient.sckcnnMainOrr
    Left = 16
    Top = 56
  end
  object dspConnection: TFVBFFCDSPConnection
    SQLConnection = dtmEDUMainClient.sckcnnMain
    Left = 280
    Top = 56
  end
end
