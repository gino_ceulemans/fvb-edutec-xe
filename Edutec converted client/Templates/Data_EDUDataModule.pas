{*****************************************************************************
  This Unit contains the Base PIF DataModule used in the Edutec Client
  Application.

  @Name       Data_EDUDataModule
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  06/09/2005   sLesage              Multi Tier Fixes.
  20/07/2005   sLesage              Added the GetMainDataModule method.
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_EDUDataModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_FVBFFCBaseDataModule, DBClient, Data_FVBFFCMainClient,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, DB, ADODB,
  MConnect, Datasnap.DSConnect, Data.FMTBcd, Data.SqlExpr,
  System.Generics.Collections, Data.DBXCommon;

type
  TCopyRecordInformationTo = ( critDefault,
                               critOrganisationToRole,
                               critOrganisationToKMOPortefeuille,
                               critOrganisationToTeacher,
                               critJoinOrganisation,
                               critQueryGrouptoQuery,
                               critParentToQueryGroup,
                               critQuerytoParameter,
                               critParameterTypeToParameter,
                               // SESSION TO ...
                               critSessionToEnroll,
                               critSessionToSesInstructor,
                               critSessionToKMOPortefeuilleFilter,
                               critSessionToKMOPortefeuille,
                               // PERSON TO ...
                               critPersonToSesWizard,
                               critPersonToRole,
                               critPersonToSesWizardExecute,
                               critPersonToEnrol,
                               // RESULT TO ...
                               critResultNlToLookUp,
                               critResultFrToLookUp,
                               // POSTALCODE TO ...
                               critPostalCodeToOrganisationInvoice,
                               critPostalCodeToOrgInvoiceAddress,
                               critPostalCodeToOrganisation,
                               // INSTRUCTOR TO ...
                               critInstructorToProgInstructor,
                               critInstructorToSesInstructor,
                               critInstructorToSesSearchInstructor,
                              // PROGRAM to ...
                               critProgramToSesWizard,
                               critProgramToSession,
                               critProgramToProgInstructor,
                               critProgramToSessionSearchCriteria,
                               // USER to ...
                               critUserToSession,
                               critUserToProgProgram,
                               // ROLE to ...
                               critRoleToSesWizard,
                               critRoleToEnrolment,
                               // INFRASTRUCTURE TO ...
                               critInfrastructureToSession,
                               critInfrastructureToSesWizard,
                               critInfrastructureToCenRoom,
                               // KMO Portefeuille
                               critKMO_Portefeuille_To_Link_Payment,
                               critKMO_PORT_Payment_To_Link_Payment,
                               critKMO_PORT_PAYMENT_STATUS
                               );

  TEDUDataModule = class(TFVBFFCBaseDataModule)
    ssckData: TFVBFFCSharedConnection;
    dspConnection: TFVBFFCDSPConnection;
    procedure FVBFFCDataModuleFilter(Sender: TObject;
      var FilterApplied: Boolean);
    procedure FVBFFCDataModuleFilterDetail(Sender: TObject; Fields: String;
      Values: Variant; Expression: String);
    procedure FVBFFCDataModuleFilterRecord(Sender: TObject; Fields: String;
      Values: Variant; Expression: String);
    procedure FVBFFCDataModuleSort(Sender: TObject; Fields: String);
    procedure cdsListBeforePost(DataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
  private
    FServerCommands: TDictionary<String, TDBXCommand>;

    function GetListViewDataSet: TClientDataSet;
    function GetRecordViewDataSet: TClientDataSet;
    function GetProviderConnection: TDSProviderConnection;
    { Private declarations }
  protected
    function GetServerCommand(const CommandName: String): TDBXCommand;
    function GetAppServer: Variant; override;
    function GetMainDataModule: TdtmFVBFFCMainClient; override;
    function GetOrderByFromListView: String; virtual;
    function GetFilterFromListView : String; virtual;
  public
    constructor Create(AOwner: TComponent; AAsSelection : Boolean = False); override;
    destructor Destroy; override;
    property ProviderConnection: TDSProviderConnection read GetProviderConnection;
    property ListViewDataSet: TClientDataSet read GetListViewDataSet;
    property RecordViewDataSet: TClientDataSet read GetRecordViewDataSet;
    { Public declarations }
  end;

var
  EDUDataModule: TEDUDataModule;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite, csDataSetRecord,
  {$ENDIF}
  Unit_PPWFrameWorkUtils,
  Data_EDUMainClient, Unit_PPWFrameWorkInterfaces, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, Unit_PPWFrameWorkDataModule;

{$R *.dfm}

constructor TEDUDataModule.Create(AOwner: TComponent; AAsSelection : Boolean = False);
begin
  inherited;
  FServerCommands := TDictionary<String, TDBXCommand>.Create;
end;

destructor TEDUDataModule.Destroy;
var
  Key: String;
begin
  for Key in FServerCommands.Keys do
  begin
    FServerCommands.Items[Key].Free;
  end;
  FServerCommands.Clear;

  FreeAndNil(FServerCommands);

  inherited;
end;

{*****************************************************************************
  This method will be executed when the user applies a filter to the List
  DataSet.  In here we will call the FilterString property on the ListView
  Interface, which will execute the necessary code to build the WHERE
  clasue.

  @Name       TEDUDataModule.FVBFFCDataModuleFilter
  @author     slesage
  @param      Sender         The Object from which the method was invoked.
  @param      FilterApplied  Boolean indicating if the filter was applied or
                             not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TEDUDataModule.FVBFFCDataModuleFilter(Sender: TObject;
  var FilterApplied: Boolean);
var
  aFilterString   : String;
  aOrderByString  : String;
  aApplyFilter    : Boolean;
  aCursorRestorer : IPPWRestorer;
  ServerCommand: TDBXCommand;
begin
  inherited;

  if ( cdsSearchCriteria.State in dsEditModes ) then
  begin
    cdsSearchCriteria.Post;
  end;

  aFilterString  := GetFilterFromListView;
  aApplyFilter   := ( aFilterString <> '' ) or AllowEmptyFilter;
  aOrderByString := GetOrderByFromListView;

  if aApplyFilter then
  begin
    aCursorRestorer := TPPWCursorRestorer.Create( crSQLWait );
    DataSet.Close;

    ServerCommand := GetServerCommand('ApplyFilter');
    ServerCommand.Parameters[0].Value.SetString(DataSet.ProviderName);
    ServerCommand.Parameters[1].Value.SetString(aFilterString);
    ServerCommand.ExecuteUpdate;
//    ssckData.AppServer.ApplyFilter( DataSet.ProviderName, aFilterString );

    if ( aOrderByString <> '' ) then
    begin
      ServerCommand := GetServerCommand('ApplyOrderBy');
      ServerCommand.Parameters[0].Value.SetString(DataSet.ProviderName);
      ServerCommand.Parameters[1].Value.SetString(aOrderByString);
      ServerCommand.ExecuteUpdate;
      //ssckData.AppServer.ApplyOrderBy( DataSet.ProviderName, aOrderByString );
    end;

    DataSet.Open;

    FilterApplied := ( aFilterString <> '' );
  end;
end;

{*****************************************************************************
  This method will be executed when the List DataSet needs to be filtered as
  a Detail in a Master / Detail relation.

  @Name       TEDUDataModule.EdutecDataModuleFilterDetail
  @author     slesage
  @param      Sender       The Object from which the Method is invoked.
  @param      Fields       Comma sepperated list of fields which form the
                           PrimaryKey
  @param      Values       Variant Array which contains the values of all
                           PrimaryKey fields.
  @param      Expression   The Expression that can be added to the WhereClause
                           of the SQL Statement so only one record will be
                           fetched.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TEDUDataModule.FVBFFCDataModuleFilterDetail(Sender: TObject;
  Fields: String; Values: Variant; Expression: String);  
var
  aCursorRestorer : IPPWRestorer;
  ServerCommand: TDBXCommand;
begin
  aCursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  cdsList.Close;
  ServerCommand := GetServerCommand('ApplyDetailFilter');
  ServerCommand.Parameters[0].Value.SetString(cdsRecord.ProviderName);
  ServerCommand.Parameters[1].Value.SetString(Expression);
  ServerCommand.ExecuteUpdate;
  //  ssckData.AppServer.ApplyDetailFilter( cdsList.ProviderName, Expression );
  cdsList.Open;
end;

{*****************************************************************************
  This method will be executed when the user applies a filter to the List
  DataSet.  In here we will call the FilterString property on the ListView
  Interface, which will execute the necessary code to build the WHERE
  clasue.


  @Name       TEDUDataModule.EdutecDataModuleFilter
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TEDUDataModule.FVBFFCDataModuleFilterRecord(Sender: TObject;
  Fields: String; Values: Variant; Expression: String);
var
  aCursorRestorer : IPPWRestorer;
  ServerCommand: TDBXCommand;
begin
  aCursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  cdsRecord.Close;

  ServerCommand := GetServerCommand('ApplyDetailFilter');
  ServerCommand.Parameters[0].Value.SetString(cdsRecord.ProviderName);
  ServerCommand.Parameters[1].Value.SetString(Expression);
  ServerCommand.ExecuteUpdate;
//  ssckData.AppServer.ApplyDetailFilter( cdsRecord.ProviderName, Expression );
  cdsRecord.Open;
end;

{*****************************************************************************
  This method will return a reference to the Project's Main DataModule.

  @Name       TEDUDataModule.GetMainDataModule
  @author     slesage
  @param      None
  @return     Returns a reference to the Project's Main DataModule.
  @Exception  None
  @See        None
******************************************************************************}

function TEDUDataModule.GetMainDataModule: TdtmFVBFFCMainClient;
begin
  Result := dtmEDUMainClient;
end;

{*****************************************************************************
  This event will be triggered when a DataSet needs to be sorted.  It will
  call a method on the Application Server which will Modify the ORDER BY
  clause of the Associated Query.

  @Name       TEDUDataModule.FVBFFCDataModuleSort
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TEDUDataModule.FVBFFCDataModuleSort(Sender: TObject;
  Fields: String);
var
  CursorRestorer : IPPWRestorer;
  aDataSetWasOpen: Boolean;
  OrderString : String;
  ServerCommand: TDBXCommand;
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'FVBFFCDataModuleSort' );
  {$ENDIF}

  if ( Assigned( DataSet ) ) then
  begin
    CursorRestorer := TPPWCursorRestorer.Create(crSQLWait);

    OrderString := GetOrderByFromListView;

    if ( OrderString <> '' ) then
    begin
      aDataSetWasOpen := DataSet.Active;

      DataSet.Close;
      ServerCommand := GetServerCommand('ApplyOrderBy');
      ServerCommand.Parameters[0].Value.SetString(DataSet.ProviderName);
      ServerCommand.Parameters[1].Value.SetString(OrderString);
      ServerCommand.ExecuteUpdate;
      //ssckData.AppServer.ApplyOrderBy( DataSet.ProviderName, OrderString );
      if ( aDataSetWasOpen ) then
      begin
        DataSet.Open;
      end;
    end;
  end;
  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'FVBFFCDataModuleSort' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to get the OrderBy String from the ListView.

  @Name       TdtmPhoenixBase.GetOrderByFromListView
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TEDUDataModule.GetOrderByFromListView: String;
var
  PIFListView : IPPWFrameWorkListView;
  EDUListView : IFVBFFCListView;
begin

  Result := '';

  PIFListView := GetListView;

  if ( Assigned( DataSet ) ) and
     ( Assigned( PIFListView ) ) and
     ( Supports( PIFListView, IFVBFFCListView, EDUListView ) ) then
  begin
    Result  := EDUListView.OrderByString;
  end;
end;

{*****************************************************************************
  This method will be used to get the Filter String from the ListView.

  @Name       TEDUDataModule.GetFilterFromListView
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TEDUDataModule.GetFilterFromListView: String;
var
  PIFListView : IPPWFrameWorkListView;
  EDUListView : IFVBFFCListView;
begin

  Result := '';

  PIFListView := GetListView;

  if ( Assigned( DataSet ) ) and
     ( Assigned( PIFListView ) ) and
     ( Supports( PIFListView, IFVBFFCListView, EDUListView ) ) then
  begin
    Result  := EDUListView.FilterString;
  end;
end;

procedure TEDUDataModule.cdsListBeforePost(DataSet: TDataSet);
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'cdsListBeforePost' );
  {$ENDIF}

  inherited;


  {$IFDEF CODESITE}
  CSSendDatasetRecord( 'Client-Side DataSet before Posting', DataSet );
  csFVBFFCDataModule.ExitMethod( Self, 'cdsListBeforePost' );
  {$ENDIF}
end;

procedure TEDUDataModule.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  //
end;

function TEDUDataModule.GetAppServer: Variant;
begin
  Result := ssckData.ParentConnection.AppServer;
end;

function TEDUDataModule.GetListViewDataSet: TClientDataSet;
begin
  Result := cdsList;
end;

function TEDUDataModule.GetProviderConnection: TDSProviderConnection;
begin
  Result := dspConnection;
end;

function TEDUDataModule.GetRecordViewDataSet: TClientDataSet;
begin
  Result := cdsRecord;
end;

function TEDUDataModule.GetServerCommand(
  const CommandName: String): TDBXCommand;
var
  ServerCommandName: String;
  ServerCommand: TDBXCommand;
begin
  if not Assigned(dspConnection.SQLConnection) then
  begin
    raise Exception.Create('SQLConnection not assigned');
  end;

  if not dspConnection.SQLConnection.Connected then
  begin
    dspConnection.SQLConnection.Open;
  end;

  ServerCommandName := Format('%s.%s', [dspConnection.ServerClassName, CommandName]);
  ServerCommand := nil;

  if not FServerCommands.TryGetValue(ServerCommandName, ServerCommand) then
  begin
    ServerCommand := dspConnection.SQLConnection.DBXConnection.CreateCommand;
    ServerCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    ServerCommand.Text := ServerCommandName;
    ServerCommand.Prepare;

    FServerCommands.Add(ServerCommandName, ServerCommand);
  end;

  Result := ServerCommand;
end;

end.
