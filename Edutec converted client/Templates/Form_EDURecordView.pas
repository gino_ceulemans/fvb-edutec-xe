{*****************************************************************************
  This Unit contains the Base PIF RecordView used in the Edutec Client
  Application.

  @Name       Form_EDURecordView
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_EDURecordView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_FVBFFCBaseRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, Form_FVBFFCMainClient,
  Data_FVBFFCMainClient, Unit_PPWFrameWorkClasses, Menus, cxControls,
  cxEdit, cxSplitter, cxLabel, cxMemo, Unit_PPWFrameWorkController, cxGraphics,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, System.Actions, cxClasses;

type
  TEDURecordView = class(TFVBFFCBaseRecordView)
    procedure FVBFFCRecordViewShow(Sender: TObject);
    procedure FVBFFCRecordViewCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetAutomaticActiveControl;

  protected
    { Protected declarations }
    procedure SetMode(const Value: TPPWFrameWorkRecordViewMode); override;
    function GetMainForm       : TfrmFVBFFCMainClient; override;
    function GetMainDataModule : TdtmFVBFFCMainClient; override;
  public
    { Public declarations }
    procedure UpdateFormCaption; override;
    procedure UpdateControlLayout( Control : TControl; ControlOptions : TPPWFrameWorkControlOptions ); override;

  end;

var
  EDURecordView: TEDURecordView;

implementation

uses
  Form_EDUMainClient,
  Data_EDUDataModule,
  Data_EDUMainClient,
  cxFormats;

{$R *.dfm}

{ TfrmEmptyBaseRecordView }

{*****************************************************************************
  Overridden Property Getter for the MainDataModule property.

  @Name       TEDURecordView.GetMainDataModule
  @author     slesage
  @param      None
  @return     Returns the Main DataModule for the Edutec Client.
  @Exception  None
  @See        None
******************************************************************************}
function TEDURecordView.GetMainDataModule: TdtmFVBFFCMainClient;
begin
  result := dtmEDUMainClient;
end;

{*****************************************************************************
  Overridden Property Getter for the MainForm property.

  @Name       TEDURecordView.GetMainForm
  @author     slesage
  @param      None
  @return     Returns the Main Form for the Edutec Client.
  @Exception  None
  @See        None
******************************************************************************}

function TEDURecordView.GetMainForm: TfrmFVBFFCMainClient;
begin
  Result := frmEDUMainClient;
end;

{*****************************************************************************
  Overridden Property Getter for the MainForm property.

  @Name       TEDURecordView.SetMode
  @author     cheuten
  @param      None
  @return     none
  @Exception  None
  @See        work-around for INSERT/ADD with Master/detail records
******************************************************************************}

procedure TEDURecordView.SetMode(const Value: TPPWFrameWorkRecordViewMode);
begin
  inherited SetMode( Value );
end;

{*****************************************************************************
  Proc to select automatic the first enabled - not readonly - component.
  ( Used in RecordViewShow )

  @Name       TEDURecordView.SetAutomaticActiveControl
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TEDURecordView.SetAutomaticActiveControl;
var
  i : integer;
  aControl, aAcControl : TcxCustomEdit;
begin
  // Overloop alle componenten aanwezig op dit panel
  // en zorg ervoor dat de meest enabled control met de kleinste
  // taborder wordt genomen ...
  aAcControl := nil;

  for i := 0 to sbxMain.ControlCount -1 do
  begin
    if sbxMain.Controls[i] is TcxCustomEdit then
    begin
      aControl := TcxCustomEdit( sbxMain.Controls[i] );
      if ( aControl.Enabled And
         not aControl.ActiveProperties.ReadOnly
         and aControl.CanFocus ) then
      begin
        if not Assigned(aAcControl) then
        begin
          aAcControl := aControl;
        end
        else
        begin
          if aControl.TabOrder < aAcControl.TabOrder then
          begin
            aAcControl := aControl;
          end;
        end;
      end;
    end;
  end;

  if Assigned ( aAcControl ) then
  begin
    self.ActivateControl( aAcControl );
  end
  else if ( cxbtnOK.Visible and cxbtnOK.Enabled ) then
  begin
    self.ActivateControl( cxbtnOK );
  end
  else if ( cxbtnApply.Visible and cxbtnApply.Enabled ) then
  begin
    self.ActivateControl( cxbtnApply );
  end
  else if ( cxbtnCancel.Visible and cxbtnCancel.Enabled ) then
  begin
    self.ActivateControl( cxbtnCancel );
  end;

  try
    self.Activate;
  finally

  end;
end;

procedure TEDURecordView.FVBFFCRecordViewShow(Sender: TObject);
begin
  inherited;

  SetAutomaticActiveControl;

end;

procedure TEDURecordView.UpdateControlLayout(Control: TControl;
  ControlOptions: TPPWFrameWorkControlOptions);
const
  cMySet = [coDataSetReadOnly, coFieldReadOnly, coControlDisabled];
begin
  { Only update the Layout for cxEdit Controls, excluding cxLabel Controls }
  if     ( Control is TcxCustomEdit  ) and
     not ( Control is TcxCustomLabel ) then
  begin
    if ( coControlReadOnly in ControlOptions ) or
       ( coFieldReadOnly   in ControlOptions ) or
       ( coDataSetReadOnly in ControlOptions ) then
    begin
      TcxCustomEdit( Control ).Style.StyleController := ReadOnlyStyleController;

      TcxCustomEdit( Control ).Tabstop := False;

      if ( Control is TcxCustomMemo ) then
      begin
        TcxCustomMemo( Control ).Properties.ReadOnly := True;
        TcxCustomMemo( Control ).Enabled  := PPWController.EnableWhenReadOnly;
      end
      else
      begin
        TcxCustomEdit( Control ).Enabled  := PPWController.EnableWhenReadOnly;
      end;
    end
    else if ( coFieldRequired in ControlOptions ) then
    begin
      TcxCustomEdit( Control ).Style.StyleController := RequiredStyleController;
    end
    else
    begin
      TcxCustomEdit( Control ).Style.StyleController := DefaultEditStyleController;
    end;
  end;
end;

procedure TEDURecordView.UpdateFormCaption;
var
 str : string;
begin
  Inherited UpdateFormCaption;
  str := self.Caption;
  // Nu Self.Caption aanpassen en bewaren en daarna wijziging ook doorsteken ...
  if pos('&&', str) > 0 then
  begin
    self.Caption := stringReplace(str, '&&', '&', [rfReplaceAll]);
  end;
  PnlRecordHeader.Caption := str;
end;

procedure TEDURecordView.FVBFFCRecordViewCreate(Sender: TObject);
begin
  inherited;
  FormatSettings.DateSeparator := '/';
  FormatSettings.ShortDateFormat := 'dd/MM/yyyy';
  cxFormatController.UseDelphiDateTimeFormats := True;
  cxFormatController.NotifyListeners;
end;

end.
