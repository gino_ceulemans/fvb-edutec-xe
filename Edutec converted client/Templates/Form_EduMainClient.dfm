inherited frmEDUMainClient: TfrmEDUMainClient
  Caption = 'EDGARD'
  ClientHeight = 468
  Constraints.MinHeight = 507
  Constraints.MinWidth = 646
  OnActivate = FormActivate
  ExplicitHeight = 507
  PixelsPerInch = 96
  TextHeight = 13
  inherited sbrMain: TFVBFFCStatusBar
    Top = 448
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Fixed = False
        Text = 'sbpnlFiller'
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        Text = 'sbpnlUser'
        Width = 150
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        MinWidth = 0
        Text = 'sbpnlDB'
        Width = 100
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        MinWidth = 0
        Text = 'sbpnlServer'
        Width = 125
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        PanelStyle.Alignment = taCenter
        MinWidth = 0
        Text = 'sbpnlSPID'
        Width = 50
      end>
    ExplicitTop = 448
    ExplicitWidth = 783
  end
  inherited dxBarManager1: TdxBarManager
    Categories.Strings = (
      'Hoofdsystemen'
      'Default'
      'Bestand'
      'Data'
      'General'
      'Window'
      'Menus'
      'Beheerschermen')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True)
    ImageOptions.LargeImages = dtmEDUMainClient.ilGlobalLarge
    Left = 16
    Top = 136
    DockControlHeights = (
      0
      0
      162
      0)
    inherited dxBarManager1Bar1: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrsiFile'
        end
        item
          Visible = True
          ItemName = 'dxbrsiData'
        end
        item
          Visible = True
          ItemName = 'dxbrsiGeneral'
        end
        item
          Visible = True
          ItemName = 'Maintenance'
        end
        item
          Visible = True
        end
        item
          Visible = True
          ItemName = 'dxbrsiWindow'
        end
        item
          Visible = True
          ItemName = 'dxbbAbout'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedTop = 85
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbPersonen'
        end
        item
          Visible = True
          ItemName = 'dxbbOrganisaties'
        end
        item
          Visible = True
          ItemName = 'dxbbDxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxbbDxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxbbOpleidingsaanbod'
        end
        item
          Visible = True
          ItemName = 'dxbbSessies'
        end
        item
          Visible = True
          ItemName = 'dxbbSessieWizardLeeg'
        end
        item
          Visible = True
          ItemName = 'dxbbTbPrefacturatie'
        end
        item
          Visible = True
          ItemName = 'dxbbDatabaseWizardEmpty'
        end
        item
          Visible = True
          ItemName = 'dxbbDxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButtonImportExcel'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbDxBarLargeButton3'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedTop = 28
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedTop = 134
    end
    object dxbbShowProgProgram: TdxBarButton [4]
      Action = dtmEDUMainClient.acShowProgProgram
      Category = 0
    end
    object dxbbShowSesSession: TdxBarButton [5]
      Action = dtmEDUMainClient.acShowSesSession
      Category = 0
    end
    object dxbbShowDocInstructor: TdxBarButton [6]
      Action = dtmEDUMainClient.acShowDocInstructor
      Category = 0
    end
    object dxbbShowCenInfrastructure: TdxBarButton [7]
      Action = dtmEDUMainClient.acShowCenInfrastructure
      Category = 0
    end
    object dxbbOrgTypes: TdxBarButton [8]
      Action = dtmEDUMainClient.acShowOrgType
      Category = 0
    end
    object dxbbOrganisations: TdxBarButton [9]
      Action = dtmEDUMainClient.acShowOrganisations
      Category = 0
    end
    object dxbbPersons: TdxBarButton [10]
      Action = dtmEDUMainClient.acShowPersons
      Category = 0
    end
    object dxbbSessieWizardEmpty: TdxBarButton [11]
      Action = dtmEDUMainClient.acSessionWizardEmpty
      Category = 0
    end
    object dxBarButton1: TdxBarButton [12]
      Caption = 'Import overzicht'
      Category = 0
      Enabled = False
      Hint = 'Import overzicht|Import overzicht'
      Visible = ivAlways
      ImageIndex = 33
    end
    object dxbbDxBarButton1: TdxBarButton [13]
      Caption = 'New Item'
      Category = 1
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxbbPersonen: TdxBarLargeButton [14]
      Action = dtmEDUMainClient.acShowPersons
      Category = 1
      AutoGrayScale = False
    end
    object dxbbOrganisaties: TdxBarLargeButton [15]
      Action = dtmEDUMainClient.acShowOrganisations
      Category = 1
      AutoGrayScale = False
    end
    object dxbbSessieWizardLeeg: TdxBarLargeButton [16]
      Caption = 'Sessie Wizard'
      Category = 1
      Enabled = False
      Visible = ivAlways
      AutoGrayScale = False
      LargeImageIndex = 22
    end
    object dxbsiDxBarSubItem1: TdxBarSubItem [17]
      Caption = 'New Item'
      Category = 1
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxbbOpleidingsaanbod: TdxBarLargeButton [18]
      Action = dtmEDUMainClient.acShowProgProgram
      Category = 1
      AutoGrayScale = False
    end
    object dxbbSessies: TdxBarLargeButton [19]
      Action = dtmEDUMainClient.acShowSesSession
      Category = 1
      AutoGrayScale = False
    end
    object dxbsiDxBarSubItem2: TdxBarSubItem [20]
      Caption = 'Sessie Wizard'
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbSessieWizardEmpty1'
        end>
    end
    object dxbbSessieWizardEmpty1: TdxBarButton [21]
      Caption = 'Sessie Wizard'
      Category = 1
      Enabled = False
      Visible = ivAlways
      ImageIndex = 22
    end
    object dxbbDxBarLargeButton1: TdxBarLargeButton [22]
      Action = dtmEDUMainClient.acShowCenInfrastructure
      Category = 1
      AutoGrayScale = False
    end
    object dxbbDxBarLargeButton2: TdxBarLargeButton [23]
      Action = dtmEDUMainClient.acShowDocInstructor
      Category = 1
      AutoGrayScale = False
    end
    object dxbbDxBarLargeButton3: TdxBarLargeButton [24]
      Action = dtmEDUMainClient.FileExit1
      Align = iaRight
      Category = 1
      AutoGrayScale = False
    end
    object dxbbDatabaseWizardEmpty1: TdxBarButton [25]
      Action = dtmEDUMainClient.acDatabaseWizard
      Category = 1
    end
    object dxbsiDxBarSubItem3: TdxBarSubItem [26]
      Caption = 'New Item'
      Category = 1
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxbsiDxBarSubItem4: TdxBarSubItem [27]
      Caption = 'Database Wizard'
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbDatabaseWizardEmpty1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbShowQuerygroup'
        end
        item
          Visible = True
          ItemName = 'dxbbShowQuery'
        end>
    end
    object dxbbDxBarButton3: TdxBarButton [28]
      Caption = 'New Item'
      Category = 1
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxbbDxBarButton4: TdxBarButton [29]
      Caption = 'Database Wizard'
      Category = 1
      Visible = ivAlways
      ImageIndex = 21
    end
    object dxbbDatabaseWizardEmpty: TdxBarLargeButton [30]
      Action = dtmEDUMainClient.acDatabaseWizard
      Category = 1
      AutoGrayScale = False
    end
    object dxbbDxBarButton2: TdxBarButton [31]
      Action = dtmEDUMainClient.acShowQueryGroup
      Category = 1
    end
    object dxbbShowQuery: TdxBarButton [32]
      Action = dtmEDUMainClient.acShowQuery
      Category = 1
    end
    object dxbsiShowQueryGroup: TdxBarSubItem [33]
      Action = dtmEDUMainClient.acShowQueryGroup
      Category = 1
      ItemLinks = <>
    end
    object dxbbShowQuerygroup: TdxBarButton [34]
      Action = dtmEDUMainClient.acShowQueryGroup
      Category = 1
    end
    object dxbbAbout: TdxBarButton [35]
      Caption = '&About'
      Category = 1
      Hint = 'About'
      Visible = ivAlways
      OnClick = dxbbAboutClick
    end
    object dxbbDxBarButton5: TdxBarButton [36]
      Align = iaRight
      Caption = 'New Item'
      Category = 1
      Description = 'Prefacturatie'
      Hint = 'New Item'
      Visible = ivAlways
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C006000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFC9762BA24F22A24F22A24F22A24F22A2
        4F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22
        A24F22A24F22A24F22A24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFDFEFE
        FDFEFEFDFEFEFDFDFDFCFBFBFAF8F8F7F6F6F5F3F3F2F1F1F0EFEFEEECECEBEA
        EAE9E8E8E7E2E2E2DDDDDDD7D7D7D2D2D2A24F22FF00FFFF00FFFF00FFFF00FF
        C9762BFEFEFD5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B
        5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5AD7D7D7A24F22FF00FFFF
        00FFFF00FFFF00FFC9762BFEFEFD5A7B5AF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF
        9CBDC6F7FFFFF7FFFFF7FFFFF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF5A7B5ADDDD
        DDA24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFD5A7B5A84A58484A5849C
        BDC684A58484A5849CBDC684A58484A58484A58484A58484A5849CBDC684A584
        84A5845A7B5AE2E2E2A24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFD5A7B
        5AF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF9CBDC6F7FFFFF7FFFFF7FFFFF7FFFFF7
        FFFF9CBDC6F7FFFFF7FFFF5A7B5AE8E8E7A24F22FF00FFFF00FFFF00FFFF00FF
        C9762BFEFEFD5A7B5A84A58484A5849CBDC684A58484A5849CBDC684A58484A5
        8484A58484A58484A5849CBDC684A58484A5845A7B5AEAEAE9A24F22FF00FFFF
        00FFFF00FFFF00FFC9762BFEFEFD5A7B5AF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF
        9CBDC6F7FFFFF7FFFFF7FFFFF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF5A7B5AECEC
        EBA24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFD5A7B5A84A58484A5849C
        BDC684A58484A5849CBDC684A58484A58484A58484A58484A5849CBDC684A584
        84A5845A7B5AF0F0EFA24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFD5A7B
        5AF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF9CBDC6F7FFFFF7FFFFF7FFFFF7FFFFF7
        FFFF9CBDC6F7FFFFF7FFFF5A7B5AF2F2F1A24F22FF00FFFF00FFFF00FFFF00FF
        C9762BFEFEFD5A7B5A84A58484A5849CBDC684A58484A5849CBDC684A58484A5
        8484A58484A58484A5849CBDC684A58484A5845A7B5AF5F5F4A24F22FF00FFFF
        00FFFF00FFFF00FFC9762BFEFEFD5A7B5AF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF
        9CBDC6F7FFFFF7FFFFF7FFFFF7FFFFF7FFFF9CBDC6F7FFFFF7FFFF5A7B5AF7F7
        F6A24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFD5A7B5A5A7B5A5A7B5A5A
        7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A
        5A7B5A5A7B5AF9F9F8A24F22FF00FFFF00FFFF00FFFF00FFC9762BFEFEFDFEFE
        FDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFE
        FEFDFEFEFDFEFEFDFEFEFDFEFEFDFCFCFBA24F22FF00FFFF00FFFF00FFFF00FF
        C9762BFEFEFDFEFEFDFEFEFDFEFEFD5A7B5A5A7B5A5A7B5A5A7B5A5A7B5A5A7B
        5A5A7B5A5A7B5A5A7B5A5A7B5AFEFEFDFEFEFDFEFEFDFEFEFDA24F22FF00FFFF
        00FFFF00FFFF00FFC9762BFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFD
        FEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FDA24F22FF00FFFF00FFFF00FFFF00FFC9762BA24F22A24F22A24F22A24F22A2
        4F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22A24F22
        A24F22A24F22A24F22A24F22FF00FFFF00FFFF00FFFF00FFC9762BD06F01D06F
        01D06F01D06F01D06F01D06F01D06F01D06F01D06F01D06F01D06F01D06F01D0
        6F01D06F01D06F01D06F01D06F01D06F01A24F22FF00FFFF00FFFF00FFFF00FF
        CE6300ED9733ED9733ED9733ED9733ED9733ED9733ED9733ED9733ED9733ED97
        33ED9733ED9733F6CA9AF3AD61F6CA9AF3AD61306DF97F7488DA7B0DFF00FFFF
        00FFFF00FFFF00FFFF00FFDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0D
        DA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B0DDA7B
        0DFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
    object dxbbTbPrefacturatie: TdxBarLargeButton [37]
      Action = dtmEDUMainClient.acInvoicing
      Category = 1
      AutoGrayScale = False
    end
    object dxbbMnuPrefacturatie: TdxBarButton [38]
      Action = dtmEDUMainClient.acInvoicing
      Category = 1
    end
    object dxbbStartNewBookYear: TdxBarButton [39]
      Action = dtmEDUMainClient.acInputLastDocNumber
      Category = 1
    end
    object dxbbDxBarLargeButton4: TdxBarLargeButton [40]
      Action = dtmEDUMainClient.acShowImport
      Category = 1
      AutoGrayScale = False
    end
    object dxBarLargeButton1: TdxBarLargeButton [41]
      Caption = 'Fakturatie'
      Category = 1
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton [42]
      Caption = 'Fakturatie'
      Category = 1
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton [43]
      Action = dtmEDUMainClient.acShowImport
      Category = 1
    end
    object dxBarSubItem1: TdxBarSubItem [44]
      Caption = 'KMO Portefeuille'
      Category = 1
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarButton5'
        end>
    end
    object dxBarButton4: TdxBarButton [45]
      Action = dtmEDUMainClient.acShowKMOPortefeuille
      Category = 1
    end
    object dxBarLargeButton2: TdxBarLargeButton [46]
      Action = dtmEDUMainClient.acShowKMOPortefeuille
      Category = 1
      AutoGrayScale = False
      SyncImageIndex = False
      ImageIndex = 35
    end
    object dxBarLargeButton3: TdxBarLargeButton [47]
      Category = 1
      Visible = ivAlways
    end
    object dxBarButton5: TdxBarButton [48]
      Action = dtmEDUMainClient.acShowKMO_PORT_Payment
      Category = 1
    end
    object dxBarButton6: TdxBarButton [49]
      Caption = 'New Item'
      Category = 1
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarLargeButtonImportExcel: TdxBarLargeButton [50]
      Action = dtmEDUMainClient.acImportExcel
      Category = 1
      AutoGrayScale = False
    end
    object dxbbFileExit: TdxBarButton [51]
      Action = dtmEDUMainClient.FileExit1
      Category = 2
    end
    inherited dxbbEdit: TdxBarButton
      Category = 3
    end
    inherited dxbbView: TdxBarButton
      Category = 3
    end
    inherited dxbbAdd: TdxBarButton
      Category = 3
    end
    inherited dxbbDelete: TdxBarButton
      Category = 3
    end
    inherited dxbbRefreshData: TdxBarButton
      Category = 3
    end
    inherited dxbbCustomiseColumns: TdxBarButton
      Category = 3
    end
    inherited dxbbPrintList: TdxBarButton
      Category = 3
    end
    inherited dxbbExportXLS: TdxBarButton
      Category = 3
    end
    inherited dxbbExportHTML: TdxBarButton
      Category = 3
    end
    inherited dxbbExportXML: TdxBarButton
      Category = 3
    end
    inherited dxbbWindowClose: TdxBarButton
      Category = 5
    end
    inherited dxbbTileHorizontally: TdxBarButton
      Category = 5
    end
    inherited dxbbTileVertically: TdxBarButton
      Category = 5
    end
    inherited dxbbCascade: TdxBarButton
      Category = 5
    end
    inherited dxbbArrangeAll: TdxBarButton
      Category = 5
    end
    inherited dxbbMinimizeAll: TdxBarButton
      Category = 5
    end
    inherited dxBarListWindows: TdxBarListItem
      Category = 5
    end
    inherited dxbrsiFile: TdxBarSubItem
      Category = 6
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbFileExit'
        end>
    end
    inherited dxbrsiData: TdxBarSubItem
      Category = 6
    end
    inherited dxbrsiGeneral: TdxBarSubItem
      Category = 6
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbShowProgProgram'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbShowSesSession'
        end
        item
          Visible = True
          ItemName = 'dxbbStartNewBookYear'
        end
        item
          Visible = True
          ItemName = 'dxbbMnuPrefacturatie'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbOrganisations'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbPersons'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbShowCenInfrastructure'
        end
        item
          Visible = True
          ItemName = 'dxbbShowDocInstructor'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbsiDxBarSubItem4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
    end
    inherited dxbrsiWindow: TdxBarSubItem
      Category = 6
    end
    inherited Maintenance: TdxBarSubItem
      Category = 6
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbbShowLanguages'
        end
        item
          Visible = True
          ItemName = 'dxbbShowCountries'
        end
        item
          Visible = True
          ItemName = 'dxbbShowCountryParts'
        end
        item
          Visible = True
          ItemName = 'dxbbShowProvinces'
        end
        item
          Visible = True
          ItemName = 'dxbbShowPostalCodes'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbStatus'
        end
        item
          Visible = True
          ItemName = 'dxbbKMOPORTStatus'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbShowDiplomas'
        end
        item
          Visible = True
          ItemName = 'dxbbShowSessionGroups'
        end
        item
          Visible = True
          ItemName = 'dxbbShowRoles'
        end
        item
          Visible = True
          ItemName = 'dxbbShowSchoolyears'
        end
        item
          Visible = True
          ItemName = 'dxbbCooperation'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbShowDisciplines'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbOrgTypes'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbbShowUsers'
        end
        item
          Visible = True
          ItemName = 'dxbbShowProfiles'
        end>
    end
    object dxbbShowLanguages: TdxBarButton
      Action = dtmEDUMainClient.acShowLanguages
      Category = 7
    end
    object dxbbShowCountries: TdxBarButton
      Action = dtmEDUMainClient.acShowCountry
      Category = 7
    end
    object dxbbShowCountryParts: TdxBarButton
      Action = dtmEDUMainClient.acShowCountryPart
      Category = 7
    end
    object dxbbShowProvinces: TdxBarButton
      Action = dtmEDUMainClient.acShowProvince
      Category = 7
    end
    object dxbbShowPostalCodes: TdxBarButton
      Action = dtmEDUMainClient.acShowPostalCode
      Category = 7
    end
    object dxbbShowDiplomas: TdxBarButton
      Action = dtmEDUMainClient.acShowDiploma
      Category = 7
    end
    object dxbbShowSessionGroups: TdxBarButton
      Action = dtmEDUMainClient.acShowSessionGroup
      Category = 7
    end
    object dxbbShowDisciplines: TdxBarButton
      Action = dtmEDUMainClient.acShowDisciplines
      Category = 7
    end
    object dxbbShowSchoolyears: TdxBarButton
      Action = dtmEDUMainClient.acShowSchoolYear
      Category = 7
    end
    object dxbbShowUsers: TdxBarButton
      Action = dtmEDUMainClient.acShowUsers
      Category = 7
    end
    object dxbbShowProfiles: TdxBarButton
      Action = dtmEDUMainClient.acShowProfiles
      Category = 7
    end
    object dxbbShowRoles: TdxBarButton
      Action = dtmEDUMainClient.acShowRole
      Category = 7
    end
    object dxbbStatus: TdxBarButton
      Action = dtmEDUMainClient.acShowStatus
      Category = 7
    end
    object dxbbCooperation: TdxBarButton
      Action = dtmEDUMainClient.acShowCooperation
      Category = 7
    end
    object dxbbKMOPORTStatus: TdxBarButton
      Action = dtmEDUMainClient.acShowKMO_PORT_Status
      Category = 7
    end
  end
end
