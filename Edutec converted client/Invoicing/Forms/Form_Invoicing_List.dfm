inherited frmInvoicing_List: TfrmInvoicing_List
  Left = 337
  Top = 163
  Width = 1024
  Height = 620
  Caption = 'Factuurlijnen'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 552
    Width = 1016
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 886
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 998
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 1008
    Height = 544
    inherited cxgrdList: TcxGrid
      Top = 166
      Width = 1008
      Height = 378
      inherited cxgrdtblvList: TcxGridDBTableView
        OptionsData.Editing = True
        OptionsSelection.CellSelect = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Options.Editing = False
          Options.Focusing = False
          Width = 89
        end
        object cxgrdtblvListF_NAAM: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAAM'
          Options.Editing = False
          Options.Focusing = False
          Width = 240
        end
        object cxgrdtblvListF_STRAAT: TcxGridDBColumn
          DataBinding.FieldName = 'F_STRAAT'
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_POSTCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTCODE'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_WOONPLAATS: TcxGridDBColumn
          DataBinding.FieldName = 'F_WOONPLAATS'
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_FACTUUR_NR: TcxGridDBColumn
          Caption = 'Factuurnr'
          DataBinding.FieldName = 'F_FACTUUR_NR'
          Options.Editing = False
          Options.Focusing = False
          Width = 65
        end
        object cxgrdtblvListF_DATUM: TcxGridDBColumn
          Caption = 'Datum'
          DataBinding.FieldName = 'F_DATUM'
          RepositoryItem = dtmEDUMainClient.cxeriDate
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_KLANT_NR: TcxGridDBColumn
          Caption = 'Klantnr'
          DataBinding.FieldName = 'F_KLANT_NR'
          Options.Editing = False
          Options.Focusing = False
          Width = 90
        end
        object cxgrdtblvListF_VERVALDATUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_VERVALDATUM'
          RepositoryItem = dtmEDUMainClient.cxeriDate
          Options.Editing = False
          Options.Focusing = False
          Width = 79
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_REFERENTIE: TcxGridDBColumn
          DataBinding.FieldName = 'F_REFERENTIE'
          Options.Editing = False
          Options.Focusing = False
          Width = 80
        end
        object cxgrdtblvListF_CODE: TcxGridDBColumn
          Caption = 'Nummer opleiding'
          DataBinding.FieldName = 'F_CODE'
          Options.Editing = False
          Options.Focusing = False
          SortIndex = 1
          SortOrder = soAscending
          Width = 80
        end
        object cxgrdtblvListF_COMMENT: TcxGridDBColumn
          Caption = 'Cursus'
          DataBinding.FieldName = 'F_COMMENT'
          Options.Editing = False
          Options.Focusing = False
          Width = 240
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          Caption = 'Datum opleiding'
          DataBinding.FieldName = 'F_START_DATE'
          Options.Editing = False
          Options.Focusing = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 76
        end
        object cxgrdtblvListF_NUMBER_PARTICIPANTS: TcxGridDBColumn
          DataBinding.FieldName = 'F_NUMBER_PARTICIPANTS'
          PropertiesClassName = 'TcxCalcEditProperties'
          Options.Editing = False
          Options.Focusing = False
          Width = 100
        end
        object cxgrdtblvListF_AANTAL_UREN: TcxGridDBColumn
          DataBinding.FieldName = 'F_AANTAL_UREN'
          PropertiesClassName = 'TcxCalcEditProperties'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_PRIJS_PER_UUR: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRIJS_PER_UUR'
          PropertiesClassName = 'TcxCalcEditProperties'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_TOTAAL: TcxGridDBColumn
          DataBinding.FieldName = 'F_TOTAAL'
          PropertiesClassName = 'TcxCalcEditProperties'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_INVOICE_AMOUNT: TcxGridDBColumn
          DataBinding.FieldName = 'F_INVOICE_AMOUNT'
          PropertiesClassName = 'TcxCalcEditProperties'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_REKENINGNR: TcxGridDBColumn
          DataBinding.FieldName = 'F_REKENINGNR'
          Options.Editing = False
          Options.Focusing = False
          Width = 60
        end
        object cxgrdtblvListF_ANA_SLEUTEL: TcxGridDBColumn
          Caption = 'Analytische code'
          DataBinding.FieldName = 'F_ANA_SLEUTEL'
          Options.Editing = False
          Options.Focusing = False
          Width = 105
        end
        object cxgrdtblvListF_TAALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_TAALCODE'
          Options.Editing = False
          Options.Focusing = False
          Width = 60
        end
        object cxgrdtblvListF_LANDCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANDCODE'
          Options.Editing = False
          Options.Focusing = False
          Width = 60
        end
        object cxgrdtblvListF_VAT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_VAT_NR'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_SYNERGY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SYNERGY_ID'
        end
      end
      object cxgrdtblvExport: TcxGridDBTableView [1]
        NavigatorButtons.ConfirmDelete = False
        DataController.DataModeController.GridMode = True
        DataController.DataSource = srcMain
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Navigator = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderAutoHeight = True
        OptionsView.Indicator = True
        Styles.StyleSheet = dtmEDUMainClient.GridTableViewStyleSheetDevExpress
        object cxgrdtblvExportF_NAAM: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAAM'
          Width = 240
        end
        object cxgrdtblvExportF_STRAAT: TcxGridDBColumn
          DataBinding.FieldName = 'F_STRAAT'
          Width = 200
        end
        object cxgrdtblvExportF_POSTCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTCODE'
          Width = 64
        end
        object cxgrdtblvExportF_WOONPLAATS: TcxGridDBColumn
          DataBinding.FieldName = 'F_WOONPLAATS'
          Width = 200
        end
        object cxgrdtblvExportF_FACTUUR_NR: TcxGridDBColumn
          Caption = 'Factuurnr'
          DataBinding.FieldName = 'F_FACTUUR_NR'
          Width = 65
        end
        object cxgrdtblvExportF_DATUM: TcxGridDBColumn
          Caption = 'Datum'
          DataBinding.FieldName = 'F_DATUM'
          RepositoryItem = dtmEDUMainClient.cxeriDate
        end
        object cxgrdtblvExportF_KLANT_NR: TcxGridDBColumn
          Caption = 'Klantnr'
          DataBinding.FieldName = 'F_KLANT_NR'
          Width = 90
        end
        object cxgrdtblvExportF_VERVALDATUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_VERVALDATUM'
          RepositoryItem = dtmEDUMainClient.cxeriDate
          Width = 80
        end
        object cxgrdtblvExportF_REFERENTIE: TcxGridDBColumn
          DataBinding.FieldName = 'F_REFERENTIE'
          Width = 80
        end
        object cxgrdtblvExportF_CODE: TcxGridDBColumn
          Caption = 'Nummeropleiding'
          DataBinding.FieldName = 'F_CODE'
          Width = 140
        end
        object cxgrdtblvExportF_COMMENT: TcxGridDBColumn
          Caption = 'Cursus'
          DataBinding.FieldName = 'F_COMMENT'
          Width = 240
        end
        object cxgrdtblvExportF_START_DATE: TcxGridDBColumn
          Caption = 'Datumopleiding'
          DataBinding.FieldName = 'F_START_DATE'
          RepositoryItem = dtmEDUMainClient.cxeriDate
          SortIndex = 0
          SortOrder = soAscending
        end
        object cxgrdtblvExportF_NUMBER_PARTICIPANTS: TcxGridDBColumn
          Caption = 'Aantaldeelnemers'
          DataBinding.FieldName = 'F_NUMBER_PARTICIPANTS'
          PropertiesClassName = 'TcxCalcEditProperties'
          Width = 115
        end
        object cxgrdtblvExportF_AANTAL_UREN: TcxGridDBColumn
          Caption = 'Aantaluren'
          DataBinding.FieldName = 'F_AANTAL_UREN'
          PropertiesClassName = 'TcxCalcEditProperties'
          Width = 80
        end
        object cxgrdtblvExportF_PRIJS_PER_UUR: TcxGridDBColumn
          Caption = 'Prijsperuur'
          DataBinding.FieldName = 'F_PRIJS_PER_UUR'
          PropertiesClassName = 'TcxCalcEditProperties'
          Width = 80
        end
        object cxgrdtblvExportF_TOTAAL: TcxGridDBColumn
          DataBinding.FieldName = 'F_TOTAAL'
          PropertiesClassName = 'TcxCalcEditProperties'
          Width = 80
        end
        object cxgrdtblvExportF_INVOICE_AMOUNT: TcxGridDBColumn
          Caption = 'Algemeentotaal'
          DataBinding.FieldName = 'F_INVOICE_AMOUNT'
          PropertiesClassName = 'TcxCalcEditProperties'
          Width = 105
        end
        object cxgrdtblvExportF_REKENINGNR: TcxGridDBColumn
          DataBinding.FieldName = 'F_REKENINGNR'
          Width = 90
        end
        object cxgrdtblvExportF_ANA_SLEUTEL: TcxGridDBColumn
          Caption = 'Analytischecode'
          DataBinding.FieldName = 'F_ANA_SLEUTEL'
          Width = 105
        end
        object cxgrdtblvExportF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
          SortIndex = 1
          SortOrder = soAscending
        end
        object cxgrdtblvExportF_INVOICE_TYPE: TcxGridDBColumn
          DataBinding.FieldName = 'F_INVOICE_TYPE'
          Visible = False
          SortIndex = 2
          SortOrder = soAscending
        end
        object cxgrdtblvExportF_TAALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_TAALCODE'
          Width = 80
        end
        object cxgrdtblvExportF_LANDCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANDCODE'
          Width = 80
        end
        object cxgrdtblvExportF_VAT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_VAT_NR'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvExportF_SYNERGY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SYNERGY_ID'
        end
      end
      object cxgrdlvlExport: TcxGridLevel
        GridView = cxgrdtblvExport
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 141
      Width = 1008
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 137
      Width = 1008
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 1008
      Height = 137
      ExpandedHeight = 137
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 104
        Width = 1006
        Height = 32
        TabOrder = 5
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 924
          Top = 0
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 1012
        end
        object cxBtnRefresh: TFVBFFCButton
          Left = 840
          Top = 0
          Width = 73
          Height = 25
          Action = acRefresh
          Anchors = [akTop, akRight]
          TabOrder = 2
        end
      end
      object cxlblF_SESSION_ID1: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInvoicing_Record.F_SESSION_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessie ID'
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object cxlblF_START_DATE1: TFVBFFCLabel
        Left = 280
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInvoicing_Record.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Start Datum'
        FocusControl = F_START_DATE1
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object F_START_DATE1: TFVBFFCDBDateEdit
        Left = 376
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInvoicing_Record.F_START_DATE'
        DataBinding.DataField = 'F_START_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 1
        Width = 137
      end
      object cxlblF_END_DATE: TFVBFFCLabel
        Left = 552
        Top = 32
        HelpType = htKeyword
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Eind Datum'
        FocusControl = F_END_DATE1
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object F_END_DATE1: TFVBFFCDBDateEdit
        Left = 648
        Top = 32
        HelpType = htKeyword
        DataBinding.DataField = 'F_END_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 2
        Width = 137
      end
      object F_STATUS: TFVBFFCDBComboBox
        Left = 376
        Top = 72
        DataBinding.DataField = 'F_STATUS'
        DataBinding.DataSource = srcSearchCriteria
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'Geattesteerd'
          'Gefaktureerd')
        TabOrder = 4
        Width = 137
      end
      object cxlblF_STATUS: TFVBFFCLabel
        Left = 280
        Top = 72
        HelpType = htKeyword
        HelpKeyword = 'frmInvoicing_Record.F_SESSION_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        FocusControl = F_STATUS
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object F_SESSION_ID1: TFVBFFCDBTextEdit
        Left = 120
        Top = 32
        DataBinding.DataField = 'F_SESSION_ID'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 0
        Width = 121
      end
      object cxlblF_CODE1: TFVBFFCLabel
        Left = 8
        Top = 72
        HelpType = htKeyword
        HelpKeyword = 'frmInvoicing_Record.F_SESSION_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessie referentie'
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object F_CODE1: TFVBFFCDBTextEdit
        Left = 120
        Top = 72
        DataBinding.DataField = 'F_CODE'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 3
        Width = 121
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 1016
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 548
    Width = 1016
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 1012
    Height = 544
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 544
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmInvoicing.cdsList
    Left = 48
    Top = 312
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxbbAdd: TdxBarButton
      Enabled = False
    end
    object dxbbCollect: TdxBarButton
      Caption = 'Collect Data'
      Category = 0
      Hint = 'Collect Data'
      Visible = ivAlways
      OnClick = dxbbCollectClick
    end
    object dxbbExport: TdxBarButton
      Action = acGenerateInvoicingData
      Category = 0
      Hint = 'Generate Invoicing Data'
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbCollect
        Visible = True
      end
      item
        Item = dxbbExport
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
    OnPopup = dxbpmnGridPopup
  end
  object alInvoicing: TFVBFFCActionList
    Left = 296
    Top = 294
    object acGenerateInvoicingData: TAction
      Caption = 'Generate Invoicing Data'
      OnExecute = acGenerateInvoicingDataExecute
    end
    object acRefresh: TAction
      Caption = 'Refresh'
      OnExecute = acRefreshExecute
    end
  end
end
