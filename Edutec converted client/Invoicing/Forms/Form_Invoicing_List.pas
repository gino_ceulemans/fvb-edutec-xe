{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Invoicing_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2008   ivdbossche           Added cxgrdtblvExportF_VAT_NR column (Mantis 2471)
  25/02/2008   ivdbossche           Added Refresh button.
  25/09/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Invoicing_List;

interface

uses
  Form_EDUListView, Unit_FVBFFCDevExpress, Unit_PPWFrameWorkClasses,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, Menus,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, cxCheckBox, cxDBEdit, cxDropDownEdit, cxImageComboBox,
  cxCalendar, cxTextEdit, cxMaskEdit, cxSpinEdit, cxContainer, cxLabel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGrid, StdCtrls, cxButtons, Classes,
  Controls, ExtCtrls, ActnList, Unit_FVBFFCComponents, Forms, cxCalc;

type
  TfrmInvoicing_List = class(TEDUListView)
    cxgrdtblvListF_NAAM: TcxGridDBColumn;
    cxgrdtblvListF_STRAAT: TcxGridDBColumn;
    cxgrdtblvListF_POSTCODE: TcxGridDBColumn;
    cxgrdtblvListF_WOONPLAATS: TcxGridDBColumn;
    cxgrdtblvListF_FACTUUR_NR: TcxGridDBColumn;
    cxgrdtblvListF_DATUM: TcxGridDBColumn;
    cxgrdtblvListF_KLANT_NR: TcxGridDBColumn;
    cxgrdtblvListF_VERVALDATUM: TcxGridDBColumn;
    cxgrdtblvListF_CODE: TcxGridDBColumn;
    cxgrdtblvListF_REFERENTIE: TcxGridDBColumn;
    cxgrdtblvListF_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_INVOICE_AMOUNT: TcxGridDBColumn;
    dxbbCollect: TdxBarButton;
    dxbbExport: TdxBarButton;
    cxlblF_SESSION_ID1: TFVBFFCLabel;
    cxlblF_START_DATE1: TFVBFFCLabel;
    F_START_DATE1: TFVBFFCDBDateEdit;
    cxgrdtblvListF_ANA_SLEUTEL: TcxGridDBColumn;
    cxgrdlvlExport: TcxGridLevel;
    cxgrdtblvExport: TcxGridDBTableView;
    cxgrdtblvExportF_NAAM: TcxGridDBColumn;
    cxgrdtblvExportF_STRAAT: TcxGridDBColumn;
    cxgrdtblvExportF_POSTCODE: TcxGridDBColumn;
    cxgrdtblvExportF_WOONPLAATS: TcxGridDBColumn;
    cxgrdtblvExportF_FACTUUR_NR: TcxGridDBColumn;
    cxgrdtblvExportF_DATUM: TcxGridDBColumn;
    cxgrdtblvExportF_KLANT_NR: TcxGridDBColumn;
    cxgrdtblvExportF_VERVALDATUM: TcxGridDBColumn;
    cxgrdtblvExportF_CODE: TcxGridDBColumn;
    cxgrdtblvExportF_START_DATE: TcxGridDBColumn;
    cxgrdtblvExportF_INVOICE_AMOUNT: TcxGridDBColumn;
    cxgrdtblvExportF_COMMENT: TcxGridDBColumn;
    cxgrdtblvExportF_ANA_SLEUTEL: TcxGridDBColumn;
    cxgrdtblvExportF_REFERENTIE: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    alInvoicing: TFVBFFCActionList;
    acGenerateInvoicingData: TAction;
    cxlblF_END_DATE: TFVBFFCLabel;
    F_END_DATE1: TFVBFFCDBDateEdit;
    F_STATUS: TFVBFFCDBComboBox;
    cxlblF_STATUS: TFVBFFCLabel;
    F_SESSION_ID1: TFVBFFCDBTextEdit;
    cxlblF_CODE1: TFVBFFCLabel;
    F_CODE1: TFVBFFCDBTextEdit;
    cxgrdtblvListF_NUMBER_PARTICIPANTS: TcxGridDBColumn;
    cxgrdtblvListF_AANTAL_UREN: TcxGridDBColumn;
    cxgrdtblvListF_PRIJS_PER_UUR: TcxGridDBColumn;
    cxgrdtblvListF_TOTAAL: TcxGridDBColumn;
    cxgrdtblvExportF_NUMBER_PARTICIPANTS: TcxGridDBColumn;
    cxgrdtblvExportF_AANTAL_UREN: TcxGridDBColumn;
    cxgrdtblvExportF_PRIJS_PER_UUR: TcxGridDBColumn;
    cxgrdtblvExportF_TOTAAL: TcxGridDBColumn;
    cxgrdtblvExportF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvExportF_INVOICE_TYPE: TcxGridDBColumn;
    cxgrdtblvListF_REKENINGNR: TcxGridDBColumn;
    cxgrdtblvListF_TAALCODE: TcxGridDBColumn;
    cxgrdtblvListF_LANDCODE: TcxGridDBColumn;
    cxgrdtblvExportF_REKENINGNR: TcxGridDBColumn;
    cxgrdtblvExportF_TAALCODE: TcxGridDBColumn;
    cxgrdtblvExportF_LANDCODE: TcxGridDBColumn;
    cxBtnRefresh: TFVBFFCButton;
    acRefresh: TAction;
    cxgrdtblvListF_VAT_NR: TcxGridDBColumn;
    cxgrdtblvExportF_VAT_NR: TcxGridDBColumn;
    cxgrdtblvListF_SYNERGY_ID: TcxGridDBColumn;
    cxgrdtblvExportF_SYNERGY_ID: TcxGridDBColumn;
    procedure dxbbCollectClick(Sender: TObject);
    procedure dxbpmnGridPopup(Sender: TObject);
    procedure acGenerateInvoicingDataExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
  private
    { Store filter properties here.  These values will be used to mark records in database using the MarkExported procedure }
    FSessionId: Integer;
    FStartDate, FEndDate: TDateTime;
    FReference: String;
    
    FAllowGenerateData: Boolean;

    { Private declarations }
  protected
    FExportFileName : String;

    function ExportGridContents(aFileExt, aFilter, aFileName: String;
      aMethod: TPPWFrameWorkSaveGridMethod): boolean; override;

    function GetFilterString     : String; override;
    function GetOrderByString    : String; override;

  public
    { Public declarations }
  end;

ResourceString
  rsConfirmationInvoicing = 'Er werden in totaal %d sessies gefactureerd, met een totaal van %d facturen' + #13#10 +
    '(betreffende %d inschrijvingen)';
  rsWarningInvoicing = 'OPGELET: het aantal factuurlijnen in het Excel bestand (%d) stemt NIET overeen' + #13#10 +
    'met het aantal lijnen op het scherm (%d). Gelieve de facturatie te controleren !';

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Invoicing, Unit_EdutecInterfaces, Data_EduMainClient, cxGridExportLink,
  Form_FVBFFCBaseListView, Unit_PPWFrameWorkListView, Unit_FVBFFCListView, unit_const,
  Unit_Global, SysUtils, Dialogs;

procedure TfrmInvoicing_List.dxbbCollectClick(Sender: TObject);
var
  aDataModule : IEDUInvoicingDatamodule;
  oldcursor: TCursor;
begin
  inherited;
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUInvoicingDatamodule, aDataModule ) ) then
  begin

    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;
      aDataModule.CollectInvoicingData;
      ApplyFilter;
    finally
      Screen.Cursor := oldcursor;
    end;
  end;

  { Added cursor - ivdbossche (08-AUG-2007) }
end;

function TfrmInvoicing_List.ExportGridContents(aFileExt, aFilter,
  aFileName: String; aMethod: TPPWFrameWorkSaveGridMethod): boolean;
var
  lcv: integer;
  aDataModule : IEDUInvoicingDatamodule;
begin
  Result := False;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUInvoicingDatamodule, aDataModule ) ) then
  begin
    if ActiveGridView.InheritsFrom(TcxGridDBTableView) then
    begin
      for lcv := 0 to TcxGridDBTableView(ActiveGridView).ColumnCount - 1 do
      begin
        if TcxGridDBTableView(ActiveGridView).Columns[lcv].Hidden then
        begin
          TcxGridDBTableView(ActiveGridView).Columns[lcv].Visible := True;
          TcxGridDBTableView(ActiveGridView).Columns[lcv].Tag := 1;
        end;
      end;
    end;
    {
    30/01/2006 : cheuten
    Bugfix begin.
    Problem : Instead of aFileName use FileName because else userinput is ignored.
    }
    with TSaveDialog.Create( Self ) do
    begin
      InitialDir := aDataModule.GetInvoicePathCurUser;
      DefaultExt := aFileExt;
      Filter     := aFilter;
      FileName   := aFileName;
      if ( Execute ) then
      begin
        FExportFileName := FileName;
        if ( aFileExt = 'htm' ) then
        begin
          try
            ExportGridToHTML( FileName, Grid );
            Result := True;
          except
            Result := False;
          end;
        end
        else if ( aFileExt = 'xls' ) then
        begin
          try
            ExportGridToExcel( FileName, Grid );
            //ExportGrid4ToExcel(FileName, Grid, True, False);
            Result := True;
          except
            Result := False;
          end;
        end
        else if ( aFileExt = 'xml' ) then
        begin
          try
            ExportGridToXML( FileName, Grid );
            Result := True;
          except
            Result := False;
          end;
        end
        else
        begin
          try
            ExportGridToText( FileName, Grid );
            Result := True;
          except
            Result := False;
          end;
        end;
      end;
      Free;
    end;
    {
    30/01/2006 : cheuten
    Bugfix ended.
    }

    if ActiveGridView.InheritsFrom(TcxGridDBTableView) then
    begin
      for lcv := 0 to TcxGridDBTableView(ActiveGridView).ColumnCount - 1 do
      begin
        if (TcxGridDBTableView(ActiveGridView).Columns[lcv].Tag = 1) then
        begin
          TcxGridDBTableView(ActiveGridView).Columns[lcv].Visible := False;
          TcxGridDBTableView(ActiveGridView).Columns[lcv].Tag := 0;
        end;
      end;
    end;
  end;
end;

function TfrmInvoicing_List.GetFilterString: String;
var
  aFilterString : String;
begin
  // **********************************************************
  // Start & End date filter (added by Ivdbossche (06/08/2007)
  FStartDate := EncodeDate (1900, 1, 1);
  FEndDate   := EncodeDate (2039, 12,31);
  FSessionId := -1;
  FReference := '';

  { Add the condition for the User ID }
  {if not ( FSearchCriteriaDataSet.FieldByName( 'F_ORGANISATION_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_ORGANISATION_ID', FSearchCriteriaDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger );
  end;
  }

  { Add the condition for the FirstName }
  {if not ( FSearchCriteriaDataSet.FieldByName( 'F_NAAM' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_NAAM', FSearchCriteriaDataSet.FieldByName( 'F_NAAM' ).AsString );
  end;
  }

  { Add the condition for the Code }
  {if not ( FSearchCriteriaDataSet.FieldByName( 'F_COMMENT' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_COMMENT', FSearchCriteriaDataSet.FieldByName( 'F_COMMENT' ).AsString );
  end;


  if not ( FSearchCriteriaDataSet.FieldByName( 'F_INVOICE_TYPE' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_INVOICE_TYPE', FSearchCriteriaDataSet.FieldByName( 'F_INVOICE_TYPE' ).AsInteger );
  end;
  }

  { Add the condition for the Session reference }
  if ( srcSearchCriteria.DataSet.FieldByName( 'F_CODE' ).AsString <> '' ) then
  begin
    FReference := srcSearchCriteria.DataSet.FieldByName('F_CODE').AsString;
    AddLikeFilterCondition ( aFilterString, 'F_CODE', FReference );
  end;

  { Add the condition for the Session Id }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_SESSION_ID' ).IsNull ) then
  begin
    FSessionId := srcSearchCriteria.DataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
    AddIntEqualCondition( aFilterString, 'F_SESSION_ID', FSessionId );
  end;

  //if ( FSearchCriteriaDataSet.FieldByName( 'F_INVOICED' ).AsBoolean ) then // Modified on 08-AUG-2007 (ivdbossche)
  if (srcSearchCriteria.DataSet.FieldByName('F_STATUS').AsString = STATUS_INVOICED) then
  begin
    AddFilterCondition( aFilterString, 'ISNULL (F_FILENAME, '''') <> ''''', 'AND' );
    FAllowGenerateData := False;
  end
  else
  begin
    AddFilterCondition( aFilterString, 'ISNULL (F_FILENAME, '''') = ''''', 'AND' );
    FAllowGenerateData := True;
  end;


  // Date filtering
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_START_DATE' ).IsNull ) then
    FStartDate := srcSearchCriteria.DataSet.FieldByName( 'F_START_DATE' ).AsDateTime;
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_END_DATE' ).IsNull ) then
    FEndDate := srcSearchCriteria.DataSet.FieldByName( 'F_END_DATE' ).AsDateTime;

  AddDateBetweenCondition (aFilterString, 'F_START_DATE', FStartDate, FEndDate);

  Result := aFilterString;
end;

function TfrmInvoicing_List.GetOrderByString: String;
begin
  if ( ActiveGridView = cxgrdtblvList ) then
  begin
    Result := Inherited GetOrderByString;
  end
  else
  begin
    Result := '';
  end;
end;

procedure TfrmInvoicing_List.dxbpmnGridPopup(Sender: TObject);
begin
  inherited;

    // Set 'Generate Invoicing Data' disabled when STATUS_INVOICED has been selected and/or no records were found
    // Modified on 06-AUG-2007 (ivdbossche)
    acGenerateInvoicingData.Enabled := FAllowGenerateData and (srcMain.DataSet.RecordCount > 0);
end;

procedure TfrmInvoicing_List.acGenerateInvoicingDataExecute(
  Sender: TObject);
var
  aDataModule : IEDUInvoicingDatamodule;
  NumberOfSessions, NumberOfInvoices, NumberOfEnrollments: Integer;
  aMessage: String;

  // Nested function
  function GenFileName: String;
  begin
    // Generate new filename for Excel file.
    Result := 'Factuurlijst - ' + dtmEDUMainClient.GetCurUser + ' - opgemaakt op ' + FormatDateTime('dd-mm-yyyy', Now) + '.xls';
  end;
begin
  try
    cxgrdList.ActiveLevel := cxgrdlvlExport;
    cxgrdList.LayoutChanged;

    inherited;

{    ClearFilterCriteria;  // Disabled by Ivdbossche on 06/08/2007.  We wish to export filtered invoices only... }
    //ApplyFilter;

    if srcMain.DataSet.RecordCount = 0 then begin
      cxgrdList.ActiveLevel := cxgrdlvlList; // Set Grid level back to cxgrdlvlList so user will see the right Grid format
      MessageDlg(eNoRecord2Export, mtInformation, [mbOk], 0);
    end else begin
      if ( ExportGridContents( 'xls', 'Microsoft Excel Worksheet (*.xls)|*.xls',
                               RemoveSpecialChars(GenFileName), Nil ) ) then
      begin
        if ( Assigned( FrameWorkDataModule ) ) and
           ( Supports( FrameWorkDataModule, IEDUInvoicingDatamodule, aDataModule ) ) then
        begin
           aDataModule.MarkExported( FExportFileName, FSessionId, FStartDate, FEndDate, FReference, NumberOfSessions, NumberOfInvoices,
             NumberOfEnrollments);
          aMessage := Format (rsConfirmationInvoicing, [NumberOfSessions, NumberOfInvoices, NumberOfEnrollments]);
          if NumberOfInvoices <> srcMain.DataSet.RecordCount then
          begin
            aMessage := aMessage + #13#10 + #13#10 + Format (rsWarningInvoicing, [NumberOfInvoices, srcMain.DataSet.RecordCount]);
            MessageDlg (aMessage, mtWarning, [mbOk], 0);
          end
          else begin
            MessageDlg (aMessage, mtInformation, [mbOk], 0);
          end;
          ApplyFilter; // Exported records will dissappear from the screen... so users can't reexport the same record one more time
        end;
      end;
    end;
  finally
    cxgrdList.ActiveLevel := cxgrdlvlList;
  end;
end;

procedure TfrmInvoicing_List.acRefreshExecute(Sender: TObject);
var
  aDataModule : IEDUInvoicingDatamodule;
begin
  inherited;
    if ( Assigned( FrameWorkDataModule ) ) and
           ( Supports( FrameWorkDataModule, IEDUInvoicingDatamodule, aDataModule ) ) then
    begin
      // Refresh all invoice lines in grid.
      aDataModule.CollectInvoicingData;
    end;
end;

end.
