{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_Invoicing
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  23/03/2008   ivdbossche           BugFix Mantis 2344 in TdtmInvoicing.MarkExported.
  07/08/2007   ivdbossche           Adjusted MarkExported in order to mark
                                    selected records only.
  06/08/2007   ivdbossche           Added field cdsSearchCriteriaF_END_DATE
  25/09/2006   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Invoicing;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule, DB,
  MConnect, Unit_FVBFFCDBComponents, Unit_PPWFrameWorkComponents, Provider,
  ADODB, DateUtils, Unit_EdutecInterfaces, Unit_PPWFrameWorkActions,
  Datasnap.DSConnect;

type
  TdtmInvoicing = class(TEDUDataModule, IEDUInvoicingDatamodule)
    cdsInvoicing: TFVBFFCClientDataSet;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_NAAM: TStringField;
    cdsListF_STRAAT: TStringField;
    cdsListF_POSTCODE: TStringField;
    cdsListF_WOONPLAATS: TStringField;
    cdsListF_FACTUUR_NR: TStringField;
    cdsListF_DATUM: TDateTimeField;
    cdsListF_KLANT_NR: TStringField;
    cdsListF_VERVALDATUM: TDateTimeField;
    cdsListF_CODE: TStringField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_INVOICE_TYPE: TIntegerField;
    cdsListF_INVOICE_AMOUNT: TFloatField;
    cdsListF_FILENAME: TStringField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsRecordF_NAAM: TStringField;
    cdsRecordF_STRAAT: TStringField;
    cdsRecordF_POSTCODE: TStringField;
    cdsRecordF_WOONPLAATS: TStringField;
    cdsRecordF_FACTUUR_NR: TStringField;
    cdsRecordF_DATUM: TDateTimeField;
    cdsRecordF_KLANT_NR: TStringField;
    cdsRecordF_VERVALDATUM: TDateTimeField;
    cdsRecordF_CODE: TStringField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_INVOICE_TYPE: TIntegerField;
    cdsRecordF_INVOICE_AMOUNT: TFloatField;
    cdsRecordF_FILENAME: TStringField;
    cdsSearchCriteriaF_ORGANISATION_ID: TIntegerField;
    cdsSearchCriteriaF_NAAM: TStringField;
    cdsSearchCriteriaF_CODE: TStringField;
    cdsSearchCriteriaF_SESSION_ID: TIntegerField;
    cdsSearchCriteriaF_NAME: TStringField;
    cdsSearchCriteriaF_START_DATE: TDateTimeField;
    cdsSearchCriteriaF_INVOICE_TYPE: TIntegerField;
    cdsSearchCriteriaF_INVOICED: TBooleanField;
    cdsMarkInvoiced: TFVBFFCClientDataSet;
    cdsListF_COMMENT: TStringField;
    cdsSearchCriteriaF_COMMENT: TStringField;
    cdsListF_ANA_SLEUTEL: TStringField;
    cdsListF_REFERENTIE: TStringField;
    cdsRecordF_REFERENTIE: TStringField;
    cdsRecordF_COMMENT: TStringField;
    cdsRecordF_ANA_SLEUTEL: TStringField;
    cdsSearchCriteriaF_END_DATE: TDateTimeField;
    cdsSearchCriteriaF_STATUS: TStringField;
    cdsListF_NUMBER_PARTICIPANTS: TIntegerField;
    cdsListF_AANTAL_UREN: TIntegerField;
    cdsListF_PRIJS_PER_UUR: TIntegerField;
    cdsListF_TOTAAL: TFloatField;
    cdsListF_REKENINGNR: TStringField;
    cdsListF_TAALCODE: TStringField;
    cdsListF_LANDCODE: TStringField;
    cdsListF_VAT_NR: TStringField;
    cdsListF_SYNERGY_ID: TStringField;
    procedure FVBFFCDataModuleInitialise(Sender: TObject);
    procedure cdsListAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    function IsListViewActionAllowed ( aAction: TPPWFrameWorkListViewAction ) : Boolean; override;
  public
    { Public declarations }
    procedure CollectInvoicingData; virtual;
    procedure MarkExported( aFileName : String; aSessionId : Integer; aStartDate, aEndDate: TDateTime; aReference: String;
      var NumberOfSessions: Integer; var NumberOfInvoices: Integer; var NumberOfEnrollments: Integer ); virtual;
    function GetInvoicePathCurUser: String;
  end;

implementation

uses Unit_PPWFrameWorkDataModule, Unit_const;

{$R *.dfm}


/// <summary>
/// This method will be used to collect the invoicing data on the DB side.
/// It will actually prepare all sessions which have the status 'geattesteerd'.
/// This will be done by executing the Invoicing Stored Procedure
/// </summary>
/// <returns>None</returns>

procedure TdtmInvoicing.CollectInvoicingData;
begin
  cdsInvoicing.Execute;

  if ( cdsList.Active ) then
  begin
    RefreshListDataSet;
  end;
end;

procedure TdtmInvoicing.FVBFFCDataModuleInitialise(Sender: TObject);
begin
  inherited;

  // Initialize default search criteria
  if cdsSearchCriteria.State in [dsEdit, dsInsert] then
  begin
    cdsSearchCriteria.FieldByName('F_STATUS').Value := STATUS_ATTESTED;
  end;

  CollectInvoicingData;
end;

function TdtmInvoicing.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
begin
  result := inherited IsListViewActionAllowed (aAction);
  if (aAction is TPPWFrameWorkListViewConsultAction) or
     (aAction is TPPWFrameWorkListViewAddAction) or
     (aAction is TPPWFrameWorkListViewDeleteAction) or
     (aAction is TPPWFrameWorkListViewEditAction) then
  begin
    result := false;
  end;
end;

procedure TdtmInvoicing.MarkExported( aFileName : String; aSessionId : Integer; aStartDate, aEndDate: TDateTime; aReference: String;
  var NumberOfSessions: Integer; var NumberOfInvoices: Integer; var NumberOfEnrollments: Integer );
begin
  // BugFix Mantis 2344.
  // Ivan Van den Bossche (27/03/2008)
  cdsMarkInvoiced.Params.Clear;
  cdsMarkInvoiced.FetchParams;

  cdsMarkInvoiced.Params.ParamByName( '@aFileName' ).Value := aFileName;
  cdsMarkInvoiced.Params.ParamByName( '@aSessionId' ).Value := aSessionId;
  cdsMarkInvoiced.Params.ParamByName( '@aStartDate' ).Value := aStartDate;
  cdsMarkInvoiced.Params.ParamByName( '@aEndDate' ).Value := aEndDate;
  cdsMarkInvoiced.Params.ParamByName( '@aReference' ).Value := aReference;
  cdsMarkInvoiced.Execute;
  NumberOfSessions    := cdsMarkInvoiced.Params.ParamByName( '@NumberOfSessions' ).Value;
  NumberOfInvoices    := cdsMarkInvoiced.Params.ParamByName( '@NumberOfInvoices' ).Value;
  NumberOfEnrollments := cdsMarkInvoiced.Params.ParamByName( '@NumberOfEnrollments' ).Value;
end;

procedure TdtmInvoicing.cdsListAfterPost(DataSet: TDataSet);
begin
  cdsList.Tag := 1;
  inherited;
  cdsList.Tag := 0;
end;

function TdtmInvoicing.GetInvoicePathCurUser: String;
begin
  Result := ssckData.AppServer.GetInvoicePathCurUser;
end;

end.
