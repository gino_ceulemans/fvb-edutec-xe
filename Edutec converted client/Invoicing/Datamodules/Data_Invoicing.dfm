inherited dtmInvoicing: TdtmInvoicing
  FilterFirst = True
  KeyFields = 'F_ORGANISATION_ID; F_SESSION_ID; F_INVOICE_TYPE'
  ListViewClass = 'TfrmInvoicing_List'
  RecordViewClass = 'TfrmInvoicing_Record'
  Registered = True
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Visible = False
    end
    object cdsListF_NAAM: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAAM'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_STRAAT: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STRAAT'
      ProviderFlags = [pfInUpdate]
      Size = 134
    end
    object cdsListF_POSTCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsListF_WOONPLAATS: TStringField
      DisplayLabel = 'Woonplaats'
      FieldName = 'F_WOONPLAATS'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_FACTUUR_NR: TStringField
      DisplayLabel = 'Factuur Nr'
      FieldName = 'F_FACTUUR_NR'
      ProviderFlags = [pfInUpdate]
      Size = 1
    end
    object cdsListF_DATUM: TDateTimeField
      DisplayLabel = 'Factuur Datum'
      FieldName = 'F_DATUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_KLANT_NR: TStringField
      DisplayLabel = 'Klant Nr'
      FieldName = 'F_KLANT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object cdsListF_VERVALDATUM: TDateTimeField
      DisplayLabel = 'Vervaldatum'
      FieldName = 'F_VERVALDATUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CODE: TStringField
      DisplayLabel = 'Sessie referentie'
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_INVOICE_TYPE: TIntegerField
      Alignment = taLeftJustify
      DisplayLabel = 'Factuur Type'
      FieldName = 'F_INVOICE_TYPE'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_INVOICE_AMOUNT: TFloatField
      DisplayLabel = 'Algemeen totaal'
      FieldName = 'F_INVOICE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_FILENAME: TStringField
      DisplayLabel = 'Bestand'
      FieldName = 'F_FILENAME'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 255
    end
    object cdsListF_COMMENT: TStringField
      DisplayLabel = 'Cursus / Mededeling'
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsListF_ANA_SLEUTEL: TStringField
      DisplayLabel = 'Analytische Code'
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_REFERENTIE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_REFERENTIE'
      ProviderFlags = []
      ReadOnly = True
      Size = 12
    end
    object cdsListF_NUMBER_PARTICIPANTS: TIntegerField
      DisplayLabel = 'Aantal deelnemers'
      FieldName = 'F_NUMBER_PARTICIPANTS'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_AANTAL_UREN: TIntegerField
      DisplayLabel = 'Aantal uren'
      FieldName = 'F_AANTAL_UREN'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_PRIJS_PER_UUR: TIntegerField
      DisplayLabel = 'Prijs per uur'
      FieldName = 'F_PRIJS_PER_UUR'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_TOTAAL: TFloatField
      DisplayLabel = 'Totaal'
      FieldName = 'F_TOTAAL'
    end
    object cdsListF_REKENINGNR: TStringField
      DisplayLabel = 'Rekeningnr'
      FieldName = 'F_REKENINGNR'
      ReadOnly = True
      Size = 6
    end
    object cdsListF_TAALCODE: TStringField
      DisplayLabel = 'Taalcode'
      FieldName = 'F_TAALCODE'
      ReadOnly = True
      Size = 1
    end
    object cdsListF_LANDCODE: TStringField
      DisplayLabel = 'Landcode'
      FieldName = 'F_LANDCODE'
      ReadOnly = True
      Size = 1
    end
    object cdsListF_VAT_NR: TStringField
      DisplayLabel = 'BTWnr'
      FieldName = 'F_VAT_NR'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_SYNERGY_ID: TStringField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = []
      ReadOnly = True
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
      Visible = False
    end
    object cdsRecordF_NAAM: TStringField
      DisplayLabel = 'Organisatie Naam'
      FieldName = 'F_NAAM'
      Size = 64
    end
    object cdsRecordF_STRAAT: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STRAAT'
      Size = 134
    end
    object cdsRecordF_POSTCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTCODE'
      Size = 10
    end
    object cdsRecordF_WOONPLAATS: TStringField
      DisplayLabel = 'Woonplaats'
      FieldName = 'F_WOONPLAATS'
      Size = 128
    end
    object cdsRecordF_FACTUUR_NR: TStringField
      DisplayLabel = 'Factuur Nr'
      FieldName = 'F_FACTUUR_NR'
      Size = 1
    end
    object cdsRecordF_DATUM: TDateTimeField
      DisplayLabel = 'Factuur Datum'
      FieldName = 'F_DATUM'
    end
    object cdsRecordF_KLANT_NR: TStringField
      DisplayLabel = 'Klant Nr'
      FieldName = 'F_KLANT_NR'
      Size = 1
    end
    object cdsRecordF_VERVALDATUM: TDateTimeField
      DisplayLabel = 'Vervaldatum'
      FieldName = 'F_VERVALDATUM'
    end
    object cdsRecordF_CODE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_CODE'
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Sessie Naam'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsRecordF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
    end
    object cdsRecordF_INVOICE_TYPE: TIntegerField
      DisplayLabel = 'Factuur Type'
      FieldName = 'F_INVOICE_TYPE'
    end
    object cdsRecordF_INVOICE_AMOUNT: TFloatField
      DisplayLabel = 'Factuur Bedrag'
      FieldName = 'F_INVOICE_AMOUNT'
    end
    object cdsRecordF_FILENAME: TStringField
      DisplayLabel = 'Bestand'
      FieldName = 'F_FILENAME'
      Visible = False
      Size = 255
    end
    object cdsRecordF_REFERENTIE: TStringField
      FieldName = 'F_REFERENTIE'
      ReadOnly = True
      Size = 1
    end
    object cdsRecordF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      Size = 255
    end
    object cdsRecordF_ANA_SLEUTEL: TStringField
      FieldName = 'F_ANA_SLEUTEL'
      Size = 50
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    Left = 432
    Top = 168
    object cdsSearchCriteriaF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      Size = 255
    end
    object cdsSearchCriteriaF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_NAAM: TStringField
      DisplayLabel = 'Organisatie Naam'
      FieldName = 'F_NAAM'
      Size = 64
    end
    object cdsSearchCriteriaF_CODE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_CODE'
    end
    object cdsSearchCriteriaF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
    end
    object cdsSearchCriteriaF_NAME: TStringField
      DisplayLabel = 'Sessie Naam'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_INVOICE_TYPE: TIntegerField
      Alignment = taLeftJustify
      DisplayLabel = 'Factuur Type'
      FieldName = 'F_INVOICE_TYPE'
    end
    object cdsSearchCriteriaF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
    end
    object cdsSearchCriteriaF_INVOICED: TBooleanField
      DisplayLabel = 'Reeds Gefaktureerd'
      FieldName = 'F_INVOICED'
    end
    object cdsSearchCriteriaF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object cdsSearchCriteriaF_STATUS: TStringField
      FieldName = 'F_STATUS'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmInvoicing'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmInvoicing'
  end
  object cdsInvoicing: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvInvoicing'
    RemoteServer = dspConnection
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 432
    Top = 112
  end
  object cdsMarkInvoiced: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftString
        Name = '@aFileName'
        ParamType = ptInput
        Size = 255
      end
      item
        DataType = ftDateTime
        Name = '@aStartDate'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@aEndDate'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@aSessionId'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = '@aReference'
        ParamType = ptInput
        Size = 20
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@NumberOfSessions'
        ParamType = ptInputOutput
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@NumberOfEnrollments'
        ParamType = ptInputOutput
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@NumberOfInvoices'
        ParamType = ptInputOutput
      end>
    ProviderName = 'prvMarkInvoiced'
    RemoteServer = dspConnection
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 432
    Top = 64
  end
end
