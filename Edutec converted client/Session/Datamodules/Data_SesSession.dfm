inherited dtmSesSession: TdtmSesSession
  OnCreate = FVBFFCDataModuleCreate
  AutoOpenDataSets = False
  KeyFields = 'F_SESSION_ID'
  ListViewClass = 'TfrmSesSession_List'
  RecordViewClass = 'TfrmSesSession_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Height = 433
  Width = 455
  DetailDataModules = <
    item
      Caption = 'Inschrijvingen'
      ForeignKeys = 'F_SESSION_ID'
      DataModuleClass = 'TdtmEnrolment'
      AutoCreate = False
    end
    item
      Caption = 'Uitgaven'
      ForeignKeys = 'F_SESSION_ID'
      DataModuleClass = 'TdtmSesExpenses'
      AutoCreate = False
    end
    item
      Caption = 'Historiek'
      ForeignKeys = 'F_SESSION_ID'
      DataModuleClass = 'TdtmLogHistory'
      AutoCreate = False
    end
    item
      Caption = 'Lesgevers'
      ForeignKeys = 'F_SESSION_ID'
      DataModuleClass = 'TdtmDocCenter'
      AutoCreate = False
    end
    item
      Caption = 'KMO Portefeuille - Fakturen'
      ForeignKeys = 'F_SESSION_ID'
      DataModuleClass = 'TdtmKMO_PORT_Payment'
      AutoCreate = False
    end
    item
      Caption = 'KMO Portefeuille - Sessie'
      ForeignKeys = 'F_SESSION_ID'
      DataModuleClass = 'TdtmKMO_PORT_Link_Port_Session'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = dspConnection
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_PROGRAM_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_PROGRAM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_PROGRAM_TYPE_NAME: TStringField
      DisplayLabel = 'Programmatype'
      FieldName = 'F_PROGRAM_TYPE_NAME'
      Size = 40
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_CODE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_DURATION: TSmallintField
      DisplayLabel = 'Duur'
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_DURATION_DAYS: TSmallintField
      DisplayLabel = 'Duur ( Dagen )'
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_MINIMUM: TSmallintField
      DisplayLabel = 'Minimum'
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_MAXIMUM: TSmallintField
      DisplayLabel = 'Maximum'
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Locatie ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_INFRASTRUCTURE_NAME: TStringField
      DisplayLabel = 'Locatie'
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_PRICING_SCHEME: TIntegerField
      DisplayLabel = 'Standaard tussenkomst'
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_OTHER: TFloatField
      DisplayLabel = 'Sessieprijs (overige)'
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ANA_SLEUTEL: TStringField
      DisplayLabel = 'Analytische sleutel'
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsListF_PRICE_WORKER: TFloatField
      DisplayLabel = 'Sessieprijs (arbeider)'
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_CLERK: TFloatField
      DisplayLabel = 'Sessieprijs (bediende)'
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CONSTRUCT_COMMENT: TStringField
      DisplayLabel = 'Construct commentaar'
      FieldName = 'F_CONSTRUCT_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsListF_SES_AANW_PRINTED: TBooleanField
      DisplayLabel = 'Aanw. Lijst Printed'
      FieldName = 'F_SES_AANW_PRINTED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ALL_CERT_PRINTED: TBooleanField
      DisplayLabel = 'Insch. Gecert.'
      FieldName = 'F_ALL_CERT_PRINTED'
      ProviderFlags = []
    end
    object cdsListF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_SESSIONGROUP_NAME: TStringField
      DisplayWidth = 25
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_RECEIVED_PARTICIPANTS_LIST: TBooleanField
      FieldName = 'F_RECEIVED_PARTICIPANTS_LIST'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_WEEK: TStringField
      FieldName = 'F_WEEK'
      ProviderFlags = []
      ReadOnly = True
      Size = 15
    end
    object cdsListF_SENT_COURSE: TBooleanField
      FieldName = 'F_SENT_COURSE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_INSTRUCTOR: TStringField
      DisplayWidth = 255
      FieldName = 'F_INSTRUCTOR'
      ProviderFlags = []
      ReadOnly = True
      Size = 255
    end
    object cdsListF_SES_DAYS: TStringField
      FieldName = 'F_SES_DAYS'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsListF_CONFIRMATION_REPORT_SENT: TBooleanField
      FieldName = 'F_CONFIRMATION_REPORT_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ENROLMENTS_WEB: TIntegerField
      DisplayLabel = 'N Inschrijvingen via Web'
      FieldName = 'F_ENROLMENTS_WEB'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_COMMENT: TStringField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsListF_VIRTUAL_SESSION: TBooleanField
      FieldName = 'F_VIRTUAL_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_COOPERATION_ID: TIntegerField
      DisplayLabel = 'In samenwerking met ( ID )'
      FieldName = 'F_COOPERATION_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_COOPERATION_NAME: TStringField
      DisplayLabel = 'In samenwerking met'
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_EPYC: TBooleanField
      FieldName = 'F_EPYC'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_EXAMINATOR: TBooleanField
      FieldName = 'F_EXAMINATOR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ATTESTS_SENT: TBooleanField
      FieldName = 'F_ATTESTS_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SCANNING: TBooleanField
      FieldName = 'F_SCANNING'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = []
    end
    object cdsListF_CODE_EPYC: TStringField
      FieldName = 'F_CODE_EPYC'
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = dspConnection
    AfterInsert = cdsRecordAfterInsert
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object cdsRecordF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_PROGRAM_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_PROGRAM_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
    end
    object cdsRecordF_PROGRAM_TYPE_NAME: TStringField
      FieldName = 'F_PROGRAM_TYPE_NAME'
      Size = 40
    end
    object cdsRecordF_USER_ID: TIntegerField
      DisplayLabel = 'Gebruiker ( ID )'
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_COMPLETE_USERNAME: TStringField
      DisplayLabel = 'Gebruiker'
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object cdsRecordF_SESSIONGROUP_ID: TIntegerField
      DisplayLabel = 'Sessiegroep ( ID )'
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_SESSIONGROUP_NAME: TStringField
      DisplayLabel = 'Sessiegroep'
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object cdsRecordF_CODE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_DURATION: TSmallintField
      DisplayLabel = 'Duur'
      FieldName = 'F_DURATION'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_DURATION_DAYS: TSmallintField
      DisplayLabel = 'Duur ( Dagen )'
      FieldName = 'F_DURATION_DAYS'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_MINIMUM: TSmallintField
      DisplayLabel = 'Minimum'
      FieldName = 'F_MINIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_MAXIMUM: TSmallintField
      DisplayLabel = 'Maximum'
      FieldName = 'F_MAXIMUM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_COMMENT: TMemoField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_START_HOUR: TStringField
      DisplayLabel = 'Start uur'
      FieldName = 'F_START_HOUR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsRecordF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Locatie ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_INFRASTRUCTURE_NAME: TStringField
      DisplayLabel = 'Locatie'
      FieldName = 'F_INFRASTRUCTURE_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_ANA_SLEUTEL: TStringField
      DisplayLabel = 'Analytische sleutel'
      FieldName = 'F_ANA_SLEUTEL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 10
    end
    object cdsRecordF_PRICING_SCHEME: TIntegerField
      DisplayLabel = 'Standaard tussenkomst'
      FieldName = 'F_PRICING_SCHEME'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PRICE_OTHER: TFloatField
      DisplayLabel = 'Sessieprijs (overige)'
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PRICE_WORKER: TFloatField
      DisplayLabel = 'Sessieprijs (arbeider)'
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PRICE_CLERK: TFloatField
      DisplayLabel = 'Sessieprijs (bediende)'
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_TK_FVB: TFloatField
      FieldName = 'F_TK_FVB'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_TK_CEVORA: TFloatField
      FieldName = 'F_TK_CEVORA'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_BT_FVB: TFloatField
      FieldName = 'F_BT_FVB'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_BT_CEVORA: TFloatField
      FieldName = 'F_BT_CEVORA'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_PRICE_FIXED: TFloatField
      DisplayLabel = 'Sessie Prijs FVB'
      FieldName = 'F_PRICE_FIXED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SES_DAYS: TMemoField
      FieldName = 'F_SES_DAYS'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_OCR: TStringField
      FieldName = 'F_OCR'
      ProviderFlags = []
      Size = 100
    end
    object cdsRecordF_INDIRECT_COST: TBCDField
      FieldName = 'F_INDIRECT_COST'
      ProviderFlags = [pfInUpdate]
      Precision = 5
      Size = 2
    end
    object cdsRecordF_WEB: TBooleanField
      FieldName = 'F_WEB'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_EXTRA_INFO: TStringField
      FieldName = 'F_EXTRA_INFO'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsRecordF_VIRTUAL_SESSION: TBooleanField
      FieldName = 'F_VIRTUAL_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_COOPERATION_ID: TIntegerField
      DisplayLabel = 'In samenwerking met ( ID )'
      FieldName = 'F_COOPERATION_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_COOPERATION_NAME: TStringField
      DisplayLabel = 'In samenwerking met'
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_EPYC: TBooleanField
      FieldName = 'F_EPYC'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_EXAMINATOR: TBooleanField
      FieldName = 'F_EXAMINATOR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ATTESTS_SENT: TBooleanField
      FieldName = 'F_ATTESTS_SENT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SCANNING: TBooleanField
      FieldName = 'F_SCANNING'
    end
    object cdsRecordF_CODE_EPYC: TStringField
      FieldName = 'F_CODE_EPYC'
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
    end
    object cdsSearchCriteriaF_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsSearchCriteriaF_PROGRAM_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_PROGRAM_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_SESSIONGROUP_ID: TIntegerField
      DisplayLabel = 'Sessie Groep ( ID )'
      FieldName = 'F_SESSIONGROUP_ID'
    end
    object cdsSearchCriteriaF_SESSIONGROUP_NAME: TStringField
      DisplayLabel = 'Sessie Groep'
      FieldName = 'F_SESSIONGROUP_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Dataum'
      FieldName = 'F_START_DATE'
    end
    object cdsSearchCriteriaF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsSearchCriteriaF_CODE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_CODE'
    end
    object cdsSearchCriteriaF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_STATUS_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_COOPERATION_ID: TIntegerField
      DisplayLabel = 'In samenwerking met ( ID )'
      FieldName = 'F_COOPERATION_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_COOPERATION_NAME: TStringField
      DisplayLabel = 'In samenwerking met'
      FieldName = 'F_COOPERATION_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Locatie ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_INFRASTRUCTURE_NAME: TStringField
      DisplayLabel = 'Locatie'
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever ( ID )'
      FieldName = 'F_INSTRUCTOR_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_INSTRUCTOR_NAME: TStringField
      DisplayLabel = 'Lesgever'
      FieldName = 'F_INSTRUCTOR_NAME'
      Size = 150
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmSesSession'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmSesSession'
  end
  object cdsSesSession: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftString
        Name = '@ACTION'
        ParamType = ptInput
        Size = 1
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@ID'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Precision = 18
        NumericScale = 2
        Name = '@PRICE_WORKER'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Precision = 18
        NumericScale = 2
        Name = '@PRICE_CLERK'
        ParamType = ptInput
      end
      item
        DataType = ftBCD
        Precision = 18
        NumericScale = 2
        Name = '@PRICE_OTHER'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = '@SESSION_DATE'
        ParamType = ptInput
      end>
    ProviderName = 'prvSES_SESSION'
    RemoteServer = ssckDataEnrolment
    AutoOpen = False
    Left = 362
    Top = 56
  end
  object ssckDataEnrolment: TFVBFFCSharedConnection
    ParentConnection = dtmEDUMainClient.sckcnnMain
    ChildName = 'rdtmEnrolment'
    Left = 72
    Top = 232
  end
end
