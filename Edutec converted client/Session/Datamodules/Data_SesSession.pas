{*****************************************************************************
  This DataModule will be used for the Maintenance of T_SES_SESSION records.

  @Name       Data_SesSession
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/02/2008   ivdbossche           New session should be published to website by default. (F_WEB)
  20/07/2007   ivdbossche           Added printing confirmation reports routines
  17/07/2007   ivdbossche           Added procedure MarkAsCertificatesPrinted (should be available to use in Form_SesSession_List)
  11/09/2006   sLesage              Added the necessary fields for Boetes and
                                    Tussenkomsten.
  11/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_SesSession;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, Unit_PPWFrameWorkActions, MConnect, unit_EdutecInterfaces, Unit_PPWFrameWorkInterfaces,
  Datasnap.DSConnect;

type
  TdtmSesSession = class(TEDUDataModule, IEDUSessionDataModule, IPPWFrameWorkDataModuleDefaultFilter )
    cdsSearchCriteriaF_SESSION_ID: TIntegerField;
    cdsSearchCriteriaF_NAME: TStringField;
    cdsSearchCriteriaF_PROGRAM_ID: TIntegerField;
    cdsSearchCriteriaF_PROGRAM_NAME: TStringField;
    cdsSearchCriteriaF_SESSIONGROUP_ID: TIntegerField;
    cdsSearchCriteriaF_SESSIONGROUP_NAME: TStringField;
    cdsSearchCriteriaF_START_DATE: TDateTimeField;
    cdsSearchCriteriaF_END_DATE: TDateTimeField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_PROGRAM_ID: TIntegerField;
    cdsListF_PROGRAM_NAME: TStringField;
    cdsListF_NAME: TStringField;
    cdsListF_CODE: TStringField;
    cdsListF_DURATION: TSmallintField;
    cdsListF_DURATION_DAYS: TSmallintField;
    cdsListF_MINIMUM: TSmallintField;
    cdsListF_MAXIMUM: TSmallintField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_STATUS_ID: TIntegerField;
    cdsListF_STATUS_NAME: TStringField;
    cdsListF_LANGUAGE_ID: TIntegerField;
    cdsListF_LANGUAGE_NAME: TStringField;
    cdsListF_INFRASTRUCTURE_ID: TIntegerField;
    cdsListF_INFRASTRUCTURE_NAME: TStringField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_PROGRAM_ID: TIntegerField;
    cdsRecordF_PROGRAM_NAME: TStringField;
    cdsRecordF_USER_ID: TIntegerField;
    cdsRecordF_COMPLETE_USERNAME: TStringField;
    cdsRecordF_SESSIONGROUP_ID: TIntegerField;
    cdsRecordF_SESSIONGROUP_NAME: TStringField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_CODE: TStringField;
    cdsRecordF_DURATION: TSmallintField;
    cdsRecordF_DURATION_DAYS: TSmallintField;
    cdsRecordF_MINIMUM: TSmallintField;
    cdsRecordF_MAXIMUM: TSmallintField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_STATUS_ID: TIntegerField;
    cdsRecordF_STATUS_NAME: TStringField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_COMMENT: TMemoField;
    cdsRecordF_START_HOUR: TStringField;
    cdsRecordF_PRICING_SCHEME: TIntegerField;
    cdsRecordF_INFRASTRUCTURE_ID: TIntegerField;
    cdsRecordF_INFRASTRUCTURE_NAME: TStringField;
    cdsSearchCriteriaF_CODE: TStringField;
    cdsSearchCriteriaF_STATUS_ID: TIntegerField;
    cdsSearchCriteriaF_STATUS_NAME: TStringField;
    cdsListF_PRICING_SCHEME: TIntegerField;
    cdsSesSession: TFVBFFCClientDataSet;
    cdsListF_ANA_SLEUTEL: TStringField;
    cdsRecordF_ANA_SLEUTEL: TStringField;
    cdsListF_PRICE_OTHER: TFloatField;
    cdsRecordF_PRICE_OTHER: TFloatField;
    cdsListF_PRICE_WORKER: TFloatField;
    cdsListF_PRICE_CLERK: TFloatField;
    cdsRecordF_PRICE_WORKER: TFloatField;
    cdsRecordF_PRICE_CLERK: TFloatField;
    cdsListF_CONSTRUCT_COMMENT: TStringField;
    cdsRecordF_TK_FVB: TFloatField;
    cdsRecordF_TK_CEVORA: TFloatField;
    cdsRecordF_BT_FVB: TFloatField;
    cdsRecordF_BT_CEVORA: TFloatField;
    cdsRecordF_PRICE_FIXED: TFloatField;
    cdsRecordF_SES_DAYS: TMemoField;
    cdsListF_SES_AANW_PRINTED: TBooleanField;
    cdsListF_ALL_CERT_PRINTED: TBooleanField;
    ssckDataEnrolment: TFVBFFCSharedConnection;
    cdsListF_NO_PARTICIPANTS: TIntegerField;
    cdsRecordF_OCR: TStringField;
    cdsRecordF_INDIRECT_COST: TBCDField;
    cdsRecordF_WEB: TBooleanField;
    cdsListF_SESSIONGROUP_NAME: TStringField;
    cdsRecordF_EXTRA_INFO: TStringField;
    cdsListF_RECEIVED_PARTICIPANTS_LIST: TBooleanField;
    cdsListF_WEEK: TStringField;
    cdsListF_SENT_COURSE: TBooleanField;
    cdsListF_INSTRUCTOR: TStringField;
    cdsListF_SES_DAYS: TStringField;
    cdsListF_CONFIRMATION_REPORT_SENT: TBooleanField;
    cdsListF_ENROLMENTS_WEB: TIntegerField;
    cdsListF_COMMENT: TStringField;
    cdsListF_VIRTUAL_SESSION: TBooleanField;
    cdsRecordF_VIRTUAL_SESSION: TBooleanField;
    cdsListF_COOPERATION_ID: TIntegerField;
    cdsListF_COOPERATION_NAME: TStringField;
    cdsRecordF_COOPERATION_ID: TIntegerField;
    cdsRecordF_COOPERATION_NAME: TStringField;
    cdsSearchCriteriaF_COOPERATION_ID: TIntegerField;
    cdsSearchCriteriaF_COOPERATION_NAME: TStringField;
    cdsSearchCriteriaF_INFRASTRUCTURE_ID: TIntegerField;
    cdsSearchCriteriaF_INFRASTRUCTURE_NAME: TStringField;
    cdsSearchCriteriaF_INSTRUCTOR_ID: TIntegerField;
    cdsSearchCriteriaF_INSTRUCTOR_NAME: TStringField;
    cdsListF_EPYC: TBooleanField;
    cdsListF_EXAMINATOR: TBooleanField;
    cdsListF_ATTESTS_SENT: TBooleanField;
    cdsListF_SCANNING: TBooleanField;
    cdsRecordF_EPYC: TBooleanField;
    cdsRecordF_EXAMINATOR: TBooleanField;
    cdsRecordF_ATTESTS_SENT: TBooleanField;
    cdsRecordF_SCANNING: TBooleanField;
    cdsListF_PROGRAM_TYPE: TIntegerField;
    cdsListF_CODE_EPYC: TStringField;
    cdsRecordF_CODE_EPYC: TStringField;
    cdsListF_PROGRAM_TYPE_NAME: TStringField;
    cdsRecordF_PROGRAM_TYPE: TIntegerField;
    cdsRecordF_PROGRAM_TYPE_NAME: TStringField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure prvRecordGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleCreate(Sender: TObject);
    procedure cdsRecordAfterInsert(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
  private
  protected
    { Protected declarations }
    function IsListViewActionAllowed         ( aAction         : TPPWFrameWorkListViewAction ) : Boolean; override;

    procedure SelectInfrastructure ( aDataSet : TDataSet ); virtual;
    procedure SelectLanguage       ( aDataSet : TDataSet ); virtual;
    procedure SelectProgram        ( aDataSet : TDataSet ); virtual;
    procedure SelectSessionGroup   ( aDataSet : TDataSet ); virtual;
    procedure SelectStatus         ( aDataSet : TDataSet ); virtual;
    procedure SelectStatusFilter   ( aDataSet : TDataSet ); virtual;
    procedure SelectUser           ( aDataSet : TDataSet ); virtual;
    procedure SelectCooperation    ( aDataSet : TDataSet ); virtual;
    procedure SelectInstructor     ( aDataSet : TDataSet ); virtual;
    procedure ShowInfrastructure   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowLanguage         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowProgram          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowSessionGroup     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowStatus           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowUser             ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowCooperation      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

    function CountEnrolments       ( aDataSet : TDataSet ): integer; virtual;
    function UpdateEnrolments      ( aDataSet : TDataSet; NewWorkerValue, NewClerkValue, NewOtherValue : double): integer; virtual;
    procedure ResetSessionDays     ( aDataSet : TDataSet ); virtual;
    function CheckStatus           ( aDataSet : TDataSet ) : integer; virtual;
    procedure ResetStatus          ( aDataSet : TDataSet; aStatus : integer ); virtual;
    procedure SetStatusFacturated  ( aDataSet : TDataSet ); virtual;

    procedure MarkAanwezigheidslijstPrinted ( aSessionList: String );
    procedure MarkAsCertificatesPrinted (EnrollmentList: String); // This datamodule should also support this procedure since it is needed in Form_SesSession_List (ivdbossche 17-07-2007)
    procedure ShowConfirmationRpts( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); // ivdbossche (20-JUL-2007)
    procedure ShowSessionWizard ( aDataSet : TDataSet); // ivdbossche (2-OCT-2007)
    procedure CreateOCR(ASessionID: Integer);
  public
    { Public declarations }
    class procedure SelectSession( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowSession  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    procedure InitDefaultFilter(ASearchCriteria: TClientDataSet);
  end;

  procedure CopySessionFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

uses
  Data_EduMainClient, Data_Language, Data_ProgProgram,
  Data_SessionGroup, Data_CenInfrastructure, Data_User,
  Data_FVBFFCBaseDataModule, Data_Status, DateUtils,
  Unit_PPWFrameWorkDataModule, Data_LogHistory, Data_DocCenter,
  Data_ConfirmationReports, Data_SessionWizard, Form_SessionWizard,
  Data_Cooperation, Data_Instructor;

{$R *.dfm}

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmSesSession.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This procedure will be used to copy Session related fields from one
  DataSet to another DataSet.

  @Name       CopySessionFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopySessionFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_SESSION_ID' ).AsInteger :=
        aSource.FieldByName( 'F_SESSION_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      critSessionToEnroll :
      begin
        aDestination.FieldByName( 'F_SESSION_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        { Initialise the Pricing Scheme fields }
        if aSource.FieldByName( 'F_PRICING_SCHEME' ).IsNull then
        begin
          aDestination.FieldByName( 'F_PRICING_SCHEME' ).AsInteger := 0;
        end
        else
        begin
          aDestination.FieldByName( 'F_PRICING_SCHEME' ).AsInteger :=
            aSource.FieldByName( 'F_PRICING_SCHEME' ).AsInteger;
        end;
        aDestination.FieldByName( 'F_PRICE_WORKER' ).AsFloat :=
          aSource.FieldByName( 'F_PRICE_WORKER' ).AsFloat;
        aDestination.FieldByName( 'F_PRICE_CLERK' ).AsFloat :=
          aSource.FieldByName( 'F_PRICE_CLERK' ).AsFloat;
        aDestination.FieldByName( 'F_PRICE_OTHER' ).AsFloat :=
          aSource.FieldByName( 'F_PRICE_OTHER' ).AsFloat;
        aDestination.FieldByName( 'F_TOTAL_SESSION_HOURS' ).AsFloat :=
          aSource.FieldByName( 'F_DURATION' ).AsInteger;
      end;
      critSessionToSesInstructor :
      begin
        aDestination.FieldByName( 'F_NAME' ).Value :=
          aSource.FieldByName( 'F_NAME' ).Value;
        aDestination.FieldByName( 'F_PROGRAM_ID' ).Value :=
          aSource.FieldByName( 'F_PROGRAM_ID' ).Value;
      end;
      critSessionToKMOPortefeuilleFilter :
      begin
        aDestination.FieldByName( 'F_SESSION_CODE' ).Value :=
          aSource.FieldByName( 'F_CODE' ).Value;
      end;
      critSessionToKMOPortefeuille :
      begin
        aDestination.FieldByName( 'F_SESSION' ).AsString := aSource.FieldByName( 'F_SESSION_ID' ).AsString;
        aDestination.FieldByName( 'F_PROGRAM' ).AsString := aSource.FieldByName( 'F_PROGRAM_NAME' ).AsString;
        aDestination.FieldByName( 'F_SESSION_DATE' ).AsDateTime := aSource.FieldByName( 'F_START_DATE' ).AsDateTime;
        
        {aDestination.FieldByName( 'F_SESSION_NAME' ).Value :=
          aSource.FieldByName( 'F_NAME' ).Value;
        aDestination.FieldByName( 'F_SESSION_CODE' ).Value :=
          aSource.FieldByName( 'F_CODE' ).Value;
        aDestination.FieldByName( 'F_SESSION_START_DATE' ).Value :=
          aSource.FieldByName( 'F_START_DATE' ).Value;
        aDestination.FieldByName( 'F_SESSION_END_DATE' ).Value :=
          aSource.FieldByName( 'F_END_DATE' ).Value;
        aDestination.FieldByName( 'F_SESSION_STATUS_NAME' ).Value :=
          aSource.FieldByName( 'F_STATUS_NAME' ).Value;   }
      end
      else
      begin
        aDestination.FieldByName( 'F_SESSION_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Infrastructure Records.

  @Name       TdtmSesSession.SelectInfrastructure
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.SelectInfrastructure(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aCopyTo := critInfrastructureToSession;
  aIDField := aDataSet.FindField( 'F_INFRASTRUCTURE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_INFRASTRUCTURE_ID <> ' + aIDField.AsString;
  end;

  TdtmCenInfrastructure.SelectCenInfrastructure( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to select one or more Language Records.

  @Name       TdtmSesSession.SelectLanguage
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.SelectLanguage(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_LANGUAGE_ID <> ' + aIDField.AsString;
  end;

  TdtmLanguage.SelectLanguage( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to select one or more Program Records.

  @Name       TdtmSesSession.SelectProgram
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.SelectProgram(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) ) then
  begin
    { Get the ID Field }
    if ( aDataSet = cdsSearchCriteria ) then
    begin
      aCopyTo := critProgramToSessionSearchCriteria;
    end
    else
    begin
      aCopyTo := critProgramToSession;
    end;

    aIDField := aDataSet.FindField( 'F_PROGRAM_ID' );
    aWhere   := '';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Language isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_PROGRAM_ID <> ' + aIDField.AsString;
    end;

    TdtmProgProgram.SelectProgram( aDataSet, aWhere, False, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more SessionGroup Records.

  @Name       TdtmSesSession.SelectSessionGroup
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.SelectSessionGroup(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUSessionGroupDataModule ) ) ) then
  begin
    { Get the ID Field }
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_SESSIONGROUP_ID' );
    aWhere   := '';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Language isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_SESSIONGROUP_ID <> ' + aIDField.AsString;
    end;

    TdtmSessionGroup.SelectSessionGroup( aDataSet, aWhere, False, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Status Records.

  @Name       TdtmSesSession.SelectStatus
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.SelectStatus(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aStatus  : TStatus;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_STATUS_ID' );
  // Controleer of deze collectie van statussen gerelateerd zijn aan
  // Sessie door boolean te controleren "F_SESSION"
  aWhere   := 'F_SESSION = 1 ';
  aCopyTo  := critDefault;

  aStatus := TStatus(aDataSet.FieldByName( 'F_STATUS_ID' ).AsInteger);

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
{
  The status 'Geattesteerd' can not be selected manually !
  it will be set automatically if all of the enrolments are changed to 'Geattesteerd'
}
    aWhere := aWhere +
      ' AND (F_STATUS_ID <> ' + aIDField.AsString +
      ')';
    // Kan ik even snel de status van de sessie terug op Attested zetten
    if ( aStatus <> stInvoiced ) then
    begin
      aWhere := aWhere + ' AND (F_STATUS_ID <> 10)';
    end;
  end;

  TdtmStatus.SelectStatus( aDataSet, aWhere, False, aCopyTo );

end;

{*****************************************************************************
  This method will be used to select one or more User Records.

  @Name       TdtmSesSession.SelectUser
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.SelectUser(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }

  aCopyTo  := critUserToSession;
  aIDField := aDataSet.FindField( 'F_USER_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_USER_ID <> ' + aIDField.AsString;
  end;

  TdtmUser.SelectUser( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Show an Infrastructure.

  @Name       TdtmSesSession.ShowInfrastructure
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.ShowInfrastructure(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critInfrastructureToSession;
  aIDField := aDataSet.FindField( 'F_INFRASTRUCTURE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCenInfrastructure.ShowCenInfrastructure( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Language.

  @Name       TdtmSesSession.ShowLanguage
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Program.

  @Name       TdtmSesSession.ShowProgram
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.ShowProgram(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) ) then
  begin
    aCopyTo := critProgramToSession;
    aIDField := aDataSet.FindField( 'F_PROGRAM_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmProgProgram.ShowProgram( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a SessionGroup.

  @Name       TdtmSesSession.ShowSessionGroup
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.ShowSessionGroup(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUSessionGroupDataModule ) ) ) then
  begin
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_SESSIONGROUP_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmSessionGroup.ShowSessionGroup( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a User.

  @Name       TdtmSesSession.ShowStatus
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.ShowStatus(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_STATUS_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmStatus.ShowStatus( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a RoleGroup.

  @Name       TdtmSesSession.ShowUser
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.ShowUser(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critUserToSession;
  aIDField := aDataSet.FindField( 'F_USER_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmUser.ShowUser( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

procedure TdtmSesSession.prvRecordGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_SES_SESSION';
end;

{*****************************************************************************************
  This method will be used to initialise some Additional Fields on the DataSet.

  @Name       TdtmSesSession.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
  @History          Author                  Description
  11/07/2008       Ivan Van den Bossche     Added initialization of F_VIRTUAL_SESSION field
*******************************************************************************************}

procedure TdtmSesSession.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_STATUS_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_STATUS_NAME' ).AsString :=
    dtmEDUMainClient.GetNameForState( 1 );
  aDataSet.FieldByName( 'F_START_DATE' ).AsDateTime := Today;
  aDataSet.FieldByName( 'F_MINIMUM' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_MAXIMUM' ).AsInteger := 9999;
  aDataSet.FieldByName( 'F_DURATION' ).AsInteger := 8;
  aDataSet.FieldByName( 'F_DURATION_DAYS' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_PRICE_FIXED' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_VIRTUAL_SESSION' ).AsBoolean := False;
  // Standaard Niets geselecteerd in cbo op rec-scherm
  aDataSet.FieldByName( 'F_ANA_SLEUTEL' ).AsString := '';
//  aDataSet.FieldByName( 'F_SESSIONGROUP_ID' ).AsInteger := 5;
//  aDataSet.FieldByName( 'F_SESSIONGROUP_NAME' ).AsString := 'Onbepaald';
  aDataSet.FieldByName('F_COOPERATION_ID').Value := 0;
  aDataSet.FieldByName( 'F_COOPERATION_NAME' ).AsString :=
    dtmEDUMainClient.GetNameForCooperation( 0 );
end;

{*****************************************************************************
  This method will be used to initialise some Foreign Key Fields on the DataSet.

  @Name       TdtmSesSession.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesSession.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) then
    begin
      { Initialise the Start Date in here so the code to determine the
        Max Enroll or Control Date can be resued }
      aDataSet.FieldByName( 'F_START_DATE' ).AsDateTime := Today;
      CopyProgramFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critProgramToSession );
    end;
    if ( Supports( MasterDataModule, IEDUSessionGroupDataModule ) ) then
    begin
      CopySessionGroupFieldValues( MasterDataModule.RecordDataset, aDataSet, False );
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Session Records.

  @Name       TdtmSesSession.SelectSession
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmSesSession.SelectSession(aDataSet: TDataSet;
  aWhere: String; aMultiSelect: Boolean;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesSession', aWhere ,
       aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopySessionFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Session
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmSesSession.ShowSession
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmSesSession.ShowSession(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmSesSession',
       'F_SESSION_ID = ' + aDataSet.FieldByName( 'F_SESSION_ID' ).AsString ,
       aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopySessionFieldValues( aSelectedRecords, aDataSet,
        ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  Procedure to check if any enrolments do exists.

  @Name       TdtmSesSession.CountEnrolments
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesSession.CountEnrolments(aDataSet: TDataSet): integer;
var
   aNumber : integer;
begin
  with cdsSesSession do
  begin
    Close;
    FetchParams;
    Params.ParamByName( '@ACTION' ).Value := 'C';
    Params.ParamByName( '@ID' ).Value :=
      aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
    Params.ParamByName( '@PRICE_OTHER' ).Value := 0;
    Params.ParamByName( '@SESSION_DATE' ).Value := Now;
    Execute;
    FetchParams;
    aNumber := Params.ParamByName( '@RETURN_VALUE' ).Value;
    Result := aNumber;
  end;
end;


{*****************************************************************************
  Procedure to distribute the dayprice over all enrolments possible.

  @Name       TdtmSesSession.UpdateEnrolments
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesSession.UpdateEnrolments(aDataSet: TDataSet;
  NewWorkerValue, NewClerkValue, NewOtherValue : double): integer;
begin
  with cdsSesSession do
  begin
    Close;
    FetchParams;
    Params.ParamByName( '@ACTION' ).Value := 'U';
    Params.ParamByName( '@ID' ).Value :=
      aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
    Params.ParamByName( '@PRICE_WORKER' ).Value :=
      NewWorkerValue;
    Params.ParamByName( '@PRICE_CLERK' ).Value :=
      NewClerkValue;
    Params.ParamByName( '@PRICE_OTHER' ).Value :=
      NewOtherValue;
    Params.ParamByName( '@SESSION_DATE' ).Value := Now;
    Execute;
    FetchParams;
    Result := Params.ParamByName( '@RETURN_VALUE' ).Value;
  end;
end;

{*****************************************************************************
  Procedure to check if the status is changed - so to retrieve him only.

  @Name       TdtmSesSession.CheckStatus
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesSession.CheckStatus(aDataSet: TDataSet): integer;
begin
  with cdsSesSession do
  begin
    Close;
    FetchParams;
    Params.ParamByName( '@ACTION' ).Value := 'S';
    Params.ParamByName( '@ID' ).Value :=
      aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
    Params.ParamByName( '@PRICE_OTHER' ).Value := 0;
    Params.ParamByName( '@SESSION_DATE' ).Value := Now;
    Execute;
    FetchParams;
    Result := Params.ParamByName( '@RETURN_VALUE' ).Value;
  end;
end;

procedure TdtmSesSession.ResetStatus(aDataSet: TDataSet; aStatus : integer);
begin
  with cdsSesSession do
  begin
    Close;
    FetchParams;
    if Params.Count > 0 then
    begin
      Params.ParamByName( '@ACTION' ).Value := 'E';
      Params.ParamByName( '@ID' ).Value :=
        aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
      Params.ParamByName( '@PRICE_OTHER' ).Value := aStatus;
      Params.ParamByName( '@SESSION_DATE' ).Value := Now;
      Execute;
//      FetchParams;
//      Result := Params.ParamByName( '@RETURN_VALUE' ).Value;
    end;
  end;
end;

{*****************************************************************************
  Name           : IsListViewActionAllowed
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Listview action "Add" is not allowed when the session entity
                   is docked as a detail of the organisation recordview.
                   This is needed because when an "Add" action was allowed, then
                   an errormessage "F_ORGANISATION_ID does not exist" occurs.
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/09/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TdtmSesSession.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
var
  aAllowed    : Boolean;
  aDataModule: IEDUOrganisationDataModule;
begin
  aAllowed := Inherited IsListViewActionAllowed( aAction );

  if ( aAction is TPPWFrameWorkListViewDeleteAction ) then
  begin
    try
      aAllowed := aAllowed and (DataSet.FieldByName('F_NO_PARTICIPANTS').AsInteger = 0);
    except
    end;
  end;

  if (supports (MasterDatamodule, IEDUOrganisationDataModule, aDataModule)
      and (aAction is TPPWFrameWorkListViewAddAction))
  then
    aAllowed := false;

  Result := aAllowed;
end;

procedure TdtmSesSession.ResetSessionDays(aDataSet: TDataSet);
var
  i : integer;
  slDays : TStringList;
  aDate : TDateTime;
//  aSesDays : string;
begin
  // verwijder de reeds bestaande Session Days uit T_SES_DAYS
  with cdsSesSession do
  begin
    Close;
    FetchParams;
    if Params.Count > 0 then
    begin
      Params.ParamByName( '@ACTION' ).Value := 'D';
      Params.ParamByName( '@ID' ).Value :=
        aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
      Params.ParamByName( '@SESSION_DATE' ).Value := Now;
      Execute;
    end;
  end;
  // Overloop alle Session Days ... ( indien slechts ��n dan slechts ��n )
  slDays := TstringList.Create;
  slDays.CommaText := aDataSet.FieldByName('F_SES_DAYS').AsString;
  for i := 0 to Pred(slDays.Count) do
  begin
    try
      aDate := StrToDate(slDays.Strings[i]);
      with cdsSesSession do
      begin
        Close;
        FetchParams;
        if Params.Count > 0 then
        begin
          Params.ParamByName( '@ACTION' ).Value := 'I';
          Params.ParamByName( '@ID' ).Value :=
            aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
          Params.ParamByName( '@SESSION_DATE' ).Value := aDate;
          Execute;
        end;
      end;
    except
    end;
  end;
  slDays.Clear;
  freeAndNil( slDays );
end;

procedure TdtmSesSession.MarkAanwezigheidslijstPrinted(
  aSessionList: String);
begin
  ssckData.AppServer.MarkAsAanwezigheidslijstPrinted(aSessionList);
end;

procedure TdtmSesSession.CreateOCR(ASessionID: Integer);
begin
  ssckData.AppServer.CreateOCR(ASessionID);
end;

procedure TdtmSesSession.MarkAsCertificatesPrinted(EnrollmentList: String);
begin
  ssckDataEnrolment.AppServer.MarkAsCertificatePrinted(EnrollmentList);
end;

procedure TdtmSesSession.ShowConfirmationRpts(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField: TField;
begin
  aIDField := aDataSet.FindField('F_SESSION_ID');

  if (Assigned(aIDField)) and (not (aIDField.IsNull)) then
    TdtmConfirmationReports.ShowConfirmationRpts(aDataSet,rvmEdit);

end;

procedure TdtmSesSession.ShowSessionWizard(aDataSet: TDataSet);
begin
    assert( aDataSet <> nil );

    if not aDataSet.FieldByName('F_SESSION_ID').IsNull then
    begin
       frmSessionWizard := TfrmSessionWizard.Create(nil);
       frmSessionWizard.SessionId := aDataSet.FindField('F_SESSION_ID').AsInteger;
       frmSessionWizard.Session := aDataSet.FindField('F_NAME').AsString;
       frmSessionWizard.StartDate := aDataSet.FindField('F_START_DATE').AsDateTime;
       frmSessionWizard.EndDate := aDataSet.FindField('F_END_DATE').AsDateTime;
       frmSessionWizard.PriceWorker := aDataSet.FindField('F_PRICE_WORKER').AsFloat;
       frmSessionWizard.PriceClerk := aDataSet.FindField('F_PRICE_CLERK').AsFloat;
       frmSessionWizard.PriceOther := aDataSet.FindField('F_PRICE_OTHER').AsFloat;
       frmSessionWizard.ShowModal;
       frmSessionWizard.Free;
    end;

end;

procedure TdtmSesSession.FVBFFCDataModuleCreate(Sender: TObject);
begin
  inherited;
end;

{*****************************************************************************
  Name           : TdtmSesSession.InitDefaultFilter
  Author         : Ivan Van den Bossche
  Arguments      : ASearchCriteria
  Return Values  : None
  Exceptions     : None
  Description    : This method is supplied so we can initialize a temp filter
                    before the form is activated. The form will show the default
                    filter as well.  The user could undo the filter...
  History        :

  Date         By                   Description
  ----         --                   -----------
  07/09/2007   ivdbossche           Initial creation of the procedure.
 *****************************************************************************}
procedure TdtmSesSession.InitDefaultFilter(
  ASearchCriteria: TClientDataSet);
begin
  cdsSearchCriteria.Edit;
  if Assigned(ASearchCriteria.FindField('F_NAME')) then
    cdsSearchCriteriaF_NAME.Value := ASearchCriteria.FieldByName('F_NAME').AsString;
  if Assigned(ASearchCriteria.FindField('F_CODE')) then
    cdsSearchCriteriaF_CODE.Value := ASearchCriteria.FieldByName('F_CODE').AsString;
  if Assigned(ASearchCriteria.FindField('F_START_DATE')) then
  begin
    cdsSearchCriteriaF_START_DATE.Value := ASearchCriteria.FieldByName('F_START_DATE').AsDateTime;
    cdsSearchCriteriaF_END_DATE.Value := IncDay(ASearchCriteria.FieldByName('F_START_DATE').AsDateTime, 1);
  end;

  cdsSearchCriteria.Post;

end;

procedure TdtmSesSession.cdsRecordAfterInsert(DataSet: TDataSet);
begin
  inherited;

  // Assign default indirect cost of 5 EUR.
  // Added by Ivan Van den Bossche (01-oct-2007)
  DataSet.FieldByName('F_INDIRECT_COST').Value := 5;

  // New sessions needs to be published to Edutec website by default.
  // 25-FEB-2008 (Ivan Van den Bossche)
  DataSet.FieldByName('F_WEB').Value := 1;

end;

procedure TdtmSesSession.cdsListAfterPost(DataSet: TDataSet);
begin
  cdsList.Tag := 1; // Make sure that changes will be posted to the database.
  inherited;

end;

procedure TdtmSesSession.SetStatusFacturated(aDataSet: TDataSet);
begin
  with cdsSesSession do
  begin
    Close;
    FetchParams;
    if Params.Count > 0 then
    begin
      Params.ParamByName( '@ACTION' ).Value := 'E';
      Params.ParamByName( '@ID' ).Value :=
        aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
      Params.ParamByName( '@PRICE_OTHER' ).Value := 12;
      Params.ParamByName( '@SESSION_DATE' ).Value := Now;
      Execute;
    end;
  end;
end;

procedure TdtmSesSession.SelectCooperation(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_COOPERATION_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := ' (F_COOPERATION_ID <> ' + aIDField.AsString + ')';
  end;

  TdtmCooperation.SelectCooperation( aDataSet, aWhere, False );

end;

procedure TdtmSesSession.ShowCooperation(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_COOPERATION_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCooperation.ShowCooperation( aDataSet, aRecordViewMode );
  end;
end;

procedure TdtmSesSession.SelectInstructor(aDataSet: TDataSet);
var
  aWhere   : String;
begin
  { Get the ID Field }
  aWhere   := '';

  TdtmInstructor.SelectInstructor( aDataSet, aWhere, False, critInstructorToSesSearchInstructor, True );

end;

procedure TdtmSesSession.SelectStatusFilter(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_STATUS_ID' );
  // Controleer of deze collectie van statussen gerelateerd zijn aan
  // Sessie door boolean te controleren "F_SESSION"
  aWhere   := 'F_SESSION = 1 ';
  aCopyTo  := critDefault;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
{
  The status 'Geattesteerd' can not be selected manually !
  it will be set automatically if all of the enrolments are changed to 'Geattesteerd'
}
    aWhere := aWhere +
      ' AND (F_STATUS_ID <> ' + aIDField.AsString +
      ')';
  end;

  TdtmStatus.SelectStatus( aDataSet, aWhere, False, aCopyTo );

end;

end.
