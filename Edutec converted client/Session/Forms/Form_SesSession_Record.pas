{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SES_SESSION records.


  @Name       Form_Role_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/03/2008   ivdbossche           Extra checks (mantis 2345)
  24/10/2007   ivdbossche           Added the web field.
  11/09/2006   sLesage              Added the necessary fields for Boetes and
                                    Tussenkomsten.
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_SesSession_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxControls,
  cxContainer, cxEdit, cxLabel, cxDropDownEdit, cxCalc, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxImageComboBox, cxMemo, cxCalendar,
  cxButtonEdit, cxSpinEdit, cxCurrencyEdit, Unit_PPWFrameWorkInterfaces,
  Menus, cxGraphics, cxSplitter, cxGroupBox, cxCheckBox, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, System.Actions, cxClasses;

const
  ANA_SLEUTEL_LEERKRACHTEN='810000';
  ANA_SLEUTEL_LEERLINGEN_EN_LEERKRACHTEN='800000';
  ANA_SLEUTEL_ANDERE='820000';
  SESSIONGROUP_LEERKRACHTEN=2;
  SESSIONGROUP_LEERLINGEN_EN_LEERKRACHTEN=4;
  SESSIONGROUP_LEERLINGEN=3;

type
  TfrmSesSession_Record = class(TEDURecordView)
    cxlblF_SESSION_ID1: TFVBFFCLabel;
    cxdblblF_SESSION_ID: TFVBFFCDBLabel;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    pnlPriceSchemeInfoSeperator: TFVBFFCPanel;
    cxlblF_SESSION_ID2: TFVBFFCLabel;
    cxdbseF_SESSION_ID: TFVBFFCDBSpinEdit;
    cxlblF_PROGRAM_NAME1: TFVBFFCLabel;
    cxdbbeF_PROGRAM_NAME: TFVBFFCDBButtonEdit;
    cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel;
    cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit;
    cxlblF_SESSIONGROUP_NAME1: TFVBFFCLabel;
    cxdbbeF_SESSIONGROUP_NAME: TFVBFFCDBButtonEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_CODE1: TFVBFFCLabel;
    cxdbteF_CODE: TFVBFFCDBTextEdit;
    cxlblF_DURATION1: TFVBFFCLabel;
    cxdbseF_DURATION: TFVBFFCDBSpinEdit;
    cxlblF_DURATION_DAYS1: TFVBFFCLabel;
    cxdbseF_DURATION_DAYS: TFVBFFCDBSpinEdit;
    cxlblF_MINIMUM1: TFVBFFCLabel;
    cxdbseF_MINIMUM: TFVBFFCDBSpinEdit;
    cxlblF_MAXIMUM1: TFVBFFCLabel;
    cxdbseF_MAXIMUM: TFVBFFCDBSpinEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE: TFVBFFCDBDateEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE: TFVBFFCDBDateEdit;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    cxdbbeF_STATUS_NAME: TFVBFFCDBButtonEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_COMMENT1: TFVBFFCLabel;
    cxdbmmoF_COMMENT: TFVBFFCDBMemo;
    cxlblF_START_HOUR1: TFVBFFCLabel;
    cxdbteF_START_HOUR: TFVBFFCDBTextEdit;
    cxlblF_INFRASTRUCTURE_NAME1: TFVBFFCLabel;
    cxdbbeF_INFRASTRUCTURE_NAME: TFVBFFCDBButtonEdit;
    acShowEnrolments: TAction;
    dxnbiEnrolment: TdxNavBarItem;
    dxnbiExpenses: TdxNavBarItem;
    acShowExpenses: TAction;
    dxnbiLogHistory: TdxNavBarItem;
    acShowLogHistory: TAction;
    cxgbFVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlblF_PRICE_WORKER: TFVBFFCLabel;
    cxlblF_MIN_DAGEN: TFVBFFCLabel;
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    cxdbcueF_PRICE_OTHER: TFVBFFCDBCurrencyEdit;
    cxdbcueF_PRICE_CLERK: TFVBFFCDBCurrencyEdit;
    cxdbcueF_PRICE_WORKER: TFVBFFCDBCurrencyEdit;
    cxbtnRecalcEnrolments: TFVBFFCButton;
    cxlblF_ANA_SLEUTEL: TFVBFFCLabel;
    dxnbiDocCente: TdxNavBarItem;
    acShowDocCenter: TAction;
    cxdbicbF_ANA_SLEUTEL: TFVBFFCDBImageComboBox;
    cxbtnSelectDays: TFVBFFCButton;
    cxmmoF_SES_DAYS: TFVBFFCMemo;
    cxlblF_TK_FVB2: TFVBFFCLabel;
    cxlblF_TK_CEVORA2: TFVBFFCLabel;
    cxlblF_BT_FVB2: TFVBFFCLabel;
    cxlblF_BT_CEVORA2: TFVBFFCLabel;
    F_BT_CEVORA1: TFVBFFCDBCurrencyEdit;
    F_BT_FVB1: TFVBFFCDBCurrencyEdit;
    F_TK_CEVORA1: TFVBFFCDBCurrencyEdit;
    F_TK_FVB1: TFVBFFCDBCurrencyEdit;
    cxlblF_PRICE_FIXED1: TFVBFFCLabel;
    F_PRICE_FIXED1: TFVBFFCDBCurrencyEdit;
    FVBFFCDBLabel1: TFVBFFCDBLabel;
    FVBFFCLabel1: TFVBFFCLabel;
    cxdbmmoF_EXTRA_INFO: TFVBFFCDBMemo;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCLabel3: TFVBFFCLabel;
    F_BT_INDIRECT_COST: TFVBFFCDBCurrencyEdit;
    cxdbcbF_WEB: TFVBFFCDBCheckBox;
    cxlblF_WEB: TFVBFFCLabel;
    FVBFFCDBCheckBox1: TFVBFFCDBCheckBox;
    FVBFFCLabel4: TFVBFFCLabel;
    cxlblF_COOPERATION_NAME: TFVBFFCLabel;
    cxdbbeF_COOPERATION_NAME: TFVBFFCDBButtonEdit;
    acShowKMO_PORT_Payment: TAction;
    dxnbiKMO_PORT_Payment: TdxNavBarItem;
    acShowKMO_PORT_Link_Port_Session: TAction;
    dxnbiKMO_PORT_Link_Port_Session: TdxNavBarItem;
    cxlblF_CODE_EPYC1: TFVBFFCLabel;
    cxdbteF_CODE_EPYC: TFVBFFCDBTextEdit;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    cxlblF_PROGRAMTYPE: TFVBFFCLabel;
    procedure cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_PROGRAM_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_SESSIONGROUP_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_STATUS_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure acShowEnrolmentsExecute(Sender: TObject);
    procedure acShowExpensesExecute(Sender: TObject);
    procedure acShowLogHistoryExecute(Sender: TObject);
    procedure cxbtnOKClick(Sender: TObject);
    procedure cxbtnRecalcEnrolmentsClick(Sender: TObject);
    procedure acShowRecordDetailExecute(Sender: TObject);
    procedure acShowDocCenterExecute(Sender: TObject);
    procedure cxbtnSelectDaysClick(Sender: TObject);
    procedure FVBFFCRecordViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure srcMainDataChange(Sender: TObject; Field: TField);
    procedure cxdbbeF_COOPERATION_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure acShowKMO_PORT_PaymentExecute(Sender: TObject);
    procedure acShowKMO_PORT_Link_Port_SessionExecute(Sender: TObject);
  private
    { Private declarations }
    slSesDays : TstringList;
    procedure CheckReCalculatedDayPriceAgainstEnrolments;
  protected
    { Protected declarations }
    procedure ShowRecordDetail( Sender : TObject ); override;
    procedure ShowDetailListView( Sender : TObject;
                                  aDataModuleClassName : String;
                                  aDockSite            : TWinControl ); override;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_SesSession, Data_EduMainClient, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, unit_EdutecInterfaces,
  Form_FVBFFCBaseRecordView, Unit_PPWFrameWorkRecordView,
  Form_Session_Days;

resourcestring
  rsRecalculate = 'Huidige basisprijs verschilt van berekende basisprijs,' +
                  ' wilt u de huidige basisprijs herrekenen ?';
  rsReDistribute = 'Wilt u de huidige basisprijs toepassen op de reeds bestaande inschrijvingen ?';
  rsCheckCalcFields = 'De berekende dagprijs bedraagt 0, gelieve de noodzakelijke velden correct op te vullen !';
  rsCheckAnaSleutel = 'De analytische sleutel is niet correct opgevuld, gelieve een duidende waarde in te geven !';
  rsCheckIndirectCost = 'De indirecte kost bevat geen waarde of een negatieve.  Gelieve dit te corrigeren !';
  rsCheckAnaSleutelLeerkrachten='Opgelet: U heeft PPV ( 810000 ) als analytische sleutel gekozen samen met een ongeldige sessiegroep.' +#10#13 + 'Bent u zeker dat u wilt verdergaan?';
  rsCheckAnaSleutelLeerlingenEnLeerkrachten='Opgelet: U heeft Scholen ( 800000 ) als analytische sleutel gekozen samen met een ongeldige sessiegroep.' +#10#13 + 'Bent u zeker dat u wilt verdergaan?';
  rsCheckAnaSleutelAndere='Opgelet: U heeft Bedrijven ( 820000 ) als analytische sleutel gekozen samen met een ongeldige sessiegroep.' +#10#13 + 'Bent u zeker dat u wilt verdergaan?';

{ TfrmSesSession_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmSesSession_Record.GetDetailName
  @author     PIFWizard Generated
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmSesSession_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption;
end;

{*****************************************************************************
  Overriden ShowDetailListView in which we will also hide Pricing Scheme info.

  @Name       TfrmSesSession_Record.ShowDetailListView
  @author     slesage
  @param      Sender                 The Object from which the method was
                                     invoked.
  @param      aDataModuleClassName   The ClassName of the DetailDataModule.
  @param      aDockSite              The control in which the DetailListview
                                     should be docked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.ShowDetailListView(Sender: TObject;
  aDataModuleClassName: String; aDockSite: TWinControl);
begin
{
  pnlPriceSchemeInfo.Visible := False;
  pnlPriceSchemeInfoSeperator.Visible := False;
  pnlPriceSchemeInfoTitle.Visible := False;
}
  inherited;
end;

{*****************************************************************************
  Overriden ShowDetailListView in which we will also Show Pricing Scheme info.

  @Name       TfrmSesSession_Record.ShowRecordDetail
  @author     slesage
  @param      Sender   The Object from which the method was invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.ShowRecordDetail(Sender: TObject);
begin
  inherited;
{
  pnlPriceSchemeInfo.Visible := True;
  pnlPriceSchemeInfoSeperator.Visible := True;
  pnlPriceSchemeInfoTitle.Visible := True;
}
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Infrastructure ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmSesSession_Record.cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowInfrastructure( srcMain.DataSet, rvmEdit );
      end;
      1 :
      begin
        aDataModule.SelectInfrastructure( srcMain.DataSet );
      end;
      else
      begin
        aDataModule.ShowInfrastructure( srcMain.DataSet, rvmAdd );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Program ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmSesSession_Record.cxdbbeF_PROGRAM_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.cxdbbeF_PROGRAM_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowProgram( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectProgram( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the User ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmSesSession_Record.cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowUser( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectUser( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Sessiongroup ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmSesSession_Record.cxdbbeF_SESSIONGROUP_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.cxdbbeF_SESSIONGROUP_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowSessionGroup( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectSessionGroup( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Language ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmSesSession_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectLanguage( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Status ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmSesSession_Record.cxdbbeF_STATUS_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.cxdbbeF_STATUS_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowStatus( srcMain.DataSet, rvmEdit );
      end
      else
      begin
        aDataModule.SelectStatus( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This event will be executed when the Form is initialised and will be used
  to Enable or Disable some controls.

  @Name       TfrmSesSession_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      aFrameWorkDataModule   The DataModule assiciated to the Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
//var
//  aSessionDataModule : IEDUSessionDataModule;
//  aCounter : integer;
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUProgProgramDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_PROGRAM_NAME );
    end;
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUSessionGroupDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_SESSIONGROUP_NAME );
    end;
  end;

//  if Assigned( aFrameWorkDataModule ) then
//  begin
//    if Supports( aFrameWorkDataModule, IEDUSessionDataModule, aSessionDataModule ) then
//    begin
//      aCounter := aSessionDataModule.CountEnrolments( srcMain.DataSet );
//      if Mode in [rvmAdd, rvmEdit] then
//      begin
//        cxbtnRecalcEnrolments.Enabled := ( aCounter > 0 );
//      end;
//    end;
//  end;

  cxbtnSelectDays.Enabled := (Mode in [rvmAdd, rvmEdit]);
  cxbtnRecalcEnrolments.Enabled := ( Mode in [rvmAdd, rvmEdit] );

  slSesDays := TstringList.Create;
  slSesDays.CommaText := srcMain.DataSet.FieldByName('F_SES_DAYS').AsString;
  cxmmoF_SES_DAYS.Text :=
    stringReplace(slSesDays.CommaText,',',chr(13),[rfReplaceAll]);
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxmmoF_SES_DAYS );
end;

procedure TfrmSesSession_Record.acShowEnrolmentsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmEnrolment', pnlRecordDetail );
end;

procedure TfrmSesSession_Record.acShowExpensesExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmSesExpenses', pnlRecordDetail );
end;

procedure TfrmSesSession_Record.acShowLogHistoryExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmLogHistory', pnlRecordDetail );
end;

{*******************************************************************************
  Check if a recalculation is needed in order to have an accurate "base-price"

  @Name       TfrmProgProgram_Record.cxbtnOKClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
  @History                  Author                    Description
  28/03/2008          Ivan Van den Bossche            Extra checks (mantis 2345)
********************************************************************************}

procedure TfrmSesSession_Record.cxbtnOKClick(Sender: TObject);
var
  aFrameWorkDataModule : IEDUSessionDataModule;
  aSessionGroupId: Integer;
  aAnaSleutel: String;
  aAnaSleutelMsg: String;
begin

  if ( Mode in [rvmAdd, rvmEdit] ) then
  begin
    if srcMain.DataSet.FieldByName( 'F_ANA_SLEUTEL' ).IsNull or
    ( Trim(srcMain.DataSet.FieldByName( 'F_ANA_SLEUTEL' ).AsString) = '' ) then
    begin
      MessageDlg( rsCheckAnaSleutel, mtError, [ mbOK ], 0 );
      exit;
    end;

    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // Mantis 2345
    aSessionGroupId := srcMain.DataSet.FieldByName('F_SESSIONGROUP_ID').AsInteger;
    aAnaSleutel := srcMain.DataSet.FieldByName('F_ANA_SLEUTEL').AsString;

    if ( aAnaSleutel = ANA_SLEUTEL_LEERKRACHTEN ) and ( aSessionGroupId <> SESSIONGROUP_LEERKRACHTEN ) then
      aAnaSleutelMsg := rsCheckAnaSleutelLeerkrachten
    else if ( aAnaSleutel = ANA_SLEUTEL_LEERLINGEN_EN_LEERKRACHTEN ) and not ( aSessionGroupId in [ SESSIONGROUP_LEERLINGEN_EN_LEERKRACHTEN, SESSIONGROUP_LEERLINGEN] ) then
      aAnaSleutelMsg := rsCheckAnaSleutelLeerlingenEnLeerkrachten
    else if ( aAnaSleutel = ANA_SLEUTEL_ANDERE ) and ( aSessionGroupId in [ SESSIONGROUP_LEERLINGEN_EN_LEERKRACHTEN, SESSIONGROUP_LEERLINGEN, SESSIONGROUP_LEERKRACHTEN] ) then
      aAnaSleutelMsg := rsCheckAnaSleutelAndere;

    if aAnaSleutelMsg <> '' then
    begin
        if MessageDlg( aAnaSleutelMsg, mtWarning, [ mbYes, mbNo ], 0)=mrNo then
          exit;
    end;
    // ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    // The indirect cost should contain at least 0 or a positive value
    if srcMain.DataSet.FieldByName( 'F_INDIRECT_COST' ).IsNull or
    ( srcMain.DataSet.FieldByName( 'F_INDIRECT_COST').AsInteger < 0) then
    begin
        MessageDlg( rsCheckIndirectCost, mtError, [ mbOK ], 0 );
        exit;
    end;

    // Nadat alle wijzigingen zijn weggeschreven gaan we deze ook
    // doorvoeren op de inschrijvingen !!! ( Enkel status updaten )
    if TStatus(srcMain.DataSet.FieldByName( 'F_STATUS_ID' ).AsInteger)
       in [stAttested, stCancelled] then
    begin
      // Zorg ervoor dat alle inschrijvingen ook terug attested gezet worden
      // ipv gefactureerd en dit om ervoor te zorgen dat de factuur terug
      // kan worden aangemaakt ( geexporteerd )
      // Indien geannuleerd dan ook inschrijvingen dit diets maken
      if Assigned( Self.FrameWorkDataModule ) then
      begin
        if Supports(
          Self.FrameWorkDataModule,
          IEDUSessionDataModule,
          aFrameWorkDataModule ) then
        begin
          aFrameWorkDataModule.ResetStatus( srcMain.DataSet, srcMain.DataSet.FieldByName( 'F_STATUS_ID' ).AsInteger );
        end;
      end;
    end;
    // Extra controle omdat voor ��n dag we niet het extra scherm gaan openen ...
    if (srcMain.DataSet.FieldByName('F_START_DATE').AsDateTime =
       srcMain.DataSet.FieldByName('F_END_DATE').AsDateTime)
       and (srcMain.DataSet.FieldByName( 'F_SES_DAYS' ).AsString <>
       FormatDateTime('c', srcMain.DataSet.FieldByName('F_START_DATE').AsDateTime))
    then
    begin
      if not ( srcMain.DataSet.State in [dsInsert, dsEdit] ) then
      begin
        srcMain.DataSet.Edit;
      end;
      srcMain.DataSet.FieldByName( 'F_SES_DAYS' ).AsString :=
        FormatDateTime('c', srcMain.DataSet.FieldByName('F_START_DATE').AsDateTime);
      cxmmoF_SES_DAYS.Text := srcMain.DataSet.FieldByName( 'F_SES_DAYS' ).AsString;
    end;
    // Voor Nieuw en Edit
    Inherited;
    // Controleer even of alle datums in de lijst reeds aanwezig zijn in de
    // corresponderende tabel T_SES_DAYS
    if Assigned( Self.FrameWorkDataModule ) then
    begin
      if Supports(
        Self.FrameWorkDataModule,
        IEDUSessionDataModule,
        aFrameWorkDataModule ) then
      begin
        aFrameWorkDataModule.ResetSessionDays( srcMain.DataSet );
      end;
    end;
  end;

  if ( Mode = rvmDelete ) then
  begin
    Inherited;
  end;

  if ( Sender = cxbtnCancel ) then
  begin
    inherited;
  end;

  if ( Sender = cxbtnApply ) then
  begin
    RefreshVisibleDetailListViews;
  end;

end;

{*****************************************************************************
  Procedure to see if the user would like to submit the changed dayprice to
  all underlying enrolments.

  @Name       TfrmSesSession_Record.CheckReCalculatedDayPriceAgainstEnrolments
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_Record.CheckReCalculatedDayPriceAgainstEnrolments;
var
  aFrameWorkDataModule : IEDUSessionDataModule;
  aCounter : integer;
begin

  if Assigned( Self.FrameWorkDataModule ) then
  begin
    if Supports(
      Self.FrameWorkDataModule,
      IEDUSessionDataModule,
      aFrameWorkDataModule ) then
    begin
      aCounter := aFrameWorkDataModule.CountEnrolments( srcMain.DataSet );
      if ( aCounter > 0 )  then
      begin
        if ( MessageDlg( rsReDistribute,
             mtWarning, [ mbYes, mbNo ], 0 ) = mrYes ) then
        begin
          aFrameWorkDataModule.UpdateEnrolments(
            srcMain.DataSet,
            srcMain.DataSet.fieldByName ( 'F_PRICE_WORKER' ).AsFloat,
            srcMain.DataSet.fieldByName ( 'F_PRICE_CLERK' ).AsFloat,
            srcMain.DataSet.fieldByName ( 'F_PRICE_OTHER' ).AsFloat
          );
        end;
      end
      else
        cxbtnRecalcEnrolments.Enabled := False;
    end;
  end;

end;

procedure TfrmSesSession_Record.cxbtnRecalcEnrolmentsClick(
  Sender: TObject);
begin
  inherited;

  if Mode in [rvmAdd, rvmEdit] then
  begin
    CheckReCalculatedDayPriceAgainstEnrolments;
  end;

end;

procedure TfrmSesSession_Record.acShowRecordDetailExecute(Sender: TObject);
var
  aSessionDataModule : IEDUSessionDataModule;
  aCounter, aStatusId : integer;
begin
  inherited;

  // Controleer even of er nog Enrolments zijn anders knop afzetten of omgekeerd
  if Assigned( self.FrameWorkDataModule ) then
  begin
    if Supports(
      self.FrameWorkDataModule,
      IEDUSessionDataModule,
      aSessionDataModule ) then
    begin
      aCounter := aSessionDataModule.CountEnrolments( srcMain.DataSet );
      cxbtnRecalcEnrolments.Enabled := ( aCounter > 0 );
      // Controleer status op scherm t.o.v. status in database ( kan gewijzigd zijn )
      // Indien gewijzigd dan gaan we dataset ook in wijziging zetten
      if Mode in [rvmAdd, rvmEdit] then
      begin
        aStatusId := aSessionDataModule.CheckStatus( srcMain.DataSet );
        if aStatusId <> srcMain.DataSet.FieldByName( 'F_STATUS_ID' ).AsInteger then
        begin
          // Zet status op correcte waarde
          if not ( srcMain.DataSet.State in [dsInsert, dsEdit] ) then
          begin
            srcMain.DataSet.Edit;
          end;
          srcMain.DataSet.FieldByName( 'F_STATUS_ID' ).AsInteger := aStatusId;
          srcMain.DataSet.FieldByName( 'F_STATUS_NAME' ).AsString :=
            dtmEDUMainClient.GetNameForState( aStatusId );
          // Zorg er ook voor dat de listview automatisch wordt bijgewerkt
          self.FrameWorkDataModule.ListViewDataModule.RefreshListDataSet;
        end;
      end;
    end;
  end;
end;

procedure TfrmSesSession_Record.acShowDocCenterExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmDocCenter', pnlRecordDetail );
end;

procedure TfrmSesSession_Record.cxbtnSelectDaysClick(Sender: TObject);
var
  frmSesDays : TfrmSessionDays;
begin
  inherited;
  // We gaan hier een niet gerelateerd scherm openen en de gegevens
  // doorgeven en ophalen die we nodig hebben ( modal getoond )
  // Input gegevens zijn : Start datum en Eind datum
  // Output gegevens zijn : een stringlist met alle geselecteerde datums in
  // Deze stringlist zal dan gekoppeld worden aan het memo-veld F_SES_DAYS
  // zodat dit door de dataset gedetecteerd wordt en kan opgenomen worden
  // in de database
  // Om er echter ook voor te zorgen dat alle dagen apart opgenomen worden
  // gaan we ervoor zorgen dat deze dagen ook in een aparte tabel opgenomen
  // worden ( maar eerst gaan we deze leegmaken ( opletten dus !!! )
  // Om vlot te werken gaan we ook de stringlist op voorhand doorgeven
  // en deze opvullen met de reeds aanwezige data
  frmSesDays := TfrmSessionDays.Create(self);
  try
    frmSesDays.dteStartDate.Date := cxdbdeF_START_DATE.Date;
    frmSesDays.dteEndDate.Date := cxdbdeF_END_DATE.Date;
    frmSesDays.SelectedDays :=
      srcMain.DataSet.FieldByName('F_SES_DAYS').AsString;

    frmSesDays.ShowModal;
    if frmSesDays.ModalResult = mrOk then
    begin
      // Neem de geselecteerde string over
      slSesDays.CommaText := frmSesDays.SelectedDays;
      // Controleer even of de string verschilt van de voorgaande
      // vooraleer deze weg te schrijven naar de dataset ...
      if srcMain.DataSet.FieldByName('F_SES_DAYS').AsString
         <> slSesDays.CommaText then
      begin
        cxmmoF_SES_DAYS.Text :=
          stringReplace(slSesDays.CommaText,',',chr(10),[rfReplaceAll]);
        if not ( srcMain.DataSet.State in [dsInsert, dsEdit] ) then
        begin
          srcMain.DataSet.Edit;
        end;
        srcMain.DataSet.FieldByName('F_SES_DAYS').AsString :=
          slSesDays.CommaText;
      end;
    end;
  finally
    freeAndNil(frmSesDays);
  end;
end;

procedure TfrmSesSession_Record.FVBFFCRecordViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  freeAndNil(slSesDays);
  inherited;
end;

procedure TfrmSesSession_Record.srcMainDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;

    // Fix MANTIS 3248
    // If checkbox Virtual has been enabled by the user, the startdate and enddate are not Required.
    // Changed by Ivan Van den Bossche (11/07/2008)
    if Field <> nil then
    begin
      if Field.FieldName = 'F_VIRTUAL_SESSION' then
      begin
        if Field.AsBoolean = True then
        begin
          srcMain.DataSet.FieldByName('F_START_DATE').Required := False;
          srcMain.DataSet.FieldByName('F_END_DATE').Required := False;
        end else
        begin
          srcMain.DataSet.FieldByName('F_START_DATE').Required := True;
          srcMain.DataSet.FieldByName('F_END_DATE').Required := True;
        end;

      end;

    end;
end;

procedure TfrmSesSession_Record.cxdbbeF_COOPERATION_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowCooperation( srcMain.DataSet, rvmEdit );
      end;
      1 :
      begin
        aDataModule.SelectCooperation( srcMain.DataSet );
      end;
      else
      begin
        aDataModule.ShowCooperation( srcMain.DataSet, rvmAdd );
      end;
    end;
  end;
end;

procedure TfrmSesSession_Record.acShowKMO_PORT_PaymentExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmKMO_PORT_Payment', pnlRecordDetail );
end;

procedure TfrmSesSession_Record.acShowKMO_PORT_Link_Port_SessionExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmKMO_PORT_Link_Port_Session', pnlRecordDetail );
end;

end.
