unit Form_Session_Days;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, cxLabel, Unit_FVBFFCDevExpress,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxGroupBox, cxCheckGroup, cxCheckBox,
  ExtCtrls, Unit_FVBFFCFoldablePanel;

type
  TfrmSessionDays = class(TForm)
    pnlDays: TFVBFFCPanel;
    pnlHeader: TFVBFFCPanel;
    chkgrpDays: TFVBFFCCheckGroup;
    pnlActions: TFVBFFCPanel;
    cxbtnApply: TFVBFFCButton;
    cxbtnCancel: TFVBFFCButton;
    dteStartDate: TFVBFFCDateEdit;
    dteEndDate: TFVBFFCDateEdit;
    cxlblStartDate: TFVBFFCLabel;
    cxlblEndDate: TFVBFFCLabel;
    procedure FormShow(Sender: TObject);
  private
    fSelectedDays : string;
    function GetSelectedDays: string;
    procedure SetSelectedDays(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    property SelectedDays : string read GetSelectedDays write SetSelectedDays;
  end;

var
  frmSessionDays: TfrmSessionDays;

implementation

uses Data_EduMainClient, DateUtils;

{$R *.dfm}

procedure TfrmSessionDays.FormShow(Sender: TObject);
var
 i, cnt, wkday : integer;
 aItem : TcxCheckGroupItem;
begin
 cnt := DaysBetween(dteStartDate.Date, dteEndDate.Date);
 for i := 0 to cnt do
 begin
  aItem := chkgrpDays.Properties.Items.Add;
  aItem.Caption := FormatDateTime('dddd c',dteStartDate.Date + i);
  if fSelectedDays = '' then
  begin
    wkday := DayOfWeek(dteStartDate.Date + i);
    // TcxCheckBoxState = (cbsUnchecked, cbsChecked, cbsGrayed);
    if (wkDay = 1) or (wkDay = 7) then
    begin
      chkgrpDays.States[aItem.Index] := cbsUnchecked;
    end
    else
    begin
      chkgrpDays.States[aItem.Index] := cbsChecked;
    end;
  end
  else
  begin
    if pos(formatDateTime('c',dteStartDate.Date + i),fSelectedDays) = 0 then
    begin
      chkgrpDays.States[aItem.Index] := cbsUnchecked;
    end
    else
    begin
      chkgrpDays.States[aItem.Index] := cbsChecked;
    end;
  end;
 end;
end;

function TfrmSessionDays.GetSelectedDays: string;
var
 i : integer;
begin
 Result := '';
 // Overloop alle items en zet de juiste data op een rijtje
 for i := 0 to Pred(chkgrpDays.Properties.Items.Count) do
 begin
  if chkgrpDays.States[i] = cbsChecked then
  begin
    Result := Result + FormatDateTime('c',dteStartDate.Date + i) + ',';
  end;
 end;
 // Haal achterste ',' weg - is ni mooi dus mag verdwijnen
 if Trim(Result) <> '' then
 begin
   Result := copy(Result,1,length(Result) -1);
 end;
end;

procedure TfrmSessionDays.SetSelectedDays(const Value: string);
begin
  fSelectedDays := Value;  
end;

end.
