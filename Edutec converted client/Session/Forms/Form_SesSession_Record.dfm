inherited frmSesSession_Record: TfrmSesSession_Record
  Left = 338
  Top = 89
  Height = 600
  Caption = 'Sessie'
  ClientHeight = 561
  ClientWidth = 796
  Constraints.MinHeight = 600
  Constraints.MinWidth = 812
  ExplicitWidth = 812
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 531
    Width = 796
    Height = 30
    ExplicitTop = 531
    ExplicitWidth = 796
    ExplicitHeight = 30
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 460
      Height = 30
      ExplicitLeft = 460
      ExplicitHeight = 30
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 796
    Height = 531
    ExplicitWidth = 796
    ExplicitHeight = 531
    inherited pnlRecord: TFVBFFCPanel
      Width = 634
      Height = 531
      ExplicitWidth = 634
      ExplicitHeight = 531
      inherited pnlRecordHeader: TFVBFFCPanel
        Top = 0
        Width = 634
        ExplicitTop = 0
        ExplicitWidth = 634
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Top = 25
        Width = 634
        ExplicitTop = 25
        ExplicitWidth = 634
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 634
        ExplicitWidth = 634
        object cxlblF_SESSION_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSesSession_Record.F_SESSION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Sessie ( ID )'
          FocusControl = cxdblblF_SESSION_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SESSION_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSesSession_Record.F_SESSION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_SESSION_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object cxlblF_NAME1: TFVBFFCLabel
          Left = 136
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSesSession_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Sessie'
          FocusControl = cxdblblF_NAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 136
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSesSession_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 289
        end
        object FVBFFCDBLabel1: TFVBFFCDBLabel
          Left = 440
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSesSession_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_START_DATE'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object FVBFFCLabel1: TFVBFFCLabel
          Left = 440
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSesSession_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Startdatum'
          FocusControl = FVBFFCDBLabel1
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 634
        ExplicitWidth = 634
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 525
        Width = 634
        Height = 6
        ExplicitTop = 525
        ExplicitWidth = 634
        ExplicitHeight = 6
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 634
        Height = 455
        ExplicitWidth = 634
        ExplicitHeight = 455
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 634
          ExplicitWidth = 634
        end
        inherited sbxMain: TScrollBox
          Width = 634
          Height = 426
          ExplicitWidth = 634
          ExplicitHeight = 426
          object cxlblF_SESSION_ID2: TFVBFFCLabel
            Left = 8
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_SESSION_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie ( ID )'
            FocusControl = cxdbseF_SESSION_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_SESSION_ID: TFVBFFCDBSpinEdit
            Left = 112
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_SESSION_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_SESSION_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 137
          end
          object cxlblF_PROGRAM_NAME1: TFVBFFCLabel
            Left = 384
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_PROGRAM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programma'
            FocusControl = cxdbbeF_PROGRAM_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_PROGRAM_NAME: TFVBFFCDBButtonEdit
            Left = 472
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_PROGRAM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PROGRAM_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PROGRAM_NAMEPropertiesButtonClick
            TabOrder = 15
            Width = 320
          end
          object cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel
            Left = 384
            Top = 77
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_COMPLETE_USERNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gebruiker'
            FocusControl = cxdbbeF_COMPLETE_USERNAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit
            Left = 472
            Top = 77
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_COMPLETE_USERNAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_COMPLETE_USERNAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick
            TabOrder = 17
            Width = 320
          end
          object cxlblF_SESSIONGROUP_NAME1: TFVBFFCLabel
            Left = 8
            Top = 98
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_SESSIONGROUP_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessiegroep'
            FocusControl = cxdbbeF_SESSIONGROUP_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_SESSIONGROUP_NAME: TFVBFFCDBButtonEdit
            Left = 112
            Top = 98
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_SESSIONGROUP_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_SESSIONGROUP_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_SESSIONGROUP_NAMEPropertiesButtonClick
            TabOrder = 4
            Width = 264
          end
          object cxlblF_NAME2: TFVBFFCLabel
            Left = 384
            Top = 53
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie Naam'
            FocusControl = cxdbteF_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME: TFVBFFCDBTextEdit
            Left = 472
            Top = 53
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_NAME'
            DataBinding.DataField = 'F_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 16
            Width = 320
          end
          object cxlblF_CODE1: TFVBFFCLabel
            Left = 8
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Referentie'
            FocusControl = cxdbteF_CODE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CODE: TFVBFFCDBTextEdit
            Left = 112
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_CODE'
            DataBinding.DataField = 'F_CODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 3
            Width = 264
          end
          object cxlblF_DURATION1: TFVBFFCLabel
            Left = 8
            Top = 122
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_DURATION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Aantal uren totaal'
            FocusControl = cxdbseF_DURATION
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_DURATION: TFVBFFCDBSpinEdit
            Left = 112
            Top = 122
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_DURATION'
            DataBinding.DataField = 'F_DURATION'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 5
            Width = 80
          end
          object cxlblF_DURATION_DAYS1: TFVBFFCLabel
            Left = 200
            Top = 122
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_DURATION_DAYS'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Aantal dagen'
            FocusControl = cxdbseF_DURATION_DAYS
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_DURATION_DAYS: TFVBFFCDBSpinEdit
            Left = 296
            Top = 122
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_DURATION_DAYS'
            DataBinding.DataField = 'F_DURATION_DAYS'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 6
            Width = 80
          end
          object cxlblF_MINIMUM1: TFVBFFCLabel
            Left = 384
            Top = 125
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_MINIMUM'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Minimum'
            FocusControl = cxdbseF_MINIMUM
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_MINIMUM: TFVBFFCDBSpinEdit
            Left = 472
            Top = 125
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_MINIMUM'
            DataBinding.DataField = 'F_MINIMUM'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 19
            Width = 120
          end
          object cxlblF_MAXIMUM1: TFVBFFCLabel
            Left = 608
            Top = 125
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_MAXIMUM'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Maximum'
            FocusControl = cxdbseF_MAXIMUM
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_MAXIMUM: TFVBFFCDBSpinEdit
            Left = 672
            Top = 125
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_MAXIMUM'
            DataBinding.DataField = 'F_MAXIMUM'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 20
            Width = 120
          end
          object cxlblF_START_DATE1: TFVBFFCLabel
            Left = 8
            Top = 170
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_START_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Start Datum'
            FocusControl = cxdbdeF_START_DATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_START_DATE: TFVBFFCDBDateEdit
            Left = 112
            Top = 170
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_START_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_START_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 8
            Width = 137
          end
          object cxlblF_END_DATE1: TFVBFFCLabel
            Left = 8
            Top = 194
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Eind Datum'
            FocusControl = cxdbdeF_END_DATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_END_DATE: TFVBFFCDBDateEdit
            Left = 112
            Top = 194
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_END_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 9
            Width = 137
          end
          object cxlblF_STATUS_NAME1: TFVBFFCLabel
            Left = 8
            Top = 218
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Status'
            FocusControl = cxdbbeF_STATUS_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_STATUS_NAME: TFVBFFCDBButtonEdit
            Left = 112
            Top = 218
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_STATUS_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_STATUS_NAMEPropertiesButtonClick
            TabOrder = 10
            Width = 264
          end
          object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
            Left = 384
            Top = 101
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Taal'
            FocusControl = cxdbbeF_LANGUAGE_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
            Left = 472
            Top = 101
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_LANGUAGE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
            TabOrder = 18
            Width = 320
          end
          object cxlblF_COMMENT1: TFVBFFCLabel
            Left = 8
            Top = 265
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Opmerking'
            FocusControl = cxdbmmoF_COMMENT
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_COMMENT: TFVBFFCDBMemo
            Left = 8
            Top = 287
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_COMMENT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 11
            Height = 114
            Width = 369
          end
          object cxlblF_START_HOUR1: TFVBFFCLabel
            Left = 8
            Top = 146
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_START_HOUR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Start/Eind uur'
            FocusControl = cxdbteF_START_HOUR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_START_HOUR: TFVBFFCDBTextEdit
            Left = 112
            Top = 146
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_START_HOUR'
            DataBinding.DataField = 'F_START_HOUR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 7
            Width = 265
          end
          object cxlblF_INFRASTRUCTURE_NAME1: TFVBFFCLabel
            Left = 8
            Top = 26
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_INFRASTRUCTURE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Infrastructuur'
            FocusControl = cxdbbeF_INFRASTRUCTURE_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_INFRASTRUCTURE_NAME: TFVBFFCDBButtonEdit
            Left = 112
            Top = 26
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_INFRASTRUCTURE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectAddLookup
            DataBinding.DataField = 'F_INFRASTRUCTURE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick
            TabOrder = 2
            Width = 264
          end
          object cxgbFVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 384
            Top = 253
            Caption = 'Prijsbepaling'
            ParentColor = False
            ParentFont = False
            TabOrder = 21
            Height = 150
            Width = 408
            object cxlblF_PRICE_WORKER: TFVBFFCLabel
              Left = 16
              Top = 24
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Sessieprijs (arbeider)'
              FocusControl = cxdbcueF_PRICE_WORKER
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_MIN_DAGEN: TFVBFFCLabel
              Left = 16
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_PRICE_SESSION'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Sessieprijs (bediende)'
              FocusControl = cxdbcueF_PRICE_CLERK
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblFVBFFCLabel2: TFVBFFCLabel
              Left = 16
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_PRICE_SESSION'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Sessieprijs (overige)'
              FocusControl = cxdbcueF_PRICE_OTHER
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcueF_PRICE_OTHER: TFVBFFCDBCurrencyEdit
              Left = 256
              Top = 72
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_OTHER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.AssignedValues.DisplayFormat = True
              Properties.DecimalPlaces = 0
              TabOrder = 2
              Width = 137
            end
            object cxdbcueF_PRICE_CLERK: TFVBFFCDBCurrencyEdit
              Left = 256
              Top = 48
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_CLERK'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.AssignedValues.DisplayFormat = True
              Properties.DecimalPlaces = 0
              TabOrder = 1
              Width = 137
            end
            object cxdbcueF_PRICE_WORKER: TFVBFFCDBCurrencyEdit
              Left = 256
              Top = 24
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_WORKER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 0
              Width = 137
            end
            object cxbtnRecalcEnrolments: TFVBFFCButton
              Left = 16
              Top = 96
              Width = 377
              Height = 25
              Caption = 'Sessieprijzen toepassen op alle inschrijvingen ?'
              OptionsImage.Margin = 10
              OptionsImage.Spacing = -1
              TabOrder = 6
              OnClick = cxbtnRecalcEnrolmentsClick
            end
            object cxlblF_PRICE_FIXED1: TFVBFFCLabel
              Left = 16
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_PRICE_FIXED'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Sessie Prijs'
              FocusControl = F_PRICE_FIXED1
              ParentColor = False
              ParentFont = False
              Transparent = True
            end
            object F_PRICE_FIXED1: TFVBFFCDBCurrencyEdit
              Left = 256
              Top = 125
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_PRICE_FIXED'
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_FIXED'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 8
              Width = 137
            end
          end
          object cxlblF_ANA_SLEUTEL: TFVBFFCLabel
            Left = 384
            Top = 157
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_ANA_SLEUTEL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Analytische sleutel'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbicbF_ANA_SLEUTEL: TFVBFFCDBImageComboBox
            Left = 511
            Top = 149
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_ANA_SLEUTEL'
            DataBinding.DataField = 'F_ANA_SLEUTEL'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Alignment.Horz = taCenter
            Properties.Items = <
              item
                Description = 'Niets geselecteerd'
                ImageIndex = 0
                Value = ''
              end
              item
                Description = 'Scholen ( 800000 )'
                ImageIndex = 0
                Value = '800000'
              end
              item
                Description = 'Bedrijven ( 820000 )'
                Value = '820000'
              end
              item
                Description = 'PPV ( 810000 )'
                Value = '810000'
              end>
            TabOrder = 44
            Width = 281
          end
          object cxbtnSelectDays: TFVBFFCButton
            Left = 256
            Top = 170
            Width = 121
            Height = 45
            Caption = 'Selecteer alle opleidingsdagen'
            OptionsImage.Margin = 10
            OptionsImage.Spacing = -1
            TabOrder = 45
            WordWrap = True
            OnClick = cxbtnSelectDaysClick
          end
          object cxmmoF_SES_DAYS: TFVBFFCMemo
            Left = 384
            Top = 173
            Lines.Strings = (
              'cxmmoF_SES_D'
              'AY'
              'S')
            ParentFont = False
            Properties.ScrollBars = ssVertical
            Style.StyleController = dtmEDUMainClient.cxescReadOnly
            TabOrder = 46
            Height = 73
            Width = 129
          end
          object cxlblF_TK_FVB2: TFVBFFCLabel
            Left = 88
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Tussenkomst FVB (1u)'
            FocusControl = F_TK_FVB1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object cxlblF_TK_CEVORA2: TFVBFFCLabel
            Left = 88
            Top = 444
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Tussenkomst Cevora (4u)'
            FocusControl = F_TK_CEVORA1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object cxlblF_BT_FVB2: TFVBFFCLabel
            Left = 384
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Boete FVB (4u)'
            FocusControl = F_BT_FVB1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object cxlblF_BT_CEVORA2: TFVBFFCLabel
            Left = 384
            Top = 444
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Boete Cevora (4u)'
            FocusControl = F_BT_CEVORA1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_BT_CEVORA1: TFVBFFCDBCurrencyEdit
            Left = 505
            Top = 444
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_CEVORA'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_BT_CEVORA'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 23
            Width = 121
          end
          object F_BT_FVB1: TFVBFFCDBCurrencyEdit
            Left = 505
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_BT_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_BT_FVB'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 22
            Width = 121
          end
          object F_TK_CEVORA1: TFVBFFCDBCurrencyEdit
            Left = 257
            Top = 444
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_TK_CEVORA'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 14
            Width = 121
          end
          object F_TK_FVB1: TFVBFFCDBCurrencyEdit
            Left = 257
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_TK_FVB'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_TK_FVB'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 13
            Width = 121
          end
          object cxdbmmoF_EXTRA_INFO: TFVBFFCDBMemo
            Left = 80
            Top = 469
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_EXTRA_INFO'
            TabStop = False
            DataBinding.DataField = 'F_EXTRA_INFO'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = False
            TabOrder = 26
            Height = 25
            Width = 713
          end
          object FVBFFCLabel2: TFVBFFCLabel
            Left = 8
            Top = 468
            HelpType = htKeyword
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Extra info:'
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object FVBFFCLabel3: TFVBFFCLabel
            Left = 632
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_INDIRECT_COST'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Indirecte kost'
            FocusControl = F_BT_INDIRECT_COST
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_BT_INDIRECT_COST: TFVBFFCDBCurrencyEdit
            Left = 721
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_INDIRECT_COST'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_INDIRECT_COST'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.AssignedValues.MinValue = True
            TabOrder = 24
            Width = 72
          end
          object cxdbcbF_WEB: TFVBFFCDBCheckBox
            Left = 720
            Top = 444
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_WEB'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_WEB'
            DataBinding.DataField = 'F_WEB'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 25
          end
          object cxlblF_WEB: TFVBFFCLabel
            Left = 632
            Top = 444
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_WEB'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Web'
            FocusControl = cxdbcbF_WEB
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCDBCheckBox1: TFVBFFCDBCheckBox
            Left = 48
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_VIRTUAL_SESSION'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_WEB'
            DataBinding.DataField = 'F_VIRTUAL_SESSION'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 12
          end
          object FVBFFCLabel4: TFVBFFCLabel
            Left = 8
            Top = 420
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_WEB'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Virtual'
            FocusControl = cxdbcbF_WEB
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_COOPERATION_NAME: TFVBFFCLabel
            Left = 8
            Top = 242
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_COOPERATION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'I.s.m.'
            FocusControl = cxdbbeF_COOPERATION_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COOPERATION_NAME: TFVBFFCDBButtonEdit
            Left = 112
            Top = 242
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_COOPERATION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectAddLookup
            DataBinding.DataField = 'F_COOPERATION_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COOPERATION_NAMEPropertiesButtonClick
            TabOrder = 56
            Width = 264
          end
          object cxlblF_CODE_EPYC1: TFVBFFCLabel
            Left = 8
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Referentie EPYC'
            FocusControl = cxdbteF_CODE_EPYC
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CODE_EPYC: TFVBFFCDBTextEdit
            Left = 112
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_CODE_EPYC'
            DataBinding.DataField = 'F_CODE_EPYC'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 58
            Width = 264
          end
          object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
            Left = 488
            Top = 29
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_PROGRAM_TYPE_NAME'
            DataBinding.DataField = 'F_PROGRAM_TYPE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 59
            Width = 304
          end
          object cxlblF_PROGRAMTYPE: TFVBFFCLabel
            Left = 384
            Top = 29
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programmatype'
            FocusControl = FVBFFCDBTextEdit1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
        end
        object pnlPriceSchemeInfoSeperator: TFVBFFCPanel
          Left = 0
          Top = 451
          Width = 634
          Height = 4
          Align = alBottom
          BevelOuter = bvNone
          BorderWidth = 4
          ParentColor = True
          TabOrder = 2
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clInactiveCaption
          StyleClientArea.BackColor2 = clInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 792
      Height = 531
      ExplicitLeft = 792
      ExplicitHeight = 531
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 531
      ExplicitHeight = 531
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiEnrolment
          end
          item
            Item = dxnbiExpenses
          end
          item
            Item = dxnbiDocCente
          end
          item
            Item = dxnbiLogHistory
          end
          item
            Item = dxnbiKMO_PORT_Link_Port_Session
          end
          item
            Item = dxnbiKMO_PORT_Payment
          end>
      end
      object dxnbiEnrolment: TdxNavBarItem
        Action = acShowEnrolments
      end
      object dxnbiExpenses: TdxNavBarItem
        Action = acShowExpenses
      end
      object dxnbiLogHistory: TdxNavBarItem
        Action = acShowLogHistory
      end
      object dxnbiDocCente: TdxNavBarItem
        Action = acShowDocCenter
      end
      object dxnbiKMO_PORT_Payment: TdxNavBarItem
        Action = acShowKMO_PORT_Payment
      end
      object dxnbiKMO_PORT_Link_Port_Session: TdxNavBarItem
        Action = acShowKMO_PORT_Link_Port_Session
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 531
      ExplicitHeight = 531
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowEnrolments: TAction
      Caption = 'Inschrijvingen'
      OnExecute = acShowEnrolmentsExecute
    end
    object acShowExpenses: TAction
      Caption = 'Uitgaven'
      OnExecute = acShowExpensesExecute
    end
    object acShowLogHistory: TAction
      Caption = 'Historiek'
      OnExecute = acShowLogHistoryExecute
    end
    object acShowDocCenter: TAction
      Caption = 'Lesgevers'
      OnExecute = acShowDocCenterExecute
    end
    object acShowKMO_PORT_Payment: TAction
      Caption = 'KMO-Fakturen'
      OnExecute = acShowKMO_PORT_PaymentExecute
    end
    object acShowKMO_PORT_Link_Port_Session: TAction
      Caption = 'KMO-Portefeuilles'
      OnExecute = acShowKMO_PORT_Link_Port_SessionExecute
    end
  end
end
