{*****************************************************************************
  This unit contains the form that will be used to display a list of
  T_SES_SESSION records.

  @Name       Form_SesSession_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/06/2009   tclaesen             Forcedirs on all 4 forlders when openeing 'Dossiermap'
  25/03/2008   ivdbossche           Added F_ENROLMENTS_WEB (Mantis 2298).
  05/03/2008   ivdbossche           Added acSetConfirmReportSent and acSetConfirmReportNotSent.
  25/02/2008   ivdbossche           Added checkboxes - Cursussen verzonden & Deelnemerslijst toegekomen.
                                    Changed SetParticipantsListReceived into SetStatusCheckbox.
                                    Added acOpenFolder.
  12/12/2007   ivdbossche           Added SetParticipantsListReceived and Unset ParticipantsListReceived
  29/10/2007   ivdbossche           Added 'printing evaluation doc functionality'
  19/07/2007   ivdbossche           Added procedures OnReportPreviewCertificateClosed
                                    PrintLetterAndCertificate, MarkAsCertificatesPrinted
                                    acListViewConfirmationReportsExecute
  11/07/2005   slesage              Initial creation of the Unit.

******************************************************************************}

unit Form_SesSession_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxDropDownEdit, cxCalendar, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxContainer, cxLabel, Menus, ActnList,
  Unit_FVBFFCComponents, Unit_PPWFrameWorkActions, Grids, DBGrids,
  cxCheckBox, cxCalc;

type
  TfrmSesSession_List = class(TEDUListView)
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM_NAME: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_CODE: TcxGridDBColumn;
    cxgrdtblvListF_DURATION: TcxGridDBColumn;
    cxgrdtblvListF_DURATION_DAYS: TcxGridDBColumn;
    cxgrdtblvListF_MINIMUM: TcxGridDBColumn;
    cxgrdtblvListF_MAXIMUM: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn;
    cxgrdtblvListF_INFRASTRUCTURE_ID: TcxGridDBColumn;
    cxgrdtblvListF_INFRASTRUCTURE_NAME: TcxGridDBColumn;
    cxlblF_PROGRAM_NAME1: TFVBFFCLabel;
    cxdbbeF_PROGRAM_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME1: TFVBFFCDBTextEdit;
    cxlblF_SESSIONGROUP_NAME1: TFVBFFCLabel;
    cxdbbeF_SESSIONGROUP_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_CODE1: TFVBFFCLabel;
    cxdbteF_CODE1: TFVBFFCDBTextEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE1: TFVBFFCDBDateEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE1: TFVBFFCDBDateEdit;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    cxdbbeF_STATUS_NAME1: TFVBFFCDBButtonEdit;
    cxgrdtblvListF_ANA_SLEUTEL: TcxGridDBColumn;
    alEnrollment: TFVBFFCActionList;
    acPrintReport: TAction;
    dxBarButton1: TdxBarButton;
    prdDialog: TPrintDialog;
    cxgrdtblvListF_PRICE_WORKER: TcxGridDBColumn;
    cxgrdtblvListF_PRICE_CLERK: TcxGridDBColumn;
    cxgrdtblvListF_PRICE_OTHER: TcxGridDBColumn;
    cxgrdtblvListF_CONSTRUCT_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_SES_AANW_PRINTED: TcxGridDBColumn;
    cxgrdtblvListF_ALL_CERT_PRINTED: TcxGridDBColumn;
    acListViewPreviewLetterCertificateAction: TAction;
    acListViewPrintLetterCertificateAction: TAction;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton2: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    acPrintConfirmationReports: TAction;
    cxgrdtblvListF_NO_PARTICIPANTS: TcxGridDBColumn;
    cxgrdtblvListF_WEEK: TcxGridDBColumn;
    acSessionWizard: TAction;
    dxBarButton9: TdxBarButton;
    acPreviewEvaluation: TAction;
    dxBarButton10: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    acPrintEvaluation: TAction;
    dxBarButton11: TdxBarButton;
    cxgrdtblvListF_SESSIONGROUP_NAME: TcxGridDBColumn;
    cxgrdtblvList2: TcxGridDBTableView;
    acSwitchView: TAction;
    dxBarButton6: TdxBarButton;
    cxgrdlvlList2: TcxGridLevel;
    cxgrdtblvList2F_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvList2F_PROGRAM_ID: TcxGridDBColumn;
    cxgrdtblvList2F_PROGRAM_NAME: TcxGridDBColumn;
    cxgrdtblvList2F_NAME: TcxGridDBColumn;
    cxgrdtblvList2F_CODE: TcxGridDBColumn;
    cxgrdtblvList2F_DURATION: TcxGridDBColumn;
    cxgrdtblvList2F_DURATION_DAYS: TcxGridDBColumn;
    cxgrdtblvList2F_MINIMUM: TcxGridDBColumn;
    cxgrdtblvList2F_MAXIMUM: TcxGridDBColumn;
    cxgrdtblvList2F_START_DATE: TcxGridDBColumn;
    cxgrdtblvList2F_END_DATE: TcxGridDBColumn;
    cxgrdtblvList2F_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvList2F_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvList2F_LANGUAGE_ID: TcxGridDBColumn;
    cxgrdtblvList2F_LANGUAGE_NAME: TcxGridDBColumn;
    cxgrdtblvList2F_INFRASTRUCTURE_ID: TcxGridDBColumn;
    cxgrdtblvList2F_INFRASTRUCTURE_NAME: TcxGridDBColumn;
    cxgrdtblvList2F_PRICING_SCHEME: TcxGridDBColumn;
    cxgrdtblvList2F_PRICE_OTHER: TcxGridDBColumn;
    cxgrdtblvList2F_ANA_SLEUTEL: TcxGridDBColumn;
    cxgrdtblvList2F_PRICE_WORKER: TcxGridDBColumn;
    cxgrdtblvList2F_PRICE_CLERK: TcxGridDBColumn;
    cxgrdtblvList2F_CONSTRUCT_COMMENT: TcxGridDBColumn;
    cxgrdtblvList2F_SES_AANW_PRINTED: TcxGridDBColumn;
    cxgrdtblvList2F_ALL_CERT_PRINTED: TcxGridDBColumn;
    cxgrdtblvList2F_WEEK: TcxGridDBColumn;
    cxgrdtblvListF_RECEIVED_PARTICIPANTS_LIST: TcxGridDBColumn;
    acSetParticipantsListReceived: TAction;
    acSetParticipantsListNotReceived: TAction;
    dxBarSubItemStatusDeelnemerslijst: TdxBarSubItem;
    dxBarButton8: TdxBarButton;
    dxBarButton12: TdxBarButton;
    acSetCoursesSent: TAction;
    acSetCoursesNotSent: TAction;
    dxBarSubItemStatusCursussen: TdxBarSubItem;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    cxgrdtblvListF_SENT_COURSE: TcxGridDBColumn;
    cxgrdtblvListF_INSTRUCTOR: TcxGridDBColumn;
    cxgrdtblvList2F_SENT_COURSE: TcxGridDBColumn;
    cxgrdtblvList2F_INSTRUCTOR: TcxGridDBColumn;
    acOpenFolder: TAction;
    dxBarButton18: TdxBarButton;
    cxgrdtblvList2F_RECEIVED_PARTICIPANTS: TcxGridDBColumn;
    cxgrdtblvList2F_SES_DAYS: TcxGridDBColumn;
    cxgrdtblvListF_CONFIRMATION_REPORT_SENT: TcxGridDBColumn;
    acSetConfirmReportSent: TAction;
    acSetConfirmReportNotSent: TAction;
    dxBarSubItemStatusBevestigingsRapport: TdxBarSubItem;
    dxBarButton14: TdxBarButton;
    dxBarButton19: TdxBarButton;
    cxgrdtblvListF_SES_DAYS: TcxGridDBColumn;
    cxgrdtblvListF_ENROLMENTS_WEB: TcxGridDBColumn;
    cxgrdtblvListF_COMMENT: TcxGridDBColumn;
    acSetSatusFacturated: TAction;
    dxBarButton20: TdxBarButton;
    cxgrdtblvListF_COOPERATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_COOPERATION_NAME: TcxGridDBColumn;
    cxlblF_COOPERATION_NAME: TFVBFFCLabel;
    cxdbbeF_COOPERATION_NAME: TFVBFFCDBButtonEdit;
    cxlblF_INFRASTRUCTURE_NAME: TFVBFFCLabel;
    cxdbbeF_INFRASTRUCTURE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_INSTRUCTOR_NAME: TFVBFFCLabel;
    cxdbbeF_INSTRUCTOR_NAME: TFVBFFCDBButtonEdit;
    cxgrdtblvListF_EPYC: TcxGridDBColumn;
    cxgrdtblvListF_EXAMINATOR: TcxGridDBColumn;
    cxgrdtblvListF_ATTESTS_SENT: TcxGridDBColumn;
    cxgrdtblvListF_SCANNING: TcxGridDBColumn;
    dxBarSubItemOpleiding: TdxBarSubItem;
    dxBarSubItemExamen: TdxBarSubItem;
    dxBarSubItem3: TdxBarSubItem;
    dxBarSubItemStatusEpyc: TdxBarSubItem;
    dxBarSubItemStatusExaminator: TdxBarSubItem;
    dxBarSubItemStatusAttesten: TdxBarSubItem;
    dxBarSubItemStatusScanning: TdxBarSubItem;
    dxBarButtonEpycOntvangen: TdxBarButton;
    dxBarButtonEpycNietOntvangen: TdxBarButton;
    dxBarButtonExaminatorOntvangen: TdxBarButton;
    dxBarButtonExaminatorNietOntvangen: TdxBarButton;
    dxBarButtonAttestenVerstuurd: TdxBarButton;
    dxBarButtonAttestenNietVerstuurd: TdxBarButton;
    dxBarButtonScanningVerzonden: TdxBarButton;
    dxBarButtonScanningNietVerzonden: TdxBarButton;
    acSetEpycReceived: TAction;
    acSetEpycNotReceived: TAction;
    acSetExaminatorReceived: TAction;
    acSetExaminatorNotReceived: TAction;
    acSetAttestsSent: TAction;
    acSetAttestsNotSent: TAction;
    acSetScanningReceived: TAction;
    acSetScanningNotReceived: TAction;
    dxBarSubItemRapportenOpleiding: TdxBarSubItem;
    dxBarSubItemRapportenExamen: TdxBarSubItem;
    dxBarButtonDrukBesvestigingsrapport: TdxBarButton;
    dxBarButtonDrukVerslag: TdxBarButton;
    dxBarButtonDrukControlellijst: TdxBarButton;
    dxBarSubItemDrukAttesten: TdxBarSubItem;
    dxBarButtonDrukGlobaalAttest: TdxBarButton;
    dxBarButtonBegeleidendeBrief: TdxBarButton;
    acDrukBevestigingsrapport: TAction;
    acDrukVerslag: TAction;
    acDrukControlelijst: TAction;
    acDrukGlobaalAttest: TAction;
    acDrukBegeleidendeBrief: TAction;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButtonDrukResultatenllijst: TdxBarButton;
    acDrukResultatenlijst: TAction;
    cxgrdtblvListF_PROGRAM_TYPE_NAME: TcxGridDBColumn;
    procedure cxdbbeF_STATUS_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_SESSIONGROUP_NAME1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_PROGRAM_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acPrintReportExecute(Sender: TObject);
    procedure cxgrdtblvListStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cxdbdeF_START_DATE1Exit(Sender: TObject);
    procedure acListViewPreviewLetterCertificateActionExecute(
      Sender: TObject);
    procedure acListViewPrintLetterCertificateActionExecute(
      Sender: TObject);
    procedure acPrintConfirmationReportsExecute(Sender: TObject);
    procedure dxbpmnGridPopup(Sender: TObject);
    procedure FVBFFCListViewShow(Sender: TObject);
    procedure FVBFFCListViewCreate(Sender: TObject);
    procedure cxbtnApplyFilterClick(Sender: TObject);
    procedure acSessionWizardExecute(Sender: TObject);
    procedure acPreviewEvaluationExecute(Sender: TObject);
    procedure acPrintEvaluationExecute(Sender: TObject);
    procedure acSwitchViewExecute(Sender: TObject);
    procedure acSetParticipantsListReceivedExecute(Sender: TObject);
    procedure acSetParticipantsListNotReceivedExecute(Sender: TObject);
    procedure acSetCoursesSentExecute(Sender: TObject);
    procedure acSetCoursesNotSentExecute(Sender: TObject);
    procedure acOpenFolderExecute(Sender: TObject);
    procedure FVBFFCListViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure acSetConfirmReportSentExecute(Sender: TObject);
    procedure acSetConfirmReportNotSentExecute(Sender: TObject);
    procedure acSetSatusFacturatedExecute(Sender: TObject);
    procedure cxdbbeF_COOPERATION_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_INSTRUCTOR_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acSetEpycReceivedExecute(Sender: TObject);
    procedure acSetEpycNotReceivedExecute(Sender: TObject);
    procedure acSetExaminatorReceivedExecute(Sender: TObject);
    procedure acSetExaminatorNotReceivedExecute(Sender: TObject);
    procedure acSetAttestsSentExecute(Sender: TObject);
    procedure acSetAttestsNotSentExecute(Sender: TObject);
    procedure acSetScanningReceivedExecute(Sender: TObject);
    procedure acSetScanningNotReceivedExecute(Sender: TObject);
    procedure acDrukBevestigingsrapportExecute(Sender: TObject);
    procedure acDrukVerslagExecute(Sender: TObject);
    procedure acDrukControlelijstExecute(Sender: TObject);
    procedure acDrukGlobaalAttestExecute(Sender: TObject);
    procedure acDrukBegeleidendeBriefExecute(Sender: TObject);
    procedure acDrukResultatenlijstExecute(Sender: TObject);
  private
    { Private declarations }
    FSelectedSessions: TStringList;
    FSelectedSessionsCommaText: String;
    FSelectedEnrolsCommaText: String;

    // Variables used to change status (Checkboxes)
    FStatusCheckbox: Integer;
    FStatusField: String; // Field(Status) being updated.

    procedure GetSessions;
    procedure SessionReport(const preview: Boolean);
    procedure PrintLetterAndCertificate(const APreview: Boolean);
    procedure PrintEvaluationDoc(const APreview: Boolean);

    procedure OnReportPreviewClosed (Sender: TObject);
    procedure OnReportPreviewCertificateClosed (Sender: TObject);
    procedure OnReportPreviewClosedNoAction (Sender: TObject);
    procedure MarkAsCertificatesPrinted;
    procedure CreateOCR;

    procedure UpdateStatusCheckbox;
    procedure SetStatusCheckbox;

  protected
    procedure OnCanFocusRecord(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; var AAllow: Boolean);
    procedure StoreActiveViewToIni; override;
    procedure RestoreActiveViewFromIni; override;
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;


implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SesSession, Data_EduMainClient, Form_FVBFFCBaseListView,
  Unit_FVBFFCInterfaces, unit_EdutecInterfaces, unit_PPWFrameworkClasses,
  unit_Const, printers, Form_Confirmation_Reports, Unit_FVBFFCUtils, Unit_Global;

{ TfrmSesSession_List }

resourcestring
  rsSessionReportName = 'Aanwezigheidslijst';
  rsCertificateReportName = 'Certficaat';
  rsLetterCertificateReportName = 'Brief Attesten Certificaten';

  rsReportNoRecords = 'Er kan geen enkel aanwezigheidsrapport worden afgedrukt.' + #13#10 + #13#10 +
    'Mogelijk oorzaak: de individuele dagen voor de sessies werden niet gekozen.';
  rsReportNoCertificateRecords = 'Er kan geen enkel certificaat worden afgedrukt.' + #13#10 + #13#10 +
    'Mogelijk oorzaken:' + #13#10 + #13#10 +
    '* De individuele dagen voor de sessies werden niet gekozen. ' + #13#10 +
    '* Alle certificaten zijn reeds afgeprint.' + #13#10 +
    '* Er is niemand die de lesuren volledig heeft gevolgd.';
  rsReportPrintedCertificates='Tenminste ��n certificaat is reeds afgeprint.  Wilt u alles herdrukken?';

  rsReportNotAllRecords = 'Voor %d sessie(s) kan GEEN aanwezigheidsrapport worden afgedrukt.' + #13#10 + #13#10 +
    'Voor deze sessie(s) werden mogelijk geen individuele dagen gekozen.';
  rsReportNotAllCertificateRecords = 'Voor %d sessie(s) kan GEEN certificaat worden afgedrukt.' + #13#10 + #13#10 +
    'Voor deze sessie(s) werden mogelijk geen individuele dagen gekozen.';
  rsMarAanwezigheidslijstPrinted = 'Aanwezigheidslijst(en) als afgedrukt markeren ?';
  rsMarCertificatePrinted = 'Certificate(n) als afgedrukt markeren?';
  rsCreateOCR = 'OCR code voor de gekozen sessie(s) (her)genereren?';
  rsReportNoRecordsEval = 'Voor de gekozen sessie kan GEEN ENKEL evaluatieformulier worden afgeprint omdat er mogelijk geen inschrijvingen zijn van personen die tenminste 1u aanwezig waren en/of geen enkele lesgever is ingebracht.';
  rsUnableToOpenSessionFolder='Dossiermap kan niet geopend worden.  Dit kunnen de redenen zijn:' + #10#13 +
                      '* Voor deze sessie zijn mogelijk nog geen bevestigingsbrieven aangemaakt.' + #10#13 +
                      '* Er is geen toegang tot de folder %s.' + #10#13 +
                      '* Mogelijk is de path veranderd en wordt het dossier op een andere plaats bewaard.' + #10#13 +
                      '* Er is een netwerkstoring.';
  rsEmptySessionRef='De referentie van de sessie is nog niet opgegeven! Gelieve dit aan te passen.';

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmSesSession_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmSesSession_List.GetFilterString: String;
var
  aFilterString : String;
  aDateString   : String;
  startdatefilter, enddatefilter: TDatetime;
begin
  { Add the Condition for the Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_NAME', FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).AsString );
  end;

  { Add the Condition for the Code / Reference }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_CODE' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_CODE', FSearchCriteriaDataSet.FieldByName( 'F_CODE' ).AsString );
  end;

  { Add the condition for the Program }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PROGRAM_ID' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_PROGRAM_ID', FSearchCriteriaDataSet.FieldByName( 'F_PROGRAM_ID' ).AsInteger );
  end;

  { Add the condition for the SessionGroup }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SESSIONGROUP_ID' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_SESSIONGROUP_ID', FSearchCriteriaDataSet.FieldByName( 'F_SESSIONGROUP_ID' ).AsInteger );
  end;

  { Add the condition for the Status }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_STATUS_ID' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_STATUS_ID', FSearchCriteriaDataSet.FieldByName( 'F_STATUS_ID' ).AsInteger );
  end;

  { Add the condition for the Cooperation }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_COOPERATION_ID' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_COOPERATION_ID', FSearchCriteriaDataSet.FieldByName( 'F_COOPERATION_ID' ).AsInteger );
  end;

  { Add the condition for the Infrastructure }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_INFRASTRUCTURE_ID' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_INFRASTRUCTURE_ID', FSearchCriteriaDataSet.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger );
  end;

  { Add the condition for the Instructor }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_INSTRUCTOR_ID' ).IsNull ) then
  begin
    AddFilterCondition ( aFilterString, 'F_SESSION_ID in (Select F_SESSION_ID from V_SES_INSTRUCTOR where F_INSTRUCTOR_ID = ' + FSearchCriteriaDataSet.FieldByName( 'F_INSTRUCTOR_ID' ).AsString + ')', 'AND' );
  end;

  startdatefilter := EncodeDate (1900, 1, 1);
  enddatefilter   := EncodeDate (2039, 12,31);

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).IsNull ) then startdatefilter := FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).AsDateTime;
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).IsNull ) then enddatefilter := FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).AsDateTime;

  AddDateBetweenCondition (aDateString, 'F_START_DATE', startdatefilter, enddatefilter);
  AddFilterCondition( aFilterString, aDateString, 'AND' );

  AddFilterCondition( aFilterString, 'F_START_DATE IS NULL', 'OR');  // Added this to ensure that Virtual Session will be shown.
{
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).IsNull ) and
     not ( FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).IsNull ) then
  begin
    AddDateBetweenCondition ( aDateString, 'F_START_DATE',
                              FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).AsDateTime,
                              FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).AsDateTime );
    AddDateBetweenCondition ( aDateString, 'F_END_DATE',
                              FSearchCriteriaDataSet.FieldByName( 'F_START_DATE' ).AsDateTime,
                              FSearchCriteriaDataSet.FieldByName( 'F_END_DATE' ).AsDateTime, 'OR' );
    AddFilterConditioan( aFilterString, aDateString, 'AND' );
  end;
}

  { Add the condition for the Start and End Date }

  Result := aFilterString;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Status ButtonEdit.  In here we will call the appropriate method on the
  IEDUSessionDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_STATUS_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_List.cxdbbeF_STATUS_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectStatusFilter( FSearchCriteriaDataSet );
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the SessionGroup ButtonEdit.  In here we will call the appropriate method on the
  IEDUSessionDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_SESSIONGROUP_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_List.cxdbbeF_SESSIONGROUP_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectSessionGroup( FSearchCriteriaDataSet );
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Program ButtonEdit.  In here we will call the appropriate method on the
  IEDUSessionDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_PROGRAM_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_List.cxdbbeF_PROGRAM_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectProgram( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmSesSession_List.acPrintReportExecute(Sender: TObject);
begin
  inherited;
  FSelectedSessions := TStringList.Create;
  try
    if TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount > 1 then
    begin //multiple sessions selected
      LoopThroughSelectedRecords(GetSessions);
    end
    else begin //only one session selected
      FSelectedSessions.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    end;

    FSelectedSessionsCommaText := FSelectedSessions.CommaText;
    SessionReport (true);
  finally
    FSelectedSessions := nil; // FSelectedSessions has been freed in TdtmCrystal.PrintReport
      //have to look at this when more time !
  end;
end;

procedure TfrmSesSession_List.acListViewPreviewLetterCertificateActionExecute(
  Sender: TObject);
begin
  inherited;
    PrintLetterAndCertificate(True); // Preview letter(s) and certificate(s) (ivdbossche 17-07-2007)

end;

procedure TfrmSesSession_List.acListViewPrintLetterCertificateActionExecute(
  Sender: TObject);
begin
  inherited;
    PrintLetterAndCertificate(False); // Print letter(s) and certificate(s) (ivdbossche 17-07-2007)
end;

procedure TfrmSesSession_List.acPreviewEvaluationExecute(Sender: TObject);
begin
  inherited;
    PrintEvaluationDoc( True ); // Preview evaluation doc

end;

procedure TfrmSesSession_List.acPrintEvaluationExecute(Sender: TObject);
begin
  inherited;
    PrintEvaluationDoc( False ); // Print evaluation doc

end;

procedure TfrmSesSession_List.GetSessions;
begin
  FSelectedSessions.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
end;

procedure TfrmSesSession_List.SessionReport (const preview: Boolean);
var
  lReportName, lReportTitle: string;
  ParamNames, ParamValues: TStringList;
  oldCursor: TCursor;
  recordcount: Integer;
begin
  //First let's check whether the report will at least be returning 1 record !
  recordcount := dtmEDUMainClient.ReportRecordCount('V_REP_AANWEZIGHEIDSLIJST', 'F_SESSION_ID',
    'F_SESSION_ID', FSelectedSessions);
  if (recordcount = 0) then
  begin
    MessageDlg(rsReportNoRecords, mtError, [mbOk], 0);
    Exit;
  end;

  if (recordcount < FSelectedSessions.Count) then
  begin
    MessageDlg(Format(rsReportNotAllRecords, [FSelectedSessions.Count - recordcount]), mtWarning, [mbOk], 0);
  end;

  lReportName := 'aanwezigheidslijst.rpt';
  lReportTitle := rsSessionReportName;
  ParamNames  := TStringList.Create;
  ParamValues := TStringList.Create;
  oldCursor := screen.cursor;
  screen.cursor := crSQLWait;
  try
    ParamNames.Add('p_session_id');
    ParamValues.AddObject ('', FSelectedSessions);
    dtmEDUMainClient.ExecuteCrystal(lReportName, lReportTitle, ParamNames, ParamValues, preview, True, OnReportPreviewClosed );

  finally
    screen.cursor := oldCursor;
    FreeAndNil (ParamValues);
    FreeAndNil (ParamNames);
    //FreeAndNil(dtm);
  end;
end;

{*****************************************************************************
  This method will be executed when the Report Preview form gets closed.  In
  here we will ask the user if he wants to flag the records as printed.

  @Name       TfrmSesSession_List.OnReportPreviewClosed
  @author     slesage
  @param      Sender: TObject
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesSession_List.OnReportPreviewClosed(Sender: TObject);
var
  aDataModule : IEDUSessionDataModule;
begin
  assert (Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ));

  if MessageDlg(rsMarAanwezigheidslijstPrinted, mtInformation, [mbYes, mbNo], 0) = mrYes then
  begin
    aDataModule.MarkAanwezigheidslijstPrinted(FSelectedSessionsCommaText);
    Self.RefreshData;
  end
end;


{*****************************************************************************
  This method will be executed when the Report Preview form gets closed.  In
  here we will ask the user if he wants to flag the records as printed.

  @Name       TfrmSesSession_List.OnReportPreviewCertificateClosed
  @author     ivdbossche
  @param      Sender: TObject
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure TfrmSesSession_List.OnReportPreviewCertificateClosed(
  Sender: TObject);
var
  aDataModule: IEDUSessionDataModule;
begin
  assert (Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ));
  if MessageDlg(rsMarCertificatePrinted, mtInformation, [mbYes, mbNo], 0) = mrYes then
  begin
    aDataModule.MarkAsCertificatesPrinted(FSelectedEnrolsCommaText);
    Self.RefreshData;
  end;
end;

{**********************************************************************************************************
  This method will be executed when the Certificate & Letter reports gets immediately (no preview) printed.

  @Name       TfrmSesSession_List.OnReportPreviewCertificateClosed
  @author     ivdbossche
  @param      None
  @return     None
  @Exception  None
  @See        None
************************************************************************************************************}
procedure TfrmSesSession_List.MarkAsCertificatesPrinted;
var
  aDataModule: IEDUSessionDataModule;
begin
  assert (Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ));
  aDataModule.MarkAsCertificatesPrinted(FSelectedEnrolsCommaText);
  Self.RefreshData;
end;

procedure TfrmSesSession_List.cxgrdtblvListStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

{  if ( ( aRecord.Values[ cxgrdtblvListF_SES_AANW_PRINTED.Index ] = False ) or
       ( aRecord.Values[ cxgrdtblvListF_ALL_CERT_PRINTED.Index ] = False ) ) then
  begin
    aStyle := dtmEDUMainClient.cxsOrange;
  end; }

  if not (aRecord.Expandable) then
  begin
    // Confirmation report already sent?  Assign blue color to record
    // Statuscodes Geattesteerd and gefactureerd get higher priority
    // Ivan Van den Bossche (05/03/2008)

    if ( aRecord.Values[ cxgrdtblvListF_CONFIRMATION_REPORT_SENT.Index ] = true )
        and ( ( aRecord.Values[ cxgrdtblvListF_STATUS_ID.Index ] <> 12 )
                and ( aRecord.Values[ cxgrdtblvListF_STATUS_ID.Index ] <> 10 )
             ) then
      aStyle := dtmEDUMainClient.cxsBlue;

    if ( aRecord.Values[ cxgrdtblvListF_STATUS_ID.Index ] = 2 ) then
      aStyle := dtmEDUMainClient.cxsRed
    else if ( aRecord.Values[ cxgrdtblvListF_STATUS_ID.Index ] = 12 ) then
      aStyle := dtmEDUMainClient.cxsGray
    else if ( aRecord.Values[ cxgrdtblvListF_STATUS_ID.Index ] = 10 ) then
      aStyle := dtmEDUMainClient.cxsYellow
    else if ( aRecord.Values[ cxgrdtblvListF_STATUS_ID.Index ] = 14 ) then
      aStyle := dtmEDUMainClient.cxsOrange;

  end;
end;

procedure TfrmSesSession_List.cxdbdeF_START_DATE1Exit(Sender: TObject);
begin
  inherited;

end;

{*****************************************************************************
  Name           : TfrmSesSession_List.PrintLetterAndCertificate
  Author         : Ivan Van den Bossche
  Arguments      : APreview
  Return Values  : None
  Exceptions     : None
  Description    : Displays (->preview=true) or prints Letter and Certificate
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
procedure TfrmSesSession_List.PrintLetterAndCertificate(const APreview: Boolean);
var
  lReportName, lReportTitle: string;
  ParamNames, ParamValues: TStringList;
  EnrolIds: TStringList;
  i: integer;
  oldCursor: TCursor;
  recordcount: Integer;
  PrinterCertificates: string;
  PrinterLetter: string;
begin
  // Get selected session_id(s)
  FSelectedSessions := TStringList.Create;
  EnrolIds := TStringList.Create; // Store Enrol Id's associated with selected session id(s) here

  try
    if TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount > 1 then
    begin //multiple sessions selected
      LoopThroughSelectedRecords(GetSessions);
    end
    else begin //only one session selected
      FSelectedSessions.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    end;

    FSelectedSessionsCommaText := FSelectedSessions.CommaText;

    // Get enrol id(s) associated with selected session id's
    for i:=0 to FSelectedSessions.Count-1 do begin
      dtmEDUMainClient.GetEnrolIdsFromSession(FSelectedSessions.Strings[i], EnrolIds);
    end;

    FSelectedEnrolsCommaText := EnrolIds.CommaText;

    //First let's check whether the report will at least be returning 1 record !
    recordcount := EnrolIds.Count;
    if (recordcount = 0) then
    begin
      MessageDlg(rsReportNoCertificateRecords, mtError, [mbOk], 0);
      FSelectedSessions.Free;
      EnrolIds.Free;
      Exit;
    end;

    if (recordcount < FSelectedSessions.Count) then
    begin
      MessageDlg(Format(rsReportNotAllCertificateRecords, [FSelectedSessions.Count - recordcount]), mtWarning, [mbOk], 0);
    end;

    //Check whether the report will at least be returning 1 record !
    recordcount := dtmEDUMainClient.ReportRecordCount('V_PROG_GET_NUMBER_PRINTED_ENROL', 'F_SESSION_ID',
    'F_SESSION_ID', FSelectedSessions);
    if (recordcount > 0) then
    begin
      if MessageDlg(rsReportPrintedCertificates, mtConfirmation,[mbYes,mbNo],0)=mrNo then begin
        FSelectedSessions.Free;
        EnrolIds.Free;
        Exit;
      end;

    end;

    PrinterCertificates := dtmEDUMainClient.GetPrinterCertificates;
    PrinterLetter := dtmEDUMainClient.GetPrinterLetters;

    lReportName := REP_LETTER_CERTIFICATE;
    lReportTitle := rsLetterCertificateReportName;
    ParamNames := TStringList.Create;
    ParamValues := TStringList.Create;
    oldCursor := screen.cursor;
    screen.cursor := crSQLWait;
    try
      ParamNames.Add('p_session_id');
      ParamNames.Add('p_user');
      ParamValues.AddObject ('', FSelectedSessions);
      ParamValues.Add(dtmEDUMainClient.GetCurUser);
      dtmEDUMainClient.ExecuteCrystal(lReportName, lReportTitle, ParamNames, ParamValues, APreview, True, OnReportPreviewClosedNoAction, PrinterLetter );
    finally
      screen.cursor := oldCursor;
      FreeAndNil (ParamValues);
      FreeAndNil (ParamNames);
      //FreeAndNil(dtm);
    end;

    // Print certificate(s)
    lReportName := REP_CERTIFICATE;
    lReportTitle := rsCertificateReportName;
    ParamNames  := TStringList.Create;
    ParamValues := TStringList.Create;
    oldCursor := screen.cursor;
    screen.cursor := crSQLWait;
    try
      ParamNames.Add('p_enrol_id');
      ParamValues.AddObject ('', EnrolIds);
      dtmEDUMainClient.ExecuteCrystal(lReportName, lReportTitle, ParamNames, ParamValues, APreview, False, OnReportPreviewCertificateClosed, PrinterCertificates );
    finally
      screen.cursor := oldCursor;
      FreeAndNil (ParamValues);
      FreeAndNil (ParamNames);
      //FreeAndNil(dtm);
    end;

    if not APreview then begin
      MarkAsCertificatesPrinted;
    end;

  finally
      FSelectedSessions := nil; // has been freed in TdtmCrystal.PrintReport
      EnrolIds := nil; // EnrolIds has been freed in TdtmCrystal.PrintReport
      //have to look at this when more time !
  end;
end;

{*****************************************************************************
  Name           : TfrmSesSession_List.PrintEvaluationDoc
  Author         : Ivan Van den Bossche
  Arguments      : APreview
  Return Values  : None
  Exceptions     : None
  Description    : Displays (->preview=true) or prints evaluation docs
  History        :

  Date         By                   Description
  ----         --                   -----------
  24/10/2007   Ivan Van den Bossche Initial creation of the Procedure.
 *****************************************************************************}
procedure TfrmSesSession_List.PrintEvaluationDoc(const APreview: Boolean);
var
  aPrinter: String;
  aOldcursor: TCursor;
  lReportName, lReportTitle: string;
  ParamNames, ParamValues: TStringList;
  aEvalEnrolNo, aEvalInstrNo: integer;
  aSessionId: String; // Used as parameter for Crystal Reports...
  aIdxDoc: Integer;
begin
  FSelectedSessions := TStringList.Create;
  try
    aOldCursor := Screen.Cursor;
    try
        Screen.Cursor := crSQLWait;
        aSessionId := srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString;
        FSelectedSessions.Add( aSessionId );

        // (Re)generate OCR codes
        CreateOCR;

        //Check whether the report evaluatie_lesgever will at least be returning 1 record !
        aEvalInstrNo := dtmEDUMainClient.ReportRecordCount('V_REP_EVALUATION_INSTRUCTORS', 'F_SESSION_ID', 'F_SESSION_ID', FSelectedSessions);

        //Check whether the report evaluatie_cursist will at least be returning 1 record !
        aEvalEnrolNo := dtmEDUMainClient.ReportRecordCount('V_REP_EVALUATION_ENROLMENTS', 'F_SESSION_ID', 'F_SESSION_ID', FSelectedSessions);

        if ( ( aEvalInstrNo = 0 ) or ( aEvalEnrolNo = 0 ) ) then
        begin
          MessageDlg(rsReportNoRecordsEval, mtWarning,[mbOk],0);
          Exit;
        end;

        // Get printer for evaluation doc
        aPrinter := dtmEDUMainClient.GetPrinterEvaluationDoc;

        // Print evaluation docs
        for aIdxDoc := 0 to 1 do
        begin
          if aIdxDoc = 0 then
          begin
            // Print evaluation doc(s) for enrolment(s)
            lReportName := REP_EVALUATION_DOC_ENROLMENTS;
            lReportTitle := rsTitleEvaluationDocEnrolment;
          end else begin
            lReportName := REP_EVALUATION_DOC_INSTRUCTORS;
            lReportTitle := rsTitleEvaluationDocInstructor;
          end;

          ParamNames := TStringList.Create;
          ParamValues := TStringList.Create;
          try
            ParamNames.Add('p_session_id');
            ParamValues.Add( aSessionId );
            dtmEDUMainClient.ExecuteCrystal(lReportName, lReportTitle, ParamNames, ParamValues, APreview, True, OnReportPreviewClosedNoAction, aPrinter );
          finally
            FreeAndNil (ParamValues);
            FreeAndNil (ParamNames);
          end;

        end;

    finally
      Screen.Cursor := aOldCursor;
    end;

  finally
      FSelectedSessions.Free;
  end;

end;

procedure TfrmSesSession_List.OnReportPreviewClosedNoAction(Sender: TObject);
begin
  // No implementation because it is used to cause the Crystal Reports engine to wait before continuing the application
end;

procedure TfrmSesSession_List.acPrintConfirmationReportsExecute(
  Sender: TObject);
var
  aDataModule: IEDUSessionDataModule;
begin
  inherited;

  if (Supports(FrameWorkDataModule, IEDUSessionDataModule, aDataModule)) then
  begin
    try
      aDataModule.ShowConfirmationRpts(srcMain.DataSet);
      RefreshData;
    except
        on E: Exception do begin
            MessageDlg(Format(eUnableToPrintConfirmationReports, [E.Message]), mtError, [mbOk], 0);
        end;
    end;
  end;

end;

procedure TfrmSesSession_List.acSessionWizardExecute(Sender: TObject);
var
  aDataModule: IEDUSessionDataModule;
begin
  inherited;

  if (Supports(FrameWorkDataModule, IEDUSessionDataModule, aDataModule)) then
  begin
    try
      aDataModule.ShowSessionWizard(srcMain.DataSet);
    except
        on E: Exception do begin
            MessageDlg(Format(eErrorInSessionWizard, [E.Message]), mtError, [mbOk], 0);
        end;
    end;
  end;


end;

procedure TfrmSesSession_List.dxbpmnGridPopup(Sender: TObject);
var
  aStatusId: Integer;
begin
  inherited;
    if srcMain.DataSet.FieldByName('F_PROGRAM_TYPE').AsInteger = 1 then
    begin
      dxBarSubItemRapportenOpleiding.Enabled := True;
      dxBarSubItemOpleiding.Enabled := True;
      dxBarSubItemRapportenExamen.Enabled := False;
      dxBarSubItemExamen.Enabled := False;
    end
    else
    begin
      dxBarSubItemRapportenOpleiding.Enabled := False;
      dxBarSubItemOpleiding.Enabled := False;
      dxBarSubItemRapportenExamen.Enabled := True;
      dxBarSubItemExamen.Enabled := True;
    end;

    // activate actions below when Dataset is active (ivdbossche 17-JUL-2007)
    if (srcMain.DataSet.Active) and (not srcMain.DataSet.FieldByName('F_STATUS_ID').IsNull) then
    begin
        acListViewPreviewLetterCertificateAction.Enabled := True;
        acListViewPrintLetterCertificateAction.Enabled := True;
    end else begin
        acListViewPreviewLetterCertificateAction.Enabled := False;
        acListViewPrintLetterCertificateAction.Enabled := False;
    end;

    acPrintConfirmationReports.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);
    acPrintReport.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);

    acSetParticipantsListReceived.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);
    acSetParticipantsListNotReceived.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);

    acSetCoursesSent.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);
    acSetCoursesSent.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);

    acSetConfirmReportSent.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);
    acSetConfirmReportNotSent.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0);

    // Not allowed to select multiple sessions at once.
    acPrintEvaluation.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0) and ( TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount = 1 );
    acPreviewEvaluation.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0) and ( TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount = 1 );
    acOpenFolder.Enabled := (srcMain.DataSet.Active) and (srcMain.DataSet.RecordCount > 0) and ( TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount = 1 );

    // Only enable SessionWizard popup when selected session is not invoiced/cancelled yet
    if ( srcMain.DataSet.Active ) and (srcMain.DataSet.RecordCount > 0) then
    begin
      aStatusId := srcMain.DataSet.FieldByName('F_STATUS_ID').AsInteger;

      acSessionWizard.Enabled := ( aStatusId <> INVOICED ) and ( aStatusId <> CANCELLED ) and ( TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount = 1 );
    end;

end;

procedure TfrmSesSession_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;
    with cxgrdtblvList.Controller do
    if (FocusedRecord <> nil) and FocusedRecord.Selected then
        FocusedRecord.Selected := False;
end;

procedure TfrmSesSession_List.FVBFFCListViewCreate(Sender: TObject);
begin
  inherited;
  // Add OnCanFocusRecord event to grid
  cxgrdtblvList.OnCanFocusRecord := OnCanFocusRecord;
  cxbtnEDUButton1.Enabled := False;
end;

procedure TfrmSesSession_List.cxbtnApplyFilterClick(Sender: TObject);
begin
    // BugFix for Mantis 2408 - 'Als je in het overzicht van de sessies dingen aanvinkt, of het inschrijvingsaantal ingeeft, verdwijnt dit als je opnieuw filtert.'
  // 11/04/2008 - Ivan Van den Bossche
  assert( srcMain.DataSet <> nil );
  if srcMain.DataSet.Active then
  begin
    if srcMain.DataSet.State in [dsInsert, dsEdit] then
      srcMain.DataSet.Post;
  end;

  inherited;
  // Disable select button after user pushed on it

  cxbtnEDUButton1.Enabled := False;
end;

{*****************************************************************************

  @Name       TfrmSesSession_List.OnCanFocusRecord
  @author     Ivan Van den Bossche
  @param      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
              var AAlow: Boolean
  @return     None
  @Exception  None
  @See        None
  @Description  Use the OnCanFocusRecord event to control focusing of a record.
                The Sender parameter identifies the affected view and
                the ARecord parameter specifies the record to be focused.
                To focus the record, set the AAllow parameter to True.
                Otherwise, the record will not be focused and as a result,
                not selected also. Select button will be enabled as a result.
******************************************************************************}
procedure TfrmSesSession_List.OnCanFocusRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  AAllow := True;
  cxbtnEDUButton1.Enabled := True;
end;

{****************************************************************
  @Name       TfrmSesSession_List.CreateOCR
  @author     Ivan Van den Bossche (25/09/2007)
  @param      None
  @return     None
  @Exception  None
  @See        None
  @Descr      Procedure creates OCR code for selected session(s)
*****************************************************************}
procedure TfrmSesSession_List.CreateOCR;
var
    aDataModule : IEDUSessionDataModule;
begin
    assert (self.srcMain.DataSet <> nil);

    if (Assigned(FrameWorkDataModule)) and
       (Supports(FrameWorkDataModule, IEDUSessionDataModule, aDataModule)) then
    begin
      aDataModule.CreateOCR(srcMain.DataSet.FieldByName('F_SESSION_ID').AsInteger);
    end;

end;

procedure TfrmSesSession_List.acSwitchViewExecute(Sender: TObject);
var
  lcv: Integer;
  curLevel: Integer;
  nextLevel: Integer;
begin
  inherited;

  nextLevel := -1;
  curLevel  := -1;

  for lcv := 0 to cxgrdList.Levels.Count -1 do
  begin
    if cxgrdList.Levels.Items[lcv].Visible then
    begin
      curLevel  := lcv;
      nextLevel := lcv + 1;
      if (nextLevel >= cxgrdList.Levels.Count) then nextLevel := 0;
      break;
    end;
  end;

  assert ((nextLevel > -1) and (curLevel > -1), 'Can''t change level');

  cxgrdList.Levels.Items[curLevel].Visible := false;
  cxgrdList.Levels.Items[NextLevel].Visible := true;

  cxgrdtblvList2F_WEEK.GroupIndex := 0;
  cxgrdtblvList2F_WEEK.Visible := false;
  cxgrdtblvList2.ViewData.Expand(true);
end;

procedure TfrmSesSession_List.RestoreActiveViewFromIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].RestoreFromIniFile( LocalSettingsIniFileName, True, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;

procedure TfrmSesSession_List.StoreActiveViewToIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].StoreToIniFile( LocalSettingsIniFileName, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;


procedure TfrmSesSession_List.acSetParticipantsListReceivedExecute(
  Sender: TObject);
begin
  inherited;
    FStatusCheckbox := 1;
    FStatusField := 'F_RECEIVED_PARTICIPANTS_LIST'; // Field being updated
    SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetParticipantsListNotReceivedExecute(
  Sender: TObject);
begin
  inherited;
    FStatusCheckbox := 0;
    FStatusField := 'F_RECEIVED_PARTICIPANTS_LIST'; // Field being updated
    SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetCoursesSentExecute(Sender: TObject);
begin
  inherited;
    FStatusCheckbox := 1;
    FStatusField := 'F_SENT_COURSE';
    SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetConfirmReportSentExecute(
  Sender: TObject);
begin
  inherited;
    FStatusCheckBox := 1;
    FStatusField := 'F_CONFIRMATION_REPORT_SENT';
    SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetConfirmReportNotSentExecute(
  Sender: TObject);
begin
  inherited;
    FStatusCheckbox := 0;
    FStatusField := 'F_CONFIRMATION_REPORT_SENT';
    SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetCoursesNotSentExecute(Sender: TObject);
begin
  inherited;
    FStatusCheckbox := 0;
    FStatusField := 'F_SENT_COURSE';
    SetStatusCheckbox;
end;

procedure TfrmSesSession_List.UpdateStatusCheckbox;
begin
  // Make sure field being updated is known.
  assert( FStatusField <> '');

  if not (srcMain.DataSet.State in [dsInsert, dsEdit])
        then srcMain.DataSet.Edit;

  srcMain.DataSet.FieldByName(FStatusField).Value := FStatusCheckbox;

end;

// Loop through selected records and update database.
procedure TfrmSesSession_List.SetStatusCheckbox;
var
  oldcursor: TCursor;
begin
    assert (self.srcMain.DataSet <> nil);

    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;
      srcMain.DataSet.DisableControls;
      try
        LoopThroughSelectedRecords( UpdateStatusCheckbox );
        srcMain.DataSet.Post;
      finally
          srcMain.DataSet.EnableControls;
      end;

    finally
      Screen.Cursor := oldcursor;
    end;

end;

procedure TfrmSesSession_List.acOpenFolderExecute(Sender: TObject);
var
  aPath: String;
  aMainPath: String;
  aSessionRef: String;
begin
  inherited;

    assert (srcMain.DataSet <> nil);

    if not srcMain.DataSet.FieldByName('F_CODE').IsNull then
    begin
        // Get Session ref - Exclude special chars which could give problems.
        aSessionRef := RemoveSpecialChars( srcMain.DataSet.FieldByName('F_CODE').AsString );
        //aPath := IncludeTrailingPathDelimiter( dtmEDUMainClient.GetConfirmationPathCurUser ) + aSessionRef + '\bevestigingsbrieven';

        try
          aMainPath := IncludeTrailingPathDelimiter( dtmEDUMainClient.GetConfirmationPathCurUser ) + 'Dossiers 20' + IncludeTrailingPathDelimiter(Format('%2.2d', [StrToInt(Copy(Trim(aSessionRef),0,2))])) + aSessionRef + '\';
          ForceDirectories(aMainPath + FLDR_Attestatie);
          ForceDirectories(aMainPath + FLDR_Bestelbonnen);
          ForceDirectories(aMainPath + FLDR_Bevestigingsbrieven);
          ForceDirectories(aMainPath + FLDR_Communicatie);

          aPath := aMainPath + FLDR_Bevestigingsbrieven;
          if DirectoryExists( aPath ) then
            OpenExplorer( aPath ) // Open path in Windows explorer.
          else
            MessageDlg(Format(rsUnableToOpenSessionFolder, [aPath]), mtError, [mbOk], 0);
        except
          MessageDlg(Format(rsUnableToOpenSessionFolder, [aPath]), mtError, [mbOk], 0);
        end;
        //PathPdfs := IncludeTrailingPathDelimiter( GetConfirmationPathCurUser ) + 'Dossiers 20' + IncludeTrailingPathDelimiter(Format('%2.2d', [StrToInt(Copy(Trim(SessionCode),0,2))])) + SessionCode + '\bevestigingsbrieven';

    end else begin
        MessageDlg(rsEmptySessionRef, mtWarning, [mbOk], 0);
    end;

end;

procedure TfrmSesSession_List.FVBFFCListViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

    // Make sure that changes made to the grid will be applied to the database.
    if (srcMain.DataSet.Active) then
    begin
        if srcMain.DataSet.State in [dsInsert, dsEdit] then
          srcMain.DataSet.Post;

    end;
end;

procedure TfrmSesSession_List.acSetSatusFacturatedExecute(Sender: TObject);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;
  assert (self.srcMain.DataSet <> nil);

  if (Assigned(FrameWorkDataModule)) and
     (Supports(FrameWorkDataModule, IEDUSessionDataModule, aDataModule)) then
  begin
    aDataModule.SetStatusFacturated( srcMain.DataSet );
    Self.RefreshData;
  end;
end;

procedure TfrmSesSession_List.cxdbbeF_COOPERATION_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectCooperation( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmSesSession_List.cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectInfrastructure( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmSesSession_List.cxdbbeF_INSTRUCTOR_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSessionDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSessionDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectInstructor( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmSesSession_List.acSetEpycReceivedExecute(Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 1;
  FStatusField := 'F_EPYC'; // Field being updated
  SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetEpycNotReceivedExecute(Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 0;
  FStatusField := 'F_EPYC'; // Field being updated
  SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetExaminatorReceivedExecute(
  Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 1;
  FStatusField := 'F_EXAMINATOR'; // Field being updated
  SetStatusCheckbox;

end;

procedure TfrmSesSession_List.acSetExaminatorNotReceivedExecute(
  Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 0;
  FStatusField := 'F_EXAMINATOR'; // Field being updated
  SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetAttestsSentExecute(Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 1;
  FStatusField := 'F_ATTESTS_SENT'; // Field being updated
  SetStatusCheckbox;

end;

procedure TfrmSesSession_List.acSetAttestsNotSentExecute(Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 0;
  FStatusField := 'F_ATTESTS_SENT'; // Field being updated
  SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetScanningReceivedExecute(
  Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 1;
  FStatusField := 'F_SCANNING'; // Field being updated
  SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acSetScanningNotReceivedExecute(
  Sender: TObject);
begin
  inherited;
  FStatusCheckbox := 0;
  FStatusField := 'F_SCANNING'; // Field being updated
  SetStatusCheckbox;
end;

procedure TfrmSesSession_List.acDrukBevestigingsrapportExecute(Sender: TObject);
var
  aDataModule: IEDUSessionDataModule;
begin
  inherited;
  if (Supports(FrameWorkDataModule, IEDUSessionDataModule, aDataModule)) then
  begin
    try
      aDataModule.ShowConfirmationRpts(srcMain.DataSet);
      RefreshData;
    except
        on E: Exception do begin
            MessageDlg(Format(eUnableToPrintConfirmationReports, [E.Message]), mtError, [mbOk], 0);
        end;
    end;
  end;
end;

procedure TfrmSesSession_List.acDrukVerslagExecute(Sender: TObject);
var
  TempNames: TStringList;
  TempValues: TStringList;
  aPrinter : String;
begin
  inherited;
  // Get printer for evaluation doc
  aPrinter := dtmEDUMainClient.GetPrinterLetters;
  TempNames := TStringList.Create;
  TempValues := TStringList.Create;
  try
    TempNames.Add('p_session_id');
    TempValues.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    dtmEDUMainClient.ExecuteCrystal(REP_REPORT_VCA_EXAMINATOR, rsTitleRptReportExaminer, TempNames, TempValues, False, True, OnReportPreviewClosedNoAction, aPrinter);
  finally
    TempNames.Free;
    TempValues.Free;
  end;
end;


procedure TfrmSesSession_List.acDrukControlelijstExecute(Sender: TObject);
var
  TempNames: TStringList;
  TempValues: TStringList;
  aPrinter : String;
begin
  inherited;
  // Get printer for doc
  aPrinter := dtmEDUMainClient.GetPrinterLetters;
  TempNames := TStringList.Create;
  TempValues := TStringList.Create;
  try
    TempNames.Add('p_session_id');
    TempValues.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    dtmEDUMainClient.ExecuteCrystal(REP_REPORT_VCA_CONTROLELIJST, rsTitleRptControlelijst, TempNames, TempValues, False, True, OnReportPreviewClosedNoAction, aPrinter);
  finally
    TempNames.Free;
    TempValues.Free;
  end;
end;

procedure TfrmSesSession_List.acDrukResultatenlijstExecute(Sender: TObject);
var
  TempNames: TStringList;
  TempValues: TStringList;
  aPrinter : String;
begin
  inherited;
  // Get printer for doc
  aPrinter := dtmEDUMainClient.GetPrinterLetters;
  TempNames := TStringList.Create;
  TempValues := TStringList.Create;
  try
    TempNames.Add('p_session_id');
    TempValues.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    dtmEDUMainClient.ExecuteCrystal(REP_REPORT_VCA_RESULTS, rsTitleRptResultatenlijst, TempNames, TempValues, False, True, OnReportPreviewClosedNoAction, aPrinter);
  finally
    TempNames.Free;
    TempValues.Free;
  end;
end;

procedure TfrmSesSession_List.acDrukBegeleidendeBriefExecute(Sender: TObject);
var
  TempNames: TStringList;
  TempValues: TStringList;
  aPrinter : String;
begin
  inherited;
  // Get printer for doc
  aPrinter := dtmEDUMainClient.GetPrinterLetters;
  TempNames := TStringList.Create;
  TempValues := TStringList.Create;
  try
    TempNames.Add('p_session_id');
    TempNames.Add('p_user');
    TempValues.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    TempValues.Add(dtmEDUMainClient.GetCurUser);
    dtmEDUMainClient.ExecuteCrystal(REP_LETTER_VCA_ATTEST, rsTitleRptBriefVCA, TempNames, TempValues, False, True, OnReportPreviewClosedNoAction, aPrinter);
  finally
    TempNames.Free;
    TempValues.Free;
  end;
end;

procedure TfrmSesSession_List.acDrukGlobaalAttestExecute(Sender: TObject);
var
  TempNames: TStringList;
  TempValues: TStringList;
  aPrinter : String;
begin
  inherited;
  // Get printer for doc
  aPrinter := dtmEDUMainClient.GetPrinterLetters;
  TempNames := TStringList.Create;
  TempValues := TStringList.Create;
  try
    TempNames.Add('p_session_id');
    TempValues.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    dtmEDUMainClient.ExecuteCrystal(REP_LETTER_VCA_GLOBAL, rsTitleRptGlobalAttestVCA, TempNames, TempValues, False, True, OnReportPreviewClosedNoAction, aPrinter);
  finally
    TempNames.Free;
    TempValues.Free;
  end;
end;

end.
