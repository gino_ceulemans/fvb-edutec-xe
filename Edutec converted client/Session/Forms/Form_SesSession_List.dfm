inherited frmSesSession_List: TfrmSesSession_List
  Left = 342
  Top = 86
  Width = 880
  Height = 784
  ActiveControl = cxdbteF_NAME1
  Caption = 'Sessies'
  Constraints.MinWidth = 812
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 716
    Width = 872
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 656
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 768
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 864
    Height = 708
    inherited cxgrdList: TcxGrid
      Top = 222
      Width = 864
      Height = 486
      inherited cxgrdtblvList: TcxGridDBTableView
        DataController.DataModeController.DetailInSQLMode = True
        DataController.DataModeController.GridMode = False
        DataController.DataModeController.SmartRefresh = True
        DataController.DetailKeyFieldNames = 'F_SESSION_ID'
        DataController.KeyFieldNames = 'F_SESSION_ID'
        OptionsData.Editing = True
        OptionsSelection.CellSelect = True
        OptionsSelection.MultiSelect = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.OnGetContentStyle = cxgrdtblvListStylesGetContentStyle
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 80
        end
        object cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_ID'
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvListF_PROGRAM_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_PROGRAM_TYPE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_TYPE_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_CODE'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_DURATION: TcxGridDBColumn
          DataBinding.FieldName = 'F_DURATION'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_DURATION_DAYS: TcxGridDBColumn
          DataBinding.FieldName = 'F_DURATION_DAYS'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 93
        end
        object cxgrdtblvListF_SESSIONGROUP_NAME: TcxGridDBColumn
          Caption = 'Doelgroep'
          DataBinding.FieldName = 'F_SESSIONGROUP_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_CONSTRUCT_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_CONSTRUCT_COMMENT'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 170
        end
        object cxgrdtblvListF_MINIMUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_MINIMUM'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_MAXIMUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAXIMUM'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriDate
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriDate
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
        object cxgrdtblvListF_INFRASTRUCTURE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvListF_INFRASTRUCTURE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 161
        end
        object cxgrdtblvListF_ANA_SLEUTEL: TcxGridDBColumn
          DataBinding.FieldName = 'F_ANA_SLEUTEL'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_PRICE_WORKER: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICE_WORKER'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Options.Editing = False
          Options.Focusing = False
          Width = 69
        end
        object cxgrdtblvListF_PRICE_CLERK: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICE_CLERK'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Options.Editing = False
          Options.Focusing = False
          Width = 78
        end
        object cxgrdtblvListF_PRICE_OTHER: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICE_OTHER'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Options.Editing = False
          Options.Focusing = False
          Width = 72
        end
        object cxgrdtblvListF_SES_AANW_PRINTED: TcxGridDBColumn
          DataBinding.FieldName = 'F_SES_AANW_PRINTED'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
          Width = 72
        end
        object cxgrdtblvListF_ALL_CERT_PRINTED: TcxGridDBColumn
          DataBinding.FieldName = 'F_ALL_CERT_PRINTED'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
          RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
          Width = 62
        end
        object cxgrdtblvListF_INSTRUCTOR: TcxGridDBColumn
          Caption = 'Lesgever(s)'
          DataBinding.FieldName = 'F_INSTRUCTOR'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 95
        end
        object cxgrdtblvListF_NO_PARTICIPANTS: TcxGridDBColumn
          Caption = '#deelnemers'
          DataBinding.FieldName = 'F_NO_PARTICIPANTS'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 82
        end
        object cxgrdtblvListF_WEEK: TcxGridDBColumn
          Caption = 'Week'
          DataBinding.FieldName = 'F_WEEK'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_RECEIVED_PARTICIPANTS_LIST: TcxGridDBColumn
          Caption = 'Deelnemerslijst ontvangen'
          DataBinding.FieldName = 'F_RECEIVED_PARTICIPANTS_LIST'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 96
        end
        object cxgrdtblvListF_SENT_COURSE: TcxGridDBColumn
          Caption = 'Cursussen verzonden'
          DataBinding.FieldName = 'F_SENT_COURSE'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 96
        end
        object cxgrdtblvListF_CONFIRMATION_REPORT_SENT: TcxGridDBColumn
          Caption = 'Bevestigingsrapport verstuurd'
          DataBinding.FieldName = 'F_CONFIRMATION_REPORT_SENT'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 177
        end
        object cxgrdtblvListF_EPYC: TcxGridDBColumn
          Caption = 'Epyc'
          DataBinding.FieldName = 'F_EPYC'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 96
        end
        object cxgrdtblvListF_EXAMINATOR: TcxGridDBColumn
          Caption = 'Examinator'
          DataBinding.FieldName = 'F_EXAMINATOR'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 96
        end
        object cxgrdtblvListF_ATTESTS_SENT: TcxGridDBColumn
          Caption = 'Attesten verstuurd'
          DataBinding.FieldName = 'F_ATTESTS_SENT'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 96
        end
        object cxgrdtblvListF_SCANNING: TcxGridDBColumn
          Caption = 'Scanning'
          DataBinding.FieldName = 'F_SCANNING'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Width = 96
        end
        object cxgrdtblvListF_SES_DAYS: TcxGridDBColumn
          Caption = 'Data'
          DataBinding.FieldName = 'F_SES_DAYS'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Editing = False
          Options.Focusing = False
          Width = 100
        end
        object cxgrdtblvListF_ENROLMENTS_WEB: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROLMENTS_WEB'
          Width = 102
        end
        object cxgrdtblvListF_COMMENT: TcxGridDBColumn
          Caption = 'Opmerkingen'
          DataBinding.FieldName = 'F_COMMENT'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 225
        end
        object cxgrdtblvListF_COOPERATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_COOPERATION_ID'
          PropertiesClassName = 'TcxTextEditProperties'
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvListF_COOPERATION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COOPERATION_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Editing = False
          Options.Focusing = False
          Width = 200
        end
      end
      object cxgrdtblvList2: TcxGridDBTableView [1]
        NavigatorButtons.ConfirmDelete = False
        OnCellDblClick = cxgrdtblvListCellDblClick
        DataController.DataModeController.DetailInSQLMode = True
        DataController.DataModeController.SmartRefresh = True
        DataController.DataSource = srcMain
        DataController.KeyFieldNames = 'F_SESSION_ID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        object cxgrdtblvList2F_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
        end
        object cxgrdtblvList2F_PROGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_ID'
          Visible = False
        end
        object cxgrdtblvList2F_PROGRAM_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_CODE'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_DURATION: TcxGridDBColumn
          DataBinding.FieldName = 'F_DURATION'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_DURATION_DAYS: TcxGridDBColumn
          DataBinding.FieldName = 'F_DURATION_DAYS'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_MINIMUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_MINIMUM'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_MAXIMUM: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAXIMUM'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_ID'
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvList2F_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_LANGUAGE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_ID'
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvList2F_LANGUAGE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_INFRASTRUCTURE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_ID'
          Visible = False
          Options.Editing = False
        end
        object cxgrdtblvList2F_INFRASTRUCTURE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_NAME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_PRICING_SCHEME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICING_SCHEME'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_PRICE_OTHER: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICE_OTHER'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_ANA_SLEUTEL: TcxGridDBColumn
          DataBinding.FieldName = 'F_ANA_SLEUTEL'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_PRICE_WORKER: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICE_WORKER'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_PRICE_CLERK: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRICE_CLERK'
          PropertiesClassName = 'TcxCalcEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_CONSTRUCT_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_CONSTRUCT_COMMENT'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_SES_AANW_PRINTED: TcxGridDBColumn
          DataBinding.FieldName = 'F_SES_AANW_PRINTED'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_ALL_CERT_PRINTED: TcxGridDBColumn
          DataBinding.FieldName = 'F_ALL_CERT_PRINTED'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
          Options.Editing = False
        end
        object cxgrdtblvList2F_WEEK: TcxGridDBColumn
          Caption = 'Week'
          DataBinding.FieldName = 'F_WEEK'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          GroupIndex = 0
          Options.Editing = False
        end
        object cxgrdtblvList2F_SENT_COURSE: TcxGridDBColumn
          Caption = 'Cursussen verzonden'
          DataBinding.FieldName = 'F_SENT_COURSE'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Visible = False
        end
        object cxgrdtblvList2F_INSTRUCTOR: TcxGridDBColumn
          Caption = 'Lesgever(s)'
          DataBinding.FieldName = 'F_INSTRUCTOR'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxgrdtblvList2F_RECEIVED_PARTICIPANTS: TcxGridDBColumn
          Caption = 'Deelnemerslijst ontvangen'
          DataBinding.FieldName = 'F_RECEIVED_PARTICIPANTS_LIST'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = False
          Visible = False
        end
        object cxgrdtblvList2F_SES_DAYS: TcxGridDBColumn
          Caption = 'Data'
          DataBinding.FieldName = 'F_SES_DAYS'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Options.Editing = False
          Width = 30
        end
      end
      object cxgrdlvlList2: TcxGridLevel
        GridView = cxgrdtblvList2
        Visible = False
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 197
      Width = 864
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 193
      Width = 864
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 864
      Height = 193
      ExpandedHeight = 193
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 160
        Width = 862
        Height = 32
        TabOrder = 10
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 694
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 782
        end
      end
      object cxlblF_PROGRAM_NAME1: TFVBFFCLabel
        Left = 440
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_PROGRAM_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Programma'
        FocusControl = cxdbbeF_PROGRAM_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_PROGRAM_NAME1: TFVBFFCDBButtonEdit
        Left = 528
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_PROGRAM_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_PROGRAM_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_PROGRAM_NAME1PropertiesButtonClick
        TabOrder = 1
        Width = 320
      end
      object cxlblF_NAME2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessie Naam'
        FocusControl = cxdbteF_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME1: TFVBFFCDBTextEdit
        Left = 112
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_NAME'
        DataBinding.DataField = 'F_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 0
        Width = 320
      end
      object cxlblF_SESSIONGROUP_NAME1: TFVBFFCLabel
        Left = 440
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSIONGROUP_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessiegroep'
        FocusControl = cxdbbeF_SESSIONGROUP_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_SESSIONGROUP_NAME1: TFVBFFCDBButtonEdit
        Left = 528
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSIONGROUP_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_SESSIONGROUP_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_SESSIONGROUP_NAME1PropertiesButtonClick
        TabOrder = 3
        Width = 320
      end
      object cxlblF_CODE1: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_CODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Referentie'
        FocusControl = cxdbteF_CODE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_CODE1: TFVBFFCDBTextEdit
        Left = 112
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_CODE'
        DataBinding.DataField = 'F_CODE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 2
        Width = 320
      end
      object cxlblF_START_DATE1: TFVBFFCLabel
        Left = 680
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'En'
        FocusControl = cxdbdeF_START_DATE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_START_DATE1: TFVBFFCDBDateEdit
        Left = 528
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_START_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 5
        OnExit = cxdbdeF_START_DATE1Exit
        Width = 136
      end
      object cxlblF_END_DATE1: TFVBFFCLabel
        Left = 440
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Tussen'
        FocusControl = cxdbdeF_END_DATE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_END_DATE1: TFVBFFCDBDateEdit
        Left = 712
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_END_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 6
        Width = 136
      end
      object cxlblF_STATUS_NAME1: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_STATUS_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        FocusControl = cxdbbeF_STATUS_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_STATUS_NAME1: TFVBFFCDBButtonEdit
        Left = 112
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_STATUS_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_STATUS_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_STATUS_NAME1PropertiesButtonClick
        TabOrder = 4
        Width = 216
      end
      object cxlblF_COOPERATION_NAME: TFVBFFCLabel
        Left = 8
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_COOPERATION_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'I.s.m.'
        FocusControl = cxdbbeF_COOPERATION_NAME
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_COOPERATION_NAME: TFVBFFCDBButtonEdit
        Left = 112
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_COOPERATION_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_COOPERATION_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_COOPERATION_NAMEPropertiesButtonClick
        TabOrder = 7
        Width = 216
      end
      object cxlblF_INFRASTRUCTURE_NAME: TFVBFFCLabel
        Left = 440
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_INFRASTRUCTURE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Locatie'
        FocusControl = cxdbbeF_INFRASTRUCTURE_NAME
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_INFRASTRUCTURE_NAME: TFVBFFCDBButtonEdit
        Left = 528
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_INFRASTRUCTURE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_INFRASTRUCTURE_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick
        TabOrder = 8
        Width = 320
      end
      object cxlblF_INSTRUCTOR_NAME: TFVBFFCLabel
        Left = 440
        Top = 128
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_INSTRUCTOR_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Lesgever'
        FocusControl = cxdbbeF_INSTRUCTOR_NAME
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_INSTRUCTOR_NAME: TFVBFFCDBButtonEdit
        Left = 528
        Top = 128
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_INSTRUCTOR_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_INSTRUCTOR_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_INSTRUCTOR_NAMEPropertiesButtonClick
        TabOrder = 9
        Width = 320
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 872
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 712
    Width = 872
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 868
    Height = 708
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 708
  end
  inherited srcMain: TFVBFFCDataSource
    Left = 48
    Top = 264
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarButton1: TdxBarButton
      Action = acPrintReport
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'Druk attesten'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton2
          Visible = True
        end
        item
          Item = dxBarButton4
          Visible = True
        end>
    end
    object dxBarButton2: TdxBarButton
      Action = acListViewPreviewLetterCertificateAction
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = acListViewPrintLetterCertificateAction
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = acPrintConfirmationReports
      Category = 0
      Hint = 'Druk bevestigingsrapporten'
    end
    object dxBarButton9: TdxBarButton
      Action = acSessionWizard
      Caption = 'Sessie Wizard'
      Category = 0
      Hint = 'Sessie Wizard'
    end
    object dxBarButton10: TdxBarButton
      Action = acPreviewEvaluation
      Category = 0
      Hint = 'Preview'
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'Druk evaluatieformulieren'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton10
          Visible = True
        end
        item
          Item = dxBarButton11
          Visible = True
        end>
    end
    object dxBarButton11: TdxBarButton
      Action = acPrintEvaluation
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = acSwitchView
      Category = 0
    end
    object dxBarSubItemStatusDeelnemerslijst: TdxBarSubItem
      Caption = 'Status deelnemerslijst'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton8
          Visible = True
        end
        item
          Item = dxBarButton12
          Visible = True
        end>
    end
    object dxBarButton8: TdxBarButton
      Action = acSetParticipantsListReceived
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = acSetParticipantsListNotReceived
      Category = 0
    end
    object dxBarSubItemStatusCursussen: TdxBarSubItem
      Caption = 'Status cursussen'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton16
          Visible = True
        end
        item
          Item = dxBarButton17
          Visible = True
        end>
    end
    object dxBarButton16: TdxBarButton
      Action = acSetCoursesSent
      Category = 0
    end
    object dxBarButton17: TdxBarButton
      Action = acSetCoursesNotSent
      Category = 0
    end
    object dxBarButton18: TdxBarButton
      Action = acOpenFolder
      Category = 0
    end
    object dxBarSubItemStatusBevestigingsRapport: TdxBarSubItem
      Caption = 'Status bevestigingsrapport'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton14
          Visible = True
        end
        item
          Item = dxBarButton19
          Visible = True
        end>
    end
    object dxBarButton14: TdxBarButton
      Action = acSetConfirmReportSent
      Category = 0
    end
    object dxBarButton19: TdxBarButton
      Action = acSetConfirmReportNotSent
      Category = 0
    end
    object dxBarButton20: TdxBarButton
      Action = acSetSatusFacturated
      Category = 0
    end
    object dxBarSubItemOpleiding: TdxBarSubItem
      Caption = 'Opleiding'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarSubItemStatusDeelnemerslijst
          Visible = True
        end
        item
          Item = dxBarSubItemStatusCursussen
          Visible = True
        end
        item
          Item = dxBarSubItemStatusBevestigingsRapport
          Visible = True
        end>
    end
    object dxBarSubItemExamen: TdxBarSubItem
      Caption = 'Examen'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarSubItemStatusEpyc
          Visible = True
        end
        item
          Item = dxBarSubItemStatusExaminator
          Visible = True
        end
        item
          Item = dxBarSubItemStatusAttesten
          Visible = True
        end
        item
          Item = dxBarSubItemStatusScanning
          Visible = True
        end>
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'New Item'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItemStatusEpyc: TdxBarSubItem
      Caption = 'Status Epyc'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonEpycOntvangen
          Visible = True
        end
        item
          Item = dxBarButtonEpycNietOntvangen
          Visible = True
        end>
    end
    object dxBarSubItemStatusExaminator: TdxBarSubItem
      Caption = 'Status Examinator'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonExaminatorOntvangen
          Visible = True
        end
        item
          Item = dxBarButtonExaminatorNietOntvangen
          Visible = True
        end>
    end
    object dxBarSubItemStatusAttesten: TdxBarSubItem
      Caption = 'Status Attesten'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonAttestenVerstuurd
          Visible = True
        end
        item
          Item = dxBarButtonAttestenNietVerstuurd
          Visible = True
        end>
    end
    object dxBarSubItemStatusScanning: TdxBarSubItem
      Caption = 'Status Scanning'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonScanningVerzonden
          Visible = True
        end
        item
          Item = dxBarButtonScanningNietVerzonden
          Visible = True
        end>
    end
    object dxBarButtonEpycOntvangen: TdxBarButton
      Action = acSetEpycReceived
      Category = 0
    end
    object dxBarButtonEpycNietOntvangen: TdxBarButton
      Action = acSetEpycNotReceived
      Category = 0
    end
    object dxBarButtonExaminatorOntvangen: TdxBarButton
      Action = acSetExaminatorReceived
      Category = 0
    end
    object dxBarButtonExaminatorNietOntvangen: TdxBarButton
      Action = acSetExaminatorNotReceived
      Category = 0
    end
    object dxBarButtonAttestenVerstuurd: TdxBarButton
      Action = acSetAttestsSent
      Category = 0
    end
    object dxBarButtonAttestenNietVerstuurd: TdxBarButton
      Action = acSetAttestsNotSent
      Category = 0
    end
    object dxBarButtonScanningVerzonden: TdxBarButton
      Action = acSetScanningReceived
      Category = 0
    end
    object dxBarButtonScanningNietVerzonden: TdxBarButton
      Action = acSetScanningNotReceived
      Category = 0
    end
    object dxBarSubItemRapportenOpleiding: TdxBarSubItem
      Caption = 'Rapporten opleiding'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButton1
          Visible = True
        end
        item
          Item = dxBarSubItem1
          Visible = True
        end
        item
          Item = dxBarButton5
          Visible = True
        end
        item
          Item = dxBarSubItem2
          Visible = True
        end>
    end
    object dxBarSubItemRapportenExamen: TdxBarSubItem
      Caption = 'Rapporten examen'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonDrukBesvestigingsrapport
          Visible = True
        end
        item
          Item = dxBarButtonDrukVerslag
          Visible = True
        end
        item
          Item = dxBarButtonDrukControlellijst
          Visible = True
        end
        item
          Item = dxBarButtonDrukResultatenllijst
          Visible = True
        end
        item
          Item = dxBarSubItemDrukAttesten
          Visible = True
        end>
    end
    object dxBarButtonDrukBesvestigingsrapport: TdxBarButton
      Action = acDrukBevestigingsrapport
      Category = 0
    end
    object dxBarButtonDrukVerslag: TdxBarButton
      Action = acDrukVerslag
      Category = 0
    end
    object dxBarButtonDrukControlellijst: TdxBarButton
      Action = acDrukControlelijst
      Category = 0
    end
    object dxBarSubItemDrukAttesten: TdxBarSubItem
      Caption = 'Druk attesten'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonDrukGlobaalAttest
          Visible = True
        end
        item
          Item = dxBarButtonBegeleidendeBrief
          Visible = True
        end>
    end
    object dxBarButtonDrukGlobaalAttest: TdxBarButton
      Action = acDrukGlobaalAttest
      Category = 0
    end
    object dxBarButtonBegeleidendeBrief: TdxBarButton
      Action = acDrukBegeleidendeBrief
      Category = 0
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = 'New Item'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButtonDrukResultatenllijst: TdxBarButton
      Action = acDrukResultatenlijst
      Caption = 'Druk resultatenlijst'
      Category = 0
      Hint = 'Druk resultatenlijst'
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        Item = dxBarButton6
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarSubItemRapportenOpleiding
        Visible = True
      end
      item
        Item = dxBarSubItemRapportenExamen
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton18
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarSubItemOpleiding
        Visible = True
      end
      item
        Item = dxBarSubItemExamen
        Visible = True
      end
      item
        Item = dxBarButton20
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton9
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
    OnPopup = dxbpmnGridPopup
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    Left = 144
    Top = 254
  end
  object alEnrollment: TFVBFFCActionList
    Left = 240
    Top = 382
    object acPrintReport: TAction
      Caption = '&Druk aanwezigheidslijst'
      OnExecute = acPrintReportExecute
    end
    object acListViewPreviewLetterCertificateAction: TAction
      Caption = 'Preview'
      OnExecute = acListViewPreviewLetterCertificateActionExecute
    end
    object acListViewPrintLetterCertificateAction: TAction
      Caption = 'Afprinten'
      OnExecute = acListViewPrintLetterCertificateActionExecute
    end
    object acPrintConfirmationReports: TAction
      Caption = 'Druk bevestigingsrapporten'
      OnExecute = acPrintConfirmationReportsExecute
    end
    object acSessionWizard: TAction
      Caption = 'Open Sessie Wizard'
      OnExecute = acSessionWizardExecute
    end
    object acPreviewEvaluation: TAction
      Caption = 'Preview'
      OnExecute = acPreviewEvaluationExecute
    end
    object acPrintEvaluation: TAction
      Caption = 'Afdrukken'
      OnExecute = acPrintEvaluationExecute
    end
    object acSwitchView: TAction
      Caption = 'Andere weergave'
      ShortCut = 114
      OnExecute = acSwitchViewExecute
    end
    object acSetParticipantsListReceived: TAction
      Caption = 'Deelnemerslijst ontvangen'
      OnExecute = acSetParticipantsListReceivedExecute
    end
    object acSetParticipantsListNotReceived: TAction
      Caption = 'Deelnemerslijst niet ontvangen'
      OnExecute = acSetParticipantsListNotReceivedExecute
    end
    object acSetCoursesSent: TAction
      Caption = 'Cursussen verzonden'
      OnExecute = acSetCoursesSentExecute
    end
    object acSetCoursesNotSent: TAction
      Caption = 'Cursusses niet verzonden'
      OnExecute = acSetCoursesNotSentExecute
    end
    object acOpenFolder: TAction
      Caption = 'Open dossiermap'
      OnExecute = acOpenFolderExecute
    end
    object acSetConfirmReportSent: TAction
      Caption = 'Bevestigingsrapport verstuurd'
      OnExecute = acSetConfirmReportSentExecute
    end
    object acSetConfirmReportNotSent: TAction
      Caption = 'Bevestigingsrapport niet verstuurd'
      OnExecute = acSetConfirmReportNotSentExecute
    end
    object acSetSatusFacturated: TAction
      Caption = 'Zet op gefactureerd'
      OnExecute = acSetSatusFacturatedExecute
    end
    object acSetEpycReceived: TAction
      Caption = 'Epyc ontvangen'
      OnExecute = acSetEpycReceivedExecute
    end
    object acSetEpycNotReceived: TAction
      Caption = 'Epyc niet ontvangen'
      OnExecute = acSetEpycNotReceivedExecute
    end
    object acSetExaminatorReceived: TAction
      Caption = 'Examinator onvangen'
      OnExecute = acSetExaminatorReceivedExecute
    end
    object acSetExaminatorNotReceived: TAction
      Caption = 'Examinator niet onvangen'
      OnExecute = acSetExaminatorNotReceivedExecute
    end
    object acSetAttestsSent: TAction
      Caption = 'Attesten verstuurd'
      OnExecute = acSetAttestsSentExecute
    end
    object acSetAttestsNotSent: TAction
      Caption = 'Attesten niet verstuurd'
      OnExecute = acSetAttestsNotSentExecute
    end
    object acSetScanningReceived: TAction
      Caption = 'Scanning ontvangen'
      OnExecute = acSetScanningReceivedExecute
    end
    object acSetScanningNotReceived: TAction
      Caption = 'Scanning niet ontvangen'
      OnExecute = acSetScanningNotReceivedExecute
    end
    object acDrukBevestigingsrapport: TAction
      Caption = 'Druk bevestigingsrapport'
      OnExecute = acDrukBevestigingsrapportExecute
    end
    object acDrukVerslag: TAction
      Caption = 'Druk verslag'
      OnExecute = acDrukVerslagExecute
    end
    object acDrukControlelijst: TAction
      Caption = 'Druk controlelijst'
      OnExecute = acDrukControlelijstExecute
    end
    object acDrukGlobaalAttest: TAction
      Caption = 'Druk globaal attest'
      OnExecute = acDrukGlobaalAttestExecute
    end
    object acDrukBegeleidendeBrief: TAction
      Caption = 'Druk begeleidende brief'
      OnExecute = acDrukBegeleidendeBriefExecute
    end
    object acDrukResultatenlijst: TAction
      Caption = 'acDrukResultatenlijst'
      OnExecute = acDrukResultatenlijstExecute
    end
  end
  object prdDialog: TPrintDialog
    Left = 332
    Top = 388
  end
end
