{*******************************************************************************
  Name           : Data_MailingOutlook (based on FVBMail.pas used in Construct)
  Author         : Ren� Clerckx
  Copyright      : (c) 2002 Peopleware
  Description    : Send a mail through a common interface. Create an instance
                   of a mail class which implements the IFVBMail interface. At
                   this point the only class is TFVBOutlookMail which uses
                   outlook. New classes can be derived from the abstract class
                   TFVBMail.
  Example        :
    var
      Mail: IFVBMail;
    begin
      Mail := TFVBOutlookMail.Create;
      Mail.Recipients.Add('info@peopleware.be');
      Mail.Subject := 'Test subject';
      Mail.Body.Add('Test body');
      Mail.Attachments.Add('c:\test.txt');
      Mail.Send;
    end;

  History        :

  Date         By              Description
  ----         --              -----------
  26/12/2007   Ivdbossche      Adjusted in order to fit into Edgard' source code
  05/08/2003   Ren� Clerckx    Initial creation.
*******************************************************************************}
unit Data_MailingOutlook;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_Mailing;//, Outlook8;

type
  TdtmMailingOutlook = class(TdtmMailing)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
//GCXE    FOutlookApplication: OutlookApplication;

    function GetSignature: string;
    property Signature: string read GetSignature;
  public
    procedure Send; override;
  end;

var
  dtmMailingOutlook: TdtmMailingOutlook;

implementation

{$R *.dfm}

uses OleServer, Registry, ShlObj;


{ TdtmMailingOutlook }

{*******************************************************************************
  Name           : TdtmMailingOutlook.DataModuleCreate
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Create an instance of Outlook or use an existing one.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailingOutlook.DataModuleCreate(Sender: TObject);
begin
  inherited;
//GCXE  FOutlookApplication := CoOutlookApplication.Create;
end;

{*******************************************************************************
  Name           : TdtmMailingOutlook.DataModuleDestroy
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Leave the instance of outlook, if there are no references to
                   outlook anymore, the application will close.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailingOutlook.DataModuleDestroy(Sender: TObject);
begin
//GCXE  FOut/lookApplication := nil;
  inherited;

end;

{*******************************************************************************
  Name           : TdtmMailingOutlook.Send
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Create a mailitem and send it. It's possible that the outlook
                   application will close before the mailitems are really send.
                   In that case the mailitem will stay in the outbox until
                   outlook is restarted!
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/10/2003   Wim Verheyen         Enhanced routine for VDAB E-mail addresses,
                                    which come in in a single recipient line but
                                    has to be split up into multiple ones.
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailingOutlook.Send;
var
//GCXE  FMailItem: MailItem;
  Index, lcv : Integer;
  aEmail: string;
  bEmailFound : Boolean;
begin
{GCXE
  FMailItem := FOutlookApplication.CreateItem(olMailItem) as MailItem;

  for Index := 0 to FRecipients.Count - 1 do
  begin
    aEmail := FRecipients.Strings[Index];
    if pos(';', aEmail) > 0 then
    begin
      repeat
        FMailItem.Recipients.Add(copy(aEmail, 0, pred(pos(';', aEmail))));
        aEmail := StringReplace(aEmail, copy(aEmail, 0, pos(';', aEmail)), '', [rfReplaceAll, rfIgnoreCase]);
      until pos(';', aEmail) = 0
    end;

    bEmailFound := False;
    for lcv := 1 to FMailItem.Recipients.Count do
    begin
      if ( aEmail = FMailItem.Recipients.Item( lcv ).Name ) then
      begin
        bEmailFound := True;
        Break;
      end;
    end;
    if not bEmailFound then
      FMailItem.Recipients.Add(aEmail);
  end;

  FMailItem.Subject := FSubject;

  FMailItem.Body := FBody.Text + #13#10 + Signature;

  for Index := 0 to FAttachments.Count - 1 do
    FMailItem.Attachments.Add(FAttachments.Strings[Index], EmptyParam, EmptyParam, EmptyParam);

  FMailItem.Send;

  }
end;

function TdtmMailingOutlook.GetSignature: string;
function GetSignatureFileName: string;
  var
    lReg: TRegistry;
  const
    cSIGNATUREKEY = 'Software\Microsoft\Windows NT\CurrentVersion\Windows Messaging Subsystem\Profiles\Microsoft Outlook Internet Settings\0A0D020000000000C000000000000046\';
    cSIGNATUREVALUENAME = '001e0361';
    cSIGNATUREEXTENSION = '.txt';
  begin
    lReg := TRegistry.Create(KEY_READ);
    try
      lReg.RootKey := HKEY_CURRENT_USER;
      // False because we do not want to create it if it doesn't exist
      lReg.OpenKey(cSIGNATUREKEY, False);
      Result := lReg.ReadString(cSIGNATUREVALUENAME) + cSIGNATUREEXTENSION;
    finally
      lReg.Free;
    end;
  end;

  function GetApplicationDataFolder: string;
  var
    ItemIDList: PItemIDList;
    FBuf: array[0..MAX_PATH] of Char;
  const
    cSUBFOLDER = 'Microsoft\Signatures\';
  begin
    ItemIDList := nil;
    SHGetSpecialFolderLocation(Application.Handle, CSIDL_APPDATA, ItemIdList);
    SHGetPathFromIDList(ItemIdList, @FBuf[0]);
    Result := IncludeTrailingPathDelimiter(string(FBuf)) + cSUBFOLDER;
  end;

var
  lSignatureText: TStringList;
  lSignatureFile: string;
begin
  lSignatureText := TStringList.Create;
  try
    lSignatureFile := GetApplicationDataFolder + GetSignatureFileName;
    if FileExists(lSignatureFile) then
    begin
      lSignatureText.LoadFromFile(lSignatureFile);
      Result := lSignatureText.Text;
    end;
  finally
    FreeAndNil(lSignatureText);
  end;
end;



end.
