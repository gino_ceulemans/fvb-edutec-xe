{*******************************************************************************
  Name           : Data_Mailing (based on FVBMail.pas used in Construct)
  Author         : Ren� Clerckx
  Copyright      : (c) 2002 Peopleware
  Description    : Send a mail through a common interface. Create an instance
                   of a mail class which implements the IFVBMail interface. At
                   this point the only class is TFVBOutlookMail which uses
                   outlook. New classes can be derived from the abstract class
                   TFVBMail.
  Example        :
    var
      Mail: IFVBMail;
    begin
      Mail := TFVBOutlookMail.Create;
      Mail.Recipients.Add('info@peopleware.be');
      Mail.Subject := 'Test subject';
      Mail.Body.Add('Test body');
      Mail.Attachments.Add('c:\test.txt');
      Mail.Send;
    end;

  History        :

  Date         By              Description
  ----         --              -----------
  26/12/2007   Ivdbossche      Adjusted in order to fit into Edgard' source code
  05/08/2003   Ren� Clerckx    Initial creation.
*******************************************************************************}

unit Data_Mailing;

interface

uses
  SysUtils, Classes, Unit_EdutecInterfaces;

type
  TdtmMailing = class(TDataModule, IEDUMailing)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  protected
    FSubject: string;
    FBody: TStrings;
    FRecipients: TStrings;
    FBlindCarbonCopy: TStrings;
    FCarbonCopy: TStrings;
    FAttachments: TStrings;
  private
    procedure FreeStrings(var Strings: TStrings);
  public
    function GetRecipients: TStrings; virtual;
    procedure SetRecipients(const Value: TStrings); virtual;
    function GetCarbonCopy: TStrings; virtual;
    procedure SetCarbonCopy(const Value: TStrings); virtual;
    function GetBlindCarbonCopy: TStrings; virtual;
    procedure SetBlindCarbonCopy(const Value: TStrings); virtual;
    function GetSubject: string; virtual;
    procedure SetSubject(const Value: string); virtual;
    function GetBody: TStrings; virtual;
    procedure SetBody(const Value: TStrings); virtual;
    function GetAttachments: TStrings;
    procedure SetAttachments(const Value: TStrings);

    property Recipients: TStrings read GetRecipients write SetRecipients;
    property CarbonCopy: TStrings read GetCarbonCopy write SetCarbonCopy;
    property BlindCarbonCopy: TStrings read GetBlindCarbonCopy write SetBlindCarbonCopy;
    property Subject: string read GetSubject write SetSubject;
    property Body: TStrings read GetBody write SetBody;
    property Attachments: TStrings read GetAttachments write SetAttachments;
    
    procedure Send; virtual; abstract;

  end;

var
  dtmMailing: TdtmMailing;

implementation

{$R *.dfm}

uses //Outlook8,
     variants;

{ TdtmMailing }

{*******************************************************************************
  Name           : TdtmMailing.DataModuleCreate
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Instantiate fields.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.DataModuleCreate(Sender: TObject);
begin
  FRecipients := TStringList.Create;
  FCarbonCopy := TStringList.Create;
  FBlindCarbonCopy := TStringList.Create;
  FBody := TStringList.Create;
  FAttachments := TStringList.Create;

end;

{*******************************************************************************
  Name           : TdtmMailing.DataModuleDestroy
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Clean up fields.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.DataModuleDestroy(Sender: TObject);
begin
  FreeStrings(FAttachments);
  FreeStrings(FBody);
  FreeStrings(FBlindCarbonCopy);
  FreeStrings(FCarbonCopy);
  FreeStrings(FRecipients);

end;

{*******************************************************************************
  Name           : TdtmMailing.FreeStrings
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Be sure to free a TStrings's objects before freeing the
                   TStrings object itself leaving the objects in memory
                   without a reference.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.FreeStrings(var Strings: TStrings);
var
  Index: Integer;
begin
  for Index := 0 to Strings.Count - 1 do
    if Assigned(Strings.Objects[Index]) then
      Strings.Objects[Index].Free;
  Strings.Free;
end;

{*******************************************************************************
  Name           : TdtmMailing.GetAttachments
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Getter for the Attachments property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
function TdtmMailing.GetAttachments: TStrings;
begin
  Result := FAttachments;
end;

{*******************************************************************************
  Name           : TdtmMailing.GetBlindCarbonCopy
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Gettter for the BlindCarbonCopy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
function TdtmMailing.GetBlindCarbonCopy: TStrings;
begin
  Result := FBlindCarbonCopy;
end;

{*******************************************************************************
  Name           : TdtmMailing.GetBody
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Getter for the Body property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
function TdtmMailing.GetBody: TStrings;
begin
  Result := FBody;
end;

{*******************************************************************************
  Name           : TdtmMailing.GetCarbonCopy
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Getter for the CarbonCopy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
function TdtmMailing.GetCarbonCopy: TStrings;
begin
  Result := FCarbonCopy;
end;

{*******************************************************************************
  Name           : TdtmMailing.GetRecipients
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Getter for the Recipients property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
function TdtmMailing.GetRecipients: TStrings;
begin
  Result := FRecipients;
end;

{*******************************************************************************
  Name           : TdtmMailing.GetSubject
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Getter for the Subject property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
function TdtmMailing.GetSubject: string;
begin
  Result := FSubject;
end;

{*******************************************************************************
  Name           : TdtmMailing.SetAttachments
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Setter for the Attachments property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.SetAttachments(const Value: TStrings);
begin
  if Assigned(Value) then
    FAttachments.Assign(Value);

end;

{*******************************************************************************
  Name           : TdtmMailing.SetBlindCarbonCopy
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Setter for the BlindCarbonCopy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.SetBlindCarbonCopy(const Value: TStrings);
begin
  if Assigned(Value) then
    FBlindCarbonCopy.Assign(Value);

end;

{*******************************************************************************
  Name           : TdtmMailing.SetBody
  Author         : Ren� Clerckx
  Arguments      :
  Return Values  : None
  Exceptions     : None
  Description    : Setter for the Body property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.SetBody(const Value: TStrings);
begin
  if Assigned(Value) then
    FBody.Assign(Value);
end;

{*******************************************************************************
  Name           : TdtmMailing.SetCarbonCopy
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Setter for the CarbonCopy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.SetCarbonCopy(const Value: TStrings);
begin
  if Assigned(Value) then
    FCarbonCopy.Assign(Value);
end;

{*******************************************************************************
  Name           : TdtmMailing.SetRecipients
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Setter for the Recipients property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.SetRecipients(const Value: TStrings);
begin
  if Assigned(Value) then
    FRecipients.Assign(Value);
end;

{*******************************************************************************
  Name           : TdtmMailing.SetSubject
  Author         : Ren� Clerckx
  Arguments      : 
  Return Values  : None
  Exceptions     : None
  Description    : Setter for the Subject property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  05/08/2003   Ren� Clerckx         Initial creation of the procedure.
*******************************************************************************}
procedure TdtmMailing.SetSubject(const Value: string);
begin
  if Value <> FSubject then
    FSubject := Value;
end;

end.
