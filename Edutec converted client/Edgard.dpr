

program Edgard;

uses
  Forms,
  Controls,
  MidasLib,
  Data_FVBFFCMainClient in 'Components\FVBFFCComponents\Data_FVBFFCMainClient.pas' {dtmFVBFFCMainClient: TDataModule},
  Form_FVBFFCMainClient in 'Components\FVBFFCComponents\Form_FVBFFCMainClient.pas' {frmFVBFFCMainClient},
  Form_FVBFFCBaseListView in 'Components\FVBFFCComponents\Form_FVBFFCBaseListView.pas' {FVBFFCBaseListView: TEdutecListView},
  Form_FVBFFCBaseRecordView in 'Components\FVBFFCComponents\Form_FVBFFCBaseRecordView.pas' {FVBFFCBaseRecordView: TEdutecRecordView},
  Data_EduMainClient in 'Templates\Data_EduMainClient.pas' {dtmEDUMainClient: TDataModule},
  Form_EduMainClient in 'Templates\Form_EduMainClient.pas' {frmEDUMainClient},
  Data_EDUDataModule in 'Templates\Data_EDUDataModule.pas' {EDUDataModule: TFVBFFCDataModule},
  Form_EDURecordView in 'Templates\Form_EDURecordView.pas' {EDURecordView: TFVBFFCRecordView},
  Form_EDUListView in 'Templates\Form_EDUListView.pas' {EDUListView: TFVBFFCListView},
  Data_Country in 'General\Datamodules\Data_Country.pas' {dtmCountry: TFVBFFCDataModule},
  Form_Country_Record in 'General\Forms\Form_Country_Record.pas' {frmCountry_Record: TFVBFFCRecordView},
  Form_Country_List in 'General\Forms\Form_Country_List.pas' {frmCountry_List: TFVBFFCListView},
  Data_FVBFFCBaseDataModule in 'components\FVBFFCComponents\Data_FVBFFCBaseDataModule.pas' {FVBFFCBaseDataModule: TFVBFFCDataModule},
  Form_Login in 'General\Forms\Form_Login.pas' {frmLogin},
  Data_Person in 'General\Datamodules\Data_Person.pas' {dtmPerson: TFVBFFCDataModule};

//  Data_Tablefield in 'DBWizard\Datamodules\Data_Tablefield.pas' {dtmTablefield: TFVBFFCDataModule},
//  Form_Tablefield_List in 'DBWizard\Forms\Form_Tablefield_List.pas' {frmTablefield_List: TFVBFFCListView},
//  Form_Tablefield_Record in 'DBWizard\Forms\Form_Tablefield_Record.pas' {frmTablefield_Record: TFVBFFCRecordView},
//  Data_QueryWizard in 'DBWizard\Datamodules\Data_QueryWizard.pas' {dtmQueryWizard: TFVBFFCDataModule},
//  Form_QueryWizard_List in 'DBWizard\Forms\Form_QueryWizard_List.pas' {frmQueryWizard_List: TFVBFFCListView},
//  Form_QueryWizard_Record in 'DBWizard\Forms\Form_QueryWizard_Record.pas' {frmQueryWizard_Record: TFVBFFCRecordView},
//  Data_Report in 'DBWizard\Datamodules\Data_Report.pas' {dtmReport: TFVBFFCDataModule},
//  Form_Report_List in 'DBWizard\Forms\Form_Report_List.pas' {frmReport_List: TFVBFFCListView},
//  Form_Report_Record in 'DBWizard\Forms\Form_Report_Record.pas' {frmReport_Record: TFVBFFCRecordView},
//  form_Attest in 'General\Forms\form_Attest.pas' {frmAttest},
//  form_Attest_VCA in 'General\Forms\form_Attest_VCA.pas' {frmAttest_VCA},
//  Data_ImportOverview in 'Import\Datamodules\Data_ImportOverview.pas' {dtmImportOverview: TFVBFFCDataModule},
//  Form_ImportOverview_List in 'Import\Forms\Form_ImportOverview_List.pas' {frmImportOverview_List: TFVBFFCListView},
//  Form_ImportOverview_Record in 'Import\Forms\Form_ImportOverview_Record.pas' {frmImportOverview_Record: TFVBFFCRecordView},
//  Unit_EdutecInterfaces in 'Units\Unit_EdutecInterfaces.pas',
//  Data_DocCenter in 'DocCenter\Datamodule\Data_DocCenter.pas' {dtmDocCenter: TFVBFFCDataModule},
//  Form_DocCenter_List in 'DocCenter\Forms\Form_DocCenter_List.pas' {frmDocCenter_List: TFVBFFCListView},
//  Form_DocCenter_Record in 'DocCenter\Forms\Form_DocCenter_Record.pas' {frmDocCenter_Record: TFVBFFCRecordView},
//  Data_SesFact in 'SesFact\Datamodules\Data_SesFact.pas' {dtmSesFact: TFVBFFCDataModule},
//  Form_SesFact_List in 'SesFact\Forms\Form_SesFact_List.pas' {frmSesFact_List: TFVBFFCListView},
//  Form_SesFact_Record in 'SesFact\Forms\Form_SesFact_Record.pas' {frmSesFact_Record: TFVBFFCRecordView},
//  data_Crystal in '..\..\..\general units\Data_Crystal.pas' {dtmCrystal: TDataModule},
//  Form_Session_Days in 'Session\Forms\Form_Session_Days.pas' {frmSessionDays},
//  Data_Invoicing in 'Invoicing\Datamodules\Data_Invoicing.pas' {dtmInvoicing: TFVBFFCDataModule},
//  Form_Invoicing_List in 'Invoicing\Forms\Form_Invoicing_List.pas' {frmInvoicing_List: TFVBFFCListView},
//  Form_Invoicing_Record in 'Invoicing\Forms\Form_Invoicing_Record.pas' {frmInvoicing_Record: TFVBFFCRecordView},
//  Data_About in 'About\Data_About.pas',
//  Data_Import in 'Import\Datamodules\Data_Import.pas' {dtmImport: TFVBFFCDataModule},
//  Form_Import_List in 'Import\Forms\Form_Import_List.pas' {frmImport_List: TFVBFFCListView},
//  Form_ImportState in 'Import\Forms\Form_ImportState.pas' {frmImportState},
//  Unit_const in 'Units\Unit_const.pas',
//  Form_Confirmation_Reports in 'General\Forms\Form_Confirmation_Reports.pas' {frmConfirmationReports: TFVBFFCRecordView},
//  Data_ConfirmationReports in 'General\Datamodules\Data_ConfirmationReports.pas' {dtmConfirmationReports: TFVBFFCDataModule},
//  Unit_Global in 'Units\Unit_Global.pas',
//  Form_SessionWizard in 'SessionWizard\Forms\Form_SessionWizard.pas' {frmSessionWizard},
//  Data_SessionWizard in 'SessionWizard\DataModules\Data_SessionWizard.pas' {dtmSessionWizard: TDataModule},
//  Form_LookupPerson in 'SessionWizard\Forms\Form_LookupPerson.pas' {frm_LookupPerson},
//  Data_Lookupperson in 'SessionWizard\DataModules\Data_Lookupperson.pas' {dmLookupPerson: TDataModule},
//  Data_Mailing in 'Mailing\DataModules\Data_Mailing.pas' {dtmMailing: TDataModule},
//  Data_MailingOutlook in 'Mailing\DataModules\Data_MailingOutlook.pas' {dtmMailingOutlook: TDataModule},
//  Data_Cooperation in 'General\Datamodules\Data_Cooperation.pas' {dtmCooperation: TFVBFFCDataModule},
//  Form_Cooperation_Record in 'General\Forms\Form_Cooperation_Record.pas' {frmCooperation_Record: TFVBFFCRecordView},
//  Form_Cooperation_List in 'General\Forms\Form_Cooperation_List.pas' {frmCooperation_List: TFVBFFCListView},
//  Unit_Types in 'Units\Unit_Types.pas',
//  Data_KMO_PORT_Status in 'KMO_Portefeuille\Datamodules\Data_KMO_PORT_Status.pas' {dtmKMO_PORT_Status: TFVBFFCDataModule},
//  Form_KMO_PORT_Status_List in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Status_List.pas' {frmKMO_PORT_Status_List: TFVBFFCListView},
//  Form_KMO_PORT_Status_Record in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Status_Record.pas' {frmKMO_PORT_Status_Record: TFVBFFCRecordView},
//  Data_KMO_Portefeuille in 'KMO_Portefeuille\Datamodules\Data_KMO_Portefeuille.pas' {dtmKMO_Portefeuille: TFVBFFCDataModule},
//  Form_KMO_Portefeuille_List in 'KMO_Portefeuille\Forms\Form_KMO_Portefeuille_List.pas' {frmKMO_Portefeuille_List: TFVBFFCListView},
//  Form_KMO_Portefeuille_Record in 'KMO_Portefeuille\Forms\Form_KMO_Portefeuille_Record.pas' {frmKMO_Portefeuille_Record: TFVBFFCRecordView},
//  Data_KMO_PORT_Payment in 'KMO_Portefeuille\Datamodules\Data_KMO_PORT_Payment.pas' {dtmKMO_PORT_Payment: TFVBFFCDataModule},
//  Form_KMO_PORT_Payment_List in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Payment_List.pas' {frmKMO_PORT_Payment_List: TFVBFFCListView},
//  Form_KMO_PORT_Payment_Record in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Payment_Record.pas' {frmKMO_PORT_Payment_Record: TFVBFFCRecordView},
//  Data_KMO_PORT_Link_Port_Payment in 'KMO_Portefeuille\Datamodules\Data_KMO_PORT_Link_Port_Payment.pas' {dtmKMO_PORT_Link_Port_Payment: TFVBFFCDataModule},
//  Form_KMO_PORT_Link_Port_Payment_List in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Link_Port_Payment_List.pas' {frmKMO_PORT_Link_Port_Payment_List: TFVBFFCListView},
//  Form_KMO_PORT_Link_Port_Payment_Record in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Link_Port_Payment_Record.pas' {frmKMO_PORT_Link_Port_Payment_Record: TFVBFFCRecordView},
//  Data_KMO_PORT_Link_Port_Session in 'KMO_Portefeuille\Datamodules\Data_KMO_PORT_Link_Port_Session.pas' {dtmKMO_PORT_Link_Port_Session: TFVBFFCDataModule},
//  Form_KMO_PORT_Link_Port_Session_Record in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Link_Port_Session_Record.pas',
//  Form_KMO_PORT_Link_Port_Session_List in 'KMO_Portefeuille\Forms\Form_KMO_PORT_Link_Port_Session_List.pas' {frmKMO_PORT_Link_Port_Session_List: TFVBFFCListView},
//  Data_Import_Excel in 'Import_Excel\Datamodules\Data_Import_Excel.pas' {dtmImport_Excel: TFVBFFCDataModule},
//  Form_Import_Excel_List in 'Import_Excel\Forms\Form_Import_Excel_List.pas' {frmImport_Excel_List: TFVBFFCListView},
//  Form_Import_XLS in 'Import_Excel\Forms\Form_Import_XLS.pas' {FormImportXLS: TForm},
//  Data_Import_XLS in 'Import_Excel\Datamodules\Data_Import_XLS.pas' {dtmImport_XLS: TDataModule};

{$R *.res}

begin


  Application.Initialize;
  Application.Title := 'EDGARD';
   {$IFDEF DEBUG}
    Application.Title := 'EDGARD - TEST!!!';
   {$ENDIF}

  Application.CreateForm(TdtmEDUMainClient, dtmEDUMainClient);
  //  Application.CreateForm(TdtmCrystal, dtmCrystal);
//  Application.CreateForm(TdtmImport_XLS, dtmImport_XLS);
  if (TfrmLogin.Create(Application).Login = mrOk) then
  begin
//    frmSplash.Free;
    Application.CreateForm(TfrmEDUMainClient, frmEDUMainClient);
  end;
    Application.Run;
end.


// cheuten : 2.6.0.0 : SQL Server 2005 versie ( verder geen wijzigingen t.o.v 2.5.3.0 )
// cheuten : 2.6.0.0 : IP adres gewijzigd naar nieuwe server
