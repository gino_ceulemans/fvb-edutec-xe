inherited dtmConfirmationReports: TdtmConfirmationReports
  KeyFields = 'F_SESSION_ID'
  ListViewClass = 'TfrmSessionGroup_List'
  RecordViewClass = 'TfrmConfirmationReports'
  Registered = True
  Height = 451
  inherited qryRecord: TFVBFFCQuery
    Connection = dtmFVBFFCMainClient.adocnnMain
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '[F_CONF_ID],'
      '[F_SESSION_ID],'
      '[F_EXTRA_COMPANIES],'
      '[F_EXTRA_INFRASTRUCTURE],'
      '[F_EXTRA_ORGANISATION]'
      'FROM '
      '  V_REP_CONFIRMATION_HEADER'
      'ww')
    object qryRecordF_CONF_ID: TAutoIncField
      FieldName = 'F_CONF_ID'
      ReadOnly = True
    end
    object qryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
    end
    object qryRecordF_EXTRA_COMPANIES: TMemoField
      FieldName = 'F_EXTRA_COMPANIES'
      BlobType = ftMemo
    end
    object qryRecordF_EXTRA_INFRASTRUCTURE: TMemoField
      FieldName = 'F_EXTRA_INFRASTRUCTURE'
      BlobType = ftMemo
    end
    object qryRecordF_EXTRA_ORGANISATION: TMemoField
      FieldName = 'F_EXTRA_ORGANISATION'
      BlobType = ftMemo
    end
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    AfterOpen = cdsRecordAfterOpen
    Top = 168
    object cdsRecordF_CONF_ID: TAutoIncField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsRecordF_EXTRA_COMPANIES: TMemoField
      FieldName = 'F_EXTRA_COMPANIES'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_EXTRA_INFRASTRUCTURE: TMemoField
      FieldName = 'F_EXTRA_INFRASTRUCTURE'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_EXTRA_ORGANISATION: TMemoField
      FieldName = 'F_EXTRA_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    Left = 72
    Top = 216
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmConfirmationReports'
  end
  object prvRecordOrganisation: TFVBFFCDataSetProvider
    DataSet = adoqryRecordOrganisation
    Left = 520
    Top = 200
  end
  object adoqryRecordOrganisation: TFVBFFCQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE],'
      '  [F_LANGUAGE_ID]'
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 2')
    AutoOpen = False
    Left = 520
    Top = 160
    object IntegerField4: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere]
    end
    object IntegerField5: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
    end
    object StringField4: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object SmallintField2: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField6: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object StringField5: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField6: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvRecordCompaniesSchools: TFVBFFCDataSetProvider
    DataSet = adoqryRecordCompaniesSchools
    Left = 384
    Top = 56
  end
  object adoqryRecordCompaniesSchools: TFVBFFCQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE],'
      '  [F_LANGUAGE_ID]'
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 0')
    AutoOpen = False
    Left = 384
    Top = 8
    object adoqryRecordCompaniesSchoolsF_CONF_SUB_ID: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere]
    end
    object adoqryRecordCompaniesSchoolsF_CONF_ID: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORGANISATION_TYPE: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT1_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_ORG_CONTACT2_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object adoqryRecordCompaniesSchoolsF_CONFIRMATION_DATE: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  object adoqryRecordInfrastructure: TFVBFFCQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      '  [F_CONF_SUB_ID],'
      '  [F_CONF_ID],'
      '  [F_ORGANISATION],'
      '  [F_ORGANISATION_TYPE],'
      '  [F_NO_PARTICIPANTS],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL],'
      '  [F_CONFIRMATION_DATE],'
      '  [F_LANGUAGE_ID]'
      'FROM '
      '  V_REP_CONFIRMATION_DETAILS'
      'WHERE F_ORGANISATION_TYPE = 1')
    AutoOpen = False
    Left = 640
    Top = 16
    object IntegerField1: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere]
    end
    object IntegerField2: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere]
    end
    object StringField1: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object SmallintField1: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField3: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object StringField2: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField3: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  object prvRecordInfrastructure: TFVBFFCDataSetProvider
    DataSet = adoqryRecordInfrastructure
    Left = 640
    Top = 64
  end
  object cdsRecordOrganisation: TFVBFFCClientDataSet
    Aggregates = <>
    IndexFieldNames = 'F_SESSION_ID'
    MasterFields = 'F_SESSION_ID'
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '1'
        ParamType = ptInput
        Size = 4
      end>
    ProviderName = 'prvRecordOrganisation'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 520
    Top = 248
    object cdsRecordOrganisationF_CONF_SUB_ID: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsRecordOrganisationF_CONF_ID: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object cdsRecordOrganisationF_ORGANISATION: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordOrganisationF_ORGANISATION_TYPE: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordOrganisationF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordOrganisationF_ORG_CONTACT1_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordOrganisationF_ORG_CONTACT2_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordOrganisationF_CONFIRMATION_DATE: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordOrganisationF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object cdsRecordOrganisationF_PRINT_CONFIRMATION: TBooleanField
      FieldName = 'F_PRINT_CONFIRMATION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordOrganisationF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordOrganisationF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordOrganisationF_INSTRUCTOR_NAME: TStringField
      FieldName = 'F_INSTRUCTOR_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 129
    end
    object cdsRecordOrganisationF_ORG_INSTRUCTOR_NAME: TStringField
      DisplayLabel = 'Organisatie lesgever'
      FieldName = 'F_ORG_INSTRUCTOR_NAME'
      Size = 64
    end
    object cdsRecordOrganisationF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = []
    end
    object cdsRecordOrganisationF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = []
      Size = 512
    end
    object cdsRecordOrganisationF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object cdsRecordCompaniesSchools: TFVBFFCClientDataSet
    Aggregates = <>
    IndexFieldNames = 'F_SESSION_ID'
    MasterFields = 'F_SESSION_ID'
    Params = <
      item
        DataType = ftInteger
        Name = '1'
        ParamType = ptInput
        Size = 4
      end>
    ProviderName = 'prvRecordCompaniesSchools'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 384
    Top = 104
    object IntegerField7: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object IntegerField8: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object StringField7: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object SmallintField3: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField9: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object StringField8: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField9: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordCompaniesSchoolsF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object cdsRecordCompaniesSchoolsF_PRINT_CONFIRMATION: TBooleanField
      FieldName = 'F_PRINT_CONFIRMATION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordCompaniesSchoolsF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordCompaniesSchoolsF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordCompaniesSchoolsF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = []
    end
    object cdsRecordCompaniesSchoolsF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object cdsRecordCompaniesSchoolsF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = []
      Size = 512
    end
    object cdsRecordCompaniesSchoolsF_PARTNER_WINTER_NAME: TStringField
      FieldName = 'F_PARTNER_WINTER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordCompaniesSchoolsF_PARTNER_WINTER_ID: TIntegerField
      FieldName = 'F_PARTNER_WINTER_ID'
      ProviderFlags = []
    end
    object cdsRecordCompaniesSchoolsF_ISCCENABLED: TBooleanField
      FieldName = 'F_ISCCENABLED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT1'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1_EMAIL: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT1_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT2'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2_EMAIL: TStringField
      FieldName = 'F_PARTNER_WINTER_CONTACT2_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object cdsRecordCompaniesSchoolsF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object cdsRecordInfrastructure: TFVBFFCClientDataSet
    Aggregates = <>
    IndexFieldNames = 'F_SESSION_ID'
    MasterFields = 'F_SESSION_ID'
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '1'
        ParamType = ptInput
        Size = 4
      end>
    ProviderName = 'prvRecordInfrastructure'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 640
    Top = 112
    object IntegerField10: TIntegerField
      FieldName = 'F_CONF_SUB_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object IntegerField11: TIntegerField
      FieldName = 'F_CONF_ID'
      ProviderFlags = [pfInWhere, pfInKey]
    end
    object StringField10: TStringField
      FieldName = 'F_ORGANISATION'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
      Size = 64
    end
    object SmallintField4: TSmallintField
      FieldName = 'F_ORGANISATION_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField12: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = [pfInUpdate]
    end
    object StringField11: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object StringField12: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'F_CONFIRMATION_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordInfrastructureF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = []
    end
    object cdsRecordInfrastructureF_PRINT_CONFIRMATION: TBooleanField
      FieldName = 'F_PRINT_CONFIRMATION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordInfrastructureF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordInfrastructureF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordInfrastructureF_LOCATION: TStringField
      FieldName = 'F_LOCATION'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object cdsRecordInfrastructureF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = []
    end
    object cdsRecordInfrastructureF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
  end
  object dtsRecord: TFVBFFCDataSource
    DataSet = cdsRecord
    Left = 264
    Top = 136
  end
  object cdsAttestRegistered: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@SessionId'
        ParamType = ptInput
      end>
    ProviderName = 'prvAttestRegistered'
    RemoteServer = ssckData
    AutoOpen = False
    Left = 274
    Top = 248
  end
end
