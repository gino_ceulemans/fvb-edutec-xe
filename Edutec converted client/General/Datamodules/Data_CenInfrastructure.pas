{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_CenInfrastructure
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_CenInfrastructure;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, DB, ADODB,
  Unit_FVBFFCInterfaces, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmCenInfrastructure = class(TEduDataModule, IEDUCenInfrastructureDataModule)
    qryListF_INFRASTRUCTURE_ID: TIntegerField;
    qryListF_NAME: TStringField;
    qryListF_FVB_CENTER_ID: TIntegerField;
    qryListF_RSZ_NR: TStringField;
    qryListF_VAT_NR: TStringField;
    qryListF_STREET: TStringField;
    qryListF_NUMBER: TStringField;
    qryListF_MAILBOX: TStringField;
    qryListF_POSTALCODE_ID: TIntegerField;
    qryListF_POSTALCODE: TStringField;
    qryListF_CITY_NAME: TStringField;
    qryListF_PHONE: TStringField;
    qryListF_FAX: TStringField;
    qryListF_EMAIL: TStringField;
    qryListF_WEBSITE: TStringField;
    qryListF_LANGUAGE_ID: TIntegerField;
    qryListF_LANGUAGE_NAME: TStringField;
    qryListF_ACC_NR: TStringField;
    qryListF_ACC_HOLDER: TStringField;
    qryListF_MEDIUM_ID: TIntegerField;
    qryListF_MEDIUM_NAME: TStringField;
    qryListF_ACTIVE: TBooleanField;
    qryListF_END_DATE: TDateTimeField;
    qryListF_COMMENT: TMemoField;
    qryListF_SATURDAY: TBooleanField;
    qryListF_EVENING: TBooleanField;
    qryListF_WEEKDAY: TBooleanField;
    qryListF_LOCATION_ID: TIntegerField;
    qryListF_LOCATION: TStringField;
    qryListF_PRECONDITION_ID: TIntegerField;
    qryListF_PRECONDITION: TStringField;
    qryRecordF_INFRASTRUCTURE_ID: TIntegerField;
    qryRecordF_NAME: TStringField;
    qryRecordF_FVB_CENTER_ID: TIntegerField;
    qryRecordF_RSZ_NR: TStringField;
    qryRecordF_VAT_NR: TStringField;
    qryRecordF_STREET: TStringField;
    qryRecordF_NUMBER: TStringField;
    qryRecordF_MAILBOX: TStringField;
    qryRecordF_POSTALCODE_ID: TIntegerField;
    qryRecordF_POSTALCODE: TStringField;
    qryRecordF_CITY_NAME: TStringField;
    qryRecordF_PHONE: TStringField;
    qryRecordF_FAX: TStringField;
    qryRecordF_EMAIL: TStringField;
    qryRecordF_WEBSITE: TStringField;
    qryRecordF_LANGUAGE_ID: TIntegerField;
    qryRecordF_LANGUAGE_NAME: TStringField;
    qryRecordF_ACC_NR: TStringField;
    qryRecordF_ACC_HOLDER: TStringField;
    qryRecordF_MEDIUM_ID: TIntegerField;
    qryRecordF_MEDIUM_NAME: TStringField;
    qryRecordF_ACTIVE: TBooleanField;
    qryRecordF_END_DATE: TDateTimeField;
    qryRecordF_COMMENT: TMemoField;
    qryRecordF_SATURDAY: TBooleanField;
    qryRecordF_EVENING: TBooleanField;
    qryRecordF_WEEKDAY: TBooleanField;
    qryRecordF_LOCATION_ID: TIntegerField;
    qryRecordF_LOCATION: TStringField;
    qryRecordF_PRECONDITION_ID: TIntegerField;
    qryRecordF_PRECONDITION: TStringField;
    cdsListF_INFRASTRUCTURE_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_FVB_CENTER_ID: TIntegerField;
    cdsListF_RSZ_NR: TStringField;
    cdsListF_VAT_NR: TStringField;
    cdsListF_STREET: TStringField;
    cdsListF_NUMBER: TStringField;
    cdsListF_MAILBOX: TStringField;
    cdsListF_POSTALCODE_ID: TIntegerField;
    cdsListF_POSTALCODE: TStringField;
    cdsListF_CITY_NAME: TStringField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsListF_WEBSITE: TStringField;
    cdsListF_LANGUAGE_ID: TIntegerField;
    cdsListF_LANGUAGE_NAME: TStringField;
    cdsListF_ACC_NR: TStringField;
    cdsListF_ACC_HOLDER: TStringField;
    cdsListF_MEDIUM_ID: TIntegerField;
    cdsListF_MEDIUM_NAME: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_COMMENT: TMemoField;
    cdsListF_SATURDAY: TBooleanField;
    cdsListF_EVENING: TBooleanField;
    cdsListF_WEEKDAY: TBooleanField;
    cdsListF_LOCATION_ID: TIntegerField;
    cdsListF_LOCATION: TStringField;
    cdsListF_PRECONDITION_ID: TIntegerField;
    cdsListF_PRECONDITION: TStringField;
    cdsRecordF_INFRASTRUCTURE_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_FVB_CENTER_ID: TIntegerField;
    cdsRecordF_RSZ_NR: TStringField;
    cdsRecordF_VAT_NR: TStringField;
    cdsRecordF_STREET: TStringField;
    cdsRecordF_NUMBER: TStringField;
    cdsRecordF_MAILBOX: TStringField;
    cdsRecordF_POSTALCODE_ID: TIntegerField;
    cdsRecordF_POSTALCODE: TStringField;
    cdsRecordF_CITY_NAME: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_WEBSITE: TStringField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_ACC_NR: TStringField;
    cdsRecordF_ACC_HOLDER: TStringField;
    cdsRecordF_MEDIUM_ID: TIntegerField;
    cdsRecordF_MEDIUM_NAME: TStringField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_COMMENT: TMemoField;
    cdsRecordF_SATURDAY: TBooleanField;
    cdsRecordF_EVENING: TBooleanField;
    cdsRecordF_WEEKDAY: TBooleanField;
    cdsRecordF_LOCATION_ID: TIntegerField;
    cdsRecordF_LOCATION: TStringField;
    cdsRecordF_PRECONDITION_ID: TIntegerField;
    cdsRecordF_PRECONDITION: TStringField;
    cdsSearchCriteriaF_NAME: TStringField;
    cdsSearchCriteriaF_POSTALCODE: TStringField;
    cdsSearchCriteriaF_CITY_NAME: TStringField;
    cdsSearchCriteriaF_LANGUAGE_ID: TIntegerField;
    cdsSearchCriteriaF_LANGUAGE_NAME: TStringField;
    cdsSearchCriteriaF_ACTIVE: TBooleanField;
    cdsRecordF_CONFIRMATION_CONTACT1: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT2: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT3: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField;
    cdsRecordF_INFO: TMemoField;
    cdsListF_MAIL_ATTACHMENT: TStringField;
    cdsRecordF_MAIL_ATTACHMENT: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
    procedure cdsListNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectLanguage   ( aDataSet : TDataSet ); virtual;
    procedure SelectMedium     ( aDataSet : TDataSet ); virtual;
    procedure SelectPostalCode ( aDataSet : TDataSet ); virtual;
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowPostalCode   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectCenInfrastructure(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure ShowCenInfrastructure  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

  procedure CopyCenInfrastructureFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_Language, Data_Medium, Data_Postalcode,
  Data_FVBFFCBaseDataModule;

{*****************************************************************************
  This procedure will be used to copy CenInfrastructure related fields from one
  DataSet to another DataSet.

  @Name       CopyCenInfrastructureFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyCenInfrastructureFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    case aCopyTo of
      critInfrastructureToSession, critInfrastructureToSesWizard, critInfrastructureToCenRoom :
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger :=
            aSource.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_INFRASTRUCTURE_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end
      else
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger :=
            aSource.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more CenInfrastructure Records.

  @Name       TdtmCenInfrastructure.SelectCenInfrastructure
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmCenInfrastructure.SelectCenInfrastructure(
  aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False
      );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmCenInfrastructure', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyCenInfrastructureFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single CenInfrastructure
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmCenInfrastructure.ShowCenInfrastructure
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmCenInfrastructure.ShowCenInfrastructure(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmCenInfrastructure', 'F_INFRASTRUCTURE_ID = ' + IntToStr (aDataSet.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger) , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyCenInfrastructureFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmCenInfrastructure.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_CEN_INFRASTRUCTURE';
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmCenInfrastructure.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which we should initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be used to initialise some Additional Fields.

  @Name       TdtmCenInfrastructure.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which we should initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_SATURDAY' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_EVENING' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_WEEKDAY' ).AsBoolean := True;
end;

{*****************************************************************************
  This method will be used to select one or more Language Records.

  @Name       TdtmCenInfrastructure.SelectLanguage
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.SelectLanguage(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_LANGUAGE_ID <> ' + aIDField.AsString;
  end;

  TdtmLanguage.SelectLanguage( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more Medium Records.

  @Name       TdtmCenInfrastructure.SelectMedium
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.SelectMedium(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Medium isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_MEDIUM_ID <> ' + aIDField.AsString;
  end;

  TdtmMedium.SelectMedium( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more PostalCode Records.

  @Name       TdtmCenInfrastructure.SelectPostalCode
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.SelectPostalCode(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_POSTALCODE_ID <> ' + aIDField.AsString;
  end;

  TdtmPostalcode.SelectPostalCode( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Language.

  @Name       TdtmCenInfrastructure.ShowLanguage
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Medium.

  @Name       TdtmCenInfrastructure.ShowMedium
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.ShowMedium(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmMedium.ShowMedium( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a PostalCode.

  @Name       TdtmCenInfrastructure.ShowPostalCode
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenInfrastructure.ShowPostalCode(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmPostalcode.ShowPostalCode( aDataSet, aRecordViewMode );
  end;
end;


procedure TdtmCenInfrastructure.cdsSearchCriteriaNewRecord(
  DataSet: TDataSet);
begin
  inherited;
  cdsSearchCriteria.FieldByName( 'F_ACTIVE' ).Value := True;
end;

procedure TdtmCenInfrastructure.cdsListNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('F_LANGUAGE_ID').AsInteger := 1; // default Dutch
end;

end.
