inherited dtmGender: TdtmGender
  KeyFields = 'F_GENDER_ID'
  ListViewClass = 'TfrmGender_List'
  RecordViewClass = 'TfrmGender_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_GENDER_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_GENDER_NAME '
      'FROM '
      '  V_GE_GENDER')
    object qryListF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryListF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_GENDER_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_GENDER_NAME '
      'FROM '
      '  V_GE_GENDER')
    object qryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_GENDER_ID: TIntegerField
      DisplayLabel = 'Geslacht ( ID )'
      FieldName = 'F_GENDER_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Geslacht ( NL )'
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Geslacht ( FR )'
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object cdsListF_GENDER_NAME: TStringField
      DisplayLabel = 'Geslacht'
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_GENDER_ID: TIntegerField
      DisplayLabel = 'Geslacht ( ID )'
      FieldName = 'F_GENDER_ID'
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Geslacht ( NL )'
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Geslacht ( FR )'
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object cdsRecordF_GENDER_NAME: TStringField
      DisplayLabel = 'Geslacht'
      FieldName = 'F_GENDER_NAME'
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmGender'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmGender'
  end
end
