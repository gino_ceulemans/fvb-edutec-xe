{*****************************************************************************
  This DataModule will be used for the maintenance of T_RO_ROLE.

  @Name       Data_Role
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Role;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmRole = class(TEduDataModule, IEDURoleDataModule )
    cdsListF_ROLE_ID: TIntegerField;
    cdsListF_PERSON_ID: TIntegerField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_RSZ_NR: TStringField;
    cdsListF_ROLEGROUP_ID: TIntegerField;
    cdsListF_ROLEGROUP_NAME: TStringField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_ACTIVE: TBooleanField;
    cdsListF_PRINCIPAL: TBooleanField;
    cdsListF_SCHOOLYEAR_ID: TIntegerField;
    cdsListF_SCHOOLYEAR: TStringField;
    cdsRecordF_ROLE_ID: TIntegerField;
    cdsRecordF_PERSON_ID: TIntegerField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_RSZ_NR: TStringField;
    cdsRecordF_ROLEGROUP_ID: TIntegerField;
    cdsRecordF_ROLEGROUP_NAME: TStringField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_PRINCIPAL: TBooleanField;
    cdsRecordF_SCHOOLYEAR_ID: TIntegerField;
    cdsRecordF_SCHOOLYEAR: TStringField;
    cdsSearchCriteriaF_LASTNAME: TStringField;
    cdsSearchCriteriaF_FIRSTNAME: TStringField;
    cdsSearchCriteriaF_SOCSEC_NR: TStringField;
    cdsSearchCriteriaF_NAME: TStringField;
    cdsSearchCriteriaF_RSZ_NR: TStringField;
    cdsSearchCriteriaF_ROLEGROUP_ID: TIntegerField;
    cdsSearchCriteriaF_ROLEGROUP_NAME: TStringField;
    cdsSearchCriteriaF_ACTIVE: TBooleanField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsListF_REMARK: TStringField;
    cdsRecordF_REMARK: TStringField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_GSM: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_GSM: TStringField;
    cdsRecordF_EMAIL: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FCreateForPerson: Boolean;
    FCreateForPersonID: Integer;
    FCreateForPersonFirstName: String;
    FCreateForPersonLastName: String;
    FCreateForPersonSocSecNr: String;

  protected
    { Protected declarations }
    function GetCreateForPerson: Boolean; virtual;
    function GetCreateForPersonFirstName: String; virtual;
    function GetCreateForPersonID: Integer; virtual;
    function GetCreateForPersonLastName: String; virtual;
    function GetCreateForPersonSocSecNr: String; virtual;

    procedure AddForPerson        ( aDataSet : TDataSet ); virtual;
    procedure InitialisePersonInfo( aDataSet : TDataSet ); virtual;
    procedure SelectOrganisation ( aDataSet : TDataSet ); virtual;
    procedure SelectPerson       ( aDataSet : TDataSet ); virtual;
    procedure SelectRoleGroup    ( aDataSet : TDataSet ); virtual;
    procedure SelectSchoolyear   ( aDataSet : TDataSet ); virtual;
    procedure ShowOrganisation   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowPerson         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowRoleGroup      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowSchoolyear     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

    property CreateForPerson          : Boolean read GetCreateForPerson;
    property CreateForPersonID        : Integer read GetCreateForPersonID;
    property CreateForPersonFirstName : String  read GetCreateForPersonFirstName;
    property CreateForPersonLastName  : String  read GetCreateForPersonLastName;
    property CreateForPersonSocSecNr  : String  read GetCreateForPersonSocSecNr;

  public
    { Public declarations }
    class Procedure CreateRoleForPerson  ( aPersonDataSet, aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    class procedure LinkRoles( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = True; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure SelectRole(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure ShowRole  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

  procedure CopyRoleFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, DateUtils, Data_Organisation, Data_Person, Unit_PPWFrameWorkUtils,
  Data_RoleGroup, Unit_PPWFrameWorkInterfaces, Unit_FVBFFCDataModule,
  Data_Schoolyear, Data_FVBFFCBaseDataModule, Unit_PPWFrameWorkDataModule;

{*****************************************************************************
  This procedure will be used to copy Role related fields from one
  DataSet to another DataSet.

  @Name       CopyRoleFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyRoleFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_ROLE_ID' ).AsInteger :=
        aSource.FieldByName( 'F_ROLE_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      critRoleToEnrolment :
      begin
        aDestination.FieldByName ( 'F_PERSON_ID' ).AsInteger :=
          aSource.FieldByName( 'F_PERSON_ID' ).AsInteger;
        aDestination.FieldByName ( 'F_LASTNAME' ).AsString :=
          aSource.FieldByName( 'F_LASTNAME' ).AsString;
        aDestination.FieldByName ( 'F_FIRSTNAME' ).AsString :=
          aSource.FieldByName( 'F_FIRSTNAME' ).AsString;
        aDestination.FieldByName ( 'F_SOCSEC_NR' ).AsString :=
          aSource.FieldByName( 'F_SOCSEC_NR' ).AsString;
        // Naam van het bedrijf ( ook tonen - niet te wijzigen )
        aDestination.FieldByName ( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName ( 'F_ROLEGROUP_ID' ).AsString :=
          aSource.FieldByName( 'F_ROLEGROUP_ID' ).AsString;
      end;
      critRoleToSesWizard :
      begin
        aDestination.FieldByName ( 'F_PERSON_ID' ).AsInteger :=
          aSource.FieldByName( 'F_PERSON_ID' ).AsInteger;
        aDestination.FieldByName ( 'F_LASTNAME' ).AsString :=
          aSource.FieldByName( 'F_LASTNAME' ).AsString;
        aDestination.FieldByName ( 'F_FIRSTNAME' ).AsString :=
          aSource.FieldByName( 'F_FIRSTNAME' ).AsString;
        aDestination.FieldByName ( 'F_SOCSEC_NR' ).AsString :=
          aSource.FieldByName( 'F_SOCSEC_NR' ).AsString;
        aDestination.FieldByName ( 'F_ORGANISATION_ID' ).AsInteger :=
          aSource.FieldByName( 'F_ORGANISATION_ID' ).AsInteger;
        aDestination.FieldByName ( 'F_ORGANISATION_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName ( 'F_ROLEGROUP_ID' ).AsInteger :=
          aSource.FieldByName( 'F_ROLEGROUP_ID' ).AsInteger;
        aDestination.FieldByName ( 'F_ROLEGROUP_NAME' ).AsString :=
          aSource.FieldByName( 'F_ROLEGROUP_NAME' ).AsString;
        aDestination.FieldByName ( 'F_START_DATE' ).AsDateTime :=
          aSource.FieldByName( 'F_START_DATE' ).AsDateTime;
//        aDestination.FieldByName ( 'F_END_DATE' ).AsDateTime :=
//          aSource.FieldByName( 'F_END_DATE' ).AsDateTime;
        aDestination.FieldByName ( 'F_ACTIVE' ).AsBoolean :=
          aSource.FieldByName( 'F_ACTIVE' ).AsBoolean;
      end;
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_ROLE_NAME' ).AsString :=
          aSource.FieldByName( 'F_ROLE_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Role Records.

  @Name       TdtmRole.SelectRole
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmRole.SelectRole(
  aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False
  );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmRole', aWhere ,
       aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyRoleFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Role
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmRole.ShowRole
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmRole.ShowRole(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aWhere : string;
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    // Controle of veld is ingevuld zodat we zeker record kunnen selecteren
    if (aDataSet.FieldByName( 'F_ROLE_ID' ).IsNull)
      and not (aRecordViewMode = rvmAdd) then
    begin
       exit;
    end;
    if not (aRecordViewMode = rvmAdd) then
    begin
      aWhere := 'F_ROLE_ID = ' + aDataSet.FieldByName( 'F_ROLE_ID' ).AsString;
    end
    else
    begin
      aWhere := '';
    end;

    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmRole',
      aWhere,
      aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 )  And ( aRecordViewMode <> rvmView ) then
      begin
        CopyRoleFieldValues( aSelectedRecords, aDataSet,
         ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmRole.prvListGetTableName(Sender: TObject; DataSet: TDataSet;
  var TableName: String);
begin
  inherited;
  TableName := 'T_RO_ROLE';
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmRole.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ROLE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be used to initialise some Additional Fields.

  @Name       TdtmRole.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( CreateForPerson ) then
  begin
    aDataSet.FieldByName( 'F_PERSON_ID' ).AsInteger := CreateForPersonID;
    aDataSet.FieldByName( 'F_LASTNAME' ).AsString := CreateForPersonLastName;
    aDataSet.FieldByName( 'F_FIRSTNAME' ).AsString := CreateForPersonFirstName;
    aDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString := CreateForPersonSocSecNr;
  end;

  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_PRINCIPAL' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_START_DATE' ).AsDateTime := Today;

  aDataSet.FieldByName( 'F_ROLEGROUP_ID' ).AsInteger := 18;
  aDataSet.FieldByName( 'F_ROLEGROUP_NAME' ).AsString := 'Overig';

end;

{*****************************************************************************
  This method will be used to initialise some Foreign Key Fields.

  @Name       TdtmRole.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  { If the DataModule is shown as a detail of another DataModule we might want
    to initialise some fields with values from the MasterRecord }
  If ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUOrganisationDataModule ) ) then
    begin
      CopyOrganisationFieldValues(
        MasterDataModule.RecordDataset,
        aDataset,
        True,
        critOrganisationToRole
      );
{
      aDataSet.FieldByName( 'F_NAME' ).AsString :=
        MasterDataModule.RecordDataset.FieldByName( 'F_NAME' ).AsString;
      aDataSet.FieldByName( 'F_RSZ_NR' ).AsString :=
        MasterDataModule.RecordDataset.FieldByName( 'F_RSZ_NR' ).AsString;
}
    end
    else if ( Supports( MasterDataModule, IEDUPersonDataModule ) ) then
    begin
      CopyPersonFieldValues(
        MasterDataModule.RecordDataset,
        aDataSet,
        False,
        critPersonToRole
      );
{
      aDataSet.FieldByName( 'F_LASTNAME' ).AsString :=
        MasterDataModule.RecordDataset.FieldByName( 'F_LASTNAME' ).AsString;
      aDataSet.FieldByName( 'F_FIRSTNAME' ).AsString :=
        MasterDataModule.RecordDataset.FieldByName( 'F_FIRSTNAME' ).AsString;
      aDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString :=
        MasterDataModule.RecordDataset.FieldByName( 'F_SOCSEC_NR' ).AsString;
}
    end;
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Organisation Records.

  @Name       TdtmRole.SelectOrganisation
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.SelectOrganisation(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUOrganisationDataModule ) ) ) then
  begin
    aCopyTo := critOrganisationToRole;
    aIDField := aDataSet.FindField( 'F_ORGANISATION_ID' );

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current PostalCode isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_ORGANISATION_ID <> ' + aIDField.AsString;
    end;

    TdtmOrganisation.SelectOrganisation( aDataSet, aWhere, False, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Person Records.

  @Name       TdtmRole.SelectPerson
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.SelectPerson(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUPersonDataModule ) ) ) then
  begin
    aCopyTo := critPersonToRole;
    aIDField := aDataSet.FindField( 'F_PERSON_ID' );

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current PostalCode isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_PERSON_ID <> ' + aIDField.AsString;
    end;

    TdtmPerson.SelectPerson( aDataSet, aWhere, False, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more RoleGroup Records.

  @Name       TdtmRole.SelectRoleGroup
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.SelectRoleGroup(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_ROLEGROUP_ID' );

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_ROLEGROUP_ID <> ' + aIDField.AsString;
  end;

  TdtmRoleGroup.SelectRoleGroup( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Show an Organistion.

  @Name       TdtmRole.ShowOrganisation
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.ShowOrganisation(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUOrganisationDataModule ) ) ) then
  begin
    aCopyTo := critOrganisationToRole;
    aIDField := aDataSet.FindField( 'F_ORGANISATION_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmOrganisation.ShowOrganisation( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Person.

  @Name       TdtmRole.ShowPerson
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.ShowPerson(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUPersonDataModule ) ) ) then
  begin
    aCopyTo := critPersonToRole;
    aIDField := aDataSet.FindField( 'F_PERSON_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmPerson.ShowPerson( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a RoleGroup.

  @Name       TdtmRole.ShowRoleGroup
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRole.ShowRoleGroup(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_ROLEGROUP_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmRoleGroup.ShowRoleGroup( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

class procedure TdtmRole.LinkRoles(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the Country ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmRole', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      aSelectedRecords.First;
      while not aSelectedRecords.Eof do
      begin
        aDataSet.Tag := 1;

        if ( aDataSet.Locate( 'F_ROLE_ID', aSelectedRecords.FieldByName( 'F_ROLE_ID' ).AsInteger, [ loCaseInsensitive ] ) ) then
        begin
          aDataSet.Edit;
        end
        else
        begin
          aDataSet.Insert;
        end;
        CopyRoleFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
        aDataSet.Post;
        aSelectedRecords.Next;
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

class procedure TdtmRole.CreateRoleForPerson(aPersonDataSet,
  aDataSet: TDataSet; aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aDataModule       : IPPWFrameWorkDataModule;
  aRoleDataModule   : IEDURoleDataModule;
  aSelectedRecords : TClientDataSet;
  aRecordView      : IPPWFrameWorkRecordView;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    aDataModule := dtmEDUMainClient.pfcMain.CreateDataModule( Nil, 'TdtmRole', False );
    aDataModule.DoFilterDetail( 'F_PERSON_ID', aDataSet.FieldByName( 'F_PERSON_ID' ).AsInteger, 'F_PERSON_ID = ' + aDataSet.FieldByName( 'F_PERSON_ID' ).AsString );

    if ( Supports( aDataModule, IEDURoleDataModule, aRoleDataModule ) ) then
    begin
      aRoleDataModule.InitialisePersonInfo( aDataSet );
      aRecordView := aRoleDataModule.CreateRecordViewForCurrentRecord( rvmAdd, True );
      if ( aRecordView.ShowModal = mrOK ) then
      begin
        CloneStructur( aRecordView.RecordViewDatamodule.RecordDataset,
                       aSelectedRecords );
        CloneRecord  ( aRecordView.RecordViewDatamodule.RecordDataset,
                       aSelectedRecords );
        if ( aSelectedRecords.RecordCount = 1 ) then
        begin
          CopyRoleFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
        end;
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmRole.InitialisePersonInfo(aDataSet: TDataSet);
begin
  FCreateForPerson          := True;
  FCreateForPersonID        := aDataSet.FieldByName( 'F_PERSON_ID' ).AsInteger;
  FCreateForPersonLastName  := aDataSet.FieldByName( 'F_LASTNAME' ).AsString;
  FCreateForPersonFirstName := aDataSet.FieldByName( 'F_FIRSTNAME' ).AsString;
  FCreateForPersonSocSecNr  := aDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString;
end;

procedure TdtmRole.AddForPerson(aDataSet: TDataSet);
var
  aRecordView : IPPWFrameWorkRecordView;
begin
  aRecordView := CreateRecordViewForCurrentRecord( rvmAdd );
  aRecordView.ShowModal;
end;

function TdtmRole.GetCreateForPerson: Boolean;
var
  aRoleDataModule : IEDURoleDataModule;
begin
  if ( Assigned( ListViewDataModule ) and
       Supports( ListViewDataModule, IEDURoleDataModule, aRoleDataModule ) ) then
  begin
    Result := aRoleDataModule.CreateForPerson;
  end
  else
  begin
    Result := FCreateForPerson;
  end;
end;

function TdtmRole.GetCreateForPersonFirstName: String;
var
  aRoleDataModule : IEDURoleDataModule;
begin
  if ( Assigned( ListViewDataModule ) and
       Supports( ListViewDataModule, IEDURoleDataModule, aRoleDataModule ) ) then
  begin
    Result := aRoleDataModule.CreateForPersonFirstName;
  end
  else
  begin
    Result := FCreateForPersonFirstName;
  end;
end;

function TdtmRole.GetCreateForPersonID: Integer;
var
  aRoleDataModule : IEDURoleDataModule;
begin
  if ( Assigned( ListViewDataModule ) and
       Supports( ListViewDataModule, IEDURoleDataModule, aRoleDataModule ) ) then
  begin
    Result := aRoleDataModule.CreateForPersonID;
  end
  else
  begin
    Result := FCreateForPersonID;
  end;
end;

function TdtmRole.GetCreateForPersonLastName: String;
var
  aRoleDataModule : IEDURoleDataModule;
begin
  if ( Assigned( ListViewDataModule ) and
       Supports( ListViewDataModule, IEDURoleDataModule, aRoleDataModule ) ) then
  begin
    Result := aRoleDataModule.CreateForPersonLastName;
  end
  else
  begin
    Result := FCreateForPersonLastName;
  end;
end;

function TdtmRole.GetCreateForPersonSocSecNr: String;
var
  aRoleDataModule : IEDURoleDataModule;
begin
  if ( Assigned( ListViewDataModule ) and
       Supports( ListViewDataModule, IEDURoleDataModule, aRoleDataModule ) ) then
  begin
    Result := aRoleDataModule.CreateForPersonSocSecNr;
  end
  else
  begin
    Result := FCreateForPersonSocSecNr;
  end;
end;

procedure TdtmRole.SelectSchoolyear(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_SCHOOLYEAR_ID' );

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_SCHOOLYEAR_ID <> ' + aIDField.AsString;
  end;

  TdtmSchoolyear.SelectSchoolyear( aDataSet, aWhere, False, aCopyTo );
end;

procedure TdtmRole.ShowSchoolyear(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_SCHOOLYEAR_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmSchoolyear.ShowSchoolyear( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

procedure TdtmRole.cdsSearchCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;

  if not ( AsDetail ) then
  begin
    cdsSearchCriteria.FieldByName( 'F_ACTIVE' ).Value := True;
  end;

end;

end.
