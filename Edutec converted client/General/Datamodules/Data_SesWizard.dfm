inherited dtmSesWizard: TdtmSesWizard
  OnCreate = FVBFFCDataModuleCreate
  DataSet = cdsSessionWizard
  RecordDataset = cdsSessionWizard
  KeyFields = 'F_SESWIZARD_ID'
  ListViewClass = 'TfrmSesWizard_List'
  RecordViewClass = 'TfrmSesWizard_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Width = 946
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    AutoOpen = False
    object cdsListF_SESWIZARD_ID: TIntegerField
      DisplayLabel = 'Wizard ID'
      FieldName = 'F_SESWIZARD_ID'
    end
    object cdsListF_SPID: TIntegerField
      DisplayLabel = 'SP ID'
      FieldName = 'F_SPID'
    end
    object cdsListF_EXTERN_NR: TStringField
      DisplayLabel = 'Extern Nr'
      FieldName = 'F_EXTERN_NR'
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
    end
    object cdsListF_COMPANY_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_COMPANY_NAME'
      Size = 64
    end
    object cdsListF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ID'
      FieldName = 'F_PERSON_ID'
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
    end
    object cdsListF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ID'
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsListF_PROGRAM_NAME: TStringField
      DisplayLabel = 'Programma Naam'
      FieldName = 'F_PROGRAM_NAME'
      Size = 64
    end
    object cdsListF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie Naam'
      FieldName = 'F_SESSION_NAME'
      Size = 64
    end
    object cdsListF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie Code'
      FieldName = 'F_SESSION_CODE'
    end
    object cdsListF_SESSION_TRANSPORT_INS: TStringField
      DisplayLabel = 'Transport'
      FieldName = 'F_SESSION_TRANSPORT_INS'
      Size = 50
    end
    object cdsListF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_SESSION_START_DATE'
    end
    object cdsListF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
    end
    object cdsListF_SESSION_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_SESSION_COMMENT'
      Size = 250
    end
    object cdsListF_SESSION_START_HOUR: TStringField
      DisplayLabel = 'Start Uur'
      FieldName = 'F_SESSION_START_HOUR'
      Size = 50
    end
    object cdsListF_ENROL_ID: TIntegerField
      DisplayLabel = 'Inschrijving ID'
      FieldName = 'F_ENROL_ID'
    end
    object cdsListF_ENROL_EVALUATION: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_ENROL_EVALUATION'
    end
    object cdsListF_ENROL_CERTIFICATE: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_ENROL_CERTIFICATE'
    end
    object cdsListF_ENROL_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_ENROL_COMMENT'
      Size = 250
    end
    object cdsListF_ENROL_INVOICED: TBooleanField
      DisplayLabel = 'Gefaktureerd'
      FieldName = 'F_ENROL_INVOICED'
    end
    object cdsListF_ENROL_LAST_MINUTE: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_ENROL_LAST_MINUTE'
    end
    object cdsListF_ENROL_HOURS: TIntegerField
      DisplayLabel = 'Uren'
      FieldName = 'F_ENROL_HOURS'
    end
    object cdsListF_SES_CREATE: TBooleanField
      DisplayLabel = 'Sessie Aanmaken'
      FieldName = 'F_SES_CREATE'
    end
    object cdsListF_SES_UPDATE: TBooleanField
      DisplayLabel = 'Sessie Aanpassen'
      FieldName = 'F_SES_UPDATE'
    end
    object cdsListF_ENR_CREATE: TBooleanField
      DisplayLabel = 'Inschrijving Aanmaken'
      FieldName = 'F_ENR_CREATE'
    end
    object cdsListF_ENR_UPDATE: TBooleanField
      DisplayLabel = 'Inschrijving Aanpassen'
      FieldName = 'F_ENR_UPDATE'
    end
    object cdsListF_FORECAST_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_FORECAST_COMMENT'
      Size = 250
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_SESWIZARD_ID: TIntegerField
      DisplayLabel = 'Wizard ID'
      FieldName = 'F_SESWIZARD_ID'
    end
    object cdsRecordF_SPID: TIntegerField
      DisplayLabel = 'SP ID'
      FieldName = 'F_SPID'
    end
    object cdsRecordF_EXTERN_NR: TStringField
      DisplayLabel = 'Extern Nr'
      FieldName = 'F_EXTERN_NR'
    end
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
    end
    object cdsRecordF_COMPANY_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_COMPANY_NAME'
      Size = 64
    end
    object cdsRecordF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ID'
      FieldName = 'F_PERSON_ID'
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
    end
    object cdsRecordF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ID'
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsRecordF_PROGRAM_NAME: TStringField
      DisplayLabel = 'Programma Naam'
      FieldName = 'F_PROGRAM_NAME'
      Size = 64
    end
    object cdsRecordF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie Naam'
      FieldName = 'F_SESSION_NAME'
      Size = 64
    end
    object cdsRecordF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie Code'
      FieldName = 'F_SESSION_CODE'
    end
    object cdsRecordF_SESSION_TRANSPORT_INS: TStringField
      DisplayLabel = 'Transport'
      FieldName = 'F_SESSION_TRANSPORT_INS'
      Size = 50
    end
    object cdsRecordF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_SESSION_START_DATE'
    end
    object cdsRecordF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
    end
    object cdsRecordF_SESSION_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_SESSION_COMMENT'
      Size = 250
    end
    object cdsRecordF_SESSION_START_HOUR: TStringField
      DisplayLabel = 'Start Uur'
      FieldName = 'F_SESSION_START_HOUR'
      Size = 50
    end
    object cdsRecordF_ENROL_ID: TIntegerField
      DisplayLabel = 'Inschrijving ID'
      FieldName = 'F_ENROL_ID'
    end
    object cdsRecordF_ENROL_EVALUATION: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_ENROL_EVALUATION'
    end
    object cdsRecordF_ENROL_CERTIFICATE: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_ENROL_CERTIFICATE'
    end
    object cdsRecordF_ENROL_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_ENROL_COMMENT'
      Size = 250
    end
    object cdsRecordF_ENROL_INVOICED: TBooleanField
      DisplayLabel = 'Gefaktureerd'
      FieldName = 'F_ENROL_INVOICED'
    end
    object cdsRecordF_ENROL_LAST_MINUTE: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_ENROL_LAST_MINUTE'
    end
    object cdsRecordF_ENROL_HOURS: TIntegerField
      DisplayLabel = 'Uren'
      FieldName = 'F_ENROL_HOURS'
    end
    object cdsRecordF_SES_CREATE: TBooleanField
      DisplayLabel = 'Sessie Aanmaken'
      FieldName = 'F_SES_CREATE'
    end
    object cdsRecordF_SES_UPDATE: TBooleanField
      DisplayLabel = 'Sessie Aanpassen'
      FieldName = 'F_SES_UPDATE'
    end
    object cdsRecordF_ENR_CREATE: TBooleanField
      DisplayLabel = 'Inschrijving Aanmaken'
      FieldName = 'F_ENR_CREATE'
    end
    object cdsRecordF_ENR_UPDATE: TBooleanField
      DisplayLabel = 'Inschrijving Aanpassen'
      FieldName = 'F_ENR_UPDATE'
    end
    object cdsRecordF_FORECAST_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_FORECAST_COMMENT'
      Size = 250
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmSesWizard'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmSesWizard'
  end
  object cdsSessionWizardClear: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end>
    ProviderName = 'prvSES_WIZ_CLEAR'
    RemoteServer = ssckData
    AutoOpen = False
    Left = 816
    Top = 24
  end
  object cdsSessionWizardExecute: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end>
    ProviderName = 'prvSES_WIZ_EXECUTE'
    RemoteServer = ssckData
    AutoOpen = False
    Left = 816
    Top = 80
  end
  object cdsSessionWizardForeCast: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end>
    ProviderName = 'prvSES_WIZ_FORECAST'
    RemoteServer = ssckData
    AutoOpen = False
    Left = 816
    Top = 136
  end
  object cdsPersonen: TFVBFFCClientDataSet
    Aggregates = <>
    IndexFieldNames = 'F_LASTNAME;F_FIRSTNAME; F_ROLEGROUP_NAME'
    Params = <
      item
        DataType = ftInteger
        Name = 'OrganisationID'
        ParamType = ptInput
        Value = 0
      end>
    AutoOpen = False
    Left = 72
    Top = 240
    object cdsPersonenF_ROLE_ID: TIntegerField
      DisplayLabel = 'ID Rol'
      FieldName = 'F_ROLE_ID'
    end
    object cdsPersonenF_PERSON_ID: TIntegerField
      DisplayLabel = 'ID Persoon'
      FieldName = 'F_PERSON_ID'
    end
    object cdsPersonenF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsPersonenF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsPersonenF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ Nr.'
      FieldName = 'F_RSZ_NR'
      Size = 14
    end
    object cdsPersonenF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksregisternr.'
      FieldName = 'F_SOCSEC_NR'
      EditMask = '99\.99\.99\ 999\-99;0; '
      Size = 11
    end
    object cdsPersonenF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'ID Organisatie'
      FieldName = 'F_ORGANISATION_ID'
    end
    object cdsPersonenF_ORGANISATION_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORGANISATION_NAME'
      Size = 64
    end
    object cdsPersonenF_ROLEGROUP_ID: TIntegerField
      DisplayLabel = 'Functie'
      FieldName = 'F_ROLEGROUP_ID'
    end
    object cdsPersonenF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Beroep'
      FieldName = 'F_ROLEGROUP_NAME'
      Size = 64
    end
    object cdsPersonenF_START_DATE: TDateTimeField
      DisplayLabel = 'Begindatum'
      FieldName = 'F_START_DATE'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object cdsPersonenF_END_DATE: TDateTimeField
      DisplayLabel = 'Einddatum'
      FieldName = 'F_END_DATE'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object cdsPersonenF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  object cdsProgram: TFVBFFCClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AutoOpen = False
    Left = 160
    Top = 240
    object cdsProgramF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'ID Programma'
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsProgramF_NAME: TStringField
      DisplayLabel = 'Programma naam'
      FieldName = 'F_NAME'
      Required = True
      Size = 64
    end
    object cdsProgramF_USER_ID: TIntegerField
      DisplayLabel = 'Verantwoordelijke'
      FieldName = 'F_USER_ID'
      Visible = False
    end
    object cdsProgramF_DURATION: TSmallintField
      DisplayLabel = 'Duur'
      FieldName = 'F_DURATION'
    end
    object cdsProgramF_DURATION_DAYS: TSmallintField
      DisplayLabel = 'Duur in dagen'
      FieldName = 'F_DURATION_DAYS'
    end
    object cdsProgramF_MINIMUM: TSmallintField
      DisplayLabel = 'Minimum'
      FieldName = 'F_MINIMUM'
    end
    object cdsProgramF_MAXIMUM: TSmallintField
      DisplayLabel = 'Maximum'
      FieldName = 'F_MAXIMUM'
    end
    object cdsProgramF_SHORT_DESC: TStringField
      DisplayLabel = 'Korte omschrijving'
      FieldName = 'F_SHORT_DESC'
      Visible = False
      Size = 128
    end
    object cdsProgramF_LONG_DESC: TMemoField
      DisplayLabel = 'Lange omschrijving'
      FieldName = 'F_LONG_DESC'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_PROFESSION_ID: TIntegerField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_ID'
      Visible = False
    end
    object cdsProgramF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_ID'
      Visible = False
    end
    object cdsProgramF_EQUIPMENT: TMemoField
      DisplayLabel = 'Gereedschap'
      FieldName = 'F_EQUIPMENT'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_SECURITY_EQ: TMemoField
      DisplayLabel = 'Veiligheidsuitrusting'
      FieldName = 'F_SECURITY_EQ'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_KNOWLEDGE: TMemoField
      DisplayLabel = 'Voorkennis'
      FieldName = 'F_KNOWLEDGE'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_DIDAC_MATERIAL: TMemoField
      DisplayLabel = 'Didactische hulpmiddelen'
      FieldName = 'F_DIDAC_MATERIAL'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_RAW_MATERIAL: TMemoField
      DisplayLabel = 'Grondstoffen'
      FieldName = 'F_RAW_MATERIAL'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_INFRASTRUCTURE: TMemoField
      DisplayLabel = 'Vereiste infrastructuur'
      FieldName = 'F_INFRASTRUCTURE'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_COMMENT: TMemoField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_COMMENT'
      Visible = False
      BlobType = ftMemo
    end
    object cdsProgramF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsProgramF_END_DATE: TDateTimeField
      DisplayLabel = 'Einddatum'
      FieldName = 'F_END_DATE'
    end
    object cdsProgramF_ENROLMENT_DATE: TIntegerField
      DisplayLabel = 'Offset controle datum'
      FieldName = 'F_ENROLMENT_DATE'
      Visible = False
      MaxValue = 31
    end
    object cdsProgramF_CONTROL_DATE: TIntegerField
      DisplayLabel = 'Offset inschrijvingsdatum'
      FieldName = 'F_CONTROL_DATE'
      Visible = False
      MaxValue = 31
    end
    object cdsProgramF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object cdsProgramF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsListF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      Size = 232
    end
    object cdsListF_PROFESSION_NAME: TStringField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_NAME'
      Size = 64
    end
  end
  object cdsSessie: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    AutoOpen = False
    Left = 232
    Top = 240
    object cdsSessieF_SESSION_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_SESSION_NAME'
      Required = True
      Size = 64
    end
    object cdsSessieF_SESSION_CODE: TStringField
      DisplayLabel = 'Referentie'
      FieldName = 'F_SESSION_CODE'
    end
    object cdsSessieF_SESSION_TRANSPORT_INS: TStringField
      FieldName = 'F_SESSION_TRANSPORT_INS'
      Visible = False
      Size = 50
    end
    object cdsSessieF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Startdatum'
      FieldName = 'F_SESSION_START_DATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object cdsSessieF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Einddatum'
      FieldName = 'F_SESSION_END_DATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object cdsSessieF_SESSION_COMMENT: TMemoField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_SESSION_COMMENT'
      Visible = False
      BlobType = ftMemo
    end
    object cdsSessieF_SESSION_START_HOUR: TStringField
      DisplayLabel = 'Aanvangsuur'
      FieldName = 'F_SESSION_START_HOUR'
      Required = True
      Visible = False
      Size = 50
    end
    object cdsSessieF_ENROL_HOURS: TIntegerField
      DefaultExpression = '0'
      DisplayLabel = 'Totaal aantal uren'
      FieldName = 'F_ENROL_HOURS'
      Required = True
    end
    object cdsSessieF_ENROL_LAST_MINUTE: TBooleanField
      DefaultExpression = #39'False'#39
      DisplayLabel = 'Last minute'
      FieldName = 'F_ENROL_LAST_MINUTE'
      Required = True
    end
    object cdsSessieF_ENROL_INVOICED: TBooleanField
      DefaultExpression = #39'False'#39
      DisplayLabel = 'Gefaktureerd'
      FieldName = 'F_ENROL_INVOICED'
      Required = True
    end
    object cdsSessieF_ENROL_COMMENT: TStringField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_ENROL_COMMENT'
      Visible = False
      Size = 128
    end
    object cdsSessieF_ENROL_CERTIFICATE: TBooleanField
      DefaultExpression = #39'False'#39
      DisplayLabel = 'Certificaat'
      FieldName = 'F_ENROL_CERTIFICATE'
      Required = True
    end
    object cdsSessieF_ENROL_EVALUATION: TBooleanField
      DefaultExpression = #39'False'#39
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_ENROL_EVALUATION'
      Required = True
    end
    object cdsSessieF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      Required = True
    end
    object cdsSessieF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Required = True
      Size = 64
    end
  end
  object cdsGetProgram: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = 'F_PROGRAM_ID'
        ParamType = ptInput
        Size = 4
        Value = 0
      end>
    ProviderName = 'prvSES_WIZ_GET_PROG'
    RemoteServer = ssckData
    AutoOpen = False
    Left = 816
    Top = 192
    object cdsGetProgramF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
    end
    object cdsGetProgramF_NAME: TStringField
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsGetProgramF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
    end
    object cdsGetProgramF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      Size = 232
    end
    object cdsGetProgramF_DURATION: TSmallintField
      FieldName = 'F_DURATION'
    end
    object cdsGetProgramF_DURATION_DAYS: TSmallintField
      FieldName = 'F_DURATION_DAYS'
    end
    object cdsGetProgramF_MINIMUM: TSmallintField
      FieldName = 'F_MINIMUM'
    end
    object cdsGetProgramF_MAXIMUM: TSmallintField
      FieldName = 'F_MAXIMUM'
    end
    object cdsGetProgramF_SHORT_DESC: TStringField
      FieldName = 'F_SHORT_DESC'
      Size = 128
    end
    object cdsGetProgramF_LONG_DESC: TMemoField
      FieldName = 'F_LONG_DESC'
      BlobType = ftMemo
    end
    object cdsGetProgramF_ENROLMENT_DATE: TIntegerField
      FieldName = 'F_ENROLMENT_DATE'
    end
    object cdsGetProgramF_CONTROL_DATE: TIntegerField
      FieldName = 'F_CONTROL_DATE'
    end
    object cdsGetProgramF_PROFESSION_ID: TIntegerField
      FieldName = 'F_PROFESSION_ID'
    end
    object cdsGetProgramF_PROFESSION_NL: TStringField
      FieldName = 'F_PROFESSION_NL'
      Size = 64
    end
    object cdsGetProgramF_PROFESSION_FR: TStringField
      FieldName = 'F_PROFESSION_FR'
      Size = 64
    end
    object cdsGetProgramF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      Size = 64
    end
    object cdsGetProgramF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
    end
    object cdsGetProgramF_LANGUAGE_NL: TStringField
      FieldName = 'F_LANGUAGE_NL'
      Size = 64
    end
    object cdsGetProgramF_LANGUAGE_FR: TStringField
      FieldName = 'F_LANGUAGE_FR'
      Size = 64
    end
    object cdsGetProgramF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsGetProgramF_EQUIPMENT: TMemoField
      FieldName = 'F_EQUIPMENT'
      BlobType = ftMemo
    end
    object cdsGetProgramF_SECURITY_EQ: TMemoField
      FieldName = 'F_SECURITY_EQ'
      BlobType = ftMemo
    end
    object cdsGetProgramF_KNOWLEDGE: TMemoField
      FieldName = 'F_KNOWLEDGE'
      BlobType = ftMemo
    end
    object cdsGetProgramF_DIDAC_MATERIAL: TMemoField
      FieldName = 'F_DIDAC_MATERIAL'
      BlobType = ftMemo
    end
    object cdsGetProgramF_RAW_MATERIAL: TMemoField
      FieldName = 'F_RAW_MATERIAL'
      BlobType = ftMemo
    end
    object cdsGetProgramF_INFRASTRUCTURE: TMemoField
      FieldName = 'F_INFRASTRUCTURE'
      BlobType = ftMemo
    end
    object cdsGetProgramF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      BlobType = ftMemo
    end
    object cdsGetProgramF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
    end
    object cdsGetProgramF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object cdsGetProgramF_EDUCATION_ID: TIntegerField
      FieldName = 'F_EDUCATION_ID'
    end
    object cdsGetProgramF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object cdsGetProgramF_PRICE_SESSION: TFloatField
      FieldName = 'F_PRICE_SESSION'
    end
    object cdsGetProgramF_PRICE_DAY: TFloatField
      FieldName = 'F_PRICE_DAY'
    end
    object cdsGetProgramF_PRICE_WORKER: TFloatField
      FieldName = 'F_PRICE_WORKER'
    end
    object cdsGetProgramF_PRICE_CLERK: TFloatField
      FieldName = 'F_PRICE_CLERK'
    end
    object cdsGetProgramF_PRICE_OTHER: TFloatField
      FieldName = 'F_PRICE_OTHER'
    end
    object cdsGetProgramF_CODE: TStringField
      FieldName = 'F_CODE'
      Size = 50
    end
  end
  object cdsSessionWizard: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <>
    ProviderName = 'prvList'
    RemoteServer = ssckData
    AfterPost = cdsSessionWizardAfterPost
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 344
    Top = 240
    object cdsSessionWizardF_SESWIZARD_ID: TIntegerField
      DisplayLabel = 'Wizard ID'
      FieldName = 'F_SESWIZARD_ID'
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'SP ID'
      FieldName = 'F_SPID'
    end
    object StringField1: TStringField
      DisplayLabel = 'Extern Nr'
      FieldName = 'F_EXTERN_NR'
    end
    object IntegerField3: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
    end
    object StringField2: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_COMPANY_NAME'
      Size = 64
    end
    object IntegerField4: TIntegerField
      DisplayLabel = 'Persoon ID'
      FieldName = 'F_PERSON_ID'
    end
    object StringField3: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object StringField4: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsSessionWizardF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
    end
    object cdsSessionWizardF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ID'
      FieldName = 'F_PROGRAM_ID'
    end
    object StringField5: TStringField
      DisplayLabel = 'Programma Naam'
      FieldName = 'F_PROGRAM_NAME'
      Size = 64
    end
    object StringField6: TStringField
      DisplayLabel = 'Sessie Naam'
      FieldName = 'F_SESSION_NAME'
      Size = 64
    end
    object StringField7: TStringField
      DisplayLabel = 'Sessie Code'
      FieldName = 'F_SESSION_CODE'
    end
    object StringField8: TStringField
      DisplayLabel = 'Transport'
      FieldName = 'F_SESSION_TRANSPORT_INS'
      Size = 50
    end
    object DateTimeField1: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_SESSION_START_DATE'
    end
    object DateTimeField2: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
    end
    object StringField9: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_SESSION_COMMENT'
      Size = 250
    end
    object StringField10: TStringField
      DisplayLabel = 'Start Uur'
      FieldName = 'F_SESSION_START_HOUR'
      Size = 50
    end
    object cdsSessionWizardF_ENROL_ID: TIntegerField
      DisplayLabel = 'Inschrijving ID'
      FieldName = 'F_ENROL_ID'
    end
    object cdsSessionWizardF_ENROL_EVALUATION: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_ENROL_EVALUATION'
    end
    object cdsSessionWizardF_ENROL_CERTIFICATE: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_ENROL_CERTIFICATE'
    end
    object cdsSessionWizardF_ENROL_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_ENROL_COMMENT'
      Size = 250
    end
    object cdsSessionWizardF_ENROL_INVOICED: TBooleanField
      DisplayLabel = 'Gefaktureerd'
      FieldName = 'F_ENROL_INVOICED'
    end
    object cdsSessionWizardF_ENROL_LAST_MINUTE: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_ENROL_LAST_MINUTE'
    end
    object cdsSessionWizardF_ENROL_HOURS: TIntegerField
      DisplayLabel = 'Uren'
      FieldName = 'F_ENROL_HOURS'
    end
    object cdsSessionWizardF_SES_CREATE: TBooleanField
      DisplayLabel = 'Sessie Aanmaken'
      FieldName = 'F_SES_CREATE'
      OnValidate = ValidateCreateAndUpdate
    end
    object cdsSessionWizardF_SES_UPDATE: TBooleanField
      DisplayLabel = 'Sessie Aanpassen'
      FieldName = 'F_SES_UPDATE'
      OnValidate = ValidateCreateAndUpdate
    end
    object cdsSessionWizardF_ENR_CREATE: TBooleanField
      DisplayLabel = 'Inschrijving Aanmaken'
      FieldName = 'F_ENR_CREATE'
      OnValidate = ValidateCreateAndUpdate
    end
    object cdsSessionWizardF_ENR_UPDATE: TBooleanField
      DisplayLabel = 'Inschrijving Aanpassen'
      FieldName = 'F_ENR_UPDATE'
      OnValidate = ValidateCreateAndUpdate
    end
    object cdsSessionWizardF_FORECAST_COMMENT: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_FORECAST_COMMENT'
      Size = 250
    end
    object cdsSessionWizardF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
    end
    object cdsSessionWizardF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Size = 64
    end
  end
  object cdsSessionWizardResult: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <>
    ProviderName = 'prvList'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 456
    Top = 240
    object IntegerField17: TIntegerField
      DisplayLabel = 'Wizard ID'
      FieldName = 'F_SESWIZARD_ID'
    end
    object IntegerField18: TIntegerField
      DisplayLabel = 'SP ID'
      FieldName = 'F_SPID'
    end
    object StringField25: TStringField
      DisplayLabel = 'Extern Nr'
      FieldName = 'F_EXTERN_NR'
    end
    object IntegerField19: TIntegerField
      DisplayLabel = 'Organisatie ID'
      FieldName = 'F_ORGANISATION_ID'
    end
    object StringField26: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_COMPANY_NAME'
      Size = 64
    end
    object IntegerField20: TIntegerField
      DisplayLabel = 'Persoon ID'
      FieldName = 'F_PERSON_ID'
    end
    object StringField27: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object StringField28: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object IntegerField21: TIntegerField
      DisplayLabel = 'Sessie ID'
      FieldName = 'F_SESSION_ID'
    end
    object IntegerField22: TIntegerField
      DisplayLabel = 'Programma ID'
      FieldName = 'F_PROGRAM_ID'
    end
    object StringField29: TStringField
      DisplayLabel = 'Programma Naam'
      FieldName = 'F_PROGRAM_NAME'
      Size = 64
    end
    object StringField30: TStringField
      DisplayLabel = 'Sessie Naam'
      FieldName = 'F_SESSION_NAME'
      Size = 64
    end
    object StringField31: TStringField
      DisplayLabel = 'Sessie Code'
      FieldName = 'F_SESSION_CODE'
    end
    object StringField32: TStringField
      DisplayLabel = 'Transport'
      FieldName = 'F_SESSION_TRANSPORT_INS'
      Size = 50
    end
    object DateTimeField5: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_SESSION_START_DATE'
    end
    object DateTimeField6: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
    end
    object StringField33: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_SESSION_COMMENT'
      Size = 250
    end
    object StringField34: TStringField
      DisplayLabel = 'Start Uur'
      FieldName = 'F_SESSION_START_HOUR'
      Size = 50
    end
    object IntegerField23: TIntegerField
      DisplayLabel = 'Inschrijving ID'
      FieldName = 'F_ENROL_ID'
    end
    object BooleanField17: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_ENROL_EVALUATION'
    end
    object BooleanField18: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_ENROL_CERTIFICATE'
    end
    object StringField35: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_ENROL_COMMENT'
      Size = 250
    end
    object BooleanField19: TBooleanField
      DisplayLabel = 'Gefaktureerd'
      FieldName = 'F_ENROL_INVOICED'
    end
    object BooleanField20: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_ENROL_LAST_MINUTE'
    end
    object IntegerField24: TIntegerField
      DisplayLabel = 'Uren'
      FieldName = 'F_ENROL_HOURS'
    end
    object BooleanField21: TBooleanField
      DisplayLabel = 'Sessie Aanmaken'
      FieldName = 'F_SES_CREATE'
    end
    object BooleanField22: TBooleanField
      DisplayLabel = 'Sessie Aanpassen'
      FieldName = 'F_SES_UPDATE'
    end
    object BooleanField23: TBooleanField
      DisplayLabel = 'Inschrijving Aanmaken'
      FieldName = 'F_ENR_CREATE'
    end
    object BooleanField24: TBooleanField
      DisplayLabel = 'Inschrijving Aanpassen'
      FieldName = 'F_ENR_UPDATE'
    end
    object StringField36: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_FORECAST_COMMENT'
      Size = 250
    end
  end
end
