inherited dtmRoleGroup: TdtmRoleGroup
  KeyFields = 'F_ROLEGROUP_ID'
  ListViewClass = 'TfrmRoleGroup_List'
  RecordViewClass = 'TfrmRoleGroup_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROLEGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ROLEGROUP_NAME '
      'FROM '
      '  V_RO_GROUP')
    object qryListF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROLEGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ROLEGROUP_NAME '
      'FROM '
      '  V_RO_GROUP')
    object qryRecordF_ROLEGROUP_ID: TIntegerField
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_ROLEGROUP_NAME: TStringField
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_ROLEGROUP_ID: TIntegerField
      DisplayLabel = 'Functie ( ID )'
      FieldName = 'F_ROLEGROUP_ID'
      Required = True
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Functie ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Functie ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Functie'
      FieldName = 'F_ROLEGROUP_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ROLEGROUP_ID: TIntegerField
      DisplayLabel = 'Functie ( ID )'
      FieldName = 'F_ROLEGROUP_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Functie ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Functie ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Functie'
      FieldName = 'F_ROLEGROUP_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmRoleGroup'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmRoleGroup'
  end
end
