{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_SesWizard
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/08/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_SesWizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmSesWizard = class(TEDUDataModule, IEDUSesWizardDataModule)
    cdsListF_SESWIZARD_ID: TIntegerField;
    cdsListF_SPID: TIntegerField;
    cdsListF_EXTERN_NR: TStringField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_COMPANY_NAME: TStringField;
    cdsListF_PERSON_ID: TIntegerField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_PROGRAM_ID: TIntegerField;
    cdsListF_PROGRAM_NAME: TStringField;
    cdsListF_SESSION_NAME: TStringField;
    cdsListF_SESSION_CODE: TStringField;
    cdsListF_SESSION_TRANSPORT_INS: TStringField;
    cdsListF_SESSION_START_DATE: TDateTimeField;
    cdsListF_SESSION_END_DATE: TDateTimeField;
    cdsListF_SESSION_COMMENT: TStringField;
    cdsListF_SESSION_START_HOUR: TStringField;
    cdsListF_ENROL_ID: TIntegerField;
    cdsListF_ENROL_EVALUATION: TBooleanField;
    cdsListF_ENROL_CERTIFICATE: TBooleanField;
    cdsListF_ENROL_COMMENT: TStringField;
    cdsListF_ENROL_INVOICED: TBooleanField;
    cdsListF_ENROL_LAST_MINUTE: TBooleanField;
    cdsListF_ENROL_HOURS: TIntegerField;
    cdsListF_SES_CREATE: TBooleanField;
    cdsListF_SES_UPDATE: TBooleanField;
    cdsListF_ENR_CREATE: TBooleanField;
    cdsListF_ENR_UPDATE: TBooleanField;
    cdsListF_FORECAST_COMMENT: TStringField;
    cdsRecordF_SESWIZARD_ID: TIntegerField;
    cdsRecordF_SPID: TIntegerField;
    cdsRecordF_EXTERN_NR: TStringField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsRecordF_COMPANY_NAME: TStringField;
    cdsRecordF_PERSON_ID: TIntegerField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_PROGRAM_ID: TIntegerField;
    cdsRecordF_PROGRAM_NAME: TStringField;
    cdsRecordF_SESSION_NAME: TStringField;
    cdsRecordF_SESSION_CODE: TStringField;
    cdsRecordF_SESSION_TRANSPORT_INS: TStringField;
    cdsRecordF_SESSION_START_DATE: TDateTimeField;
    cdsRecordF_SESSION_END_DATE: TDateTimeField;
    cdsRecordF_SESSION_COMMENT: TStringField;
    cdsRecordF_SESSION_START_HOUR: TStringField;
    cdsRecordF_ENROL_ID: TIntegerField;
    cdsRecordF_ENROL_EVALUATION: TBooleanField;
    cdsRecordF_ENROL_CERTIFICATE: TBooleanField;
    cdsRecordF_ENROL_COMMENT: TStringField;
    cdsRecordF_ENROL_INVOICED: TBooleanField;
    cdsRecordF_ENROL_LAST_MINUTE: TBooleanField;
    cdsRecordF_ENROL_HOURS: TIntegerField;
    cdsRecordF_SES_CREATE: TBooleanField;
    cdsRecordF_SES_UPDATE: TBooleanField;
    cdsRecordF_ENR_CREATE: TBooleanField;
    cdsRecordF_ENR_UPDATE: TBooleanField;
    cdsRecordF_FORECAST_COMMENT: TStringField;
    cdsSessionWizardClear: TFVBFFCClientDataSet;
    cdsSessionWizardExecute: TFVBFFCClientDataSet;
    cdsSessionWizardForeCast: TFVBFFCClientDataSet;
    cdsPersonen: TFVBFFCClientDataSet;
    cdsPersonenF_ROLE_ID: TIntegerField;
    cdsPersonenF_PERSON_ID: TIntegerField;
    cdsPersonenF_LASTNAME: TStringField;
    cdsPersonenF_FIRSTNAME: TStringField;
    cdsPersonenF_RSZ_NR: TStringField;
    cdsPersonenF_SOCSEC_NR: TStringField;
    cdsPersonenF_ORGANISATION_ID: TIntegerField;
    cdsPersonenF_ORGANISATION_NAME: TStringField;
    cdsPersonenF_ROLEGROUP_ID: TIntegerField;
    cdsPersonenF_ROLEGROUP_NAME: TStringField;
    cdsPersonenF_START_DATE: TDateTimeField;
    cdsPersonenF_END_DATE: TDateTimeField;
    cdsPersonenF_ACTIVE: TBooleanField;
    cdsProgram: TFVBFFCClientDataSet;
    cdsProgramF_PROGRAM_ID: TIntegerField;
    cdsProgramF_NAME: TStringField;
    cdsProgramF_USER_ID: TIntegerField;
    cdsProgramF_DURATION: TSmallintField;
    cdsProgramF_DURATION_DAYS: TSmallintField;
    cdsProgramF_MINIMUM: TSmallintField;
    cdsProgramF_MAXIMUM: TSmallintField;
    cdsProgramF_SHORT_DESC: TStringField;
    cdsProgramF_LONG_DESC: TMemoField;
    cdsProgramF_PROFESSION_ID: TIntegerField;
    cdsProgramF_LANGUAGE_ID: TIntegerField;
    cdsProgramF_EQUIPMENT: TMemoField;
    cdsProgramF_SECURITY_EQ: TMemoField;
    cdsProgramF_KNOWLEDGE: TMemoField;
    cdsProgramF_DIDAC_MATERIAL: TMemoField;
    cdsProgramF_RAW_MATERIAL: TMemoField;
    cdsProgramF_INFRASTRUCTURE: TMemoField;
    cdsProgramF_COMMENT: TMemoField;
    cdsProgramF_ACTIVE: TBooleanField;
    cdsProgramF_END_DATE: TDateTimeField;
    cdsProgramF_ENROLMENT_DATE: TIntegerField;
    cdsProgramF_CONTROL_DATE: TIntegerField;
    cdsSessie: TFVBFFCClientDataSet;
    cdsSessieF_SESSION_NAME: TStringField;
    cdsSessieF_SESSION_CODE: TStringField;
    cdsSessieF_SESSION_TRANSPORT_INS: TStringField;
    cdsSessieF_SESSION_START_DATE: TDateTimeField;
    cdsSessieF_SESSION_END_DATE: TDateTimeField;
    cdsSessieF_SESSION_COMMENT: TMemoField;
    cdsSessieF_SESSION_START_HOUR: TStringField;
    cdsSessieF_ENROL_HOURS: TIntegerField;
    cdsSessieF_ENROL_LAST_MINUTE: TBooleanField;
    cdsSessieF_ENROL_INVOICED: TBooleanField;
    cdsSessieF_ENROL_COMMENT: TStringField;
    cdsSessieF_ENROL_CERTIFICATE: TBooleanField;
    cdsSessieF_ENROL_EVALUATION: TBooleanField;
    cdsSessieF_INFRASTRUCTURE_ID: TIntegerField;
    cdsSessieF_INFRASTRUCTURE_NAME: TStringField;
    cdsGetProgram: TFVBFFCClientDataSet;
    cdsGetProgramF_PROGRAM_ID: TIntegerField;
    cdsGetProgramF_NAME: TStringField;
    cdsGetProgramF_USER_ID: TIntegerField;
    cdsGetProgramF_COMPLETE_USERNAME: TStringField;
    cdsGetProgramF_DURATION: TSmallintField;
    cdsGetProgramF_DURATION_DAYS: TSmallintField;
    cdsGetProgramF_MINIMUM: TSmallintField;
    cdsGetProgramF_MAXIMUM: TSmallintField;
    cdsGetProgramF_SHORT_DESC: TStringField;
    cdsGetProgramF_LONG_DESC: TMemoField;
    cdsGetProgramF_ENROLMENT_DATE: TIntegerField;
    cdsGetProgramF_CONTROL_DATE: TIntegerField;
    cdsGetProgramF_PROFESSION_ID: TIntegerField;
    cdsGetProgramF_PROFESSION_NL: TStringField;
    cdsGetProgramF_PROFESSION_FR: TStringField;
    cdsGetProgramF_PROFESSION_NAME: TStringField;
    cdsGetProgramF_LANGUAGE_ID: TIntegerField;
    cdsGetProgramF_LANGUAGE_NL: TStringField;
    cdsGetProgramF_LANGUAGE_FR: TStringField;
    cdsGetProgramF_LANGUAGE_NAME: TStringField;
    cdsGetProgramF_EQUIPMENT: TMemoField;
    cdsGetProgramF_SECURITY_EQ: TMemoField;
    cdsGetProgramF_KNOWLEDGE: TMemoField;
    cdsGetProgramF_DIDAC_MATERIAL: TMemoField;
    cdsGetProgramF_RAW_MATERIAL: TMemoField;
    cdsGetProgramF_INFRASTRUCTURE: TMemoField;
    cdsGetProgramF_COMMENT: TMemoField;
    cdsGetProgramF_ACTIVE: TBooleanField;
    cdsGetProgramF_END_DATE: TDateTimeField;
    cdsGetProgramF_EDUCATION_ID: TIntegerField;
    cdsGetProgramF_PRICING_SCHEME: TIntegerField;
    cdsGetProgramF_PRICE_SESSION: TFloatField;
    cdsGetProgramF_PRICE_DAY: TFloatField;
    cdsGetProgramF_PRICE_WORKER: TFloatField;
    cdsGetProgramF_PRICE_CLERK: TFloatField;
    cdsGetProgramF_PRICE_OTHER: TFloatField;
    cdsGetProgramF_CODE: TStringField;
    cdsSessionWizard: TFVBFFCClientDataSet;
    cdsSessionWizardF_SESWIZARD_ID: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    IntegerField4: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    cdsSessionWizardF_SESSION_ID: TIntegerField;
    cdsSessionWizardF_PROGRAM_ID: TIntegerField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    StringField9: TStringField;
    StringField10: TStringField;
    cdsSessionWizardF_ENROL_ID: TIntegerField;
    cdsSessionWizardF_ENROL_EVALUATION: TBooleanField;
    cdsSessionWizardF_ENROL_CERTIFICATE: TBooleanField;
    cdsSessionWizardF_ENROL_COMMENT: TStringField;
    cdsSessionWizardF_ENROL_INVOICED: TBooleanField;
    cdsSessionWizardF_ENROL_LAST_MINUTE: TBooleanField;
    cdsSessionWizardF_ENROL_HOURS: TIntegerField;
    cdsSessionWizardF_SES_CREATE: TBooleanField;
    cdsSessionWizardF_SES_UPDATE: TBooleanField;
    cdsSessionWizardF_ENR_CREATE: TBooleanField;
    cdsSessionWizardF_ENR_UPDATE: TBooleanField;
    cdsSessionWizardF_FORECAST_COMMENT: TStringField;
    cdsSessionWizardResult: TFVBFFCClientDataSet;
    IntegerField17: TIntegerField;
    IntegerField18: TIntegerField;
    StringField25: TStringField;
    IntegerField19: TIntegerField;
    StringField26: TStringField;
    IntegerField20: TIntegerField;
    StringField27: TStringField;
    StringField28: TStringField;
    IntegerField21: TIntegerField;
    IntegerField22: TIntegerField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    DateTimeField5: TDateTimeField;
    DateTimeField6: TDateTimeField;
    StringField33: TStringField;
    StringField34: TStringField;
    IntegerField23: TIntegerField;
    BooleanField17: TBooleanField;
    BooleanField18: TBooleanField;
    StringField35: TStringField;
    BooleanField19: TBooleanField;
    BooleanField20: TBooleanField;
    IntegerField24: TIntegerField;
    BooleanField21: TBooleanField;
    BooleanField22: TBooleanField;
    BooleanField23: TBooleanField;
    BooleanField24: TBooleanField;
    StringField36: TStringField;
    cdsProgramF_LANGUAGE_NAME: TStringField;
    cdsProgramF_PRICING_SCHEME: TIntegerField;
    cdsListF_COMPLETE_USERNAME: TStringField;
    cdsListF_PROFESSION_NAME: TStringField;
    cdsSessionWizardF_INFRASTRUCTURE_ID: TIntegerField;
    cdsSessionWizardF_INFRASTRUCTURE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleCreate(Sender: TObject);
    procedure cdsSessionWizardAfterPost(DataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure ValidateCreateAndUpdate(Sender: TField);

  private
    { Private declarations }
    FDataChanged: Boolean;
    FJumpToPage: Integer;
    FCanDoExecute: Boolean;
    FExecuteDone: Boolean;
    FForeCastDone: Boolean;
    function GetDataChanged: Boolean;
    procedure SetDataChanged(const Value: Boolean);
    function GetExecuteDone: Boolean;
    function GetForeCastDone: Boolean;

  protected
    { Protected declarations }
    { IEDUSesWizardDataModule }
    function GetSelectedRolesDataSet : TClientDataSet; virtual;
    function GetSessionDataSet       : TClientDataSet; virtual;
    function GetProgramDataSet       : TClientDataSet; virtual;
    function GetWizardDataSet        : TClientDataSet; virtual;
    function GetWizardResultDataSet  : TClientDataSet; virtual;

    function GetCanDoForeCast        : Boolean; virtual;
    function GetCanDoExecute         : Boolean; virtual;

    procedure CreatePersonAndRole( aDataSet : TDataSet ); virtual;
    procedure ForeCast; virtual;
    procedure Execute; virtual;
    procedure RemoveAllRoles     ( aDataSet : TDataSet ); virtual;
    procedure RestoreWizardData; virtual;
    procedure SaveWizardData; virtual;

    procedure SelectInfrastructure ( aDataSet : TDataSet ); virtual;
    procedure SelectProgram      ( aDataSet : TDataSet ); virtual;
    procedure SelectRole         ( aDataSet : TDataSet ); virtual;

    procedure ShowInfrastructure   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowProgram        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowPerson         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowRole           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

    function GetCanUpdateEnrolment: Boolean; virtual;
    function GetCanUpdateSession: Boolean; virtual;
    function GetWizardDataDir: string; virtual;

    { Other Methods }
    procedure CopyFieldValuesToDataSet( aSource, aDest : TDataSet; aFieldList : TStringList ); virtual;

    property DataChanged          : Boolean        read GetDataChanged write SetDataChanged;
    property SelectedRolesDataSet : TClientDataSet read GetSelectedRolesDataSet;
    property SessionDataSet       : TClientDataSet read GetSessionDataSet;
    property ProgramDataSet       : TClientDataSet read GetProgramDataSet;
    property WizardDataSet        : TClientDataSet read GetWizardDataSet;
    property WizardResultDataSet  : TClientDataSet read GetWizardResultDataSet;

  public
    { Public declarations }
    class function OpenWizardForSession( aDataSet : TDataSet; Restore : Boolean = False ) : TModalResult;
    class function OpenWizard          ( aDataSet : TDataSet; Restore : Boolean = False ) : TModalResult;

    { Wizard Save and Restore methods }
    function ShouldRestoreWizardData : Boolean;
    procedure ClearSavedWizardFiles;

    procedure SetDefaultFieldValuesForSession;

    { Properties }
    property CanDoForeCast      : Boolean read GetCanDoForeCast;
    property CanDoExecute       : Boolean read GetCanDoExecute;
    property CanUpdateSession   : Boolean read GetCanUpdateSession;
    property CanUpdateEnrolment : Boolean read GetCanUpdateEnrolment;

    property JumpToPage    : Integer read FJumpToPage;
    property WizardDataDir : string  read GetWizardDataDir;

    property ForeCastDone : Boolean read GetForeCastDone;
    property ExecuteDone  : Boolean read GetExecuteDone;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite, csDataSetRecord,
  {$ENDIF}
  Data_EduMainClient, Unit_PPWFrameWorkInterfaces, Data_Role, Data_Person,
  Data_ProgProgram, Data_CenInfrastructure, Unit_PPWFrameWorkUtils;

resourcestring
  rsRestoreWizardData =
    'Wilt u de bewaarde data van de Sessie Wizard opnieuw inlezen ?';

procedure TdtmSesWizard.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_SYS_SESWIZARD';
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmSesWizard.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be Initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_SESWIZARD_ID' ).AsInteger :=
    ssckData.AppServer.GetNewRecordID
end;

procedure TdtmSesWizard.FVBFFCDataModuleCreate(Sender: TObject);
begin
  inherited;
  cdsSessionWizardClear.Execute;
  WizardDataSet.Open;
  SelectedRolesDataSet.CreateDataSet;
  ProgramDataSet.CreateDataSet;
  SessionDataSet.CreateDataSet;

  SetDefaultFieldValuesForSession;
end;

{*****************************************************************************
  This method will be used to clear all the Saved Wizard Data Files.

  @Name       TdtmSesWizard.ClearSavedWizardFiles
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.ClearSavedWizardFiles;
begin
  if ( DirectoryExists( WizardDataDir ) ) then
  begin
    DeleteFile( WizardDataDir + 'cdsPersonen.cds' );
    DeleteFile( WizardDataDir + 'cdsProgram.cds' );
    DeleteFile( WizardDataDir + 'cdsSession.cds' );
    RemoveDir( WizardDataDir );
  end;
end;

{*****************************************************************************
  This method will be executed when the Wizard is finished and it can be
  Executed.

  @Name       TdtmSesWizard.Execute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.Execute;
var
  CursorRestorer : IPPWRestorer;
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'Execute' );
  {$ENDIF}

  { Change the cursor to SQLWait and store it so we can restore the cursor
    later. }
  CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  if ( WizardDataSet.State in dsEditModes ) then
  begin
    WizardDataSet.Post;
  end;

  // First execute to make a new appointments.
  cdsSessionWizardExecute.Execute;

  WizardResultDataSet.Close;
  WizardResultDataSet.Open;

  FCanDoExecute := False;
  DataChanged  := True;
  FExecuteDone := True;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'Execute' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to generate the ForeCast that is needed before
  completing the Wizard.

  @Name       TdtmSesWizard.ForeCast
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.ForeCast;
var
  CursorRestorer : IPPWRestorer;
begin
  { Change the cursor to SQLWait and store it so we can restore the cursor
    later. }
  CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  { First Make sure all data is Posted in all the DataSets }
  if ( SessionDataSet.State in dsEditModes ) then
  begin
    SessionDataSet.Post;
  end;

  { Close all SessionWizard related DataSets if they were already Open }
  WizardDataSet.Close;
  cdsSessionWizardForeCast.Close;
  WizardResultDataSet.Close;
//  cdsReport.Close;

  { Make sure all records in T_SYS_SESWIZARD for the current SPID are
    cleared.  This can be done by Executing the SessionWizardClear, which
    will call the SP_SYS_SESWIZARD_EMPTY stored procedure }
  cdsSessionWizardClear.Execute;

  { Now open our SessionWizard dataset so we can add the new records to it }
  WizardDataSet.Open;

  { For each Person / Company we should add a record in T_SYS_SESWIZARD }
  SelectedRolesDataSet.DisableControls;
  WizardDataSet.DisableControls;

  try
    SelectedRolesDataSet.First;
    while not SelectedRolesDataSet.Eof do
    begin
      WizardDataSet.Append;

      CopyPersonFieldValues( SelectedRolesDataSet, WizardDataSet,
                             True, critPersonToSesWizardExecute );
//      CopyPersonData( cdsPersonen, cdsSessionWizard );
      CopyFieldValues( ProgramDataSet, WizardDataSet );
      WizardDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger :=
        SelectedRolesDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger;
      WizardDataSet.FieldByName( 'F_COMPANY_NAME' ).AsString :=
        SelectedRolesDataSet.FieldByName( 'F_ORGANISATION_NAME' ).AsString;
      WizardDataSet.FieldByName( 'F_PROGRAM_NAME' ).AsString :=
        ProgramDataSet.FieldByName( 'F_NAME' ).AsString;
      CopyFieldValues( SessionDataSet, WizardDataSet );

      if ( WizardDataSet.FieldByName( 'F_SESSION_NAME' ).IsNull ) then
      begin
        WizardDataSet.FieldByName( 'F_SESSION_NAME' ).AsString :=
        WizardDataSet.FieldByName( 'F_PROGRAM_NAME' ).AsString;
      end;

      WizardDataSet.Post;
      SelectedRolesDataSet.Next;
    end;
  finally
    WizardDataSet.EnableControls;
    SelectedRolesDataSet.EnableControls;
  end;

  { And now we must execute the Stored Procedure which will do the actual
    ForeCast }
  cdsSessionWizardForeCast.Execute;

  { Refresh the Client DataSet }
  WizardDataSet.Refresh;

  FCanDoExecute := True;
  DataChanged  := False;
  FForeCastDone := True;
  FExecuteDone := False;
end;

{*****************************************************************************
  This method will be used to determine if the user can Execute the Wizard.


  @Name       TdtmSesWizard.GetCanDoExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesWizard.GetCanDoExecute: Boolean;
begin
  Result := ( SessionDataSet.State in [ dsBrowse ] ) and not DataChanged;
end;

{*****************************************************************************
  This method will be used to check if the user can do the ForeCast of the
  Wizard.

  @Name       TdtmSesWizard.GetCanDoForeCast
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesWizard.GetCanDoForeCast: Boolean;
begin
  Result := Not ( ( SelectedRolesDataSet.RecordCount = 0 ) or
                  ( ProgramDataSet.FieldByName( 'F_PROGRAM_ID' ).IsNull ) or
                  ( ProgramDataSet.FieldByName( 'F_DURATION' ).Value = 0 ) or
                  ( ProgramDataSet.FieldByName( 'F_DURATION_DAYS' ).Value = 0 ) or
                  ( SessionDataSet.FieldByName( 'F_INFRASTRUCTURE_ID' ).IsNull ) or
                  ( SessionDataSet.FieldByName( 'F_SESSION_END_DATE' ).IsNull ) or
                  ( SessionDataSet.FieldByName( 'F_SESSION_START_HOUR' ).IsNull ) or
                  ( SessionDataSet.FieldByName( 'F_ENROL_HOURS' ).IsNull ) );
end;

{*****************************************************************************
  This function will return a boolean indicating if the Enrolment can be
  updated.  This is used in the ForeCast result Grid.

  @Name       TdtmSesWizard.GetCanUpdateEnrolment
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesWizard.GetCanUpdateEnrolment: Boolean;
begin
  Result := WizardDataSet.Active and
            Not ( WizardDataSet.FieldByName( 'F_ENROL_ID' ).IsNull ) and
            Not ( WizardDataSet.FieldByName( 'F_SES_CREATE' ).AsBoolean );
end;

{*****************************************************************************
  This function will return a boolean indicating if the Session can be updated.
  This is used in the ForeCast result Grid.

  @Name       TdtmSesWizard.GetCanUpdateSession
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesWizard.GetCanUpdateSession: Boolean;
begin
  Result := WizardDataSet.Active and
            Not( WizardDataSet.FieldByName( 'F_SESSION_ID' ).IsNull );
end;

{*****************************************************************************
  This function will return the Directory in which the Wizard will store the
  ClientDataSets for the Save / Restore functionality

  @Name       TdtmSesWizard.GetWizardDataDir
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesWizard.GetWizardDataDir: string;
begin
  Result := ExtractFilePath( Application.ExeName ) + 'WizardData\' ;
end;

{*****************************************************************************
  This method will be used to Restore the data contained in the Wizard from
  some ClientDataSets.  Possibly the saving stored some required fields
  with some dummy values.  If that was the case, we will clear those again.

  @Name       TdtmSesWizard.RestoreWizardData
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.RestoreWizardData;
begin
  if ( DirectoryExists( WizardDataDir ) ) then
  begin
    SelectedRolesDataSet.LoadFromFile( WizardDataDir + 'cdsPersonen.cds' );
    ProgramDataSet.LoadFromFile( WizardDataDir + 'cdsProgram.cds' );
    SessionDataSet.LoadFromFile( WizardDataDir + 'cdsSession.cds' );
  end;

  DataChanged := True;
end;

{*****************************************************************************
  This method will be used to save the data contained in the Wizard to some
  ClientDataSets for later use.  We had to fill in some required fields with
  some dummy values prior to saving, since otherwise we couldn't use the
  default .SaveToFile method.

  @Name       TdtmSesWizard.SaveWizardData
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.SaveWizardData;
begin
  if ( Not( DirectoryExists( WizardDataDir ) ) ) then
  begin
    CreateDir( WizardDataDir );
  end;

  SelectedRolesDataSet.SaveToFile( WizardDataDir + 'cdsPersonen.cds' );
  ProgramDataSet.SaveToFile( WizardDataDir + 'cdsProgram.cds' );
  SessionDataSet.FieldByName( 'F_SESSION_END_DATE' ).Required := False;
  SessionDataSet.SaveToFile( WizardDataDir + 'cdsSession.cds' );
  SessionDataSet.FieldByName( 'F_SESSION_END_DATE' ).Required := True;
end;

{*****************************************************************************
  This method will be used to fill in the Session / Enrollment dataset with
  some default values.

  @Name       TdtmSesWizard.SetDefaultFieldValuesForSession
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.SetDefaultFieldValuesForSession;
begin
  if SessionDataSet.State in [ dsBrowse ] then SessionDataSet.Edit;

  with cdsSessie do
  begin
    FieldByName( 'F_SESSION_START_DATE' ).AsDateTime := Date;
    FieldByName( 'F_ENROL_HOURS' ).AsInteger := 0;
    FieldByName( 'F_ENROL_LAST_MINUTE' ).AsBoolean := False;
    FieldByName( 'F_ENROL_INVOICED' ).AsBoolean := False;
    FieldByName( 'F_ENROL_CERTIFICATE' ).AsBoolean := False;
    FieldByName( 'F_ENROL_EVALUATION' ).AsBoolean := False;
  end;
end;

{*****************************************************************************
  This function will be used to ask the user if the Saved data should be
  restored or not.  If the user answers yes, then the Saved data will be
  resotred and the result will be true, otherwise the result will be false;

  @Name       TdtmSesWizard.ShouldRestoreWizardData
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmSesWizard.ShouldRestoreWizardData: Boolean;
begin
  Result := False;

  { If the directory exists then we know that data was saved,
    and we should ask the user if they want to restore the
    saved data. }
  if ( DirectoryExists( WizardDataDir ) ) then
  begin
    if ( MessageDlg( rsRestoreWizardData, mtConfirmation, [ mbYes, mbNo ], 0 ) = mrYes ) then
    begin
      RestoreWizardData;
      Result := True;
    end
    else
    begin
      ClearSavedWizardFiles;
    end;
  end;
end;

procedure TdtmSesWizard.CopyFieldValuesToDataSet(aSource, aDest: TDataSet;
  aFieldList: TStringList);
var
  aSrcField, aDstField : TField;
  lcv                  : Integer;
  OldReadOnly          : Boolean;
begin
  if ( Assigned( aSource ) and
       Assigned( aDest ) and
       Assigned( aFieldList ) and
       ( aDest.State in dsEditModes ) and
       ( aFieldList.Count <> 0 ) ) then
  begin
    for lcv := 0 to Pred( aFieldList.Count ) do
    begin
      aSrcField := aSource.FindField( aFieldList.Strings[ lcv ] );

      if ( ( Assigned( aSrcField ) ) and
           ( aSrcField.FieldKind = fkData ) and
           ( not ( aSrcField.IsNull ) ) ) then
      begin
        aDstField := aDest.FindField( aFieldList.Strings[ lcv ] );

        if ( Assigned( aDstField ) ) then
        begin
          if ( aSrcField.Value <> aDstField.Value ) then
          begin
            OldReadOnly := aDstField.ReadOnly;
            aDstField.ReadOnly := False;
            try
              aDstField.Value := aSrcField.Value;
            finally
              aDstField.ReadOnly := OldReadOnly;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TdtmSesWizard.SelectRole(aDataSet: TDataSet);
begin
  TdtmRole.LinkRoles( aDataSet, 'F_ACTIVE = 1', True, critRoleToSesWizard );
  FCanDoExecute := False;
  DataChanged  := True;
end;

procedure TdtmSesWizard.ShowRole(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  TdtmRole.ShowRole( aDataSet, aRecordViewMode, critRoleToSesWizard );
end;

function TdtmSesWizard.GetSelectedRolesDataSet: TClientDataSet;
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.SelectedRolesDataSet;
  end
  else
  begin
    Result := cdsPersonen;
  end;
end;

procedure TdtmSesWizard.CreatePersonAndRole(aDataSet: TDataSet);
var
  aPersonDataSet, aRoleDataSet : TClientDataSet;
begin
  aPersonDataSet := TClientDataSet.Create( Self );
  aRoleDataSet   := TClientDataSet.Create( Self );

  try
    aDataSet.Append;
    TdtmPerson.ShowPerson( aDataSet, rvmAdd, critPersonToSesWizard );
    if ( not aDataSet.FieldByName( 'F_PERSON_ID' ).IsNull ) then
    begin
      TdtmRole.CreateRoleForPerson( aDataSet, aDataSet, rvmAdd, critRoleToSesWizard );
    end;
    FCanDoExecute := False;
    DataChanged  := True;
  finally
    FreeAndNil( aPersonDataSet );
    FreeAndNil( aRoleDataSet );
  end;
end;

procedure TdtmSesWizard.ShowPerson(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  TdtmPerson.ShowPerson( aDataSet, aRecordViewMode, critPersonToSesWizard );
end;

procedure TdtmSesWizard.RemoveAllRoles(aDataSet: TDataSet);
begin
  SelectedRolesDataSet.Close;
  SelectedRolesDataSet.CreateDataSet;
end;

procedure TdtmSesWizard.SelectProgram(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) ) then
  begin
    aCopyTo := critProgramToSesWizard;
    aIDField := aDataSet.FindField( 'F_PROGRAM_ID' );
    aWhere   := '';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Language isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_PROGRAM_ID <> ' + aIDField.AsString;
    end;

    TdtmProgProgram.SelectProgram( aDataSet, aWhere, False, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Program.

  @Name       TdtmSesWizard.ShowProgram
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.ShowProgram(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) ) then
  begin
    aCopyTo := critProgramToSesWizard;
    aIDField := aDataSet.FindField( 'F_PROGRAM_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmProgProgram.ShowProgram( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Infrastructure Records.

  @Name       TdtmSesWizard.SelectInfrastructure
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.SelectInfrastructure(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aCopyTo := critInfrastructureToSesWizard;
  aIDField := aDataSet.FindField( 'F_INFRASTRUCTURE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_INFRASTRUCTURE_ID <> ' + aIDField.AsString;
  end;

  TdtmCenInfrastructure.SelectCenInfrastructure( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Show an Infrastructure.

  @Name       TdtmSesWizard.ShowInfrastructure
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesWizard.ShowInfrastructure(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critInfrastructureToSession;
  aIDField := aDataSet.FindField( 'F_INFRASTRUCTURE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCenInfrastructure.ShowCenInfrastructure( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

procedure TdtmSesWizard.cdsSessionWizardAfterPost(DataSet: TDataSet);
begin
  try
    ( DataSet as TClientDataSet ).ApplyUpdates( 0 );
  except
    ( DataSet as TClientDataSet ).CancelUpdates;
    Abort;
//  inherited;
  end;
end;

procedure TdtmSesWizard.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_SES_CREATE' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_SES_UPDATE' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_ENR_CREATE' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_ENR_UPDATE' ).AsBoolean := False;
end;

function TdtmSesWizard.GetProgramDataSet: TClientDataSet;
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.ProgramDataSet;
  end
  else
  begin
    Result := cdsProgram;
  end;
end;

function TdtmSesWizard.GetSessionDataSet: TClientDataSet;
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.SessionDataSet;
  end
  else
  begin
    Result := cdsSessie;
  end;
end;

function TdtmSesWizard.GetWizardDataSet: TClientDataSet;
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.WizardDataSet;
  end
  else
  begin
    Result := cdsSessionWizard;
  end;
end;

function TdtmSesWizard.GetWizardResultDataSet: TClientDataSet;
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.WizardResultDataSet;
  end
  else
  begin
    Result := cdsSessionWizardResult;
  end;
end;

procedure TdtmSesWizard.ValidateCreateAndUpdate(Sender: TField);
begin
  if ( Sender = cdsSessionWizardF_SES_CREATE ) then
  begin
    if ( Sender.AsBoolean ) then
    begin
      cdsSessionWizardF_SES_UPDATE.AsBoolean := not Sender.AsBoolean;
      cdsSessionWizardF_ENR_UPDATE.AsBoolean := not Sender.AsBoolean;
      cdsSessionWizardF_SESSION_ID.AsInteger := -1;
      cdsSessionWizardF_ENROL_ID.AsInteger := -1;
    end;
  end
  else if ( Sender = cdsSessionWizardF_SES_UPDATE ) then
  begin
    if ( Sender.AsBoolean ) then
    begin
      if ( not CanUpdateSession ) then
      begin
        Raise Exception.Create( 'Kan geen sessie aanpassen, er werd geen sessie gevonden' );
      end;
      cdsSessionWizardF_SES_CREATE.AsBoolean := False;
    end;
  end
  else if ( Sender = cdsSessionWizardF_ENR_CREATE ) then
  begin
    if ( Sender.AsBoolean ) then
    begin
      cdsSessionWizardF_ENR_UPDATE.AsBoolean := not Sender.AsBoolean;
      cdsSessionWizardF_ENROL_ID.AsInteger := -1;
    end;
  end
  else if ( Sender = cdsSessionWizardF_ENR_UPDATE ) then
  begin
    if ( Sender.AsBoolean ) then
    begin
      if ( not CanUpdateEnrolment ) then
      begin
        Raise Exception.Create( 'Kan geen inschrijving aanpassen, er werd geen inschrijving gevonden, of er wordt een nieuwe sessie aangemaakt' );
      end;

      cdsSessionWizardF_ENR_CREATE.AsBoolean := False;
    end;
  end
end;

class function TdtmSesWizard.OpenWizard(aDataSet: TDataSet;
  Restore: Boolean): TModalResult;
var
  aSesWizardDataModule : IEDUSesWizardDataModule;
  aDataModule          : IPPWFrameWorkDataModule;
  aClientDataSet       : TClientDataSet;
  aRecordView          : IPPWFrameWorkRecordView;
begin
  Result := mrCancel;

  aClientDataSet := TClientDataSet.Create( Nil );

  try
    aDataModule := dtmEDUMainClient.pfcMain.CreateDataModule( Nil, 'TdtmSesWizard', False );
    //aDataModule.OpenDataSets;

    { If necessary we should do some initialisations in here }
    if ( Supports( aDataModule, IEDUSesWizardDataModule, aSesWizardDataModule ) ) then
    begin
      if ( Restore ) then
      begin
        aSesWizardDataModule.RestoreWizardData;
      end
      else
      begin
        aSesWizardDataModule.ShouldRestoreWizardData;
      end;

      aRecordView := aSesWizardDataModule.CreateRecordViewForCurrentRecord( rvmAdd, True );
      Result := aRecordView.ShowModal;
    end;
  finally
    FreeAndNil( aClientDataSet );
    aRecordView := Nil;
    aSesWizardDataModule := Nil;
    aDataModule := Nil;
  end;
end;

class function TdtmSesWizard.OpenWizardForSession(aDataSet: TDataSet;
  Restore: Boolean): TModalResult;
var
  aSesWizardDataModule : IEDUSesWizardDataModule;
  aDataModule          : IPPWFrameWorkDataModule;
  aClientDataSet       : TClientDataSet;
  aRecordView          : IPPWFrameWorkRecordView;
begin
  Result := mrCancel;

  aClientDataSet := TClientDataSet.Create( Nil );

  try
    aDataModule := dtmEDUMainClient.pfcMain.CreateDataModule( Nil, 'TdtmSesWizard', False );

    { If necessary we should do some initialisations in here }

    if ( Supports( aDataModule, IEDUSesWizardDataModule, aSesWizardDataModule ) ) then
    begin
      if ( Restore ) then
      begin
        aSesWizardDataModule.RestoreWizardData;
      end
      else
      begin
        aSesWizardDataModule.ShouldRestoreWizardData;
      end;

      aRecordView := aSesWizardDataModule.CreateRecordViewForCurrentRecord( rvmAdd, True );
      Result := aRecordView.ShowModal;
    end;
  finally
    FreeAndNil( aClientDataSet );
    aRecordView := Nil;
    aSesWizardDataModule := Nil;
    aDataModule := Nil;
  end;
end;

function TdtmSesWizard.GetDataChanged: Boolean;
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.DataChanged;
  end
  else
  begin
    Result := FDataChanged;
  end;
end;

procedure TdtmSesWizard.SetDataChanged(const Value: Boolean);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Value <> DataChanged ) then
  begin
    FDataChanged := Value;

    if ( ( Assigned( ListViewDataModule ) ) and
         ( Supports( ListViewDataModule, IEDUSesWizardDataModule, aDataModule ) ) ) then
    begin
      aDataModule.DataChanged := Value;
    end;

    FForeCastDone := False;
    FExecuteDone  := False;
  End;
end;

function TdtmSesWizard.GetExecuteDone: Boolean;
begin
  Result := FExecuteDone;
end;

function TdtmSesWizard.GetForeCastDone: Boolean;
begin
  Result := FForeCastDone;
end;

end.
