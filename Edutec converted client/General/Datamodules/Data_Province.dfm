inherited dtmProvince: TdtmProvince
  KeyFields = 'F_PROVINCE_ID'
  ListViewClass = 'TfrmProvince_List'
  RecordViewClass = 'TfrmProvince_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Postalcodes'
      ForeignKeys = 'F_PROVINCE_ID'
      DataModuleClass = 'TdtmPostalcode'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROVINCE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRYPART_ID, '
      '  F_COUNTRY_PART_NAME,'
      '  F_PROVINCE_NAME'
      'FROM '
      '  V_GE_PROVINCE')
    object qryListF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROVINCE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRYPART_ID, '
      '  F_COUNTRY_PART_NAME,'
      '  F_PROVINCE_NAME'
      'FROM '
      '  V_GE_PROVINCE')
    object qryRecordF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_PROVINCE_ID: TIntegerField
      DisplayLabel = 'Provincie ( ID )'
      FieldName = 'F_PROVINCE_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Provincie ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Provincie ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 64
    end
    object cdsListF_COUNTRYPART_ID: TIntegerField
      DisplayLabel = 'Landsgedeelte ( ID )'
      FieldName = 'F_COUNTRYPART_ID'
      Visible = False
    end
    object cdsListF_COUNTRY_PART_NAME: TStringField
      DisplayLabel = 'Landsgedeelte'
      FieldName = 'F_COUNTRY_PART_NAME'
      Size = 64
    end
    object cdsListF_PROVINCE_NAME: TStringField
      DisplayLabel = 'Provincie'
      FieldName = 'F_PROVINCE_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_PROVINCE_ID: TIntegerField
      DisplayLabel = 'Provincie ( ID )'
      FieldName = 'F_PROVINCE_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Provincie ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Provincie ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_COUNTRYPART_ID: TIntegerField
      DisplayLabel = 'Landsgedeelte ( ID )'
      FieldName = 'F_COUNTRYPART_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_COUNTRY_PART_NAME: TStringField
      DisplayLabel = 'Landsgedeelte'
      FieldName = 'F_COUNTRY_PART_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_PROVINCE_NAME: TStringField
      DisplayLabel = 'Provincie'
      FieldName = 'F_PROVINCE_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmProvince'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmProvince'
  end
end
