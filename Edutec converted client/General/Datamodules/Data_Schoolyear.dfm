inherited dtmSchoolyear: TdtmSchoolyear
  KeyFields = 'F_SCHOOLYEAR_ID'
  ListViewClass = 'TfrmSchoolyear_List'
  RecordViewClass = 'TfrmSchoolyear_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_SCHOOLYEAR_ID, '
      '  F_SCHOOLYEAR, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM         '
      '  V_GE_SCHOOLYEAR')
    object qryListF_SCHOOLYEAR_ID: TIntegerField
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_SCHOOLYEAR: TStringField
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object qryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_SCHOOLYEAR_ID, '
      '  F_SCHOOLYEAR, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM         '
      '  V_GE_SCHOOLYEAR')
    object qryRecordF_SCHOOLYEAR_ID: TIntegerField
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_SCHOOLYEAR: TStringField
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object qryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
      ReadOnly = True
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_SCHOOLYEAR_ID: TIntegerField
      DisplayLabel = 'Schooljaar ( ID )'
      FieldName = 'F_SCHOOLYEAR_ID'
    end
    object cdsListF_SCHOOLYEAR: TStringField
      DisplayLabel = 'Schooljaar'
      FieldName = 'F_SCHOOLYEAR'
      Size = 50
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_SCHOOLYEAR_ID: TIntegerField
      DisplayLabel = 'Schooljaar ( ID )'
      FieldName = 'F_SCHOOLYEAR_ID'
      Required = True
    end
    object cdsRecordF_SCHOOLYEAR: TStringField
      DisplayLabel = 'Schooljaar'
      FieldName = 'F_SCHOOLYEAR'
      Required = True
      Size = 50
    end
    object cdsRecordF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      Required = True
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      Required = True
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmSchoolYear'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmSchoolYear'
  end
end
