inherited dtmOrganisation: TdtmOrganisation
  AutoOpenDataSets = False
  KeyFields = 'F_ORGANISATION_ID'
  ListViewClass = 'TfrmOrganisation_List'
  RecordViewClass = 'TfrmOrganisation_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Functies'
      ForeignKeys = 'F_ORGANISATION_ID'
      DataModuleClass = 'TdtmRole'
      AutoCreate = False
    end
    item
      Caption = 'Historiek'
      ForeignKeys = 'F_ORGANISATION_ID'
      DataModuleClass = 'TdtmLogHistory'
      AutoCreate = False
    end
    item
      Caption = 'Sessies'
      ForeignKeys = 'F_ORGANISATION_ID'
      DataModuleClass = 'TdtmSesSession'
      AutoCreate = False
    end
    item
      Caption = 'KMO Portefeuille'
      ForeignKeys = 'F_ORGANISATION_ID'
      DataModuleClass = 'TdtmKMO_Portefeuille'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ( ID )'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nr'
      FieldName = 'F_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object cdsListF_VAT_NR: TStringField
      DisplayLabel = 'BTW Nr'
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object cdsListF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 10
    end
    object cdsListF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object cdsListF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ORGTYPE_ID: TIntegerField
      DisplayLabel = 'Organisatie Type ( ID )'
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_ORGTYPE_NAME: TStringField
      DisplayLabel = 'Organisatie Type'
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
    object cdsListF_ORG_NR: TStringField
      DisplayLabel = 'Organisatie nr'
      FieldName = 'F_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object cdsListF_SCHOOL_NR: TStringField
      DisplayLabel = 'Stam nr (school)'
      FieldName = 'F_SCHOOL_NR'
      ProviderFlags = []
      Size = 10
    end
    object cdsListF_MANUALLY_ADDED: TBooleanField
      DisplayLabel = 'Toegevoegd door EduTEC'
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CONSTRUCT_ID: TIntegerField
      DisplayLabel = 'Construct (ID)'
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CONTACT_DATA: TStringField
      DisplayLabel = 'Contact data'
      FieldName = 'F_CONTACT_DATA'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsListF_EMAIL: TStringField
      DisplayWidth = 25
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ( ID )'
      FieldName = 'F_ORGANISATION_ID'
      Required = True
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nr'
      FieldName = 'F_RSZ_NR'
      Size = 14
    end
    object cdsRecordF_VAT_NR: TStringField
      DisplayLabel = 'BTW Nr'
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsRecordF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsRecordF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsRecordF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Visible = False
    end
    object cdsRecordF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsRecordF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
    object cdsRecordF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      Visible = False
    end
    object cdsRecordF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Size = 64
    end
    object cdsRecordF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsRecordF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Visible = False
      Size = 128
    end
    object cdsRecordF_WEBSITE: TStringField
      DisplayLabel = 'Website'
      FieldName = 'F_WEBSITE'
      Size = 128
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object cdsRecordF_ACC_NR: TStringField
      DisplayLabel = 'Rekening Nr'
      FieldName = 'F_ACC_NR'
      Size = 14
    end
    object cdsRecordF_ACC_HOLDER: TStringField
      DisplayLabel = 'Rekening houder'
      FieldName = 'F_ACC_HOLDER'
      Size = 64
    end
    object cdsRecordF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = []
      Visible = False
    end
    object cdsRecordF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsRecordF_INVOICE_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_INVOICE_STREET'
      Size = 128
    end
    object cdsRecordF_INVOICE_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_INVOICE_NUMBER'
      Size = 5
    end
    object cdsRecordF_INVOICE_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_INVOICE_MAILBOX'
      Size = 10
    end
    object cdsRecordF_INVOICE_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_INVOICE_POSTALCODE_ID'
      Visible = False
    end
    object cdsRecordF_INVOICE_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_INVOICE_COUNTRY_ID'
      Visible = False
    end
    object cdsRecordF_ORGTYPE_ID: TIntegerField
      DisplayLabel = 'Organisatie Type ( ID )'
      FieldName = 'F_ORGTYPE_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_ORGTYPE_NAME: TStringField
      DisplayLabel = 'Organisatie Type'
      FieldName = 'F_ORGTYPE_NAME'
      Required = True
      Size = 40
    end
    object cdsRecordF_INVOICE_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_INVOICE_POSTALCODE'
      Size = 10
    end
    object cdsRecordF_INVOICE_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_INVOICE_CITY_NAME'
      Size = 128
    end
    object cdsRecordF_INVOICE_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_INVOICE_COUNTRY_NAME'
      Size = 64
    end
    object cdsRecordF_ORG_NR: TStringField
      FieldName = 'F_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object cdsRecordF_SCHOOL_NR: TStringField
      FieldName = 'F_SCHOOL_NR'
      ProviderFlags = []
      Size = 10
    end
    object cdsRecordF_MANUALLY_ADDED: TBooleanField
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_CONSTRUCT_ID: TIntegerField
      DisplayLabel = 'Construct (ID)'
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_CONTACT_DATA: TStringField
      DisplayLabel = 'Contact data'
      FieldName = 'F_CONTACT_DATA'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsRecordF_CONFIRMATION_CONTACT1: TStringField
      DisplayLabel = 'Contactpersoon 1'
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      DisplayLabel = 'EMail contactpersoon 1'
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_CONFIRMATION_CONTACT2: TStringField
      DisplayLabel = 'Contactpersoon 2'
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      DisplayLabel = 'EMail contactpersoon 2'
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_CONFIRMATION_CONTACT3: TStringField
      DisplayLabel = 'Contactpersoon 3'
      FieldName = 'F_CONFIRMATION_CONTACT3'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField
      DisplayLabel = 'EMail contactpersoon 3'
      FieldName = 'F_CONFIRMATION_CONTACT3_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_INVOICE_EXTRA_INFO: TStringField
      DisplayLabel = 'Extra info'
      FieldName = 'F_INVOICE_EXTRA_INFO'
      Size = 128
    end
    object cdsRecordF_EXTRA_INFO: TStringField
      DisplayLabel = 'Extra info'
      FieldName = 'F_EXTRA_INFO'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PARTNER_WINTER: TBooleanField
      FieldName = 'F_PARTNER_WINTER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_AUTHORIZATION_NR: TStringField
      FieldName = 'F_AUTHORIZATION_NR'
      ProviderFlags = [pfInUpdate]
      Size = 25
    end
    object cdsRecordF_ADMIN_COST: TFloatField
      FieldName = 'F_ADMIN_COST'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ADMIN_COST_TYPE: TStringField
      FieldName = 'F_ADMIN_COST_TYPE'
      ProviderFlags = [pfInUpdate]
      FixedChar = True
      Size = 1
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaStringField: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsSearchCriteriaStringField2: TStringField
      DisplayLabel = 'Sociaalzekerheids Nr'
      FieldName = 'F_RSZ_NR'
      Size = 14
    end
    object cdsSearchCriteriaStringField3: TStringField
      DisplayLabel = 'BTW Nr'
      FieldName = 'F_VAT_NR'
    end
    object cdsSearchCriteriaStringField4: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsSearchCriteriaStringField5: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
    object cdsSearchCriteriaIntegerField: TIntegerField
      DisplayLabel = 'Organisatie Type ( ID )'
      FieldName = 'F_ORGTYPE_ID'
      Visible = False
    end
    object cdsSearchCriteriaStringField6: TStringField
      DisplayLabel = 'Organisatie Type'
      FieldName = 'F_ORGTYPE_NAME'
      Size = 40
    end
    object cdsSearchCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmOrganisation'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmOrganisation'
  end
  object cdsJoinToOrganisation: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvRecord'
    AutoOpen = False
    Left = 424
    Top = 160
    object cdsJoinToOrganisationF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
    end
  end
end
