inherited dtmTitle: TdtmTitle
  ListViewClass = 'TfrmTitle_List'
  RecordViewClass = 'TfrmTitle_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_TITLE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_TITLE_NAME '
      'FROM '
      '  V_GE_TITLE')
    object qryListF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryListF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_TITLE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_TITLE_NAME '
      'FROM '
      '  V_GE_TITLE'
      '')
    object qryRecordF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryRecordF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_TITLE_ID: TIntegerField
      DisplayLabel = 'Title ( ID )'
      FieldName = 'F_TITLE_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Title ( NL )'
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Title ( FR )'
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object cdsListF_TITLE_NAME: TStringField
      DisplayLabel = 'Title'
      FieldName = 'F_TITLE_NAME'
      ReadOnly = True
      Size = 64
    end
    object cdsListF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = []
    end
    object cdsListF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_TITLE_ID: TIntegerField
      DisplayLabel = 'Title ( ID )'
      FieldName = 'F_TITLE_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Title ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Title ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_TITLE_NAME: TStringField
      DisplayLabel = 'Title'
      FieldName = 'F_TITLE_NAME'
      Size = 64
    end
    object cdsRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = []
    end
    object cdsRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmTitle'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmTitle'
  end
end
