inherited dtmPostalcode: TdtmPostalcode
  KeyFields = 'F_POSTALCODE_ID'
  ListViewClass = 'TfrmPostalcode_List'
  RecordViewClass = 'TfrmPostalcode_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_POSTALCODE_ID, '
      '  F_PROVINCE_ID, '
      '  F_CITY_NL, '
      '  F_CITY_FR, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PROVINCE_NAME '
      'FROM '
      '  V_GE_POSTALCODE')
    object qryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_CITY_NL: TStringField
      FieldName = 'F_CITY_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_CITY_FR: TStringField
      FieldName = 'F_CITY_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object qryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object qryListF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_POSTALCODE_ID, '
      '  F_PROVINCE_ID, '
      '  F_CITY_NL, '
      '  F_CITY_FR, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PROVINCE_NAME '
      'FROM '
      '  V_GE_POSTALCODE')
    object qryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_PROVINCE_ID: TIntegerField
      FieldName = 'F_PROVINCE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_CITY_NL: TStringField
      FieldName = 'F_CITY_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_CITY_FR: TStringField
      FieldName = 'F_CITY_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object qryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object qryRecordF_PROVINCE_NAME: TStringField
      FieldName = 'F_PROVINCE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Required = True
    end
    object cdsListF_PROVINCE_ID: TIntegerField
      DisplayLabel = 'Provincie ( ID )'
      FieldName = 'F_PROVINCE_ID'
      Visible = False
    end
    object cdsListF_CITY_NL: TStringField
      DisplayLabel = 'Gemeente ( NL )'
      FieldName = 'F_CITY_NL'
      Required = True
      Visible = False
      Size = 128
    end
    object cdsListF_CITY_FR: TStringField
      DisplayLabel = 'Gemeente ( FR )'
      FieldName = 'F_CITY_FR'
      Required = True
      Visible = False
      Size = 128
    end
    object cdsListF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsListF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Required = True
      Size = 128
    end
    object cdsListF_PROVINCE_NAME: TStringField
      DisplayLabel = 'Provincie'
      FieldName = 'F_PROVINCE_NAME'
      Required = True
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Required = True
    end
    object cdsRecordF_PROVINCE_ID: TIntegerField
      DisplayLabel = 'Provincie ( ID )'
      FieldName = 'F_PROVINCE_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_CITY_NL: TStringField
      DisplayLabel = 'Gemeente ( NL )'
      FieldName = 'F_CITY_NL'
      Required = True
      Size = 128
    end
    object cdsRecordF_CITY_FR: TStringField
      DisplayLabel = 'Gemeente ( FR )'
      FieldName = 'F_CITY_FR'
      Required = True
      Size = 128
    end
    object cdsRecordF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Required = True
      Size = 10
    end
    object cdsRecordF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Visible = False
      Size = 128
    end
    object cdsRecordF_PROVINCE_NAME: TStringField
      DisplayLabel = 'Provincie'
      FieldName = 'F_PROVINCE_NAME'
      Required = True
      Size = 64
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
    end
    object cdsSearchCriteriaF_PROVINCE_ID: TIntegerField
      DisplayLabel = 'Provincie ( ID )'
      FieldName = 'F_PROVINCE_ID'
    end
    object cdsSearchCriteriaF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsSearchCriteriaF_PROVINCE_NAME: TStringField
      DisplayLabel = 'Provincie'
      FieldName = 'F_PROVINCE_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmPostalCode'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmPostalCode'
  end
end
