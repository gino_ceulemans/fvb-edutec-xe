inherited dtmDiploma: TdtmDiploma
  KeyFields = 'F_DIPLOMA_ID'
  ListViewClass = 'TfrmDiploma_List'
  RecordViewClass = 'TfrmDiploma_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_DIPLOMA_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_DIPLOMA_NAME'
      'FROM '
      '  V_DOC_DIPLOMA')
    object qryListF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_DIPLOMA_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_DIPLOMA_NAME'
      'FROM '
      '  V_DOC_DIPLOMA')
    object qryRecordF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_DIPLOMA_ID: TIntegerField
      DisplayLabel = 'Diploma ( ID )'
      FieldName = 'F_DIPLOMA_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Diploma ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Diploma ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 64
    end
    object cdsListF_DIPLOMA_NAME: TStringField
      DisplayLabel = 'Diploma'
      FieldName = 'F_DIPLOMA_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_DIPLOMA_ID: TIntegerField
      DisplayLabel = 'Diploma ( ID )'
      FieldName = 'F_DIPLOMA_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Diploma ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Diploma ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_DIPLOMA_NAME: TStringField
      DisplayLabel = 'Diploma'
      FieldName = 'F_DIPLOMA_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmDiploma'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmDiploma'
  end
end
