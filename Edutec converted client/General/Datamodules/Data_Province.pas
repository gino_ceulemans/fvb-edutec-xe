{*****************************************************************************
  This DataModule will be used for the Maintenance of T_GE_PROVINCE records.
  
  @Name       Data_Province
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Province;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmProvince = class(TEduDataModule, IEDUProvinceDataModule )
    qryListF_PROVINCE_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_COUNTRYPART_ID: TIntegerField;
    qryListF_COUNTRY_PART_NAME: TStringField;
    qryListF_PROVINCE_NAME: TStringField;
    qryRecordF_PROVINCE_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_COUNTRYPART_ID: TIntegerField;
    qryRecordF_COUNTRY_PART_NAME: TStringField;
    qryRecordF_PROVINCE_NAME: TStringField;
    cdsListF_PROVINCE_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_COUNTRYPART_ID: TIntegerField;
    cdsListF_COUNTRY_PART_NAME: TStringField;
    cdsListF_PROVINCE_NAME: TStringField;
    cdsRecordF_PROVINCE_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_COUNTRYPART_ID: TIntegerField;
    cdsRecordF_COUNTRY_PART_NAME: TStringField;
    cdsRecordF_PROVINCE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectCountryPart( aDataSet : TDataSet ); virtual;
    procedure ShowCountryPart  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectProvince( aDataSet : TDataSet ); virtual;
    class procedure ShowProvince  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  end;

var
  dtmProvince: TdtmProvince;

implementation

uses Data_CountryPart, Data_EDUMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Province related fields from one
  DataSet to another DataSet.

  @Name       CopyProvinceFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyProvinceFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_PROVINCE_ID' ).AsInteger :=
        aSource.FieldByName( 'F_PROVINCE_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    aDestination.FieldByName( 'F_PROVINCE_NAME' ).AsString :=
      aSource.FieldByName( 'F_PROVINCE_NAME' ).AsString;
  end;
end;

procedure TdtmProvince.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_GE_PROVINCE';
end;

{*****************************************************************************
  This Method will be used to select one or more Country Part Records.

  @Name       TdtmProvince.SelectCountryPart
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProvince.SelectCountryPart(aDataSet: TDataSet);
begin
  TdtmCountryPart.SelectCountryPart( aDataSet );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Country Part
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmProvince.ShowCountryPart
  @author     slesage
  @param      None
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProvince.ShowCountryPart(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_COUNTRYPART_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCountryPart.ShowCountryPart( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Province Records.

  @Name       TdtmProvince.SelectProvince
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProvince.SelectProvince(aDataSet: TDataSet);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the CountryPart ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmProvince', {'PE_IS_MILITANT = 1'} '' , aSelectedRecords, False ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyProvinceFieldValues( aSelectedRecords, aDataSet, True );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Province
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmProvince.ShowProvince
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProvince.ShowProvince(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView for TdtmProvince.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmProvince', 'F_PROVINCE_ID = ' + aDataSet.FieldByName( 'F_PROVINCE_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyProvinceFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ) );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmProvince.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProvince.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PROVINCE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmCountryPart.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProvince.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUCountryPartDataModule ) ) then
    begin
       aDataSet.FieldByName( 'F_COUNTRY_PART_NAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_COUNTRY_PART_NAME' ).AsString;
    end;
  end;
end;

end.