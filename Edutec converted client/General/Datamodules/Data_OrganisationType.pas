{*****************************************************************************
  This DataModule will be used for the maintenance of T_OR_TYPE.

  @Name       Data_OrganisationType
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_OrganisationType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, DB, ADODB,
  Unit_FVBFFCInterfaces, MConnect, unit_EdutecInterfaces, Unit_PPWFrameWorkActions,
  Datasnap.DSConnect;

type
  TdtmOrganisationType = class(TEduDataModule, IEDUOrgTypeDataModule)
    qryListF_ORGTYPE_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_ORGTYPE_NAME: TStringField;
    qryRecordF_ORGTYPE_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_ORGTYPE_NAME: TStringField;
    cdsListF_ORGTYPE_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_ORGTYPE_NAME: TStringField;
    cdsRecordF_ORGTYPE_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_ORGTYPE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    function IsListViewActionAllowed(aAction: TPPWFrameWorkListViewAction): Boolean; override;
  public
    { Public declarations }
    class procedure SelectOrganisationType( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowOrganisationType  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient;

{*****************************************************************************
  This procedure will be used to copy OrganisationType related fields from one
  DataSet to another DataSet.

  @Name       CopyOrganisationTypeFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyOrganisationTypeFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault  );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_ORGTYPE_ID' ).AsInteger :=
        aSource.FieldByName( 'F_ORGTYPE_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_ORGTYPE_NAME' ).AsString :=
          aSource.FieldByName( 'F_ORGTYPE_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more OrganisationType Records.

  @Name       TdtmOrganisationType.SelectOrganisationType
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmOrganisationType.SelectOrganisationType(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmOrganisationType', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyOrganisationTypeFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single OrganisationType
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmOrganisationType.ShowOrganisationType
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmOrganisationType.ShowOrganisationType(
  aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmOrganisationType', 'F_ORGTYPE_ID = ' + aDataSet.FieldByName( 'F_ORGTYPE_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyOrganisationTypeFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmOrganisationType.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_OR_TYPE';
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmOrganisationType.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which to initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisationType.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ORGTYPE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  Name           : IsListViewActionAllowed
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : We just rearranged the organisation types. Because business logic is
                   linked to each organisation type, we now no longer allow organisationtypes to be
                   modified or inserted manually.

  History        :

  Date         By                   Description
  ----         --                   -----------
  16/08/2007   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TdtmOrganisationType.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
var
  aAllowed: Boolean;
begin

  aAllowed := Inherited IsListViewActionAllowed( aAction );

  if (aAction is TPPWFrameWorkListViewDeleteAction) or
     (aAction is TPPWFrameWorkListViewAddAction) or
     (aAction is TPPWFrameWorkListViewEditAction) then
  begin
    aAllowed := false;
  end;
  
  Result := aAllowed;
end;

end.