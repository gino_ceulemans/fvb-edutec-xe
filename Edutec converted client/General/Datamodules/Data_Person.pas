{************************************************************************************
  This DataModule will be used for the maintenance of <T_PE_PERSON>.

  @Name       Data_Person
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/04/2008   ivdbossche           Added JoinPersons (Mantis 2073).
  18/04/2008   ivdbossche           Added cdsDoublePersons (Mantis 2073).
  04/04/2008   Ivan Van den Bossche Added ShowDiploma and SelectDiploma (Mantis 2196)
  04/04/2008   Ivan Van den Bossche Added F_DIPLOMA_ID to adoqryRecord (Mantis 2196)
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
*************************************************************************************}

unit Data_Person;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

const
  DEFAULT_VALUE_DIPLOMA_ID=0;
  DEFAULT_VALUE_DIPLOMA_NAME='Niet van toepassing';

resourcestring
  rsNoDuplicatesFound='Voor persoon %s %s zijn er geen dubbele persoonsgegevens gevonden in hetzelfde bedrijf.';
  rsPersonJoinedWith='Personen met ID''s %s  en %s worden samengevoegd.' + #13 + #13
                    + 'Persoon met ID %s blijft behouden.' + #13 + #13
                    + 'Bent u zeker?';

type
  TdtmPerson = class(TEDUDataModule, IEDUPersonDataModule )
    cdsListF_PERSON_ID: TIntegerField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_POSTALCODE_ID: TIntegerField;
    cdsListF_POSTALCODE: TStringField;
    cdsListF_CITY_NAME: TStringField;
    cdsListF_COUNTRY_ID: TIntegerField;
    cdsListF_COUNTRY_NAME: TStringField;
    cdsListF_LANGUAGE_ID: TIntegerField;
    cdsListF_LANGUAGE_NAME: TStringField;
    cdsListF_MEDIUM_ID: TIntegerField;
    cdsListF_MEDIUM_NAME: TStringField;
    cdsRecordF_PERSON_ID: TIntegerField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_STREET: TStringField;
    cdsRecordF_NUMBER: TStringField;
    cdsRecordF_MAILBOX: TStringField;
    cdsRecordF_POSTALCODE_ID: TIntegerField;
    cdsRecordF_POSTALCODE: TStringField;
    cdsRecordF_CITY_NAME: TStringField;
    cdsRecordF_COUNTRY_ID: TIntegerField;
    cdsRecordF_COUNTRY_NAME: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_GSM: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_DATEBIRTH: TDateTimeField;
    cdsRecordF_GENDER_ID: TIntegerField;
    cdsRecordF_GENDER_NAME: TStringField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_TITLE_ID: TIntegerField;
    cdsRecordF_TITLE_NAME: TStringField;
    cdsRecordF_MEDIUM_ID: TIntegerField;
    cdsRecordF_MEDIUM_NAME: TStringField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_ACC_NR: TStringField;
    cdsSearchCriteriaF_SOCSEC_NR: TStringField;
    cdsSearchCriteriaF_LASTNAME: TStringField;
    cdsSearchCriteriaF_FIRSTNAME: TStringField;
    cdsSearchCriteriaF_POSTALCODE_ID: TIntegerField;
    cdsSearchCriteriaF_POSTALCODE: TStringField;
    cdsSearchCriteriaF_CITY_NAME: TStringField;
    cdsSearchCriteriaF_COUNTRY_ID: TIntegerField;
    cdsSearchCriteriaF_COUNTRY_NAME: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsSearchCriteriaF_ACTIVE: TBooleanField;
    cdsListF_MANUALLY_ADDED: TBooleanField;
    cdsListF_CONSTRUCT_ID: TIntegerField;
    cdsRecordF_MANUALLY_ADDED: TBooleanField;
    cdsRecordF_CONSTRUCT_ID: TIntegerField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_GSM: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsListF_ROLE: TStringField;
    cdsRecordF_DIPLOMA_ID: TIntegerField;
    cdsRecordF_DIPLOMA_NAME: TStringField;
    cdsDoublePersons: TFVBFFCClientDataSet;
    cdsDoublePersonsF_PERSON_ID: TIntegerField;
    cdsRecordF_SYNERGY_ID: TLargeintField;
    cdsListF_SYNERGY_ID: TLargeintField;
    cdsListF_PLACE_OF_BIRTH: TStringField;
    cdsRecordF_PLACE_OF_BIRTH: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectCountry    ( aDataSet : TDataSet ); virtual;
    procedure SelectGender     ( aDataSet : TDataSet ); virtual;
    procedure SelectLanguage   ( aDataSet : TDataSet ); virtual;
    procedure SelectMedium     ( aDataSet : TDataSet ); virtual;
    procedure SelectPostalCode ( aDataSet : TDataSet ); virtual;
    procedure SelectTitle      ( aDataSet : TDataSet ); virtual;
    procedure SelectDiploma    ( aDataSet : TDataSet ); virtual;
    procedure ShowCountry      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowGender       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowPostalCode   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowTitle        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowDiploma      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectPerson(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure ShowPerson  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    procedure RetrieveDoublePersons ( aPersonId: Integer; aDoublePersonIds: TStringList);
    procedure JoinPersons(const APersonList: WideString; ACopyToPersonID: Integer);
  end;

  procedure CopyPersonFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_Gender, Data_Country, Data_Language,
  Data_Medium, Data_Title, Data_Postalcode, Data_Diploma;

{*****************************************************************************
  This procedure will be used to copy Person related fields from one
  DataSet to another DataSet.

  @Name       CopyPersonFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyPersonFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_PERSON_ID' ).AsInteger :=
        aSource.FieldByName( 'F_PERSON_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_PERSON_NAME' ).AsString :=
          aSource.FieldByName( 'F_PERSON_NAME' ).AsString;
      end;
      critPersonToRole, critPersonToEnrol, critPersonToSesWizard,
      critPersonToSesWizardExecute :
      begin
        if ( not ( aCopyTo = critPersonToSesWizardExecute ) ) then
        begin
          aDestination.FieldByName( 'F_SOCSEC_NR' ).AsString :=
            aSource.FieldByName( 'F_SOCSEC_NR' ).AsString;
        end;
        aDestination.FieldByName( 'F_LASTNAME' ).AsString :=
          aSource.FieldByName( 'F_LASTNAME' ).AsString;
        aDestination.FieldByName( 'F_FIRSTNAME' ).AsString :=
          aSource.FieldByName( 'F_FIRSTNAME' ).AsString;
        aDestination.FieldByName('F_PHONE').AsString := aSource.FieldByName('F_PHONE').AsString;
        aDestination.FieldByName('F_GSM').AsString := aSource.FieldByName('F_GSM').AsString;
        aDestination.FieldByName('F_FAX').AsString := aSource.FieldByName('F_FAX').AsString;
        aDestination.FieldByName('F_EMAIL').AsString := aSource.FieldByName('F_EMAIL').AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Person Records.

  @Name       TdtmPerson.SelectPerson
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmPerson.SelectPerson(
    aDataSet : TDataSet;
    aWhereClause : String = '';
    aMultiSelect : Boolean = False;
    aCopyTo : TCopyRecordInformationTo = critDefault;
    aShowInactive : boolean = False
    );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmPerson', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyPersonFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Person
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmPerson.ShowPerson
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmPerson.ShowPerson(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aWhere : string;
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    // Controle of veld is ingevuld zodat we zeker record kunnen selecteren
    if (aDataSet.FieldByName( 'F_PERSON_ID' ).IsNull)
      and not (aRecordViewMode = rvmAdd) then
    begin
       exit;
    end;
    if not (aRecordViewMode = rvmAdd) then
    begin
      aWhere := 'F_PERSON_ID = ' + aDataSet.FieldByName( 'F_PERSON_ID' ).AsString;
    end
    else
    begin
      aWhere := '';
    end;

    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmPerson',
      aWhere,
      aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 )  And ( aRecordViewMode <> rvmView ) then
      begin
        CopyPersonFieldValues( aSelectedRecords, aDataSet,
         ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmPerson.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_PE_PERSON';
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmPerson.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which to initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PERSON_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
  // dtmEDUMainClient.GetNewRecordID( 'T_PE_PERSON' );
end;

{*************************************************************************************
  This method will be used to initialise some Additional Fields.

  @Name       TdtmPerson.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which to initialise the Fields.
  @return     None
  @Exception  None
  @See        None
  @History                Author                  Description
  04/04/2008        Ivan Van den Bossche          Added F_DIPLOMA_ID & F_DIPLOMA_NAME;
                                                  (Mantis 2196).
**************************************************************************************}

procedure TdtmPerson.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_COUNTRY_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_COUNTRY_NAME' ).AsString := 'Belgi�';
  aDataSet.FieldByName( 'F_MEDIUM_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_MEDIUM_NAME' ).AsString := 'Post';
  aDataSet.FieldByName( 'F_LANGUAGE_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_LANGUAGE_NAME' ).AsString := 'Nederlands';
  aDataSet.FieldByName( 'F_MANUALLY_ADDED' ).AsBoolean := True;
  // BUGFIX OM LEGE RR-NR's TOE TE LATEN
  aDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString := '';
  aDataSet.FieldByName( 'F_DIPLOMA_ID').AsInteger := DEFAULT_VALUE_DIPLOMA_ID;
  aDataSet.FieldByName( 'F_DIPLOMA_NAME').AsString := DEFAULT_VALUE_DIPLOMA_NAME;

end;

{*****************************************************************************
  This method will be used to select one or more Country Records.

  @Name       TdtmPerson.SelectCountry
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectCountry(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_COUNTRY_ID <> ' + aIDField.AsString;
  end;

  TdtmCountry.SelectCountry( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to select one or more Language Records.

  @Name       TdtmPerson.SelectLanguage
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectLanguage(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_LANGUAGE_ID <> ' + aIDField.AsString;
  end;

  TdtmLanguage.SelectLanguage( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more Medium Records.

  @Name       TdtmPerson.SelectMedium
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectMedium(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Medium isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_MEDIUM_ID <> ' + aIDField.AsString;
  end;

  TdtmMedium.SelectMedium( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more PostalCode Records.

  @Name       TdtmPerson.SelectPostalCode
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectPostalCode(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_POSTALCODE_ID <> ' + aIDField.AsString;
  end;

  TdtmPostalcode.SelectPostalCode( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Language.

  @Name       TdtmPerson.ShowLanguage
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView );
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Medium.

  @Name       TdtmPerson.ShowMedium
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowMedium(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView );
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmMedium.ShowMedium( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a PostalCode.

  @Name       TdtmPerson.ShowPostalCode
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowPostalCode(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmPostalcode.ShowPostalCode( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Country.

  @Name       TdtmPerson.ShowCountry
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowCountry(aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCountry.ShowCountry( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;


{*****************************************************************************
  This method will be used to allow the user to Show a Country.

  @Name       TdtmPerson.ShowDiploma
  @author     Ivan Van den Bossche
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowDiploma(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_DIPLOMA_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmDiploma.ShowDiploma( aDataSet, aRecordViewMode );
  end;

end;


{*****************************************************************************
  This method will be used to select one or more Gender Records.

  @Name       TdtmPerson.SelectGender
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectGender(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_GENDER_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_GENDER_ID <> ' + aIDField.AsString;
  end;

  TdtmGender.SelectGender( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Gender.

  @Name       TdtmPerson.ShowGender
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowGender(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_GENDER_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmGender.ShowGender( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Title Records.

  @Name       TdtmPerson.SelectTitle
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectTitle(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_TITLE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_TITLE_ID <> ' + aIDField.AsString;
  end;

  TdtmTitle.SelectTitle( aDataSet, aWhere );
end;


{*****************************************************************************
  This method will be used to select one Diploma record.

  @Name       TdtmPerson.SelectDiploma
  @author     Ivan Van den Bossche
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.SelectDiploma(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_DIPLOMA_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_DIPLOMA_ID <> ' + aIDField.AsString;
  end;

  TdtmDiploma.SelectDiploma( aDataSet, aWhere );

end;


{*****************************************************************************
  This method will be used to allow the user to Show a Title.

  @Name       TdtmPerson.ShowTitle
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPerson.ShowTitle(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_TITLE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmTitle.ShowTitle( aDataSet, aRecordViewMode );
  end;
end;

procedure TdtmPerson.cdsSearchCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsSearchCriteriaF_ACTIVE.Value := True;
end;

{********************************************************************************************************
  This method will be used to retrieve person id's from people with
  the same lastname, firstname and organisation

  @Name       TdtmPerson.RetrieveDoublePersons
  @author     Ivan Van den Bossche
  @param      aPersonId         Person we would like to find duplicates of.
  @param      aDoublePersonIds  Enumeration of person ids with same lastname, firstname and organisation.
  @return     None
  @Exception  None
  @See        None
*********************************************************************************************************}

procedure TdtmPerson.RetrieveDoublePersons(aPersonId: Integer;
  aDoublePersonIds: TStringList);
begin
  assert( aDoublePersonIds <> nil );

  cdsDoublePersons.Params.Clear;
  cdsDoublePersons.FetchParams;
  cdsDoublePersons.Params.ParamByName('@aPersonId').AsInteger := aPersonId;
  cdsDoublePersons.Open;
  try
    while not cdsDoublePersons.Eof do
    begin
        aDoublePersonIds.Add( cdsDoublePersons.FieldByName('F_PERSON_ID').AsString );
        cdsDoublePersons.Next;
    end;

  finally
      cdsDoublePersons.Close;
  end;

end;

{************************************************************************************
  @Name       TdtmPerson.JoinPersons
  @author     Ivan Van den Bossche (25/04/2008)
  @param      APersonList --> PersonId(s) which must be converted
              ACopyToPersonId --> PersonId to which APersonList must be converted to
  @return     None
  @Exception  None
  @See        None
  @Descr      Procedure converts different persons to 1 person. (Mantis 2073).

**************************************************************************************}
procedure TdtmPerson.JoinPersons(const APersonList: WideString;
  ACopyToPersonID: Integer);
begin
  ssckData.AppServer.JoinPersons( APersonList, ACopyToPersonID );
  
end;

end.
