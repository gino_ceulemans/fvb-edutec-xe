inherited dtmProfile: TdtmProfile
  KeyFields = 'F_PROFILE_ID'
  ListViewClass = 'TfrmProfile_List'
  RecordViewClass = 'TfrmProfile_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Gebruikers/Profielen'
      ForeignKeys = 'F_PROFILE_ID'
      DataModuleClass = 'TdtmUserProfile'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFILE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_PROFILE')
    object qryListF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFILE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_PROFILE')
    object qryRecordF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_PROFILE_ID: TIntegerField
      DisplayLabel = 'Profiel ( ID )'
      FieldName = 'F_PROFILE_ID'
      Required = True
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Profiel ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Profiel ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_PROFILE_NAME: TStringField
      DisplayLabel = 'Profiel'
      FieldName = 'F_PROFILE_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_PROFILE_ID: TIntegerField
      DisplayLabel = 'Profiel ( ID )'
      FieldName = 'F_PROFILE_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Profiel ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Profiel ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_PROFILE_NAME: TStringField
      DisplayLabel = 'Profiel'
      FieldName = 'F_PROFILE_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmProfile'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmProfile'
  end
end
