inherited dtmRole: TdtmRole
  AutoOpenDataSets = False
  FilterFirst = True
  KeyFields = 'F_ROLE_ID'
  ListViewClass = 'TfrmRole_List'
  RecordViewClass = 'TfrmRole_Record'
  Registered = True
  AutoOpenWhenDetail = False
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    PacketRecords = 500
    object cdsListF_ROLE_ID: TIntegerField
      DisplayLabel = 'Functie ( ID )'
      FieldName = 'F_ROLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Visible = False
    end
    object cdsListF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksr. Nr'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie ( ID )'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ Nr'
      FieldName = 'F_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
    object cdsListF_ROLEGROUP_ID: TIntegerField
      DisplayLabel = 'Rol ( ID )'
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Rol'
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      ProviderFlags = []
    end
    object cdsListF_PRINCIPAL: TBooleanField
      DisplayLabel = 'Hoofdrol'
      FieldName = 'F_PRINCIPAL'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SCHOOLYEAR_ID: TIntegerField
      DisplayLabel = 'Schooljaar ( ID )'
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_SCHOOLYEAR: TStringField
      DisplayLabel = 'Schooljaar'
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = []
      Size = 50
    end
    object cdsListF_REMARK: TStringField
      DisplayLabel = 'Opmerkingen'
      FieldName = 'F_REMARK'
      Size = 1024
    end
    object cdsListF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object cdsListF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object cdsListF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object cdsListF_EMAIL: TStringField
      DisplayLabel = 'Email'
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ROLE_ID: TIntegerField
      DisplayLabel = 'Functie ( ID )'
      FieldName = 'F_ROLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object cdsRecordF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksr. Nr'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ Nr'
      FieldName = 'F_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
    object cdsRecordF_ROLEGROUP_ID: TIntegerField
      DisplayLabel = 'Rol ( ID )'
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Rol'
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      ProviderFlags = []
    end
    object cdsRecordF_PRINCIPAL: TBooleanField
      DisplayLabel = 'Hoofdrol'
      FieldName = 'F_PRINCIPAL'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_SCHOOLYEAR_ID: TIntegerField
      DisplayLabel = 'Schooljaar ( ID )'
      FieldName = 'F_SCHOOLYEAR_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SCHOOLYEAR: TStringField
      DisplayLabel = 'Schooljaar'
      FieldName = 'F_SCHOOLYEAR'
      ProviderFlags = []
      Size = 50
    end
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie (ID)'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_REMARK: TStringField
      DisplayLabel = 'Opmerkingen'
      FieldName = 'F_REMARK'
      Size = 1024
    end
    object cdsRecordF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object cdsRecordF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object cdsRecordF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsSearchCriteriaF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsSearchCriteriaF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksr. Nr'
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsSearchCriteriaF_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ Nr'
      FieldName = 'F_RSZ_NR'
      Size = 14
    end
    object cdsSearchCriteriaF_ROLEGROUP_ID: TIntegerField
      DisplayLabel = 'Rol ( ID )'
      FieldName = 'F_ROLEGROUP_ID'
    end
    object cdsSearchCriteriaF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Rol'
      FieldName = 'F_ROLEGROUP_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmRole'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmRole'
  end
end
