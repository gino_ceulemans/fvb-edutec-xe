{*****************************************************************************
  This DataModule will be used for the Maintenance of T_SYS_USER_PROF records.

  @Name       Data_UserProfile
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  05/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_UserProfile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect,unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmUserProfile = class(TEduDataModule, IEDUUserProfileDataModule )
    qryListF_USER_ID: TIntegerField;
    qryListF_LASTNAME: TStringField;
    qryListF_FIRSTNAME: TStringField;
    qryListF_COMPLETE_USERNAME: TStringField;
    qryListF_PROFILE_ID: TIntegerField;
    qryListF_PROFILE_NAME: TStringField;
    cdsListF_USER_ID: TIntegerField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_COMPLETE_USERNAME: TStringField;
    cdsListF_PROFILE_ID: TIntegerField;
    cdsListF_PROFILE_NAME: TStringField;
    cdsRecordF_USER_ID: TIntegerField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_COMPLETE_USERNAME: TStringField;
    cdsRecordF_PROFILE_ID: TIntegerField;
    cdsRecordF_PROFILE_NAME: TStringField;
    qryRecordF_USER_ID: TIntegerField;
    qryRecordF_LASTNAME: TStringField;
    qryRecordF_FIRSTNAME: TStringField;
    qryRecordF_COMPLETE_USERNAME: TStringField;
    qryRecordF_PROFILE_ID: TIntegerField;
    qryRecordF_PROFILE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectProfile( aDataSet : TDataSet ); virtual;
    procedure SelectUser   ( aDataSet : TDataSet ); virtual;
    procedure ShowProfile  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowUser     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    procedure Add; override;
  end;

var
  dtmUserProfile: TdtmUserProfile;

implementation

uses Data_User, Data_EDUMainClient, Data_Profile;

{$R *.dfm}

const
  cUsersNotLinkedToProfile = 'F_USER_ID NOT IN ( SELECT F_USER_ID FROM V_SYS_USER_PROFILE WHERE F_PROFILE_ID = %s )';
  cProfilesNotLinkedToUser = 'F_PROFILE_ID NOT IN ( SELECT F_PROFILE_ID FROM V_SYS_USER_PROFILE WHERE F_USER_ID = %s )';
  
procedure TdtmUserProfile.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_SYS_USER_PROF';
end;

{*****************************************************************************
  This method will be used to select a User from a List of Users.

  @Name       TdtmUserProfile.SelectUser
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUserProfile.SelectUser(aDataSet: TDataSet );
var
  aWhereClause : String;
begin
  aWhereClause := Format( cUsersNotLinkedToProfile, [ aDataSet.FieldByName( 'F_PROFILE_ID' ).AsString ] );
  TdtmUser.SelectUser( aDataSet, aWhereClause );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmUserProfile.ShowUser
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUserProfile.ShowUser(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  TdtmUser.ShowUser( aDataSet, aRecordViewMode );
end;

{*****************************************************************************
  This method will be executed when a record should be added to the List.  When
  the DataModule is used as a Detail of a Profile or User Datamodule we will
  allow the user to select a record from a list of available records instead
  of using the Default functionality.

  @Name       TdtmUserProfile.Add
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUserProfile.Add;
var
  aSelectedRecords : TClientDataSet;
  aWhereClause : String;
begin
  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUProfileDataModule ) ) then
    begin
      aWhereClause := Format( cUsersNotLinkedToProfile, [ MasterDataModule.RecordDataset.FieldByName( 'F_PROFILE_ID' ).AsString ] );
      aSelectedRecords := TClientDataSet.Create( Application );
      try
        { Show the Country ListView for selection purposes, passing in a
          WHERE clause as second parameter to limit the resultset if necessary.
          The selected records should be added to our aSelectedRecords dataset,
          and the user should only be able to select one record }
        if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmUser', aWhereClause , aSelectedRecords, True ) = mrOk ) then
        begin
          aSelectedRecords.First;
          while not aSelectedRecords.Eof do
          begin
            cdsList.Tag := 1;
            cdsList.Insert;
            CopyUserFieldValues( aSelectedRecords, cdsList, True );
            cdsList.Post;
            aSelectedRecords.Next;
          end;
        end;
      finally
        FreeAndNil( aSelectedRecords );
      end;
    end
    else if ( Supports( MasterDataModule, IEDUUserDataModule ) ) then
    begin
      aWhereClause := Format( cProfilesNotLinkedToUser, [ MasterDataModule.RecordDataset.FieldByName( 'F_USER_ID' ).AsString ] );
      aSelectedRecords := TClientDataSet.Create( Application );
      try
        { Show the Country ListView for selection purposes, passing in a
          WHERE clause as second parameter to limit the resultset if necessary.
          The selected records should be added to our aSelectedRecords dataset,
          and the user should only be able to select one record }
        if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmProfile', aWhereClause , aSelectedRecords, True ) = mrOk ) then
        begin
          aSelectedRecords.First;
          while not aSelectedRecords.Eof do
          begin
            cdsList.Tag := 1;
            cdsList.Insert;
            CopyProfileFieldValues( aSelectedRecords, cdsList, True );
            cdsList.Post;
            aSelectedRecords.Next;
          end;
        end;
      finally
        FreeAndNil( aSelectedRecords );
      end;
    end;
  end
  else
  begin
    Inherited Add;
  end;
end;

{*****************************************************************************
  This method will be used to select a Profile from a List of Users.

  @Name       TdtmUserProfile.SelectProfile
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUserProfile.SelectProfile(aDataSet: TDataSet);
var
  aWhereClause : String;
begin
  aWhereClause := Format( cProfilesNotLinkedToUser, [ aDataSet.FieldByName( 'F_USER_ID' ).AsString ] );
  TdtmProfile.SelectProfile( aDataSet, aWhereClause );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmUserProfile.ShowProfile
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUserProfile.ShowProfile(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  TdtmProfile.ShowProfile( aDataSet, aRecordViewMode );
end;

{*****************************************************************************
  This method will be used to initialise some fields based on the FK Fields
  used in a Master / Detail Relation.

  @Name       TdtmUserProfile.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUserProfile.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUProfileDataModule ) ) then
    begin
       aDataSet.FieldByName( 'F_PROFILE_NAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_PROFILE_NAME' ).AsString;
    end
    else if ( Supports( MasterDataModule, IEDUUserDataModule ) ) then
    begin
       aDataSet.FieldByName( 'F_LASTNAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_LASTNAME' ).AsString;
       aDataSet.FieldByName( 'F_FIRSTNAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_FIRSTNAME' ).AsString;
       aDataSet.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
    end;
  end;
end;

end.
