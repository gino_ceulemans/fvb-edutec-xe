{*****************************************************************************
  This DataModule will be used for the maintenance of T_SES_EXPENSES.

  @Name       Data_SesExpenses
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  02/08/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_SesExpenses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, Datasnap.DSConnect;

type
  TdtmSesExpenses = class(TEDUDataModule)
    qryListF_EXPENSE_ID: TIntegerField;
    qryListF_SESSION_ID: TIntegerField;
    qryListF_SESSION_NAME: TStringField;
    qryListF_EXPENSE_TYPE: TIntegerField;
    qryListF_EXPENSE_DESC: TMemoField;
    qryListF_EXPENSE_AMOUNT: TFloatField;
    qryRecordF_EXPENSE_ID: TIntegerField;
    qryRecordF_SESSION_ID: TIntegerField;
    qryRecordF_SESSION_NAME: TStringField;
    qryRecordF_EXPENSE_TYPE: TIntegerField;
    qryRecordF_EXPENSE_DESC: TMemoField;
    qryRecordF_EXPENSE_AMOUNT: TFloatField;
    cdsListF_EXPENSE_ID: TIntegerField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_SESSION_NAME: TStringField;
    cdsListF_EXPENSE_TYPE: TIntegerField;
    cdsListF_EXPENSE_DESC: TMemoField;
    cdsListF_EXPENSE_AMOUNT: TFloatField;
    cdsRecordF_EXPENSE_ID: TIntegerField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_SESSION_NAME: TStringField;
    cdsRecordF_EXPENSE_TYPE: TIntegerField;
    cdsRecordF_EXPENSE_DESC: TMemoField;
    cdsRecordF_EXPENSE_AMOUNT: TFloatField;
    cdsRecordF_EXPENSE_DATE: TDateTimeField;
    cdsListF_EXPENSE_DATE: TDateTimeField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectSesExpenses( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowSesExpenses  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_SesSession, unit_EdutecInterfaces;

{*****************************************************************************
  This procedure will be used to copy SesExpenses related fields from one
  DataSet to another DataSet.

  @Name       CopySesExpensesFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopySesExpensesFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_SESEXPENSES_ID' ).AsInteger :=
        aSource.FieldByName( 'F_SESEXPENSES_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_SESEXPENSES_NAME' ).AsString :=
          aSource.FieldByName( 'F_SESEXPENSES_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more SesExpenses Records.

  @Name       TdtmSesExpenses.SelectSesExpenses
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmSesExpenses.SelectSesExpenses(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesExpenses', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopySesExpensesFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single SesExpenses
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmSesExpenses.ShowSesExpenses
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmSesExpenses.ShowSesExpenses(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmSesExpenses', 'F_SESEXPENSES_ID = ' + aDataSet.FieldByName( 'F_SESEXPENSES_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopySesExpensesFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmSesExpenses.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_SES_EXPENSE';
end;

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmSesExpenses.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesExpenses.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_EXPENSE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This event will be use to initialise the Foreign Key Fields.

  @Name       TdtmSesExpenses.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesExpenses.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUSessionDataModule ) ) then
    begin
      { Initialise the Start Date in here so the code to determine the
        Max Enroll or Control Date can be resued }
      CopySessionFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critProgramToSession );
    end;
  end;
end;

{*****************************************************************************
  This event will be use to initialise the Additional Fields.

  @Name       TdtmSesExpenses.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSesExpenses.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_EXPENSE_TYPE' ).AsInteger := 1;
end;

end.