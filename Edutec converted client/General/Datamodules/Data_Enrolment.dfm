inherited dtmEnrolment: TdtmEnrolment
  OnCreate = FVBFFCDataModuleCreate
  KeyFields = 'F_ENROL_ID'
  ListViewClass = 'TfrmEnrolment_List'
  RecordViewClass = 'TfrmEnrolment_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Width = 363
  DetailDataModules = <
    item
      Caption = 'Historiek'
      ForeignKeys = 'F_ENROL_ID'
      DataModuleClass = 'TdtmLogHistory'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    OnCalcFields = cdsListCalcFields
    object cdsListRowNumber: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'RowNumber'
    end
    object cdsListF_ENROL_ID: TIntegerField
      DisplayLabel = 'Inschrijving ( ID )'
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksreg. NR'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object cdsListF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ID'
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_EVALUATION: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_EVALUATION'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CERTIFICATE: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_INVOICED: TBooleanField
      DisplayLabel = 'Gefactureerd'
      FieldName = 'F_INVOICED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_LAST_MINUTE: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_LAST_MINUTE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_HOURS: TIntegerField
      DisplayLabel = 'Geplande uren'
      FieldName = 'F_HOURS'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_TOTAL_SESSION_HOURS: TSmallintField
      FieldName = 'F_TOTAL_SESSION_HOURS'
      ProviderFlags = []
      Required = True
    end
    object cdsListF_HOURS_PRESENT: TSmallintField
      DisplayLabel = 'Uren aanwezig'
      FieldName = 'F_HOURS_PRESENT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_HOURS_ABSENT_JUSTIFIED: TSmallintField
      DisplayLabel = 'Uren gewettigd afwezig'
      FieldName = 'F_HOURS_ABSENT_JUSTIFIED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_HOURS_ABSENT_ILLEGIT: TSmallintField
      DisplayLabel = 'Uren onwettig afwezig'
      FieldName = 'F_HOURS_ABSENT_ILLEGIT'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICING_SCHEME: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object cdsListF_ROLE_ID: TIntegerField
      DisplayLabel = 'Rol (ID)'
      FieldName = 'F_ROLE_ID'
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Bedrijf'
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_PRICE_WORKER: TFloatField
      DisplayLabel = 'Sessieprijs (arbeider)'
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_CLERK: TFloatField
      DisplayLabel = 'Sessieprijs (bediende)'
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PRICE_OTHER: TFloatField
      DisplayLabel = 'Sessieprijs (overige)'
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CERTIFICATE_PRINT_DATE: TDateTimeField
      DisplayLabel = 'Printdatum certificaat'
      FieldName = 'F_CERTIFICATE_PRINT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SELECTED_PRICE: TFloatField
      DisplayLabel = 'Prijs ('#8364')'
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ROLEGROUP_NAME: TStringField
      DisplayLabel = 'Functie'
      FieldName = 'F_ROLEGROUP_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object cdsListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = []
    end
    object cdsListF_NO_PARTICIPANTS: TIntegerField
      FieldName = 'F_NO_PARTICIPANTS'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = []
    end
    object cdsListF_CONSTRUCT: TBooleanField
      FieldName = 'F_CONSTRUCT'
      ProviderFlags = []
      ReadOnly = True
    end
    object cdsListF_PARTNER_WINTER_ID: TIntegerField
      FieldName = 'F_PARTNER_WINTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PARTNER_WINTER_NAME: TStringField
      FieldName = 'F_PARTNER_WINTER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = []
    end
    object cdsListF_SCORE: TIntegerField
      DisplayLabel = 'Score'
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_MAX_SCORE: TIntegerField
      DisplayLabel = 'Max score'
      FieldName = 'F_MAX_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_MAX_PUNTEN: TIntegerField
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = []
    end
    object cdsListF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = []
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ENROL_ID: TIntegerField
      DisplayLabel = 'Inschrijving ( ID )'
      FieldName = 'F_ENROL_ID'
      Required = True
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksregister NR'
      FieldName = 'F_SOCSEC_NR'
      Required = True
      Size = 11
    end
    object cdsRecordF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ID'
      FieldName = 'F_STATUS_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_EVALUATION: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_EVALUATION'
      Required = True
    end
    object cdsRecordF_CERTIFICATE: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_CERTIFICATE'
    end
    object cdsRecordF_COMMENT: TStringField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_COMMENT'
      Size = 128
    end
    object cdsRecordF_ENROLMENT_DATE: TDateTimeField
      DisplayLabel = 'Inschrijvingsdatum'
      FieldName = 'F_ENROLMENT_DATE'
      Required = True
    end
    object cdsRecordF_INVOICED: TBooleanField
      DisplayLabel = 'Gefactureerd'
      FieldName = 'F_INVOICED'
      Required = True
    end
    object cdsRecordF_LAST_MINUTE: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_LAST_MINUTE'
      Required = True
    end
    object cdsRecordF_HOURS: TIntegerField
      DisplayLabel = 'Uren Gepland'
      FieldName = 'F_HOURS'
    end
    object cdsRecordF_TOTAL_SESSION_HOURS: TSmallintField
      FieldName = 'F_TOTAL_SESSION_HOURS'
      ProviderFlags = []
      Required = True
    end
    object cdsRecordF_HOURS_PRESENT: TSmallintField
      DisplayLabel = 'Uren Aanwezig'
      FieldName = 'F_HOURS_PRESENT'
    end
    object cdsRecordF_HOURS_ABSENT_JUSTIFIED: TSmallintField
      DisplayLabel = 'Uren Afwezig ( Gewettigd )'
      FieldName = 'F_HOURS_ABSENT_JUSTIFIED'
    end
    object cdsRecordF_HOURS_ABSENT_ILLEGIT: TSmallintField
      DisplayLabel = 'Uren Afwezig ( Ongewettigd )'
      FieldName = 'F_HOURS_ABSENT_ILLEGIT'
    end
    object cdsRecordF_FVB_FILE_ID: TIntegerField
      DisplayLabel = 'FVB ID'
      FieldName = 'F_FVB_FILE_ID'
      Visible = False
    end
    object cdsRecordF_PRICING_SCHEME: TIntegerField
      DisplayLabel = 'Tussenkomst'
      FieldName = 'F_PRICING_SCHEME'
      Required = True
    end
    object cdsRecordF_ROLE_ID: TIntegerField
      DisplayLabel = 'Rol (ID)'
      FieldName = 'F_ROLE_ID'
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Bedrijf'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsRecordF_PRICE_WORKER: TFloatField
      DisplayLabel = 'Sessieprijs (arbeider)'
      FieldName = 'F_PRICE_WORKER'
    end
    object cdsRecordF_PRICE_CLERK: TFloatField
      DisplayLabel = 'Sessieprijs (bediende)'
      FieldName = 'F_PRICE_CLERK'
    end
    object cdsRecordF_PRICE_OTHER: TFloatField
      DisplayLabel = 'Sessieprijs (overige)'
      FieldName = 'F_PRICE_OTHER'
    end
    object cdsRecordF_CERTIFICATE_PRINT_DATE: TDateTimeField
      FieldName = 'F_CERTIFICATE_PRINT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SELECTED_PRICE: TFloatField
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = []
    end
    object cdsRecordF_ROLEGROUP_ID: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'F_ROLEGROUP_ID'
      ProviderFlags = []
    end
    object cdsRecordF_PARTNER_WINTER_ID: TIntegerField
      FieldName = 'F_PARTNER_WINTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PARTNER_WINTER_NAME: TStringField
      FieldName = 'F_PARTNER_WINTER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordF_SCORE: TIntegerField
      FieldName = 'F_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_MAX_SCORE: TIntegerField
      FieldName = 'F_MAX_SCORE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_MAX_PUNTEN: TIntegerField
      FieldName = 'F_MAX_PUNTEN'
      ProviderFlags = []
    end
    object cdsRecordF_PROGRAM_TYPE: TIntegerField
      FieldName = 'F_PROGRAM_TYPE'
      ProviderFlags = []
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    Top = 168
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmEnrolment'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmEnrolment'
  end
  object cdsSesSession: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvSES_SESSION'
    RemoteServer = ssckData
    AutoOpen = False
    Left = 274
    Top = 120
  end
  object cdsMoveToSession: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
        Value = 0
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@EnrollmnentID'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@NewSessionID'
        ParamType = ptInput
        Value = 0
      end>
    ProviderName = 'prvMoveToSession'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 72
    Top = 208
    object IntegerField1: TIntegerField
      DisplayLabel = 'Inschrijving ( ID )'
      FieldName = 'F_ENROL_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object IntegerField2: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object StringField1: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object IntegerField3: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object StringField2: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object StringField3: TStringField
      DisplayLabel = 'Rijksreg. NR'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object IntegerField4: TIntegerField
      DisplayLabel = 'Status ID'
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object StringField4: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object BooleanField1: TBooleanField
      DisplayLabel = 'Evaluatie'
      FieldName = 'F_EVALUATION'
      ProviderFlags = [pfInUpdate]
    end
    object BooleanField2: TBooleanField
      DisplayLabel = 'Certificaat'
      FieldName = 'F_CERTIFICATE'
      ProviderFlags = [pfInUpdate]
    end
    object BooleanField3: TBooleanField
      DisplayLabel = 'Gefactureerd'
      FieldName = 'F_INVOICED'
      ProviderFlags = [pfInUpdate]
    end
    object BooleanField4: TBooleanField
      DisplayLabel = 'Last Minute'
      FieldName = 'F_LAST_MINUTE'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField5: TIntegerField
      DisplayLabel = 'Geplande uren'
      FieldName = 'F_HOURS'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object SmallintField1: TSmallintField
      FieldName = 'F_TOTAL_SESSION_HOURS'
      ProviderFlags = []
      Required = True
    end
    object SmallintField2: TSmallintField
      DisplayLabel = 'Uren aanwezig'
      FieldName = 'F_HOURS_PRESENT'
      ProviderFlags = [pfInUpdate]
    end
    object SmallintField3: TSmallintField
      DisplayLabel = 'Uren gewettigd afwezig'
      FieldName = 'F_HOURS_ABSENT_JUSTIFIED'
      ProviderFlags = [pfInUpdate]
    end
    object SmallintField4: TSmallintField
      DisplayLabel = 'Uren onwettig afwezig'
      FieldName = 'F_HOURS_ABSENT_ILLEGIT'
      ProviderFlags = [pfInUpdate]
    end
    object IntegerField6: TIntegerField
      FieldName = 'F_PRICING_SCHEME'
    end
    object IntegerField7: TIntegerField
      DisplayLabel = 'Rol (ID)'
      FieldName = 'F_ROLE_ID'
    end
    object StringField5: TStringField
      DisplayLabel = 'Bedrijf'
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object StringField6: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object FloatField1: TFloatField
      DisplayLabel = 'Sessieprijs (arbeider)'
      FieldName = 'F_PRICE_WORKER'
      ProviderFlags = [pfInUpdate]
    end
    object FloatField2: TFloatField
      DisplayLabel = 'Sessieprijs (bediende)'
      FieldName = 'F_PRICE_CLERK'
      ProviderFlags = [pfInUpdate]
    end
    object FloatField3: TFloatField
      DisplayLabel = 'Sessieprijs (overige)'
      FieldName = 'F_PRICE_OTHER'
      ProviderFlags = [pfInUpdate]
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'F_CERTIFICATE_PRINT_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object FloatField4: TFloatField
      DisplayLabel = 'Prijs ('#8364')'
      FieldName = 'F_SELECTED_PRICE'
      ProviderFlags = [pfInUpdate]
    end
  end
end
