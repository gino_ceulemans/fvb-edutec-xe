inherited dtmCooperation: TdtmCooperation
  KeyFields = 'F_COOPERATION_ID'
  ListViewClass = 'TfrmCooperation_List'
  RecordViewClass = 'TfrmCooperation_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_COOPERATION_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COOPERATION_NAME '
      'FROM '
      '  V_GE_COOPERATION')
    object qryListF_COOPERATION_ID: TIntegerField
      FieldName = 'F_COOPERATION_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryListF_COOPERATION_NAME: TStringField
      FieldName = 'F_COOPERATION_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_COOPERATION_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COOPERATION_NAME '
      'FROM '
      '  V_GE_COOPERATION')
    object qryRecordF_COOPERATION_ID: TIntegerField
      FieldName = 'F_COOPERATION_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryRecordF_COOPERATION_NAME: TStringField
      FieldName = 'F_COOPERATION_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_COOPERATION_ID: TIntegerField
      DisplayLabel = 'In samenwerking met ( ID )'
      FieldName = 'F_COOPERATION_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'In samenwerking met ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'In samenwerking met ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 64
    end
    object cdsListF_COOPERATION_NAME: TStringField
      DisplayLabel = 'In samenwerking met'
      FieldName = 'F_COOPERATION_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_COOPERATION_ID: TIntegerField
      DisplayLabel = 'In samenwerking met ( ID )'
      FieldName = 'F_COOPERATION_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'In samenwerking met ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'In samenwerking met ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_COOPERATION_NAME: TStringField
      DisplayLabel = 'In samenwerking met'
      FieldName = 'F_COOPERATION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmCooperation'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmCooperation'
  end
end
