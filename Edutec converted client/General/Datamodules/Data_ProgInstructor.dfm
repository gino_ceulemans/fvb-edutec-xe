inherited dtmProgInstructor: TdtmProgInstructor
  AutoOpenDataSets = False
  KeyFields = 'F_PROG_INSTRUCTOR_ID'
  ListViewClass = 'TfrmProgInstructor_List'
  RecordViewClass = 'TfrmProgInstructor_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROG_INSTRUCTOR_ID, '
      '  F_INSTRUCTOR_ID, '
      '  F_FULLNAME,'
      '  F_LASTNAME,'
      '  F_FIRSTNAME,'
      '  F_SOCSEC_NR, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_PROGRAM_ID, '
      '  F_NAME, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM '
      '  V_PROG_INSTRUCTOR')
    object qryListF_PROG_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_PROG_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryListF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 129
    end
    object qryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object qryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object qryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object qryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object qryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object qryListF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROG_INSTRUCTOR_ID, '
      '  F_INSTRUCTOR_ID, '
      '  F_FULLNAME,'
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_SOCSEC_NR, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_PROGRAM_ID, '
      '  F_NAME, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_ACTIVE'
      'FROM '
      '  V_PROG_INSTRUCTOR')
    object qryRecordF_PROG_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_PROG_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 129
    end
    object qryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = []
      Size = 11
    end
    object qryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = []
    end
    object qryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = []
    end
    object qryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = []
    end
    object qryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Size = 128
    end
    object qryRecordF_PROGRAM_ID: TIntegerField
      FieldName = 'F_PROGRAM_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_PROG_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_PROG_INSTRUCTOR_ID'
      Visible = False
    end
    object cdsListF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever ( ID )'
      FieldName = 'F_INSTRUCTOR_ID'
    end
    object cdsListF_FULLNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_FULLNAME'
      Visible = False
      Size = 129
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksregisternr.'
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsListF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsListF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsListF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
    end
    object cdsListF_EMAIL: TStringField
      DisplayLabel = 'Email'
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsListF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
      Visible = False
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Programma'
      FieldName = 'F_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_PROG_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_PROG_INSTRUCTOR_ID'
      Required = True
    end
    object cdsRecordF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever ( ID )'
      FieldName = 'F_INSTRUCTOR_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_FULLNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_FULLNAME'
      Required = True
      Size = 129
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksregisternr.'
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsRecordF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsRecordF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsRecordF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'Email'
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsRecordF_PROGRAM_ID: TIntegerField
      DisplayLabel = 'Programma ( ID )'
      FieldName = 'F_PROGRAM_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      Required = True
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmProgInstructor'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmProgInstructor'
  end
end
