inherited dtmSesExpenses: TdtmSesExpenses
  KeyFields = 'F_EXPENSE_ID'
  ListViewClass = 'TfrmSesExpenses_List'
  RecordViewClass = 'TfrmSesExpenses_Record'
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_EXPENSE_ID, '
      '  F_SESSION_ID, '
      '  F_SESSION_NAME, '
      '  F_EXPENSE_TYPE, '
      '  F_EXPENSE_DESC, '
      '  F_EXPENSE_AMOUNT '
      'FROM '
      '  V_SES_EXPENSE')
    object qryListF_EXPENSE_ID: TIntegerField
      FieldName = 'F_EXPENSE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_EXPENSE_TYPE: TIntegerField
      FieldName = 'F_EXPENSE_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_EXPENSE_DESC: TMemoField
      FieldName = 'F_EXPENSE_DESC'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object qryListF_EXPENSE_AMOUNT: TFloatField
      FieldName = 'F_EXPENSE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_EXPENSE_ID, '
      '  F_SESSION_ID, '
      '  F_SESSION_NAME, '
      '  F_EXPENSE_TYPE, '
      '  F_EXPENSE_DESC, '
      '  F_EXPENSE_AMOUNT '
      'FROM '
      '  V_SES_EXPENSE')
    object qryRecordF_EXPENSE_ID: TIntegerField
      FieldName = 'F_EXPENSE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_SESSION_NAME: TStringField
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_EXPENSE_TYPE: TIntegerField
      FieldName = 'F_EXPENSE_TYPE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_EXPENSE_DESC: TMemoField
      FieldName = 'F_EXPENSE_DESC'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object qryRecordF_EXPENSE_AMOUNT: TFloatField
      FieldName = 'F_EXPENSE_AMOUNT'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_EXPENSE_ID: TIntegerField
      DisplayLabel = 'Uitgave ( ID )'
      FieldName = 'F_EXPENSE_ID'
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
    end
    object cdsListF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      Size = 64
    end
    object cdsListF_EXPENSE_TYPE: TIntegerField
      Alignment = taLeftJustify
      DisplayLabel = 'Oorsprong'
      FieldName = 'F_EXPENSE_TYPE'
    end
    object cdsListF_EXPENSE_DESC: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_EXPENSE_DESC'
      BlobType = ftMemo
    end
    object cdsListF_EXPENSE_AMOUNT: TFloatField
      DisplayLabel = 'Bedrag'
      FieldName = 'F_EXPENSE_AMOUNT'
    end
    object cdsListF_EXPENSE_DATE: TDateTimeField
      FieldName = 'F_EXPENSE_DATE'
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_EXPENSE_ID: TIntegerField
      DisplayLabel = 'Uitgave ( ID )'
      FieldName = 'F_EXPENSE_ID'
      Required = True
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      Required = True
    end
    object cdsRecordF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_EXPENSE_TYPE: TIntegerField
      Alignment = taLeftJustify
      DisplayLabel = 'Oorsprong'
      FieldName = 'F_EXPENSE_TYPE'
      Required = True
    end
    object cdsRecordF_EXPENSE_DESC: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_EXPENSE_DESC'
      BlobType = ftMemo
    end
    object cdsRecordF_EXPENSE_AMOUNT: TFloatField
      DisplayLabel = 'Bedrag'
      FieldName = 'F_EXPENSE_AMOUNT'
      Required = True
    end
    object cdsRecordF_EXPENSE_DATE: TDateTimeField
      FieldName = 'F_EXPENSE_DATE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmSesExpenses'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TdtmRdtmSesExpenses'
  end
end
