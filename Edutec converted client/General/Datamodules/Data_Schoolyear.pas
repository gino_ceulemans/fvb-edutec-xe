{*****************************************************************************
  This DataModule will be used for the Maintenance of T_GE_SCHOOLYEAR records.

  @Name       Data_Schoolyear
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/10/2005   sLesage              Fixed Display Labels of some fields.
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Schoolyear;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmSchoolyear = class(TEduDataModule, IEDUSchoolyearDataModule )
    qryListF_SCHOOLYEAR_ID: TIntegerField;
    qryListF_SCHOOLYEAR: TStringField;
    qryListF_START_DATE: TDateTimeField;
    qryListF_END_DATE: TDateTimeField;
    qryListF_ACTIVE: TBooleanField;
    qryRecordF_SCHOOLYEAR_ID: TIntegerField;
    qryRecordF_SCHOOLYEAR: TStringField;
    qryRecordF_START_DATE: TDateTimeField;
    qryRecordF_END_DATE: TDateTimeField;
    qryRecordF_ACTIVE: TBooleanField;
    cdsListF_SCHOOLYEAR_ID: TIntegerField;
    cdsListF_SCHOOLYEAR: TStringField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_ACTIVE: TBooleanField;
    cdsRecordF_SCHOOLYEAR_ID: TIntegerField;
    cdsRecordF_SCHOOLYEAR: TStringField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_ACTIVE: TBooleanField;
    procedure prvRecordGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    class procedure SelectSchoolyear( aDataSet : TDataSet; aWhereClause : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault; aShowInactive : boolean = False  ); virtual;
    class procedure ShowSchoolyear  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

var
  dtmSchoolyear: TdtmSchoolyear;

implementation

uses Data_EduMainClient;

{$R *.dfm}
procedure CopySchoolyearFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_SCHOOLYEAR_ID' ).AsInteger :=
        aSource.FieldByName( 'F_SCHOOLYEAR_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      critDefault :
      begin
        aDestination.FieldByName( 'F_SCHOOLYEAR' ).AsString :=
          aSource.FieldByName( 'F_SCHOOLYEAR' ).AsString;
      end;
    end;
  end;
end;

procedure TdtmSchoolyear.prvRecordGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_GE_SCHOOLYEAR';
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmSchoolyear.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmSchoolyear.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_SCHOOLYEAR_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

procedure TdtmSchoolyear.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
end;

class procedure TdtmSchoolyear.SelectSchoolyear( aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False  );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSchoolyear', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopySchoolyearFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

class procedure TdtmSchoolyear.ShowSchoolyear(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmSchoolyear', 'F_SCHOOLYEAR_ID = ' + aDataSet.FieldByName( 'F_SCHOOLYEAR_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopySchoolyearFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

end.