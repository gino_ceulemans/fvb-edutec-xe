{*****************************************************************************
  This DataModule will be used for the maintenance of T_PROGRAM_TYPE.
  
  @Name       Data_ProgramType
  @Author     slesage
  @Copyright  (c) 2017 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  19/04/2017   gceulemans           Initial creation of the Unit.
******************************************************************************}

unit Data_ProgramType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, Unit_FVBFFCDBComponents, MConnect, unit_EdutecInterfaces,
  Datasnap.DSConnect;

type
  TdtmProgramType = class(TEDUDataModule, IEDUProgramTypeDataModule)
    cdsRecordF_PROGRAMTYPE_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_PROGRAMTYPE_NAME: TStringField;
    cdsListF_PROGRAMTYPE_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_PROGRAMTYPE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectProgramtype( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    class procedure ShowProgramtype  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

uses 
  Data_EDUMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Language related fields from one
  DataSet to another DataSet.

  @Name       CopyLanguageFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyProgramtypeFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    case aCopyTo of
      critDefault :
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_PROGRAM_TYPE' ).AsInteger :=
            aSource.FieldByName( 'F_PROGRAMTYPE_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_PROGRAM_TYPE_NAME' ).AsString :=
          aSource.FieldByName( 'F_PROGRAMTYPE_NAME' ).AsString;
      end;
    end;
  end;
end;

procedure TdtmProgramType.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_PROGRAM_TYPE';
end;

{*****************************************************************************
  This class procedure will be used to select one or more Language Records.

  @Name       TdtmLanguage.SelectLanguage
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgramType.SelectProgramtype( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the CountryPart ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmProgramType', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyProgramtypeFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Language
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmLanguage.ShowLanguage
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgramType.ShowProgramtype(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView;
  aCopyTo : TCopyRecordInformationTo = critDefault);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView for TdtmPerson.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmProgramType', 'F_PROGRAMTYPE_ID = ' + aDataSet.FieldByName( 'F_PROGRAMTYPE_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyProgramtypeFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmLanguage.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgramType.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PROGRAMTYPE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

end.
