{*****************************************************************************
  This DataModule will be used for the Maintenance of T_RO_GROUP records.

  @Name       Data_RoleGroup
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_RoleGroup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, DB, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, ADODB, Unit_PPWFrameWorkClasses,
  MConnect, Unit_PPWFrameWorkActions, Datasnap.DSConnect;

type
  TdtmRoleGroup = class(TEDUDataModule)
    qryListF_ROLEGROUP_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_ROLEGROUP_NAME: TStringField;
    cdsListF_ROLEGROUP_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_ROLEGROUP_NAME: TStringField;
    cdsRecordF_ROLEGROUP_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_ROLEGROUP_NAME: TStringField;
    qryRecordF_ROLEGROUP_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_ROLEGROUP_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }

  protected
    function IsListViewActionAllowed(aAction: TPPWFrameWorkListViewAction): Boolean; override;

  public
    { Public declarations }
    class procedure SelectRoleGroup( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowRoleGroup  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
  end;

var
  dtmRoleGroup: TdtmRoleGroup;

implementation

uses Data_EduMainClient;

{$R *.dfm}

procedure TdtmRoleGroup.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_RO_GROUP';
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmRoleGroup.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmRoleGroup.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ROLEGROUP_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This procedure will be used to copy RoleGroup related fields from one
  DataSet to another DataSet.

  @Name       CopyRoleGroupFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyRoleGroupFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_ROLEGROUP_ID' ).AsInteger :=
        aSource.FieldByName( 'F_ROLEGROUP_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      critDefault :
      { By Default copy these Fields }
//      else
      begin
        aDestination.FieldByName( 'F_ROLEGROUP_NAME' ).AsString :=
          aSource.FieldByName( 'F_ROLEGROUP_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more RoleGroup Records.

  @Name       TdtmRoleGroup.SelectRoleGroup
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmRoleGroup.SelectRoleGroup(aDataSet: TDataSet;
  aWhere: String; aMultiSelect: Boolean;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmRoleGroup', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyRoleGroupFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single RoleGroup
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmRoleGroup.ShowRoleGroup
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmRoleGroup.ShowRoleGroup(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmRoleGroup', 'F_ROLEGROUP_ID = ' + aDataSet.FieldByName( 'F_ROLEGROUP_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyRoleGroupFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  Name           : IsListViewActionAllowed
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : We just rearranged the rolegroups. Because business logic is
                   linked to each rolegroup, we now no longer allow rolegroups to be
                   modified or inserted manually.

  History        :

  Date         By                   Description
  ----         --                   -----------
  06/08/2007   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TdtmRoleGroup.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
var
  aAllowed: Boolean;
begin

  aAllowed := Inherited IsListViewActionAllowed( aAction );

  if (aAction is TPPWFrameWorkListViewDeleteAction) or
     (aAction is TPPWFrameWorkListViewAddAction) or
     (aAction is TPPWFrameWorkListViewEditAction) then
  begin
    aAllowed := false;
  end;
  
  Result := aAllowed;
end;


end.