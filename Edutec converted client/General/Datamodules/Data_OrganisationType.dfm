inherited dtmOrganisationType: TdtmOrganisationType
  KeyFields = 'F_ORGTYPE_ID'
  ListViewClass = 'TfrmOrganisationType_List'
  RecordViewClass = 'TfrmOrganisationType_Record'
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ORGTYPE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ORGTYPE_NAME '
      'FROM '
      '  V_OR_TYPE')
    object qryListF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object qryListF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ORGTYPE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ORGTYPE_NAME '
      'FROM '
      '  V_OR_TYPE')
    object qryRecordF_ORGTYPE_ID: TIntegerField
      FieldName = 'F_ORGTYPE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 40
    end
    object qryRecordF_ORGTYPE_NAME: TStringField
      FieldName = 'F_ORGTYPE_NAME'
      ProviderFlags = []
      Size = 40
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_ORGTYPE_ID: TIntegerField
      DisplayLabel = 'Organisatie Type ( ID )'
      FieldName = 'F_ORGTYPE_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Organisatie Type ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 40
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Organisatie Type ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 40
    end
    object cdsListF_ORGTYPE_NAME: TStringField
      DisplayLabel = 'Organisatie Type'
      FieldName = 'F_ORGTYPE_NAME'
      Size = 40
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ORGTYPE_ID: TIntegerField
      DisplayLabel = 'Organisatie Type ( ID )'
      FieldName = 'F_ORGTYPE_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Organisatie Type ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 40
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Organisatie Type ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 40
    end
    object cdsRecordF_ORGTYPE_NAME: TStringField
      DisplayLabel = 'Organisatie Type'
      FieldName = 'F_ORGTYPE_NAME'
      Visible = False
      Size = 40
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmOrganisationType'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmOrganisationType'
  end
end
