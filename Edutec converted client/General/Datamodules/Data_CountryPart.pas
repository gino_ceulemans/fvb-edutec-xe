{*****************************************************************************
  This unit contains the DataModule that will be used for the maintenance of
  T_GE_COUNTRY_PART records.
  
  @Name       Data_CountryPart
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_CountryPart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect,
  Data.FMTBcd, Data.SqlExpr;

type
  TdtmCountryPart = class(TEduDataModule, IEDUCountryPartDataModule)
    qryListF_COUNTRYPART_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_COUNTRY_ID: TIntegerField;
    qryRecordF_COUNTRYPART_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_COUNTRY_ID: TIntegerField;
    cdsListF_COUNTRYPART_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_COUNTRY_ID: TIntegerField;
    cdsRecordF_COUNTRYPART_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_COUNTRY_ID: TIntegerField;
    qryListF_COUNTRY_NAME: TStringField;
    qryRecordF_COUNTRY_NAME: TStringField;
    cdsListF_COUNTRY_NAME: TStringField;
    cdsRecordF_COUNTRY_NAME: TStringField;
    qryListF_COUNTRY_PART_NAME: TStringField;
    qryRecordF_COUNTRY_PART_NAME: TStringField;
    cdsListF_COUNTRY_PART_NAME: TStringField;
    cdsRecordF_COUNTRY_PART_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectCountry( aDataSet : TDataSet ); virtual;
    procedure ShowCountry  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectCountryPart( aDataSet : TDataSet ); virtual;
    class procedure ShowCountryPart  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  end;

var
  dtmCountryPart: TdtmCountryPart;

implementation

uses Data_Country, Data_EDUMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Country Part related fields from one
  DataSet to another DataSet.

  @Name       CopyCountryPartFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyCountryPartFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_COUNTRYPART_ID' ).AsInteger :=
        aSource.FieldByName( 'F_COUNTRYPART_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    aDestination.FieldByName( 'F_COUNTRY_PART_NAME' ).AsString :=
      aSource.FieldByName( 'F_COUNTRY_PART_NAME' ).AsString;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Country Records.

  @Name       TdtmCountryPart.SelectCountry
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCountryPart.SelectCountry(aDataSet: TDataSet);
begin
  TdtmCountry.SelectCountry( aDataSet );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Country Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmCountryPart.ShowCountry
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCountryPart.ShowCountry(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCountry.ShowCountry( aDataSet, aRecordViewMode );
  end;
end;

procedure TdtmCountryPart.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_GE_COUNTRY_PART';
end;

{*****************************************************************************
  This class procedure will be used to select one or more CountryPart Records.

  @Name       TdtmCountryPart.SelectCountryPart
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmCountryPart.SelectCountryPart(aDataSet: TDataSet);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the CountryPart ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmCountryPart', {'PE_IS_MILITANT = 1'} '' , aSelectedRecords, False ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyCountryPartFieldValues( aSelectedRecords, aDataSet, True );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single CountryPart
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmCountryPart.ShowCountryPart
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmCountryPart.ShowCountryPart(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView for TdtmCountryPart.  Make sure it is filtered on
      the correct F_CountryPart_Id, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmCountryPart', 'F_COUNTRYPART_ID = ' + aDataSet.FieldByName( 'F_COUNTRYPART_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyCountryPartFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ) );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmCountryPart.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCountryPart.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataset.FieldByName( 'F_COUNTRYPART_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmCountryPart.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCountryPart.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  
  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUCountryDataModule ) ) then
    begin
      CopyCountryFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critDefault );
    end;
  end;
end;

end.
