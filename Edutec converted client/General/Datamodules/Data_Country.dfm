inherited dtmCountry: TdtmCountry
  KeyFields = 'F_COUNTRY_ID'
  ListViewClass = 'TfrmCountry_List'
  RecordViewClass = 'TfrmCountry_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Landsgedeeltes'
      ForeignKeys = 'F_COUNTRY_ID'
      DataModuleClass = 'TdtmCountryPart'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_COUNTRY_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ISO_CODE, '
      '  F_COUNTRY_NAME'
      'FROM '
      '  V_GE_COUNTRY')
    object qryListF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_ISO_CODE: TStringField
      FieldName = 'F_ISO_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 3
    end
    object qryListF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_COUNTRY_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_ISO_CODE, '
      '  F_COUNTRY_NAME'
      'FROM '
      '  V_GE_COUNTRY')
    object qryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_ISO_CODE: TStringField
      FieldName = 'F_ISO_CODE'
      ProviderFlags = [pfInUpdate]
      Size = 3
    end
    object qryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Land ( NL )'
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Land ( FR )'
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_ISO_CODE: TStringField
      DisplayLabel = 'ISO Code'
      FieldName = 'F_ISO_CODE'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 3
    end
    object cdsListF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ID'
      FieldName = 'F_COUNTRY_ID'
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Land ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Land ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_ISO_CODE: TStringField
      DisplayLabel = 'ISO Code'
      FieldName = 'F_ISO_CODE'
      Required = True
      Size = 3
    end
    object cdsRecordF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmCountry'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmCountry'
  end
end
