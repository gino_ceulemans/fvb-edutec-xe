unit Data_Languages;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_PhoenixBase, Data_EduDataModule,
  Unit_PPWFrameWorkComponents, Unit_EdutecDBComponents, Provider, DB,
  ADODB, unit_PPWFrameWorkADO, Datasnap.DSConnect, Datasnap.Win.MConnect,
  Unit_FVBFFCDBComponents;

type
  TdtmLanguages = class(TEduDataModule)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dtmLanguages: TdtmLanguages;

implementation

{$R *.dfm}

end.