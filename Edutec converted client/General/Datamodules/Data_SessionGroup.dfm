inherited dtmSessionGroup: TdtmSessionGroup
  KeyFields = 'F_SESSIONGROUP_ID'
  ListViewClass = 'TfrmSessionGroup_List'
  RecordViewClass = 'TfrmSessionGroup_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Sessies'
      ForeignKeys = 'F_SESSIONGROUP_ID'
      DataModuleClass = 'TdtmSesSession'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_SESSIONGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_SESSIONGROUP_NAME '
      'FROM'
      '  V_SES_GROUP')
    object qryListF_SESSIONGROUP_ID: TIntegerField
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_SESSIONGROUP_NAME: TStringField
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_SESSIONGROUP_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_SESSIONGROUP_NAME '
      'FROM'
      '  V_SES_GROUP')
    object qryRecordF_SESSIONGROUP_ID: TIntegerField
      FieldName = 'F_SESSIONGROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_SESSIONGROUP_NAME: TStringField
      FieldName = 'F_SESSIONGROUP_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_SESSIONGROUP_ID: TIntegerField
      DisplayLabel = 'Sessie Groep ( ID )'
      FieldName = 'F_SESSIONGROUP_ID'
      Required = True
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Sessie Groep ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Sessie Groep ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_SESSIONGROUP_NAME: TStringField
      DisplayLabel = 'Sessie Groep'
      FieldName = 'F_SESSIONGROUP_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_SESSIONGROUP_ID: TIntegerField
      DisplayLabel = 'Sessie Groep ( ID )'
      FieldName = 'F_SESSIONGROUP_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Sessie Groep ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Sessie Groep ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_SESSIONGROUP_NAME: TStringField
      DisplayLabel = 'Sessie Groep'
      FieldName = 'F_SESSIONGROUP_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmSessionGroup'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmSessionGroup'
  end
end
