inherited dtmCountryPart: TdtmCountryPart
  KeyFields = 'F_COUNTRYPART_ID'
  ListViewClass = 'TfrmCountryPart_List'
  RecordViewClass = 'TfrmCountryPart_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Provinces'
      ForeignKeys = 'F_COUNTRYPART_ID'
      DataModuleClass = 'TdtmProvince'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_COUNTRYPART_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME,'
      '  F_COUNTRY_PART_NAME'
      'FROM         '
      '  V_GE_COUNTRY_PART')
    object qryListF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryListF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object qryListF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      '  F_COUNTRYPART_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME,'
      '  F_COUNTRY_PART_NAME'
      'FROM         '
      '  V_GE_COUNTRY_PART')
    object qryRecordF_COUNTRYPART_ID: TIntegerField
      FieldName = 'F_COUNTRYPART_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object qryRecordF_COUNTRY_PART_NAME: TStringField
      FieldName = 'F_COUNTRY_PART_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_COUNTRYPART_ID: TIntegerField
      DisplayLabel = 'Landsgedeelte ( ID )'
      FieldName = 'F_COUNTRYPART_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Landsgedeelte ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Landsgedeelte ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 64
    end
    object cdsListF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      Required = True
      Visible = False
    end
    object cdsListF_COUNTRY_PART_NAME: TStringField
      DisplayLabel = 'Landsgedeelte'
      FieldName = 'F_COUNTRY_PART_NAME'
      Size = 64
    end
    object cdsListF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Required = True
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_COUNTRYPART_ID: TIntegerField
      DisplayLabel = 'Landsgedeelte ( ID )'
      FieldName = 'F_COUNTRYPART_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Landsgedeelte ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Landsgedeelte ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_COUNTRY_PART_NAME: TStringField
      DisplayLabel = 'Landsgedeelte'
      FieldName = 'F_COUNTRY_PART_NAME'
      Visible = False
      Size = 64
    end
    object cdsRecordF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Required = True
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmCountryPart'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmCountryPart'
  end
end
