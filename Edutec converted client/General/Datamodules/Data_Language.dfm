inherited dtmLanguage: TdtmLanguage
  KeyFields = 'F_LANGUAGE_ID'
  ListViewClass = 'TfrmLanguage_List'
  RecordViewClass = 'TfrmLanguage_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_LANGUAGE_ID,'
      '  F_NAME_NL,'
      '  F_NAME_FR,'
      '  F_LANGUAGE_NAME'
      'FROM'
      '  V_GE_LANGUAGE')
    object qryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_LANGUAGE_ID,'
      '  F_NAME_NL,'
      '  F_NAME_FR,'
      '  F_LANGUAGE_NAME'
      'FROM'
      '  V_GE_LANGUAGE'
      '')
    object qryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Taal ( NL )'
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Taal ( FR )'
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Taal ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Taal ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmLanguage'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmLanguage'
  end
end
