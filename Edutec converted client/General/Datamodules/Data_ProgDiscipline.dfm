inherited dtmProgDiscipline: TdtmProgDiscipline
  KeyFields = 'F_PROFESSION_ID'
  ListViewClass = 'TfrmProgDiscipline_List'
  RecordViewClass = 'TfrmProgDiscipline_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFESSION_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFESSION_NAME'
      'FROM '
      '  V_PROG_PROFESSION')
    object qryListF_PROFESSION_ID: TIntegerField
      FieldName = 'F_PROFESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryListF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_PROFESSION_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_PROFESSION_NAME'
      'FROM '
      '  V_PROG_PROFESSION')
    object qryRecordF_PROFESSION_ID: TIntegerField
      FieldName = 'F_PROFESSION_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 64
    end
    object qryRecordF_PROFESSION_NAME: TStringField
      FieldName = 'F_PROFESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_PROFESSION_ID: TIntegerField
      DisplayLabel = 'Discipline ( ID )'
      FieldName = 'F_PROFESSION_ID'
      Required = True
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Discipline ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Discipline ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Visible = False
      Size = 64
    end
    object cdsListF_PROFESSION_NAME: TStringField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_PROFESSION_ID: TIntegerField
      DisplayLabel = 'Discipline ( ID )'
      FieldName = 'F_PROFESSION_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Discipline ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Discipline ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_PROFESSION_NAME: TStringField
      DisplayLabel = 'Discipline'
      FieldName = 'F_PROFESSION_NAME'
      Visible = False
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmProgDiscipline'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmProgDiscipline'
  end
end
