{*****************************************************************************
  This DataModule will be used for Confirmation reports

  @Name       Data_ConfirmationReports
  @Author     ivdbossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/04/2008   ivdbossche           Added MarkConfrepSent
  19/07/2007   ivdbossche           Initial creation of the Unit.
******************************************************************************}

unit Data_ConfirmationReports;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Data_EDUDataModule, DB, DBClient, MConnect,
  Unit_FVBFFCDBComponents, Unit_PPWFrameWorkComponents, Provider, ADODB,
  Unit_EdutecInterfaces, Unit_PPWFrameWorkClasses, Datasnap.DSConnect;

type EconfirmationReports=class(Exception);

resourcestring
  rs_confirm_updating_contact_ORGANISATION
       = 'U heeft de contact informatie van de geselecteerde organisatie aangepast.'
       + #13#10
       + 'Wilt u deze gegevens ook op organisatieniveau bewaren zodat in de toekomst deze gegevens steeds standaard zullen voorgesteld worden?';
  rs_confirm_updating_contact_INFRASTRUCTURE
       = 'U heeft de contact informatie van de geselecteerde infrastructuur aangepast.'
       + #13#10
       + 'Wilt u deze gegevens ook op infrastructuurniveau bewaren zodat in de toekomst deze gegevens steeds standaard zullen voorgesteld worden?';
  rs_confirm_updating_contact_TEACHER
       = 'U heeft de contact informatie van de geselecteerde lesgever aangepast.'
       + #13#10
       + 'Wilt u deze gegevens ook op lesgeverniveau bewaren zodat in de toekomst deze gegevens steeds standaard zullen voorgesteld worden?';
  rs_no_recipients='Gelieve voor alle geselecteerde organisaties tenminste 1 emailadres te vermelden!';
  rs_txt_mailnotification='Onderstaand bericht werd afgeleverd aan:';

const
  TASK_INFRASTRUCTURE=1;
  TASK_SCHOOLS=2;
  TASK_COMPANIES=3;
  TASK_ORGANISATION=4;
  TASK_COMPANIES_PARTNER_WINTER=5; // Partner Winter

// Mailinglist
type TMail=record
  FContact1: String;
  FContact1Email: String;
  FContact2: String;
  FContact2Email: String;
  FPdfFile: String;
  FReportTypeID: Integer;
  FInfraAttach: String;
  FLanguage : Integer;
end;

type PMail=^TMail;

type
  TdtmConfirmationReports = class(TEDUDataModule, IEDUConfirmationReportsDataModule)
    prvRecordOrganisation: TFVBFFCDataSetProvider;
    adoqryRecordOrganisation: TFVBFFCQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField4: TStringField;
    SmallintField2: TSmallintField;
    IntegerField6: TIntegerField;
    StringField5: TStringField;
    StringField6: TStringField;
    DateTimeField2: TDateTimeField;
    prvRecordCompaniesSchools: TFVBFFCDataSetProvider;
    adoqryRecordCompaniesSchools: TFVBFFCQuery;
    adoqryRecordCompaniesSchoolsF_CONF_SUB_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_CONF_ID: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ORGANISATION: TStringField;
    adoqryRecordCompaniesSchoolsF_ORGANISATION_TYPE: TSmallintField;
    adoqryRecordCompaniesSchoolsF_NO_PARTICIPANTS: TIntegerField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT1_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_ORG_CONTACT2_EMAIL: TStringField;
    adoqryRecordCompaniesSchoolsF_CONFIRMATION_DATE: TDateTimeField;
    adoqryRecordInfrastructure: TFVBFFCQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    SmallintField1: TSmallintField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    prvRecordInfrastructure: TFVBFFCDataSetProvider;
    cdsRecordOrganisation: TFVBFFCClientDataSet;
    cdsRecordOrganisationF_CONF_SUB_ID: TIntegerField;
    cdsRecordOrganisationF_CONF_ID: TIntegerField;
    cdsRecordOrganisationF_ORGANISATION: TStringField;
    cdsRecordOrganisationF_ORGANISATION_TYPE: TSmallintField;
    cdsRecordOrganisationF_NO_PARTICIPANTS: TIntegerField;
    cdsRecordOrganisationF_ORG_CONTACT1_EMAIL: TStringField;
    cdsRecordOrganisationF_ORG_CONTACT2_EMAIL: TStringField;
    cdsRecordOrganisationF_CONFIRMATION_DATE: TDateTimeField;
    cdsRecordCompaniesSchools: TFVBFFCClientDataSet;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    StringField7: TStringField;
    SmallintField3: TSmallintField;
    IntegerField9: TIntegerField;
    StringField8: TStringField;
    StringField9: TStringField;
    DateTimeField3: TDateTimeField;
    cdsRecordInfrastructure: TFVBFFCClientDataSet;
    IntegerField10: TIntegerField;
    IntegerField11: TIntegerField;
    StringField10: TStringField;
    SmallintField4: TSmallintField;
    IntegerField12: TIntegerField;
    StringField11: TStringField;
    StringField12: TStringField;
    DateTimeField4: TDateTimeField;
    qryRecordF_CONF_ID: TAutoIncField;
    qryRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_CONF_ID: TAutoIncField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordCompaniesSchoolsF_SESSION_ID: TIntegerField;
    dtsRecord: TFVBFFCDataSource;
    cdsRecordOrganisationF_SESSION_ID: TIntegerField;
    cdsRecordInfrastructureF_SESSION_ID: TIntegerField;
    cdsRecordCompaniesSchoolsF_PRINT_CONFIRMATION: TBooleanField;
    cdsRecordInfrastructureF_PRINT_CONFIRMATION: TBooleanField;
    cdsRecordOrganisationF_PRINT_CONFIRMATION: TBooleanField;
    cdsRecordCompaniesSchoolsF_ORG_CONTACT2: TStringField;
    cdsRecordOrganisationF_ORG_CONTACT1: TStringField;
    cdsRecordOrganisationF_ORG_CONTACT2: TStringField;
    cdsRecordInfrastructureF_ORG_CONTACT2: TStringField;
    cdsRecordInfrastructureF_ORG_CONTACT1: TStringField;
    cdsRecordCompaniesSchoolsF_ORG_CONTACT1: TStringField;
    cdsRecordCompaniesSchoolsF_ORGTYPE_ID: TIntegerField;
    cdsRecordCompaniesSchoolsF_ORGANISATION_ID: TIntegerField;
    cdsRecordInfrastructureF_LOCATION: TStringField;
    cdsRecordOrganisationF_INSTRUCTOR_NAME: TStringField;
    cdsRecordOrganisationF_ORG_INSTRUCTOR_NAME: TStringField;
    cdsRecordInfrastructureF_INFRASTRUCTURE_ID: TIntegerField;
    cdsRecordOrganisationF_INSTRUCTOR_ID: TIntegerField;
    qryRecordF_EXTRA_COMPANIES: TMemoField;
    qryRecordF_EXTRA_INFRASTRUCTURE: TMemoField;
    qryRecordF_EXTRA_ORGANISATION: TMemoField;
    cdsRecordF_EXTRA_COMPANIES: TMemoField;
    cdsRecordF_EXTRA_INFRASTRUCTURE: TMemoField;
    cdsRecordF_EXTRA_ORGANISATION: TMemoField;
    cdsRecordCompaniesSchoolsF_MAIL_ATTACHMENT: TStringField;
    cdsRecordOrganisationF_MAIL_ATTACHMENT: TStringField;
    cdsAttestRegistered: TFVBFFCClientDataSet;
    cdsRecordCompaniesSchoolsF_PARTNER_WINTER_NAME: TStringField;
    cdsRecordCompaniesSchoolsF_PARTNER_WINTER_ID: TIntegerField;
    cdsRecordCompaniesSchoolsF_ISCCENABLED: TBooleanField;
    cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1: TStringField;
    cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT1_EMAIL: TStringField;
    cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2: TStringField;
    cdsRecordCompaniesSchoolsF_PARTNER_WINTER_CONTACT2_EMAIL: TStringField;
    cdsRecordCompaniesSchoolsF_LANGUAGE_ID: TIntegerField;
    cdsRecordInfrastructureF_LANGUAGE_ID: TIntegerField;
    cdsRecordOrganisationF_LANGUAGE_ID: TIntegerField;
    procedure cdsRecordAfterOpen(DataSet: TDataSet);
    procedure cdsListNewRecord(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    FSessionGroupId: Integer;
    FProgramType:Integer;
    procedure CreateConfirmations( const AMailingList,AInfraMailAttachCompSchool,AInfraMailAttachTeacher:Boolean );
    procedure CreateConfirmationsForPreview ( const AMailingList,AInfraMailAttachCompSchool,AInfraMailAttachTeacher:Boolean );
    procedure OpenDetails;
    //procedure OpenExplorer(const APath: String);
    function GetConfirmationPathCurUser: String;
    function GetProgramNameFromSessionId(ASessionId: Integer): String;
    function GetTextFromSessionId(ASessionId: Integer): String;
    function GetSessionGroupIdFromSessionId(const ASessionID: Integer): Integer;
    function GetProgramTypeFromSessionId(const ASessionID: Integer): Integer;

    function GetCodeFromSessionId(ASessionId: Integer): String;
    procedure MarkConfirmationAsPrinted(const AConf_Id,AConf_Sub_Id: Integer);

    function GetStartdateFromSessionId(ASessionId: Integer): String;
    function GetMailingSignature(ALanguage: Integer): WideString; safecall;
    function GetMailingVCASignature(ALanguage: Integer): WideString; safecall;
    function GetMailingBedrijfVCASignature(ALanguage: Integer): WideString; safecall;
    procedure ApplyChanges;
    procedure RecalcRolegroups(SessionId, TotalParticipants, OrganisationId, PartnerWinterId: Integer);
    procedure UpdateContactCompany(AOrganisationId, ANoContact: Integer; const AContactName, AContactEmail: WideString; AConfSubId: Integer; AIsCCEnabled: Integer);
    procedure UpdateContactInfrastruct(AInfrastructure_Id, ANoContact: Integer; const AContactName, AContactEmail: WideString);
    procedure UpdateContactInstructorOrg(AInstructorId, ANoContact: Integer; const AContactName, AContactEmail: WideString);
    procedure MarkConfrepSent(AConfId: Integer);

  protected
    function AttestRegistered(aDataSet: TDataSet): integer; virtual;

  public
    { Public declarations }
    class procedure ShowConfirmationRpts( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    class procedure ShowExaminerReportRpts( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

implementation

{$R *.dfm}

uses Data_EDUMainClient, Unit_Const, Unit_Global, DateUtils, ShellApi, StrUtils, Data_MailingOutlook;

{ TdtmConfirmationReports }

class procedure TdtmConfirmationReports.ShowConfirmationRpts( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
var
  aSelectedRecords: TClientDataSet;
  aRecordcount: Integer;
  SelectedSession: TStringList;
begin
  aSelectedRecords := TClientDataSet.Create(Application);
  try
    // Do we have all the information we need to create confirmation reports?
    // Validate user input
    SelectedSession := TStringList.Create;
    try
        SelectedSession.Add(aDataSet.FieldByName('F_SESSION_ID').AsString);

        // Instructors linked to session?
        aRecordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CONF_PREPARE_INITIAL_DATA_ORGANISATION', 'F_SESSION_ID',
                          'F_SESSION_ID', SelectedSession);
        if (aRecordcount = 0) then
          raise EconfirmationReports.Create(eNoInstructors);

        // Companies/Schools linked to session?
        aRecordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CONF_PREPARE_INITIAL_DATA_COMPANY', 'F_SESSION_ID',
                          'F_SESSION_ID', SelectedSession);
        if (aRecordcount = 0) then
          raise EconfirmationReports.Create(eNoEnrolments);

        // Infrastructure linked to session?
        aRecordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CONF_PREPARE_INITIAL_DATA_INFRASTRUCT', 'F_SESSION_ID',
                          'F_SESSION_ID', SelectedSession);
        if (aRecordcount = 0) then
          raise EconfirmationReports.Create(eNoInfrastructureSpecified);

    finally
        SelectedSession.Free;
    end;

    // Prepare initial confirmation reports data for given session id
    dtmEDUMainClient.PrepareConfirmationReports(aDataSet.FieldByName('F_SESSION_ID').AsString);

    { Show modal recordview for TdtmConfirmationReports }
    dtmEDUMainClient.pfcMain.ShowModalRecord('TdtmConfirmationReports', 'F_SESSION_ID = ' + aDataSet.FieldByName('F_SESSION_ID').AsString, aSelectedRecords, aRecordViewMode);

  finally
    aSelectedRecords.Free;
  end;
end;

class procedure TdtmConfirmationReports.ShowExaminerReportRpts( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
var
  aSelectedRecords: TClientDataSet;
  aRecordcount: Integer;
  SelectedSession: TStringList;
begin
  aSelectedRecords := TClientDataSet.Create(Application);
  try
    // Do we have all the information we need to create examiner reports?
    // Validate user input
    SelectedSession := TStringList.Create;
    try
        SelectedSession.Add(aDataSet.FieldByName('F_SESSION_ID').AsString);

        // Instructors linked to session?
        aRecordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CONF_PREPARE_INITIAL_DATA_ORGANISATION', 'F_SESSION_ID',
                          'F_SESSION_ID', SelectedSession);
        if (aRecordcount = 0) then
          raise EconfirmationReports.Create(eNoInstructors);

        // Companies/Schools linked to session?
        aRecordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CONF_PREPARE_INITIAL_DATA_COMPANY', 'F_SESSION_ID',
                          'F_SESSION_ID', SelectedSession);
        if (aRecordcount = 0) then
          raise EconfirmationReports.Create(eNoEnrolments);

        // Infrastructure linked to session?
        aRecordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CONF_PREPARE_INITIAL_DATA_INFRASTRUCT', 'F_SESSION_ID',
                          'F_SESSION_ID', SelectedSession);
        if (aRecordcount = 0) then
          raise EconfirmationReports.Create(eNoInfrastructureSpecified);

    finally
        SelectedSession.Free;
    end;

    // Prepare initial examiner reports data for given session id
    dtmEDUMainClient.PrepareConfirmationReports(aDataSet.FieldByName('F_SESSION_ID').AsString);

    { Show modal recordview for TdtmConfirmationReports }
    dtmEDUMainClient.pfcMain.ShowModalRecord('TdtmConfirmationReports', 'F_SESSION_ID = ' + aDataSet.FieldByName('F_SESSION_ID').AsString, aSelectedRecords, aRecordViewMode);

  finally
    aSelectedRecords.Free;
  end;
end;

procedure TdtmConfirmationReports.OpenDetails;
  procedure OpenCds(AClientDataSet: TClientDataSet);
  begin
    with AClientDataSet do begin
        DisableControls;
        try
            Close;
            MasterFields := 'F_SESSION_ID';
            MasterSource := dtsRecord;
            PacketRecords := 0;
            Open;
        finally
          EnableControls;
        end;
    end;
  end;
begin
    OpenCds(cdsRecordCompaniesSchools);
    OpenCds(cdsRecordOrganisation);
    OpenCds(cdsRecordInfrastructure);
end;

procedure TdtmConfirmationReports.cdsRecordAfterOpen(DataSet: TDataSet);
begin
  inherited;
    OpenDetails; // Open client dataset based on this
end;

procedure TdtmConfirmationReports.cdsListNewRecord(DataSet: TDataSet);
begin
  DataSet.Cancel; // Do not insert new record
end;

{*******************************************************************************************************************
  This method will create Confirmation reports depending on user selection

  @Name       TdtmConfirmationReports.CreateConfirmations
  @author     ivdbossche
  @param      AMailingList
  @return     None
  @Exception  None
  @See        None
  @History
              07/11/2008    ivdbossche    Mantis 4202 (storing pdf's in directory based on first 2 numbers of refnr)
              07/01/2008    ivdbossche    Subject mail
              28/12/2007    ivdbossche    Added mailinglist functionality
********************************************************************************************************************}
procedure TdtmConfirmationReports.CreateConfirmations( const AMailingList,AInfraMailAttachCompSchool,AInfraMailAttachTeacher:Boolean );
const
  CNO_TASKS=4;

var
  ParamNames, ParamValues, PdfFiles, CRReports, CRReportTitles: TStringList;
  TempNames, TempValues, ListConf_SUB_ID: TStringList;
  MailingList: TList;
  Mail: PMail;
  CRReportname, CRReportTitle: String;
  SessionCode, Startdate, ProgramName, Tekst, PathPdfs: String;
  SubjectMail: String;
  ci, numberReports, SessionId, taskIdx, ValueIdx, I: Integer;
  oldcursor: TCursor;
  Conf_ID: integer;
  PresenceListPrinted: Boolean;
  ReportExaminerPrinted : Boolean;

  // ****************************************************************************
  // Nested procedures
    procedure AddToMailingList(const AReportTypeID: Integer; const APdf: String; ACds: TClientDataSet);
    var
      aContact1, aContact2, aContact1Email, aContact2Email: String;
      aInfraAttach: string;
      aLanguage : integer;
    begin
      if AReportTypeID = TASK_COMPANIES_PARTNER_WINTER then
      begin
        if not ACds.FieldByName( 'F_PARTNER_WINTER_CONTACT1' ).IsNull then
          aContact1 := ACds.FieldByName('F_PARTNER_WINTER_CONTACT1').AsString;
        if not ACds.FieldByName( 'F_PARTNER_WINTER_CONTACT1_EMAIL' ).IsNull then
          aContact1Email := ACds.FieldByName('F_PARTNER_WINTER_CONTACT1_EMAIL').AsString;
        if not ACds.FieldByName( 'F_PARTNER_WINTER_CONTACT2' ).IsNull then
          aContact2 := ACds.FieldByName('F_PARTNER_WINTER_CONTACT2').AsString;
        if not ACds.FieldByName( 'F_PARTNER_WINTER_CONTACT2_EMAIL' ).IsNull then
          aContact2Email := ACds.FieldByName('F_PARTNER_WINTER_CONTACT2_EMAIL').AsString;
      end else
      begin
        if not ACds.FieldByName( 'F_ORG_CONTACT1' ).IsNull then
          aContact1 := ACds.FieldByName( 'F_ORG_CONTACT1' ).AsString;

        if not ACds.FieldByName( 'F_ORG_CONTACT2' ).IsNull then
          aContact2 := ACds.FieldByName( 'F_ORG_CONTACT2' ).AsString;

        if not ACds.FieldByName( 'F_ORG_CONTACT1_EMAIL' ).IsNull then
          aContact1Email := ACds.FieldByName( 'F_ORG_CONTACT1_EMAIL' ).AsString;

        if not ACds.FieldByName( 'F_ORG_CONTACT2_EMAIL' ).IsNull then
          aContact2Email := ACds.FieldByName( 'F_ORG_CONTACT2_EMAIL' ).AsString;

        if AReportTypeID in [TASK_ORGANISATION,TASK_SCHOOLS,TASK_COMPANIES] then
          if not ACds.FieldByName( 'F_MAIL_ATTACHMENT' ).IsNull then
            aInfraAttach := ACds.FieldByName( 'F_MAIL_ATTACHMENT' ).AsString;
      end;

      if ( aContact1Email = '' ) and ( aContact2Email = '' ) then
          raise Exception.Create( rs_no_recipients );

      if not ACds.FieldByName('F_LANGUAGE_ID').IsNull then
        aLanguage := ACds.FieldByName('F_LANGUAGE_ID').AsInteger
      else
        aLanguage := 1;

      New( Mail );
      Mail^.FContact1 := aContact1;
      Mail^.FContact1Email := AnsiReplaceStr(aContact1Email, 'mailto:', '');
      Mail^.FContact2 := aContact2;
      Mail^.FContact2Email := AnsiReplaceStr(aContact2Email, 'mailto:', '');
      Mail^.FPdfFile := IncludeTrailingPathDelimiter(PathPdfs) + APdf + '.PDF'; // Attachment
      Mail^.FReportTypeID := AReportTypeID; // School/Company/Infrastructure/Organisation instructor
      Mail^.FInfraAttach := aInfraAttach; // e-mail bijlage van de infrastructuur
      Mail^.FLanguage := aLanguage; //taal voor verzenden mail
      MailingList.Add( Mail );
    end;

    // Send Emails to all recipients
    procedure SendMailingList;
    var
      aMailOutlook: TdtmMailingOutlook;
      aPathPresenceList: String;
      I: Integer;
      aEmailPublFolder: String;
      aPathRichtlijnen, aPathReglement : String;
    begin

      // Retrieve Email public folder based on current user
      // Ivan Van den Bossche (15/03/2008)
      aEmailPublFolder := dtmEDUMainClient.GetEmailPublFolder;

      for I:=0 to MailingList.Count-1 do
      begin
        aMailOutlook := TdtmMailingOutlook.Create( nil );
        try
          Mail := MailingList.Items[I];

          if Length( Mail.FContact1Email ) > 0 then
            aMailOutlook.Recipients.Add( Mail.FContact1Email );

          if Length( Mail.FContact2Email ) > 0 then
            aMailOutlook.Recipients.Add( Mail.FContact2Email );

          // Aanwezigheidslijst
          aPathPresenceList := IncludeTrailingPathDelimiter(PathPdfs) + REP_PRESENCE_LIST + '.PDF';
          case Mail.FReportTypeID of // Presence list should be added as attachment?
            //TASK_INFRASTRUCTURE: aMailOutlook.Attachments.Add( aPathPresenceList );
            TASK_ORGANISATION: aMailOutlook.Attachments.Add( aPathPresenceList );
          end;

          // bijlage van infrastructuur
          if (Mail.FInfraAttach <> '') and FileExists(Mail.FInfraAttach) then
          begin
            case Mail.FReportTypeID of
              TASK_ORGANISATION : if AInfraMailAttachTeacher then aMailOutlook.Attachments.Add( Mail.FInfraAttach );
              TASK_SCHOOLS      : if AInfraMailAttachCompSchool then aMailOutlook.Attachments.Add( Mail.FInfraAttach );
              TASK_COMPANIES    : if AInfraMailAttachCompSchool then aMailOutlook.Attachments.Add( Mail.FInfraAttach );
            end;
          end;

          // Send message to organisation/person
          aMailOutlook.Attachments.Add( Mail.FPdfFile );
          aMailOutlook.Subject := SubjectMail; //'Bevestiging';
          //aMailOutlook.Body.Add( '');
          if FProgramType = 1 then // 1=opleiding , 2=VCA
            aMailOutlook.Body.Add( GetMailingSignature(Mail.FLanguage) )
          else
          begin
            if (Mail.FReportTypeID = TASK_SCHOOLS) or (Mail.FReportTypeID = TASK_COMPANIES) then
            begin
              aMailOutlook.Body.Add( GetMailingBedrijfVCASignature(Mail.FLanguage) );
              aPathRichtlijnen := IncludeTrailingPathDelimiter(dtmEDUMainClient.GetPathCRReports) + 'praktische richtlijnen.pdf';
              aMailOutlook.Attachments.Add( aPathRichtlijnen );  //add praktische richtlijnen
              aPathReglement := IncludeTrailingPathDelimiter(dtmEDUMainClient.GetPathCRReports) + 'huishoudelijk reglement.pdf';
              aMailOutlook.Attachments.Add( aPathReglement );  //add huishoudelijk reglement
            end
            else
              aMailOutlook.Body.Add( GetMailingVCASignature(Mail.FLanguage) );
          end;
          aMailOutlook.Send;

          // Send message without attachment to email public folder
          // Please ignore sending this msg when email public folder is blanco.
          if aEmailPublFolder <> '' then
          begin
            aMailOutlook.Attachments.Clear;
            aMailOutlook.Recipients.Clear;

            // Mention recipients of email in body
            aMailOutlook.Body.Insert(0, rs_txt_mailnotification);

            if Length( Mail.FContact1Email ) > 0 then
            begin
              aMailOutlook.Body.Insert(1, Mail.FContact1Email);
              aMailOutlook.Body.Insert(2, '');
            end;

            if Length( Mail.FContact2Email ) > 0 then
            begin
              aMailOutlook.Body.Insert(2, Mail.FContact2Email);
              aMailOutlook.Body.Insert(3, '');
            end;

            aMailOutlook.Recipients.Add(aEmailPublFolder);
            aMailOutlook.Send;
          end;

        finally
          aMailOutlook.Free;
        end;

      end;

    end;

    // At least one organisation has been selected?
    procedure ValidateSelection;
        // Nested procedure in nested procedure :-)
        function SelectedRecordInCDS(ACds: TClientDataSet): Boolean;
        var
          Bookm: TBookmarkStr;
          R: Boolean;
        begin
            R := False;
            try
              ACds.DisableControls;
              try
//GCXE                Bookm := ACds.Bookmark;
                ACds.First;
                while not ACds.Eof do begin
                  if (ACds.FieldByName('F_PRINT_CONFIRMATION').AsBoolean) then
                  begin
                      R := True;
                      break;
                  end;
                  ACds.Next;
                end;

              finally
//GCXE                  ACds.Bookmark := Bookm;
                  ACds.EnableControls;
              end;
            finally
                Result := R;
            end;
        end;
    begin
      // At least one record should be selected in order to continue exporting Crystal Reports reports to Pdf files
      if ( not SelectedRecordInCDS(cdsRecordOrganisation ))
          and ( not SelectedRecordInCDS(cdsRecordCompaniesSchools )
          and ( not SelectedRecordInCDS(cdsRecordInfrastructure ))) then
      begin
        raise EconfirmationReports.Create(eNoConfirmationSelected);
      end;

    end;

    procedure Crystal2Pdf(AReportName, AReportTitle, AOutputfile: String; AParamNames, AParamValues: TStringList; APortrait: Boolean; OnClosed: TNotifyEvent);
    begin
      dtmEDUMainClient.ExecuteCrystal( AReportName, AReportTitle, AParamNames, AParamValues, APortrait, OnClosed, AOutputfile );
    end;

    // Look for selected companies and schools...
    procedure _GetSelCompaniesSchools(AIsSchool: Boolean; AParamNames, AParamValues, AOutputFiles, CRReports, CRReportTitles : TStringList);
    var
      aPdf: String;
      aReportTypeID: Integer;
      bIsSchool, isCCEnabled: Boolean;
      fi, org_type_id: integer;
      bookmark: TBookmark;//TBookmarkStr;
      session_id, total_participants, organisation_id, partner_winter_id: Integer;
      tcrReport, tcrReportTitle: String;
      processedPartnerWinters: TStringList;
    begin
      Assert(Assigned(CRReports), 'CRReports is nil');
      Assert(Assigned(CRReportTitles), 'CRReportTitles is nil');
      Assert(Assigned(AOutputFiles), 'AOutputFiles is nil');

      processedPartnerWinters := TStringList.Create;
      try
      // Search for selected organisationtype
      cdsRecordCompaniesSchools.DisableControls;
      try
        bookmark := cdsRecordCompaniesSchools.Bookmark;
        cdsRecordCompaniesSchools.First;
        try
          while not cdsRecordCompaniesSchools.Eof do begin
            tcrReport := '';
            tcrReportTitle := '';
            aPdf := '';

            if (cdsRecordCompaniesSchools.FieldByName('F_PRINT_CONFIRMATION').AsBoolean) then
            begin
              partner_winter_id := cdsRecordCompaniesSchoolsF_PARTNER_WINTER_ID.AsInteger;
              isCCEnabled := cdsRecordCompaniesSchoolsF_ISCCENABLED.AsBoolean;
              org_type_id := cdsRecordCompaniesSchools.FieldByName('F_ORGTYPE_ID').AsInteger;
              if org_type_id = 4 then
                bIsSchool := True
              else
                bIsSchool := False;

                if AIsSchool = bIsSchool then
                begin
                  if bIsSchool then
                  begin
                    if FSessionGroupId = 4 then // Doelgroep leerlingen en leerkrachten?
                    begin
                      tcrReport := REP_CONFIRMATION_SCHOOL_STUDENTS;
                    end else
                    begin
                      if FProgramType = 1 then // 1=opleiding , 2=VCA
                        tcrReport := REP_CONFIRMATION_SCHOOL
                      else
                        tcrReport := REP_CONFIRMATION_VCA_SCHOOL;
                    end;
                    tcrReportTitle := rsTitleRptConfirmationSchool;
                  end
                  else
                  begin
                    // Handle normal company
                    if partner_winter_id = 0 then
                    begin
                      // No partner winter found
                      if FProgramType = 1 then // 1=opleiding , 2=VCA
                        tcrReport := REP_CONFIRMATION_EDUTEC_WEEK
                      else
                        tcrReport := REP_CONFIRMATION_VCA_BEDRIJF;
                      tcrReportTitle := rsTitleRptConfirmationCompany;
                    end else
                    begin
                        // We have to send letter to specific partner winter only once, check whether we've already added it for processing or not.
                        if not processedPartnerWinters.Find(IntToStr(partner_winter_id), fi) then
                        begin
                          AParamValues.Add(IntToStr(Conf_ID));
                          AParamValues.Add(cdsRecordCompaniesSchools.FieldByName('F_CONF_SUB_ID').AsString);

                          AParamNames.Add('p_conf_id');
                          AParamNames.Add('p_conf_sub_id');

                          if FProgramType = 1 then // 1=opleiding , 2=VCA
                            CRReports.Add(REP_CONFIRMATION_WORKERS_CONSTRUCTION)
                          else
                            CRReports.Add(REP_CONFIRMATION_VCA_BEDRIJF);
                          CRReportTitles.Add(rsTitleRptConfirmationPartnerWinter);

                          processedPartnerWinters.Add(IntToStr(partner_winter_id));

                          aPdf := 'Bevestiging partner winter ' + RemoveSpecialChars(cdsRecordCompaniesSchools.FieldByName('F_PARTNER_WINTER_NAME').AsString);

                          // Create name for PDF file
                          // (please note that F_ORGANISATION_ID is added to ensure that a PDF file is unique since one Company/School may have multiple organisation_id's.
                          AOutputfiles.Add( aPdf );

                          // Add to mailing list
                          AddToMailingList(TASK_COMPANIES_PARTNER_WINTER, aPdf, cdsRecordCompaniesSchools );
                          aPdf := '';
                        end;

                        // Partner winter found, handle this case
                        isCCEnabled := cdsRecordCompaniesSchoolsF_ISCCENABLED.AsBoolean;

                        if isCCEnabled then
                        begin
                          if FProgramType = 1 then // 1=opleiding , 2=VCA
                            tcrReport := REP_CONFIRMATION_WORKERS_OTHER
                          else
                            tcrReport := REP_CONFIRMATION_VCA_BEDRIJF;
                          tcrReportTitle := rsTitleRptConfirmationWinterCC;

                          //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                          // Adjust mailinglist
                          aPdf := 'Bevestiging winter cc ' + RemoveSpecialChars(cdsRecordCompaniesSchools.FieldByName('F_ORGANISATION').AsString);

                          // Create name for PDF file
                          // (please note that F_ORGANISATION_ID is added to ensure that a PDF file is unique since one Company/School may have multiple organisation_id's.
                          AOutputfiles.Add( aPdf );

                          // Add to mailing list
                          AddToMailingList(TASK_COMPANIES, aPdf, cdsRecordCompaniesSchools );
                          aPdf := '';
                        end;
                      end;
                    end;
                    if Length(tcrReport) > 0 then
                    begin
                      Assert(Length(tcrReportTitle) > 0, 'Length(tcrReportTitle) = 0');

                      CRReports.Add(tcrReport);
                      CRReportTitles.Add(tcrReportTitle);

                      AParamNames.Add('p_conf_id');
                      AParamNames.Add('p_conf_sub_id');
                      AParamValues.Add( IntToStr(Conf_ID) );
                      AParamValues.Add( cdsRecordCompaniesSchools.FieldByName('F_CONF_SUB_ID').AsString );
                    end;

                    ListConf_SUB_ID.Add( cdsRecordCompaniesSchools.FieldByName('F_CONF_SUB_ID').AsString );

                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    // Adjust mailinglist
                    if partner_winter_id = 0 then
                    begin
                      aPdf := 'Bevestiging ' + RemoveSpecialChars(cdsRecordCompaniesSchools.FieldByName('F_ORGANISATION').AsString);
                    end;

                    if bIsSchool then
                      aReportTypeID := TASK_SCHOOLS
                    else
                      aReportTypeID := TASK_COMPANIES;

                    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    if Length(aPdf) > 0 then
                    begin
                      // Create name for PDF file
                      // (please note that F_ORGANISATION_ID is added to ensure that a PDF file is unique since one Company/School may have multiple organisation_id's.
                      AOutputfiles.Add( aPdf );
                      AddToMailingList( aReportTypeID, aPdf, cdsRecordCompaniesSchools );
                    end;

                    // For companies, we need to determine number of participants per category.
                    // Stored procedure SP_CONFIRMATION_CALC_ROLEGROUPS will be called in order to insert data
                    // into table T_REP_CONFIRMATION_COMPANY_ROLES
                    if not bIsSchool then
                    begin
                      session_id := cdsRecordCompaniesSchools.FieldByName('F_SESSION_ID').AsInteger;
                      total_participants := cdsRecordCompaniesSchools.FieldByName('F_NO_PARTICIPANTS').AsInteger;
                      organisation_id := cdsRecordCompaniesSchools.FieldByName('F_ORGANISATION_ID').AsInteger;
                      partner_winter_id := cdsRecordCompaniesSchools.FieldByName('F_PARTNER_WINTER_ID').AsInteger;
                      RecalcRolegroups( session_id, total_participants, organisation_id, partner_winter_id );
                    end;
                end;
              end;
            cdsRecordCompaniesSchools.Next;
          end;
          finally
            cdsRecordCompaniesSchools.Bookmark := bookmark;
          end;
        finally
          cdsRecordCompaniesSchools.EnableControls;
        end;
      finally
        processedPartnerWinters.Free;
      end;
    end;

    // Get selected schools
    procedure GetSelSchools(AParamNames, AParamValues, AOutputfiles, CRReports, CRReportTitles : TStringList);
    begin
      Assert(Assigned(CRReports), 'CRReports is nil');
      Assert(Assigned(CRReportTitles), 'CRReportTitles is nil');

      _GetSelCompaniesSchools( True, AParamNames, AParamValues, AOutputfiles, CRReports, CRReportTitles );
    end;

    // Get selected companies
    procedure GetSelCompanies(AParamNames, AParamValues, AOutputfiles, CRReports, CRReportTitles : TStringList);
    begin
      Assert(Assigned(CRReports), 'CRReports is nil');
      Assert(Assigned(CRReportTitles), 'CRReportTitles is nil');

      // Look for selected schools and companies
      _GetSelCompaniesSchools( False, AParamNames, AParamValues, AOutputfiles, CRReports, CRReportTitles );
    end;

    // Get selected infrastructure
    procedure GetSelInfrastructure(AParamNames,AParamValues,AOutputfiles: TStringList; var ACRReportname, ACRReportTitle: String);
    var
      aPdf: String;
    begin
      assert( cdsRecordInfrastructure.RecordCount > 0);

      if FProgramType = 1 then // 1=opleiding , 2=VCA
        ACRReportname := REP_CONFIRMATION_INFRASTRUCTURE
      else
        ACRReportname := REP_CONFIRMATION_VCA_INFRASTRUCTURE;
      ACRReportTitle := rsTitleRptConfirmationInfrastructure;

      // Search for selected infrastructure
      cdsRecordInfrastructure.DisableControls;
      try
        if cdsRecordInfrastructure.FieldByName('F_PRINT_CONFIRMATION').AsBoolean then
        begin
            AParamNames.Add('p_session_id');
            AParamValues.Add(cdsRecord.FieldByName('F_SESSION_ID').AsString);

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Adjust mailinglist
            aPdf := 'Bevestiging infrastructuur ' + RemoveSpecialChars( cdsRecordInfrastructure.FieldByName('F_LOCATION').AsString);
            AddToMailingList( TASK_INFRASTRUCTURE, aPdf, cdsRecordInfrastructure );

            AOutputfiles.Add( aPdf );
            ListConf_SUB_ID.Add(cdsREcordInfrastructure.FieldByName('F_CONF_SUB_ID').AsString);
        end;
      finally
          cdsRecordInfrastructure.EnableControls;
      end;
    end;

    // Get selected organisation
    procedure GetSelOrganisations(AParamNames,AParamValues,AOutputfiles: TStringList; var ACRReportname, ACRReportTitle: String);
    var
      InstructorOrganisations: TStringList;
      organisation: String;
      Bookm: TBookmark;//TBookmarkStr;
      aPdf: String;
    begin
      assert(cdsRecordOrganisation.RecordCount > 0);

      // Store names of organisations instructors here
      // This stringlist will be used to check whether specific organisation has been already selected
      // We want only one letter for each instructor organisation
      InstructorOrganisations := TStringList.Create;
      try
          InstructorOrganisations.Sorted := True;
          InstructorOrganisations.Duplicates := dupError;

          if FProgramType = 1 then // 1=opleiding , 2=VCA
            ACRReportname := REP_CONFIRMATION_ORGANISATION
          else
            ACRReportname := REP_CONFIRMATION_VCA_EXAMINATOR;
          ACRReportTitle := rsTitleRptConfirmationOrganisation;

          // Search for selected organisation
          cdsRecordOrganisation.DisableControls;
          try
            Bookm := cdsRecordOrganisation.Bookmark;
            cdsRecordOrganisation.First;
            while not cdsRecordOrganisation.Eof do begin
                if cdsRecordOrganisation.FieldByName('F_PRINT_CONFIRMATION').AsBoolean then
                begin
                    AParamNames.Add('p_conf_id');
                    AParamNames.Add('p_conf_sub_id');

                    // Selected Instructor is a freelancer?  Create a separate confirmation report for this case
                    if (cdsRecordOrganisation.FieldByName('F_ORG_INSTRUCTOR_NAME').IsNull) or (Length(Trim(cdsRecordOrganisation.FieldByName('F_ORG_INSTRUCTOR_NAME').AsString)) = 0) then
                    begin
                      AParamValues.Add( IntToStr(Conf_ID) );
                      AParamValues.Add( cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString );

                      //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                      // Adjust mailinglist
                      aPdf := 'Bevestiging Lesgever ' + RemoveSpecialChars( cdsRecordOrganisation.FieldByName('F_INSTRUCTOR_NAME').AsString );
                      AddToMailingList( TASK_ORGANISATION, aPdf, cdsRecordOrganisation );

                      AOutputfiles.Add( aPdf );
                      ListConf_SUB_ID.Add( cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString );
                    end else begin
                        organisation := Trim( cdsRecordOrganisation.FieldByName('F_ORG_INSTRUCTOR_NAME').AsString );
                        try
                          // Place Conf SUB ID on list so it could be marked as printed later
                          ListConf_SUB_ID.Add( cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString );

                          // Try to add organisation to list
                          InstructorOrganisations.Add( organisation );

                          // and select organisation record so confirmation report will be generated...
                          AParamValues.Add(IntToStr(Conf_ID));
                          AParamValues.Add(cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString);

                          //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                          // Adjust mailinglist
                          aPdf := 'Bevestiging Lesgever ' + RemoveSpecialChars( organisation );
                          AddToMailingList( TASK_ORGANISATION, aPdf, cdsRecordOrganisation );

                          AOutputfiles.Add( aPdf );
                        except
                            // Ignore error-> organisation already selected, so leave it
                        end;
                    end;
                end;
                cdsRecordOrganisation.Next;
            end;
          finally
            cdsRecordOrganisation.Bookmark := Bookm;
            cdsRecordOrganisation.EnableControls;
          end;
      finally
          InstructorOrganisations.Free;
      end;
    end;

    procedure PrintPresenceList(const APathPdfs: String);
    var
      TempNames: TStringList;
    begin
      TempNames := TStringList.Create;
      TempValues := TStringList.Create;
      try
        TempNames.Add('p_session_id');
        TempValues.Add(cdsRecord.FieldByName('F_SESSION_ID').AsString);

        Crystal2Pdf(REP_PRESENCE_LIST, rsTitleRptPresenceList, IncludeTrailingPathDelimiter(aPathPdfs) + REP_PRESENCE_LIST + '.PDF',
                    TempNames, TempValues, True { Portrait}, nil);

        PresenceListPrinted := True;  // Presence list has been printed
      finally
        TempNames.Free;
        TempValues.Free;
      end;
    end;

  // *************** NESTED PROCEDURES END HERE *********************************
  // ****************************************************************************
begin
  oldCursor := screen.cursor;
  screen.cursor := crSQLWait;

  PresenceListPrinted := False;
  MailingList := TList.Create;
  try
      SessionId := cdsRecord.FieldByName('F_SESSION_ID').AsInteger;

      // At least one record selected?  If none, exception will be triggered
      ValidateSelection;

      // Get SessionGroup id from selected session
      FSessionGroupId := GetSessionGroupIdFromSessionId( SessionId );
      FProgramType := GetProgramTypeFromSessionId( SessionId );

      // Export Report(s) to PDF file(s)
      SessionCode := GetCodeFromSessionId( SessionId );
      SessionCode := RemoveSpecialChars( SessionCode );

      ProgramName := GetProgramNameFromSessionId( SessionId );
      Tekst := GetTextFromSessionId( SessionId );
      Startdate := GetStartdateFromSessionId( SessionId );

      // Compose subject used for all mails
      if FProgramType = 1 then // 1=opleiding , 2=VCA
        SubjectMail := Format('''%s''- ref. %s %s', [ProgramName, SessionCode, StartDate])
      else
        SubjectMail := Format('''%s''- ref. %s %s', [Tekst, SessionCode, StartDate]);

      try
        PathPdfs := IncludeTrailingPathDelimiter( GetConfirmationPathCurUser ) + 'Dossiers 20' + IncludeTrailingPathDelimiter(Format('%2.2d', [StrToInt(Copy(Trim(SessionCode),0,2))])) + SessionCode + '\bevestigingsbrieven';
      except
            raise Exception.Create('De eerste 2 karakters van de refererentie van het dossier moet bestaan uit cijfers...');
      end;

      // Record identifier
      Conf_ID := cdsRecord.FieldByName('F_CONF_ID').AsInteger;

      if not ForceDirectories(PathPdfs) then
        raise EconfirmationReports.Create( Format( eUnableToCreateDir, [PathPdfs] ) );

      // Loop through all kinds of records (Schools/Companies, Infrastructure, Organisations (instructors)
      for taskIdx:=1 to CNO_TASKS do begin
        ParamNames := TStringList.Create;
        ParamValues := TStringList.Create;
        PdfFiles := TStringList.Create;
        ListConf_SUB_ID := TStringList.Create;
        CRReports := TStringList.Create;
        CRReportTitles := TStringList.Create;

        try
          case taskIdx of
            TASK_INFRASTRUCTURE:
            begin
              GetSelInfrastructure( ParamNames,ParamValues,PdfFiles,CRReportName,CRReportTitle );
              CRReports.Add(CRReportName);
              CRReportTitles.Add(CRReportTitle);
            end;
            TASK_SCHOOLS:
            begin
              GetSelSchools( ParamNames,ParamValues, PdfFiles, CRReports, CRReportTitles );
            end;
            TASK_COMPANIES: GetSelCompanies( ParamNames, ParamValues, PdfFiles, CRReports, CRReportTitles );
            TASK_ORGANISATION:
            begin
              GetSelOrganisations( ParamNames, ParamValues, PdfFiles, CRReportName,CRReportTitle );

              numberReports := PdfFiles.Count;

              for ci := 0 to numberReports - 1 do
              begin
                CRReports.Add(CRReportName);
                CRReportTitles.Add(CRReportTitle);
              end;
            end;
          end;

          if ParamValues.Count = 1 then
          begin
            if (taskIdx = TASK_INFRASTRUCTURE) and (not PresenceListPrinted) then begin
                // At least one infrastructure was selected and Presence list hasn't been printed yet
                PrintPresenceList( PathPdfs );
            end;

            Assert(CRReports.Count = 1, 'CRReports.Count <> 1');
            Assert(CRReportTitles.Count = 1, 'CRReportTitles.Count <> 1');

            PdfFiles[0] := IncludeTrailingPathDelimiter( PathPdfs ) + PdfFiles[0] + '.PDF';

            Crystal2Pdf( CRReports[0], CRReportTitles[0], PdfFiles.Strings[0],
                        ParamNames, ParamValues, True {Portrait}, nil );

            MarkConfirmationAsPrinted(Conf_ID, StrToInt(ListConf_SUB_ID.Strings[0]) );

          end else if ParamValues.Count > 1 then
          begin
              //assert(ParamNames.Count = 2); // Number of parameters should be 2!!!

              if (taskIdx = TASK_ORGANISATION) and (not PresenceListPrinted) then begin
                // At least one organisation (instructor) was selected and Presence list hasn't been printed yet
                PrintPresenceList( PathPdfs );
              end;

              // Create for each selected record another outputfile (Schools and companies only)
              // Loop through set of 2 parameter values...  For each set, create another PDF file
              ValueIdx := 0;
              i := 0;

              while ValueIdx < ParamValues.Count-1 do begin
                TempValues := TStringList.Create;
                TempNames := TStringList.Create;
                try
                    // Add
                    TempValues.Add(ParamValues.Strings[ValueIdx]);
                    TempValues.Add(ParamValues.Strings[ValueIdx+1]);

                    TempNames.Add(ParamNames.Strings[ValueIdx]);
                    TempNames.Add(ParamNames.Strings[ValueIdx+1]);

                    PdfFiles[i] := IncludeTrailingPathDelimiter( PathPdfs ) + PdfFiles[i] + '.PDF';
                    Crystal2Pdf( CRReports[i], CRReportTitles[i], PdfFiles[i],
                                  TempNames, TempValues, True {Portrait}, nil );

                    ValueIdx := ValueIdx + 2; // Go to next pair of Parameter values
                    Inc(i);
                finally
                  TempValues.Free;
                  TempNames.Free;
                end;
              end;

              for i:=0 to ListConf_SUB_ID.Count-1 do begin
                MarkConfirmationAsPrinted( Conf_ID, StrToInt( ListConf_SUB_ID.Strings[i] ) );
              end;
          end;
        finally
            ListConf_SUB_ID.Free;
            ParamNames.Free;
            ParamValues.Free;
            PdfFiles.Free;
            CRReports.Free;
            CRReportTitles.Free;
        end;
      end;

      //All reports that have not been printed, should be marked as to be printed next time
      ssckData.AppServer.SetToBePrinted( Conf_ID );

      // Check whether all necessary confirmation reports have been sent and set F_CONFIRMATION_REPORT_SENT when possible.
      MarkConfrepSent( Conf_ID );

      // Do we need to send the confirmations reports?
      if AMailingList then
      begin
        SendMailingList;
      end
      else begin
        OpenExplorer( PathPdfs );
      end;

  finally
      { Cleanup: must free the list items as well as the list }
      try
        for I:=0 to MailingList.Count-1 do
        begin
          Mail := MailingList.Items[I];
          Dispose( Mail );
        end;

      finally
        MailingList.Free;
      end;

      Screen.Cursor := oldCursor;
  end;

end;

{*****************************************************************************
  This method will be executed when Confirmation reports needs to be exported.
  It will return the confirmation path configured for this user

  @Name       TdtmConfirmationReports.GetConfirmationPathCurUser
  @author     ivdbossche
  @param      None
  @return     Path PDF files(confirmation reports) will be stored for current user
  @Exception  None
  @See        None
******************************************************************************}
function TdtmConfirmationReports.GetConfirmationPathCurUser: String;
begin
  Result := dtmEduMainClient.GetConfirmationPathCurUser;
end;

{*****************************************************************************
  This method will be executed when Confirmation reports needs to be exported.
  It will return the confirmation path configured for this user

  @Name       TdtmConfirmationReports.GetProgramNameFromSessionId
  @author     ivdbossche
  @param      SessionId
  @return     Program name for given SessionId
  @Exception  None
  @See        None
******************************************************************************}
function TdtmConfirmationReports.GetProgramNameFromSessionId( ASessionId: Integer): String;
begin
  Result := ssckData.AppServer.GetProgramNameFromSessionId( ASessionId);
end;

function TdtmConfirmationReports.GetTextFromSessionId( ASessionId: Integer): String;
begin
  Result := ssckData.AppServer.GetTextFromSessionId( ASessionId);
end;

function TdtmConfirmationReports.GetCodeFromSessionId (ASessionId: Integer): String;
begin
  Result := ssckData.AppServer.GetCodeFromSessionId( ASessionId );
end;

function TdtmConfirmationReports.GetStartdateFromSessionId(
  ASessionId: Integer): String;
begin
  Result := ssckData.AppServer.GetStartdateFromSessionId( ASessionId );
end;

function TdtmConfirmationReports.GetSessionGroupIdFromSessionId(
  const ASessionID: Integer): Integer;
begin
  Result := ssckData.AppServer.GetSessionGroupIdFromSessionId( ASessionID );
end;

function TdtmConfirmationReports.GetProgramTypeFromSessionId(
  const ASessionID: Integer): Integer;
begin
  Result := ssckData.AppServer.GetProgramTypeFromSessionId( ASessionID );
end;

function TdtmConfirmationReports.GetMailingSignature(ALanguage: Integer): WideString;
begin
  Result := ssckData.AppServer.GetMailingSignature(ALanguage);

end;

function TdtmConfirmationReports.GetMailingVCASignature(ALanguage: Integer): WideString;
begin
  Result := ssckData.AppServer.GetMailingVCASignature(ALanguage);

end;

function TdtmConfirmationReports.GetMailingBedrijfVCASignature(ALanguage: Integer): WideString;
begin
  Result := ssckData.AppServer.GetMailingBedrijfVCASignature(ALanguage);

end;

procedure TdtmConfirmationReports.ApplyChanges;

  procedure ApplyChangesCDS(AClientDataSet: TClientDataSet);
  begin
    AClientDataSet.DisableControls;
    try
      if AClientDataSet.State in [ dsEdit, dsInsert ] then
      begin
        AClientDataSet.Post;
      end;

      if AClientDataSet.ChangeCount > 0 then
        AClientDataSet.ApplyUpdates(-1);
    finally
        AClientDataSet.EnableControls;
    end;

  end;
begin
  ApplyChangesCDS(cdsRecord);
  ApplyChangesCDS(cdsRecordOrganisation);
  ApplyChangesCDS(cdsRecordCompaniesSchools);
  ApplyChangesCDS(cdsRecordInfrastructure);

end;

{****************************************************************************************************
Name           : RecalcRolegroups
Author         : Ivan Van den Bossche
Arguments      : SessionId, TotalParticipants, OrganisationId
Return Values  : None
Exceptions     : None
Description    : Recalculate number of participants per rolegroup for given
                  session id/OrganisationId (Please note that OrganisationId refers to school/company
                  and inserts this totals into table T_REP_CONFIRMATION_COMPANY_ROLES
History        :

Date         By                   Description
----         --                   -----------
31/07/2007   Ivan Van den Bossche Initial creation of the Procedure.
*****************************************************************************************************}
procedure TdtmConfirmationReports.RecalcRolegroups(SessionId,
  TotalParticipants, OrganisationId, PartnerWinterId: Integer);
begin
  ssckData.AppServer.RecalcRolegroups(SessionId, TotalParticipants, OrganisationId, PartnerWinterId);
end;

procedure TdtmConfirmationReports.cdsListAfterPost(DataSet: TDataSet);
begin
  // Base class procedure may not be called here...
  //  inherited;

end;

procedure TdtmConfirmationReports.UpdateContactCompany(AOrganisationId,
  ANoContact: Integer; const AContactName, AContactEmail: WideString; AConfSubId: Integer; AIsCCEnabled: Integer);
begin
  ssckData.AppServer.UpdateContactCompany( AOrganisationId, ANoContact, AContactName, AContactEmail, AConfSubId, AIsCCEnabled);
end;

procedure TdtmConfirmationReports.UpdateContactInfrastruct(
  AInfrastructure_Id, ANoContact: Integer; const AContactName,
  AContactEmail: WideString);
begin
  ssckData.AppServer.UpdateContactInfrastruct( AInfrastructure_Id, ANoContact, AContactName, AContactEmail );
end;

procedure TdtmConfirmationReports.UpdateContactInstructorOrg(AInstructorId,
  ANoContact: Integer; const AContactName, AContactEmail: WideString);
begin
  ssckData.AppServer.UpdateContactInstructorOrg( AInstructorId, ANoContact, AContactName, AContactEmail );
end;

procedure TdtmConfirmationReports.MarkConfirmationAsPrinted(const AConf_Id,
  AConf_Sub_Id: Integer);
begin
  ssckData.AppServer.MarkConfirmationRptAsPrinted( AConf_Id, AConf_Sub_Id );
end;

{****************************************************************************************
  Name           : MarkConfrepSent
  Author         : Ivan Van den Bossche
  Arguments      : AConfId
  Return Values  : None
  Exceptions     : None
  Description    : Set [F_CONFIRMATION_REPORT_SENT] in T_SES_SESSION when all corresponding
                    records in T_REP_CONFIRMATION_DETAILS have F_CONFIRMATION_DATE set.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/04/2008   Ivan Van den Bossche Initial creation of the Procedure.
 ****************************************************************************************}
procedure TdtmConfirmationReports.MarkConfrepSent(AConfId: Integer);
begin
  ssckData.AppServer.MarkConfrepSent( AConfId );

end;

function TdtmConfirmationReports.AttestRegistered(aDataSet: TDataSet): integer;
var
   aNumber : integer;
begin
  cdsAttestRegistered.Close;
  cdsAttestRegistered.FetchParams;
  cdsAttestRegistered.Params.ParamByName( '@SessionId' ).Value := aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
  cdsAttestRegistered.Execute;
  cdsAttestRegistered.FetchParams;
  aNumber := cdsAttestRegistered.Params.ParamByName( '@RETURN_VALUE' ).Value;
  Result := aNumber;
end;

procedure TdtmConfirmationReports.CreateConfirmationsForPreview( const AMailingList,AInfraMailAttachCompSchool,AInfraMailAttachTeacher:Boolean );
const
  CNO_TASKS=4;
var
  ParamNames, ParamValues, PdfFiles, CRReports, CRReportTitles: TStringList;
  TempNames, TempValues, ListConf_SUB_ID: TStringList;
  CRReportname, CRReportTitle: String;
  ci, numberReports, SessionId: Integer;
  taskIdx, ValueIdx, I: integer;
  oldcursor: TCursor;
  Conf_ID: integer;
  PresenceListPrinted: Boolean;
  ReportExaminerPrinted: Boolean;

  // ****************************************************************************
  // Nested procedures

    // At least one organisation has been selected?
    procedure ValidateSelection;
        // Nested procedure in nested procedure :-)
        function SelectedRecordInCDS(ACds: TClientDataSet): Boolean;
        var
          Bookm: TBookmark;//TBookmarkStr;
          R: Boolean;
        begin
            R := False;
            try
              ACds.DisableControls;
              try
                Bookm := ACds.Bookmark;
                ACds.First;
                while not ACds.Eof do begin
                  if (ACds.FieldByName('F_PRINT_CONFIRMATION').AsBoolean) then
                  begin
                      R := True;
                      break;
                  end;
                  ACds.Next;
                end;

              finally
                  ACds.Bookmark := Bookm;
                  ACds.EnableControls;
              end;
            finally
                Result := R;
            end;
        end;
    begin
      // At least one record should be selected in order to continue exporting Crystal Reports reports to Pdf files
      if ( not SelectedRecordInCDS(cdsRecordOrganisation ))
          and ( not SelectedRecordInCDS(cdsRecordCompaniesSchools )
          and ( not SelectedRecordInCDS(cdsRecordInfrastructure ))) then
      begin
        raise EconfirmationReports.Create(eNoConfirmationSelected);
      end;

    end;

    procedure Crystal2Pdf(AReportName, AReportTitle: String; AParamNames, AParamValues: TStringList; APortrait: Boolean; OnClosed: TNotifyEvent);
    begin
      dtmEDUMainClient.ExecuteCrystal( AReportName, AReportTitle, AParamNames, AParamValues, True, APortrait, OnClosed );
    end;

    // Look for selected companies and schools...
    procedure _GetSelCompaniesSchools(AIsSchool: Boolean; AParamNames,AParamValues, CRReports, CRReportTitles : TStringList);
    var
      bIsSchool, isCCEnabled: Boolean;
      fi, org_type_id: integer;
      bookmark: TBookmark;//TBookmarkStr;
      session_id, total_participants, organisation_id, partner_winter_id: Integer;
      tcrReport, tcrReportTitle: String;
      processedPartnerWinters: TStringList;
    begin
      Assert(Assigned(CRReports), 'CRReports is nil');
      Assert(Assigned(CRReportTitles), 'CRReportTitles is nil');

      processedPartnerWinters := TStringList.Create;

      try
        // Search for selected organisationtype
        cdsRecordCompaniesSchools.DisableControls;
        try
          bookmark := cdsRecordCompaniesSchools.Bookmark;
          cdsRecordCompaniesSchools.First;
          try
            while not cdsRecordCompaniesSchools.Eof do begin
              tcrReport := '';
              tcrReportTitle := '';

              if cdsRecordCompaniesSchools.FieldByName('F_PRINT_CONFIRMATION').AsBoolean then
              begin
                org_type_id := cdsRecordCompaniesSchools.FieldByName('F_ORGTYPE_ID').AsInteger;
                if org_type_id = 4 then
                  bIsSchool := True
                else
                  bIsSchool := False;

                if AIsSchool = BIsSchool then
                begin
                  if bIsSchool then
                  begin
                    if FSessionGroupId = 4 then // Doelgroep leerlingen en leerkrachten?
                    begin
                      tcrReport := REP_CONFIRMATION_SCHOOL_STUDENTS;
                    end else
                    begin
                      if FProgramType = 1 then // 1=opleiding , 2=VCA
                        tcrReport := REP_CONFIRMATION_SCHOOL
                      else
                        tcrReport := REP_CONFIRMATION_VCA_SCHOOL;
                    end;
                    tcrReportTitle := rsTitleRptConfirmationSchool;
                  end
                  else
                  begin
                    // Handle normal company
                    partner_winter_id := cdsRecordCompaniesSchoolsF_PARTNER_WINTER_ID.AsInteger;

                    if partner_winter_id = 0 then
                    begin
                      // No partner winter found
                      if FProgramType = 1 then // 1=opleiding , 2=VCA
                        tcrReport := REP_CONFIRMATION_EDUTEC_WEEK
                      else
                        tcrReport := REP_CONFIRMATION_VCA_BEDRIJF;
                      tcrReportTitle := rsTitleRptConfirmationCompany;
                    end else
                    begin
                      // We have to send letter to specific partner winter only once, check whether we've already added it for processing or not.
                      if not processedPartnerWinters.Find(IntToStr(partner_winter_id), fi) then
                      begin
                        AParamValues.Add(IntToStr(Conf_ID));
                        AParamValues.Add(cdsRecordCompaniesSchools.FieldByName('F_CONF_SUB_ID').AsString);

                        AParamNames.Add('p_conf_id');
                        AParamNames.Add('p_conf_sub_id');

                        if FProgramType = 1 then // 1=opleiding , 2=VCA
                          CRReports.Add(REP_CONFIRMATION_WORKERS_CONSTRUCTION)
                        else
                          CRReports.Add(REP_CONFIRMATION_VCA_BEDRIJF);
                        CRReportTitles.Add(rsTitleRptConfirmationPartnerWinter);

                        processedPartnerWinters.Add(IntToStr(partner_winter_id));
                      end;

                      // Partner winter found, handle this case
                      isCCEnabled := cdsRecordCompaniesSchoolsF_ISCCENABLED.AsBoolean;

                      if isCCEnabled then
                      begin
                        if FProgramType = 1 then // 1=opleiding , 2=VCA
                          tcrReport := REP_CONFIRMATION_WORKERS_OTHER
                      else
                          tcrReport := REP_CONFIRMATION_VCA_BEDRIJF;
                        tcrReportTitle := rsTitleRptConfirmationWinterCC;
                      end;
                    end;
                  end;

                  if Length(tcrReport) > 0 then
                  begin
                    Assert(Length(tcrReportTitle) > 0, 'Length(tcrReportTitle) = 0');

                    AParamNames.Add('p_conf_id');
                    AParamNames.Add('p_conf_sub_id');

                    AParamValues.Add( IntToStr(Conf_ID) );
                    AParamValues.Add( cdsRecordCompaniesSchools.FieldByName('F_CONF_SUB_ID').AsString );

                    CRReports.Add(tcrReport);
                    CRReportTitles.Add(tcrReportTitle);
                  end;

                  ListConf_SUB_ID.Add(cdsRecordCompaniesSchools.FieldByName('F_CONF_SUB_ID').AsString );

                  // For companies, we need to determine number of participants per category.
                  // Stored procedure SP_CONFIRMATION_CALC_ROLEGROUPS will be called in order to insert data
                  // into table T_REP_CONFIRMATION_COMPANY_ROLES
                  if not bIsSchool then
                  begin
                    session_id := cdsRecordCompaniesSchools.FieldByName('F_SESSION_ID').AsInteger;
                    total_participants := cdsRecordCompaniesSchools.FieldByName('F_NO_PARTICIPANTS').AsInteger;
                    organisation_id := cdsRecordCompaniesSchools.FieldByName('F_ORGANISATION_ID').AsInteger;
                    partner_winter_id := cdsRecordCompaniesSchoolsF_PARTNER_WINTER_ID.AsInteger;
                    RecalcRolegroups( session_id, total_participants, organisation_id, partner_winter_id );
                  end;
                end;
              end;
              cdsRecordCompaniesSchools.Next;
            end;
          finally
            cdsRecordCompaniesSchools.Bookmark := bookmark;
          end;
        finally
          cdsRecordCompaniesSchools.EnableControls;
        end;
        finally
          processedPartnerWinters.Free;
        end;
    end;

    // Get selected schools
    procedure GetSelSchools(AParamNames, AParamValues, CRReports, CRReportTitles : TStringList);
    begin
      Assert(Assigned(CRReports), 'CRReports is nil');
      Assert(Assigned(CRReportTitles), 'CRReportTitles is nil');

      _GetSelCompaniesSchools( True, AParamNames, AParamValues, CRReports, CRReportTitles );
    end;

    // Get selected companies
    procedure GetSelCompanies(AParamNames,AParamValues,CRReports, CRReportTitles : TStringList);
    begin
      Assert(Assigned(CRReports), 'CRReports is nil');
      Assert(Assigned(CRReportTitles), 'CRReportTitles is nil');

      // Look for selected schools and companies
      _GetSelCompaniesSchools( False, AParamNames, AParamValues, CRReports, CRReportTitles );
    end;

    // Get selected infrastructure
    procedure GetSelInfrastructure(AParamNames,AParamValues : TStringList; var ACRReportname, ACRReportTitle: String);
    begin
      assert( cdsRecordInfrastructure.RecordCount > 0);

      if FProgramType = 1 then // 1=opleiding , 2=VCA
        ACRReportname := REP_CONFIRMATION_INFRASTRUCTURE
      else
        ACRReportname := REP_CONFIRMATION_VCA_INFRASTRUCTURE;
      ACRReportTitle := rsTitleRptConfirmationInfrastructure;

      // Search for selected infrastructure
      cdsRecordInfrastructure.DisableControls;
      try
        if cdsRecordInfrastructure.FieldByName('F_PRINT_CONFIRMATION').AsBoolean then
        begin
            AParamNames.Add('p_session_id');
            AParamValues.Add(cdsRecord.FieldByName('F_SESSION_ID').AsString);

            ListConf_SUB_ID.Add(cdsREcordInfrastructure.FieldByName('F_CONF_SUB_ID').AsString);
        end;
      finally
          cdsRecordInfrastructure.EnableControls;
      end;
    end;

    // Get selected organisation
    function GetSelOrganisations(AParamNames,AParamValues : TStringList; var ACRReportname, ACRReportTitle: String): Integer;
    var
      InstructorOrganisations: TStringList;
      organisation: String;
      Bookm: TBookmark;//TBookmarkStr;
      nSelectedOrganisations: Integer;
    begin
      assert(cdsRecordOrganisation.RecordCount > 0);

      nSelectedOrganisations := 0;

      // test Ivan
      //AParamNames.Add('p_conf_id');
      //AParamNames.Add('p_conf_sub_id');

      // Store names of organisations instructors here
      // This stringlist will be used to check whether specific organisation has been already selected
      // We want only one letter for each instructor organisation
      InstructorOrganisations := TStringList.Create;
      try
          InstructorOrganisations.Sorted := True;
          InstructorOrganisations.Duplicates := dupError;

          if FProgramType = 1 then // 1=opleiding , 2=VCA
            ACRReportname := REP_CONFIRMATION_ORGANISATION
          else
            ACRReportname := REP_CONFIRMATION_VCA_EXAMINATOR;
          ACRReportTitle := rsTitleRptConfirmationOrganisation;

          // Search for selected organisation
          cdsRecordOrganisation.DisableControls;
          try
            Bookm := cdsRecordOrganisation.Bookmark;
            cdsRecordOrganisation.First;
            while not cdsRecordOrganisation.Eof do begin
                if cdsRecordOrganisation.FieldByName('F_PRINT_CONFIRMATION').AsBoolean then
                begin
                    Inc(nSelectedOrganisations);
                    
                    // test Ivan
                    AParamNames.Add('p_conf_id');
                    AParamNames.Add('p_conf_sub_id');

                    // Selected Instructor is a freelancer?  Create a separate confirmation report for this case
                    if (cdsRecordOrganisation.FieldByName('F_ORG_INSTRUCTOR_NAME').IsNull) or (Length(Trim(cdsRecordOrganisation.FieldByName('F_ORG_INSTRUCTOR_NAME').AsString)) = 0) then
                    begin
                      AParamValues.Add( IntToStr(Conf_ID) );
                      AParamValues.Add( cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString );

                      ListConf_SUB_ID.Add( cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString );
                    end else begin
                        organisation := Trim( cdsRecordOrganisation.FieldByName('F_ORG_INSTRUCTOR_NAME').AsString );
                        try
                          // Place Conf SUB ID on list so it could be marked as printed later
                          ListConf_SUB_ID.Add( cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString );

                          // Try to add organisation to list
                          InstructorOrganisations.Add( organisation );

                          // and select organisation record so confirmation report will be generated...
                          AParamValues.Add(IntToStr(Conf_ID));
                          AParamValues.Add(cdsRecordOrganisation.FieldByName('F_CONF_SUB_ID').AsString);

                        except
                            // Ignore error-> organisation already selected, so leave it
                        end;
                    end;
                end;
                cdsRecordOrganisation.Next;
            end;
          finally
            cdsRecordOrganisation.Bookmark := Bookm;
            cdsRecordOrganisation.EnableControls;
          end;
      finally
          InstructorOrganisations.Free;
          Result := nSelectedOrganisations;
      end;
    end;

    procedure PrintPresenceList();
    var
      TempNames: TStringList;
    begin
      TempNames := TStringList.Create;
      TempValues := TStringList.Create;
      try
        TempNames.Add('p_session_id');
        TempValues.Add(cdsRecord.FieldByName('F_SESSION_ID').AsString);

        Crystal2Pdf(REP_PRESENCE_LIST, rsTitleRptPresenceList, TempNames, TempValues, True { Portrait}, nil);

        PresenceListPrinted := True;  // Presence list has been printed
      finally
        TempNames.Free;
        TempValues.Free;
      end;
    end;

  // *************** NESTED PROCEDURES END HERE *********************************
  // ****************************************************************************
begin
  oldCursor := screen.cursor;
  screen.cursor := crSQLWait;

  PresenceListPrinted := False;
  try
      SessionId := cdsRecord.FieldByName('F_SESSION_ID').AsInteger;

      // At least one record selected?  If none, exception will be triggered
      ValidateSelection;

      // Get SessionGroup id from selected session
      FSessionGroupId := GetSessionGroupIdFromSessionId( SessionId );
      FProgramType :=  GetProgramTypeFromSessionId( SessionId );

      // Record identifier
      Conf_ID := cdsRecord.FieldByName('F_CONF_ID').AsInteger;

      // Loop through all kinds of records (Schools/Companies, Infrastructure, Organisations (instructors)
      for taskIdx:=1 to CNO_TASKS do begin
        ParamNames := TStringList.Create;
        ParamValues := TStringList.Create;
        PdfFiles := TStringList.Create;
        ListConf_SUB_ID := TStringList.Create;
        CRReports := TStringList.Create;
        CRReportTitles := TStringList.Create;
        try
          case taskIdx of
            TASK_INFRASTRUCTURE:
            begin
              GetSelInfrastructure( ParamNames,ParamValues,CRReportName,CRReportTitle );
              CRReports.Add(CRReportName);
              CRReportTitles.Add(CRReportTitle);
            end;
            TASK_SCHOOLS:
            begin
              GetSelSchools( ParamNames,ParamValues, CRReports, CRReportTitles );
            end;
            TASK_COMPANIES: GetSelCompanies( ParamNames,ParamValues, CRReports, CRReportTitles );
            TASK_ORGANISATION:
            begin
              numberReports := GetSelOrganisations( ParamNames,ParamValues,CRReportName,CRReportTitle );

              for ci := 0 to numberReports - 1 do
              begin
                CRReports.Add(CRReportName);
                CRReportTitles.Add(CRReportTitle);
              end;
            end;
          end;

          if ParamValues.Count = 1 then
          begin
            if (taskIdx = TASK_INFRASTRUCTURE) and (not PresenceListPrinted) then begin
                // At least one infrastructure was selected and Presence list hasn't been printed yet
               PrintPresenceList();
            end;
              Assert(CRReports.Count = 1, 'CRReports.Count <> 1');
              Assert(CRReportTitles.Count = 1, 'CRReportTitles.Count <> 1');

              Crystal2Pdf( CRReports[0], CRReportTitles[0],
                        ParamNames, ParamValues, True {Portrait}, nil );
          end else if ParamValues.Count > 1 then
          begin
              assert(ParamNames.Count = ParamValues.Count); // Number of parameters should be 2!!!

              if (taskIdx = TASK_ORGANISATION) and (not PresenceListPrinted) then begin
                // At least one organisation (instructor) was selected and Presence list hasn't been printed yet
                PrintPresenceList();
              end;

              // Create for each selected record another outputfile (Schools and companies only)
              // Loop through set of 2 parameter values...  For each set, create another PDF file
              ValueIdx := 0;
              i := 0;

              while ValueIdx < ParamValues.Count-1 do begin
                TempValues := TStringList.Create;
                TempNames := TStringList.Create;
                try
                  // Add
                  TempValues.Add(ParamValues.Strings[ValueIdx]);
                  TempValues.Add(ParamValues.Strings[ValueIdx+1]);

                  TempNames.Add(ParamNames.Strings[ValueIdx]);
                  TempNames.Add(ParamNames.Strings[ValueIdx+1]);

                  Crystal2Pdf( CRReports[i], CRReportTitles[i],
                               TempNames, TempValues, True {Portrait}, nil );

                  ValueIdx := ValueIdx + 2; // Go to next pair of Parameter values
                  Inc(i);
                finally
                  TempValues.Free;
                  TempNames.Free;
                end;
              end;
          end;
        finally
          ListConf_SUB_ID.Free;
          ParamNames.Free;
          ParamValues.Free;
          PdfFiles.Free;
          CRReports.Free;
          CRReportTitles.Free;
        end;
      end;
  finally
      Screen.Cursor := oldCursor;
  end;
end;

end.

