{*****************************************************************************
  This DataModule will be used for the Maintenance of T_GE_POSTALCODE records.

  @Name       Data_Postalcode
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   sLesage              Added support for multiple destinations
                                    using the SelectXXX and ShowXXX class
                                    functions.
  26/07/2005   sLesage              The field F_POSTALCODE will now also be
                                    copied in the CopyPostalCodeFieldValues
                                    Method.
  30/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Postalcode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_PPWFrameWorkClasses,
  Unit_FVBFFCInterfaces, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmPostalcode = class(TEduDataModule, IEDUPostalCodeDataModule )
    qryListF_POSTALCODE_ID: TIntegerField;
    qryListF_PROVINCE_ID: TIntegerField;
    qryListF_CITY_NL: TStringField;
    qryListF_CITY_FR: TStringField;
    qryListF_POSTALCODE: TStringField;
    qryListF_CITY_NAME: TStringField;
    qryListF_PROVINCE_NAME: TStringField;
    qryRecordF_POSTALCODE_ID: TIntegerField;
    qryRecordF_PROVINCE_ID: TIntegerField;
    qryRecordF_CITY_NL: TStringField;
    qryRecordF_CITY_FR: TStringField;
    qryRecordF_POSTALCODE: TStringField;
    qryRecordF_CITY_NAME: TStringField;
    qryRecordF_PROVINCE_NAME: TStringField;
    cdsListF_POSTALCODE_ID: TIntegerField;
    cdsListF_PROVINCE_ID: TIntegerField;
    cdsListF_CITY_NL: TStringField;
    cdsListF_CITY_FR: TStringField;
    cdsListF_POSTALCODE: TStringField;
    cdsListF_CITY_NAME: TStringField;
    cdsListF_PROVINCE_NAME: TStringField;
    cdsRecordF_POSTALCODE_ID: TIntegerField;
    cdsRecordF_PROVINCE_ID: TIntegerField;
    cdsRecordF_CITY_NL: TStringField;
    cdsRecordF_CITY_FR: TStringField;
    cdsRecordF_POSTALCODE: TStringField;
    cdsRecordF_CITY_NAME: TStringField;
    cdsRecordF_PROVINCE_NAME: TStringField;
    cdsSearchCriteriaF_POSTALCODE_ID: TIntegerField;
    cdsSearchCriteriaF_PROVINCE_ID: TIntegerField;
    cdsSearchCriteriaF_POSTALCODE: TStringField;
    cdsSearchCriteriaF_CITY_NAME: TStringField;
    cdsSearchCriteriaF_PROVINCE_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectProvince( aDataSet : TDataSet ); virtual;
    procedure ShowProvince  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectPostalCode( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault     ); virtual;
    class procedure ShowPostalCode  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
  end;

var
  dtmPostalcode: TdtmPostalcode;

implementation

uses
  Data_Province, Data_EDUMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy PostalCode related fields from one
  DataSet to another DataSet.

  @Name       CopyPostalCodeFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyPostalCodeFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault  );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    case aCopyTo of
      critPostalCodeToOrgInvoiceAddress :
      begin
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_INVOICE_POSTALCODE_ID' ).AsInteger :=
            aSource.FieldByName( 'F_POSTALCODE_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_INVOICE_POSTALCODE' ).AsString :=
          aSource.FieldByName( 'F_POSTALCODE' ).AsString;
        aDestination.FieldByName( 'F_INVOICE_CITY_NAME' ).AsString :=
          aSource.FieldByName( 'F_CITY_NAME' ).AsString;
      end;
      critDefault :
      begin
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_POSTALCODE_ID' ).AsInteger :=
            aSource.FieldByName( 'F_POSTALCODE_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_POSTALCODE' ).AsString :=
          aSource.FieldByName( 'F_POSTALCODE' ).AsString;
        aDestination.FieldByName( 'F_CITY_NAME' ).AsString :=
          aSource.FieldByName( 'F_CITY_NAME' ).AsString;
      end;
    end;
  end;
end;

procedure TdtmPostalcode.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_GE_POSTALCODE';
end;

{*****************************************************************************
  This Method will be used to select one or more Province Records.

  @Name       TdtmPostalcode.SelectProvince
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPostalcode.SelectProvince(aDataSet: TDataSet);
begin
  TdtmProvince.SelectProvince( aDataSet );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Province Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmPostalcode.ShowProvince
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPostalcode.ShowProvince(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_PROVINCE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmProvince.ShowProvince( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Province Records.

  @Name       TdtmPostalcode.SelectPostalCode
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmPostalcode.SelectPostalCode( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault     );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the CountryPart ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmPostalcode', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyPostalCodeFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Province
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmPostalcode.ShowPostalCode
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmPostalcode.ShowPostalCode(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView;
  aCopyTo : TCopyRecordInformationTo = critDefault  );
var
  aSelectedRecords : TClientDataSet;
  aWhere           : String;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    case aCopyTo of
      critPostalCodeToOrgInvoiceAddress :
      begin
        aWhere := 'F_POSTALCODE_ID = ' + aDataSet.FieldByName( 'F_INVOICE_POSTALCODE_ID' ).AsString;
      end;
      else
      begin
        aWhere := 'F_POSTALCODE_ID = ' + aDataSet.FieldByName( 'F_POSTALCODE_ID' ).AsString;
      end;
    end;

    { Show a Modal RecordView for TdtmProvince.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmPostalcode', aWhere, aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyPostalCodeFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmPostalcode.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPostalcode.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_POSTALCODE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmPostalcode.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmPostalcode.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUProvinceDataModule ) ) then
    begin
       aDataSet.FieldByName( 'F_PROVINCE_NAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_PROVINCE_NAME' ).AsString;
    end;
  end;
end;

end.
