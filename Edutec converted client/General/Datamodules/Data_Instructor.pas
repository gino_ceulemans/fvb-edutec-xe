{*****************************************************************************
  This DataModule will be used for the maintenance of T_DOC_INSTRUCTOR.

  @Name       Data_Instructor
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  21/10/2005   sLesage              Fixed some Typos.
  26/07/2005   sLesage              Added the F_FULLNAME Field.
                                    Added functionality to link Instructors
                                    to a Program.
                                    Added a Detail DataModule for ProgInstructor
  20/07/2005   sLesage              Finished the implementation of the
                                    different SelectXXX and ShowXXX methods.
  19/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Instructor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, DB, ADODB,
  Unit_FVBFFCInterfaces, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmInstructor = class(TEDUDataModule, IEDUDocInstructorDataModule)
    qryListF_INSTRUCTOR_ID: TIntegerField;
    qryListF_LASTNAME: TStringField;
    qryListF_FIRSTNAME: TStringField;
    qryListF_STREET: TStringField;
    qryListF_NUMBER: TStringField;
    qryListF_MAILBOX: TStringField;
    qryListF_POSTALCODE_ID: TIntegerField;
    qryListF_POSTALCODE: TStringField;
    qryListF_CITY_NAME: TStringField;
    qryListF_LANGUAGE_ID: TIntegerField;
    qryListF_LANGUAGE_NAME: TStringField;
    qryListF_ACC_NR: TStringField;
    qryRecordF_INSTRUCTOR_ID: TIntegerField;
    qryRecordF_TITLE_ID: TIntegerField;
    qryRecordF_TITLE_NAME: TStringField;
    qryRecordF_LASTNAME: TStringField;
    qryRecordF_FIRSTNAME: TStringField;
    qryRecordF_STREET: TStringField;
    qryRecordF_NUMBER: TStringField;
    qryRecordF_MAILBOX: TStringField;
    qryRecordF_POSTALCODE_ID: TIntegerField;
    qryRecordF_POSTALCODE: TStringField;
    qryRecordF_CITY_NAME: TStringField;
    qryRecordF_COUNTRY_ID: TIntegerField;
    qryRecordF_COUNTRY_NAME: TStringField;
    qryRecordF_PHONE: TStringField;
    qryRecordF_FAX: TStringField;
    qryRecordF_GSM: TStringField;
    qryRecordF_EMAIL: TStringField;
    qryRecordF_LANGUAGE_ID: TIntegerField;
    qryRecordF_LANGUAGE_NAME: TStringField;
    qryRecordF_GENDER_ID: TIntegerField;
    qryRecordF_GENDER_NAME: TStringField;
    qryRecordF_MEDIUM_ID: TIntegerField;
    qryRecordF_MEDIUM_NAME: TStringField;
    qryRecordF_ACC_NR: TStringField;
    qryRecordF_FOREIGN_ACC_NR: TStringField;
    qryRecordF_SOCSEC_NR: TStringField;
    qryRecordF_DATEBIRTH: TDateTimeField;
    qryRecordF_ACTIVE: TBooleanField;
    qryRecordF_END_DATE: TDateTimeField;
    qryRecordF_DUTCH: TBooleanField;
    qryRecordF_FRENCH: TBooleanField;
    qryRecordF_ENGLISH: TBooleanField;
    qryRecordF_GERMAN: TBooleanField;
    qryRecordF_OTHER_LANGUAGE: TStringField;
    qryRecordF_DIPLOMA_ID: TIntegerField;
    qryRecordF_DIPLOMA_NAME: TStringField;
    qryRecordF_COMMENT: TMemoField;
    qryRecordF_CODE: TStringField;
    qryRecordF_CV_ID: TIntegerField;
    qryRecordF_DOCUMENT_NAME: TStringField;
    cdsListF_INSTRUCTOR_ID: TIntegerField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_STREET: TStringField;
    cdsListF_NUMBER: TStringField;
    cdsListF_MAILBOX: TStringField;
    cdsListF_POSTALCODE_ID: TIntegerField;
    cdsListF_POSTALCODE: TStringField;
    cdsListF_CITY_NAME: TStringField;
    cdsListF_LANGUAGE_ID: TIntegerField;
    cdsListF_LANGUAGE_NAME: TStringField;
    cdsListF_ACC_NR: TStringField;
    cdsRecordF_INSTRUCTOR_ID: TIntegerField;
    cdsRecordF_TITLE_ID: TIntegerField;
    cdsRecordF_TITLE_NAME: TStringField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_STREET: TStringField;
    cdsRecordF_NUMBER: TStringField;
    cdsRecordF_MAILBOX: TStringField;
    cdsRecordF_POSTALCODE_ID: TIntegerField;
    cdsRecordF_POSTALCODE: TStringField;
    cdsRecordF_CITY_NAME: TStringField;
    cdsRecordF_COUNTRY_ID: TIntegerField;
    cdsRecordF_COUNTRY_NAME: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_GSM: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_GENDER_ID: TIntegerField;
    cdsRecordF_GENDER_NAME: TStringField;
    cdsRecordF_MEDIUM_ID: TIntegerField;
    cdsRecordF_MEDIUM_NAME: TStringField;
    cdsRecordF_ACC_NR: TStringField;
    cdsRecordF_FOREIGN_ACC_NR: TStringField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_DATEBIRTH: TDateTimeField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_DUTCH: TBooleanField;
    cdsRecordF_FRENCH: TBooleanField;
    cdsRecordF_ENGLISH: TBooleanField;
    cdsRecordF_GERMAN: TBooleanField;
    cdsRecordF_OTHER_LANGUAGE: TStringField;
    cdsRecordF_DIPLOMA_ID: TIntegerField;
    cdsRecordF_DIPLOMA_NAME: TStringField;
    cdsRecordF_COMMENT: TMemoField;
    cdsRecordF_CODE: TStringField;
    cdsRecordF_CV_ID: TIntegerField;
    cdsRecordF_DOCUMENT_NAME: TStringField;
    cdsSearchCriteriaF_INSTRUCTOR_ID: TIntegerField;
    cdsSearchCriteriaF_LASTNAME: TStringField;
    cdsSearchCriteriaF_FIRSTNAME: TStringField;
    cdsSearchCriteriaF_LANGUAGE_ID: TIntegerField;
    cdsSearchCriteriaF_LANGUAGE_NAME: TStringField;
    cdsSearchCriteriaF_CODE: TStringField;
    cdsListF_FULLNAME: TStringField;
    qryRecordF_FULLNAME: TStringField;
    cdsRecordF_FULLNAME: TStringField;
    qryListF_FULLNAME: TStringField;
    qryListF_SOCSEC_NR: TStringField;
    cdsListF_SOCSEC_NR: TStringField;
    qryListF_PHONE: TStringField;
    qryListF_FAX: TStringField;
    qryListF_EMAIL: TStringField;
    qryListF_GSM: TStringField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsListF_GSM: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsSearchCriteriaF_ACTIVE: TBooleanField;
    cdsListF_BTW: TStringField;
    cdsRecordF_BTW: TStringField;
    qryRecordF_ORG_LESGEVER: TStringField;
    qryRecordF_ORG_CONTACT1: TStringField;
    qryRecordF_ORG_CONTACT2: TStringField;
    qryRecordF_ORG_CONTACT1_EMAIL: TStringField;
    qryRecordF_ORG_CONTACT2_EMAIL: TStringField;
    cdsListF_ORGANISATION_NAME: TStringField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsRecordF_ORGANISATION_NAME: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT1: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT2: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    cdsListF_CONFIRMATION_CONTACT1: TStringField;
    cdsListF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    cdsListF_CONFIRMATION_CONTACT2: TStringField;
    cdsListF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SelectDiploma      ( aDataSet : TDataSet ); virtual;
    procedure SelectGender       ( aDataSet : TDataSet ); virtual;
    procedure SelectLanguage     ( aDataSet : TDataSet ); virtual;
    procedure SelectMedium       ( aDataSet : TDataSet ); virtual;
    procedure SelectPostalCode   ( aDataSet : TDataSet ); virtual;
    procedure SelectCountry      ( aDataSet : TDataSet ); virtual;
    procedure SelectTitle        ( aDataSet : TDataSet ); virtual;
    procedure SelectOrganisation ( aDataSet : TDataSet ); virtual;
    procedure ShowDiploma      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowGender       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowPostalCode   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowCountry      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowTitle        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowOrganisation ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure LinkInstructorsToProgram(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aShowInactive : Boolean = False
          ); virtual;
    class procedure SelectInstructor(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure ShowInstructor  (
          aDataSet : TDataSet;
          aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView;
          aCopyTo : TCopyRecordInformationTo = critDefault
          ); virtual;
  end;

  procedure CopyInstructorFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_Diploma, Data_Gender, Data_Language,
  Data_Medium, Data_Postalcode, Data_Title, Data_Country,
  Data_FVBFFCBaseDataModule, data_Organisation;

{*****************************************************************************
  This procedure will be used to copy Instructor related fields from one
  DataSet to another DataSet.

  @Name       CopyInstructorFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyInstructorFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_INSTRUCTOR_ID' ).AsInteger :=
        aSource.FieldByName( 'F_INSTRUCTOR_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      critInstructorToProgInstructor :
      begin
         aDestination.FieldByName( 'F_FULLNAME' ).AsString :=
           aSource.FieldByName( 'F_FULLNAME' ).AsString;
         aDestination.FieldByName( 'F_SOCSEC_NR' ).AsString :=
           aSource.FieldByName( 'F_SOCSEC_NR' ).AsString;
         aDestination.FieldByName( 'F_PHONE' ).AsString :=
           aSource.FieldByName( 'F_PHONE' ).AsString;
         aDestination.FieldByName( 'F_FAX' ).AsString :=
           aSource.FieldByName( 'F_FAX' ).AsString;
         aDestination.FieldByName( 'F_GSM' ).AsString :=
           aSource.FieldByName( 'F_GSM' ).AsString;
         aDestination.FieldByName( 'F_EMAIL' ).AsString :=
           aSource.FieldByName( 'F_EMAIL' ).AsString;
         aDestination.FieldByName( 'F_LASTNAME' ).AsString :=
           aSource.FieldByName( 'F_LASTNAME' ).AsString;
         aDestination.FieldByName( 'F_FIRSTNAME' ).AsString :=
           aSource.FieldByName( 'F_FIRSTNAME' ).AsString;
      end;
      critInstructorToSesInstructor :
      begin
         aDestination.FieldByName( 'F_FULLNAME' ).AsString :=
           aSource.FieldByName( 'F_FULLNAME' ).AsString;
         aDestination.FieldByName( 'F_SOCSEC_NR' ).AsString :=
           aSource.FieldByName( 'F_SOCSEC_NR' ).AsString;
         aDestination.FieldByName( 'F_PHONE' ).AsString :=
           aSource.FieldByName( 'F_PHONE' ).AsString;
         aDestination.FieldByName( 'F_FAX' ).AsString :=
           aSource.FieldByName( 'F_FAX' ).AsString;
         aDestination.FieldByName( 'F_GSM' ).AsString :=
           aSource.FieldByName( 'F_GSM' ).AsString;
         aDestination.FieldByName( 'F_EMAIL' ).AsString :=
           aSource.FieldByName( 'F_EMAIL' ).AsString;
      end;
      critInstructorToSesSearchInstructor :
      begin
         aDestination.FieldByName( 'F_INSTRUCTOR_NAME' ).AsString :=
           aSource.FieldByName( 'F_FULLNAME' ).AsString;
      end
      else
      begin
        aDestination.FieldByName( 'F_FULLNAME' ).AsString :=
          aSource.FieldByName( 'F_FULLNAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Instructor Records.

  @Name       TdtmInstructor.SelectInstructor
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmInstructor.SelectInstructor(
  aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False
  );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    aWhere := aWhereClause;
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmInstructor', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyInstructorFieldValues( aSelectedRecords, aDataSet, True, aCopyTo);
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Instructor
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmInstructor.ShowInstructor
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmInstructor.ShowInstructor(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView;
  aCopyTo : TCopyRecordInformationTo = critDefault
);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmInstructor',
    'F_INSTRUCTOR_ID = ' + aDataSet.FieldByName( 'F_INSTRUCTOR_ID' ).AsString ,
    aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyInstructorFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmInstructor.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_DOC_INSTRUCTOR';
end;

{*****************************************************************************
  This method will be used to allow the user to Select a Diploma.

  @Name       TdtmInstructor.SelectDiploma
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.SelectDiploma(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_DIPLOMA_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Diploma isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_DIPLOMA_ID <> ' + aIDField.AsString;
  end;

  TdtmDiploma.SelectDiploma( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Select a Gender.

  @Name       TdtmInstructor.SelectGender
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.SelectGender(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_GENDER_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Gender isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_Gender_ID <> ' + aIDField.AsString;
  end;

  TdtmGender.SelectGender( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Select a Language.

  @Name       TdtmInstructor.SelectLanguage
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.SelectLanguage(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_LANGUAGE_ID <> ' + aIDField.AsString;
  end;

  TdtmLanguage.SelectLanguage( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Select a Medium.

  @Name       TdtmInstructor.SelectMedium
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.SelectMedium(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Medium isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_MEDIUM_ID <> ' + aIDField.AsString;
  end;

  TdtmMedium.SelectMedium( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Select a PostalCode.

  @Name       TdtmInstructor.SelectPostalCode
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.SelectPostalCode(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_POSTALCODE_ID <> ' + aIDField.AsString;
  end;

  TdtmPostalcode.SelectPostalCode( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to allow the user to Select a Title.

  @Name       TdtmInstructor.SelectTitle
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.SelectTitle(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_TITLE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Title isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_TITLE_ID <> ' + aIDField.AsString;
  end;

  TdtmTitle.SelectTitle( aDataSet, aWhere )
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Diploma.

  @Name       TdtmInstructor.ShowDiploma
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.ShowDiploma(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_DIPLOMA_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmDiploma.ShowDiploma( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Gender.

  @Name       TdtmInstructor.ShowGender
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.ShowGender(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_GENDER_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmGender.ShowGender( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Language.

  @Name       TdtmInstructor.ShowLanguage
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Medium.

  @Name       TdtmInstructor.ShowMedium
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.ShowMedium(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmMedium.ShowMedium( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a PostalCode.

  @Name       TdtmInstructor.ShowPostalCode
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.ShowPostalCode(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmPostalcode.ShowPostalCode( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Title.

  @Name       TdtmInstructor.ShowTitle
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmInstructor.ShowTitle(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_TITLE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmTitle.ShowTitle( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to link one or more Instructors to a Program.

  @Name       TdtmInstructor.LinkInstructorsToProgram
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmInstructor.LinkInstructorsToProgram(
  aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aShowInactive : Boolean = False
  );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the Country ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
{    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin}
      aWhere := aWhereClause;
//    end;
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmInstructor', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      aSelectedRecords.First;
      while not aSelectedRecords.Eof do
      begin
        aDataSet.Tag := 1;
        aDataSet.Insert;
        CopyInstructorFieldValues( aSelectedRecords, aDataSet, True, critInstructorToProgInstructor );
        aDataSet.Post;
        aSelectedRecords.Next;
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmInstructor.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_INSTRUCTOR_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

procedure TdtmInstructor.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  // Work around tot we bekeken hebben wat hiermee te doen ...
  aDataSet.FieldByName( 'F_COUNTRY_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_COUNTRY_NAME' ).AsString := 'Belgi�';
  // Taalselectie voorbereiden
  aDataSet.FieldByName( 'F_ENGLISH' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_DUTCH'   ).AsBoolean := False;
  aDataSet.FieldByName( 'F_GERMAN'  ).AsBoolean := False;
  aDataSet.FieldByName( 'F_FRENCH'  ).AsBoolean := False;
  // Status actief -> altijd True
  aDataSet.FieldByName( 'F_ACTIVE'  ).AsBoolean := True;

end;

procedure TdtmInstructor.SelectCountry(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_COUNTRY_ID <> ' + aIDField.AsString;
  end;

  TdtmCountry.SelectCountry( aDataSet, aWhere );
end;

procedure TdtmInstructor.ShowCountry(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCountry.ShowCountry( aDataSet, aRecordViewMode );
  end;
end;

procedure TdtmInstructor.cdsSearchCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsSearchCriteria.FieldByName( 'F_ACTIVE' ).Value := True;
end;

procedure TdtmInstructor.SelectOrganisation(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere : String;
begin
  aIDField := aDataSet.FindField ('F_ORGANISATION_ID');
  aWhere := 'F_ORGTYPE_ID = 11'; //only 'opleidingsbedrijven'
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := aWhere + 'and F_ORGANISATION_ID <> ' + aIDField.AsString;
  end;

  TdtmOrganisation.SelectOrganisation (aDataSet, aWhere, false, critOrganisationToTeacher, false);
end;



procedure TdtmInstructor.ShowOrganisation(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  //todo
end;

end.
