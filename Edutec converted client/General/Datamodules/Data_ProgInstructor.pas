{*****************************************************************************
  This DataModule will be used for the maintenance of V_PROG_INSTRUCTOR.

  @Name       Data_ProgInstructor
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_ProgInstructor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, DB, ADODB,
  Unit_FVBFFCInterfaces, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmProgInstructor = class(TEduDataModule, IEDUProgInstructorDataModule )
    qryListF_PROG_INSTRUCTOR_ID: TIntegerField;
    qryListF_INSTRUCTOR_ID: TIntegerField;
    qryListF_FULLNAME: TStringField;
    qryListF_SOCSEC_NR: TStringField;
    qryListF_PHONE: TStringField;
    qryListF_FAX: TStringField;
    qryListF_GSM: TStringField;
    qryListF_EMAIL: TStringField;
    qryListF_PROGRAM_ID: TIntegerField;
    qryListF_NAME: TStringField;
    qryListF_START_DATE: TDateTimeField;
    qryListF_END_DATE: TDateTimeField;
    qryListF_ACTIVE: TBooleanField;
    qryRecordF_PROG_INSTRUCTOR_ID: TIntegerField;
    qryRecordF_INSTRUCTOR_ID: TIntegerField;
    qryRecordF_FULLNAME: TStringField;
    qryRecordF_LASTNAME: TStringField;
    qryRecordF_FIRSTNAME: TStringField;
    qryRecordF_SOCSEC_NR: TStringField;
    qryRecordF_PHONE: TStringField;
    qryRecordF_FAX: TStringField;
    qryRecordF_GSM: TStringField;
    qryRecordF_EMAIL: TStringField;
    qryRecordF_PROGRAM_ID: TIntegerField;
    qryRecordF_NAME: TStringField;
    qryRecordF_START_DATE: TDateTimeField;
    qryRecordF_END_DATE: TDateTimeField;
    qryRecordF_ACTIVE: TBooleanField;
    cdsListF_PROG_INSTRUCTOR_ID: TIntegerField;
    cdsListF_INSTRUCTOR_ID: TIntegerField;
    cdsListF_FULLNAME: TStringField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_GSM: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsListF_PROGRAM_ID: TIntegerField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_ACTIVE: TBooleanField;
    cdsRecordF_PROG_INSTRUCTOR_ID: TIntegerField;
    cdsRecordF_INSTRUCTOR_ID: TIntegerField;
    cdsRecordF_FULLNAME: TStringField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_GSM: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_PROGRAM_ID: TIntegerField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_ACTIVE: TBooleanField;
    qryListF_LASTNAME: TStringField;
    qryListF_FIRSTNAME: TStringField;
    cdsRecordF_NAME: TStringField;
    cdsListF_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);

  private
    { Private declarations }

  protected
    { Protected declarations }
    procedure SelectInstructor ( aDataSet : TDataSet ); virtual;
    procedure SelectProgram    ( aDataSet : TDataSet ); virtual;
    procedure ShowInstructor   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowProgram      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

  public
    { Public declarations }
    procedure Add; override;

    class procedure SelectProgInstructor(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure ShowProgInstructor  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  end;

  procedure CopyProgInstructorFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_Instructor, Data_ProgProgram, DateUtils,
  Data_FVBFFCBaseDataModule;

{*****************************************************************************
  This procedure will be used to copy ProgInstructor related fields from one
  DataSet to another DataSet.

  @Name       CopyProgInstructorFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyProgInstructorFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_PROG_INSTRUCTOR_ID' ).AsInteger :=
        aSource.FieldByName( 'F_PROG_INSTRUCTOR_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
//    aDestination.FieldByName( 'F_PROGINSTRUCTOR_NAME' ).AsString :=
//      aSource.FieldByName( 'F_PROGINSTRUCTOR_NAME' ).AsString;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more ProgInstructor Records.

  @Name       TdtmProgInstructor.SelectProgInstructor
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgInstructor.SelectProgInstructor(
  aDataSet : TDataSet;
  aWhereClause : String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False
  );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmProgInstructor', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyProgInstructorFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single ProgInstructor
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmProgInstructor.ShowProgInstructor
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgInstructor.ShowProgInstructor(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmProgInstructor', 'F_PROG_INSTRUCTOR_ID = ' + aDataSet.FieldByName( 'F_PROG_INSTRUCTOR_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyProgInstructorFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ) );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmProgInstructor.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_PROG_INSTRUCTOR';
end;

{*****************************************************************************
  This method will be used to allow the user to Select an Instructor.

  @Name       TdtmProgInstructor.SelectInstructor
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.SelectInstructor(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) ) then
  begin
    { Get the ID Field }
    aIDField := aDataSet.FindField( 'F_INSTRUCTOR_ID' );
    aWhere   := '';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Diploma isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_INSTRUCTOR_ID <> ' + aIDField.AsString;
    end;

    TdtmInstructor.SelectInstructor( aDataSet, aWhere );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Select a Program.

  @Name       TdtmProgInstructor.SelectProgram
  @author     slesage
  @param      aDataSet   The DataSet in which we should copy some information
                         from the Selected Record.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.SelectProgram(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) ) then
  begin
    { Get the ID Field }
    aIDField := aDataSet.FindField( 'F_PROGRAM_ID' );
    aWhere   := '';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Diploma isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := 'F_PROGRAM_ID <> ' + aIDField.AsString;
    end;

    TdtmProgProgram.SelectProgram( aDataSet, aWhere, False, critProgramToProgInstructor );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show an Instructor.

  @Name       TdtmProgInstructor.ShowInstructor
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.ShowInstructor(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) ) then
  begin
    { Get the ID Field }
    aIDField := aDataSet.FindField( 'F_INSTRUCTOR_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmInstructor.ShowInstructor( aDataSet,
                                     aRecordViewMode,
                                     critInstructorToProgInstructor
                                     );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Program.

  @Name       TdtmProgInstructor.ShowProgram
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.ShowProgram(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) ) then
  begin
    { Get the ID Field }
    aIDField := aDataSet.FindField( 'F_PROGRAM_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmProgProgram.ShowProgram( aDataSet, aRecordViewMode );
    end;
  end;
end;

{*****************************************************************************
  Overridden Add method in which we will allow the user to select a number of
  Program or Instructor records if the DataModule is shown as a detail of one
  of those.

  @Name       TdtmProgInstructor.Add
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.Add;
begin
  Inherited;
{
  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) then
    begin
      TdtmInstructor.LinkInstructorsToProgram( cdsList, '', True );
    end
    else if ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) then
    begin
      TdtmProgProgram.LinkProgramsToInstructor( cdsList, '', True );
    end
    else
    begin
      Inherited Add;
    end;
  end
  else
  begin
    Inherited Add;
  end;}
end;

{*****************************************************************************
  This method will be used to initialise some Foreign Key Fields based on the
  Master RecordDataSet.

  @Name       TdtmProgInstructor.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which we should initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
//  Inherited FVBFFCDataModuleInitialiseForeignKeyFields( aDataSet );
  Inherited;

  { Check if the DataModule was used as a Detail of another DataModule }
  if ( Assigned( MasterDataModule ) ) then
  begin
    { If it is used as a detail of an Instructor DataModule, copy some
      Instructor Fields }
    if ( Supports( MasterDataModule, IEDUDocInstructorDataModule ) ) then
    begin
       aDataSet.FieldByName( 'F_FULLNAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_FULLNAME' ).AsString;
       aDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_SOCSEC_NR' ).AsString;
       aDataSet.FieldByName( 'F_PHONE' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_PHONE' ).AsString;
       aDataSet.FieldByName( 'F_FAX' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_FAX' ).AsString;
       aDataSet.FieldByName( 'F_GSM' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_GSM' ).AsString;
       aDataSet.FieldByName( 'F_EMAIL' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_EMAIL' ).AsString;
       aDataSet.FieldByName( 'F_LASTNAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_LASTNAME' ).AsString;
       aDataSet.FieldByName( 'F_FIRSTNAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_FIRSTNAME' ).AsString;
    end
    { If it is used as a detail of a Program DataModule, copy some
      Program Fields }
    else if ( Supports( MasterDataModule, IEDUProgProgramDataModule ) ) then
    begin
//       aDataSet.FieldByName( 'F_PROGRAM_ID' ).AsInteger :=
//         MasterDataModule.RecordDataset.FieldByName( 'F_PROGRAM_ID' ).AsInteger;

       aDataSet.FieldByName( 'F_NAME' ).AsString :=
         MasterDataModule.RecordDataset.FieldByName( 'F_NAME' ).AsString;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to initialise some Additional Fields.

  @Name       TdtmProgInstructor.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which we should initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
//  inherited FVBFFCDataModuleInitialiseAdditionalFields( aDataSet );
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_START_DATE' ).AsDateTime := Today;
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmProgInstructor.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which we should initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmProgInstructor.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PROG_INSTRUCTOR_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

end.
