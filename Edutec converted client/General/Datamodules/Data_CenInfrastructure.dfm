inherited dtmCenInfrastructure: TdtmCenInfrastructure
  AutoOpenDataSets = False
  KeyFields = 'F_INFRASTRUCTURE_ID'
  ListViewClass = 'TfrmCenInfrastructure_List'
  RecordViewClass = 'TfrmCenInfrastructure_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Lokalen'
      ForeignKeys = 'F_INFRASTRUCTURE_ID'
      DataModuleClass = 'TdtmCenRoom'
      AutoCreate = False
    end
    item
      Caption = 'Historiek'
      ForeignKeys = 'F_INFRASTRUCTURE_ID'
      DataModuleClass = 'TdtmLogHistory'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME, '
      '  F_FVB_CENTER_ID, '
      '  F_RSZ_NR, '
      '  F_VAT_NR, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_EMAIL, '
      '  F_WEBSITE, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME ,'
      '  F_ACC_NR, '
      '  F_ACC_HOLDER, '
      '  F_MEDIUM_ID,'
      '  F_MEDIUM_NAME, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_COMMENT, '
      '  F_SATURDAY, '
      '  F_EVENING, '
      '  F_WEEKDAY, '
      '  F_LOCATION_ID, '
      '  F_LOCATION, '
      '  F_PRECONDITION_ID, '
      '  F_PRECONDITION,'
      '  F_MAIL_ATTACHMENT '
      'FROM '
      '  V_CEN_INFRASTRUCTURE')
    object qryListF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_FVB_CENTER_ID: TIntegerField
      FieldName = 'F_FVB_CENTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object qryListF_VAT_NR: TStringField
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object qryListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object qryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object qryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object qryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_WEBSITE: TStringField
      FieldName = 'F_WEBSITE'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object qryListF_ACC_HOLDER: TStringField
      FieldName = 'F_ACC_HOLDER'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object qryListF_SATURDAY: TBooleanField
      FieldName = 'F_SATURDAY'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_EVENING: TBooleanField
      FieldName = 'F_EVENING'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_WEEKDAY: TBooleanField
      FieldName = 'F_WEEKDAY'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_LOCATION_ID: TIntegerField
      FieldName = 'F_LOCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_LOCATION: TStringField
      FieldName = 'F_LOCATION'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_PRECONDITION_ID: TIntegerField
      FieldName = 'F_PRECONDITION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_PRECONDITION: TStringField
      FieldName = 'F_PRECONDITION'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = [pfInUpdate]
      Size = 512
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME, '
      '  F_FVB_CENTER_ID, '
      '  F_RSZ_NR, '
      '  F_VAT_NR, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_EMAIL, '
      '  F_WEBSITE, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME ,'
      '  F_ACC_NR, '
      '  F_ACC_HOLDER, '
      '  F_MEDIUM_ID,'
      '  F_MEDIUM_NAME, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_COMMENT, '
      '  F_SATURDAY, '
      '  F_EVENING, '
      '  F_WEEKDAY, '
      '  F_LOCATION_ID, '
      '  F_LOCATION, '
      '  F_PRECONDITION_ID, '
      '  F_PRECONDITION,'
      '  F_MAIL_ATTACHMENT '
      'FROM '
      '  V_CEN_INFRASTRUCTURE')
    object qryRecordF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_FVB_CENTER_ID: TIntegerField
      FieldName = 'F_FVB_CENTER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_RSZ_NR: TStringField
      FieldName = 'F_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object qryRecordF_VAT_NR: TStringField
      FieldName = 'F_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object qryRecordF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object qryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object qryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object qryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_WEBSITE: TStringField
      FieldName = 'F_WEBSITE'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object qryRecordF_ACC_HOLDER: TStringField
      FieldName = 'F_ACC_HOLDER'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object qryRecordF_SATURDAY: TBooleanField
      FieldName = 'F_SATURDAY'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_EVENING: TBooleanField
      FieldName = 'F_EVENING'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_WEEKDAY: TBooleanField
      FieldName = 'F_WEEKDAY'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_LOCATION_ID: TIntegerField
      FieldName = 'F_LOCATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_LOCATION: TStringField
      FieldName = 'F_LOCATION'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_PRECONDITION_ID: TIntegerField
      FieldName = 'F_PRECONDITION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_PRECONDITION: TStringField
      FieldName = 'F_PRECONDITION'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_MAIL_ATTACHMENT: TStringField
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = [pfInUpdate]
      Size = 512
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Infrastructuur ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsListF_FVB_CENTER_ID: TIntegerField
      FieldName = 'F_FVB_CENTER_ID'
      Visible = False
    end
    object cdsListF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ Nr'
      FieldName = 'F_RSZ_NR'
      Size = 14
    end
    object cdsListF_VAT_NR: TStringField
      DisplayLabel = 'BTW Nr'
      FieldName = 'F_VAT_NR'
    end
    object cdsListF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsListF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsListF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsListF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Visible = False
    end
    object cdsListF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsListF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
    object cdsListF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsListF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsListF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsListF_WEBSITE: TStringField
      DisplayLabel = 'WebSite'
      FieldName = 'F_WEBSITE'
      Size = 128
    end
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Visible = False
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsListF_ACC_NR: TStringField
      DisplayLabel = 'Rekening Nr'
      FieldName = 'F_ACC_NR'
      Size = 12
    end
    object cdsListF_ACC_HOLDER: TStringField
      DisplayLabel = 'Rekening Houder'
      FieldName = 'F_ACC_HOLDER'
      Size = 64
    end
    object cdsListF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      Visible = False
    end
    object cdsListF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      Visible = False
      Size = 64
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      Visible = False
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      Visible = False
    end
    object cdsListF_COMMENT: TMemoField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_COMMENT'
      Visible = False
      BlobType = ftMemo
    end
    object cdsListF_SATURDAY: TBooleanField
      DisplayLabel = 'Zaterdag'
      FieldName = 'F_SATURDAY'
      Visible = False
    end
    object cdsListF_EVENING: TBooleanField
      DisplayLabel = 'Avond'
      FieldName = 'F_EVENING'
      Visible = False
    end
    object cdsListF_WEEKDAY: TBooleanField
      DisplayLabel = 'Weekdag'
      FieldName = 'F_WEEKDAY'
      Visible = False
    end
    object cdsListF_LOCATION_ID: TIntegerField
      DisplayLabel = 'Locatie ( ID )'
      FieldName = 'F_LOCATION_ID'
      Visible = False
    end
    object cdsListF_LOCATION: TStringField
      DisplayLabel = 'Locatie'
      FieldName = 'F_LOCATION'
      Visible = False
      Size = 64
    end
    object cdsListF_PRECONDITION_ID: TIntegerField
      DisplayLabel = 'Voorwaarde ( ID )'
      FieldName = 'F_PRECONDITION_ID'
      Visible = False
    end
    object cdsListF_PRECONDITION: TStringField
      DisplayLabel = 'Voorwaarde'
      FieldName = 'F_PRECONDITION'
      Visible = False
      Size = 64
    end
    object cdsListF_MAIL_ATTACHMENT: TStringField
      DisplayLabel = 'E-Mail bijlage'
      FieldName = 'F_MAIL_ATTACHMENT'
      Size = 512
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Infrastructuur ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
      Required = True
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_FVB_CENTER_ID: TIntegerField
      DisplayLabel = 'FVB Centrum ( ID )'
      FieldName = 'F_FVB_CENTER_ID'
    end
    object cdsRecordF_RSZ_NR: TStringField
      DisplayLabel = 'RSZ Nr'
      FieldName = 'F_RSZ_NR'
      ProviderFlags = []
      Visible = False
      Size = 14
    end
    object cdsRecordF_VAT_NR: TStringField
      DisplayLabel = 'BTW Nr'
      FieldName = 'F_VAT_NR'
    end
    object cdsRecordF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsRecordF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsRecordF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsRecordF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Visible = False
    end
    object cdsRecordF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Required = True
      Size = 10
    end
    object cdsRecordF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Required = True
      Size = 128
    end
    object cdsRecordF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsRecordF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      ProviderFlags = []
      Visible = False
      Size = 128
    end
    object cdsRecordF_WEBSITE: TStringField
      DisplayLabel = 'WebSite'
      FieldName = 'F_WEBSITE'
      Size = 128
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object cdsRecordF_ACC_NR: TStringField
      DisplayLabel = 'Rekening Nr'
      FieldName = 'F_ACC_NR'
      Size = 12
    end
    object cdsRecordF_ACC_HOLDER: TStringField
      DisplayLabel = 'Rekening Houder'
      FieldName = 'F_ACC_HOLDER'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object cdsRecordF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = []
      Visible = False
    end
    object cdsRecordF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Visible = False
      Size = 64
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      Visible = False
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      Visible = False
    end
    object cdsRecordF_COMMENT: TMemoField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_SATURDAY: TBooleanField
      DisplayLabel = 'Zaterdag'
      FieldName = 'F_SATURDAY'
      ProviderFlags = []
      Visible = False
    end
    object cdsRecordF_EVENING: TBooleanField
      DisplayLabel = 'Avond'
      FieldName = 'F_EVENING'
      ProviderFlags = []
      Visible = False
    end
    object cdsRecordF_WEEKDAY: TBooleanField
      DisplayLabel = 'Weekdag'
      FieldName = 'F_WEEKDAY'
      ProviderFlags = []
      Visible = False
    end
    object cdsRecordF_LOCATION_ID: TIntegerField
      DisplayLabel = 'Locatie ( ID )'
      FieldName = 'F_LOCATION_ID'
      Visible = False
    end
    object cdsRecordF_LOCATION: TStringField
      DisplayLabel = 'Locatie'
      FieldName = 'F_LOCATION'
      Size = 64
    end
    object cdsRecordF_PRECONDITION_ID: TIntegerField
      DisplayLabel = 'Voorwaarde ( ID )'
      FieldName = 'F_PRECONDITION_ID'
      Visible = False
    end
    object cdsRecordF_PRECONDITION: TStringField
      DisplayLabel = 'Voorwaarde'
      FieldName = 'F_PRECONDITION'
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT1: TStringField
      DisplayLabel = 'Contactpersoon 1'
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      DisplayLabel = 'EMail contactpersoon 1'
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_CONFIRMATION_CONTACT2: TStringField
      DisplayLabel = 'Contactpersoon 2'
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      DisplayLabel = 'EMail contactpersoon 2'
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_CONFIRMATION_CONTACT3: TStringField
      DisplayLabel = 'Contactpersoon 3'
      FieldName = 'F_CONFIRMATION_CONTACT3'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField
      DisplayLabel = 'EMail contactpersoon 3'
      FieldName = 'F_CONFIRMATION_CONTACT3_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_INFO: TMemoField
      DisplayLabel = 'Info'
      FieldName = 'F_INFO'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsRecordF_MAIL_ATTACHMENT: TStringField
      DisplayLabel = 'E-Mail bijlage'
      FieldName = 'F_MAIL_ATTACHMENT'
      ProviderFlags = [pfInUpdate]
      Size = 512
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsSearchCriteriaF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
    object cdsSearchCriteriaF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmCenInfrastructure'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmCenInfrastructure'
  end
end
