inherited dtmInstructor: TdtmInstructor
  AutoOpenDataSets = False
  KeyFields = 'F_INSTRUCTOR_ID'
  ListViewClass = 'TfrmInstructor_List'
  RecordViewClass = 'TfrmInstructor_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Height = 257
  Width = 364
  DetailDataModules = <
    item
      Caption = 'Programmas'
      ForeignKeys = 'F_INSTRUCTOR_ID'
      DataModuleClass = 'TdtmProgInstructor'
      AutoCreate = False
    end
    item
      Caption = 'Sessies'
      ForeignKeys = 'F_INSTRUCTOR_ID'
      DataModuleClass = 'TdtmDocCenter'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INSTRUCTOR_ID, '
      '  F_FULLNAME,'
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_ACC_NR,'
      '  F_SOCSEC_NR,'
      '  F_PHONE,'
      '  F_FAX,'
      '  F_EMAIL,'
      '  F_GSM,'
      ' F_ACTIVE'
      'FROM '
      '  V_DOC_INSTRUCTOR')
    object qryListF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object qryListF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object qryListF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object qryListF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object qryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object qryListF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object qryListF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object qryListF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_INSTRUCTOR_ID, '
      '  F_TITLE_ID, '
      '  F_TITLE_NAME, '
      '  F_FULLNAME,'
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_STREET, '
      '  F_NUMBER, '
      '  F_MAILBOX, '
      '  F_POSTALCODE_ID, '
      '  F_POSTALCODE, '
      '  F_CITY_NAME, '
      '  F_COUNTRY_ID, '
      '  F_COUNTRY_NAME, '
      '  F_PHONE, '
      '  F_FAX, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_LANGUAGE_ID, '
      '  F_LANGUAGE_NAME, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME, '
      '  F_MEDIUM_ID, '
      '  F_MEDIUM_NAME, '
      '  F_ACC_NR,'
      '  F_FOREIGN_ACC_NR, '
      '  F_SOCSEC_NR, '
      '  F_DATEBIRTH, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_DUTCH, '
      '  F_FRENCH, '
      '  F_ENGLISH, '
      '  F_GERMAN, '
      '  F_OTHER_LANGUAGE, '
      '  F_DIPLOMA_ID, '
      '  F_DIPLOMA_NAME, '
      '  F_COMMENT, '
      '  F_CODE, '
      '  F_CV_ID, '
      '  F_DOCUMENT_NAME,'
      '  [F_ORG_LESGEVER],'
      '  [F_ORG_CONTACT1],'
      '  [F_ORG_CONTACT2],'
      '  [F_ORG_CONTACT1_EMAIL],'
      '  [F_ORG_CONTACT2_EMAIL] '
      'FROM '
      '  V_DOC_INSTRUCTOR')
    object qryRecordF_INSTRUCTOR_ID: TIntegerField
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_TITLE_ID: TIntegerField
      FieldName = 'F_TITLE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_TITLE_NAME: TStringField
      FieldName = 'F_TITLE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_STREET: TStringField
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_NUMBER: TStringField
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object qryRecordF_MAILBOX: TStringField
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object qryRecordF_POSTALCODE_ID: TIntegerField
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_POSTALCODE: TStringField
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object qryRecordF_CITY_NAME: TStringField
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object qryRecordF_COUNTRY_ID: TIntegerField
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = []
    end
    object qryRecordF_COUNTRY_NAME: TStringField
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_PHONE: TStringField
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_FAX: TStringField
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_ACC_NR: TStringField
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object qryRecordF_FOREIGN_ACC_NR: TStringField
      FieldName = 'F_FOREIGN_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object qryRecordF_SOCSEC_NR: TStringField
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object qryRecordF_DATEBIRTH: TDateTimeField
      FieldName = 'F_DATEBIRTH'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_DUTCH: TBooleanField
      FieldName = 'F_DUTCH'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_FRENCH: TBooleanField
      FieldName = 'F_FRENCH'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_ENGLISH: TBooleanField
      FieldName = 'F_ENGLISH'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_GERMAN: TBooleanField
      FieldName = 'F_GERMAN'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_OTHER_LANGUAGE: TStringField
      FieldName = 'F_OTHER_LANGUAGE'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object qryRecordF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_COMMENT: TMemoField
      FieldName = 'F_COMMENT'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object qryRecordF_CODE: TStringField
      FieldName = 'F_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_CV_ID: TIntegerField
      FieldName = 'F_CV_ID'
      ProviderFlags = []
    end
    object qryRecordF_DOCUMENT_NAME: TStringField
      FieldName = 'F_DOCUMENT_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object qryRecordF_ORG_LESGEVER: TStringField
      FieldName = 'F_ORG_LESGEVER'
      Size = 64
    end
    object qryRecordF_ORG_CONTACT1: TStringField
      FieldName = 'F_ORG_CONTACT1'
      Size = 64
    end
    object qryRecordF_ORG_CONTACT2: TStringField
      FieldName = 'F_ORG_CONTACT2'
      Size = 64
    end
    object qryRecordF_ORG_CONTACT1_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT1_EMAIL'
      Size = 128
    end
    object qryRecordF_ORG_CONTACT2_EMAIL: TStringField
      FieldName = 'F_ORG_CONTACT2_EMAIL'
      Size = 128
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever ( ID )'
      FieldName = 'F_INSTRUCTOR_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      ProviderFlags = [pfInUpdate]
      Size = 5
    end
    object cdsListF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsListF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object cdsListF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_ACC_NR: TStringField
      DisplayLabel = 'Rekening Nr'
      FieldName = 'F_ACC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 12
    end
    object cdsListF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      ProviderFlags = []
      Size = 130
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksregsiter Nr.'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object cdsListF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_EMAIL: TStringField
      DisplayLabel = 'e-mail'
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_GSM: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_BTW: TStringField
      DisplayLabel = 'BTW-nummer'
      FieldName = 'F_BTW'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ORGANISATION_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORGANISATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever ( ID )'
      FieldName = 'F_INSTRUCTOR_ID'
      Required = True
    end
    object cdsRecordF_TITLE_ID: TIntegerField
      DisplayLabel = 'Titel ( ID )'
      FieldName = 'F_TITLE_ID'
    end
    object cdsRecordF_TITLE_NAME: TStringField
      DisplayLabel = 'Titel'
      FieldName = 'F_TITLE_NAME'
      Size = 64
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsRecordF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsRecordF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsRecordF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsRecordF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Required = True
      Size = 10
    end
    object cdsRecordF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Required = True
      Size = 128
    end
    object cdsRecordF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsRecordF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsRecordF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Required = True
      Size = 64
    end
    object cdsRecordF_GENDER_ID: TIntegerField
      DisplayLabel = 'Geslacht ( ID )'
      FieldName = 'F_GENDER_ID'
      Visible = False
    end
    object cdsRecordF_GENDER_NAME: TStringField
      DisplayLabel = 'Geslacht'
      FieldName = 'F_GENDER_NAME'
      Size = 64
    end
    object cdsRecordF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      Visible = False
    end
    object cdsRecordF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      Size = 64
    end
    object cdsRecordF_ACC_NR: TStringField
      DisplayLabel = 'Rekening Nr'
      FieldName = 'F_ACC_NR'
      Size = 14
    end
    object cdsRecordF_FOREIGN_ACC_NR: TStringField
      DisplayLabel = 'Buitenlands Rekening Nr.'
      FieldName = 'F_FOREIGN_ACC_NR'
      Size = 30
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksregister Nr.'
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsRecordF_DATEBIRTH: TDateTimeField
      DisplayLabel = 'Geboortedatum'
      FieldName = 'F_DATEBIRTH'
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsRecordF_DUTCH: TBooleanField
      DisplayLabel = 'Nederlands'
      FieldName = 'F_DUTCH'
    end
    object cdsRecordF_FRENCH: TBooleanField
      DisplayLabel = 'Frans'
      FieldName = 'F_FRENCH'
    end
    object cdsRecordF_ENGLISH: TBooleanField
      DisplayLabel = 'Engels'
      FieldName = 'F_ENGLISH'
    end
    object cdsRecordF_GERMAN: TBooleanField
      DisplayLabel = 'Duits'
      FieldName = 'F_GERMAN'
    end
    object cdsRecordF_OTHER_LANGUAGE: TStringField
      DisplayLabel = 'Andere'
      FieldName = 'F_OTHER_LANGUAGE'
      Size = 50
    end
    object cdsRecordF_DIPLOMA_ID: TIntegerField
      DisplayLabel = 'Diploma ( ID )'
      FieldName = 'F_DIPLOMA_ID'
      Visible = False
    end
    object cdsRecordF_DIPLOMA_NAME: TStringField
      DisplayLabel = 'Diploma'
      FieldName = 'F_DIPLOMA_NAME'
      Size = 64
    end
    object cdsRecordF_COMMENT: TMemoField
      DisplayLabel = 'Commentaar'
      FieldName = 'F_COMMENT'
      BlobType = ftMemo
    end
    object cdsRecordF_CODE: TStringField
      DisplayLabel = 'Code'
      FieldName = 'F_CODE'
    end
    object cdsRecordF_CV_ID: TIntegerField
      DisplayLabel = 'CV ( ID )'
      FieldName = 'F_CV_ID'
      Visible = False
    end
    object cdsRecordF_DOCUMENT_NAME: TStringField
      DisplayLabel = 'CV'
      FieldName = 'F_DOCUMENT_NAME'
      Size = 64
    end
    object cdsRecordF_FULLNAME: TStringField
      FieldName = 'F_FULLNAME'
      Size = 130
    end
    object cdsRecordF_BTW: TStringField
      DisplayLabel = 'BTW-nummer'
      FieldName = 'F_BTW'
    end
    object cdsRecordF_ORGANISATION_NAME: TStringField
      FieldName = 'F_ORGANISATION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_CONFIRMATION_CONTACT1: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT1_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_CONFIRMATION_CONTACT2: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField
      FieldName = 'F_CONFIRMATION_CONTACT2_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaF_INSTRUCTOR_ID: TIntegerField
      DisplayLabel = 'Lesgever ( ID )'
      FieldName = 'F_INSTRUCTOR_ID'
    end
    object cdsSearchCriteriaF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsSearchCriteriaF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsSearchCriteriaF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_CODE: TStringField
      DisplayLabel = 'Code'
      FieldName = 'F_CODE'
    end
    object cdsSearchCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmInstructor'
    Left = 24
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmInstructor'
  end
end
