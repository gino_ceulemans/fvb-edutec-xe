{*****************************************************************************
  This DataModule will be used for the Maintenance of T_DOC_DIPLOMA records.

  @Name       Data_Diploma
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  19/07/2005   sLesage              Added SelectDiploma and ShowDiploma
                                    class procedures.
  01/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Diploma;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_PPWFrameWorkClasses,
  MConnect, Datasnap.DSConnect;

type
  TdtmDiploma = class(TEDUDataModule)
    qryListF_DIPLOMA_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_DIPLOMA_NAME: TStringField;
    qryRecordF_DIPLOMA_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_DIPLOMA_NAME: TStringField;
    cdsListF_DIPLOMA_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_DIPLOMA_NAME: TStringField;
    cdsRecordF_DIPLOMA_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_DIPLOMA_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectDiploma( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False    ); virtual;
    class procedure ShowDiploma  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  end;

var
  dtmDiploma: TdtmDiploma;

implementation

uses Data_EduMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Diploma related fields from one
  DataSet to another DataSet.

  @Name       CopyDiplomaFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyDiplomaFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_DIPLOMA_ID' ).AsInteger :=
        aSource.FieldByName( 'F_DIPLOMA_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    aDestination.FieldByName( 'F_DIPLOMA_NAME' ).AsString :=
      aSource.FieldByName( 'F_DIPLOMA_NAME' ).AsString;
  end;
end;

procedure TdtmDiploma.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_DOC_DIPLOMA';
end;

{*****************************************************************************
  This class procedure will be used to select one or more Diploma Records.

  @Name       TdtmDiploma.SelectDiploma
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmDiploma.SelectDiploma(aDataSet: TDataSet;
  aWhere: String; aMultiSelect: Boolean);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmDiploma', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit; 
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyDiplomaFieldValues( aSelectedRecords, aDataSet, True );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Diploma
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmDiploma.ShowDiploma
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmDiploma.ShowDiploma(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmDiploma', 'F_DIPLOMA_ID = ' + aDataSet.FieldByName( 'F_DIPLOMA_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyDiplomaFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ) );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmDiploma.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmDiploma.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_DIPLOMA_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

end.