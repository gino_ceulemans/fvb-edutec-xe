{*****************************************************************************
  This DataModule will be used for the maintenance of T_GE_STATUS.

  @Name       Data_Status
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Status;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmStatus = class(TEduDataModule, IEDUStatusDataModule)
    qryListF_STATUS_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_LONG_DESC_NL: TStringField;
    qryListF_LONG_DESC_FR: TStringField;
    qryListF_SESSION: TBooleanField;
    qryListF_ENROL: TBooleanField;
    qryListF_WAITING_LIST: TBooleanField;
    qryListF_FOREGROUNDCOLOR: TIntegerField;
    qryListF_BACKGROUNDCOLOR: TIntegerField;
    qryListF_STATUS_NAME: TStringField;
    qryRecordF_STATUS_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_LONG_DESC_NL: TStringField;
    qryRecordF_LONG_DESC_FR: TStringField;
    qryRecordF_SESSION: TBooleanField;
    qryRecordF_ENROL: TBooleanField;
    qryRecordF_WAITING_LIST: TBooleanField;
    qryRecordF_FOREGROUNDCOLOR: TIntegerField;
    qryRecordF_BACKGROUNDCOLOR: TIntegerField;
    qryRecordF_STATUS_NAME: TStringField;
    cdsListF_STATUS_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_LONG_DESC_NL: TStringField;
    cdsListF_LONG_DESC_FR: TStringField;
    cdsListF_SESSION: TBooleanField;
    cdsListF_ENROL: TBooleanField;
    cdsListF_WAITING_LIST: TBooleanField;
    cdsListF_FOREGROUNDCOLOR: TIntegerField;
    cdsListF_BACKGROUNDCOLOR: TIntegerField;
    cdsListF_STATUS_NAME: TStringField;
    cdsRecordF_STATUS_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_LONG_DESC_NL: TStringField;
    cdsRecordF_LONG_DESC_FR: TStringField;
    cdsRecordF_SESSION: TBooleanField;
    cdsRecordF_ENROL: TBooleanField;
    cdsRecordF_WAITING_LIST: TBooleanField;
    cdsRecordF_FOREGROUNDCOLOR: TIntegerField;
    cdsRecordF_BACKGROUNDCOLOR: TIntegerField;
    cdsRecordF_STATUS_NAME: TStringField;
    qryListF_LONG_DESC: TStringField;
    qryRecordF_LONG_DESC: TStringField;
    cdsRecordF_LONG_DESC: TStringField;
    cdsListF_LONG_DESC: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectStatus( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowStatus  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient;

{*****************************************************************************
  This procedure will be used to copy Status related fields from one
  DataSet to another DataSet.

  @Name       CopyStatusFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyStatusFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_STATUS_ID' ).AsInteger :=
        aSource.FieldByName( 'F_STATUS_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_STATUS_NAME' ).AsString :=
          aSource.FieldByName( 'F_STATUS_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Status Records.

  @Name       TdtmStatus.SelectStatus
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmStatus.SelectStatus(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmStatus', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyStatusFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Status
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmStatus.ShowStatus
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmStatus.ShowStatus(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmStatus', 'F_STATUS_ID = ' + aDataSet.FieldByName( 'F_STATUS_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyStatusFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmStatus.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_GE_STATUS';
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmStatus.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmStatus.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_STATUS_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be executed when the Additonal Fields are initialised.

  @Name       TdtmStatus.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which Additional Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmStatus.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ENROL' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_WAITING_LIST' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_SESSION' ).AsBoolean := False;
end;

end.
