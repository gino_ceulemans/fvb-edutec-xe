inherited dtmUser: TdtmUser
  AutoOpenDataSets = False
  KeyFields = 'F_USER_ID'
  ListViewClass = 'TfrmUser_List'
  RecordViewClass = 'TfrmUser_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'GebruikersProfielen'
      ForeignKeys = 'F_USER_ID'
      DataModuleClass = 'TdtmUserProfile'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_PHONE_INT, '
      '  F_PHONE_EXT, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_LOGIN_ID, '
      '  F_ACTIVE, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME, '
      '  F_LOCKED, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_LOGIN_TRY, '
      '  F_DOMAIN, '
      '  F_LANGUAGE_ID,'
      '  F_LANGUAGE_NAME, '
      '  F_PASSWORD,'
      '  F_COMPLETE_USERNAME '
      'FROM '
      '  V_SYS_USER')
    object qryListF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_PHONE_INT: TStringField
      FieldName = 'F_PHONE_INT'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_PHONE_EXT: TStringField
      FieldName = 'F_PHONE_EXT'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_LOGIN_ID: TStringField
      FieldName = 'F_LOGIN_ID'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object qryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_LOCKED: TBooleanField
      FieldName = 'F_LOCKED'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_LOGIN_TRY: TSmallintField
      FieldName = 'F_LOGIN_TRY'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_DOMAIN: TStringField
      FieldName = 'F_DOMAIN'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object qryListF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object qryListF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 232
    end
    object qryListF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_PHONE_INT, '
      '  F_PHONE_EXT, '
      '  F_GSM, '
      '  F_EMAIL, '
      '  F_LOGIN_ID, '
      '  F_ACTIVE, '
      '  F_GENDER_ID, '
      '  F_GENDER_NAME, '
      '  F_LOCKED, '
      '  F_START_DATE, '
      '  F_END_DATE, '
      '  F_LOGIN_TRY, '
      '  F_DOMAIN, '
      '  F_LANGUAGE_ID,'
      '  F_LANGUAGE_NAME, '
      '  F_PASSWORD,'
      '  F_COMPLETE_USERNAME ,'
      '  F_PRINTER_LETTER,'
      '  F_PRINTER_CERTIFICATE'
      'FROM '
      '  V_SYS_USER')
    object qryRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_PHONE_INT: TStringField
      FieldName = 'F_PHONE_INT'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_PHONE_EXT: TStringField
      FieldName = 'F_PHONE_EXT'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_GSM: TStringField
      FieldName = 'F_GSM'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_LOGIN_ID: TStringField
      FieldName = 'F_LOGIN_ID'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object qryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_GENDER_ID: TIntegerField
      FieldName = 'F_GENDER_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_LOCKED: TBooleanField
      FieldName = 'F_LOCKED'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_START_DATE: TDateTimeField
      FieldName = 'F_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_LOGIN_TRY: TSmallintField
      FieldName = 'F_LOGIN_TRY'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_DOMAIN: TStringField
      FieldName = 'F_DOMAIN'
      ProviderFlags = [pfInUpdate]
      Size = 100
    end
    object qryRecordF_LANGUAGE_NAME: TStringField
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 64
    end
    object qryRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 232
    end
    object qryRecordF_LANGUAGE_ID: TIntegerField
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_GENDER_NAME: TStringField
      FieldName = 'F_GENDER_NAME'
      ReadOnly = True
      Size = 64
    end
    object qryRecordF_PASSWORD: TStringField
      FieldName = 'F_PASSWORD'
    end
    object qryRecordF_PRINTER_LETTER: TStringField
      FieldName = 'F_PRINTER_LETTER'
      Size = 50
    end
    object qryRecordF_PRINTER_CERTIFICATE: TStringField
      FieldName = 'F_PRINTER_CERTIFICATE'
      Size = 50
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_USER_ID: TIntegerField
      DisplayLabel = 'Gebruiker ( ID )'
      FieldName = 'F_USER_ID'
      Required = True
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsListF_PHONE_INT: TStringField
      DisplayLabel = 'Telefoon ( int )'
      FieldName = 'F_PHONE_INT'
      Visible = False
    end
    object cdsListF_PHONE_EXT: TStringField
      DisplayLabel = 'Telefoon ( ext )'
      FieldName = 'F_PHONE_EXT'
      Visible = False
    end
    object cdsListF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
      Visible = False
    end
    object cdsListF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      Visible = False
      Size = 128
    end
    object cdsListF_LOGIN_ID: TStringField
      DisplayLabel = 'Login ID'
      FieldName = 'F_LOGIN_ID'
      Size = 100
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsListF_GENDER_ID: TIntegerField
      DisplayLabel = 'Geslacht ( ID )'
      FieldName = 'F_GENDER_ID'
      Visible = False
    end
    object cdsListF_LOCKED: TBooleanField
      DisplayLabel = 'Geblokkeerd'
      FieldName = 'F_LOCKED'
      Visible = False
    end
    object cdsListF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
      Visible = False
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      Visible = False
    end
    object cdsListF_LOGIN_TRY: TSmallintField
      FieldName = 'F_LOGIN_TRY'
      Visible = False
    end
    object cdsListF_DOMAIN: TStringField
      FieldName = 'F_DOMAIN'
      Visible = False
      Size = 100
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsListF_COMPLETE_USERNAME: TStringField
      DisplayLabel = 'Gebruiker'
      FieldName = 'F_COMPLETE_USERNAME'
      Size = 232
    end
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Required = True
    end
    object cdsListF_GENDER_NAME: TStringField
      DisplayLabel = 'Geslacht'
      FieldName = 'F_GENDER_NAME'
      Size = 64
    end
    object cdsListF_PASSWORD: TStringField
      FieldName = 'F_PASSWORD'
      Visible = False
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_USER_ID: TIntegerField
      DisplayLabel = 'Gebruiker ( ID )'
      FieldName = 'F_USER_ID'
      Required = True
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_PHONE_INT: TStringField
      DisplayLabel = 'Telefoon ( int )'
      FieldName = 'F_PHONE_INT'
    end
    object cdsRecordF_PHONE_EXT: TStringField
      DisplayLabel = 'Telefoon ( ext )'
      FieldName = 'F_PHONE_EXT'
    end
    object cdsRecordF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsRecordF_LOGIN_ID: TStringField
      DisplayLabel = 'Login ID'
      FieldName = 'F_LOGIN_ID'
      Required = True
      Size = 100
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_GENDER_ID: TIntegerField
      DisplayLabel = 'Geslacht ( ID )'
      FieldName = 'F_GENDER_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_LOCKED: TBooleanField
      DisplayLabel = 'Geblokkeerd'
      FieldName = 'F_LOCKED'
      Required = True
    end
    object cdsRecordF_START_DATE: TDateTimeField
      DisplayLabel = 'Start Datum'
      FieldName = 'F_START_DATE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsRecordF_LOGIN_TRY: TSmallintField
      DisplayLabel = 'Aanlogpogingen'
      FieldName = 'F_LOGIN_TRY'
      Required = True
    end
    object cdsRecordF_DOMAIN: TStringField
      DisplayLabel = 'Domein'
      FieldName = 'F_DOMAIN'
      Required = True
      Size = 100
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_COMPLETE_USERNAME: TStringField
      DisplayLabel = 'Gebruiker'
      FieldName = 'F_COMPLETE_USERNAME'
      Size = 232
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_GENDER_NAME: TStringField
      DisplayLabel = 'Geslacht'
      FieldName = 'F_GENDER_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_PASSWORD: TStringField
      FieldName = 'F_PASSWORD'
      Visible = False
    end
    object cdsRecordF_PRINTER_LETTER: TStringField
      FieldName = 'F_PRINTER_LETTER'
      Size = 50
    end
    object cdsRecordF_PRINTER_CERTIFICATE: TStringField
      FieldName = 'F_PRINTER_CERTIFICATE'
      Size = 50
    end
    object cdsRecordF_PATH_CONFIRMATIONS: TStringField
      FieldName = 'F_PATH_CONFIRMATIONS'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsRecordF_PATH_INVOICES: TStringField
      FieldName = 'F_PATH_INVOICES'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsRecordF_PRINTER_EVALUATION: TStringField
      FieldName = 'F_PRINTER_EVALUATION'
      ProviderFlags = [pfInUpdate]
      Size = 50
    end
    object cdsRecordF_PATH_CRREPORTS: TStringField
      FieldName = 'F_PATH_CRREPORTS'
      ProviderFlags = [pfInUpdate]
      Size = 255
    end
    object cdsRecordF_EMAIL_PUBLIC_FOLDER: TStringField
      FieldName = 'F_EMAIL_PUBLIC_FOLDER'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_DEFAULT_INFO_CONFIRMATION: TStringField
      FieldName = 'F_DEFAULT_INFO_CONFIRMATION'
      ProviderFlags = []
      ReadOnly = True
      Visible = False
      Size = 50
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSeachCriteriaNewRecord
    object cdsSeachCriteriaF_USER_ID: TIntegerField
      DisplayLabel = 'Gebruiker ( ID )'
      FieldName = 'F_USER_ID'
    end
    object cdsSeachCriteriaF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsSeachCriteriaF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsSeachCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Aktief'
      FieldName = 'F_ACTIVE'
    end
    object cdsSeachCriteriaF_LOGIN_ID: TStringField
      DisplayLabel = 'Login ID'
      FieldName = 'F_LOGIN_ID'
      Size = 100
    end
    object cdsSeachCriteriaF_LOCKED: TBooleanField
      DisplayLabel = 'Geblokkeerd'
      FieldName = 'F_LOCKED'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmUser'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmUser'
  end
end
