{*****************************************************************************
  This DataModule will be used for the Maintenance of T_PROG_PROFESSION records.

  @Name       Data_ProgDiscipline
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_ProgDiscipline;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmProgDiscipline = class(TEduDataModule, IEDUProgDisciplineDataModule)
    qryListF_PROFESSION_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_PROFESSION_NAME: TStringField;
    cdsListF_PROFESSION_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_PROFESSION_NAME: TStringField;
    cdsRecordF_PROFESSION_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_PROFESSION_NAME: TStringField;
    qryRecordF_PROFESSION_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_PROFESSION_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectDiscipline( aDataSet : TDataSet ); virtual;
    class procedure ShowDiscipline  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  end;

implementation

uses
  Data_EDUMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Discipline related fields from one
  DataSet to another DataSet.

  @Name       CopyDisciplineFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyDisciplineFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_PROFESSION_ID' ).AsInteger :=
        aSource.FieldByName( 'F_PROFESSION_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    aDestination.FieldByName( 'F_PROFESSION_NAME' ).AsString :=
      aSource.FieldByName( 'F_PROFESSION_NAME' ).AsString;
  end;
end;

procedure TdtmProgDiscipline.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_PROG_PROFESSION';
end;

{*****************************************************************************
  This class procedure will be used to select one or more Discipline Records.

  @Name       TdtmProgDiscipline.SelectDiscipline
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgDiscipline.SelectDiscipline(aDataSet: TDataSet);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the CountryPart ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmProgDiscipline', {'PE_IS_MILITANT = 1'} '' , aSelectedRecords, False ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyDisciplineFieldValues( aSelectedRecords, aDataSet, True );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Discipline
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmProgDiscipline.ShowDiscipline
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmProgDiscipline.ShowDiscipline(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView for TdtmPerson.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmProgDiscipline', 'F_PROFESSION_ID = ' + aDataSet.FieldByName( 'F_PROFESSION_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyDisciplineFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ) );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmProgDiscipline.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PROFESSION_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

end.