{*****************************************************************************
  This DataModule will be used for the maintenance of T_GE_TITLE.

  @Name       Data_Title
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Title;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EDUDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_PPWFrameWorkClasses,
  MConnect, Datasnap.DSConnect;

type
  TdtmTitle = class(TEduDataModule)
    qryListF_TITLE_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_TITLE_NAME: TStringField;
    qryRecordF_TITLE_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_TITLE_NAME: TStringField;
    cdsListF_TITLE_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_TITLE_NAME: TStringField;
    cdsRecordF_TITLE_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_TITLE_NAME: TStringField;
    cdsListF_GENDER_ID: TIntegerField;
    cdsRecordF_GENDER_ID: TIntegerField;
    cdsListF_GENDER_NAME: TStringField;
    cdsRecordF_GENDER_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectTitle( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False    ); virtual;
    class procedure ShowTitle  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  end;

implementation

uses Data_EduMainClient;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Title related fields from one
  DataSet to another DataSet.

  @Name       CopyTitleFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyTitleFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_TITLE_ID' ).AsInteger :=
        aSource.FieldByName( 'F_TITLE_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    aDestination.FieldByName( 'F_TITLE_NAME' ).AsString :=
      aSource.FieldByName( 'F_TITLE_NAME' ).AsString;
    aDestination.FieldByName('F_GENDER_ID').Value := aSource.FieldByName('F_GENDER_ID').Value;
    aDestination.FieldByName('F_GENDER_NAME').AsString := aSource.FieldByName('F_GENDER_NAME').Value;
  end;
end;

procedure TdtmTitle.prvListGetTableName(Sender: TObject; DataSet: TDataSet;
  var TableName: String);
begin
  inherited;
  TableName := 'T_GE_TITLE';
end;

{*****************************************************************************
  This class procedure will be used to select one or more Title Records.

  @Name       TdtmTitle.SelectTitle
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmTitle.SelectTitle( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False    );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmTitle', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyTitleFieldValues( aSelectedRecords, aDataSet, True );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Title
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmTitle.ShowTitle
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmTitle.ShowTitle(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmTitle', 'F_TITLE_ID = ' + aDataSet.FieldByName( 'F_TITLE_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyTitleFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ) );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmTitle.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_TITLE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

end.