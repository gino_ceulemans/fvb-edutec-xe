{*****************************************************************************
  This DataModule will be used for the Maintenance of T_COUNTRY records.
  
  @Name       Data_Country
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   sLesage              Added support for multiple destinations
                                    using the SelectXXX and ShowXXX class
                                    functions.
  30/06/2005   sLesage              Added Class functions which can be used
                                    to select country records or to show one
                                    specific country.
  28/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_Country;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces,
  Datasnap.DSConnect,
  Data.FMTBcd, Data.SqlExpr;

type
  TdtmCountry = class(TEDUDataModule, IEDUCountryDataModule )
    qryListF_COUNTRY_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_ISO_CODE: TStringField;
    qryRecordF_COUNTRY_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_ISO_CODE: TStringField;
    cdsListF_COUNTRY_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_ISO_CODE: TStringField;
    cdsRecordF_COUNTRY_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_ISO_CODE: TStringField;
    qryRecordF_COUNTRY_NAME: TStringField;
    qryListF_COUNTRY_NAME: TStringField;
    cdsRecordF_COUNTRY_NAME: TStringField;
    cdsListF_COUNTRY_NAME: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
  public
    { Public declarations }
    class procedure SelectCountry( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    class procedure ShowCountry  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

  procedure CopyCountryFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault   );

var
  dtmCountry: TdtmCountry;

implementation

uses Data_EDUMainClient,Data.DBXCommon;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy Country related fields from one DataSet
  to another DataSet.

  @Name       CopyCountryFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyCountryFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault   );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    case aCopyTo of
      critPostalCodeToOrgInvoiceAddress :
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_INVOICE_COUNTRY_ID' ).AsInteger :=
            aSource.FieldByName( 'F_COUNTRY_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_INVOICE_COUNTRY_NAME' ).AsString :=
          aSource.FieldByName( 'F_COUNTRY_NAME' ).AsString;
      end;
      critDefault :
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_COUNTRY_ID' ).AsInteger :=
            aSource.FieldByName( 'F_COUNTRY_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_COUNTRY_NAME' ).AsString :=
          aSource.FieldByName( 'F_COUNTRY_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method is executed when a resolver initializes its information about
  the table to which it applies updates.

  @Name       TdtmCountry.prvListGetTableName
  @author     slesage
  @param      Sender      The provider that needs the table name for applying
                          updates.
  @param      DataSet     The dataset to which updates should be applied. This
                          may be the providerís source dataset or a nested
                          detail dataset.
  @param      TableName   Returns the name of the target table. This is the
                          name used in generated SQL statements that insert,
                          delete, or modify records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCountry.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_GE_COUNTRY';
end;

{*****************************************************************************
  This class procedure will be used to select one or more Country Records.

  @Name       TdtmCountry.SelectCountry
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmCountry.SelectCountry(aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the Country ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmCountry', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyCountryFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Country Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmCountry.ShowCountry
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmCountry.ShowCountry(aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault);
var
  aSelectedRecords : TClientDataSet;
  aWhere           : String;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    case aCopyTo of
      critPostalCodeToOrgInvoiceAddress :
      begin
        aWhere := 'F_COUNTRY_ID = ' + aDataSet.FieldByName( 'F_INVOICE_COUNTRY_ID' ).AsString;
      end;
      else
      begin
        aWhere := 'F_COUNTRY_ID = ' + aDataSet.FieldByName( 'F_COUNTRY_ID' ).AsString;
      end;
    end;

    { Show a Modal RecordView for TdtmPerson.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmCountry', aWhere , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyCountryFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmCountry.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCountry.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
var
  aServerMethod : TSqlServerMethod;
  ServerCommand: TDBXCommand;
begin
  inherited;
  ServerCommand := GetServerCommand('GetNewRecordID');
  ServerCommand.ExecuteUpdate;
  //aDataset.FieldByName( 'F_COUNTRY_ID' ).AsInteger := ServerCommand.Parameters[0].Value.AsInteger;

  //aDataset.FieldByName( 'F_COUNTRY_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

end.
