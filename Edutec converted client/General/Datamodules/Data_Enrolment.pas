{*****************************************************************************
  This DataModule will be used for the maintenance of T_SES_ENROL.

  @Name       Data_Enrolment
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  10/11/2005   sLesage              Changes for Attestation.
  28/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Enrolment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, Unit_PPWFrameWorkActions, MConnect, unit_EdutecInterfaces,
  Datasnap.DSConnect;

type
  TdtmEnrolment = class(TEDUDataModule, IEDUEnrolmentDataModule)
    cdsListF_ENROL_ID: TIntegerField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_SESSION_NAME: TStringField;
    cdsListF_PERSON_ID: TIntegerField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_SOCSEC_NR: TStringField;
    cdsListF_STATUS_ID: TIntegerField;
    cdsListF_STATUS_NAME: TStringField;
    cdsListF_EVALUATION: TBooleanField;
    cdsListF_CERTIFICATE: TBooleanField;
    cdsListF_INVOICED: TBooleanField;
    cdsListF_LAST_MINUTE: TBooleanField;
    cdsRecordF_ENROL_ID: TIntegerField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_SESSION_NAME: TStringField;
    cdsRecordF_PERSON_ID: TIntegerField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_SOCSEC_NR: TStringField;
    cdsRecordF_STATUS_ID: TIntegerField;
    cdsRecordF_STATUS_NAME: TStringField;
    cdsRecordF_EVALUATION: TBooleanField;
    cdsRecordF_CERTIFICATE: TBooleanField;
    cdsRecordF_COMMENT: TStringField;
    cdsRecordF_ENROLMENT_DATE: TDateTimeField;
    cdsRecordF_INVOICED: TBooleanField;
    cdsRecordF_LAST_MINUTE: TBooleanField;
    cdsRecordF_HOURS: TIntegerField;
    cdsRecordF_FVB_FILE_ID: TIntegerField;
    cdsRecordF_HOURS_PRESENT: TSmallintField;
    cdsRecordF_HOURS_ABSENT_JUSTIFIED: TSmallintField;
    cdsRecordF_HOURS_ABSENT_ILLEGIT: TSmallintField;
    cdsListF_HOURS: TIntegerField;
    cdsListF_HOURS_PRESENT: TSmallintField;
    cdsListF_HOURS_ABSENT_JUSTIFIED: TSmallintField;
    cdsListF_HOURS_ABSENT_ILLEGIT: TSmallintField;
    cdsRecordF_PRICING_SCHEME: TIntegerField;
    cdsListF_PRICING_SCHEME: TIntegerField;
    cdsListF_TOTAL_SESSION_HOURS: TSmallintField;
    cdsRecordF_TOTAL_SESSION_HOURS: TSmallintField;
    cdsSesSession: TFVBFFCClientDataSet;
    cdsListF_ROLE_ID: TIntegerField;
    cdsRecordF_ROLE_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsRecordF_NAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_PRICE_WORKER: TFloatField;
    cdsListF_PRICE_CLERK: TFloatField;
    cdsListF_PRICE_OTHER: TFloatField;
    cdsRecordF_PRICE_WORKER: TFloatField;
    cdsRecordF_PRICE_CLERK: TFloatField;
    cdsRecordF_PRICE_OTHER: TFloatField;
    cdsListF_CERTIFICATE_PRINT_DATE: TDateTimeField;
    cdsRecordF_CERTIFICATE_PRINT_DATE: TDateTimeField;
    cdsListF_SELECTED_PRICE: TFloatField;
    cdsRecordF_SELECTED_PRICE: TFloatField;
    cdsMoveToSession: TFVBFFCClientDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    IntegerField3: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    IntegerField4: TIntegerField;
    StringField4: TStringField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    BooleanField3: TBooleanField;
    BooleanField4: TBooleanField;
    IntegerField5: TIntegerField;
    SmallintField1: TSmallintField;
    SmallintField2: TSmallintField;
    SmallintField3: TSmallintField;
    SmallintField4: TSmallintField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    StringField5: TStringField;
    StringField6: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    DateTimeField1: TDateTimeField;
    FloatField4: TFloatField;
    cdsListRowNumber: TIntegerField;
    cdsListF_ROLEGROUP_NAME: TStringField;
    cdsRecordF_ROLEGROUP_ID: TIntegerField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_NO_PARTICIPANTS: TIntegerField;
    cdsListF_CODE: TStringField;
    cdsListF_CONSTRUCT: TBooleanField;
    cdsRecordF_PARTNER_WINTER_ID: TIntegerField;
    cdsRecordF_PARTNER_WINTER_NAME: TStringField;
    cdsListF_PARTNER_WINTER_ID: TIntegerField;
    cdsListF_PARTNER_WINTER_NAME: TStringField;
    cdsListF_ORGTYPE_ID: TIntegerField;
    cdsListF_SCORE: TIntegerField;
    cdsListF_MAX_SCORE: TIntegerField;
    cdsRecordF_SCORE: TIntegerField;
    cdsRecordF_MAX_SCORE: TIntegerField;
    cdsListF_MAX_PUNTEN: TIntegerField;
    cdsRecordF_MAX_PUNTEN: TIntegerField;
    cdsListF_PROGRAM_TYPE: TIntegerField;
    cdsRecordF_PROGRAM_TYPE: TIntegerField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure cdsListBeforePost(DataSet: TDataSet);
    procedure cdsListCalcFields(DataSet: TDataSet);
    procedure FVBFFCDataModuleCreate(Sender: TObject);
    procedure cdsListAfterPost(DataSet: TDataSet);

  private
    FSessionID: Integer;
    FStatus: Integer;
  protected
    { Protected declarations }
    function IsListViewActionAllowed         ( aAction         : TPPWFrameWorkListViewAction ) : Boolean; override;

    procedure MoveToSession      ( const aEnrollment, aNewSessionID : Integer ); virtual;

    procedure SelectRole         ( aDataSet : TDataSet ); virtual;
    procedure SelectPerson       ( aDataSet : TDataSet ); virtual;
    procedure SelectSession      ( aDataSet : TDataSet ); virtual;
    procedure SelectStatus       ( aDataSet : TDataSet ); virtual;
    procedure ShowPerson         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowRole           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowSession        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowStatus         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    function  CheckStatus        ( aDataSet : TDataSet ) : integer; virtual;
    procedure SelectPartnerWinter ( aDataSet : TDataSet); virtual;
    procedure ShowPartnerWinter ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure RemovePartnerWinter ( aDataSet : TDataSet ); virtual;
  public
    { Public declarations }
    procedure MarkAsCertificatesPrinted (EnrollmentList: String);
    function  GetDefaultPrice ( TussenkomstType: Integer; SessieID: Integer): double;
    class procedure SelectEnrolment( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowEnrolment  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, DateUtils, Data_SesSession, Data_Person, Data_Status,
  Unit_PPWFrameWorkDataModule, Data_Role, Data_Organisation;

{*****************************************************************************
  This procedure will be used to copy Enrolment related fields from one
  DataSet to another DataSet.

  @Name       CopyEnrolmentFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable indicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyEnrolmentFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_ENROLMENT_ID' ).AsInteger :=
        aSource.FieldByName( 'F_ENROLMENT_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_ENROLMENT_NAME' ).AsString :=
          aSource.FieldByName( 'F_ENROLMENT_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Enrolment Records.

  @Name       TdtmEnrolment.SelectEnrolment
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmEnrolment.SelectEnrolment(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmEnrolment', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyEnrolmentFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Enrolment
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmEnrolment.ShowEnrolment
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmEnrolment.ShowEnrolment(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmEnrolment', 'F_ENROLMENT_ID = ' + aDataSet.FieldByName( 'F_ENROLMENT_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyEnrolmentFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmEnrolment.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_SES_ENROL';
end;

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmEnrolment.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ENROL_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmEnrolment.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_STATUS_ID' ).Value := stSubscribed;
  aDataSet.FieldByName( 'F_STATUS_NAME' ).AsString :=
    dtmEDUMainClient.GetNameForState(6); //  'Ingeschreven';
  aDataSet.FieldByName( 'F_EVALUATION' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_CERTIFICATE' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_INVOICED' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_LAST_MINUTE' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_ENROLMENT_DATE' ).AsDateTime := Today;
  // Tussenkomst : Geen (0), F.V.B (1), CEVORA (2)
  aDataSet.FieldByName( 'F_PRICING_SCHEME' ).Clear;
  aDataSet.FieldByName( 'F_PRICE_WORKER' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_PRICE_CLERK' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_PRICE_OTHER' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_HOURS' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_HOURS_PRESENT' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_HOURS_ABSENT_JUSTIFIED' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_HOURS_ABSENT_ILLEGIT' ).AsInteger := 0;
end;

{*****************************************************************************
  This event will be use to initialise the Foreign Key Fields.

  @Name       TdtmEnrolment.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUSessionDataModule ) ) then
    begin
      CopySessionFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critSessionToEnroll );
    end;
//    if ( Supports( MasterDataModule, IEDUPersonDataModule ) ) then
    if ( Supports( MasterDataModule, IEDURoleDataModule ) ) then
    begin
//      CopyPersonFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critPersonToEnrol );
      CopyRoleFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critRoleToEnrolment );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to select one or more (Person) Role Records.

  @Name       TdtmEnrolment.SelectPerson
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.SelectPerson(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and
     not ( Supports( MasterDataModule, IEDUPersonDataModule ) ) ) then
  begin
    aCopyTo := critPersonToEnrol;
    aIDField := aDataSet.FindField( 'F_SESSION_ID' );
    aWhere := '( F_ACTIVE <> 0 ) ';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Person isn't shown in the List
      AND all other already selected persons aren't shown also !!! }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := aWhere +
      ' AND ( F_PERSON_ID NOT IN ' +
       '( ' +
       'SELECT F_PERSON_ID FROM T_SES_ENROL WHERE F_SESSION_ID = ' +
       aIDField.AsString +
       ')) ';
    end;

    TdtmPerson.SelectPerson( aDataSet, aWhere, False, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Session Records.

  @Name       TdtmEnrolment.SelectSession
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.SelectSession(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
//  if ( not ( Assigned( MasterDataModule ) ) )
//  or
//     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUSessionDataModule ) ) ) then
//  begin
    aCopyTo := critSessionToEnroll;
    aIDField := aDataSet.FindField( 'F_SESSION_ID' );

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current PostalCode isn't shown in the List }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := ' AND (F_SESSION_ID <> ' + aIDField.AsString + ')';
    end;

    { Do not allow the user to select a Closed, Canceled, Full or Inactive Session }
    aWhere := '( F_STATUS_ID <> 2 AND F_STATUS_ID <> 3 AND F_STATUS_ID <> 5 )' + aWhere;

    TdtmSesSession.SelectSession( aDataSet, aWhere, False, aCopyTo );
//  end;
end;

{*****************************************************************************
  This method will be used to select one or more Status Records.

  @Name       TdtmEnrolment.SelectStatus
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.SelectStatus(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_STATUS_ID' );
  aWhere   := 'F_ENROL = 1 ';
  aCopyTo  := critDefault;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := aWhere + 'AND (F_STATUS_ID <> ' + aIDField.AsString +
      ') AND (F_STATUS_ID <> 10)';
      // The status 'Geattesteerd' can not be selected manually:
      // it will be set automatically in the bacth job that transfers the data
      // to Construct
  end;

  TdtmStatus.SelectStatus( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Person.

  @Name       TdtmEnrolment.ShowPerson
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.ShowPerson(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUPersonDataModule ) ) ) then
  begin
    aCopyTo := critPersonToEnrol;
    aIDField := aDataSet.FindField( 'F_PERSON_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmPerson.ShowPerson( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Session.

  @Name       TdtmEnrolment.ShowSession
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.ShowSession(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
//  if ( not ( Assigned( MasterDataModule ) ) )
//  or
//     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDUSessionDataModule ) ) ) then
//  begin
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_SESSION_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmSesSession.ShowSession( aDataSet, aRecordViewMode, aCopyTo );
    end;
//  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Status.

  @Name       TdtmEnrolment.ShowStatus
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.ShowStatus(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_STATUS_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmStatus.ShowStatus( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;


function TdtmEnrolment.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
var
  aAllowed    : Boolean;
  aDataModule : IEDUSessionDataModule;
begin
  aAllowed := Inherited IsListViewActionAllowed( aAction );

  if ( ( Assigned( aAction ) ) and
       ( Assigned( MasterDataModule ) ) and
       ( Supports( MasterDataModule, IEDUSessionDataModule, aDataModule ) ) ) then
  begin
    { Adding Enrolments,  when the Enrolment entity is used as a Detail of a
      Session Entity, should only be allowed if the Status of the Session is
      Planned or MinEnrolments or Attested}
    if ( aAction is TPPWFrameWorkListViewAddAction ) then
    begin
      aAllowed := aAllowed and
        ( TStatus( CheckStatus( aDataModule.RecordDataSet ) )
          in [ stPlanned, stMinEnrolments,stFull, stAttested ] );
    end;
  end;

  { When the enrollment has status 'Geattesteerd', then we don't allow for new
    changes, nor delete of the enrollment. The initial data when 'geattesteerd'
    was already sent to Construct (if applicable) }
    //04/12/2006 WL  As requested by Edytec (Eefje): we now DO allow the enrollment still to be
   //edited regardless of any state it may have
   //15/10/2007 WL  Enrollments of statue invoiced CAN not be edited. Because if we would:
   // the enrollment might be invoiced twice }
  if (
     ( aAction is TPPWFrameWorkListViewDeleteAction ) or
     ( aAction is TPPWFrameWorkListViewEditAction   )
     )
     and
     ( TStatus( DataSet.FieldByName ( 'F_STATUS_ID' ).AsInteger ) in [stInvoiced] )
  then
  begin
    aAllowed := false;
  end;

  Result := aAllowed;
end;

{*****************************************************************************
  Procedure to check if the Sessionstatus is changed when adding or removing
  an enrolment - to make sure the right actions are en- or disabled.
  25/09/2007 WL - CheckStatus was called from within IsListViewActionAllowed.
    This caused a never ending series of queries to the database: the result is
    now cached.

  @Name       TdtmEnrolment.CheckStatus
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmEnrolment.CheckStatus(aDataSet: TDataSet): integer;
begin
  Result := 0;
  if aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger <> FSessionID then
  begin
    with cdsSesSession do
    begin
      Close;
      FetchParams;
      if Params.Count > 0 then
      begin
        Params.ParamByName( '@ACTION' ).Value := 'S';
        Params.ParamByName( '@ID' ).Value :=
          aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
        Params.ParamByName( '@PRICE_WORKER').Value := 0;
        Params.ParamByName( '@PRICE_CLERK').Value := 0;
        Params.ParamByName( '@PRICE_OTHER' ).Value := 0;
        Execute;
        FetchParams;
        Result := Params.ParamByName( '@RETURN_VALUE' ).Value;
      end;
    end;
    FSessionID := aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger;
    FStatus    := result;
  end //if aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger <> FSessionID then
  else begin
    result := FStatus;
  end;
end;

procedure TdtmEnrolment.SelectRole(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and
     not ( Supports( MasterDataModule, IEDUPersonDataModule ) ) ) then
  begin
    aCopyTo := critRoleToEnrolment;
    aIDField := aDataSet.FindField( 'F_SESSION_ID' );
    aWhere := '( F_ACTIVE <> 0 ) ';

    { If the ID Field was found and it didn't contain a NULL Value, then we
      can build a Where clause so the current Person isn't shown in the List
      AND all other already selected persons aren't shown also !!! }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    begin
      aWhere := aWhere +
       ' AND ( F_PERSON_ID NOT IN ' +
       '( ' +
       'SELECT F_PERSON_ID FROM T_SES_ENROL WHERE F_SESSION_ID = ' +
       aIDField.AsString +
       ')) ';
    end;

    TdtmRole.SelectRole( aDataSet, aWhere, False, aCopyTo );
  end;
end;

procedure TdtmEnrolment.ShowRole(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  if ( not ( Assigned( MasterDataModule ) ) ) or
     ( Assigned( MasterDataModule ) and not ( Supports( MasterDataModule, IEDURoleDataModule ) ) ) then
  begin
    aCopyTo := critRoleToEnrolment;
    aIDField := aDataSet.FindField( 'F_PERSON_ID' );

    { Only execute the ShowXXX Method if the ID Field was found and it
      didn't contain a NULL Value or the entity should be shown in Add Mode. }
    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
    begin
      TdtmRole.ShowRole( aDataSet, aRecordViewMode, aCopyTo );
    end;
  end;
end;


procedure TdtmEnrolment.MarkAsCertificatesPrinted(EnrollmentList: String);
begin
  ssckData.AppServer.MarkAsCertificatePrinted(EnrollmentList);
end;


procedure TdtmEnrolment.cdsListBeforePost(DataSet: TDataSet);
begin
  inherited;
  with Dataset do
  begin
    if FieldByName ('F_PRICING_SCHEME').AsInteger = 0 then
      FieldByName ('F_SELECTED_PRICE').Value := FieldByName ('F_PRICE_OTHER').Value
    else if FieldByName ('F_PRICING_SCHEME').AsInteger = 1 then
      FieldByName ('F_SELECTED_PRICE').Value := FieldByName ('F_PRICE_WORKER').Value
    else if FieldByName ('F_PRICING_SCHEME').AsInteger = 2 then
      FieldByName ('F_SELECTED_PRICE').Value := FieldByName ('F_PRICE_CLERK').Value
    else
      assert (false, 'Unknown Pricing scheme');
  end;

  //wanneer inschrijving op ingeschreven wordt gezet, clear attestatie data
  with Dataset do
  begin
    if FieldByName('F_STATUS_ID').AsInteger = 6 then
    begin
      FieldByName('F_HOURS_PRESENT').AsInteger :=0;
      FieldByName('F_HOURS_ABSENT_JUSTIFIED').AsInteger := 0;
      FieldByName('F_HOURS_ABSENT_ILLEGIT').AsInteger := 0;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to move the Current Enrollment to the session with
  the given Session ID.

  @Name       TdtmEnrolment.MoveToSession
  @author     slesage
  @param      aSessionID: Integer
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmEnrolment.MoveToSession(const aEnrollment, aNewSessionID: Integer);
begin
  cdsMoveToSession.Params.ParamByName( '@EnrollmnentID' ).AsInteger := aEnrollment;
  cdsMoveToSession.Params.ParamByName( '@NewSessionID' ).AsInteger := aNewSessionID;
  cdsMoveToSession.Execute;
end;

procedure TdtmEnrolment.cdsListCalcFields(DataSet: TDataSet);
begin
  inherited;
  Dataset.FieldByName('RowNumber').AsInteger := Dataset.RecNo;
end;

procedure TdtmEnrolment.FVBFFCDataModuleCreate(Sender: TObject);
begin
  inherited;
  FSessionID := -1;
  FStatus := -1;
end;

function TdtmEnrolment.GetDefaultPrice(TussenkomstType,
  SessieID: Integer): double;
begin
  result := ssckData.AppServer.GetDefaultPrice(TussenkomstType, SessieID);
end;

procedure TdtmEnrolment.cdsListAfterPost(DataSet: TDataSet);
var
  aDataModule: IEDUSessionDataModule;
begin
  inherited;
  if ( ( Assigned( MasterDataModule ) ) and
       ( Supports( MasterDataModule, IEDUSessionDataModule, aDataModule ) ) ) then
  begin
    aDataModule.RefreshRecordDataSet;
  end;
end;

procedure TdtmEnrolment.SelectPartnerWinter (aDataSet: TDataSet);
var
  //aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
    aCopyTo := critSessionToEnroll;
    aWhere := '(F_PARTNER_WINTER = 1 AND F_ACTIVE = 1)';  
    TdtmOrganisation.SelectOrganisationForPartnerWinter( aDataSet, aWhere, False, aCopyTo );
end;

procedure TdtmEnrolment.ShowPartnerWinter (aDataSet: TDataSet; aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
    aCopyTo := critSessionToEnroll;
     aIDField := aDataSet.FindField( 'F_PARTNER_WINTER_ID' );

    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    TdtmOrganisation.ShowOrganisation( aDataSet, aRecordViewMode, aCopyTo );
end;

procedure TdtmEnrolment.RemovePartnerWinter (aDataSet: TDataSet);
var
  aIDField : TField;

begin
     aIDField := aDataSet.FindField( 'F_PARTNER_WINTER_ID' );

    if ( Assigned( aIDField ) ) and
       ( not ( aIDField.IsNull ) ) then
    TdtmOrganisation.RemoveOrganisationFieldValuesForPartnerWinter( aDataSet );
end;
end.
