{*****************************************************************************
  This DataModule will be used for the Maintenance of T_SYS_USER records.

  @Name       Data_User
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Data_User;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

resourcestring
  rsEMAIL_PUBLIC_FOLDER='info@edutec.be';
  rsDEFAULT_INFO_CONFIRMATION='lunchpakket zelf te voorzien';

type
  TdtmUser = class(TEDUDataModule, IEDUUserDataModule )
    qryListF_USER_ID: TIntegerField;
    qryListF_LASTNAME: TStringField;
    qryListF_FIRSTNAME: TStringField;
    qryListF_PHONE_INT: TStringField;
    qryListF_PHONE_EXT: TStringField;
    qryListF_GSM: TStringField;
    qryListF_EMAIL: TStringField;
    qryListF_LOGIN_ID: TStringField;
    qryListF_ACTIVE: TBooleanField;
    qryListF_GENDER_ID: TIntegerField;
    qryListF_LOCKED: TBooleanField;
    qryListF_START_DATE: TDateTimeField;
    qryListF_END_DATE: TDateTimeField;
    qryListF_LOGIN_TRY: TSmallintField;
    qryListF_DOMAIN: TStringField;
    qryListF_LANGUAGE_NAME: TStringField;
    qryListF_COMPLETE_USERNAME: TStringField;
    cdsListF_USER_ID: TIntegerField;
    cdsListF_LASTNAME: TStringField;
    cdsListF_FIRSTNAME: TStringField;
    cdsListF_PHONE_INT: TStringField;
    cdsListF_PHONE_EXT: TStringField;
    cdsListF_GSM: TStringField;
    cdsListF_EMAIL: TStringField;
    cdsListF_LOGIN_ID: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsListF_GENDER_ID: TIntegerField;
    cdsListF_LOCKED: TBooleanField;
    cdsListF_START_DATE: TDateTimeField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_LOGIN_TRY: TSmallintField;
    cdsListF_DOMAIN: TStringField;
    cdsListF_LANGUAGE_NAME: TStringField;
    cdsListF_COMPLETE_USERNAME: TStringField;
    cdsRecordF_USER_ID: TIntegerField;
    cdsRecordF_LASTNAME: TStringField;
    cdsRecordF_FIRSTNAME: TStringField;
    cdsRecordF_PHONE_INT: TStringField;
    cdsRecordF_PHONE_EXT: TStringField;
    cdsRecordF_GSM: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_LOGIN_ID: TStringField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_GENDER_ID: TIntegerField;
    cdsRecordF_LOCKED: TBooleanField;
    cdsRecordF_START_DATE: TDateTimeField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_LOGIN_TRY: TSmallintField;
    cdsRecordF_DOMAIN: TStringField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_COMPLETE_USERNAME: TStringField;
    qryListF_LANGUAGE_ID: TIntegerField;
    qryRecordF_USER_ID: TIntegerField;
    qryRecordF_LASTNAME: TStringField;
    qryRecordF_FIRSTNAME: TStringField;
    qryRecordF_PHONE_INT: TStringField;
    qryRecordF_PHONE_EXT: TStringField;
    qryRecordF_GSM: TStringField;
    qryRecordF_EMAIL: TStringField;
    qryRecordF_LOGIN_ID: TStringField;
    qryRecordF_ACTIVE: TBooleanField;
    qryRecordF_GENDER_ID: TIntegerField;
    qryRecordF_LOCKED: TBooleanField;
    qryRecordF_START_DATE: TDateTimeField;
    qryRecordF_END_DATE: TDateTimeField;
    qryRecordF_LOGIN_TRY: TSmallintField;
    qryRecordF_DOMAIN: TStringField;
    qryRecordF_LANGUAGE_NAME: TStringField;
    qryRecordF_COMPLETE_USERNAME: TStringField;
    qryRecordF_LANGUAGE_ID: TIntegerField;
    cdsListF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsSeachCriteriaF_USER_ID: TIntegerField;
    cdsSeachCriteriaF_LASTNAME: TStringField;
    cdsSeachCriteriaF_FIRSTNAME: TStringField;
    cdsSeachCriteriaF_ACTIVE: TBooleanField;
    cdsSeachCriteriaF_LOCKED: TBooleanField;
    cdsSeachCriteriaF_LOGIN_ID: TStringField;
    cdsListF_GENDER_NAME: TStringField;
    qryListF_GENDER_NAME: TStringField;
    qryRecordF_GENDER_NAME: TStringField;
    cdsRecordF_GENDER_NAME: TStringField;
    cdsListF_PASSWORD: TStringField;
    cdsRecordF_PASSWORD: TStringField;
    qryRecordF_PASSWORD: TStringField;
    qryRecordF_PRINTER_LETTER: TStringField;
    qryRecordF_PRINTER_CERTIFICATE: TStringField;
    cdsRecordF_PRINTER_LETTER: TStringField;
    cdsRecordF_PRINTER_CERTIFICATE: TStringField;
    cdsRecordF_PATH_CONFIRMATIONS: TStringField;
    cdsRecordF_PATH_INVOICES: TStringField;
    cdsRecordF_PRINTER_EVALUATION: TStringField;
    cdsRecordF_PATH_CRREPORTS: TStringField;
    cdsRecordF_EMAIL_PUBLIC_FOLDER: TStringField;
    cdsRecordF_DEFAULT_INFO_CONFIRMATION: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure cdsSeachCriteriaNewRecord(DataSet: TDataSet);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure cdsListNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SelectGender     ( aDataSet : TDataSet ); virtual;
    procedure SelectLanguage   ( aDataSet : TDataSet ); virtual;
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowGender       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

  public
    { Public declarations }
    class procedure SelectUser(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False
          ); virtual;
    class procedure ShowUser  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

procedure CopyUserFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

var
  dtmUser: TdtmUser;

implementation

uses
  Data_EDUMainClient, Data_Language, Data_Gender;

{$R *.dfm}

{*****************************************************************************
  This procedure will be used to copy User related fields from one DataSet
  to another DataSet.

  @Name       CopyUserFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyUserFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    case aCopyTo of
      critUserToSession :
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_USER_ID' ).AsInteger :=
            aSource.FieldByName( 'F_USER_ID' ).AsInteger;
          aDestination.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
            aSource.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
        end;
      end;
      critUserToProgProgram :
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_USER_ID' ).AsInteger :=
            aSource.FieldByName( 'F_USER_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
          aSource.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
      end;
      else
      begin
        { Copy the ID if necessary }
        if ( IncludeID ) then
        begin
          aDestination.FieldByName( 'F_USER_ID' ).AsInteger :=
            aSource.FieldByName( 'F_USER_ID' ).AsInteger;
        end;

        { Copy the Other Fields }
        aDestination.FieldByName( 'F_FIRSTNAME' ).AsString :=
          aSource.FieldByName( 'F_FIRSTNAME' ).AsString;
        aDestination.FieldByName( 'F_LASTNAME' ).AsString :=
          aSource.FieldByName( 'F_LASTNAME' ).AsString;
        aDestination.FieldByName( 'F_COMPLETE_USERNAME' ).AsString :=
          aSource.FieldByName( 'F_COMPLETE_USERNAME' ).AsString;
      end;
    end;
  end;
end;

procedure TdtmUser.prvListGetTableName(Sender: TObject; DataSet: TDataSet;
  var TableName: String);
begin
  inherited;
  TableName := 'T_SYS_USER';
end;

procedure TdtmUser.cdsSeachCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  DataSet.FieldByName( 'F_LOCKED' ).AsBoolean := False;
end;

{*****************************************************************************
  This class procedure will be used to select one or more User Records.

  @Name       TdtmUser.SelectUser
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmUser.SelectUser(aDataSet: TDataSet;
  aWhereClause: String = '';
  aMultiSelect : Boolean = False;
  aCopyTo : TCopyRecordInformationTo = critDefault;
  aShowInactive : boolean = False );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the Country ListView for selection purposes, passing in a
      WHERE clause as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEDUMainClient.pfcMain.SelectRecords( 'TdtmUser', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyUserFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmUser.ShowUser
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmUser.ShowUser(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView;
  aCopyTo : TCopyRecordInformationTo = critDefault);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView for TdtmUser.  Make sure it is filtered on
      the correct PE_IDPerson, and show it in View Mode.  If the user modfies
      data in that Modal RecordView, we can still update our DataSet if needed.
      The Modified record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmUser', 'F_USER_ID = ' + aDataSet.FieldByName( 'F_USER_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyUserFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmUser.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUser.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_USER_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

procedure TdtmUser.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_LOCKED' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_LOGIN_TRY' ).AsInteger := 0;
  aDataSet.FieldByName( 'F_LOGIN_TRY' ).AsInteger := 0;
  // Select as default language : "Nederlands ( 1 )"
  aDataSet.FieldByName( 'F_LANGUAGE_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_LANGUAGE_NAME' ).AsString := 'Nederlands';
end;

{*****************************************************************************
  This class procedure will be used to select one or more Language Records.

  @Name       TdtmUser.SelectLanguage
  @author     cheuten
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUser.SelectLanguage(aDataSet: TDataSet);
begin
  TdtmLanguage.SelectLanguage( aDataSet );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Language Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmUser.ShowLanguage
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUser.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Gender Records.

  @Name       TdtmUser.SelectGender
  @author     cheuten
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUser.SelectGender(aDataSet: TDataSet);
begin
  TdtmGender.SelectGender( aDataSet );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Gender Record
  based on the PK Field values found in the given aDataSet.

  @Name       TdtmUser.ShowGender
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmUser.ShowGender(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_GENDER_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmGender.ShowGender( aDataSet, aRecordViewMode );
  end;
end;

procedure TdtmUser.cdsListNewRecord(DataSet: TDataSet);
begin
  inherited;
    // Stel standaard onderstaand emailadres en standaard bevestigingsinfo voor.
    // Emailadres veranderd waarschijnlijk nooit!
    // Ivan Van den Bossche (15-03-2008)
    DataSet.FieldByName('F_EMAIL_PUBLIC_FOLDER').Value := rsEMAIL_PUBLIC_FOLDER;
    //DataSet.FieldByName('F_DEFAULT_INFO_CONFIRMATION').Value := rsDEFAULT_INFO_CONFIRMATION;
    
end;

end.
