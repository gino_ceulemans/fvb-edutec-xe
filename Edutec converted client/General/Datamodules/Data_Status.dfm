inherited dtmStatus: TdtmStatus
  KeyFields = 'F_STATUS_ID'
  ListViewClass = 'TfrmStatus_List'
  RecordViewClass = 'TfrmStatus_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_STATUS_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_LONG_DESC_NL, '
      ' F_LONG_DESC_FR, '
      ' F_SESSION, '
      ' F_ENROL, '
      ' F_WAITING_LIST, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR, '
      ' F_STATUS_NAME,'
      ' F_LONG_DESC '
      'FROM '
      ' V_GE_STATUS')
    object qryListF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryListF_LONG_DESC_NL: TStringField
      FieldName = 'F_LONG_DESC_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_LONG_DESC_FR: TStringField
      FieldName = 'F_LONG_DESC_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryListF_SESSION: TBooleanField
      FieldName = 'F_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_ENROL: TBooleanField
      FieldName = 'F_ENROL'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_WAITING_LIST: TBooleanField
      FieldName = 'F_WAITING_LIST'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object qryListF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_LONG_DESC: TStringField
      FieldName = 'F_LONG_DESC'
      ReadOnly = True
      Size = 128
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_STATUS_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR, '
      ' F_LONG_DESC_NL, '
      ' F_LONG_DESC_FR, '
      ' F_SESSION, '
      ' F_ENROL, '
      ' F_WAITING_LIST, '
      ' F_FOREGROUNDCOLOR, '
      ' F_BACKGROUNDCOLOR, '
      ' F_STATUS_NAME,'
      ' F_LONG_DESC '
      'FROM '
      ' V_GE_STATUS')
    object qryRecordF_STATUS_ID: TIntegerField
      FieldName = 'F_STATUS_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object qryRecordF_LONG_DESC_NL: TStringField
      FieldName = 'F_LONG_DESC_NL'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_LONG_DESC_FR: TStringField
      FieldName = 'F_LONG_DESC_FR'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object qryRecordF_SESSION: TBooleanField
      FieldName = 'F_SESSION'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_ENROL: TBooleanField
      FieldName = 'F_ENROL'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_WAITING_LIST: TBooleanField
      FieldName = 'F_WAITING_LIST'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = [pfInUpdate]
    end
    object qryRecordF_STATUS_NAME: TStringField
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_LONG_DESC: TStringField
      FieldName = 'F_LONG_DESC'
      ReadOnly = True
      Size = 128
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_STATUS_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 64
    end
    object cdsListF_LONG_DESC_NL: TStringField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_LONG_DESC_NL'
      Visible = False
      Size = 128
    end
    object cdsListF_LONG_DESC_FR: TStringField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_LONG_DESC_FR'
      Visible = False
      Size = 128
    end
    object cdsListF_SESSION: TBooleanField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION'
    end
    object cdsListF_ENROL: TBooleanField
      DisplayLabel = 'Inschrijving'
      FieldName = 'F_ENROL'
    end
    object cdsListF_WAITING_LIST: TBooleanField
      DisplayLabel = 'Wachtlijst'
      FieldName = 'F_WAITING_LIST'
    end
    object cdsListF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      Visible = False
    end
    object cdsListF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      Visible = False
    end
    object cdsListF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      Size = 64
    end
    object cdsListF_LONG_DESC: TStringField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_LONG_DESC'
      Size = 128
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_STATUS_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_LONG_DESC_NL: TStringField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_LONG_DESC_NL'
      Size = 128
    end
    object cdsRecordF_LONG_DESC_FR: TStringField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_LONG_DESC_FR'
      Size = 128
    end
    object cdsRecordF_SESSION: TBooleanField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION'
      Required = True
    end
    object cdsRecordF_ENROL: TBooleanField
      DisplayLabel = 'Inschrijving'
      FieldName = 'F_ENROL'
      Required = True
    end
    object cdsRecordF_WAITING_LIST: TBooleanField
      DisplayLabel = 'Wachtlijst'
      FieldName = 'F_WAITING_LIST'
      Required = True
    end
    object cdsRecordF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
    end
    object cdsRecordF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
    end
    object cdsRecordF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 64
    end
    object cdsRecordF_LONG_DESC: TStringField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_LONG_DESC'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 128
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmStatus'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmStatus'
  end
end
