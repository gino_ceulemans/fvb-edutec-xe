inherited dtmMedium: TdtmMedium
  ListViewClass = 'TfrmMedium_List'
  RecordViewClass = 'TfrmMedium_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_MEDIUM_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_MEDIUM_NAME '
      'FROM '
      '  V_GE_MEDIUM')
    object qryListF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryListF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_MEDIUM_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_MEDIUM_NAME '
      'FROM '
      '  V_GE_MEDIUM')
    object qryRecordF_MEDIUM_ID: TIntegerField
      FieldName = 'F_MEDIUM_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryRecordF_MEDIUM_NAME: TStringField
      FieldName = 'F_MEDIUM_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Medium ( NL )'
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Medium ( FR )'
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object cdsListF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      ReadOnly = True
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Medium ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Medium ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmMedium'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmMedium'
  end
end
