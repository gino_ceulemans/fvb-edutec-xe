inherited dtmUserProfile: TdtmUserProfile
  KeyFields = 'F_USER_ID; F_PROFILE_ID'
  ListViewClass = 'TfrmUserProfile_List'
  RecordViewClass = 'TfrmUserProfile_Record'
  Registered = True
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_COMPLETE_USERNAME, '
      '  F_PROFILE_ID, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_USER_PROFILE')
    object qryListF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryListF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object qryListF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryListF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_USER_ID, '
      '  F_LASTNAME, '
      '  F_FIRSTNAME, '
      '  F_COMPLETE_USERNAME, '
      '  F_PROFILE_ID, '
      '  F_PROFILE_NAME '
      'FROM '
      '  V_SYS_USER_PROFILE')
    object qryRecordF_USER_ID: TIntegerField
      FieldName = 'F_USER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_LASTNAME: TStringField
      FieldName = 'F_LASTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_FIRSTNAME: TStringField
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = []
      Size = 64
    end
    object qryRecordF_COMPLETE_USERNAME: TStringField
      FieldName = 'F_COMPLETE_USERNAME'
      ProviderFlags = []
      Size = 232
    end
    object qryRecordF_PROFILE_ID: TIntegerField
      FieldName = 'F_PROFILE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_PROFILE_NAME: TStringField
      FieldName = 'F_PROFILE_NAME'
      ProviderFlags = []
      Size = 64
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_USER_ID: TIntegerField
      DisplayLabel = 'Gebruiker ( ID )'
      FieldName = 'F_USER_ID'
      Required = True
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Visible = False
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Visible = False
      Size = 64
    end
    object cdsListF_COMPLETE_USERNAME: TStringField
      DisplayLabel = 'Gebruiker'
      FieldName = 'F_COMPLETE_USERNAME'
      Required = True
      Size = 232
    end
    object cdsListF_PROFILE_ID: TIntegerField
      DisplayLabel = 'Profiel ( ID )'
      FieldName = 'F_PROFILE_ID'
      Required = True
    end
    object cdsListF_PROFILE_NAME: TStringField
      DisplayLabel = 'Profiel'
      FieldName = 'F_PROFILE_NAME'
      Required = True
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_USER_ID: TIntegerField
      DisplayLabel = 'Gebruiker ( ID )'
      FieldName = 'F_USER_ID'
      Required = True
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsRecordF_COMPLETE_USERNAME: TStringField
      DisplayLabel = 'Gebruiker'
      FieldName = 'F_COMPLETE_USERNAME'
      Required = True
      Size = 232
    end
    object cdsRecordF_PROFILE_ID: TIntegerField
      DisplayLabel = 'Profiel ( ID )'
      FieldName = 'F_PROFILE_ID'
      Required = True
    end
    object cdsRecordF_PROFILE_NAME: TStringField
      DisplayLabel = 'Profiel'
      FieldName = 'F_PROFILE_NAME'
      Required = True
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmUserProfile'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmUserProfile'
  end
end
