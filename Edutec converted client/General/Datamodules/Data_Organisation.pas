{*****************************************************************************
  This DataModule will be used for the maintenance of <T_OR_ORGANISATION>.

  @Name       Data_Organisation
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                       Description
  ----         --                       -----------
  27/07/2005   PIFWizard Generated      Initial creation of the Unit.
  26/07/2013   IVDBOSSCHE & KBOGAERTS   Added ClearPartnerWinter
******************************************************************************}

unit Data_Organisation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule,
  Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents, Provider, DB, ADODB,
  Unit_FVBFFCInterfaces, MConnect, unit_EdutecInterfaces, Datasnap.DSConnect;

resourcestring
  rsOrganisationJoinedWith='Organisaties met ID''s %s worden samengevoegd.' + #13 + #13
                    + 'Organisatie met ID %d blijft behouden.' + #13 + #13
                    + 'Bent u zeker?';
const
  cUnselectedAdminCostType = -1;
  cFieldAdminCostType = 'F_ADMIN_COST_TYPE';

type
  TdtmOrganisation = class(TEduDataModule, IEDUOrganisationDataModule)
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_RSZ_NR: TStringField;
    cdsListF_VAT_NR: TStringField;
    cdsListF_STREET: TStringField;
    cdsListF_NUMBER: TStringField;
    cdsListF_MAILBOX: TStringField;
    cdsListF_POSTALCODE_ID: TIntegerField;
    cdsListF_POSTALCODE: TStringField;
    cdsListF_CITY_NAME: TStringField;
    cdsListF_COUNTRY_ID: TIntegerField;
    cdsListF_COUNTRY_NAME: TStringField;
    cdsListF_PHONE: TStringField;
    cdsListF_FAX: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsListF_ORGTYPE_ID: TIntegerField;
    cdsListF_ORGTYPE_NAME: TStringField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_RSZ_NR: TStringField;
    cdsRecordF_VAT_NR: TStringField;
    cdsRecordF_STREET: TStringField;
    cdsRecordF_NUMBER: TStringField;
    cdsRecordF_MAILBOX: TStringField;
    cdsRecordF_POSTALCODE_ID: TIntegerField;
    cdsRecordF_POSTALCODE: TStringField;
    cdsRecordF_CITY_NAME: TStringField;
    cdsRecordF_COUNTRY_ID: TIntegerField;
    cdsRecordF_COUNTRY_NAME: TStringField;
    cdsRecordF_PHONE: TStringField;
    cdsRecordF_FAX: TStringField;
    cdsRecordF_EMAIL: TStringField;
    cdsRecordF_WEBSITE: TStringField;
    cdsRecordF_LANGUAGE_ID: TIntegerField;
    cdsRecordF_LANGUAGE_NAME: TStringField;
    cdsRecordF_ACC_NR: TStringField;
    cdsRecordF_ACC_HOLDER: TStringField;
    cdsRecordF_MEDIUM_ID: TIntegerField;
    cdsRecordF_MEDIUM_NAME: TStringField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_INVOICE_STREET: TStringField;
    cdsRecordF_INVOICE_NUMBER: TStringField;
    cdsRecordF_INVOICE_MAILBOX: TStringField;
    cdsRecordF_INVOICE_POSTALCODE_ID: TIntegerField;
    cdsRecordF_INVOICE_COUNTRY_ID: TIntegerField;
    cdsRecordF_ORGTYPE_ID: TIntegerField;
    cdsRecordF_ORGTYPE_NAME: TStringField;
    cdsRecordF_INVOICE_POSTALCODE: TStringField;
    cdsRecordF_INVOICE_CITY_NAME: TStringField;
    cdsRecordF_INVOICE_COUNTRY_NAME: TStringField;
    cdsSearchCriteriaStringField: TStringField;
    cdsSearchCriteriaStringField2: TStringField;
    cdsSearchCriteriaStringField3: TStringField;
    cdsSearchCriteriaStringField4: TStringField;
    cdsSearchCriteriaStringField5: TStringField;
    cdsSearchCriteriaIntegerField: TIntegerField;
    cdsSearchCriteriaStringField6: TStringField;
    cdsSearchCriteriaF_ACTIVE: TBooleanField;
    cdsListF_ORG_NR: TStringField;
    cdsListF_SCHOOL_NR: TStringField;
    cdsListF_MANUALLY_ADDED: TBooleanField;
    cdsRecordF_ORG_NR: TStringField;
    cdsRecordF_SCHOOL_NR: TStringField;
    cdsRecordF_MANUALLY_ADDED: TBooleanField;
    cdsListF_CONSTRUCT_ID: TIntegerField;
    cdsRecordF_CONSTRUCT_ID: TIntegerField;
    cdsListF_CONTACT_DATA: TStringField;
    cdsRecordF_CONTACT_DATA: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT1: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT1_EMAIL: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT2: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT2_EMAIL: TStringField;
    cdsJoinToOrganisation: TFVBFFCClientDataSet;
    cdsJoinToOrganisationF_ORGANISATION_ID: TIntegerField;
    cdsListF_EMAIL: TStringField;
    cdsRecordF_EXTRA_INFO: TStringField;
    cdsRecordF_INVOICE_EXTRA_INFO: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT3: TStringField;
    cdsRecordF_CONFIRMATION_CONTACT3_EMAIL: TStringField;
    cdsListF_SYNERGY_ID: TLargeintField;
    cdsRecordF_SYNERGY_ID: TLargeintField;
    cdsRecordF_PARTNER_WINTER: TBooleanField;
    cdsRecordF_AUTHORIZATION_NR: TStringField;
    cdsRecordF_ADMIN_COST: TFloatField;
    cdsRecordF_ADMIN_COST_TYPE: TStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure cdsSearchCriteriaNewRecord(DataSet: TDataSet);
    procedure cdsListNewRecord(DataSet: TDataSet);
  protected
    procedure CopyAddressToInvoiceAddress( aDataSet : TDataSet ); virtual;
    procedure SelectCountry    ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False ); virtual;
    procedure SelectLanguage   ( aDataSet : TDataSet ); virtual;
    procedure SelectMedium     ( aDataSet : TDataSet ); virtual;
    procedure SelectOrgType    ( aDataSet : TDataSet ); virtual;
    procedure SelectPostalCode ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False ); virtual;
    procedure ShowCountry      ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowOrgType      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowPostalCode   ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    procedure JoinOrganisations( OrganisationFilter: String);
    procedure JoinPersons (OrganisationID: Integer);

    function ClearPartnerWinter ( aDataSet: TDataSet ): Boolean;
    procedure ClearAuthorizationNr ( aDataSet: TDataSet );
    function IsEducationCenter ( aDataSet: TDataSet ): Boolean;

    procedure UpdateAdminCostType ( aDataSet: TDataSet; adminCostTypeIndex: Integer );
    function GetAdminCostTypeItemIndex ( aDataSet: TDataSet ): Integer;

    class procedure RemoveOrganisationFieldValuesForPartnerWinter ( aDataSet : TDataSet);
    class procedure SelectOrganisation(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False ); virtual;
    class procedure SelectOrganisationForPartnerWinter(
          aDataSet : TDataSet;
          aWhereClause : String = '';
          aMultiSelect : Boolean = False;
          aCopyTo : TCopyRecordInformationTo = critDefault;
          aShowInactive : boolean = False ); virtual;

    class procedure ShowOrganisation  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

  procedure CopyOrganisationFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
  procedure CopyOrganisationFieldValuesForPartnerWinter( aSource, aDestination : TDataSet );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_Language, Data_Medium, Data_OrganisationType,
  Data_Postalcode, Data_Country, Unit_PPWFrameWorkDataModule;

{*****************************************************************************
  WL: 27/11/2006
  F_ORGANISATION_ID no longer copied in this procedure because F_ORGANISATION_ID
  field is readonly at the time this procedure gets called.
  see: TFVBFFCBaseDataModule.FVBFFCDataModuleInitialiseForeignKeyFields

  This procedure will be used to copy Organisation related fields from one
  DataSet to another DataSet.

  @Name       CopyOrganisationFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyOrganisationFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      if not (aDestination.FieldByName('F_ORGANISATION_ID').ReadOnly) then
        aDestination.FieldByName( 'F_ORGANISATION_ID' ).AsInteger :=
          aSource.FieldByName( 'F_ORGANISATION_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      critOrganisationToRole :
      begin
        aDestination.FieldByName( 'F_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
        aDestination.FieldByName( 'F_RSZ_NR' ).AsString :=
          aSource.FieldByName( 'F_RSZ_NR' ).AsString;
      end;

      critOrganisationToTeacher:
      begin
        aDestination.FieldByName( 'F_ORGANISATION_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end;

      critJoinOrganisation:
      begin
        //Do nothing: for join organisation, w eonly need the F_ORGANISATION_ID
      end;

      critOrganisationToKMOPortefeuille:
      begin
        aDestination.FieldByName( 'F_ORG_NAME' ).AsString :=
          aSource.FieldByName( 'F_NAME' ).AsString;
      end;

      { By Default copy these Fields }
      else
      begin
        aDestination.FieldByName( 'F_ORGANISATION_NAME' ).AsString :=
          aSource.FieldByName( 'F_ORGANISATION_NAME' ).AsString;
      end;
    end;
  end;
end;

procedure CopyOrganisationFieldValuesForPartnerWinter( aSource, aDestination : TDataSet );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

     begin;
        aDestination.FieldByName( 'F_PARTNER_WINTER_ID' ).AsInteger :=
        aSource.FieldByName( 'F_ORGANISATION_ID' ).AsInteger;
        aDestination.FieldByName( 'F_PARTNER_WINTER_NAME' ).AsString :=
        aSource.FieldByName ( 'F_NAME').AsString;
     end;

  end;
 end;

 class procedure TdtmOrganisation.RemoveOrganisationFieldValuesForPartnerWinter(aDataSet : TDataSet );
 begin
  { Check if both DataSets are assigned }
  if ( Assigned( aDataSet ) ) then
  begin
      { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDataSet.State in dsEditModes ) then
    begin
      aDataSet.Edit;
    end;

     begin;
        aDataSet.FieldByName( 'F_PARTNER_WINTER_ID' ).AsVariant:= Null;
        aDataSet.FieldByName( 'F_PARTNER_WINTER_NAME' ).AsString := '';
     end;
  end;
 end;


{*****************************************************************************
  This class procedure will be used to select one or more Organisation Records.

  @Name       TdtmOrganisation.SelectOrganisation
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmOrganisation.SelectOrganisation(
    aDataSet : TDataSet;
    aWhereClause : String = '';
    aMultiSelect : Boolean = False;
    aCopyTo : TCopyRecordInformationTo = critDefault;
    aShowInactive : boolean = False
    );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmOrganisation', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyOrganisationFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;


class procedure TdtmOrganisation.SelectOrganisationForPartnerWinter(
    aDataSet : TDataSet;
    aWhereClause : String = '';
    aMultiSelect : Boolean = False;
    aCopyTo : TCopyRecordInformationTo = critDefault;
    aShowInactive : boolean = False
    );
var
  aSelectedRecords : TClientDataSet;
  aWhere : string;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if not aShowInactive And (Trim(aWhereClause) = '') then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) ';
    end
    else if not aShowInactive then
    begin
      aWhere := ' ( F_ACTIVE = 1 ) AND ' + aWhereClause;
    end
    else
    begin
      aWhere := aWhereClause;
    end;
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmOrganisation', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyOrganisationFieldValuesForPartnerWinter( aSelectedRecords, aDataSet );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Organisation
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmOrganisation.ShowOrganisation
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmOrganisation.ShowOrganisation(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aWhere : string;
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    // Controle of veld is ingevuld zodat we zeker record kunnen selecteren
    if (aDataSet.FieldByName( 'F_ORGANISATION_ID' ).IsNull)
      and not (aRecordViewMode = rvmAdd) then
    begin
       exit;
    end;
    if not (aRecordViewMode = rvmAdd) then
    begin
      aWhere := 'F_ORGANISATION_ID = ' + aDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsString;
    end
    else
    begin
      aWhere := '';
    end;

    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEDUMainClient.pfcMain.ShowModalRecord( 'TdtmOrganisation',
      aWhere,
      aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 )  And ( aRecordViewMode <> rvmView ) then
      begin
        CopyOrganisationFieldValues( aSelectedRecords, aDataSet,
         ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmOrganisation.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_OR_ORGANISATION';
end;

{*****************************************************************************
  This method will be used to initialise the Primary Key Fields.

  @Name       TdtmOrganisation.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which to initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be used to select one or more Country Records.

  @Name       TdtmOrganisation.SelectCountry
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.SelectCountry(aDataSet: TDataSet;
  aInvoiceAddress: Boolean);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  if ( aInvoiceAddress ) then
  begin
    aCopyTo := critPostalCodeToOrgInvoiceAddress;
    aIDField := aDataSet.FindField( 'F_INVOICE_COUNTRY_ID' );
  end
  else
  begin
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );
  end;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_COUNTRY_ID <> ' + aIDField.AsString;
  end;

  TdtmCountry.SelectCountry( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to select one or more Language Records.

  @Name       TdtmOrganisation.SelectLanguage
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.SelectLanguage(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_LANGUAGE_ID <> ' + aIDField.AsString;
  end;

  TdtmLanguage.SelectLanguage( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more Medium Records.

  @Name       TdtmOrganisation.SelectMedium
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.SelectMedium(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Medium isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_MEDIUM_ID <> ' + aIDField.AsString;
  end;

  TdtmMedium.SelectMedium( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more OrgType Records.

  @Name       TdtmOrganisation.SelectOrgType
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.SelectOrgType(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_ORGTYPE_ID' );
  aWhere   := '';

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Medium isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_ORGTYPE_ID <> ' + aIDField.AsString;
  end;

  TdtmOrganisationType.SelectOrganisationType( aDataSet, aWhere );
end;

{*****************************************************************************
  This method will be used to select one or more PostalCode Records.

  @Name       TdtmOrganisation.SelectPostalCode
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.SelectPostalCode(aDataSet: TDataSet; aInvoiceAddress : Boolean = False);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  if ( aInvoiceAddress ) then
  begin
    aCopyTo := critPostalCodeToOrgInvoiceAddress;
    aIDField := aDataSet.FindField( 'F_INVOICE_POSTALCODE_ID' );
  end
  else
  begin
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );
  end;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current PostalCode isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_POSTALCODE_ID <> ' + aIDField.AsString;
  end;

  TdtmPostalcode.SelectPostalCode( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Language.

  @Name       TdtmOrganisation.ShowLanguage
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.ShowLanguage(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView );
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_LANGUAGE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmLanguage.ShowLanguage( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Medium.

  @Name       TdtmOrganisation.ShowMedium
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.ShowMedium(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView );
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_MEDIUM_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmMedium.ShowMedium( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show an Organisation Type.

  @Name       TdtmOrganisation.ShowOrgType
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.ShowOrgType(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView );
var
  aIDField : TField;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_ORGTYPE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmOrganisationType.ShowOrganisationType( aDataSet, aRecordViewMode );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a PostalCode.

  @Name       TdtmOrganisation.ShowPostalCode
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.ShowPostalCode(aDataSet: TDataSet; aInvoiceAddress : Boolean = False;
  aRecordViewMode: TPPWFrameWorkRecordViewMode = rvmView);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  if ( aInvoiceAddress ) then
  begin
    aCopyTo := critPostalCodeToOrgInvoiceAddress;
    aIDField := aDataSet.FindField( 'F_INVOICE_POSTALCODE_ID' );
  end
  else
  begin
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_POSTALCODE_ID' );
  end;

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmPostalcode.ShowPostalCode( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

{*****************************************************************************
  This method will be used to allow the user to Show a Country.

  @Name       TdtmOrganisation.ShowCountry
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmOrganisation.ShowCountry(aDataSet : TDataSet; aInvoiceAddress : Boolean = False; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); 
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  if ( aInvoiceAddress ) then
  begin
    aCopyTo := critPostalCodeToOrgInvoiceAddress;
    aIDField := aDataSet.FindField( 'F_INVOICE_COUNTRY_ID' );
  end
  else
  begin
    aCopyTo := critDefault;
    aIDField := aDataSet.FindField( 'F_COUNTRY_ID' );
  end;

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCountry.ShowCountry( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

procedure TdtmOrganisation.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ORGTYPE_ID' ).AsInteger := 7;
  aDataSet.FieldByName( 'F_ORGTYPE_NAME' ).AsString := 'Onbepaald';
  aDataSet.FieldByName( 'F_STREET' ).AsString := '';
  aDataSet.FieldByName( 'F_NUMBER' ).AsString := '';
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
  aDataSet.FieldByName( 'F_COUNTRY_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_COUNTRY_NAME' ).AsString := 'Belgi�';
  aDataSet.FieldByName( 'F_MEDIUM_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_MEDIUM_NAME' ).AsString := 'Post';
  aDataSet.FieldByName( 'F_LANGUAGE_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_LANGUAGE_NAME' ).AsString := 'Nederlands';
  aDataSet.FieldByName( 'F_MANUALLY_ADDED' ).Asboolean := True;
end;

{*****************************************************************************
  This method will be used to copy the Address to the InvoiceAddress.

  @Name       TdtmOrganisation.CopyAddressToInvoiceAddress
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
  @History
  21/11/2008  IVDBOSSCHE  Added copy of F_EXTRA_INFO to F_INVOICE_EXTRA_INFO
******************************************************************************}

procedure TdtmOrganisation.CopyAddressToInvoiceAddress(aDataSet: TDataSet);
begin
  if ( Assigned( aDataSet ) and
       ( aDataSet = cdsRecord ) ) then
  begin
    if not ( aDataSet.State in dsEditModes ) then
    begin
      aDataSet.Edit;
    end;

    aDataSet.FieldByName( 'F_INVOICE_STREET' ).AsString :=
      aDataSet.FieldByName( 'F_STREET' ).AsString;
    aDataSet.FieldByName( 'F_INVOICE_NUMBER' ).AsString :=
      aDataSet.FieldByName( 'F_NUMBER' ).AsString;
    aDataSet.FieldByName( 'F_INVOICE_MAILBOX' ).AsString :=
      aDataSet.FieldByName( 'F_MAILBOX' ).AsString;
    aDataSet.FieldByName( 'F_INVOICE_POSTALCODE_ID' ).AsInteger :=
      aDataSet.FieldByName( 'F_POSTALCODE_ID' ).AsInteger;
    aDataSet.FieldByName( 'F_INVOICE_POSTALCODE' ).AsString :=
      aDataSet.FieldByName( 'F_POSTALCODE' ).AsString;
    aDataSet.FieldByName( 'F_INVOICE_CITY_NAME' ).AsString :=
      aDataSet.FieldByName( 'F_CITY_NAME' ).AsString;
    aDataSet.FieldByName( 'F_INVOICE_COUNTRY_ID' ).AsInteger :=
      aDataSet.FieldByName( 'F_COUNTRY_ID' ).AsInteger;
    aDataSet.FieldByName( 'F_INVOICE_COUNTRY_NAME' ).AsString :=
      aDataSet.FieldByName( 'F_COUNTRY_NAME' ).AsString;
    aDataSet.FieldByName( 'F_INVOICE_EXTRA_INFO').AsString :=
      aDataSet.FieldByName( 'F_EXTRA_INFO' ).AsString;
  end;
end;

procedure TdtmOrganisation.cdsSearchCriteriaNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsSearchCriteria.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
end;

//Krijgt de ,-separated lijst van organisatie ID's binnen (van organisaties om samen te voegen)
procedure TdtmOrganisation.JoinOrganisations(OrganisationFilter: String);
var
  OrgIDToCopyTo: Integer;
begin
  cdsJoinToOrganisation.Close;
  cdsJoinToOrganisation.CreateDataSet;
  cdsJoinToOrganisation.Append;
  SelectOrganisation (cdsJoinToOrganisation, 'F_ORGANISATION_ID in (' + OrganisationFilter + ')', false, critJoinOrganisation, true);
  cdsJoinToOrganisation.Post;

  if TryStrToInt(cdsJoinToOrganisation.FieldByName('F_ORGANISATION_ID').AsString, OrgIDToCopyTo) then
  begin
    if MessageDlg( Format(rsOrganisationJoinedWith, [ OrganisationFilter, OrgIDToCopyTo ]), mtConfirmation, [mbYes,mbNo], 0) = mrYes then
      ssckData.AppServer.JoinOrganisations (OrganisationFilter, OrgIDToCopyTo);
    RefreshListDataSet;
  end;
end;



procedure TdtmOrganisation.JoinPersons(OrganisationID: Integer);
begin
  ssckData.AppServer.JoinPersons (OrganisationID);
end;

procedure TdtmOrganisation.cdsListNewRecord(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('F_LANGUAGE_ID').AsInteger := 1; // default Dutch
end;

function TdtmOrganisation.ClearPartnerWinter(aDataSet: TDataSet): Boolean;
var
  clearPartnerWinter: Boolean;
begin
  clearPartnerWinter := not IsEducationCenter(aDataSet);

  if clearPartnerWinter then
  begin
    aDataSet.FieldByName('F_PARTNER_WINTER').AsBoolean := False;
  end;
  Result := clearPartnerWinter;
end;

procedure TdtmOrganisation.ClearAuthorizationNr(aDataSet: TDataSet);
begin
  aDataSet.FieldByName('F_AUTHORIZATION_NR').AsString := '';
end;


function TdtmOrganisation.IsEducationCenter(aDataSet: TDataSet): Boolean;
const
  educationCenterTypeId = 11;
begin
  Result := aDataSet.FieldByName('F_ORGTYPE_ID').AsInteger = educationCenterTypeId;
end;

procedure TdtmOrganisation.UpdateAdminCostType ( aDataSet: TDataSet; adminCostTypeIndex: Integer );
const
  cFieldAdminCost = 'F_ADMIN_COST';
begin
  if not (aDataSet.State in [dsInsert, dsEdit]) then
  begin
    aDataSet.Edit;
  end;

  if adminCostTypeIndex = cUnselectedAdminCostType then
  begin
    aDataSet.FieldByName(cFieldAdminCost).AsVariant := Null;
  end else
  begin
    aDataSet.FieldByName(cFieldAdminCostType).AsInteger := adminCostTypeIndex;
  end;
end;

function TdtmOrganisation.GetAdminCostTypeItemIndex ( aDataSet: TDataSet ): Integer;
var
  v: Variant;
begin
  if (aDataSet.State in [dsInsert, dsEdit]) then
  begin
    //aDataSet.Post;
  end;

  v := aDataSet.FieldByName(cFieldAdminCostType).AsVariant;

  if VarIsNull(v) then
  begin
    Result := cUnselectedAdminCostType;
  end
  else
  begin
    if VarIsStr(v) then
    begin
      if not TryStrToInt(v, Result) then
      begin
        Result := cUnselectedAdminCostType;
      end
    end
    else
    begin
      Result := VarAsType(v, varInteger);
    end;
  end;
end;

end.
