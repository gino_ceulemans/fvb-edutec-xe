inherited dtmProgramType: TdtmProgramType
  KeyFields = 'F_PROGRAMTYPE_ID'
  ListViewClass = 'TfrmProgramType_List'
  RecordViewClass = 'TfrmProgramType_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_PROGRAMTYPE_ID,'
      '  F_NAME_NL,'
      '  F_NAME_FR,'
      '  F_PROGRAMTYPE_NAME'
      'FROM'
      '  V_T_PROGRAM_TYPE')
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      '  F_PROGRAMTYPE_ID,'
      '  F_NAME_NL,'
      '  F_NAME_FR,'
      '  F_PROGRAMTYPE_NAME'
      'FROM'
      '  V_T_PROGRAM_TYPE'
      '')
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_PROGRAMTYPE_ID: TIntegerField
      FieldName = 'F_PROGRAMTYPE_ID'
    end
    object cdsListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 40
    end
    object cdsListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 40
    end
    object cdsListF_PROGRAMTYPE_NAME: TStringField
      FieldName = 'F_PROGRAMTYPE_NAME'
      ReadOnly = True
      Size = 40
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_PROGRAMTYPE_ID: TIntegerField
      FieldName = 'F_PROGRAMTYPE_ID'
    end
    object cdsRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 40
    end
    object cdsRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 40
    end
    object cdsRecordF_PROGRAMTYPE_NAME: TStringField
      FieldName = 'F_PROGRAMTYPE_NAME'
      ReadOnly = True
      Size = 40
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmProgramType'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmProgramType'
  end
end
