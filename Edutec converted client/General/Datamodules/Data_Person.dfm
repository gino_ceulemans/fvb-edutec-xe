inherited dtmPerson: TdtmPerson
  AutoOpenDataSets = False
  KeyFields = 'F_PERSON_ID'
  ListViewClass = 'TfrmPerson_List'
  RecordViewClass = 'TfrmPerson_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Functies'
      ForeignKeys = 'F_PERSON_ID'
      DataModuleClass = 'TdtmRole'
      AutoCreate = False
    end
    item
      Caption = 'Inschrijvingen'
      ForeignKeys = 'F_PERSON_ID'
      DataModuleClass = 'TdtmEnrolment'
      AutoCreate = False
    end
    item
      Caption = 'Historiek'
      ForeignKeys = 'F_PERSON_ID'
      DataModuleClass = 'TdtmLogHistory'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = dspConnection
    object cdsListF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksr. Nr'
      FieldName = 'F_SOCSEC_NR'
      ProviderFlags = [pfInUpdate]
      Size = 11
    end
    object cdsListF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      ProviderFlags = []
      Size = 10
    end
    object cdsListF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsListF_MANUALLY_ADDED: TBooleanField
      DisplayLabel = 'Manueel toegevoegd'
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_CONSTRUCT_ID: TIntegerField
      DisplayLabel = 'Construct (ID)'
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PHONE: TStringField
      FieldName = 'F_PHONE'
    end
    object cdsListF_FAX: TStringField
      FieldName = 'F_FAX'
    end
    object cdsListF_GSM: TStringField
      FieldName = 'F_GSM'
    end
    object cdsListF_EMAIL: TStringField
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsListF_ROLE: TStringField
      DisplayWidth = 35
      FieldName = 'F_ROLE'
      ProviderFlags = []
      ReadOnly = True
      Size = 131
    end
    object cdsListF_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = dspConnection
    object cdsRecordF_PERSON_ID: TIntegerField
      DisplayLabel = 'Persoon ( ID )'
      FieldName = 'F_PERSON_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object cdsRecordF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksr. Nr'
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsRecordF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_STREET: TStringField
      DisplayLabel = 'Straat'
      FieldName = 'F_STREET'
      Size = 128
    end
    object cdsRecordF_NUMBER: TStringField
      DisplayLabel = 'Nummer'
      FieldName = 'F_NUMBER'
      Size = 5
    end
    object cdsRecordF_MAILBOX: TStringField
      DisplayLabel = 'Bus'
      FieldName = 'F_MAILBOX'
      Size = 10
    end
    object cdsRecordF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Visible = False
    end
    object cdsRecordF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsRecordF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
    object cdsRecordF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Required = True
      Size = 64
    end
    object cdsRecordF_PHONE: TStringField
      DisplayLabel = 'Telefoon'
      FieldName = 'F_PHONE'
    end
    object cdsRecordF_FAX: TStringField
      DisplayLabel = 'Fax'
      FieldName = 'F_FAX'
    end
    object cdsRecordF_GSM: TStringField
      DisplayLabel = 'GSM'
      FieldName = 'F_GSM'
    end
    object cdsRecordF_EMAIL: TStringField
      DisplayLabel = 'EMail'
      FieldName = 'F_EMAIL'
      Size = 128
    end
    object cdsRecordF_DATEBIRTH: TDateTimeField
      DisplayLabel = 'Geboortedatum'
      FieldName = 'F_DATEBIRTH'
    end
    object cdsRecordF_GENDER_ID: TIntegerField
      DisplayLabel = 'Geslacht ( ID )'
      FieldName = 'F_GENDER_ID'
      Visible = False
    end
    object cdsRecordF_GENDER_NAME: TStringField
      DisplayLabel = 'Geslacht'
      FieldName = 'F_GENDER_NAME'
      Size = 64
    end
    object cdsRecordF_LANGUAGE_ID: TIntegerField
      DisplayLabel = 'Taal ( ID )'
      FieldName = 'F_LANGUAGE_ID'
      Visible = False
    end
    object cdsRecordF_LANGUAGE_NAME: TStringField
      DisplayLabel = 'Taal'
      FieldName = 'F_LANGUAGE_NAME'
      Size = 64
    end
    object cdsRecordF_TITLE_ID: TIntegerField
      DisplayLabel = 'Titel ( ID )'
      FieldName = 'F_TITLE_ID'
      Visible = False
    end
    object cdsRecordF_TITLE_NAME: TStringField
      DisplayLabel = 'Titel'
      FieldName = 'F_TITLE_NAME'
      Size = 64
    end
    object cdsRecordF_MEDIUM_ID: TIntegerField
      DisplayLabel = 'Medium ( ID )'
      FieldName = 'F_MEDIUM_ID'
      Visible = False
    end
    object cdsRecordF_MEDIUM_NAME: TStringField
      DisplayLabel = 'Medium'
      FieldName = 'F_MEDIUM_NAME'
      Size = 64
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
      Visible = False
    end
    object cdsRecordF_ACC_NR: TStringField
      DisplayLabel = 'Rek. Nr'
      FieldName = 'F_ACC_NR'
      Size = 12
    end
    object cdsRecordF_MANUALLY_ADDED: TBooleanField
      DisplayLabel = 'Manueel toegevoegd'
      FieldName = 'F_MANUALLY_ADDED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_CONSTRUCT_ID: TIntegerField
      DisplayLabel = 'Construct (ID)'
      FieldName = 'F_CONSTRUCT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_DIPLOMA_ID: TIntegerField
      FieldName = 'F_DIPLOMA_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_DIPLOMA_NAME: TStringField
      FieldName = 'F_DIPLOMA_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsRecordF_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PLACE_OF_BIRTH: TStringField
      FieldName = 'F_PLACE_OF_BIRTH'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    OnNewRecord = cdsSearchCriteriaNewRecord
    object cdsSearchCriteriaF_SOCSEC_NR: TStringField
      DisplayLabel = 'Rijksr. Nr'
      FieldName = 'F_SOCSEC_NR'
      Size = 11
    end
    object cdsSearchCriteriaF_LASTNAME: TStringField
      DisplayLabel = 'Familienaam'
      FieldName = 'F_LASTNAME'
      Size = 64
    end
    object cdsSearchCriteriaF_FIRSTNAME: TStringField
      DisplayLabel = 'Voornaam'
      FieldName = 'F_FIRSTNAME'
      Size = 64
    end
    object cdsSearchCriteriaF_POSTALCODE_ID: TIntegerField
      DisplayLabel = 'Postcode ( ID )'
      FieldName = 'F_POSTALCODE_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_POSTALCODE: TStringField
      DisplayLabel = 'Postcode'
      FieldName = 'F_POSTALCODE'
      Size = 10
    end
    object cdsSearchCriteriaF_CITY_NAME: TStringField
      DisplayLabel = 'Gemeente'
      FieldName = 'F_CITY_NAME'
      Size = 128
    end
    object cdsSearchCriteriaF_COUNTRY_ID: TIntegerField
      DisplayLabel = 'Land ( ID )'
      FieldName = 'F_COUNTRY_ID'
      Visible = False
    end
    object cdsSearchCriteriaF_COUNTRY_NAME: TStringField
      DisplayLabel = 'Land'
      FieldName = 'F_COUNTRY_NAME'
      Size = 64
    end
    object cdsSearchCriteriaF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmPerson'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmPerson'
  end
  object cdsDoublePersons: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <
      item
        DataType = ftInteger
        Precision = 10
        Name = '@RETURN_VALUE'
        ParamType = ptResult
      end
      item
        DataType = ftInteger
        Precision = 10
        Name = '@APersonId'
        ParamType = ptInput
      end>
    ProviderName = 'prvP_SYS_RETRIEVE_DOUBLE_PERSONS'
    RemoteServer = dspConnection
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    Left = 72
    Top = 216
    object cdsDoublePersonsF_PERSON_ID: TIntegerField
      FieldName = 'F_PERSON_ID'
      ProviderFlags = []
    end
  end
end
