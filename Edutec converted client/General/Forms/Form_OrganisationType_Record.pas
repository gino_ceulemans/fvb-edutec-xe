{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_OrganisationType_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_OrganisationType_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons, cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxDBLabel, cxControls, cxContainer, cxEdit, cxLabel, Menus, cxSplitter;

type
  TfrmOrganisationType_Record = class(TEDURecordView)
    cxlblF_ORGTYPE_ID: TFVBFFCLabel;
    cxdblblF_ORGTYPE_ID: TFVBFFCDBLabel;
    cxlblF_ORGTYPE_NAME: TFVBFFCLabel;
    cxdblblF_ORGTYPE_NAME: TFVBFFCDBLabel;
    cxlblF_ORGTYPE_ID1: TFVBFFCLabel;
    cxdbseF_ORGTYPE_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_OrganisationType, Data_EduMainClient;

{ TfrmOrganisationType_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmOrganisationType_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmOrganisationType_Record.GetDetailName: String;
begin
  Result := cxdblblF_ORGTYPE_NAME.caption; // Inherited GetDetailName;
end;

end.