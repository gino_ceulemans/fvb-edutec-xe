{*****************************************************************************
  This unit contains the form that will be used to display a list of
  T_GE_POSTALCODE records.
  
  @Name       Form_Postalcode_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Postalcode_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, StdCtrls, cxButtons, Unit_FVBFFCFoldablePanel,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, cxMaskEdit, cxLabel, cxButtonEdit, cxContainer, cxTextEdit,
  Unit_PPWFrameWorkInterfaces, cxDBEdit, Menus;

type
  TfrmPostalcode_List = class(TEduListView)
    cxlblPostcodeID: TFVBFFCLabel;
    cxlblLblGemeente: TFVBFFCLabel;
    cxlblProvincie: TFVBFFCLabel;
    cxlblPostCode: TFVBFFCLabel;
    cxdbbeProvincie: TFVBFFCDBButtonEdit;
    cxdbtePostalCodeID: TFVBFFCDBTextEdit;
    cxdbtePostalCode: TFVBFFCDBTextEdit;
    cxdbteCity: TFVBFFCDBTextEdit;
    cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROVINCE_ID: TcxGridDBColumn;
    cxgrdtblvListF_CITY_NL: TcxGridDBColumn;
    cxgrdtblvListF_CITY_FR: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE: TcxGridDBColumn;
    cxgrdtblvListF_CITY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PROVINCE_NAME: TcxGridDBColumn;
    procedure cxdbbeProvinciePropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

var
  frmPostalcode_List: TfrmPostalcode_List;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Postalcode, Data_EDUMainClient, Unit_FVBFFCInterfaces, unit_EdutecInterfaces;

{ TfrmPostalcode_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmPostalcode_List.GetFilterString
  @author     slesage
  @param      None
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPostalcode_List.GetFilterString: String;
var
  aFilterString : String;
  aCityLike     : String;
begin
  { Add the condition for the PostalCode ID }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_POSTALCODE_ID', FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE_ID' ).AsInteger );
  end;

  { Add the condition for the PostalCode }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_POSTALCODE', FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).AsString );
  end;

  { Add the Condition for the City }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aCityLike, 'F_CITY_NL', FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).AsString, 'OR' );
    AddLikeFilterCondition( aCityLike, 'F_CITY_FR', FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).AsString, 'OR' );
    AddFilterCondition( aFilterString, aCityLike );
  end;

  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_PROVINCE_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_PROVINCE_ID', srcSearchCriteria.DataSet.FieldByName( 'F_PROVINCE_ID' ).AsInteger );
  end;

  Result := aFilterString;
end;

{*****************************************************************************
  This method will be executed when the user clicks the Select Province button.

  @Name       TfrmPostalcode_List.cxdbbeProvinciePropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPostalcode_List.cxdbbeProvinciePropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aPostalCodeDataModule : IEDUPostalCodeDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUPostalCodeDataModule, aPostalCodeDataModule ) ) then
  begin
    aPostalCodeDataModule.SelectProvince( FSearchCriteriaDataSet );
  end;
end;

end.