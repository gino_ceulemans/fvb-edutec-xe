{*****************************************************************************
  This unit contains the form that will be used to display a list of
  T_SYS_USER records.

  @Name       Form_User_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_User_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, StdCtrls, cxButtons, Unit_FVBFFCFoldablePanel,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, cxCheckBox, cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxContainer, cxLabel, Menus, cxDropDownEdit, cxImageComboBox;

type
  TfrmUser_List = class(TEduListView)
    cxgrdtblvListF_USER_ID: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_PHONE_INT: TcxGridDBColumn;
    cxgrdtblvListF_PHONE_EXT: TcxGridDBColumn;
    cxgrdtblvListF_GSM: TcxGridDBColumn;
    cxgrdtblvListF_EMAIL: TcxGridDBColumn;
    cxgrdtblvListF_LOGIN_ID: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxgrdtblvListF_GENDER_ID: TcxGridDBColumn;
    cxgrdtblvListF_LOCKED: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_LOGIN_TRY: TcxGridDBColumn;
    cxgrdtblvListF_DOMAIN: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn;
    cxgrdtblvListF_COMPLETE_USERNAME: TcxGridDBColumn;
    cxlblF_USER_ID2: TFVBFFCLabel;
    cxdbseF_USER_ID: TFVBFFCDBSpinEdit;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdbteF_LASTNAME: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit;
    cxlblF_LOCKED1: TFVBFFCLabel;
    csdbcbF_LOCKED: TFVBFFCDBCheckBox;
    cxlblF_LOGIN_ID1: TFVBFFCLabel;
    cxdbteF_LOGIN_ID: TFVBFFCDBTextEdit;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

var
  frmUser_List: TfrmUser_List;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_User, Data_EDUMainClient, Form_FVBFFCBaseListView;

{ TfrmUser_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmUser_List.GetFilterString
  @author     slesage
  @param      None
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmUser_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the condition for the User ID }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_USER_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_USER_ID', FSearchCriteriaDataSet.FieldByName( 'F_USER_ID' ).AsInteger );
  end;

  { Add the condition for the FirstName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_FIRSTNAME', FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).AsString );
  end;

  { Add the condition for the LastName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_LASTNAME', FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).AsString );
  end;

  { Add the condition for the LoginID }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_LOGIN_ID' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_LOGIN_ID', FSearchCriteriaDataSet.FieldByName( 'F_LOGIN_ID' ).AsString );
  end;

  { Add the condition for the Locked }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_LOCKED' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_LOCKED', Ord( FSearchCriteriaDataSet.FieldByName( 'F_LOCKED' ).AsBoolean ) );
  end;

  { Add the condition for the Active }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddBlnEqualCondition ( aFilterString, 'F_ACTIVE', FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;

  Result := aFilterString;
end;

end.
