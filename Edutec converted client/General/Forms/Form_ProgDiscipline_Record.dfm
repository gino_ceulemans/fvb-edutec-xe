inherited frmProgDiscipline_Record: TfrmProgDiscipline_Record
  Width = 939
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Discipline'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 931
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 595
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 931
    inherited pnlRecord: TFVBFFCPanel
      Width = 672
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 672
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 672
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 672
        object cxlblF_PROFESSION_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgDiscipline_Record.F_PROFESSION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Discipline ( ID )'
          FocusControl = cxdblblF_PROFESSION_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROFESSION_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgDiscipline_Record.F_PROFESSION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROFESSION_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 89
        end
        object cxlblF_PROFESSION_NAME1: TFVBFFCLabel
          Left = 112
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgDiscipline_Record.F_PROFESSION_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Discipline'
          FocusControl = cxdblblF_PROFESSION_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROFESSION_NAME: TFVBFFCDBLabel
          Left = 112
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgDiscipline_Record.F_PROFESSION_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_PROFESSION_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 644
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 672
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 672
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 672
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 672
        end
        inherited sbxMain: TScrollBox
          Width = 672
          object cxlblF_PROFESSION_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgDiscipline_Record.F_PROFESSION_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Discipline ( ID )'
            FocusControl = cxdbseF_PROFESSION_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PROFESSION_ID: TFVBFFCDBSpinEdit
            Left = 120
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgDiscipline_Record.F_PROFESSION_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PROFESSION_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgDiscipline_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Discipline ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 120
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgDiscipline_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 313
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgDiscipline_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Discipline ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 120
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgDiscipline_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 313
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 927
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmProgDiscipline.cdsRecord
  end
end
