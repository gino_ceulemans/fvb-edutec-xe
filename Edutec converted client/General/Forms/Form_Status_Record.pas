{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_Status_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Status_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxCheckBox,
  cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit, cxControls, cxContainer,
  cxEdit, cxLabel, Menus, cxSplitter;

type
  TfrmStatus_Record = class(TEDURecordView)
    cxlblF_STATUS_ID: TFVBFFCLabel;
    cxdbseF_STATUS_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    cxlblF_LONG_DESC_NL: TFVBFFCLabel;
    cxdbteF_LONG_DESC_NL: TFVBFFCDBTextEdit;
    cxlblF_LONG_DESC_FR: TFVBFFCLabel;
    cxdbteF_LONG_DESC_FR: TFVBFFCDBTextEdit;
    cxlblF_SESSION: TFVBFFCLabel;
    cxdbcbF_SESSION: TFVBFFCDBCheckBox;
    cxlblF_ENROL: TFVBFFCLabel;
    cxdbcbF_ENROL: TFVBFFCDBCheckBox;
    cxlblF_WAITING_LIST: TFVBFFCLabel;
    cxdbcbF_WAITING_LIST: TFVBFFCDBCheckBox;
    cxlblF_STATUS_ID2: TFVBFFCLabel;
    cxdblblF_STATUS_ID: TFVBFFCDBLabel;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    cxdblblF_STATUS_NAME: TFVBFFCDBLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Status, Data_EduMainClient;

{ TfrmStatus_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmStatus_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmStatus_Record.GetDetailName: String;
begin
  Result := cxdblblF_STATUS_NAME.Caption;
end;

end.