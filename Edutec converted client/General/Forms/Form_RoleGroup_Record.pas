{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_RO_GROUP record.

  @Name       Form_RoleGroup_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_RoleGroup_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmRoleGroup_Record = class(TEDURecordView)
    cxlblF_ROLEGROUP_ID1: TFVBFFCLabel;
    cxdblblF_ROLEGROUP_ID: TFVBFFCDBLabel;
    cxlblF_ROLEGROUP_NAME1: TFVBFFCLabel;
    cxdblblF_ROLEGROUP_NAME: TFVBFFCDBLabel;
    cxlblF_ROLEGROUP_ID2: TFVBFFCLabel;
    cxdbseF_ROLEGROUP_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmRoleGroup_Record: TfrmRoleGroup_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_RoleGroup, Data_EDUMainClient;

{ TfrmRoleGroup_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmRoleGroup_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmRoleGroup_Record.GetDetailName: String;
begin
  Result := cxdblblF_ROLEGROUP_NAME.Caption;
end;

end.