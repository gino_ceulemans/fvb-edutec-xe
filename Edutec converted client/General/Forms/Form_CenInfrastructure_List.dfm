inherited frmCenInfrastructure_List: TfrmCenInfrastructure_List
  Left = 259
  Top = 237
  Width = 906
  Height = 449
  ActiveControl = cxdbteF_NAME
  Caption = 'Opleidingsinfrastructuren'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 381
    Width = 898
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 421
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 533
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 890
    Height = 373
    inherited cxgrdList: TcxGrid
      Top = 166
      Width = 890
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_INFRASTRUCTURE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_ID'
          Visible = False
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 200
        end
        object cxgrdtblvListF_FVB_CENTER_ID: TcxGridDBColumn
          Caption = 'FVB Centrum (ID)'
          DataBinding.FieldName = 'F_FVB_CENTER_ID'
          Visible = False
        end
        object cxgrdtblvListF_VAT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_VAT_NR'
          Width = 68
        end
        object cxgrdtblvListF_STREET: TcxGridDBColumn
          DataBinding.FieldName = 'F_STREET'
          Width = 200
        end
        object cxgrdtblvListF_NUMBER: TcxGridDBColumn
          DataBinding.FieldName = 'F_NUMBER'
          Width = 64
        end
        object cxgrdtblvListF_MAILBOX: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAILBOX'
        end
        object cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE_ID'
          Visible = False
        end
        object cxgrdtblvListF_POSTALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE'
          Width = 84
        end
        object cxgrdtblvListF_CITY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_NAME'
          Width = 200
        end
        object cxgrdtblvListF_PHONE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE'
        end
        object cxgrdtblvListF_FAX: TcxGridDBColumn
          DataBinding.FieldName = 'F_FAX'
        end
        object cxgrdtblvListF_WEBSITE: TcxGridDBColumn
          DataBinding.FieldName = 'F_WEBSITE'
          Visible = False
          Width = 200
        end
        object cxgrdtblvListF_ACC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACC_NR'
          Width = 97
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Visible = False
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          Visible = False
        end
        object cxgrdtblvListF_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_COMMENT'
          Visible = False
        end
        object cxgrdtblvListF_LOCATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LOCATION_ID'
          Visible = False
        end
        object cxgrdtblvListF_LOCATION: TcxGridDBColumn
          DataBinding.FieldName = 'F_LOCATION'
          Visible = False
        end
        object cxgrdtblvListF_PRECONDITION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRECONDITION_ID'
          Visible = False
        end
        object cxgrdtblvListF_PRECONDITION: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRECONDITION'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 137
      Width = 890
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 162
      Width = 890
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 890
      Height = 137
      ExpandedHeight = 137
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 106
        Width = 888
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 720
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 808
        end
      end
      object cxdbtePostalCode: TFVBFFCDBTextEdit
        Left = 80
        Top = 56
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 3
        Width = 217
      end
      object cxlblF_NAME2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam'
        FocusControl = cxdbteF_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME: TFVBFFCDBTextEdit
        Left = 80
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
        DataBinding.DataField = 'F_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 1
        Width = 513
      end
      object cxlblF_CITY_NAME1: TFVBFFCLabel
        Left = 304
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Gemeente'
        FocusControl = cxdbteF_CITY_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_CITY_NAME: TFVBFFCDBTextEdit
        Left = 376
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        DataBinding.DataField = 'F_CITY_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 5
        Width = 216
      end
      object cxlblF_POSTALCODE1: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Postcode'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 80
        Top = 80
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Items = <>
        TabOrder = 8
        Width = 216
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 898
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 377
    Width = 898
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 894
    Height = 373
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 373
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmCenInfrastructure.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmCenInfrastructure.cdsSearchCriteria
  end
end
