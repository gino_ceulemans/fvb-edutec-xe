inherited frmSchoolyear_Record: TfrmSchoolyear_Record
  Width = 829
  ActiveControl = cxdbteF_SCHOOLYEAR
  Caption = 'Schooljaar'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 821
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 485
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 821
    inherited pnlRecord: TFVBFFCPanel
      Width = 562
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 562
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 562
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 562
        object cxlblF_SCHOOLYEAR_ID2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Shooljaar ( ID )'
          FocusControl = cxdblblF_SCHOOLYEAR_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SCHOOLYEAR_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_SCHOOLYEAR_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 97
        end
        object cxlblF_SCHOOLYEAR2: TFVBFFCLabel
          Left = 128
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Schooljaar'
          FocusControl = cxdblblF_SCHOOLYEAR
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SCHOOLYEAR: TFVBFFCDBLabel
          Left = 128
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_SCHOOLYEAR'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 518
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 562
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 562
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 562
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 562
        end
        inherited sbxMain: TScrollBox
          Width = 562
          object cxlblF_SCHOOLYEAR_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Schooljaar ( ID )'
            FocusControl = cxdbseF_SCHOOLYEAR_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_SCHOOLYEAR_ID: TFVBFFCDBSpinEdit
            Left = 120
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_SCHOOLYEAR_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 136
          end
          object cxlblF_SCHOOLYEAR1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Schooljaar'
            FocusControl = cxdbteF_SCHOOLYEAR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_SCHOOLYEAR: TFVBFFCDBTextEdit
            Left = 120
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_SCHOOLYEAR'
            DataBinding.DataField = 'F_SCHOOLYEAR'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 377
          end
          object cxlblF_START_DATE1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_START_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Start Datum'
            FocusControl = cxdbdeF_START_DATE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_START_DATE: TFVBFFCDBDateEdit
            Left = 120
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_START_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_START_DATE'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 136
          end
          object cxlblF_END_DATE1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Eind Datum'
            FocusControl = cxdbdeF_END_DATE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_END_DATE: TFVBFFCDBDateEdit
            Left = 120
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_END_DATE'
            DataBinding.DataSource = srcMain
            TabOrder = 7
            Width = 136
          end
          object cxlblF_ACTIVE1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_ACTIVE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Actief'
            FocusControl = csdbcbF_ACTIVE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object csdbcbF_ACTIVE: TFVBFFCDBCheckBox
            Left = 120
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmSchoolyear_Record.F_ACTIVE'
            Caption = 'csdbcbF_ACTIVE'
            DataBinding.DataField = 'F_ACTIVE'
            DataBinding.DataSource = srcMain
            ParentColor = False
            Properties.NullStyle = nssInactive
            Properties.ReadOnly = True
            TabOrder = 9
            Width = 21
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 817
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmSchoolyear.cdsRecord
  end
end
