unit form_Attest_VCA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Unit_FVBFFCDevExpress, ExtCtrls, Unit_FVBFFCFoldablePanel, cxControls,
  cxContainer, cxEdit, cxLabel, cxTextEdit, cxMaskEdit, cxSpinEdit,
  ActnList, Unit_FVBFFCComponents;

type
  TAttestData_VCA = record
    TotalSessionScore : Integer;
    Score : Integer;
    EnableAction : boolean;
  end;

  TfrmAttest_VCA = class(TForm)
    FVBFFCPanel1: TFVBFFCPanel;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    cxbtnCancel: TFVBFFCButton;
    cxlblF_SCORE: TFVBFFCLabel;
    cxseF_SCORE: TFVBFFCSpinEdit;
    alButtons: TFVBFFCActionList;
    acOk: TAction;
    acCancel: TAction;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxseF_MAX_SCORE: TFVBFFCSpinEdit;
    bvlBevel1: TBevel;
    procedure acOkUpdate(Sender: TObject);
    procedure acOkExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function GetAttestData_VCA (var AttestData_VCA: TAttestData_VCA): TModalResult;
{
resourcestring
  rs_RecordsSelected = '%d afspraken geselecteerd';
}
implementation

{$R *.dfm}


{*****************************************************************************
  Creates the Attest form and obtains the attest data.
  @Name       GetAttestData
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function GetAttestData_VCA (var AttestData_VCA: TAttestData_VCA): TModalResult;
var
  frmAttest_VCA: TfrmAttest_VCA;
begin
  frmAttest_VCA := TfrmAttest_VCA.Create(nil);
  try
    frmAttest_VCA.cxseF_MAX_SCORE.Value := AttestData_VCA.TotalSessionScore;
    result := frmAttest_VCA.ShowModal;
    AttestData_VCA.Score         :=
      frmAttest_VCA.cxseF_SCORE.Value;
    AttestData_VCA.TotalSessionScore         :=
      frmAttest_VCA.cxseF_MAX_SCORE.Value;
  finally
    freeAndNil(frmAttest_VCA);
  end;
end;

{*****************************************************************************
  Procedure to check if the input equals the total of hours of the master
  ( session ) and the total not equals zero. ( last not needed any more but
  can't hurt to have it checked more than once ;)

  @Name       TfrmAttest.acOkUpdate
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmAttest_VCA.acOkUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := True;
end;

procedure TfrmAttest_VCA.acOkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmAttest_VCA.acCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
