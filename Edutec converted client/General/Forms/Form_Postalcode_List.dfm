inherited frmPostalcode_List: TfrmPostalcode_List
  ActiveControl = cxdbtePostalCodeID
  Caption = 'Postcodes'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      Top = 166
      Height = 223
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE_ID'
          Width = 100
        end
        object cxgrdtblvListF_PROVINCE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROVINCE_NAME'
          Width = 160
        end
        object cxgrdtblvListF_POSTALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE'
          Width = 100
        end
        object cxgrdtblvListF_CITY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_NAME'
          Width = 400
        end
        object cxgrdtblvListF_PROVINCE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROVINCE_ID'
          Visible = False
        end
        object cxgrdtblvListF_CITY_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_FR'
          Visible = False
        end
        object cxgrdtblvListF_CITY_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_NL'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 141
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 137
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Height = 137
      ExpandedHeight = 137
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 106
        TabOrder = 8
      end
      object cxlblPostcodeID: TFVBFFCLabel
        Left = 8
        Top = 32
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Postcode ( ID )'
        FocusControl = cxdbtePostalCodeID
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxlblLblGemeente: TFVBFFCLabel
        Left = 8
        Top = 56
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Gemeente'
        FocusControl = cxdbteCity
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxlblProvincie: TFVBFFCLabel
        Left = 8
        Top = 80
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Provincie'
        FocusControl = cxdbbeProvincie
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxlblPostCode: TFVBFFCLabel
        Left = 272
        Top = 32
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Postcode'
        FocusControl = cxdbtePostalCode
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeProvincie: TFVBFFCDBButtonEdit
        Left = 120
        Top = 80
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_PROVINCE_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeProvinciePropertiesButtonClick
        TabOrder = 3
        Width = 361
      end
      object cxdbtePostalCodeID: TFVBFFCDBTextEdit
        Left = 120
        Top = 32
        DataBinding.DataField = 'F_POSTALCODE_ID'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 0
        Width = 145
      end
      object cxdbtePostalCode: TFVBFFCDBTextEdit
        Left = 336
        Top = 32
        DataBinding.DataField = 'F_POSTALCODE'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 1
        Width = 145
      end
      object cxdbteCity: TFVBFFCDBTextEdit
        Left = 120
        Top = 56
        DataBinding.DataField = 'F_CITY_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 2
        Width = 361
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmPostalcode.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmPostalcode.cdsSearchCriteria
  end
end
