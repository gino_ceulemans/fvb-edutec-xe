{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <T_SES_ENROL> records.


  @Name       Form_Enrolment_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/09/2007   ivdbossche           IIPWFrameWorkDeleteMultiSelect implementation
                                    -> Added DeleteRecords & DeleteSelectedRecords
  28/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Enrolment_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  Menus, ActnList, Unit_FVBFFCComponents, form_Attest, Unit_PPWFrameWorkInterfaces,
  cxCalc, cxCheckBox, form_Attest_VCA,
  cxButtonEdit;

type
  TfrmEnrolment_List = class(TEDUListView, IIPWFrameWorkFormDeleteMultiSelect)
    cxgrdtblvListF_ENROL_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListF_EVALUATION: TcxGridDBColumn;
    cxgrdtblvListF_CERTIFICATE: TcxGridDBColumn;
    cxgrdtblvListF_INVOICED: TcxGridDBColumn;
    cxgrdtblvListF_LAST_MINUTE: TcxGridDBColumn;
    cxgrdtblvListF_HOURS: TcxGridDBColumn;
    cxgrdtblvListF_HOURS_PRESENT: TcxGridDBColumn;
    cxgrdtblvListF_HOURS_ABSENT_JUSTIFIED: TcxGridDBColumn;
    cxgrdtblvListF_HOURS_ABSENT_ILLEGIT: TcxGridDBColumn;
    alEnrollment: TFVBFFCActionList;
    acAttest: TAction;
    dxBarButton1: TdxBarButton;
    cxgrdtblvListF_ROLE_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    acPrintCertificate: TAction;
    dxBarButton2: TdxBarButton;
    cxgrdtblvListF_SELECTED_PRICE: TcxGridDBColumn;
    acMoveToOtherSession: TAction;
    dxbbMoveToOtherSession: TdxBarButton;
    cxgrdtblvListRowNumber: TcxGridDBColumn;
    acSetIngeschreven: TAction;
    cxgrdtblvListF_CERTIFICATE_PRINT_DATE: TcxGridDBColumn;
    acSetGeattesteerd: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    cxgrdtblvListF_ROLEGROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_NO_PARTICIPANTS: TcxGridDBColumn;
    cxgrdtblvListF_CODE: TcxGridDBColumn;
    cxgrdtblvListColumnF_CONSTRUCT: TcxGridDBColumn;
    cxgrdtblvListWinterPartner: TcxGridDBColumn;
    cxgrdtblvListF_ORGTYPE_ID: TcxGridDBColumn;
    cxgrdtblvListF_SCORE: TcxGridDBColumn;
    cxgrdtblvListF_MAX_SCORE: TcxGridDBColumn;
    procedure acAttestExecute(Sender: TObject);
    procedure dxbpmnGridPopup(Sender: TObject);
    procedure acPrintCertificateExecute(Sender: TObject);
    procedure FVBFFCListViewActivate(Sender: TObject);
    procedure acMoveToOtherSessionExecute(Sender: TObject);
    procedure acSetIngeschrevenExecute(Sender: TObject);
    procedure acSetGeattesteerdExecute(Sender: TObject);
    procedure acMoveToOtherSessionUpdate(Sender: TObject);
    procedure cxgrdtblvListColumn1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxgrdtblvListEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
  private
  private
    { Private declarations }
    AttestData: TAttestData;
    AttestData_VCA: TAttestData_VCA;
    FSelectedEnrollments: TStringList;
    FEnrollmentsList: String;

    procedure SetAttestationData;
    procedure SetIngeschreven;
    procedure CheckSelectedRecords;
    procedure GetEnrollments;
    procedure CertificateReport(const preview: Boolean);
    procedure SetGeattesteerd;
    procedure DeleteRecord;
    procedure DeleteSelectedRecords;
  public
    procedure OnCertificatesPreviewClosed (Sender: TObject);
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Enrolment, Data_EduMainClient, Unit_PPWFrameWorkForm, DBClient,
  Form_FVBFFCBaseListView, Data_EDUDataModule, unit_EdutecInterfaces,
  cxContainer, cxGridDBDataDefinitions, Unit_PPWFrameWorkListView;

resourcestring
  rsCertificaatReportName = 'Certficaat';
  rsReportNoCertificates = 'Er kunnen geen certificaten worden afgedrukt.' + #13#10 + #13#10 +
    'Mogelijk hebben de inschrijvingen of de sessie een status die het afdrukken van certificaten niet toelaat.';
  rsReportNotAllCertificates = 'Voor sommige inschrijvingen kunnen GEEN certificaten worden afgedrukt.' + #13#10 + #13#10 +
    'Mogelijk hebben deze inschrijvingen een status die het afdrukken van certificaten niet niet toelaat.';
  rsMarkEnrollmentsAsCertificated = 'Certifica(a)t(en) als afgedrukt markeren ?';
  rsConfirmDeletionEnrolmentRecords='Bent u zeker dat u de gekozen inschrijving(en) wilt verwijderen?';
  rsUnableToDeleteEnrolment='De inschrijving van %s %s kan niet gewist worden daar deze reeds geattesteerd/gefaktureerd is.';
  rsMoveToSessionNotComplete='Een of meerdere inschrijvingen konden niet naar een andere sessie verplaatst worden omdat de perso(o)n(en) reeds is(zijn) ingeschreven in die sessie.';

{*****************************************************************************
  Extra action added to make sure the attestation can be done properly.

  @Name       TfrmEnrolment_List.acAttestExecute
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_List.acAttestExecute(Sender: TObject);
begin
  inherited;

  if srcMain.DataSet.FieldByName('F_PROGRAM_TYPE').AsInteger = 1 then
  begin
    AttestData.TotalSessionHours :=
      srcMain.DataSet.FieldByName('F_TOTAL_SESSION_HOURS').AsInteger;
    if (GetAttestData (AttestData) = mrOk) then
    begin
      LoopThroughSelectedRecords (SetAttestationData);
    end;
  end
  else
  begin
    if srcMain.DataSet.FieldByName('F_MAX_SCORE').AsInteger = 0 then
      AttestData_VCA.TotalSessionScore :=
        srcMain.DataSet.FieldByName('F_MAX_PUNTEN').AsInteger
    else
      AttestData_VCA.TotalSessionScore :=
        srcMain.DataSet.FieldByName('F_MAX_SCORE').AsInteger;

    if (GetAttestData_VCA (AttestData_VCA) = mrOk) then
    begin
      LoopThroughSelectedRecords (SetAttestationData);
    end;
  end;
end;

procedure TfrmEnrolment_List.acSetIngeschrevenExecute(Sender: TObject);
begin
  inherited;
  LoopThroughSelectedRecords (SetIngeschreven);
end;

procedure TfrmEnrolment_List.SetIngeschreven;
var oldTag: Integer;
begin
  with srcMain.Dataset do
  begin
    if FieldByName('F_STATUS_ID').AsInteger = 10 then //geattesteerd
    begin
      oldTag := Tag;
      try
        Tag := 1;
        Edit;
        FieldByName('F_STATUS_ID').AsInteger := 6; //ingeschreven
        FieldByName('F_HOURS_PRESENT').AsInteger := 0;
        FieldByName('F_HOURS_ABSENT_JUSTIFIED').AsInteger := 0;
        FieldByName('F_HOURS_ABSENT_ILLEGIT').AsInteger := 0;
        FieldByName ('F_STATUS_NAME').AsString := dtmEDUMainClient.GetNameForState( 6 );
        Post;
      finally
        Tag := oldTag;
      end;
    end;
  end;
end;

{*****************************************************************************
  Attestion is only possible for records with state <> 'verstuurd'
  or <> 'Gefactureerd' !
  @Name       TfrmEnrolment_List.SetAttestationData
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_List.SetAttestationData;
var
  oldTag: Integer;
begin

  assert (srcMain.Dataset <> nil);
  oldTag := srcMain.Dataset.Tag;

  try
    if not( TStatus(srcMain.DataSet.FieldByName( 'F_STATUS_ID' ).AsInteger)
    in [ {stAttested,} stSend, stInvoiced ] ) then
    begin
      // First set the F_FLAG back to 'O'
      // make sure we get the update done (remember this is a listview !)
      srcMain.Dataset.Tag := 1;
      srcMain.Dataset.Edit;
      if srcMain.DataSet.FieldByName('F_PROGRAM_TYPE').AsInteger = 1 then
      begin
        srcMain.Dataset.FieldByName ('F_HOURS_PRESENT').AsInteger          :=
          AttestData.HoursPresent;
        srcMain.Dataset.FieldByName ('F_HOURS_ABSENT_JUSTIFIED').AsInteger :=
          AttestData.HoursAbsentJustified;
        srcMain.Dataset.FieldByName ('F_HOURS_ABSENT_ILLEGIT').AsInteger   :=
          AttestData.HoursAbsentIllegit;
      end
      else
      begin
        srcMain.Dataset.FieldByName ('F_SCORE').AsInteger          :=
          AttestData_VCA.Score;
        srcMain.Dataset.FieldByName ('F_MAX_SCORE').AsInteger      :=
          AttestData_VCA.TotalSessionScore;
      end;
      srcMain.Dataset.FieldByName ('F_STATUS_ID').AsInteger   :=
        10;
      srcMain.Dataset.FieldByName ('F_STATUS_NAME').AsString :=
        dtmEDUMainClient.GetNameForState( 10 );
      // Then try to process the record
      srcMain.Dataset.Post;
    end;
  finally
    srcMain.Dataset.Tag := oldTag;
  end;

end;

{*****************************************************************************
  Extra check when accessing popupmenu to allow action Attestation to be active
  or not ( instead of checking after validating the input in Form_Attest )

  @Name       TfrmEnrolment_List.dxbpmnGridPopup
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_List.dxbpmnGridPopup(Sender: TObject);
begin
  inherited;

  // Ook indien er geen uren zijn om te attesteren op sessie niveau
  // is attesteren niet echt aangewezen
  acAttest.Enabled :=
    ( srcMain.DataSet.FieldByName( 'F_TOTAL_SESSION_HOURS' ).AsInteger > 0 );
  // Indien er reeds uren zijn geattesteerd of alle geselecteerde inschr.n
  // reeds zijn verzonden wordt dit item niet meer toegankelijk gezet
  // ( Echter als er 1 inschr is die nog moet w geattesteerd moet dit toe-
  // gankelijk gezet worden )
  if acAttest.Enabled then
  begin
    if srcMain.DataSet.FieldByName('F_PROGRAM_TYPE').AsInteger = 1 then
      AttestData.EnableAction := False
    else
      AttestData_VCA.EnableAction := False;
    LoopThroughSelectedRecords ( CheckSelectedRecords );
    if srcMain.DataSet.FieldByName('F_PROGRAM_TYPE').AsInteger = 1 then
      acAttest.Enabled := AttestData.EnableAction
    else
      acAttest.Enabled := AttestData_VCA.EnableAction;
  end;

end;

{*****************************************************************************
  Procedure to make the action enabled if only one of all selected records
  not in status ( Send or Invoiced ) !

  @Name       TfrmEnrolment_List.CheckSelectedRecords
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_List.CheckSelectedRecords;
begin
  if not ( TStatus(srcMain.DataSet.FieldByName( 'F_STATUS_ID' ).AsInteger)
    in [ {stAttested,} stSend, stInvoiced ] ) then
  begin
    if srcMain.DataSet.FieldByName('F_PROGRAM_TYPE').AsInteger = 1 then
      AttestData.EnableAction := True
    else
      AttestData_VCA.EnableAction := True;
  end;
end;

procedure TfrmEnrolment_List.acPrintCertificateExecute(Sender: TObject);
begin
  inherited;
  FSelectedEnrollments := TStringList.Create;
  try
    if TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount > 1 then
    begin //multiple sessions selected
      LoopThroughSelectedRecords(GetEnrollments);
    end
    else begin //only one session selected
      FSelectedEnrollments.Add(srcMain.Dataset.FieldByName ('F_ENROL_ID').AsString);
    end;

    CertificateReport (true);
  finally
    FreeAndNil (FSelectedEnrollments); //FSelectedEnrollments will be freed in TdtmCrystal.PrintReport
  end;
end;

procedure TfrmEnrolment_List.GetEnrollments;
begin
  FSelectedEnrollments.Add(srcMain.Dataset.FieldByName ('F_ENROL_ID').AsString);
end;

procedure TfrmEnrolment_List.CertificateReport (const preview: Boolean);
var
  lReportName, lReportTitle: string;
  ParamNames, ParamValues: TStringList;
  oldCursor: TCursor;
  recordcount: Integer;
begin
  //First let's check whether the report will at least be returning 1 record !
  recordcount := dtmEDUMainClient.ReportRecordCount('V_REP_CERTIFICATE', 'F_ENROL_ID',
    'F_ENROL_ID', FSelectedEnrollments);
  if (recordcount = 0) then
  begin
    MessageDlg(rsReportNoCertificates, mtError, [mbOk], 0);
    Exit;
  end;

  if (recordcount < FSelectedEnrollments.Count) then
  begin
    MessageDlg(Format(rsReportNotAllCertificates, [FSelectedEnrollments.Count - recordcount]), mtWarning, [mbOk], 0);
  end;

  lReportName   := 'certificaat.rpt';
  lReportTitle  := rsCertificaatReportName;
  ParamNames    := TStringList.Create;
  ParamValues   := TStringList.Create;
  oldCursor     := screen.cursor;
  screen.cursor := crSQLWait;
  try
    ParamNames.Add('p_enrol_id');
    ParamValues.AddObject('', FSelectedEnrollments);
    FEnrollmentsList := FSelectedEnrollments.CommaText;
    dtmEDUMainClient.ExecuteCrystal(lReportName, lReportTitle, ParamNames, ParamValues, preview, False, OnCertificatesPreviewClosed);
    FSelectedEnrollments := nil; // FSelectedEnrollments has been freed in TdtmCrystal.PrintReport
      //have to look at this when more time !
  finally
    screen.cursor := oldCursor;
    FreeAndNil (ParamValues);
    FreeAndNil (ParamNames);
    //FreeAndNil(dtm);
  end;
end;

{*****************************************************************************
  Name           : OnCertificatesPreviewClosed
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    :
  History        :

  Date         By                   Description
  ----         --                   -----------
  08/06/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
procedure TfrmEnrolment_List.OnCertificatesPreviewClosed (Sender: TObject);
var
  aDataModule : IEDUEnrolmentDataModule;
begin
  assert (Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ));

  if MessageDlg(rsMarkEnrollmentsAsCertificated, mtInformation, [mbYes, mbNo], 0) = mrYes then
  begin
    aDataModule.MarkAsCertificatesPrinted(FEnrollmentsList);
    Self.RefreshData;
  end
end;

procedure TfrmEnrolment_List.FVBFFCListViewActivate(Sender: TObject);
begin
  inherited;
  // Refresh
  DataSet.Refresh;

end;


procedure TfrmEnrolment_List.acMoveToOtherSessionExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
  aDataModule     : IEDUEnrolmentDataModule;
  aDataSet        : TClientDataSet;
  lcv             : Integer;
  isOK            : boolean;
  lstID           : TStringList;
begin
  if ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aDataSet := TClientDataSet.Create( Self );

    try
      aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
      aDataController := TcxGridDBDataController( ActiveGridView.DataController );

      lstID := TStringList.Create;

      { Check if there are selected records }
      if ( aViewController. SelectedRecordCount <> 0 ) then
      begin
        // Allow the user to select a Session
        if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmSesSession', 'F_STATUS_ID IN ( 1, 4 ) AND F_SESSION_ID <> ' + srcMain.DataSet.FieldByName( 'F_SESSION_ID' ).AsString ,
                                                 aDataSet, False ) = mrOk ) then
        begin
          { Loop over each selected record in the grid }
          for lcv := 0 to Pred( aViewController.SelectedRecordCount ) do
          begin
            srcMain.DataSet.Bookmark := aDataController.GetSelectedBookmark( lcv );

            // Call the Procedure
            lstID.Add(srcMain.DataSet.FieldByName( 'F_ENROL_ID' ).AsString);
            aDataModule.MoveToSession( srcMain.DataSet.FieldByName( 'F_ENROL_ID' ).AsInteger,
                                       aDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger );

          end;

          RefreshData;

          isOK := True;
          for lcv := 0 to (lstID.Count - 1) do
          begin
            if srcMain.DataSet.Locate('F_ENROL_ID', lstID.Strings[lcv], []) then
            begin
              isOK := False;
              break;
            end;
          end;
          if not(isOK) then
            MessageDlg(rsMoveToSessionNotComplete, mtWarning, [mbOk], 0);
        end;
      end;
    finally
      FreeAndNil( aDataSet );
      FreeAndNil( lstID );
    end;
  end;
end;



procedure TfrmEnrolment_List.acSetGeattesteerdExecute(Sender: TObject);
begin
  inherited;
  LoopThroughSelectedRecords (SetGeattesteerd);
end;

procedure TfrmEnrolment_List.SetGeattesteerd;
var oldTag: Integer;
begin
  with srcMain.Dataset do
  begin
    if FieldByName('F_STATUS_ID').AsInteger = 12 then //gefactureerd
    begin
      oldTag := Tag;
      try
        Tag := 1;
        Edit;
        FieldByName('F_STATUS_ID').AsInteger := 10; //ingeschreven
        FieldByName ('F_STATUS_NAME').AsString := dtmEDUMainClient.GetNameForState( 10);
        Post;
      finally
        Tag := oldTag;
      end;
    end;
  end;
end;

{*****************************************************************************
  @Name       TfrmEnrolment_List.DeleteRecord
  @author     Ivan Van den Bossche (17/09/2007)
  @param      None
  @return     None
  @Exception  None
  @See        None
  @Descr      Functions deletes all selected records from Grid & database
******************************************************************************}
procedure TfrmEnrolment_List.DeleteRecord;
var
    oldTag: Integer;
    statusid: Integer;
begin
    assert (self.srcMain.DataSet <> nil);

    oldTag := srcMain.Dataset.Tag;
    try
      srcMain.DataSet.Tag := 1;
      statusid := srcMain.DataSet.FieldByName('F_STATUS_ID').AsInteger;

      if (statusid = 10) or (statusid = 12) then // Geattesteerd/Gefaktureerd
        MessageDlg(Format(rsUnableToDeleteEnrolment,
                    [srcMain.DataSet.FieldByName('F_FIRSTNAME').AsString,
                      srcMain.DataSet.FieldByName('F_LASTNAME').AsString]), mtError, [mbOk], 0)
      else
        srcMain.DataSet.Delete;
    finally
      srcMain.Dataset.Tag := oldTag;
    end;

end;

procedure TfrmEnrolment_List.DeleteSelectedRecords;
var
  oldcursor: TCursor;
begin
  if MessageDlg(rsConfirmDeletionEnrolmentRecords, mtConfirmation, [mbYes,mbNo], 0)=mrYes then
  begin
    oldcursor := Screen.Cursor;
    try
      Screen.Cursor := crSQLWait;
      LoopThroughSelectedRecords(DeleteRecord);
      cxgrdtblvList.DataController.ClearSelection;
    finally
      Screen.Cursor := oldcursor;
    end;
  end;

end;


//Verplaatsen naar andere sessie alleen toegelaten als de enrollment nog niet gefactureerd
//werd: anders kan de enrollment mogelijk dubbel worden gefactureerd
procedure TfrmEnrolment_List.acMoveToOtherSessionUpdate(Sender: TObject);
begin
  inherited;
  if srcMain.DataSet <> nil then
    TAction(Sender).Enabled := srcMain.DataSet.FieldByName('F_STATUS_ID').AsInteger <> 12;
end;

procedure TfrmEnrolment_List.cxgrdtblvListColumn1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUEnrolmentDataModule;
begin
  inherited;

  if ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      1:
        aDataModule.RemovePartnerWinter ( srcMain.DataSet);
      0 :
        aDataModule.SelectPartnerWinter( srcMain.DataSet );
  end;
end;
begin
if (srcMain.DataSet.State in dsEditModes) then
 begin
       srcMain.DataSet.Tag := 1;
       srcMain.DataSet.Post;
 end;
end;
end;
procedure TfrmEnrolment_List.cxgrdtblvListEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
var
  organisationTypeId: Integer;
begin
  inherited;
    //
    if AItem.Index = cxgrdtblvListWinterPartner.Index then
    begin
      // Do not allow to change partner winter when organisation is school.
      organisationTypeId := Integer(cxgrdtblvList.DataController.Values[AItem.FocusedCellViewInfo.GridRecord.RecordIndex,
        cxgrdtblvListF_ORGTYPE_ID.Index]);

      AAllow := organisationTypeId <> 4;
    end;
end;

end.
