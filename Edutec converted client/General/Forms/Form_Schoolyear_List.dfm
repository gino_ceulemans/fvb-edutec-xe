inherited frmSchoolyear_List: TfrmSchoolyear_List
  Caption = 'Schooljaren'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_SCHOOLYEAR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SCHOOLYEAR_ID'
          Width = 141
        end
        object cxgrdtblvListF_SCHOOLYEAR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SCHOOLYEAR'
          Width = 200
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
          RepositoryItem = dtmEDUMainClient.cxeriDate
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          RepositoryItem = dtmEDUMainClient.cxeriDate
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Width = 80
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmSchoolyear.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
