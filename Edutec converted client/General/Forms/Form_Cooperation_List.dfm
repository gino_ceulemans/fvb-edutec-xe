inherited frmCooperation_List: TfrmCooperation_List
  Caption = 'In samenwerking met'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_COOPERATION_ID: TcxGridDBColumn
          Caption = 'I.s.m. ( ID )'
          DataBinding.FieldName = 'F_COOPERATION_ID'
          Width = 103
        end
        object cxgrdtblvListF_COOPERATION_NAME: TcxGridDBColumn
          Caption = 'I.s.m.'
          DataBinding.FieldName = 'F_COOPERATION_NAME'
          Width = 300
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          Caption = 'I.s.m. ( NL )'
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
          Width = 200
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          Caption = 'I.s.m.( FR )'
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
          Width = 200
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmCooperation.cdsSearchCriteria
  end
end
