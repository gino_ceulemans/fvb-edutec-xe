{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_PROVINCE record.

  @Name       Form_Province_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Province_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxButtonEdit, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, Unit_PPWFrameWorkInterfaces, Menus, cxSplitter;

type
  TfrmProvince_Record = class(TEDURecordView)
    cxlblF_PROVINCE_ID1: TFVBFFCLabel;
    cxdblblF_PROVINCE_ID: TFVBFFCDBLabel;
    cxlblF_PROVINCE_NAME1: TFVBFFCLabel;
    cxdblblF_PROVINCE_NAME: TFVBFFCDBLabel;
    cxlblF_PROVINCE_ID2: TFVBFFCLabel;
    cxdbseF_PROVINCE_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    cxlblF_COUNTRY_PART_NAME1: TFVBFFCLabel;
    cxdbbeF_COUNTRY_PART_NAME: TFVBFFCDBButtonEdit;
    acShowPostalCodes: TAction;
    dxnbiPostCodes: TdxNavBarItem;
    procedure cxdbbeF_COUNTRY_PART_NAME1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure acShowPostalCodesExecute(Sender: TObject);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmProvince_Record: TfrmProvince_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Province, Data_EDUMainClient, Unit_FVBFFCInterfaces, Unit_PPWFrameWorkClasses,
  unit_EdutecInterfaces;

{ TfrmProvince_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmProvince_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmProvince_Record.GetDetailName: String;
begin
  Result := cxdblblF_PROVINCE_NAME.Caption;
end;

{*****************************************************************************
  This method will be executed when the user clicks one of the Buttons in the
  ButtonEdit.  Depending on the clicked button it will execute the ShowXXX or
  SelectXXX methods.

  @Name       TfrmProvince_Record.cxdbbeF_COUNTRY_PART_NAME1PropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProvince_Record.cxdbbeF_COUNTRY_PART_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUProvinceDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUProvinceDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowCountryPart( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectCountryPart( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmProvince_Record.acShowPostalCodesExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmPostalcode', pnlRecordDetail );
end;

{*****************************************************************************
  This method will be executed when the form is Initalised and will be used
  to set up some things.

  @Name       TfrmProvince_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      aFrameWorkDataModule   The DataModule associated to the Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmProvince_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUCountryPartDataModule ) ) then
    begin
      cxdbbeF_COUNTRY_PART_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbteF_NAME_NL );
//      aFrameWorkDataModule.RecordDataset.FieldByName( 'F_COUNTRY_PART_NAME' ).ReadOnly := True;
//      cxdbbeF_COUNTRY_PART_NAME.Properties.ReadOnly := True;
    end;
  end;
end;

end.