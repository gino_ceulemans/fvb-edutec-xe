{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <T_PE_PERSON> records.


  @Name       Form_Person_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/04/2008   Ivan Van den Bossche Added cxdbbeF_DIPLOMA_IDPropertiesButtonClick
                                    (Mantis 2196).
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Person_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxCurrencyEdit, cxDBEdit,
  cxDropDownEdit, cxImageComboBox, cxCalendar, cxCheckBox, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, cxMemo, Menus, cxGraphics, cxSplitter, cxGroupBox,
  cxHyperLinkEdit;

type
  TfrmPerson_Record = class(TEDURecordView)
    cxgbGeneralInfo: TFVBFFCGroupBox;
    cxgbAddress: TFVBFFCGroupBox;
    cxlblF_STREET1: TFVBFFCLabel;
    cxdbteF_STREET: TFVBFFCDBTextEdit;
    cxlblF_NUMBER1: TFVBFFCLabel;
    cxdbteF_NUMBER: TFVBFFCDBTextEdit;
    cxlblF_MAILBOX1: TFVBFFCLabel;
    cxdbteF_MAILBOX: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxdbbeF_POSTALCODE: TFVBFFCDBButtonEdit;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdbteF_CITY_NAME: TFVBFFCDBTextEdit;
    cxlblF_COUNTRY_NAME1: TFVBFFCLabel;
    cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit;
    cxlblF_PERSON_ID1: TFVBFFCLabel;
    cxdbseF_PERSON_ID: TFVBFFCDBSpinEdit;
    cxlblF_SOCSEC_NR1: TFVBFFCLabel;
    cxdbmeF_SOCSEC_NR: TFVBFFCDBMaskEdit;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdbteF_LASTNAME: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit;
    cxlblF_PHONE1: TFVBFFCLabel;
    cxdbteF_PHONE: TFVBFFCDBTextEdit;
    cxlblF_DATEBIRTH1: TFVBFFCLabel;
    cxdbdeF_DATEBIRTH: TFVBFFCDBDateEdit;
    cxlblF_GENDER_NAME1: TFVBFFCLabel;
    cxdbbeF_GENDER_NAME: TFVBFFCDBButtonEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_TITLE_NAME1: TFVBFFCLabel;
    cxdbbeF_TITLE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_ACC_NR1: TFVBFFCLabel;
    cxdbteF_ACC_NR: TFVBFFCDBTextEdit;
    cxgbAdditionalInfo: TFVBFFCGroupBox;
    cxlblF_FAX1: TFVBFFCLabel;
    cxdbteF_FAX: TFVBFFCDBTextEdit;
    cxlblF_GSM1: TFVBFFCLabel;
    cxdbteF_GSM: TFVBFFCDBTextEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    cxdbhleF_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlblF_MEDIUM_NAME1: TFVBFFCLabel;
    cxdbbePersoon: TFVBFFCDBButtonEdit;
    cxlblF_PERSON_ID: TFVBFFCLabel;
    cxdblblF_PERSON_ID: TFVBFFCDBLabel;
    cxlblF_LASTNAME: TFVBFFCLabel;
    cxdblblF_LASTNAME: TFVBFFCDBLabel;
    cxlblF_FIRSTNAME: TFVBFFCLabel;
    cxdblblF_FIRSTNAME: TFVBFFCDBLabel;
    acShowRoles: TAction;
    dxnbiRoles: TdxNavBarItem;
    acShowEnrolments: TAction;
    dxnbiEnrolments: TdxNavBarItem;
    acShowHistory: TAction;
    dxnbiShowHistory: TdxNavBarItem;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxdbseF_CONSTRUCT_ID: TFVBFFCDBSpinEdit;
    cxdbcbF_MANUALLY_ADDED: TFVBFFCDBCheckBox;
    cxgbEducation: TFVBFFCGroupBox;
    cxglblEducation: TFVBFFCLabel;
    cxdbbeF_DIPLOMA_ID: TFVBFFCDBButtonEdit;
    FVBFFCLabel4: TFVBFFCLabel;
    cxdbseF_SYNERGY_ID: TFVBFFCDBSpinEdit;
    cxdbteF_PLACE_OF_BIRTH: TFVBFFCDBTextEdit;
    FVBFFCLabel1: TFVBFFCLabel;
    procedure cxdbbeF_TITLE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_GENDER_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_POSTALCODEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_MEDIUM_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acShowRolesExecute(Sender: TObject);
    procedure acShowEnrolmentsExecute(Sender: TObject);
    procedure acShowHistoryExecute(Sender: TObject);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_DIPLOMA_IDPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Person, Data_EduMainClient, unit_EdutecInterfaces;

{ TfrmPerson_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmPerson_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPerson_Record.GetDetailName: String;
begin
{
  Result := cxdblblF_LASTNAME.Caption + ', ' +
            cxdblblF_FIRSTNAME.Caption;
}
  if (trim(cxdblblF_LASTNAME.Caption) = '') and (trim(cxdblblF_FIRSTNAME.Caption) = '') then
     Result := 'Nieuwe persoon'
  else if trim(cxdblblF_LASTNAME.Caption) = '' then
    Result := cxdblblF_FIRSTNAME.Caption
  else if trim(cxdblblF_FIRSTNAME.Caption) = '' then
    Result := cxdblblF_LASTNAME.Caption
  else
    Result := cxdblblF_LASTNAME.Caption + ', ' +
            cxdblblF_FIRSTNAME.Caption;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Title ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_TITLE_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_TITLE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowTitle( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectTitle( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Language ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectLanguage( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Gender ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_GENDER_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_GENDER_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowGender( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectGender( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the PostalCode ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_POSTALCODEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_POSTALCODEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowPostalCode( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectPostalCode( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Country ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_COUNTRY_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowCountry( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectCountry( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Medium ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_MEDIUM_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_MEDIUM_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowMedium( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectMedium( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Medium ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_DIPLOMA_IDPropertiesButtonClick
  @author     Ivan Van den Bossche
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPerson_Record.cxdbbeF_DIPLOMA_IDPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPersonDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowDiploma( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectDiploma( srcMain.DataSet );
      end;
    end;
  end;

end;

procedure TfrmPerson_Record.acShowRolesExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmRole', pnlRecordDetail );
end;

procedure TfrmPerson_Record.acShowEnrolmentsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmEnrolment', pnlRecordDetail );
end;

procedure TfrmPerson_Record.acShowHistoryExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmLogHistory', pnlRecordDetail );
end;

procedure TfrmPerson_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcbF_MANUALLY_ADDED );
end;



end.