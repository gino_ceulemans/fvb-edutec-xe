inherited frmSesWizard_List: TfrmSesWizard_List
  Left = 63
  Top = 182
  Width = 913
  Caption = 'Sessie wizard'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 905
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 689
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 801
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 897
    inherited cxgrdList: TcxGrid
      Width = 897
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_SESWIZARD_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESWIZARD_ID'
        end
        object cxgrdtblvListF_SPID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SPID'
        end
        object cxgrdtblvListF_EXTERN_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_EXTERN_NR'
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
        end
        object cxgrdtblvListF_COMPANY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COMPANY_NAME'
        end
        object cxgrdtblvListF_PERSON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_ID'
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
        end
        object cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_ID'
        end
        object cxgrdtblvListF_PROGRAM_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_NAME'
        end
        object cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_NAME'
        end
        object cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_CODE'
        end
        object cxgrdtblvListF_SESSION_TRANSPORT_INS: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_TRANSPORT_INS'
        end
        object cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_START_DATE'
        end
        object cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_END_DATE'
        end
        object cxgrdtblvListF_SESSION_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_COMMENT'
        end
        object cxgrdtblvListF_SESSION_START_HOUR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_START_HOUR'
        end
        object cxgrdtblvListF_ENROL_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_ID'
        end
        object cxgrdtblvListF_ENROL_EVALUATION: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_EVALUATION'
        end
        object cxgrdtblvListF_ENROL_CERTIFICATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_CERTIFICATE'
        end
        object cxgrdtblvListF_ENROL_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_COMMENT'
        end
        object cxgrdtblvListF_ENROL_INVOICED: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_INVOICED'
        end
        object cxgrdtblvListF_ENROL_LAST_MINUTE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_LAST_MINUTE'
        end
        object cxgrdtblvListF_ENROL_HOURS: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_HOURS'
        end
        object cxgrdtblvListF_SES_CREATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SES_CREATE'
        end
        object cxgrdtblvListF_SES_UPDATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SES_UPDATE'
        end
        object cxgrdtblvListF_ENR_CREATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENR_CREATE'
        end
        object cxgrdtblvListF_ENR_UPDATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENR_UPDATE'
        end
        object cxgrdtblvListF_FORECAST_COMMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_FORECAST_COMMENT'
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Width = 897
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Width = 897
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 897
      inherited pnlSearchCriteriaButtons: TPanel
        Width = 895
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 727
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 815
        end
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 905
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 905
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 901
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
