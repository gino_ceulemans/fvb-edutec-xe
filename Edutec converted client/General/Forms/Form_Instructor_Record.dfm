inherited frmInstructor_Record: TfrmInstructor_Record
  Left = 221
  Top = 76
  Width = 1024
  Height = 657
  ActiveControl = cxdbteF_LASTNAME1
  Caption = 'Lesgever'
  Constraints.MinHeight = 584
  Constraints.MinWidth = 1024
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 600
    Width = 1016
    Height = 30
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 680
      Height = 30
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 1016
    Height = 600
    inherited pnlRecord: TFVBFFCPanel
      Width = 854
      Height = 600
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 854
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 854
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 854
        object cxlblF_INSTRUCTOR_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmInstructor_Record.F_INSTRUCTOR_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Lesgever ( ID )'
          FocusControl = cxdblblF_INSTRUCTOR_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_INSTRUCTOR_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmInstructor_Record.F_INSTRUCTOR_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_INSTRUCTOR_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 89
        end
        object cxlblF_LASTNAME1: TFVBFFCLabel
          Left = 112
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Naam'
          FocusControl = cxdblblF_LASTNAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_LASTNAME: TFVBFFCDBLabel
          Left = 112
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_LASTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 200
        end
        object cxlblF_FIRSTNAME1: TFVBFFCLabel
          Left = 328
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Voornaam'
          FocusControl = cxdblblF_FIRSTNAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_FIRSTNAME: TFVBFFCDBLabel
          Left = 328
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_FIRSTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 200
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 854
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 595
        Width = 854
        Height = 5
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 854
        Height = 525
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 854
        end
        inherited sbxMain: TScrollBox
          Width = 854
          Height = 500
          object cxlblF_INSTRUCTOR_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Lesgever ( ID )'
            FocusControl = cxdbseF_INSTRUCTOR_ID1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_INSTRUCTOR_ID1: TFVBFFCDBSpinEdit
            Left = 136
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_INSTRUCTOR_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 121
          end
          object cxlblF_TITLE_NAME1: TFVBFFCLabel
            Left = 344
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_TITLE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Titel'
            FocusControl = cxdbbeF_TITLE_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_TITLE_NAME1: TFVBFFCDBButtonEdit
            Left = 448
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_TITLE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_TITLE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_TITLE_NAME1PropertiesButtonClick
            TabOrder = 3
            Width = 200
          end
          object cxlblF_LASTNAME2: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam'
            FocusControl = cxdbteF_LASTNAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_LASTNAME1: TFVBFFCDBTextEdit
            Left = 136
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
            DataBinding.DataField = 'F_LASTNAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 5
            Width = 200
          end
          object cxlblF_FIRSTNAME2: TFVBFFCLabel
            Left = 344
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Voornaam'
            FocusControl = cxdbteF_FIRSTNAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit
            Left = 448
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
            DataBinding.DataField = 'F_FIRSTNAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 7
            Width = 200
          end
          object cxlblF_STREET1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_STREET'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Straat'
            FocusControl = cxdbteF_STREET1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_STREET1: TFVBFFCDBTextEdit
            Left = 136
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_STREET'
            DataBinding.DataField = 'F_STREET'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 9
            Width = 200
          end
          object cxlblF_NUMBER1: TFVBFFCLabel
            Left = 344
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_NUMBER'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Nummer'
            FocusControl = cxdbteF_NUMBER1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NUMBER1: TFVBFFCDBTextEdit
            Left = 448
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_NUMBER'
            DataBinding.DataField = 'F_NUMBER'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 11
            Width = 121
          end
          object cxlblF_MAILBOX1: TFVBFFCLabel
            Left = 656
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_MAILBOX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bus'
            FocusControl = cxdbteF_MAILBOX1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_MAILBOX1: TFVBFFCDBTextEdit
            Left = 696
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_MAILBOX'
            DataBinding.DataField = 'F_MAILBOX'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 13
            Width = 121
          end
          object cxlblF_POSTALCODE1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Postcode'
            FocusControl = cxdbbeF_POSTALCODE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_POSTALCODE1: TFVBFFCDBButtonEdit
            Left = 136
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_POSTALCODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_POSTALCODE1PropertiesButtonClick
            TabOrder = 15
            Width = 200
          end
          object cxlblF_CITY_NAME1: TFVBFFCLabel
            Left = 344
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_CITY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gemeente'
            FocusControl = cxdbteF_CITY_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CITY_NAME1: TFVBFFCDBTextEdit
            Left = 448
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_CITY_NAME'
            DataBinding.DataField = 'F_CITY_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 16
            Width = 200
          end
          object cxlblF_COUNTRY_NAME1: TFVBFFCLabel
            Left = 656
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_COUNTRY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land'
            FocusControl = cxdbteF_COUNTRY_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_COUNTRY_NAME1: TFVBFFCDBTextEdit
            Left = 696
            Top = 112
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_COUNTRY_NAME'
            DataBinding.DataField = 'F_COUNTRY_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 20
            Visible = False
            Width = 155
          end
          object cxlblF_PHONE1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_PHONE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Telefoon'
            FocusControl = cxdbteF_PHONE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_PHONE1: TFVBFFCDBTextEdit
            Left = 136
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_PHONE'
            DataBinding.DataField = 'F_PHONE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 22
            Width = 121
          end
          object cxlblF_FAX1: TFVBFFCLabel
            Left = 344
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_FAX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Fax'
            FocusControl = cxdbteF_FAX1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FAX1: TFVBFFCDBTextEdit
            Left = 448
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_FAX'
            DataBinding.DataField = 'F_FAX'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 24
            Width = 121
          end
          object cxlblF_GSM1: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_GSM'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'GSM'
            FocusControl = cxdbteF_GSM1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_GSM1: TFVBFFCDBTextEdit
            Left = 136
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_GSM'
            DataBinding.DataField = 'F_GSM'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 26
            Width = 121
          end
          object cxlblF_EMAIL1: TFVBFFCLabel
            Left = 344
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'EMail'
            FocusControl = cxdbteF_EMAIL1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbteF_EMAIL1: TFVBFFCDBHyperLinkEdit
            Left = 448
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriEMail
            DataBinding.DataField = 'F_EMAIL'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Prefix = 'mailto:'
            TabOrder = 28
            Visible = False
            Width = 201
          end
          object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Taal'
            FocusControl = cxdbbeF_LANGUAGE_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_LANGUAGE_NAME1: TFVBFFCDBButtonEdit
            Left = 136
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_LANGUAGE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
            TabOrder = 30
            Width = 200
          end
          object cxlblF_GENDER_NAME1: TFVBFFCLabel
            Left = 344
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_GENDER_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Geslacht'
            FocusControl = cxdbbeF_GENDER_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_GENDER_NAME1: TFVBFFCDBButtonEdit
            Left = 448
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_GENDER_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_GENDER_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_GENDER_NAME1PropertiesButtonClick
            TabOrder = 32
            Width = 200
          end
          object cxlblF_MEDIUM_NAME1: TFVBFFCLabel
            Left = 8
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_MEDIUM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Medium'
            FocusControl = cxdbbeF_MEDIUM_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_MEDIUM_NAME1: TFVBFFCDBButtonEdit
            Left = 136
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_MEDIUM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_MEDIUM_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_MEDIUM_NAME1PropertiesButtonClick
            TabOrder = 34
            Width = 200
          end
          object cxlblF_ACC_NR1: TFVBFFCLabel
            Left = 344
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_ACC_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rekening Nr'
            FocusControl = F_ACC_NR1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object F_ACC_NR1: TFVBFFCDBMaskEdit
            Left = 448
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_ACC_NR'
            DataBinding.DataField = 'F_ACC_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 36
            Width = 137
          end
          object cxlblF_FOREIGN_ACC_NR1: TFVBFFCLabel
            Left = 8
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_FOREIGN_ACC_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Buitenlands Rek. Nr.'
            FocusControl = cxdbteF_FOREIGN_ACC_NR1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FOREIGN_ACC_NR1: TFVBFFCDBTextEdit
            Left = 136
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_FOREIGN_ACC_NR'
            DataBinding.DataField = 'F_FOREIGN_ACC_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 39
            Width = 137
          end
          object cxlblF_SOCSEC_NR1: TFVBFFCLabel
            Left = 344
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_SOCSEC_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rijksregister Nr.'
            FocusControl = F_SOCSEC_NR1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object F_SOCSEC_NR1: TFVBFFCDBMaskEdit
            Left = 448
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_SOCSEC_NR'
            DataBinding.DataField = 'F_SOCSEC_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 41
            Width = 137
          end
          object cxlblF_DATEBIRTH1: TFVBFFCLabel
            Left = 600
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_DATEBIRTH'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Geboortedatum'
            FocusControl = cxdbdeF_DATEBIRTH1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_DATEBIRTH1: TFVBFFCDBDateEdit
            Left = 696
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_DATEBIRTH'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_DATEBIRTH'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 43
            Width = 155
          end
          object cxlblF_ACTIVE1: TFVBFFCLabel
            Left = 656
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_ACTIVE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Actief'
            FocusControl = csdbcbF_ACTIVE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object csdbcbF_ACTIVE1: TFVBFFCDBCheckBox
            Left = 696
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_ACTIVE'
            Caption = 'csdbcbF_ACTIVE1'
            DataBinding.DataField = 'F_ACTIVE'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            Properties.NullStyle = nssInactive
            Properties.ReadOnly = False
            TabOrder = 45
            Width = 21
          end
          object cxlblF_END_DATE1: TFVBFFCLabel
            Left = 8
            Top = 224
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Eind Datum'
            FocusControl = cxdbdeF_END_DATE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_END_DATE1: TFVBFFCDBDateEdit
            Left = 136
            Top = 224
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_END_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 47
            Width = 137
          end
          object cxlblF_DIPLOMA_NAME1: TFVBFFCLabel
            Left = 8
            Top = 248
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_DIPLOMA_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Diploma'
            FocusControl = cxdbbeF_DIPLOMA_NAME1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_DIPLOMA_NAME1: TFVBFFCDBButtonEdit
            Left = 136
            Top = 248
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_DIPLOMA_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_DIPLOMA_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_DIPLOMA_NAME1PropertiesButtonClick
            TabOrder = 49
            Width = 200
          end
          object cxlblF_CODE1: TFVBFFCLabel
            Left = 8
            Top = 272
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Code'
            FocusControl = cxdbteF_CODE1
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CODE1: TFVBFFCDBTextEdit
            Left = 136
            Top = 272
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_CODE'
            DataBinding.DataField = 'F_CODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 51
            Width = 121
          end
          object cxgbFVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 448
            Top = 232
            Caption = 'Taal waarin les kan gegeven worden'
            ParentColor = False
            ParentFont = False
            TabOrder = 52
            Transparent = True
            Height = 73
            Width = 401
            object cxlblF_DUTCH1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_DUTCH'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Nederlands'
              FocusControl = csdbcbF_DUTCH1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Transparent = True
            end
            object csdbcbF_DUTCH1: TFVBFFCDBCheckBox
              Left = 80
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_DUTCH'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'csdbcbF_DUTCH1'
              DataBinding.DataField = 'F_DUTCH'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 1
              Width = 21
            end
            object cxlblF_FRENCH1: TFVBFFCLabel
              Left = 104
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_FRENCH'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Frans'
              FocusControl = csdbcbF_FRENCH1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Transparent = True
            end
            object csdbcbF_FRENCH1: TFVBFFCDBCheckBox
              Left = 176
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_FRENCH'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'csdbcbF_FRENCH1'
              DataBinding.DataField = 'F_FRENCH'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 3
              Width = 21
            end
            object cxlblF_ENGLISH1: TFVBFFCLabel
              Left = 208
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_ENGLISH'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Engels'
              FocusControl = csdbcbF_ENGLISH1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Transparent = True
            end
            object csdbcbF_ENGLISH1: TFVBFFCDBCheckBox
              Left = 280
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_ENGLISH'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'csdbcbF_ENGLISH1'
              DataBinding.DataField = 'F_ENGLISH'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 5
              Width = 21
            end
            object cxlblF_GERMAN1: TFVBFFCLabel
              Left = 304
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_GERMAN'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Duits'
              FocusControl = csdbcbF_GERMAN1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Transparent = True
            end
            object csdbcbF_GERMAN1: TFVBFFCDBCheckBox
              Left = 376
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_GERMAN'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'csdbcbF_GERMAN1'
              DataBinding.DataField = 'F_GERMAN'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 7
              Width = 21
            end
            object cxlblF_OTHER_LANGUAGE1: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_OTHER_LANGUAGE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Andere'
              FocusControl = cxdbteF_OTHER_LANGUAGE1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Transparent = True
            end
            object cxdbteF_OTHER_LANGUAGE1: TFVBFFCDBTextEdit
              Left = 80
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmInstructor_Record.F_OTHER_LANGUAGE'
              DataBinding.DataField = 'F_OTHER_LANGUAGE'
              DataBinding.DataSource = srcMain
              TabOrder = 9
              Width = 313
            end
          end
          object cxlblF_COMMENT1: TFVBFFCLabel
            Left = 8
            Top = 304
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Commentaar'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit
            Left = 696
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_COUNTRY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_COUNTRY_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COUNTRY_NAMEPropertiesButtonClick
            TabOrder = 17
            Width = 156
          end
          object cxlblF_BTW: TFVBFFCLabel
            Left = 600
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_BTW'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'BTW-nummer'
            FocusControl = cxdbmeF_BTW
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmeF_BTW: TFVBFFCDBMaskEdit
            Left = 696
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_BTW'
            DataBinding.DataField = 'F_BTW'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 37
            Width = 155
          end
          object cxDBmmo_FCOMMENT: TcxDBMemo
            Left = 136
            Top = 304
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_COMMENT'
            DataBinding.DataField = 'F_COMMENT'
            DataBinding.DataSource = srcMain
            TabOrder = 54
            Height = 25
            Width = 713
          end
          object FVBFFCLabel1: TFVBFFCLabel
            Left = 8
            Top = 338
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Organisatie'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_ORGANISATION_NAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 336
            HelpType = htKeyword
            HelpKeyword = 'frmInstructor_Record.F_MEDIUM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
            DataBinding.DataField = 'F_ORGANISATION_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbteF_ORGANISATION_NAMEPropertiesButtonClick
            TabOrder = 57
            OnKeyDown = cxdbteF_ORGANISATION_NAMEKeyDown
            Width = 200
          end
          object FVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 8
            Top = 368
            Caption = 'Contactgegevens bevestigingsrapporten'
            ParentColor = False
            ParentFont = False
            TabOrder = 58
            Transparent = True
            Height = 81
            Width = 769
            object cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 1'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit
              Left = 120
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT1'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 273
            end
            object F_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT1_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 2
              Width = 300
            end
            object cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT2_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 4
              Width = 300
            end
            object cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit
              Left = 120
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT2'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 273
            end
            object cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 2'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 1012
      Height = 600
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 600
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiPrograms
          end
          item
            Item = dxnbiDocCenter
          end>
      end
      object dxnbiPrograms: TdxNavBarItem
        Action = acShowPrograms
      end
      object dxnbiDocCenter: TdxNavBarItem
        Action = acShowDocCenter
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 600
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmInstructor.cdsRecord
    Left = 76
    Top = 240
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowPrograms: TAction
      Caption = 'Programmas'
      OnExecute = acShowProgramsExecute
    end
    object acShowDocCenter: TAction
      Caption = 'Sessies'
      OnExecute = acShowDocCenterExecute
    end
  end
end
