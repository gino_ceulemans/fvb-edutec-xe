inherited frmRole_List: TfrmRole_List
  Left = 188
  Top = 238
  Width = 904
  ActiveControl = cxdbteF_LASTNAME1
  Caption = 'Functies'
  Constraints.MinWidth = 904
  OnCreate = FVBFFCListViewCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 896
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 680
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 792
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 888
    inherited cxgrdList: TcxGrid
      Top = 174
      Width = 888
      Height = 215
      inherited cxgrdtblvList: TcxGridDBTableView
        OptionsView.GroupByBox = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_ROLEGROUP_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROLEGROUP_NAME'
          GroupIndex = 0
          Width = 200
        end
        object cxgrdtblvListF_ROLE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROLE_ID'
          Visible = False
          Width = 109
        end
        object cxgrdtblvListF_PERSON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_ID'
          Visible = False
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
          Width = 200
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
          Width = 200
        end
        object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SOCSEC_NR'
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Visible = False
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 200
        end
        object cxgrdtblvListF_RSZ_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_RSZ_NR'
        end
        object cxgrdtblvListF_ROLEGROUP_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROLEGROUP_ID'
          Visible = False
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Width = 60
        end
        object cxgrdtblvListF_PRINCIPAL: TcxGridDBColumn
          DataBinding.FieldName = 'F_PRINCIPAL'
          Width = 68
        end
        object cxgrdtblvListF_SCHOOLYEAR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SCHOOLYEAR_ID'
          Visible = False
        end
        object cxgrdtblvListF_SCHOOLYEAR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SCHOOLYEAR'
          Width = 200
        end
        object cxgrdtblvListF_REMARK: TcxGridDBColumn
          DataBinding.FieldName = 'F_REMARK'
          Width = 300
        end
        object cxgrdtblvListF_PHONE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE'
          Width = 80
        end
        object cxgrdtblvListF_FAX: TcxGridDBColumn
          DataBinding.FieldName = 'F_FAX'
          Width = 80
        end
        object cxgrdtblvListF_GSM: TcxGridDBColumn
          DataBinding.FieldName = 'F_GSM'
          Width = 80
        end
        object cxgrdtblvListF_EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'F_EMAIL'
          Width = 80
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 149
      Width = 888
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 145
      Width = 888
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 888
      Height = 145
      ExpandedHeight = 145
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 114
        Width = 886
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 718
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 806
        end
      end
      object cxlblF_LASTNAME2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam'
        FocusControl = cxdbteF_LASTNAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_LASTNAME1: TFVBFFCDBTextEdit
        Left = 80
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
        DataBinding.DataField = 'F_LASTNAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 2
        Width = 216
      end
      object cxlblF_FIRSTNAME2: TFVBFFCLabel
        Left = 304
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Voornaam'
        FocusControl = cxdbteF_FIRSTNAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit
        Left = 374
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        DataBinding.DataField = 'F_FIRSTNAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 4
        Width = 195
      end
      object cxlblSocSecNr: TFVBFFCLabel
        Left = 584
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Rijksr. Nr'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbmeSocSecNr: TFVBFFCDBMaskEdit
        Left = 656
        Top = 32
        DataBinding.DataField = 'F_SOCSEC_NR'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 6
        Width = 216
      end
      object cxlblF_NAME2: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam Org.'
        FocusControl = cxdbteF_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME: TFVBFFCDBTextEdit
        Left = 80
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
        DataBinding.DataField = 'F_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 8
        Width = 489
      end
      object cxlblF_ROLEGROUP_NAME1: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmRole_Record.F_ROLEGROUP_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Rol'
        FocusControl = cxdbbeF_ROLEGROUP_NAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_ROLEGROUP_NAME1: TFVBFFCDBButtonEdit
        Left = 80
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmRole_Record.F_ROLEGROUP_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
        DataBinding.DataField = 'F_ROLEGROUP_NAME'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick
        TabOrder = 11
        Width = 489
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 584
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 656
        Top = 56
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Items = <>
        TabOrder = 9
        Width = 216
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 896
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 896
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 892
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
