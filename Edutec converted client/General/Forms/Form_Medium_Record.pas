{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_MEDIUM record.

  @Name       Form_Medium_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  15/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Medium_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDURecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmMedium_Record = class(TEDURecordView)
    cxlblF_MEDIUM_ID1: TFVBFFCLabel;
    cxdblblF_MEDIUM_ID: TFVBFFCDBLabel;
    cxlblF_MEDIUM_NAME1: TFVBFFCLabel;
    cxdblblF_MEDIUM_NAME: TFVBFFCDBLabel;
    cxlblF_MEDIUM_ID2: TFVBFFCLabel;
    cxdbseF_MEDIUM_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Medium, Data_EduMainClient;

{ TfrmMedium_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmMedium_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmMedium_Record.GetDetailName: String;
begin
  Result := cxdblblF_MEDIUM_NAME.Caption;
end;

end.