inherited frmPerson_Record: TfrmPerson_Record
  Left = 365
  Top = 178
  Width = 967
  Height = 597
  ActiveControl = cxdbbeF_TITLE_NAME
  Caption = 'Persoon'
  Constraints.MinHeight = 480
  Constraints.MinWidth = 812
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 538
    Width = 959
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 623
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 959
    Height = 538
    inherited pnlRecord: TFVBFFCPanel
      Width = 797
      Height = 538
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 797
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 797
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 797
        object cxlblF_PERSON_ID: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmPerson_Record.F_PERSON_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Persoon ( ID )'
          FocusControl = cxdblblF_PERSON_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PERSON_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmPerson_Record.F_PERSON_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PERSON_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 89
        end
        object cxlblF_LASTNAME: TFVBFFCLabel
          Left = 112
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmPerson_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Naam'
          FocusControl = cxdblblF_LASTNAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_LASTNAME: TFVBFFCDBLabel
          Left = 112
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmPerson_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_LASTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 200
        end
        object cxlblF_FIRSTNAME: TFVBFFCLabel
          Left = 328
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmPerson_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Voornaam'
          FocusControl = cxdblblF_FIRSTNAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_FIRSTNAME: TFVBFFCDBLabel
          Left = 328
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmPerson_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_FIRSTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 200
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 797
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 534
        Width = 797
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 797
        Height = 464
        inherited sbxMain: TScrollBox [0]
          Width = 797
          Height = 439
          object cxgbGeneralInfo: TFVBFFCGroupBox
            Left = 8
            Top = 8
            Caption = 'Algemeen'
            ParentColor = False
            TabOrder = 0
            Transparent = True
            Height = 329
            Width = 385
            object cxlblF_PERSON_ID1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_PERSON_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Persoon ( ID )'
              FocusControl = cxdbseF_PERSON_ID
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_PERSON_ID: TFVBFFCDBSpinEdit
              Left = 120
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_PERSON_ID'
              RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
              DataBinding.DataField = 'F_PERSON_ID'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 121
            end
            object cxlblF_SOCSEC_NR1: TFVBFFCLabel
              Left = 8
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_SOCSEC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Rijksr. Nr'
              FocusControl = cxdbmeF_SOCSEC_NR
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbmeF_SOCSEC_NR: TFVBFFCDBMaskEdit
              Left = 120
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_SOCSEC_NR'
              DataBinding.DataField = 'F_SOCSEC_NR'
              DataBinding.DataSource = srcMain
              TabOrder = 13
              Width = 256
            end
            object cxlblF_LASTNAME1: TFVBFFCLabel
              Left = 8
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_LASTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Familienaam'
              FocusControl = cxdbteF_LASTNAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_LASTNAME: TFVBFFCDBTextEdit
              Left = 120
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_LASTNAME'
              DataBinding.DataField = 'F_LASTNAME'
              DataBinding.DataSource = srcMain
              TabOrder = 4
              Width = 256
            end
            object cxlblF_FIRSTNAME1: TFVBFFCLabel
              Left = 8
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_FIRSTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Voornaam'
              FocusControl = cxdbteF_FIRSTNAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit
              Left = 120
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_FIRSTNAME'
              DataBinding.DataField = 'F_FIRSTNAME'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 256
            end
            object cxlblF_PHONE1: TFVBFFCLabel
              Left = 8
              Top = 264
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_PHONE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Telefoon'
              FocusControl = cxdbteF_PHONE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_PHONE: TFVBFFCDBTextEdit
              Left = 120
              Top = 264
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_PHONE'
              DataBinding.DataField = 'F_PHONE'
              DataBinding.DataSource = srcMain
              TabOrder = 19
              Width = 256
            end
            object cxlblF_DATEBIRTH1: TFVBFFCLabel
              Left = 8
              Top = 168
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_DATEBIRTH'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Geboortedatum'
              FocusControl = cxdbdeF_DATEBIRTH
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_DATEBIRTH: TFVBFFCDBDateEdit
              Left = 120
              Top = 168
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_DATEBIRTH'
              RepositoryItem = dtmEDUMainClient.cxeriDate
              DataBinding.DataField = 'F_DATEBIRTH'
              DataBinding.DataSource = srcMain
              TabOrder = 8
              Width = 137
            end
            object cxlblF_GENDER_NAME1: TFVBFFCLabel
              Left = 8
              Top = 144
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_GENDER_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Geslacht'
              FocusControl = cxdbbeF_GENDER_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_GENDER_NAME: TFVBFFCDBButtonEdit
              Left = 120
              Top = 144
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_GENDER_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_GENDER_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_GENDER_NAMEPropertiesButtonClick
              TabOrder = 7
              Width = 256
            end
            object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
              Left = 8
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_LANGUAGE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Taal'
              FocusControl = cxdbbeF_LANGUAGE_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
              Left = 120
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_LANGUAGE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_LANGUAGE_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
              TabOrder = 6
              Width = 256
            end
            object cxlblF_TITLE_NAME1: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_TITLE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Titel'
              FocusControl = cxdbbeF_TITLE_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_TITLE_NAME: TFVBFFCDBButtonEdit
              Left = 120
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_TITLE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_TITLE_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_TITLE_NAMEPropertiesButtonClick
              TabOrder = 3
              Width = 256
            end
            object cxlblF_ACC_NR1: TFVBFFCLabel
              Left = 8
              Top = 216
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_ACC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Rek. Nr'
              FocusControl = cxdbteF_ACC_NR
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_ACC_NR: TFVBFFCDBTextEdit
              Left = 120
              Top = 216
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_ACC_NR'
              DataBinding.DataField = 'F_ACC_NR'
              DataBinding.DataSource = srcMain
              TabOrder = 9
              Width = 256
            end
            object cxlblFVBFFCLabel1: TFVBFFCLabel
              Left = 8
              Top = 288
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_PERSON_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Construct ( ID )'
              FocusControl = cxdbseF_CONSTRUCT_ID
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_CONSTRUCT_ID: TFVBFFCDBSpinEdit
              Left = 120
              Top = 288
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_CONSTRUCT_ID'
              DataBinding.DataField = 'F_CONSTRUCT_ID'
              DataBinding.DataSource = srcMain
              TabOrder = 21
              Width = 85
            end
            object cxdbcbF_MANUALLY_ADDED: TFVBFFCDBCheckBox
              Left = 248
              Top = 24
              HelpType = htKeyword
              TabStop = False
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'Zelf toegevoegd ?'
              DataBinding.DataField = 'F_MANUALLY_ADDED'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 22
              Transparent = True
              Width = 129
            end
            object FVBFFCLabel4: TFVBFFCLabel
              Left = 214
              Top = 288
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Synergy ID'
              FocusControl = cxdbseF_SYNERGY_ID
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_SYNERGY_ID: TFVBFFCDBSpinEdit
              Left = 291
              Top = 288
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_SYNERGY_ID'
              DataBinding.DataField = 'F_SYNERGY_ID'
              DataBinding.DataSource = srcMain
              TabOrder = 24
              Width = 85
            end
            object cxdbteF_PLACE_OF_BIRTH: TFVBFFCDBTextEdit
              Left = 120
              Top = 192
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_PLACE_OF_BIRTH'
              DataBinding.DataField = 'F_PLACE_OF_BIRTH'
              DataBinding.DataSource = srcMain
              TabOrder = 25
              Width = 256
            end
            object FVBFFCLabel1: TFVBFFCLabel
              Left = 8
              Top = 192
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_ACC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Geboorteplaats'
              FocusControl = cxdbteF_PLACE_OF_BIRTH
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
          end
          object cxgbAddress: TFVBFFCGroupBox
            Left = 397
            Top = 8
            Caption = 'Adres'
            ParentColor = False
            TabOrder = 1
            Transparent = True
            Height = 145
            Width = 385
            object cxlblF_STREET1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_STREET'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Straat'
              FocusControl = cxdbteF_STREET
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_STREET: TFVBFFCDBTextEdit
              Left = 80
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_STREET'
              DataBinding.DataField = 'F_STREET'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 300
            end
            object cxlblF_NUMBER1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_NUMBER'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Nummer'
              FocusControl = cxdbteF_NUMBER
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_NUMBER: TFVBFFCDBTextEdit
              Left = 80
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_NUMBER'
              DataBinding.DataField = 'F_NUMBER'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 121
            end
            object cxlblF_MAILBOX1: TFVBFFCLabel
              Left = 208
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_MAILBOX'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Bus'
              FocusControl = cxdbteF_MAILBOX
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_MAILBOX: TFVBFFCDBTextEdit
              Left = 256
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_MAILBOX'
              DataBinding.DataField = 'F_MAILBOX'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 121
            end
            object cxlblF_POSTALCODE1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_POSTALCODE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Postcode'
              FocusControl = cxdbbeF_POSTALCODE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_POSTALCODE: TFVBFFCDBButtonEdit
              Left = 80
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_POSTALCODE'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_POSTALCODE'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_POSTALCODEPropertiesButtonClick
              TabOrder = 7
              Width = 121
            end
            object cxlblF_CITY_NAME1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_CITY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Gemeente'
              FocusControl = cxdbteF_CITY_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CITY_NAME: TFVBFFCDBTextEdit
              Left = 80
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_CITY_NAME'
              DataBinding.DataField = 'F_CITY_NAME'
              DataBinding.DataSource = srcMain
              TabOrder = 9
              Width = 300
            end
            object cxlblF_COUNTRY_NAME1: TFVBFFCLabel
              Left = 8
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_COUNTRY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Land'
              FocusControl = cxdbbeF_COUNTRY_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit
              Left = 80
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_COUNTRY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_COUNTRY_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_COUNTRY_NAMEPropertiesButtonClick
              TabOrder = 11
              Width = 300
            end
          end
          object cxgbAdditionalInfo: TFVBFFCGroupBox
            Left = 400
            Top = 176
            Caption = 'Bijkomende Informatie'
            ParentColor = False
            TabOrder = 2
            Transparent = True
            Height = 129
            Width = 385
            object cxlblF_FAX1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_FAX'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Fax'
              FocusControl = cxdbteF_FAX
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_FAX: TFVBFFCDBTextEdit
              Left = 80
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_FAX'
              DataBinding.DataField = 'F_FAX'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 300
            end
            object cxlblF_GSM1: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_GSM'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'GSM'
              FocusControl = cxdbteF_GSM
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_GSM: TFVBFFCDBTextEdit
              Left = 80
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_GSM'
              DataBinding.DataField = 'F_GSM'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 300
            end
            object cxlblF_EMAIL1: TFVBFFCLabel
              Left = 8
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              FocusControl = cxdbhleF_EMAIL
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbhleF_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 80
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 5
              Width = 300
            end
            object cxlblF_MEDIUM_NAME1: TFVBFFCLabel
              Left = 8
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_MEDIUM_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Medium'
              FocusControl = cxdbbePersoon
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbePersoon: TFVBFFCDBButtonEdit
              Left = 80
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_MEDIUM_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_MEDIUM_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_MEDIUM_NAMEPropertiesButtonClick
              TabOrder = 7
              Width = 300
            end
          end
          object cxgbEducation: TFVBFFCGroupBox
            Left = 8
            Top = 342
            Caption = 'Opleidingsniveau (enkel van toepassing op bediende)'
            ParentBackground = False
            ParentColor = False
            TabOrder = 3
            Transparent = True
            Height = 57
            Width = 385
            object cxglblEducation: TFVBFFCLabel
              Left = 8
              Top = 28
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_DIPLOMA_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Opleidingsniveau'
              FocusControl = cxdbteF_ACC_NR
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_DIPLOMA_ID: TFVBFFCDBButtonEdit
              Left = 120
              Top = 28
              HelpType = htKeyword
              HelpKeyword = 'frmPerson_Record.F_DIPLOMA_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_DIPLOMA_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_DIPLOMA_IDPropertiesButtonClick
              TabOrder = 1
              Width = 257
            end
          end
        end
        inherited pnlRecordDetailTitle: TFVBFFCPanel [1]
          Width = 797
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 955
      Height = 538
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 538
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRoles
          end
          item
            Item = dxnbiEnrolments
          end
          item
            Item = dxnbiShowHistory
          end>
      end
      object dxnbiRoles: TdxNavBarItem
        Action = acShowRoles
      end
      object dxnbiEnrolments: TdxNavBarItem
        Action = acShowEnrolments
      end
      object dxnbiShowHistory: TdxNavBarItem
        Action = acShowHistory
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 538
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowRoles: TAction
      Caption = 'Functies'
      OnExecute = acShowRolesExecute
    end
    object acShowEnrolments: TAction
      Caption = 'Inschrijvingen'
      OnExecute = acShowEnrolmentsExecute
    end
    object acShowHistory: TAction
      Caption = 'Historiek'
      OnExecute = acShowHistoryExecute
    end
  end
end
