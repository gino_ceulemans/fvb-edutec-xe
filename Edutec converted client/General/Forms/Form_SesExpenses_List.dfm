inherited frmSesExpenses_List: TfrmSesExpenses_List
  Left = 902
  Top = 272
  Caption = 'Uitgaven'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_EXPENSE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_EXPENSE_ID'
          Width = 85
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
          Width = 79
        end
        object cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_NAME'
          Width = 200
        end
        object cxgrdtblvListF_EXPENSE_TYPE: TcxGridDBColumn
          DataBinding.FieldName = 'F_EXPENSE_TYPE'
          RepositoryItem = dtmEDUMainClient.cxeriExpenseOrigin
          Width = 66
        end
        object cxgrdtblvListF_EXPENSE_DATE: TcxGridDBColumn
          Caption = 'Datum'
          DataBinding.FieldName = 'F_EXPENSE_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
        end
        object cxgrdtblvListF_EXPENSE_DESC: TcxGridDBColumn
          DataBinding.FieldName = 'F_EXPENSE_DESC'
          Width = 81
        end
        object cxgrdtblvListF_EXPENSE_AMOUNT: TcxGridDBColumn
          DataBinding.FieldName = 'F_EXPENSE_AMOUNT'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Top = 328
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
