inherited frmCenInfrastructure_Record: TfrmCenInfrastructure_Record
  Left = 315
  Top = 154
  Width = 960
  Height = 670
  ActiveControl = cxdbteF_NAME
  Caption = 'Opleidingsinfrastructuur'
  Constraints.MinHeight = 440
  Constraints.MinWidth = 952
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 611
    Width = 952
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 616
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 952
    Height = 611
    inherited pnlRecord: TFVBFFCPanel
      Width = 790
      Height = 611
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 790
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 790
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 790
        object cxlblF_INFRASTRUCTURE_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCenInfrastructure_Record.F_INFRASTRUCTURE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Infrastructuur ( ID )'
          FocusControl = cxdblblF_INFRASTRUCTURE_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_INFRASTRUCTURE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCenInfrastructure_Record.F_INFRASTRUCTURE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_INFRASTRUCTURE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 113
        end
        object cxlblF_NAME1: TFVBFFCLabel
          Left = 136
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Naam'
          FocusControl = cxdblblF_NAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 136
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 790
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 607
        Width = 790
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 790
        Height = 537
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 790
        end
        inherited sbxMain: TScrollBox
          Width = 790
          Height = 512
          object cxlblF_INFRASTRUCTURE_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_INFRASTRUCTURE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'ID'
            FocusControl = cxdbseF_INFRASTRUCTURE_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_INFRASTRUCTURE_ID: TFVBFFCDBSpinEdit
            Left = 96
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_INFRASTRUCTURE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_INFRASTRUCTURE_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME2: TFVBFFCLabel
            Left = 328
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam'
            FocusControl = cxdbteF_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME: TFVBFFCDBTextEdit
            Left = 440
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
            DataBinding.DataField = 'F_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 2
            Width = 336
          end
          object cxlblF_RSZ_NR1: TFVBFFCLabel
            Left = 16
            Top = 536
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_RSZ_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'RSZ Nr'
            FocusControl = F_RSZ_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object F_RSZ_NR: TFVBFFCDBMaskEdit
            Left = 104
            Top = 536
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_RSZ_NR'
            TabStop = False
            DataBinding.DataField = 'F_RSZ_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 18
            Visible = False
            Width = 216
          end
          object cxlblF_VAT_NR1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_VAT_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'BTW Nr'
            FocusControl = F_VAT_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object F_VAT_NR: TFVBFFCDBMaskEdit
            Left = 96
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_VAT_NR'
            DataBinding.DataField = 'F_VAT_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 3
            Width = 216
          end
          object cxlblF_STREET1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_STREET'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Straat'
            FocusControl = cxdbteF_STREET
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_STREET: TFVBFFCDBTextEdit
            Left = 96
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_STREET'
            DataBinding.DataField = 'F_STREET'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 4
            Width = 216
          end
          object cxlblF_NUMBER1: TFVBFFCLabel
            Left = 328
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_NUMBER'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Nummer'
            FocusControl = cxdbteF_NUMBER
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NUMBER: TFVBFFCDBTextEdit
            Left = 440
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_NUMBER'
            DataBinding.DataField = 'F_NUMBER'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 5
            Width = 80
          end
          object cxlblF_MAILBOX1: TFVBFFCLabel
            Left = 528
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_MAILBOX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bus'
            FocusControl = cxdbteF_MAILBOX
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_MAILBOX: TFVBFFCDBTextEdit
            Left = 560
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_MAILBOX'
            DataBinding.DataField = 'F_MAILBOX'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 6
            Width = 80
          end
          object cxlblF_POSTALCODE1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Postcode'
            FocusControl = cxdbbeF_POSTALCODE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_POSTALCODE: TFVBFFCDBButtonEdit
            Left = 96
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_POSTALCODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_POSTALCODEPropertiesButtonClick
            TabOrder = 7
            Width = 216
          end
          object cxlblF_CITY_NAME1: TFVBFFCLabel
            Left = 328
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gemeente'
            FocusControl = cxdbteF_CITY_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CITY_NAME: TFVBFFCDBTextEdit
            Left = 440
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
            DataBinding.DataField = 'F_CITY_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 8
            Width = 336
          end
          object cxlblF_PHONE1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_PHONE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Telefoon'
            FocusControl = cxdbteF_PHONE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_PHONE: TFVBFFCDBTextEdit
            Left = 96
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_PHONE'
            DataBinding.DataField = 'F_PHONE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 9
            Width = 216
          end
          object cxlblF_FAX1: TFVBFFCLabel
            Left = 328
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_FAX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Fax'
            FocusControl = cxdbteF_FAX
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FAX: TFVBFFCDBTextEdit
            Left = 440
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_FAX'
            DataBinding.DataField = 'F_FAX'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 10
            Width = 216
          end
          object cxlblF_EMAIL1: TFVBFFCLabel
            Left = 16
            Top = 512
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'EMail'
            FocusControl = F_EMAIL
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object F_EMAIL: TFVBFFCDBHyperLinkEdit
            Left = 104
            Top = 512
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_EMAIL'
            TabStop = False
            DataBinding.DataField = 'F_EMAIL'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Prefix = 'mailto:'
            TabOrder = 19
            Visible = False
            Width = 216
          end
          object cxlblF_WEBSITE1: TFVBFFCLabel
            Left = 328
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_WEBSITE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'WebSite'
            FocusControl = F_WEBSITE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object F_WEBSITE: TFVBFFCDBHyperLinkEdit
            Left = 440
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_WEBSITE'
            DataBinding.DataField = 'F_WEBSITE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 12
            Width = 216
          end
          object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
            Left = 328
            Top = 560
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Taal'
            FocusControl = cxdbbeF_LANGUAGE_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
            Left = 440
            Top = 560
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_LANGUAGE_NAME'
            TabStop = False
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_LANGUAGE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
            TabOrder = 17
            Visible = False
            Width = 216
          end
          object cxlblF_ACC_NR1: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_ACC_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rekening Nr'
            FocusControl = cxdbteF_ACC_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_ACC_NR: TFVBFFCDBTextEdit
            Left = 96
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_ACC_NR'
            DataBinding.DataField = 'F_ACC_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 11
            Width = 216
          end
          object cxlblF_ACC_HOLDER1: TFVBFFCLabel
            Left = 328
            Top = 536
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_ACC_HOLDER'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rek. Houder'
            FocusControl = cxdbteF_ACC_HOLDER
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbteF_ACC_HOLDER: TFVBFFCDBTextEdit
            Left = 440
            Top = 536
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_ACC_HOLDER'
            TabStop = False
            DataBinding.DataField = 'F_ACC_HOLDER'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 20
            Visible = False
            Width = 216
          end
          object cxlblF_MEDIUM_NAME1: TFVBFFCLabel
            Left = 328
            Top = 512
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_MEDIUM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Medium'
            FocusControl = cxdbbeF_MEDIUM_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbbeF_MEDIUM_NAME: TFVBFFCDBButtonEdit
            Left = 440
            Top = 512
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_MEDIUM_NAME'
            TabStop = False
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_MEDIUM_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_MEDIUM_NAMEPropertiesButtonClick
            TabOrder = 22
            Visible = False
            Width = 216
          end
          object cxlblF_COMMENT: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Commentaar'
            FocusControl = cxdbmmoF_COMMENT
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_COMMENT: TFVBFFCDBMemo
            Left = 96
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_COMMENT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 13
            Height = 97
            Width = 681
          end
          object cxgbAvailability: TFVBFFCGroupBox
            Left = 664
            Top = 512
            Caption = 'Beschikbaarheid'
            ParentColor = False
            ParentFont = False
            TabOrder = 21
            Transparent = True
            Visible = False
            Height = 89
            Width = 113
            object cxlblF_SATURDAY1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmCenInfrastructure_Record.F_SATURDAY'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Zaterdag'
              FocusControl = csdbcbF_SATURDAY
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object csdbcbF_SATURDAY: TFVBFFCDBCheckBox
              Left = 80
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmCenInfrastructure_Record.F_SATURDAY'
              TabStop = False
              RepositoryItem = dtmEDUMainClient.cxeriBooleanCenterAlign
              Caption = 'csdbcbF_SATURDAY'
              DataBinding.DataField = 'F_SATURDAY'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 1
              Width = 21
            end
            object cxlblF_EVENING1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmCenInfrastructure_Record.F_EVENING'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Avond'
              FocusControl = csdbcbF_EVENING
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object csdbcbF_EVENING: TFVBFFCDBCheckBox
              Left = 80
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmCenInfrastructure_Record.F_EVENING'
              TabStop = False
              RepositoryItem = dtmEDUMainClient.cxeriBooleanCenterAlign
              Caption = 'csdbcbF_EVENING'
              DataBinding.DataField = 'F_EVENING'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 3
              Width = 21
            end
            object cxlblF_WEEKDAY1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmCenInfrastructure_Record.F_WEEKDAY'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Weekdag'
              FocusControl = csdbcbF_WEEKDAY
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object csdbcbF_WEEKDAY: TFVBFFCDBCheckBox
              Left = 80
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmCenInfrastructure_Record.F_WEEKDAY'
              TabStop = False
              RepositoryItem = dtmEDUMainClient.cxeriBooleanCenterAlign
              Caption = 'csdbcbF_WEEKDAY'
              DataBinding.DataField = 'F_WEEKDAY'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 5
              Width = 21
            end
          end
          object FVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 8
            Top = 368
            Caption = 'Contactgegevens bevestigingsrapporten'
            ParentColor = False
            ParentFont = False
            TabOrder = 15
            Transparent = True
            Height = 105
            Width = 769
            object cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 1'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit
              Left = 120
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT1'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 273
            end
            object F_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT1_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 2
              Width = 300
            end
            object cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT2_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 4
              Width = 300
            end
            object cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit
              Left = 120
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT2'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 273
            end
            object cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 2'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlbF_CONFIRMATION_CONTACT3: TFVBFFCLabel
              Left = 8
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 3'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT3: TFVBFFCDBTextEdit
              Left = 120
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT3'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 273
            end
            object cxlbF_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT3_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 6
              Width = 300
            end
          end
          object cxlblF_INFO: TFVBFFCLabel
            Left = 8
            Top = 256
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_INFO'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Info'
            FocusControl = cxdbmmoF_INFO
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_INFO: TFVBFFCDBMemo
            Left = 96
            Top = 256
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_INFO'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_INFO'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 14
            Height = 105
            Width = 681
          end
          object cxdbbeF_MAIL_ATTACHMENT: TFVBFFCDBButtonEdit
            Left = 104
            Top = 480
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_MAIL_ATTACHMENT'
            TabStop = False
            RepositoryItem = dtmEDUMainClient.cxeriSelectLookupClear
            DataBinding.DataField = 'F_MAIL_ATTACHMENT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_MAIL_ATTACHMENTPropertiesButtonClick
            TabOrder = 16
            Width = 673
          end
          object cxlblF_MAIL_ATTACHMENT: TFVBFFCLabel
            Left = 16
            Top = 480
            HelpType = htKeyword
            HelpKeyword = 'frmCenInfrastructure_Record.F_MAIL_ATTACHMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'E-Mail bijlage'
            FocusControl = cxdbbeF_MAIL_ATTACHMENT
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 948
      Height = 611
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 611
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiShowCenRoom
          end
          item
            Item = dxnbiShowLogHistory
          end>
      end
      object dxnbiShowCenRoom: TdxNavBarItem
        Action = acShowCenRoom
      end
      object dxnbiShowLogHistory: TdxNavBarItem
        Action = acShowLogHistory
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 611
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowCenRoom: TAction
      Caption = 'Lokalen'
      OnExecute = acShowCenRoomExecute
    end
    object acShowLogHistory: TAction
      Caption = 'Historiek'
      OnExecute = acShowLogHistoryExecute
    end
    object acShowDocCenter: TAction
      Caption = 'Lesgevers'
    end
  end
end
