{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_GENDER record.

  @Name       Form_Gender_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Gender_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDURecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmGender_Record = class(TEDURecordView)
    cxlblF_GENDER_ID1: TFVBFFCLabel;
    cxdblblF_GENDER_ID: TFVBFFCDBLabel;
    cxlblF_GENDER_NAME1: TFVBFFCLabel;
    cxdblblF_GENDER_NAME: TFVBFFCDBLabel;
    cxlblF_GENDER_ID2: TFVBFFCLabel;
    cxdbseF_GENDER_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Gender, Data_EduMainClient;

{ TfrmGender_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmGender_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmGender_Record.GetDetailName: String;
begin
  Result := cxdblblF_GENDER_NAME.Caption;
end;

end.