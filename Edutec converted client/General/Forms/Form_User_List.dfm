inherited frmUser_List: TfrmUser_List
  Left = 357
  Top = 168
  Width = 800
  Height = 512
  ActiveControl = cxdbseF_USER_ID
  Caption = 'Gebruikers'
  Constraints.MinWidth = 800
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 444
    Width = 792
  end
  inherited pnlList: TFVBFFCPanel
    Width = 784
    Height = 436
    inherited cxgrdList: TcxGrid
      Top = 190
      Width = 784
      Height = 246
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_USER_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_USER_ID'
          Width = 144
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
          Width = 200
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
          Width = 200
        end
        object cxgrdtblvListF_PHONE_INT: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE_INT'
          Visible = False
        end
        object cxgrdtblvListF_PHONE_EXT: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE_EXT'
          Visible = False
        end
        object cxgrdtblvListF_GSM: TcxGridDBColumn
          DataBinding.FieldName = 'F_GSM'
          Visible = False
        end
        object cxgrdtblvListF_EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'F_EMAIL'
          Visible = False
        end
        object cxgrdtblvListF_LOGIN_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LOGIN_ID'
          Width = 200
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
          Width = 76
        end
        object cxgrdtblvListF_GENDER_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_GENDER_ID'
          Visible = False
        end
        object cxgrdtblvListF_LOCKED: TcxGridDBColumn
          DataBinding.FieldName = 'F_LOCKED'
          Visible = False
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
          Visible = False
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          Visible = False
        end
        object cxgrdtblvListF_LOGIN_TRY: TcxGridDBColumn
          DataBinding.FieldName = 'F_LOGIN_TRY'
          Visible = False
        end
        object cxgrdtblvListF_DOMAIN: TcxGridDBColumn
          DataBinding.FieldName = 'F_DOMAIN'
          Visible = False
        end
        object cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_NAME'
          Width = 200
        end
        object cxgrdtblvListF_COMPLETE_USERNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COMPLETE_USERNAME'
          Width = 200
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 165
      Width = 784
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 161
      Width = 784
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 784
      Height = 161
      ExpandedHeight = 161
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 130
        Width = 782
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 614
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 702
        end
      end
      object cxlblF_USER_ID2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_USER_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Gebruiker ( ID )'
        FocusControl = cxdbseF_USER_ID
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbseF_USER_ID: TFVBFFCDBSpinEdit
        Left = 120
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_USER_ID'
        DataBinding.DataField = 'F_USER_ID'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 2
        Width = 121
      end
      object cxlblF_LASTNAME1: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_LASTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam'
        FocusControl = cxdbteF_LASTNAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_LASTNAME: TFVBFFCDBTextEdit
        Left = 120
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_LASTNAME'
        DataBinding.DataField = 'F_LASTNAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 4
        Width = 265
      end
      object cxlblF_FIRSTNAME1: TFVBFFCLabel
        Left = 392
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Voornaam'
        FocusControl = cxdbteF_FIRSTNAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit
        Left = 504
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_FIRSTNAME'
        DataBinding.DataField = 'F_FIRSTNAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 6
        Width = 265
      end
      object cxlblF_LOCKED1: TFVBFFCLabel
        Left = 392
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_LOCKED'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Geblokkeerd'
        FocusControl = csdbcbF_LOCKED
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object csdbcbF_LOCKED: TFVBFFCDBCheckBox
        Left = 504
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_LOCKED'
        RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
        DataBinding.DataField = 'F_LOCKED'
        DataBinding.DataSource = srcSearchCriteria
        ParentColor = False
        Properties.NullStyle = nssUnchecked
        TabOrder = 11
        Width = 21
      end
      object cxlblF_LOGIN_ID1: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_LOGIN_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Login ID'
        FocusControl = cxdbteF_LOGIN_ID
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_LOGIN_ID: TFVBFFCDBTextEdit
        Left = 120
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmUser_Record.F_LOGIN_ID'
        DataBinding.DataField = 'F_LOGIN_ID'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 8
        Width = 265
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 8
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 120
        Top = 104
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Items = <>
        TabOrder = 12
        Width = 265
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 792
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 440
    Width = 792
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 788
    Height = 436
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 436
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmUser.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
