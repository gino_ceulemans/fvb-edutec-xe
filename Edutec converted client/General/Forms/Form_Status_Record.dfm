inherited frmStatus_Record: TfrmStatus_Record
  Left = 406
  Top = 193
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Status'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_STATUS_ID2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmStatus_Record.F_STATUS_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Status ( ID )'
          FocusControl = cxdblblF_STATUS_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_STATUS_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmStatus_Record.F_STATUS_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_STATUS_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 73
        end
        object cxlblF_STATUS_NAME1: TFVBFFCLabel
          Left = 96
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmStatus_Record.F_STATUS_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Status'
          FocusControl = cxdblblF_STATUS_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_STATUS_NAME: TFVBFFCDBLabel
          Left = 96
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmStatus_Record.F_STATUS_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_STATUS_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_STATUS_ID: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_STATUS_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Status ( ID )'
            FocusControl = cxdbseF_STATUS_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_STATUS_ID: TFVBFFCDBSpinEdit
            Left = 168
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_STATUS_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_STATUS_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 168
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 250
          end
          object cxlblF_NAME_FR: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 168
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 250
          end
          object cxlblF_LONG_DESC_NL: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_LONG_DESC_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Omschrijving ( NL )'
            FocusControl = cxdbteF_LONG_DESC_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_LONG_DESC_NL: TFVBFFCDBTextEdit
            Left = 168
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_LONG_DESC_NL'
            DataBinding.DataField = 'F_LONG_DESC_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 7
            Width = 250
          end
          object cxlblF_LONG_DESC_FR: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_LONG_DESC_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Omschrijving ( FR )'
            FocusControl = cxdbteF_LONG_DESC_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_LONG_DESC_FR: TFVBFFCDBTextEdit
            Left = 168
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_LONG_DESC_FR'
            DataBinding.DataField = 'F_LONG_DESC_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 9
            Width = 250
          end
          object cxlblF_SESSION: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_SESSION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie'
            FocusControl = cxdbcbF_SESSION
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_SESSION: TFVBFFCDBCheckBox
            Left = 168
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_SESSION'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_SESSION'
            DataBinding.DataField = 'F_SESSION'
            DataBinding.DataSource = srcMain
            ParentColor = False
            TabOrder = 11
            Width = 21
          end
          object cxlblF_ENROL: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_ENROL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Inschrijving'
            FocusControl = cxdbcbF_ENROL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_ENROL: TFVBFFCDBCheckBox
            Left = 168
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_ENROL'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_ENROL'
            DataBinding.DataField = 'F_ENROL'
            DataBinding.DataSource = srcMain
            ParentColor = False
            TabOrder = 13
            Width = 21
          end
          object cxlblF_WAITING_LIST: TFVBFFCLabel
            Left = 8
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_WAITING_LIST'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Wachtlijst'
            FocusControl = cxdbcbF_WAITING_LIST
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_WAITING_LIST: TFVBFFCDBCheckBox
            Left = 168
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmStatus_Record.F_WAITING_LIST'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_WAITING_LIST'
            DataBinding.DataField = 'F_WAITING_LIST'
            DataBinding.DataSource = srcMain
            ParentColor = False
            TabOrder = 15
            Width = 21
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmStatus.cdsRecord
  end
end
