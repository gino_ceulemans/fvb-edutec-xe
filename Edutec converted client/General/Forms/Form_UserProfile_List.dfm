inherited frmUserProfile_List: TfrmUserProfile_List
  Caption = 'Gebruikers Profielen'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_USER_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_USER_ID'
          Width = 111
        end
        object cxgrdtblvListF_COMPLETE_USERNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COMPLETE_USERNAME'
          Width = 233
        end
        object cxgrdtblvListF_PROFILE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROFILE_ID'
          Width = 80
        end
        object cxgrdtblvListF_PROFILE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROFILE_NAME'
          Width = 400
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
          Visible = False
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
          Visible = False
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmUserProfile.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
