inherited frmOrganisationType_Record: TfrmOrganisationType_Record
  Width = 870
  ActiveControl = cxdbteF_NAME_NL1
  Caption = 'Organisatie Type'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 862
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 526
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 862
    inherited pnlRecord: TFVBFFCPanel
      Width = 700
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 700
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 700
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 700
        object cxlblF_ORGTYPE_ID: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisationType_Record.F_ORGTYPE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Organisatie Type ( ID )'
          FocusControl = cxdblblF_ORGTYPE_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ORGTYPE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisationType_Record.F_ORGTYPE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ORGTYPE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object cxlblF_ORGTYPE_NAME: TFVBFFCLabel
          Left = 160
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisationType_Record.F_ORGTYPE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Organisatie Type'
          FocusControl = cxdblblF_ORGTYPE_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ORGTYPE_NAME: TFVBFFCDBLabel
          Left = 160
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisationType_Record.F_ORGTYPE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ORGTYPE_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 700
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 700
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 700
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 700
        end
        inherited sbxMain: TScrollBox
          Width = 700
          object cxlblF_ORGTYPE_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisationType_Record.F_ORGTYPE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Organisatie Type ( ID )'
            FocusControl = cxdbseF_ORGTYPE_ID1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_ORGTYPE_ID1: TFVBFFCDBSpinEdit
            Left = 160
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisationType_Record.F_ORGTYPE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_ORGTYPE_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisationType_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Organisatie Type ( NL )'
            FocusControl = cxdbteF_NAME_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL1: TFVBFFCDBTextEdit
            Left = 160
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisationType_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 400
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisationType_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Organisatie Type ( FR )'
            FocusControl = cxdbteF_NAME_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR1: TFVBFFCDBTextEdit
            Left = 160
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisationType_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 400
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 858
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Visible = False
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmOrganisationType.cdsRecord
  end
end
