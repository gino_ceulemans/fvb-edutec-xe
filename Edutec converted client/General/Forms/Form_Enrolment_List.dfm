inherited frmEnrolment_List: TfrmEnrolment_List
  Left = 515
  Top = 228
  Width = 646
  Caption = 'Inschrijvingen'
  Constraints.MinWidth = 646
  OnActivate = FVBFFCListViewActivate
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 638
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 540
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 652
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 630
    inherited cxgrdList: TcxGrid
      Width = 630
      inherited cxgrdtblvList: TcxGridDBTableView
        OnEditing = cxgrdtblvListEditing
        DataController.DataModeController.SyncMode = False
        OptionsData.Editing = True
        OptionsSelection.CellSelect = True
        OptionsSelection.MultiSelect = True
        OptionsView.ShowEditButtons = gsebAlways
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListRowNumber: TcxGridDBColumn
          DataBinding.FieldName = 'RowNumber'
          Options.Editing = False
          Options.Focusing = False
          Options.Sorting = False
          Width = 25
          IsCaptionAssigned = True
        end
        object cxgrdtblvListF_ENROL_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL_ID'
          Visible = False
          Width = 110
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
          Width = 81
        end
        object cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_NAME'
          Visible = False
          Width = 200
        end
        object cxgrdtblvListF_ROLE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROLE_ID'
          Visible = False
        end
        object cxgrdtblvListF_PERSON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_ID'
          Visible = False
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Options.Editing = False
          Options.Focusing = False
          Width = 242
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
          Options.Editing = False
          Options.Focusing = False
          Width = 170
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
          Options.Editing = False
          Options.Focusing = False
          Width = 152
        end
        object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SOCSEC_NR'
          Options.Editing = False
          Options.Focusing = False
          Width = 77
        end
        object cxgrdtblvListF_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_ID'
          Visible = False
        end
        object cxgrdtblvListF_ROLEGROUP_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROLEGROUP_NAME'
          Options.Editing = False
          Options.Focusing = False
          Width = 105
        end
        object cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          Options.Editing = False
          Options.Focusing = False
          Width = 135
        end
        object cxgrdtblvListF_EVALUATION: TcxGridDBColumn
          DataBinding.FieldName = 'F_EVALUATION'
          Options.Editing = False
          Options.Focusing = False
          Width = 58
        end
        object cxgrdtblvListF_CERTIFICATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_CERTIFICATE'
          Options.Editing = False
          Options.Focusing = False
          Width = 65
        end
        object cxgrdtblvListF_INVOICED: TcxGridDBColumn
          DataBinding.FieldName = 'F_INVOICED'
          Visible = False
          Hidden = True
          Width = 82
        end
        object cxgrdtblvListF_LAST_MINUTE: TcxGridDBColumn
          DataBinding.FieldName = 'F_LAST_MINUTE'
          Options.Editing = False
          Options.Focusing = False
          Width = 70
        end
        object cxgrdtblvListF_HOURS: TcxGridDBColumn
          DataBinding.FieldName = 'F_HOURS'
          Visible = False
          Hidden = True
        end
        object cxgrdtblvListF_HOURS_PRESENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_HOURS_PRESENT'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_CERTIFICATE_PRINT_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_CERTIFICATE_PRINT_DATE'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_HOURS_ABSENT_JUSTIFIED: TcxGridDBColumn
          DataBinding.FieldName = 'F_HOURS_ABSENT_JUSTIFIED'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_HOURS_ABSENT_ILLEGIT: TcxGridDBColumn
          DataBinding.FieldName = 'F_HOURS_ABSENT_ILLEGIT'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_SELECTED_PRICE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SELECTED_PRICE'
          Options.Editing = False
          Options.Focusing = False
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          Caption = 'Start Datum'
          DataBinding.FieldName = 'F_START_DATE'
          Visible = False
        end
        object cxgrdtblvListF_NO_PARTICIPANTS: TcxGridDBColumn
          Caption = 'Deelnemers'
          DataBinding.FieldName = 'F_NO_PARTICIPANTS'
          PropertiesClassName = 'TcxCalcEditProperties'
          Visible = False
          MinWidth = 25
          Width = 76
        end
        object cxgrdtblvListF_SCORE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SCORE'
        end
        object cxgrdtblvListF_MAX_SCORE: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAX_SCORE'
        end
        object cxgrdtblvListF_CODE: TcxGridDBColumn
          Caption = 'Referentie'
          DataBinding.FieldName = 'F_CODE'
          Visible = False
        end
        object cxgrdtblvListColumnF_CONSTRUCT: TcxGridDBColumn
          Caption = 'Import vanuit Construct'
          DataBinding.FieldName = 'F_CONSTRUCT'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ReadOnly = True
          Options.Editing = False
          Options.Focusing = False
          Width = 85
        end
        object cxgrdtblvListWinterPartner: TcxGridDBColumn
          Caption = 'Winter Partner'
          DataBinding.FieldName = 'F_PARTNER_WINTER_NAME'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end
            item
            end>
          Properties.OnButtonClick = cxgrdtblvListColumn1PropertiesButtonClick
          RepositoryItem = dtmEDUMainClient.cxeriSelectLookupClear
          Options.ShowEditButtons = isebAlways
        end
        object cxgrdtblvListF_ORGTYPE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGTYPE_ID'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Width = 630
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Width = 630
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 630
      Visible = False
      inherited pnlSearchCriteriaButtons: TPanel
        Width = 628
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 578
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 666
        end
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 638
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 638
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 634
  end
  inherited srcMain: TFVBFFCDataSource
    Top = 280
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarButton1: TdxBarButton
      Action = acAttest
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = acPrintCertificate
      Category = 0
    end
    object dxbbMoveToOtherSession: TdxBarButton
      Action = acMoveToOtherSession
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = acSetGeattesteerd
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = acSetIngeschreven
      Category = 0
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton1
        Visible = True
      end
      item
        Item = dxBarButton2
        Visible = True
      end
      item
        Item = dxbbMoveToOtherSession
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton4
        Visible = True
      end
      item
        Item = dxBarButton3
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
    OnPopup = dxbpmnGridPopup
  end
  object alEnrollment: TFVBFFCActionList
    Left = 240
    Top = 384
    object acAttest: TAction
      Caption = '&Attesteer'
      OnExecute = acAttestExecute
    end
    object acSetIngeschreven: TAction
      Caption = 'Geattesteerden &terug op ingeschreven zetten'
      OnExecute = acSetIngeschrevenExecute
    end
    object acPrintCertificate: TAction
      Caption = '&Druk certificaat'
      OnExecute = acPrintCertificateExecute
    end
    object acMoveToOtherSession: TAction
      Caption = '&Verplaats naar andere Sessie'
      OnExecute = acMoveToOtherSessionExecute
      OnUpdate = acMoveToOtherSessionUpdate
    end
    object acSetGeattesteerd: TAction
      Caption = 'Gefactureerde &terug op geattesteerd zetten'
      OnExecute = acSetGeattesteerdExecute
    end
  end
end
