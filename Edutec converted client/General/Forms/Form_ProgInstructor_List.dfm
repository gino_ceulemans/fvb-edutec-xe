inherited frmProgInstructor_List: TfrmProgInstructor_List
  ActiveControl = cxgrdList
  Caption = 'Lesgevers'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_PROG_INSTRUCTOR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROG_INSTRUCTOR_ID'
          Visible = False
        end
        object cxgrdtblvListF_INSTRUCTOR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INSTRUCTOR_ID'
          Visible = False
        end
        object cxgrdtblvListF_FULLNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FULLNAME'
          Width = 200
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
          Visible = False
          Width = 200
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
          Visible = False
        end
        object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SOCSEC_NR'
          Width = 113
        end
        object cxgrdtblvListF_PHONE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE'
        end
        object cxgrdtblvListF_FAX: TcxGridDBColumn
          DataBinding.FieldName = 'F_FAX'
        end
        object cxgrdtblvListF_GSM: TcxGridDBColumn
          DataBinding.FieldName = 'F_GSM'
        end
        object cxgrdtblvListF_EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'F_EMAIL'
          Width = 200
        end
        object cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM_ID'
          Visible = False
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 200
        end
        object cxgrdtblvListF_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_START_DATE'
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          Visible = False
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Visible = False
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmProgInstructor.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmProgInstructor.cdsList
  end
end
