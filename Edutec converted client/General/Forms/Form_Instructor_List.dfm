inherited frmInstructor_List: TfrmInstructor_List
  Left = 380
  Top = 161
  Width = 820
  Height = 647
  ActiveControl = cxdbseF_INSTRUCTOR_ID1
  Caption = 'Lesgevers'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 579
    Width = 812
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 538
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 650
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 804
    Height = 571
    inherited cxgrdList: TcxGrid
      Top = 190
      Width = 804
      Height = 381
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_INSTRUCTOR_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INSTRUCTOR_ID'
          Width = 111
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
          Width = 200
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
          Width = 200
        end
        object cxgrdtblvListF_STREET: TcxGridDBColumn
          DataBinding.FieldName = 'F_STREET'
          Width = 200
        end
        object cxgrdtblvListF_NUMBER: TcxGridDBColumn
          DataBinding.FieldName = 'F_NUMBER'
          Width = 72
        end
        object cxgrdtblvListF_MAILBOX: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAILBOX'
        end
        object cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE_ID'
          Visible = False
        end
        object cxgrdtblvListF_POSTALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE'
          Width = 74
        end
        object cxgrdtblvListF_CITY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_NAME'
          Width = 200
        end
        object cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_ID'
          Visible = False
        end
        object cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_NAME'
          Width = 200
        end
        object cxgrdtblvListF_ACC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACC_NR'
          Width = 93
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Width = 120
        end
        object cxgrdtblvListF_PHONE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE'
          Visible = False
        end
        object cxgrdtblvListF_FAX: TcxGridDBColumn
          DataBinding.FieldName = 'F_FAX'
          Visible = False
          Width = 106
        end
        object cxgrdtblvListF_EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'F_EMAIL'
          Visible = False
          Width = 179
        end
        object cxgrdtblvListF_ORGANISATION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_NAME'
          Width = 200
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 165
      Width = 804
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 161
      Width = 804
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 804
      Height = 161
      ExpandedHeight = 161
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 130
        Width = 802
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 637
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 725
        end
      end
      object cxlblF_LASTNAME2: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam'
        FocusControl = cxdbteF_LASTNAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_LASTNAME1: TFVBFFCDBTextEdit
        Left = 104
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
        DataBinding.DataField = 'F_LASTNAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 4
        Width = 200
      end
      object cxlblF_FIRSTNAME2: TFVBFFCLabel
        Left = 320
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Voornaam'
        FocusControl = cxdbteF_FIRSTNAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit
        Left = 416
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        DataBinding.DataField = 'F_FIRSTNAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 6
        Width = 200
      end
      object cxlblF_INSTRUCTOR_ID2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_INSTRUCTOR_ID'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Lesgever ( ID )'
        FocusControl = cxdbseF_INSTRUCTOR_ID1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbseF_INSTRUCTOR_ID1: TFVBFFCDBSpinEdit
        Left = 104
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_INSTRUCTOR_ID'
        DataBinding.DataField = 'F_INSTRUCTOR_ID'
        DataBinding.DataSource = srcSearchCriteria
        Properties.ReadOnly = False
        TabOrder = 1
        Width = 200
      end
      object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LANGUAGE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Taal'
        FocusControl = cxdbbeF_LANGUAGE_NAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_LANGUAGE_NAME1: TFVBFFCDBButtonEdit
        Left = 104
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LANGUAGE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
        DataBinding.DataField = 'F_LANGUAGE_NAME'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
        TabOrder = 8
        Width = 200
      end
      object cxlblF_CODE1: TFVBFFCLabel
        Left = 320
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_CODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Code'
        FocusControl = cxdbteF_CODE1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_CODE1: TFVBFFCDBTextEdit
        Left = 416
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_CODE'
        DataBinding.DataField = 'F_CODE'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 10
        Width = 121
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 8
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        FocusControl = cxdbteF_FIRSTNAME1
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 104
        Top = 104
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Items = <>
        TabOrder = 12
        Width = 200
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 812
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 575
    Width = 812
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 808
    Height = 571
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 571
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmInstructor.cdsList
    Top = 336
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
