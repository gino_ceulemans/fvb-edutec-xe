inherited frmOrganisation_Record: TfrmOrganisation_Record
  Left = 696
  Top = 100
  Width = 1006
  Height = 788
  Caption = 'Organisatie'
  Constraints.MinHeight = 540
  Constraints.MinWidth = 970
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 729
    Width = 998
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 662
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 998
    Height = 729
    inherited pnlRecord: TFVBFFCPanel
      Width = 836
      Height = 729
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 836
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 836
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 836
        object cxlblF_ORGANISATION_ID: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisation_Record.F_ORGANISATION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Organisatie ( ID )'
          FocusControl = cxdblblF_ORGANISATION_ID
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ORGANISATION_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisation_Record.F_ORGANISATION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ORGANISATION_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 97
        end
        object cxlblF_NAME: TFVBFFCLabel
          Left = 128
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisation_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Organisatie'
          FocusControl = cxdblblF_NAME
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 128
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmOrganisation_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 657
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 836
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 725
        Width = 836
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 836
        Height = 655
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 836
        end
        inherited sbxMain: TScrollBox
          Width = 836
          Height = 630
          object cxgbAddress: TFVBFFCGroupBox
            Left = 408
            Top = 0
            Caption = 'Adres'
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 3
            Transparent = True
            Height = 169
            Width = 385
            object cxlblF_STREET1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_STREET'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Straat'
              FocusControl = cxdbteF_STREET1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_STREET1: TFVBFFCDBTextEdit
              Left = 80
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_STREET'
              DataBinding.DataField = 'F_STREET'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 2
              Width = 300
            end
            object cxlblF_NUMBER1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_NUMBER'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Nummer'
              FocusControl = cxdbteF_NUMBER1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_NUMBER1: TFVBFFCDBTextEdit
              Left = 80
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_NUMBER'
              DataBinding.DataField = 'F_NUMBER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 3
              Width = 121
            end
            object cxlblF_MAILBOX1: TFVBFFCLabel
              Left = 208
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_MAILBOX'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Bus'
              FocusControl = cxdbteF_MAILBOX1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_MAILBOX1: TFVBFFCDBTextEdit
              Left = 256
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_MAILBOX'
              DataBinding.DataField = 'F_MAILBOX'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 4
              Width = 121
            end
            object cxlblF_POSTALCODE1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_POSTALCODE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Postcode'
              FocusControl = cxdbbeF_POSTALCODE1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_POSTALCODE1: TFVBFFCDBButtonEdit
              Left = 80
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_POSTALCODE'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_POSTALCODE'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_POSTALCODE1PropertiesButtonClick
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 5
              Width = 121
            end
            object cxlblF_CITY_NAME1: TFVBFFCLabel
              Left = 8
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CITY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Gemeente'
              FocusControl = cxdbteF_CITY_NAME1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CITY_NAME1: TFVBFFCDBTextEdit
              Left = 80
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CITY_NAME'
              DataBinding.DataField = 'F_CITY_NAME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.ReadOnly = True
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 6
              Width = 300
            end
            object cxlblF_COUNTRY_NAME1: TFVBFFCLabel
              Left = 8
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_COUNTRY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Land'
              FocusControl = cxdbbeF_COUNTRY_NAME1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_COUNTRY_NAME1: TFVBFFCDBButtonEdit
              Left = 80
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_COUNTRY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_COUNTRY_NAME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_COUNTRY_NAME1PropertiesButtonClick
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 7
              Width = 300
            end
            object FVBFFCLabel2: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_STREET'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Extra info'
              FocusControl = cxdbteF_STREET1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_EXTRA_INFO: TFVBFFCDBTextEdit
              Left = 80
              Top = 16
              HelpType = htKeyword
              DataBinding.DataField = 'F_EXTRA_INFO'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 1
              Width = 300
            end
          end
          object cxgbInvoiceAddress: TFVBFFCGroupBox
            Left = 408
            Top = 208
            Caption = 'Facturatie Adres'
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 2
            Transparent = True
            Height = 201
            Width = 385
            object cxlblF_INVOICE_STREET1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_STREET'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Straat'
              FocusControl = cxdbteF_INVOICE_STREET
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_INVOICE_NUMBER1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_NUMBER'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Nummer'
              FocusControl = cxdbteF_INVOICE_NUMBER
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_INVOICE_POSTALCODE1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_POSTALCODE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Postcode'
              FocusControl = cxdbbeF_INVOICE_POSTALCODE
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_INVOICE_CITY_NAME1: TFVBFFCLabel
              Left = 8
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_CITY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Gemeente'
              FocusControl = cxdbteF_INVOICE_CITY_NAME
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_INVOICE_COUNTRY_NAME1: TFVBFFCLabel
              Left = 8
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_COUNTRY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Land'
              FocusControl = cxdbbeF_INVOICE_COUNTRY_NAME
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_INVOICE_COUNTRY_NAME: TFVBFFCDBButtonEdit
              Left = 80
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_COUNTRY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_INVOICE_COUNTRY_NAME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_COUNTRY_NAME1PropertiesButtonClick
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 7
              Width = 300
            end
            object cxdbteF_INVOICE_CITY_NAME: TFVBFFCDBTextEdit
              Left = 80
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_CITY_NAME'
              DataBinding.DataField = 'F_INVOICE_CITY_NAME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.ReadOnly = True
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 6
              Width = 300
            end
            object cxdbbeF_INVOICE_POSTALCODE: TFVBFFCDBButtonEdit
              Left = 80
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_POSTALCODE'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_INVOICE_POSTALCODE'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_POSTALCODE1PropertiesButtonClick
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 5
              Width = 121
            end
            object cxdbteF_INVOICE_NUMBER: TFVBFFCDBTextEdit
              Left = 80
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_NUMBER'
              DataBinding.DataField = 'F_INVOICE_NUMBER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 3
              Width = 121
            end
            object cxdbteF_INVOICE_STREET: TFVBFFCDBTextEdit
              Left = 80
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_STREET'
              DataBinding.DataField = 'F_INVOICE_STREET'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 2
              Width = 300
            end
            object cxlblF_INVOICE_MAILBOX1: TFVBFFCLabel
              Left = 208
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_MAILBOX'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Bus'
              FocusControl = cxdbteF_INVOICE_MAILBOX
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_INVOICE_MAILBOX: TFVBFFCDBTextEdit
              Left = 256
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_INVOICE_MAILBOX'
              DataBinding.DataField = 'F_INVOICE_MAILBOX'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 4
              Width = 123
            end
            object FVBFFCLabel3: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_STREET'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Extra info'
              FocusControl = cxdbteF_STREET1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_INVOICE_EXTRA_INFO: TFVBFFCDBTextEdit
              Left = 80
              Top = 16
              HelpType = htKeyword
              DataBinding.DataField = 'F_INVOICE_EXTRA_INFO'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 1
              Width = 300
            end
          end
          object cxgbGeneral: TFVBFFCGroupBox
            Left = 8
            Top = 0
            Caption = 'Algemeen'
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 0
            Transparent = True
            Height = 409
            Width = 393
            object cxlblF_ORGANISATION_ID1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ORGANISATION_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'ID'
              FocusControl = cxdbseF_ORGANISATION_ID1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_ORGANISATION_ID1: TFVBFFCDBSpinEdit
              Left = 88
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ORGANISATION_ID'
              RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
              DataBinding.DataField = 'F_ORGANISATION_ID'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 1
              Width = 105
            end
            object cxlblF_NAME1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Organisatie'
              FocusControl = cxdbteF_NAME1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_NAME1: TFVBFFCDBTextEdit
              Left = 88
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_NAME'
              DataBinding.DataField = 'F_NAME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 4
              Width = 300
            end
            object cxlblF_RSZ_NR1: TFVBFFCLabel
              Left = 8
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_RSZ_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'RSZ nr'
              FocusControl = F_RSZ_NR1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_RSZ_NR1: TFVBFFCDBMaskEdit
              Left = 88
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_RSZ_NR'
              DataBinding.DataField = 'F_RSZ_NR'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 9
              Width = 200
            end
            object cxlblF_VAT_NR1: TFVBFFCLabel
              Left = 8
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_VAT_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'BTW Nr'
              FocusControl = F_VAT_NR1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_VAT_NR1: TFVBFFCDBMaskEdit
              Left = 88
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_VAT_NR'
              DataBinding.DataField = 'F_VAT_NR'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 11
              Width = 200
            end
            object cxlblF_PHONE1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_PHONE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Telefoon'
              FocusControl = cxdbteF_PHONE1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_PHONE1: TFVBFFCDBTextEdit
              Left = 88
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_PHONE'
              DataBinding.DataField = 'F_PHONE'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 6
              Width = 200
            end
            object cxlblF_FAX1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_FAX'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Fax'
              FocusControl = cxdbteF_FAX1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_FAX1: TFVBFFCDBTextEdit
              Left = 88
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_FAX'
              DataBinding.DataField = 'F_FAX'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 7
              Width = 200
            end
            object cxlblF_WEBSITE1: TFVBFFCLabel
              Left = 8
              Top = 208
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_WEBSITE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Website'
              FocusControl = F_WEBSITE1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_WEBSITE1: TFVBFFCDBHyperLinkEdit
              Left = 88
              Top = 208
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_WEBSITE'
              RepositoryItem = dtmEDUMainClient.cxeriURL
              DataBinding.DataField = 'F_WEBSITE'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 16
              Width = 300
            end
            object cxlblF_ACC_NR1: TFVBFFCLabel
              Left = 8
              Top = 160
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ACC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Rek. Nr'
              FocusControl = F_ACC_NR1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_ACC_NR1: TFVBFFCDBMaskEdit
              Left = 88
              Top = 160
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ACC_NR'
              DataBinding.DataField = 'F_ACC_NR'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 13
              Width = 200
            end
            object cxlblF_ACC_HOLDER1: TFVBFFCLabel
              Left = 8
              Top = 184
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ACC_HOLDER'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Rek. houder'
              FocusControl = cxdbteF_ACC_HOLDER1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_ACC_HOLDER1: TFVBFFCDBTextEdit
              Left = 88
              Top = 184
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ACC_HOLDER'
              DataBinding.DataField = 'F_ACC_HOLDER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 14
              Width = 300
            end
            object cxlblF_ORGTYPE_NAME1: TFVBFFCLabel
              Left = 8
              Top = 232
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ORGTYPE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Org. Type'
              FocusControl = cxdbbeF_ORGTYPE_NAME1
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_ORGTYPE_NAME1: TFVBFFCDBButtonEdit
              Left = 88
              Top = 232
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_ORGTYPE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
              DataBinding.DataField = 'F_ORGTYPE_NAME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_ORGTYPE_NAME1PropertiesButtonClick
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 20
              Width = 200
            end
            object cxlblFVBFFCLabel1: TFVBFFCLabel
              Left = 8
              Top = 376
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'ConstructID'
              FocusControl = cxdbteF_CONSTRUCT_ID
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONSTRUCT_ID: TFVBFFCDBTextEdit
              Left = 88
              Top = 376
              HelpType = htKeyword
              DataBinding.DataField = 'F_CONSTRUCT_ID'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 26
              Width = 100
            end
            object cxdbcbF_MANUALLY_ADDED: TFVBFFCDBCheckBox
              Left = 364
              Top = 16
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              DataBinding.DataField = 'F_MANUALLY_ADDED'
              DataBinding.DataSource = srcMain
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 2
              Width = 21
            end
            object cxlblF_MANUALLY_ADDED: TFVBFFCLabel
              Left = 202
              Top = 16
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Toegevoegd door EduTEC ?'
              FocusControl = cxdbcbF_MANUALLY_ADDED
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCLabel1: TFVBFFCLabel
              Left = 8
              Top = 305
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contact opm'
              FocusControl = cxdbteF_CONSTRUCT_ID
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBMemo1: TFVBFFCDBMemo
              Left = 88
              Top = 304
              DataBinding.DataField = 'F_CONTACT_DATA'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.ScrollBars = ssVertical
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 25
              Height = 70
              Width = 300
            end
            object FVBFFCLabel4: TFVBFFCLabel
              Left = 208
              Top = 376
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Synergy ID'
              FocusControl = cxdbteF_SYNERGY_ID
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_SYNERGY_ID: TFVBFFCDBTextEdit
              Left = 287
              Top = 376
              HelpType = htKeyword
              DataBinding.DataField = 'F_SYNERGY_ID'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 27
              Width = 100
            end
            object cxlblF_PARTNER_WINTER: TFVBFFCLabel
              Left = 8
              Top = 257
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Partner winter'
              FocusControl = cxdbteF_CONSTRUCT_ID
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcbF_PARTNER_WINTER: TFVBFFCDBCheckBox
              Left = 92
              Top = 256
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              DataBinding.DataField = 'F_PARTNER_WINTER'
              DataBinding.DataSource = srcMain
              Enabled = False
              ParentColor = False
              ParentFont = False
              Properties.OnEditValueChanged = cxdbcbF_PARTNER_WINTERPropertiesEditValueChanged
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 21
              Width = 21
            end
            object FVBFFCLabel5: TFVBFFCLabel
              Left = 128
              Top = 257
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'DVO'
              FocusControl = cxdbteF_CONSTRUCT_ID
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_AUTHORIZATION_NR: TFVBFFCDBTextEdit
              Left = 160
              Top = 256
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_PHONE'
              DataBinding.DataField = 'F_AUTHORIZATION_NR'
              DataBinding.DataSource = srcMain
              Enabled = False
              ParentFont = False
              Properties.MaxLength = 25
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 22
              Width = 224
            end
            object cxlblF_ADMIN_COST_TYPE: TFVBFFCLabel
              Left = 8
              Top = 281
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Admin kost type'
              FocusControl = cxdbteF_CONSTRUCT_ID
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_ADMIN_COST: TFVBFFCLabel
              Left = 160
              Top = 281
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Admin kost'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_ADMIN_COST: TFVBFFCDBCurrencyEdit
              Left = 280
              Top = 280
              DataBinding.DataField = 'F_ADMIN_COST'
              DataBinding.DataSource = srcMain
              Enabled = False
              ParentFont = False
              Properties.DisplayFormat = ',0.00;(,0.00)'
              TabOrder = 24
              Width = 105
            end
            object cboAdminCostType: TFVBFFCComboBox
              Left = 104
              Top = 280
              Enabled = False
              ParentFont = False
              Properties.Items.Strings = (
                #8364
                '%')
              Properties.OnEditValueChanged = cboAdminCostTypePropertiesEditValueChanged
              TabOrder = 23
              Width = 49
            end
          end
          object cxbtnCopyToInvoice: TFVBFFCButton
            Left = 504
            Top = 178
            Width = 201
            Height = 25
            Caption = 'Kopieer naar Fakturatie Adres'
            TabOrder = 1
            OnClick = cxbtnCopyToInvoiceClick
            LookAndFeel.Kind = lfUltraFlat
          end
          object FVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 7
            Top = 416
            Caption = 'Contactgegevens bevestigingsrapporten'
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 4
            Transparent = True
            Height = 97
            Width = 786
            object cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 1'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit
              Left = 120
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT1'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 0
              Width = 273
            end
            object cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT1_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT1_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 1
              Width = 300
            end
            object cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 2'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit
              Left = 120
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT2'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 2
              Width = 273
            end
            object cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT2_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT2_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 3
              Width = 300
            end
            object cxlbF_CONFIRMATION_CONTACT3: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Contactpersoon 3'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CONFIRMATION_CONTACT3: TFVBFFCDBTextEdit
              Left = 120
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3'
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT3'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 4
              Width = 273
            end
            object cxlbF_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCLabel
              Left = 408
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              ParentColor = False
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object F_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCDBHyperLinkEdit
              Left = 448
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmOrganisation_Record.F_CONFIRMATION_CONTACT3_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriEMail
              DataBinding.DataField = 'F_CONFIRMATION_CONTACT3_EMAIL'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Style.Font.Charset = ANSI_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Verdana'
              Style.Font.Style = []
              TabOrder = 5
              Width = 300
            end
          end
          object cxlblF_EMAIL1: TFVBFFCLabel
            Left = 32
            Top = 536
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'EMail'
            FocusControl = F_EMAIL1
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object F_EMAIL1: TFVBFFCDBHyperLinkEdit
            Left = 112
            Top = 536
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriEMail
            DataBinding.DataField = 'F_EMAIL'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 6
            Visible = False
            Width = 300
          end
          object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
            Left = 32
            Top = 560
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Taal'
            FocusControl = cxdbbeF_LANGUAGE_NAME1
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbbeF_LANGUAGE_NAME1: TFVBFFCDBButtonEdit
            Left = 112
            Top = 560
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_LANGUAGE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_LANGUAGE_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 8
            Visible = False
            Width = 200
          end
          object cxlblF_MEDIUM_NAME1: TFVBFFCLabel
            Left = 32
            Top = 584
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_MEDIUM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Medium'
            FocusControl = cxdbbeF_MEDIUM_NAME1
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbbeF_MEDIUM_NAME1: TFVBFFCDBButtonEdit
            Left = 112
            Top = 584
            HelpType = htKeyword
            HelpKeyword = 'frmOrganisation_Record.F_MEDIUM_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_MEDIUM_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_MEDIUM_NAME1PropertiesButtonClick
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 10
            Visible = False
            Width = 200
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 994
      Height = 729
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 729
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiShowSessions
          end
          item
            Item = dxnbiRoles
          end
          item
            Item = dxnbiShowKMO_Portefeuille
          end
          item
            Item = dxnbiShowLogHistory
          end>
      end
      object dxnbiRoles: TdxNavBarItem
        Action = acShowRoles
      end
      object dxnbiShowLogHistory: TdxNavBarItem
        Action = acShowHistory
      end
      object dxnbiShowSessions: TdxNavBarItem
        Action = acShowSessions
      end
      object dxnbNavShowInstructors: TdxNavBarItem
        Action = acShowInstructors
      end
      object dxnbiShowKMO_Portefeuille: TdxNavBarItem
        Action = acShowKMO_Portefeuille
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 729
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowRoles: TAction
      Caption = 'Functies'
      OnExecute = acShowRolesExecute
    end
    object acShowHistory: TAction
      Caption = 'Historiek'
      OnExecute = acShowHistoryExecute
    end
    object acShowSessions: TAction
      Caption = 'Sessies'
      OnExecute = acShowSessionsExecute
    end
    object acShowInstructors: TAction
      Caption = 'Lesgevers'
    end
    object acShowKMO_Portefeuille: TAction
      Caption = 'KMO Portefeuille'
      OnExecute = acShowKMO_PortefeuilleExecute
    end
  end
end
