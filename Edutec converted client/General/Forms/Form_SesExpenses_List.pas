{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_SesExpenses_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  02/08/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_SesExpenses_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  Menus, cxCalendar;

type
  TfrmSesExpenses_List = class(TEDUListView)
    cxgrdtblvListF_EXPENSE_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_EXPENSE_TYPE: TcxGridDBColumn;
    cxgrdtblvListF_EXPENSE_DESC: TcxGridDBColumn;
    cxgrdtblvListF_EXPENSE_AMOUNT: TcxGridDBColumn;
    cxgrdtblvListF_EXPENSE_DATE: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SesExpenses, Data_EduMainClient;

end.