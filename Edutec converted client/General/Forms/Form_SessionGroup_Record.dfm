inherited frmSessionGroup_Record: TfrmSessionGroup_Record
  Width = 820
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Sessie Groep'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 812
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 476
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 812
    inherited pnlRecord: TFVBFFCPanel
      Width = 650
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 650
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 650
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 650
        DesignSize = (
          650
          37)
        object cxlblF_SESSIONGROUP_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Sessie Groep ( ID )'
          FocusControl = cxdblblF_SESSIONGROUP_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SESSIONGROUP_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_SESSIONGROUP_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 113
        end
        object cxlblF_SESSIONGROUP_NAME1: TFVBFFCLabel
          Left = 144
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Sessie Groep'
          FocusControl = cxdblblF_SESSIONGROUP_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SESSIONGROUP_NAME: TFVBFFCDBLabel
          Left = 144
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_SESSIONGROUP_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 493
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 650
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 650
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 650
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 650
        end
        inherited sbxMain: TScrollBox
          Width = 650
          object cxlblF_SESSIONGROUP_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie Groep ( ID )'
            FocusControl = cxdbseF_SESSIONGROUP_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_SESSIONGROUP_ID: TFVBFFCDBSpinEdit
            Left = 144
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmSessionGroup_Record.F_SESSIONGROUP_ID'
            DataBinding.DataField = 'F_SESSIONGROUP_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmSessionGroup_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie Groep ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 144
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmSessionGroup_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 337
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmSessionGroup_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie Groep ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 144
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmSessionGroup_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 337
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 808
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiSessions
          end>
      end
      object dxnbiSessions: TdxNavBarItem
        Action = acShowSessions
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmSessionGroup.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowSessions: TAction
      Caption = 'Sessies'
      OnExecute = acShowSessionsExecute
    end
  end
end
