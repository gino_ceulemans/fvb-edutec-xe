{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_SesWizard_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/08/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_SesWizard_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  Menus;

type
  TfrmSesWizard_List = class(TEduListView)
    cxgrdtblvListF_SESWIZARD_ID: TcxGridDBColumn;
    cxgrdtblvListF_SPID: TcxGridDBColumn;
    cxgrdtblvListF_EXTERN_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_COMPANY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM_NAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_TRANSPORT_INS: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_START_HOUR: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_ID: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_EVALUATION: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_CERTIFICATE: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_INVOICED: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_LAST_MINUTE: TcxGridDBColumn;
    cxgrdtblvListF_ENROL_HOURS: TcxGridDBColumn;
    cxgrdtblvListF_SES_CREATE: TcxGridDBColumn;
    cxgrdtblvListF_SES_UPDATE: TcxGridDBColumn;
    cxgrdtblvListF_ENR_CREATE: TcxGridDBColumn;
    cxgrdtblvListF_ENR_UPDATE: TcxGridDBColumn;
    cxgrdtblvListF_FORECAST_COMMENT: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SesWizard;

end.