{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_LANGUAGE record.

  @Name       Form_Language_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Language_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, DB,
  dxNavBarCollns, dxNavBarBase, dxNavBar,
  cxButtons, ExtCtrls, 
  StdCtrls, Buttons, cxDBLabel, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxControls, cxContainer, cxEdit, cxLabel, ActnList,
  Unit_FVBFFCDevExpress, Unit_FVBFFCComponents, Unit_FVBFFCDBComponents,
  Unit_FVBFFCFoldablePanel, Menus, cxSplitter;

type
  TfrmLanguage_Record = class(TEDURecordView)
    cxlblF_LANGUAGE_ID1: TFVBFFCLabel;
    cxdbseF_LANGUAGE_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    cxlblF_LANGUAGE_ID2: TFVBFFCLabel;
    F_LANGUAGE_ID2: TFVBFFCDBLabel;
    cxlblF_NAME_NL2: TFVBFFCLabel;
    F_NAME_NL2: TFVBFFCDBLabel;
    cxlblF_NAME_FR2: TFVBFFCLabel;
    F_NAME_FR2: TFVBFFCDBLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmLanguage_Record: TfrmLanguage_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Language, Data_EDUMainClient;

{ TfrmLanguage_Record }

function TfrmLanguage_Record.GetDetailName: String;
begin
  Result := cxdbteF_NAME_NL.Text;
end;

end.
