inherited frmEnrolment_Record: TfrmEnrolment_Record
  Left = 503
  Top = 165
  Height = 507
  ActiveControl = cxdbbeF_SESSION_NAME
  Caption = 'Inschrijving'
  Constraints.MinHeight = 507
  OnAllDetailsCreated = FVBFFCRecordViewAllDetailsCreated
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 448
  end
  inherited pnlTop: TFVBFFCPanel
    Height = 448
    inherited pnlRecord: TFVBFFCPanel
      Height = 448
      inherited pnlRecordHeader: TFVBFFCPanel
        Top = 45
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Top = 4
        object cxlblF_ENROL_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmEnrolment_Record.F_ENROL_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Inschrijving ( ID )'
          FocusControl = cxdblblF_ENROL_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ENROL_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmEnrolment_Record.F_ENROL_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ENROL_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 105
        end
        object cxlblF_LASTNAME1: TFVBFFCLabel
          Left = 128
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmEnrolment_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Familienaam'
          FocusControl = cxdblblF_LASTNAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_LASTNAME: TFVBFFCDBLabel
          Left = 128
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmEnrolment_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_LASTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 209
        end
        object cxlblF_FIRSTNAME1: TFVBFFCLabel
          Left = 344
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmEnrolment_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Voornaam'
          FocusControl = cxdblblF_FIRSTNAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_FIRSTNAME: TFVBFFCDBLabel
          Left = 344
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmEnrolment_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_FIRSTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 225
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Top = 41
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 444
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Height = 374
        inherited sbxMain: TScrollBox
          Height = 345
          object cxlblF_ENROL_ID: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_ENROL_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Inschrijving ( ID )'
            FocusControl = cxdbseF_ENROL_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_ENROL_ID: TFVBFFCDBSpinEdit
            Left = 136
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_ENROL_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_ENROL_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 121
          end
          object cxlblF_SESSION_NAME: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_SESSION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie'
            FocusControl = cxdbbeF_SESSION_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_SESSION_NAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_SESSION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_SESSION_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_SESSION_NAMEPropertiesButtonClick
            TabOrder = 3
            Width = 400
          end
          object cxlblF_LASTNAME: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_LASTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Familienaam'
            FocusControl = cxdbbeF_LASTNAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_LASTNAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_LASTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectAddLookup
            DataBinding.DataField = 'F_LASTNAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_LASTNAMEPropertiesButtonClick
            TabOrder = 5
            Width = 400
          end
          object cxlblF_FIRSTNAME: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_FIRSTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Voornaam'
            FocusControl = cxdbteF_FIRSTNAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit
            Left = 136
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_FIRSTNAME'
            DataBinding.DataField = 'F_FIRSTNAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 7
            Width = 265
          end
          object cxlblF_SOCSEC_NR: TFVBFFCLabel
            Left = 408
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_SOCSEC_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rijksregister NR'
            FocusControl = cxdbteF_SOCSEC_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit
            Left = 512
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_SOCSEC_NR'
            DataBinding.DataField = 'F_SOCSEC_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 9
            Width = 145
          end
          object cxlblF_STATUS_NAME: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Status'
            FocusControl = cxdbbeF_STATUS_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_STATUS_NAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_STATUS_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_STATUS_NAMEPropertiesButtonClick
            TabOrder = 12
            Width = 160
          end
          object cxlblF_EVALUATION: TFVBFFCLabel
            Left = 304
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_EVALUATION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Evaluatie'
            FocusControl = cxdbcbF_EVALUATION
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_EVALUATION: TFVBFFCDBCheckBox
            Left = 376
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_EVALUATION'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_EVALUATION'
            DataBinding.DataField = 'F_EVALUATION'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 13
            Width = 21
          end
          object cxlblF_CERTIFICATE: TFVBFFCLabel
            Left = 304
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_CERTIFICATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Certificaat'
            FocusControl = cxdbcbF_CERTIFICATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_CERTIFICATE: TFVBFFCDBCheckBox
            Left = 376
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_CERTIFICATE'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_CERTIFICATE'
            DataBinding.DataField = 'F_CERTIFICATE'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 16
            Width = 21
          end
          object cxlblF_ENROLMENT_DATE: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_ENROLMENT_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Inschrijvingsdatum'
            FocusControl = cxdbdeF_ENROLMENT_DATE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_ENROLMENT_DATE: TFVBFFCDBDateEdit
            Left = 136
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_ENROLMENT_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_ENROLMENT_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 15
            Width = 160
          end
          object cxlblF_INVOICED: TFVBFFCLabel
            Left = 408
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_INVOICED'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gefactureerd'
            FocusControl = cxdbcbF_INVOICED
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbcbF_INVOICED: TFVBFFCDBCheckBox
            Left = 496
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_INVOICED'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_INVOICED'
            DataBinding.DataField = 'F_INVOICED'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 25
            Visible = False
            Width = 21
          end
          object cxlblF_LAST_MINUTE: TFVBFFCLabel
            Left = 416
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_LAST_MINUTE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Last Minute'
            FocusControl = cxdbcbF_LAST_MINUTE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_LAST_MINUTE: TFVBFFCDBCheckBox
            Left = 510
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_LAST_MINUTE'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_LAST_MINUTE'
            DataBinding.DataField = 'F_LAST_MINUTE'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 14
            Width = 21
          end
          object cxlblF_COMMENT: TFVBFFCLabel
            Left = 8
            Top = 384
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Commentaar'
            FocusControl = cxdbmmoF_COMMENT
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_COMMENT: TFVBFFCDBMemo
            Left = 8
            Top = 402
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_COMMENT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_COMMENT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 21
            Height = 81
            Width = 649
          end
          object cxgbFVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 360
            Top = 184
            Caption = 'Prijsbepaling'
            ParentColor = False
            ParentFont = False
            TabOrder = 19
            Height = 121
            Width = 297
            object cxlblF_PRICE_WORKER: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Factuurbedrag'
              FocusControl = cxdbcueF_PRICE_WORKER
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcueF_PRICE_WORKER: TFVBFFCDBCurrencyEdit
              Left = 152
              Top = 40
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_WORKER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 0
              Width = 136
            end
            object cxdbcueF_PRICE_CLERK: TFVBFFCDBCurrencyEdit
              Left = 152
              Top = 40
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_CLERK'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 1
              Width = 136
            end
            object cxbtnF_CORRECT_PRICE: TFVBFFCButton
              Left = 72
              Top = 74
              Width = 193
              Height = 25
              Caption = 'Corrigeer sessieprijs !'
              TabOrder = 3
              OnClick = cxbtnF_CORRECT_PRICEClick
            end
            object cxdbcueF_PRICE_OTHER: TFVBFFCDBCurrencyEdit
              Left = 152
              Top = 40
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriBedrag
              DataBinding.DataField = 'F_PRICE_OTHER'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 2
              Width = 136
            end
            object cxdbicbF_PRICE_REDUCTION: TFVBFFCDBImageComboBox
              Left = 152
              Top = 12
              DataBinding.DataField = 'F_PRICING_SCHEME'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.Alignment.Horz = taCenter
              Properties.Items = <
                item
                  Description = 'Geen'
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = 'F.V.B.'
                  Value = 1
                end
                item
                  Description = 'Cevora'
                  Value = 2
                end>
              Properties.OnChange = cxdbicbF_PRICE_REDUCTIONPropertiesChange
              TabOrder = 5
              Width = 137
            end
            object cxlblF_PRICE_REDUCTION: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_PRICE_REDUCTION'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Selecteer tussenkomst'
              FocusControl = cxdbicbF_PRICE_REDUCTION
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
          end
          object cxgbFVBFFCGroupBox2: TFVBFFCGroupBox
            Left = 8
            Top = 184
            Caption = 'Attestatie'
            ParentColor = False
            ParentFont = False
            TabOrder = 18
            Height = 169
            Width = 345
            object cxlblF_HOURS_ABSENT_ILLEGIT1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS_ABSENT_ILLEGIT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Uren Afwezig ( Ongewettigd )'
              FocusControl = cxdbseF_HOURS_ABSENT_ILLEGIT1
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_HOURS_ABSENT_ILLEGIT1: TFVBFFCDBSpinEdit
              Left = 185
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS_ABSENT_ILLEGIT'
              DataBinding.DataField = 'F_HOURS_ABSENT_ILLEGIT'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 2
              Width = 152
            end
            object cxdbseF_HOURS_ABSENT_JUSTIFIED1: TFVBFFCDBSpinEdit
              Left = 185
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS_ABSENT_JUSTIFIED'
              DataBinding.DataField = 'F_HOURS_ABSENT_JUSTIFIED'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 1
              Width = 152
            end
            object cxdbseF_HOURS_PRESENT1: TFVBFFCDBSpinEdit
              Left = 185
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS_PRESENT'
              DataBinding.DataField = 'F_HOURS_PRESENT'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 0
              Width = 152
            end
            object cxlblF_HOURS_PRESENT1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS_PRESENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Uren Aanwezig'
              FocusControl = cxdbseF_HOURS_PRESENT1
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_HOURS_ABSENT_JUSTIFIED1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS_ABSENT_JUSTIFIED'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Uren Afwezig ( Gewettigd )'
              FocusControl = cxdbseF_HOURS_ABSENT_JUSTIFIED1
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_HOURS1: TFVBFFCDBSpinEdit
              Left = 184
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS'
              DataBinding.DataField = 'F_TOTAL_SESSION_HOURS'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 6
              Width = 153
            end
            object cxlblF_HOURS1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Totaal Aantal Uren ( Sessie )'
              FocusControl = cxdbseF_HOURS1
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_SCORE: TFVBFFCLabel
              Left = 8
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Score ( Sessie )'
              FocusControl = cxdbseF_SCORE
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_SCORE: TFVBFFCDBSpinEdit
              Left = 184
              Top = 112
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_SCORE'
              DataBinding.DataField = 'F_SCORE'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 9
              Width = 153
            end
            object cxdbseF_MAX_SCORE: TFVBFFCDBSpinEdit
              Left = 184
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_MAX_SCORE'
              DataBinding.DataField = 'F_MAX_SCORE'
              DataBinding.DataSource = srcMain
              ParentFont = False
              TabOrder = 10
              Width = 153
            end
            object FVBFFCLabel2: TFVBFFCLabel
              Left = 8
              Top = 136
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_HOURS'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Max score ( Sessie )'
              FocusControl = cxdbseF_MAX_SCORE
              ParentColor = False
              ParentFont = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
          end
          object cxlblF_NAME: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_NAME_COMPANY'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bedrijf'
            FocusControl = cxdbteF_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME: TFVBFFCDBTextEdit
            Left = 136
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_NAME_COMPANY'
            DataBinding.DataField = 'F_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 10
            Width = 265
          end
          object cxlblCertificatePrintDate: TFVBFFCLabel
            Left = 416
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_EVALUATION'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Print datum'
            FocusControl = cxdbcbF_EVALUATION
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object csdbedtCertificatePrintDate: TFVBFFCDBTextEdit
            Left = 512
            Top = 152
            DataBinding.DataField = 'F_CERTIFICATE_PRINT_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 17
            Width = 145
          end
          object cxdbbef_PARTNERWINTER: TFVBFFCDBButtonEdit
            Left = 136
            Top = 360
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_SESSION_PARTNER_WINTER'
            RepositoryItem = dtmEDUMainClient.cxeriSelectLookupClear
            DataBinding.DataField = 'F_PARTNER_WINTER_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Kind = bkGlyph
              end
              item
                Caption = 'Remove'
                Default = True
                Kind = bkText
              end>
            Properties.OnButtonClick = cxdbbef_PARTNER_WINTERPropertiesButtonClick
            TabOrder = 30
            Width = 400
          end
          object FVBFFCLabel1: TFVBFFCLabel
            Left = 8
            Top = 360
            HelpType = htKeyword
            HelpKeyword = 'frmEnrolment_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Partner Winter'
            FocusControl = cxdbbeF_STATUS_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
        end
        object pnlPriceSchemeInfoSeperator: TFVBFFCPanel
          Left = 0
          Top = 370
          Width = 630
          Height = 4
          Align = alBottom
          BevelOuter = bvNone
          BorderWidth = 4
          ParentColor = True
          TabOrder = 2
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clInactiveCaption
          StyleClientArea.BackColor2 = clInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Height = 448
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 448
      DefaultStyles.Background.BackColor = clInactiveCaption
      DefaultStyles.Background.BackColor2 = clInactiveCaption
      DefaultStyles.Background.Font.Charset = DEFAULT_CHARSET
      DefaultStyles.Background.Font.Name = 'MS Sans Serif'
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiShowLogHistory
          end>
      end
      object dxnbiShowLogHistory: TdxNavBarItem
        Action = acShowLogHistory
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 448
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowLogHistory: TAction
      Caption = 'Historiek'
      OnExecute = acShowLogHistoryExecute
    end
  end
end
