{*****************************************************************************
  This unit contains the form that will be used for the confirmation reports

  @Name       Form_Confirmation_Reports
  @Author     ivdbossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  19/07/2007   ivdbossche           Initial creation of this unit.
******************************************************************************}

unit Form_Confirmation_Reports;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDURecordView, Menus, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, cxControls,
  cxSplitter, Unit_FVBFFCDevExpress, dxNavBarCollns, dxNavBarBase,
  dxNavBar, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel, StdCtrls,
  Buttons, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxBlobEdit, cxSpinEdit, cxDBLookupComboBox,
  cxCalendar, cxImageComboBox, cxCalc, cxContainer, cxTextEdit, cxMemo,
  cxCheckBox, unit_EdutecInterfaces, Data_ConfirmationReports, Data_EDUMainClient,
  Grids, DBGrids, cxDBEdit, DBCtrls, DBClient, //dxCntner, dxExEdtr, dxEdLib,
  Unit_Types, cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, dxSkinscxPCPainter, cxNavigator,
  System.Actions;

const
  CTITLE='Exporteren van bevestigingsrapporten';

type
  TfrmConfirmationReports = class(TEDURecordView)
    grpCompanies: TGroupBox;
    gbCompExtra: TGroupBox;
    grpInfrastructure: TGroupBox;
    grpTeacher: TGroupBox;
    cxGridComp: TcxGrid;
    cxTVCompaniesAndSchools: TcxGridDBTableView;
    cxGridLevel5: TcxGridLevel;
    srcCompanies: TFVBFFCDataSource;
    cxTVCompaniesAndSchoolsF_CONF_SUB_ID: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_CONF_ID: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_ORGANISATION: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_ORGANISATION_TYPE: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_NO_PARTICIPANTS: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_ORG_CONTACT1_EMAIL: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_ORG_CONTACT2_EMAIL: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_CONFIRMATION_DATE: TcxGridDBColumn;
    srcInfrastructure: TFVBFFCDataSource;
    srcOrganisation: TFVBFFCDataSource;
    cxGridInfra: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    gbInfraExtra: TGroupBox;
    cxGridTeach: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    gbTeachExtra: TGroupBox;
    cxGridDBTableView1F_CONF_SUB_ID: TcxGridDBColumn;
    cxGridDBTableView1F_CONF_ID: TcxGridDBColumn;
    cxGridDBTableView1F_LOCATION: TcxGridDBColumn;
    cxGridDBTableView1F_ORGANISATION_TYPE: TcxGridDBColumn;
    cxGridDBTableView1F_NO_PARTICIPANTS: TcxGridDBColumn;
    cxGridDBTableView1F_ORG_CONTACT1_EMAIL: TcxGridDBColumn;
    cxGridDBTableView1F_ORG_CONTACT2_EMAIL: TcxGridDBColumn;
    cxGridDBTableView1F_CONFIRMATION_DATE: TcxGridDBColumn;
    cxGridDBTableView1F_SESSION_ID: TcxGridDBColumn;
    cxGridDBTableView2F_CONF_SUB_ID: TcxGridDBColumn;
    cxGridDBTableView2F_CONF_ID: TcxGridDBColumn;
    cxGridDBTableView2F_ORGANISATION: TcxGridDBColumn;
    cxGridDBTableView2F_ORGANISATION_TYPE: TcxGridDBColumn;
    cxGridDBTableView2F_NO_PARTICIPANTS: TcxGridDBColumn;
    cxGridDBTableView2F_ORG_CONTACT1_EMAIL: TcxGridDBColumn;
    cxGridDBTableView2F_ORG_CONTACT2_EMAIL: TcxGridDBColumn;
    cxGridDBTableView2F_CONFIRMATION_DATE: TcxGridDBColumn;
    cxGridDBTableView2F_SESSION_ID: TcxGridDBColumn;
    FVBFFCDBExtra_Infrastructure: TFVBFFCDBMemo;
    FVBFFCDBExtra_Organisation: TFVBFFCDBMemo;
    cxGridDBF_PRINT_CONFIRMATION: TcxGridDBColumn;
    FVBFFCDBCheckBox1: TFVBFFCDBCheckBox;
    cxGridDBTableViewF_ORG_CONTACT1: TcxGridDBColumn;
    cxTVCompaniesAndSchoolsF_ORG_CONTACT2: TcxGridDBColumn;
    cxGridDBTableView1F_ORG_CONTACT1: TcxGridDBColumn;
    cxGridDBTableView1F_ORG_CONTACT2: TcxGridDBColumn;
    cxGridDBTableView2F_ORG_CONTACT1: TcxGridDBColumn;
    cxGridDBTableView2F_ORG_CONTACT2: TcxGridDBColumn;
    cxGridDBTableView2F_INSTRUCTOR_NAME: TcxGridDBColumn;
    cxGridDBTableView2F_PRINT_CONFIRMATION: TcxGridDBColumn;
    cxGridDBTableView2F_ORG_INSTRUCTOR_NAME: TcxGridDBColumn;
    FVBFFCDBEXTRA_COMPANIES: TFVBFFCDBMemo;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    chkMailing: TCheckBox;
    chkMailInfraAttachCompanySchool: TCheckBox;
    chkMailInfraAttachTeacher: TCheckBox;
    cxBtnEnlargeCompanies: TFVBFFCButton;
    cxBtnEnlargeInfra: TFVBFFCButton;
    cxBtnEnlargeTeacher: TFVBFFCButton;
    cxBtnNormalCompanies: TFVBFFCButton;
    cxBtnNormalInfra: TFVBFFCButton;
    cxBtnNormalTeacher: TFVBFFCButton;
    cxTVCompaniesAndSchoolsF_PARTNER_WINTER_NAME: TcxGridDBColumn;
    btnPreview: TFVBFFCButton;
    cxTVCompaniesAndSchoolsF_ISCCENABLED: TcxGridDBColumn;
    procedure srcMainStateChange(Sender: TObject);
    procedure FVBFFCRecordViewShow(Sender: TObject);
    procedure FVBFFCRecordViewActivate(Sender: TObject);
    procedure srcDataChanged(Sender: TObject; Field: TField);
    procedure cxbtnApplyClick(Sender: TObject);
    procedure cxbtnOKClick(Sender: TObject);
    procedure FVBFFCRecordViewCreate(Sender: TObject);
    procedure cxBtnEnlargeCompaniesClick(Sender: TObject);
    procedure cxBtnEnlargeInfraClick(Sender: TObject);
    procedure cxBtnEnlargeTeacherClick(Sender: TObject);
    procedure cxBtnNormalCompaniesClick(Sender: TObject);
    procedure cxBtnNormalInfraClick(Sender: TObject);
    procedure cxBtnNormalTeacherClick(Sender: TObject);
    procedure btnPreviewClick(Sender: TObject);
    procedure cxTVCompaniesAndSchoolsEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
  private
    { Private declarations }
    FExpandedGroupBox: TGroupBox;
    FFirstTime      : boolean;
    FgbDisplayComp  : TgbDisplay;
    FgbDisplayInfra : TgbDisplay;
    FgbDisplayTeach : TgbDisplay;
    procedure btnApplyEnabled;
    procedure ConfirmUpdatingContactInfo( AField: TField );
    procedure gbExpandCollapse(aGroupBox: TGroupBox);
    procedure gbInit;
    procedure gbNormal(aGroupBox: TGroupBox);
    procedure gbExpand(aGroupBox: TGroupBox);
  protected
    function GetDetailName : String; override;
  public
    { Public declarations }
  end;

var
  frmConfirmationReports: TfrmConfirmationReports;

implementation

{$R *.dfm}

uses Unit_Const;

{ TfrmConfirmationReports }

procedure TfrmConfirmationReports.btnApplyEnabled;
begin
  if srcMain.DataSet <> nil then begin
        cxbtnApply.Enabled := ((srcMain.State in [ dsEdit, dsInsert ]) or (TClientDataSet(srcMain.DataSet).ChangeCount > 0))
                            or ((srcCompanies.State in [ dsEdit, dsInsert ]) or (TClientDataSet(srcCompanies.DataSet).ChangeCount > 0))
                            or ((srcInfrastructure.State in [ dsEdit, dsInsert ] ) or (TClientDataSet(srcInfrastructure.DataSet).ChangeCount > 0))
                            or ((srcOrganisation.State in [ dsEdit, dsInsert ]) or (TClientDataSet(srcOrganisation.DataSet).ChangeCount > 0))
      end;
end;

procedure TfrmConfirmationReports.srcMainStateChange(Sender: TObject);
begin
  btnApplyEnabled;
end;

procedure TfrmConfirmationReports.srcDataChanged(Sender: TObject;
  Field: TField);
begin
  inherited;

    if Field <> nil then
      ConfirmUpdatingContactInfo( Field ); // Check whether contact info has been changed or not...

end;

function TfrmConfirmationReports.GetDetailName: String;
begin
  Result := CTITLE;
end;

procedure TfrmConfirmationReports.FVBFFCRecordViewShow(Sender: TObject);
begin
//  inherited;

end;

procedure TfrmConfirmationReports.FVBFFCRecordViewActivate(
  Sender: TObject);
begin
  inherited;

    WindowState := wsMaximized;
end;

procedure TfrmConfirmationReports.ConfirmUpdatingContactInfo(
  AField: TField);
var
  aFieldName: String;
  aSourceDataSet: TDataSet;
  aNoContact, aOrganisationId: Integer;
  aContact, aContactEmail: String;
  aDatamodule: IEDUConfirmationReportsDataModule;
  confSubId, isCcEnabled: Integer;
  oldcursor: TCursor;
begin
  aFieldName := AField.FieldName;
  aSourceDataSet := AField.DataSet;

  // Contact information has been changed?
  if ( aFieldName = 'F_ORG_CONTACT1' ) or ( aFieldName = 'F_ORG_CONTACT2' )
    or ( aFieldName = 'F_ORG_CONTACT1_EMAIL' ) or ( aFieldName = 'F_ORG_CONTACT2_EMAIL' )
  then begin
      oldcursor := Screen.Cursor;
      try
        Screen.Cursor := crSQLWait;

        if ( Assigned( FrameWorkDataModule ) ) and
        ( Supports( FrameWorkDataModule, IEDUConfirmationReportsDataModule, aDataModule ) )
        then begin
          if (
            {
               (aSourceDataSet = srcOrganisation.DataSet)
                  and not (aSourceDataSet.FieldByName('F_ORG_INSTRUCTOR_NAME').IsNull )
                ) // Selected contact is linked to instructor with organisation?

                or (  ( aSourceDataSet <> srcOrganisation.DataSet) // Make sure that an instructor' contact has not been selected
                          and (MessageDlg( rs_confirm_updating_contact, mtConfirmation, [mbYes, mbNo], 0 )=mrYes)
            }
              ( (aSourceDataSet = srcCompanies.DataSet)
                and (MessageDlg( rs_confirm_updating_contact_ORGANISATION, mtConfirmation, [mbYes, mbNo], 0 )=mrYes)
               )
              or
              ( (aSourceDataSet = srcInfrastructure.DataSet)
                and (MessageDlg( rs_confirm_updating_contact_INFRASTRUCTURE, mtConfirmation, [mbYes, mbNo], 0 )=mrYes)
               )
              or
              ( (aSourceDataSet = srcOrganisation.DataSet)
                and (MessageDlg( rs_confirm_updating_contact_TEACHER, mtConfirmation, [mbYes, mbNo], 0 )=mrYes)
               )
             ) then // Ask user for confirmation when contact is linked to company/school or infrastructure
          begin
              // Which contact have been changed?  The first one or the second one?
              if (aFieldName = 'F_ORG_CONTACT1' ) or ( aFieldName = 'F_ORG_CONTACT1_EMAIL' ) then
              begin
                aNoContact := 1;
                aContact := AField.DataSet.FieldByName( 'F_ORG_CONTACT1' ).AsString;
                aContactEmail := AField.DataSet.FieldByName( 'F_ORG_CONTACT1_EMAIL' ).AsString;
              end else begin
                aNoContact := 2;
                aContact := AField.DataSet.FieldByName( 'F_ORG_CONTACT2' ).AsString;
                aContactEmail := AField.DataSet.FieldByName( 'F_ORG_CONTACT2_EMAIL' ).AsString;
              end;

              // Which contact info has been changed?
              if aSourceDataSet = srcCompanies.DataSet then // School / Company contact has been changed?
                begin
                  aOrganisationId := aSourceDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger;
                  confSubId := aSourceDataSet.FieldByName('F_CONF_SUB_ID').AsInteger;

                  if aSourceDataSet.FieldByName('F_ISCCENABLED').AsBoolean then
                  begin
                    isCcEnabled := 1;
                  end else
                  begin
                    isCcEnabled := 0;
                  end;
                  aDataModule.UpdateContactCompany( aOrganisationId, aNoContact, aContact, aContactEmail, confSubId, isCcEnabled);
                end
              else if aSourceDataSet = srcInfrastructure.DataSet then // Infrastructure contact has been changed?
                begin
                  aOrganisationId := aSourceDataSet.FieldByName( 'F_INFRASTRUCTURE_ID' ).AsInteger;
                  aDataModule.UpdateContactInfrastruct( aOrganisationId, aNoContact, aContact, aContactEmail );
                end
              else
                begin
                // Ask user for confirmation here since instructor might not have a link to an organisation
                //if MessageDlg( rs_confirm_updating_contact, mtConfirmation, [mbYes, mbNo], 0 )=mrYes then
                //begin
                  aOrganisationId := aSourceDataSet.FieldByName('F_INSTRUCTOR_ID' ).AsInteger;
                  aDataModule.UpdateContactInstructorOrg( aOrganisationId, ANoContact, AContact, aContactEmail );
                //end;
                end;

          end;

        end;

       finally
        Screen.Cursor := oldcursor;
       end;

  end;

end;

procedure TfrmConfirmationReports.cxbtnApplyClick(Sender: TObject);
var
  aDataModule: IEDUConfirmationReportsDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUConfirmationReportsDataModule, aDataModule ) )
  then
      aDatamodule.ApplyChanges;

  btnApplyEnabled;

end;

procedure TfrmConfirmationReports.cxbtnOKClick(Sender: TObject);
var
  aDatamodule: IEDUConfirmationReportsDataModule;
begin
  inherited;
    cxbtnApplyClick(Sender);

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUConfirmationReportsDataModule, aDataModule ) )
  then begin
      try
        // Mantis 5106 (12/03/2009)
        // Modified by Ivan Van den Bossche
        // Post changes before creation of confirmation reports. --> 12/03/2009
        if srcCompanies.State in [dsInsert, dsEdit] then
          srcCompanies.DataSet.Post;

        if srcInfrastructure.State in [dsInsert, dsEdit] then
          srcInfrastructure.DataSet.Post;

        if srcOrganisation.State in [dsInsert, dsEdit] then
          srcOrganisation.DataSet.Post;

        if srcMain.State in [dsInsert, dsEdit] then
          srcMain.DataSet.Post;

        // Create and send (optional) confirmation reports
        aDatamodule.CreateConfirmations( chkMailing.Checked, chkMailInfraAttachCompanySchool.Checked, chkMailInfraAttachTeacher.Checked );

        // attesteer direct alle ingeschreven personen -> voorfakturatie
        // aDatamodule.AttestRegistered(srcmain.dataset);

        ModalResult := mrOk;
      except
          on E: Exception do begin
              MessageDlg(E.Message, mtError, [mbOk], 0);
              ModalResult := mrNone;
          end;
      end;
  end;

end;

procedure TfrmConfirmationReports.FVBFFCRecordViewCreate(Sender: TObject);
begin
  inherited;
  FFirstTime      := True;
  FExpandedGroupBox := nil;
  cxBtnEnlargeCompanies.Visible := True;
  cxBtnNormalCompanies.Visible := False;
  cxBtnEnlargeInfra.Visible := True;
  cxBtnNormalInfra.Visible := False;
  cxBtnEnlargeTeacher.Visible := True;
  cxBtnNormalTeacher.Visible := False;
end;

procedure TfrmConfirmationReports.gbExpandCollapse(aGroupBox: TGroupBox);
  procedure gbEC_Normal(aGB: TGroupBox; agbD: TgbDisplay);
  begin
    aGB.Align    := alNone;
    aGB.Anchors  := [];
    aGB.Top      := agbD.Top;
    aGB.Left     := agbD.Left;
    aGB.Width    := agbD.Width;
    aGB.Height   := agbD.Height;
    aGB.Align    := agbD.Align;
    aGB.Anchors  := agbD.Anchors;
    agbD.Grid.Height := agbD.GridHeight;
    agbD.gbExtra.Top := agbD.gbExtraTop;
    agbD.gbExtra.Height := agbD.gbExtraHeight;
    //aGB.Visible  := True;
    agbD.btnPlus.Visible := True;
    agbD.btnMinus.Visible := False;
  end;
  procedure gbEC_Expand(aGB: TGroupBox; agbD: TgbDisplay);
  begin
    aGB.Align := alClient;
    agbD.gbExtra.Height := 200;
    agbD.gbExtra.Top := agb.ClientHeight - agbD.gbExtra.Height - 10;
    agbD.Grid.Height := agbD.gbExtra.Top - agbD.Grid.Top - 10;
    agbD.btnPlus.Visible := False;
    agbD.btnMinus.Visible := True;
    if (aGB = grpCompanies) then
    begin
      grpInfrastructure.Height := 30;
      grpTeacher.Height := 30;
    end;
    if (aGB = grpInfrastructure) then
    begin
      grpCompanies.Height := 30;
      grpTeacher.Height := 30;
    end;
    if (aGB = grpTeacher) then
    begin
      grpInfrastructure.Height := 30;
      grpCompanies.Height := 30;
    end;
  end;
begin
  gbInit;
  //grpCompanies.Visible      := assigned(FExpandedGroupBox) or (aGroupBox = grpCompanies);
  //grpInfrastructure.Visible := assigned(FExpandedGroupBox) or (aGroupBox = grpInfrastructure);
  //grpTeacher.Visible        := assigned(FExpandedGroupBox) or (aGroupBox = grpTeacher);

  //if (grpCompanies.Visible) and (grpInfrastructure.Visible) and (grpTeacher.Visible) then
  if assigned(FExpandedGroupBox) then
    begin
      gbEC_Normal(grpCompanies, FgbDisplayComp);
      gbEC_Normal(grpInfrastructure, FgbDisplayInfra);
      gbEC_Normal(grpTeacher, FgbDisplayTeach);
      FExpandedGroupBox := nil;
    end
  else
    begin
      if (aGroupBox = grpCompanies)       then gbEC_Expand(grpCompanies, FgbDisplayComp);
      if (aGroupBox = grpInfrastructure)  then gbEC_Expand(grpInfrastructure, FgbDisplayInfra);
      if (aGroupBox = grpTeacher)         then gbEC_Expand(grpTeacher, FgbDisplayTeach);
      FExpandedGroupBox := aGroupBox;
    end;

end;

procedure TfrmConfirmationReports.gbExpand(aGroupBox: TGroupBox);
const
  collapseHeight = 36;
begin
  gbInit;

  if (aGroupBox = grpCompanies) then
  begin
    FgbDisplayTeach.gbExtra.Visible := False;
    FgbDisplayTeach.Grid.Visible := False;
    FgbDisplayTeach.GroupBox.Height := collapseHeight;
    FgbDisplayTeach.GroupBox.Top := FgbDisplayTeach.GroupBox.Parent.ClientHeight
                                  - FgbDisplayTeach.GroupBox.Height - 10;
    FgbDisplayTeach.btnPlus.Visible := True;
    FgbDisplayTeach.btnMinus.Visible := False;

    FgbDisplayInfra.gbExtra.Visible := False;
    FgbDisplayInfra.Grid.Visible := False;
    FgbDisplayInfra.GroupBox.Height := collapseHeight;
    FgbDisplayInfra.GroupBox.Top := FgbDisplayTeach.GroupBox.Top
                                  - FgbDisplayInfra.GroupBox.Height - 10;
    FgbDisplayInfra.btnPlus.Visible := True;
    FgbDisplayInfra.btnMinus.Visible := False;

    FgbDisplayComp.GroupBox.Top := 10;
    FgbDisplayComp.gbExtra.Visible := True;
    FgbDisplayComp.Grid.Visible := True;
    FgbDisplayComp.GroupBox.Height := FgbDisplayInfra.GroupBox.Top - 10;
    FgbDisplayComp.gbExtra.Height := 200;
    FgbDisplayComp.gbExtra.Top := FgbDisplayComp.GroupBox.ClientHeight - FgbDisplayComp.gbExtra.Height - 10;
    FgbDisplayComp.Grid.Height := FgbDisplayComp.gbExtra.Top - FgbDisplayComp.Grid.Top - 10;
    FgbDisplayComp.btnPlus.Visible := False;
    FgbDisplayComp.btnMinus.Visible := True;
  end;

  if (aGroupBox = grpInfrastructure) then
  begin
    FgbDisplayComp.gbExtra.Visible := False;
    FgbDisplayComp.Grid.Visible := False;
    FgbDisplayComp.GroupBox.Height := collapseHeight;
    FgbDisplayComp.GroupBox.Top := 10;
    FgbDisplayComp.btnPlus.Visible := True;
    FgbDisplayComp.btnMinus.Visible := False;

    FgbDisplayTeach.gbExtra.Visible := False;
    FgbDisplayTeach.Grid.Visible := False;
    FgbDisplayTeach.GroupBox.Height := collapseHeight;
    FgbDisplayTeach.GroupBox.Top := FgbDisplayTeach.GroupBox.Parent.ClientHeight
                                  - FgbDisplayTeach.GroupBox.Height - 10;
    FgbDisplayTeach.btnPlus.Visible := True;
    FgbDisplayTeach.btnMinus.Visible := False;

    FgbDisplayInfra.GroupBox.Top := FgbDisplayComp.GroupBox.Top + FgbDisplayComp.GroupBox.Height + 10;
    FgbDisplayInfra.gbExtra.Visible := True;
    FgbDisplayInfra.Grid.Visible := True;
    FgbDisplayInfra.GroupBox.Height := FgbDisplayTeach.GroupBox.Top
                                     - FgbDisplayComp.GroupBox.Height - 20;
    FgbDisplayInfra.gbExtra.Height := 200;
    FgbDisplayInfra.gbExtra.Top := FgbDisplayInfra.GroupBox.ClientHeight - FgbDisplayInfra.gbExtra.Height - 10;
    FgbDisplayInfra.Grid.Height := FgbDisplayInfra.gbExtra.Top - FgbDisplayInfra.Grid.Top - 10;
    FgbDisplayInfra.btnPlus.Visible := False;
    FgbDisplayInfra.btnMinus.Visible := True;
  end;

  if (aGroupBox = grpTeacher) then
  begin
    FgbDisplayComp.gbExtra.Visible := False;
    FgbDisplayComp.Grid.Visible := False;
    FgbDisplayComp.GroupBox.Height := collapseHeight;
    FgbDisplayComp.GroupBox.Top := 10;
    FgbDisplayComp.btnPlus.Visible := True;
    FgbDisplayComp.btnMinus.Visible := False;

    FgbDisplayInfra.gbExtra.Visible := False;
    FgbDisplayInfra.Grid.Visible := False;
    FgbDisplayInfra.GroupBox.Height := collapseHeight;
    FgbDisplayInfra.GroupBox.Top := FgbDisplayComp.GroupBox.Top
                                  + FgbDisplayComp.GroupBox.Height + 10;
    FgbDisplayInfra.btnPlus.Visible := True;
    FgbDisplayInfra.btnMinus.Visible := False;

    FgbDisplayTeach.GroupBox.Top := FgbDisplayInfra.GroupBox.Top
                                  + FgbDisplayInfra.GroupBox.Height + 10;
    FgbDisplayTeach.gbExtra.Visible := True;
    FgbDisplayTeach.Grid.Visible := True;
    FgbDisplayTeach.GroupBox.Height := FgbDisplayTeach.GroupBox.Parent.ClientHeight
                                     - FgbDisplayTeach.GroupBox.Top
                                     - 10;
    FgbDisplayTeach.gbExtra.Height := 200;
    FgbDisplayTeach.gbExtra.Top := FgbDisplayTeach.GroupBox.ClientHeight - FgbDisplayTeach.gbExtra.Height - 10;
    FgbDisplayTeach.Grid.Height := FgbDisplayTeach.gbExtra.Top - FgbDisplayTeach.Grid.Top - 10;
    FgbDisplayTeach.btnPlus.Visible := False;
    FgbDisplayTeach.btnMinus.Visible := True;
  end;

end;

procedure TfrmConfirmationReports.gbNormal(aGroupBox: TGroupBox);
  procedure gbNormal_Item(aGB: TGroupBox; agbD: TgbDisplay);
  begin
    aGB.Align    := alNone;
    aGB.Anchors  := [];
    aGB.Top      := agbD.Top;
    aGB.Left     := agbD.Left;
    aGB.Width    := agbD.Width;
    aGB.Height   := agbD.Height;
    aGB.Align    := agbD.Align;
    aGB.Anchors  := agbD.Anchors;
    agbD.Grid.Height := agbD.GridHeight;
    agbD.gbExtra.Top := agbD.gbExtraTop;
    agbD.gbExtra.Height := agbD.gbExtraHeight;
    agbD.btnPlus.Visible := True;
    agbD.btnMinus.Visible := False;
    agbD.gbExtra.Visible := True;
    agbD.Grid.Visible := True;
  end;
begin
  gbInit;

  gbNormal_Item(grpCompanies, FgbDisplayComp);
  gbNormal_Item(grpInfrastructure, FgbDisplayInfra);
  gbNormal_Item(grpTeacher, FgbDisplayTeach);
end;

procedure TfrmConfirmationReports.cxBtnEnlargeCompaniesClick(
  Sender: TObject);
begin
  inherited;
  gbExpand(grpCompanies);
end;

procedure TfrmConfirmationReports.cxBtnEnlargeInfraClick(Sender: TObject);
begin
  inherited;
  gbExpand(grpInfrastructure);
end;

procedure TfrmConfirmationReports.cxBtnEnlargeTeacherClick(
  Sender: TObject);
begin
  inherited;
  gbExpand(grpTeacher);
end;

procedure TfrmConfirmationReports.cxBtnNormalCompaniesClick(
  Sender: TObject);
begin
  inherited;
  gbNormal(grpCompanies);
end;

procedure TfrmConfirmationReports.cxBtnNormalInfraClick(Sender: TObject);
begin
  inherited;
  gbNormal(grpInfrastructure);
end;

procedure TfrmConfirmationReports.cxBtnNormalTeacherClick(Sender: TObject);
begin
  inherited;
  gbNormal(grpTeacher);
end;

procedure TfrmConfirmationReports.gbInit;
begin
  if FFirstTime then
  begin
    FgbDisplayComp.Align    := grpCompanies.Align;
    FgbDisplayComp.Anchors  := grpCompanies.Anchors;
    FgbDisplayComp.Top      := grpCompanies.Top;
    FgbDisplayComp.Left     := grpCompanies.Left;
    FgbDisplayComp.Width    := grpCompanies.Width;
    FgbDisplayComp.Height   := grpCompanies.Height;
//    FgbDisplayComp.btnPlus  := cxBtnEnlargeCompanies;
//    FgbDisplayComp.btnMinus := cxBtnNormalCompanies;
    FgbDisplayComp.Grid     := cxGridComp;
    FgbDisplayComp.GridHeight := cxGridComp.Height;
    FgbDisplayComp.gbExtra  := gbCompExtra;
    FgbDisplayComp.gbExtraHeight := gbCompExtra.Height;
    FgbDisplayComp.gbExtraTop := gbCompExtra.Top;
    FgbDisplayComp.GroupBox := grpCompanies;

    FgbDisplayInfra.Align    := grpInfrastructure.Align;
    FgbDisplayInfra.Anchors  := grpInfrastructure.Anchors;
    FgbDisplayInfra.Top      := grpInfrastructure.Top;
    FgbDisplayInfra.Left     := grpInfrastructure.Left;
    FgbDisplayInfra.Width    := grpInfrastructure.Width;
    FgbDisplayInfra.Height   := grpInfrastructure.Height;
//    FgbDisplayInfra.btnPlus  := cxBtnEnlargeInfra;
//    FgbDisplayInfra.btnMinus := cxBtnNormalInfra;
    FgbDisplayInfra.Grid     := cxGridInfra;
    FgbDisplayInfra.GridHeight := cxGridInfra.Height;
    FgbDisplayInfra.gbExtra  := gbInfraExtra;
    FgbDisplayInfra.gbExtraHeight := gbInfraExtra.Height;
    FgbDisplayInfra.gbExtraTop := gbInfraExtra.Top;
    FgbDisplayInfra.GroupBox := grpInfrastructure;

    FgbDisplayTeach.Align    := grpTeacher.Align;
    FgbDisplayTeach.Anchors  := grpTeacher.Anchors;
    FgbDisplayTeach.Top      := grpTeacher.Top;
    FgbDisplayTeach.Left     := grpTeacher.Left;
    FgbDisplayTeach.Width    := grpTeacher.Width;
    FgbDisplayTeach.Height   := grpTeacher.Height;
//    FgbDisplayTeach.btnPlus  := cxBtnEnlargeTeacher;
//    FgbDisplayTeach.btnMinus := cxBtnNormalTeacher;
    FgbDisplayTeach.Grid     := cxGridTeach;
    FgbDisplayTeach.GridHeight := cxGridTeach.Height;
    FgbDisplayTeach.gbExtra  := gbTeachExtra;
    FgbDisplayTeach.gbExtraHeight := gbTeachExtra.Height;
    FgbDisplayTeach.gbExtraTop := gbTeachExtra.Top;
    FgbDisplayTeach.GroupBox := grpTeacher;

    FFirstTime      := false;
  end;
end;

procedure TfrmConfirmationReports.btnPreviewClick(Sender: TObject);

 var
  aDatamodule: IEDUConfirmationReportsDataModule;
begin
  inherited;
    cxbtnApplyClick(Sender);

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUConfirmationReportsDataModule, aDataModule ) )
  then begin
      try
        // Mantis 5106 (12/03/2009)
        // Modified by Ivan Van den Bossche
        // Post changes before creation of confirmation reports. --> 12/03/2009
        if srcCompanies.State in [dsInsert, dsEdit] then
          srcCompanies.DataSet.Post;

        if srcInfrastructure.State in [dsInsert, dsEdit] then
          srcInfrastructure.DataSet.Post;

        if srcOrganisation.State in [dsInsert, dsEdit] then
          srcOrganisation.DataSet.Post;

        if srcMain.State in [dsInsert, dsEdit] then
          srcMain.DataSet.Post;

        // Create and send (optional) confirmation reports
        aDatamodule.CreateConfirmationsForPreview( chkMailing.Checked, chkMailInfraAttachCompanySchool.Checked, chkMailInfraAttachTeacher.Checked );  

        ModalResult := mrNone;
      except
          on E: Exception do begin
              MessageDlg(E.Message, mtError, [mbOk], 0);
              ModalResult := mrNone;
          end;
      end;
  end;
end;

procedure TfrmConfirmationReports.cxTVCompaniesAndSchoolsEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
var
  partnerWinter: String;
begin
  inherited;

    if AItem.Index = cxTVCompaniesAndSchoolsF_ISCCENABLED.Index then
    begin
      // CC is disabled when there's no partner winter
      partnerWinter := VarToStr(cxTVCompaniesAndSchools.DataController.Values[AItem.FocusedCellViewInfo.GridRecord.RecordIndex,
                                                                              cxTVCompaniesAndSchoolsF_PARTNER_WINTER_NAME.Index]);
      AAllow := Length(partnerWinter) > 0;
    end;
end;

end.
