{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  T_PROGRAMTYPE records.
  
  @Name       Form_ProgramType_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  20/04/2017   gceulemans           Initial creation of the Unit.
******************************************************************************}

unit Form_ProgramType_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, 
  StdCtrls, cxButtons, 
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, Unit_FVBFFCDevExpress, Unit_FVBFFCDBComponents,
  Unit_FVBFFCFoldablePanel, Menus;

type
  TfrmProgramType_List = class(TEDUListView)
    cxgrdtblvListF_PROGRAMTYPE_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME_NL: TcxGridDBColumn;
    cxgrdtblvListF_NAME_FR: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProgramType_List: TfrmProgramType_List;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ProgramType;

end.
