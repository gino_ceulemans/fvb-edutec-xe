inherited frmTitle_Record: TfrmTitle_Record
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Title'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_TITLE_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmTitle_Record.F_TITLE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Title ( ID )'
          FocusControl = cxdblblF_TITLE_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_TITLE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmTitle_Record.F_TITLE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_TITLE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 65
        end
        object cxlblF_TITLE_NAME1: TFVBFFCLabel
          Left = 88
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmTitle_Record.F_TITLE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Title'
          FocusControl = cxdblblF_TITLE_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_TITLE_NAME: TFVBFFCDBLabel
          Left = 88
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmTitle_Record.F_TITLE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_TITLE_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_TITLE_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmTitle_Record.F_TITLE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Title ( ID )'
            FocusControl = cxdbseF_TITLE_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_TITLE_ID: TFVBFFCDBSpinEdit
            Left = 88
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmTitle_Record.F_TITLE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_TITLE_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmTitle_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Title ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 88
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmTitle_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 400
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmTitle_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Title ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 88
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmTitle_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 400
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmTitle.cdsList
  end
end
