inherited frmCountryPart_List: TfrmCountryPart_List
  Caption = 'Landsgedeeltes'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_COUNTRYPART_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRYPART_ID'
          Width = 128
        end
        object cxgrdtblvListF_COUNTRY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_NAME'
          Width = 200
        end
        object cxgrdtblvListF_COUNTRY_PART_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_PART_NAME'
          Width = 200
        end
        object cxgrdtblvListF_COUNTRY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_ID'
          Visible = False
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmCountryPart.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmCountryPart.cdsSearchCriteria
  end
end
