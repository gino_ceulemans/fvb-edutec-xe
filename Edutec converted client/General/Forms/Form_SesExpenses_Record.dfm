inherited frmSesExpenses_Record: TfrmSesExpenses_Record
  Left = 23
  Top = 145
  ActiveControl = cxdbicbF_EXPENSE_TYPE
  Caption = 'Uitgaven'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_SESSION_NAME1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSesExpenses_Record.F_SESSION_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Sessie'
          FocusControl = cxdblblF_SESSION_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SESSION_NAME: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSesExpenses_Record.F_SESSION_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_SESSION_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 240
        end
        object cxlblF_EXPENSE_TYPE1: TFVBFFCLabel
          Left = 264
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_TYPE'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Oorsprong'
          FocusControl = cxdblblF_EXPENSE_TYPE
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_EXPENSE_TYPE: TFVBFFCDBLabel
          Left = 264
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_TYPE'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_EXPENSE_TYPE'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 240
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_EXPENSE_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Uitgave ( ID )'
            FocusControl = cxdbseF_EXPENSE_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_EXPENSE_ID: TFVBFFCDBSpinEdit
            Left = 104
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_EXPENSE_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 121
          end
          object cxlblF_SESSION_NAME2: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_SESSION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie'
            FocusControl = cxdbbeF_SESSION_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_SESSION_NAME: TFVBFFCDBButtonEdit
            Left = 104
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_SESSION_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_SESSION_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            TabOrder = 3
            Width = 400
          end
          object cxlblF_EXPENSE_TYPE2: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_TYPE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Oorsprong'
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_EXPENSE_AMOUNT1: TFVBFFCLabel
            Left = 8
            Top = 208
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_AMOUNT'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Bedrag'
            FocusControl = cxdbcueF_EXPENSE_AMOUNT
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcueF_EXPENSE_AMOUNT: TFVBFFCDBCurrencyEdit
            Left = 104
            Top = 208
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_AMOUNT'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_EXPENSE_AMOUNT'
            DataBinding.DataSource = srcMain
            TabOrder = 8
            Width = 121
          end
          object cxdbicbF_EXPENSE_TYPE: TFVBFFCDBImageComboBox
            Left = 104
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_TYPE'
            RepositoryItem = dtmEDUMainClient.cxeriExpenseOrigin
            DataBinding.DataField = 'F_EXPENSE_TYPE'
            DataBinding.DataSource = srcMain
            Properties.Items = <>
            TabOrder = 5
            Width = 401
          end
          object cxlblF_EXPENSE_DESC1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Omschrijving'
            FocusControl = cxdbmmoF_EXPENSE_DESC
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_EXPENSE_DESC: TFVBFFCDBMemo
            Left = 104
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_DESC'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_EXPENSE_DESC'
            DataBinding.DataSource = srcMain
            TabOrder = 7
            Height = 97
            Width = 401
          end
          object FVBFFCLabel1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Datum'
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_EXPENSE_DATE: TFVBFFCDBDateEdit
            Left = 104
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmSesExpenses_Record.F_EXPENSE_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_EXPENSE_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 11
            Width = 137
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
end
