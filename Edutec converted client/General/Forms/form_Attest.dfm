object frmAttest: TfrmAttest
  Left = 566
  Top = 391
  BorderStyle = bsDialog
  Caption = 'Attesteer'
  ClientHeight = 200
  ClientWidth = 401
  Color = clInactiveCaption
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object FVBFFCPanel1: TFVBFFCPanel
    Left = 0
    Top = 0
    Width = 401
    Height = 168
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 4
    TabOrder = 0
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clWindow
    StyleClientArea.BackColor2 = 15254445
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object bvlBevel1: TBevel
      Left = 16
      Top = 96
      Width = 361
      Height = 65
      Shape = bsTopLine
    end
    object cxlblF_HOURS_PRESENT1: TFVBFFCLabel
      Left = 24
      Top = 21
      HelpType = htKeyword
      HelpKeyword = 'frmEnrolment_Record.F_HOURS_PRESENT'
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Uren Aanwezig'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
    object cxlblF_HOURS_ABSENT_JUSTIFIED1: TFVBFFCLabel
      Left = 24
      Top = 45
      HelpType = htKeyword
      HelpKeyword = 'frmEnrolment_Record.F_HOURS_ABSENT_JUSTIFIED'
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Uren Afwezig ( Gewettigd )'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
    object cxlblF_HOURS_ABSENT_ILLEGIT1: TFVBFFCLabel
      Left = 24
      Top = 69
      HelpType = htKeyword
      HelpKeyword = 'frmEnrolment_Record.F_HOURS_ABSENT_ILLEGIT'
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Uren Afwezig ( Ongewettigd )'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
    object cxseF_HOURS_PRESENT: TFVBFFCSpinEdit
      Left = 240
      Top = 19
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      TabOrder = 0
      Width = 121
    end
    object cxseF_HOURS_ABSENT_JUSTIFIED: TFVBFFCSpinEdit
      Left = 240
      Top = 43
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      TabOrder = 1
      Width = 121
    end
    object cxseF_HOURS_ABSENT_ILLEGIT: TFVBFFCSpinEdit
      Left = 240
      Top = 66
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      TabOrder = 2
      Width = 121
    end
    object cxlblFVBFFCLabel1: TFVBFFCLabel
      Left = 24
      Top = 133
      HelpType = htKeyword
      HelpKeyword = 'frmEnrolment_Record.F_HOURS_PRESENT'
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Totaal Aantal Uren Voor Deze Sessie'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
    object seTotal: TFVBFFCSpinEdit
      Left = 240
      Top = 131
      TabStop = False
      ParentFont = False
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescReadOnly
      TabOrder = 4
      Width = 121
    end
    object seSUM: TFVBFFCSpinEdit
      Left = 240
      Top = 107
      TabStop = False
      ParentFont = False
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescReadOnly
      TabOrder = 3
      Width = 121
    end
    object cxlblFVBFFCLabel2: TFVBFFCLabel
      Left = 24
      Top = 109
      HelpType = htKeyword
      HelpKeyword = 'frmEnrolment_Record.F_HOURS_PRESENT'
      RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
      Caption = 'Totaal Aantal Geattesteerde Uren'
      ParentColor = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      Transparent = True
    end
  end
  object pnlBottom: TFVBFFCPanel
    Left = 0
    Top = 168
    Width = 401
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 0
    ParentColor = True
    TabOrder = 1
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object pnlBottomButtons: TFVBFFCPanel
      Left = 65
      Top = 0
      Width = 336
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      BorderWidth = 0
      ParentColor = True
      TabOrder = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object cxbtnOK: TFVBFFCButton
        Left = 113
        Top = 4
        Width = 104
        Height = 25
        Action = acOk
        Cancel = True
        Default = True
        ModalResult = 1
        TabOrder = 0
      end
      object cxbtnCancel: TFVBFFCButton
        Left = 225
        Top = 2
        Width = 104
        Height = 25
        Action = acCancel
        Cancel = True
        Default = True
        ModalResult = 2
        TabOrder = 1
      end
    end
  end
  object alButtons: TFVBFFCActionList
    Left = 192
    Top = 16
    object acOk: TAction
      Caption = '&Ok'
      OnExecute = acOkExecute
      OnUpdate = acOkUpdate
    end
    object acCancel: TAction
      Caption = '&Cancel'
      OnExecute = acCancelExecute
    end
  end
end
