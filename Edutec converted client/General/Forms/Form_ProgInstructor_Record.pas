{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_ProgInstructor_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_ProgInstructor_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons, cxDropDownEdit, cxCalendar, cxDBEdit, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, Unit_PPWFrameWorkInterfaces, Menus, cxSplitter;

type
  TfrmProgInstructor_Record = class(TEDURecordView)
    cxlblF_PROG_INSTRUCTOR_ID1: TFVBFFCLabel;
    cxdblblF_PROG_INSTRUCTOR_ID1: TFVBFFCDBLabel;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdblblF_LASTNAME: TFVBFFCDBLabel;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxlblF_PROG_INSTRUCTOR_ID2: TFVBFFCLabel;
    cxdbseF_PROG_INSTRUCTOR_ID: TFVBFFCDBSpinEdit;
    cxlblF_INSTRUCTOR_ID1: TFVBFFCLabel;
    cxdbbeF_FULLNAME: TFVBFFCDBButtonEdit;
    cxlblF_SOCSEC_NR1: TFVBFFCLabel;
    cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit;
    cxlblF_PHONE1: TFVBFFCLabel;
    cxdbteF_PHONE: TFVBFFCDBTextEdit;
    cxlblF_FAX1: TFVBFFCLabel;
    cxdbteF_FAX: TFVBFFCDBTextEdit;
    cxlblF_GSM1: TFVBFFCLabel;
    cxdbteF_GSM: TFVBFFCDBTextEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    cxdbteF_EMAIL: TFVBFFCDBTextEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbbeF_NAME: TFVBFFCDBButtonEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE: TFVBFFCDBDateEdit;
    procedure cxdbbeF_FULLNAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ProgInstructor, Data_EduMainClient, Unit_FVBFFCInterfaces,
  Unit_FVBFFCRecordView, Form_FVBFFCBaseRecordView, unit_EdutecInterfaces;

{ TfrmProgInstructor_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmProgInstructor_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmProgInstructor_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption + '-' + cxdblblF_LASTNAME.Caption;
end;

procedure TfrmProgInstructor_Record.cxdbbeF_FULLNAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aProgInstructorDataModule : IEDUProgInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUProgInstructorDataModule, aProgInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aProgInstructorDataModule.ShowInstructor( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aProgInstructorDataModule.SelectInstructor( srcMain.DataSet );
        end;
      end;
    end;
  end;
end;

procedure TfrmProgInstructor_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  { Check if the DataModule was opened as a Detail of another DataModule }
  if ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUProgProgramDataModule ) ) then
    begin
      cxdbbeF_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl( cxdbbeF_FULLNAME );
    end
    else if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUDocInstructorDataModule ) ) then
    begin
      cxdbbeF_FULLNAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl( cxdbbeF_NAME );
    end;
  end;
end;

procedure TfrmProgInstructor_Record.cxdbbeF_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUProgInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUProgInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowProgram( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aInstructorDataModule.SelectProgram( srcMain.DataSet );
        end;
      end;
    end;
  end;
end;

end.
