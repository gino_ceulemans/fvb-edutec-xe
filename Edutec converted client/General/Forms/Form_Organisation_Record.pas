{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <T_OR_ORGANISATION> records.


  @Name       Form_Organisation_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Organisation_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxCurrencyEdit, cxDBEdit,
  cxDropDownEdit, cxImageComboBox, cxCalendar, cxCheckBox, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, cxMemo, Menus, cxGraphics, cxSplitter, cxGroupBox,
  cxHyperLinkEdit;

const
  cUnselectedAdminCostType = -1;

type
  TfrmOrganisation_Record = class(TEDURecordView)
    cxlblF_ORGANISATION_ID: TFVBFFCLabel;
    cxdblblF_ORGANISATION_ID: TFVBFFCDBLabel;
    cxlblF_NAME: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxgbAddress: TFVBFFCGroupBox;
    cxgbInvoiceAddress: TFVBFFCGroupBox;
    cxgbGeneral: TFVBFFCGroupBox;
    cxlblF_ORGANISATION_ID1: TFVBFFCLabel;
    cxdbseF_ORGANISATION_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdbteF_NAME1: TFVBFFCDBTextEdit;
    cxlblF_RSZ_NR1: TFVBFFCLabel;
    F_RSZ_NR1: TFVBFFCDBMaskEdit;
    cxlblF_VAT_NR1: TFVBFFCLabel;
    F_VAT_NR1: TFVBFFCDBMaskEdit;
    cxlblF_PHONE1: TFVBFFCLabel;
    cxdbteF_PHONE1: TFVBFFCDBTextEdit;
    cxlblF_FAX1: TFVBFFCLabel;
    cxdbteF_FAX1: TFVBFFCDBTextEdit;
    cxlblF_WEBSITE1: TFVBFFCLabel;
    F_WEBSITE1: TFVBFFCDBHyperLinkEdit;
    cxlblF_ACC_NR1: TFVBFFCLabel;
    F_ACC_NR1: TFVBFFCDBMaskEdit;
    cxlblF_ACC_HOLDER1: TFVBFFCLabel;
    cxdbteF_ACC_HOLDER1: TFVBFFCDBTextEdit;
    cxlblF_ORGTYPE_NAME1: TFVBFFCLabel;
    cxdbbeF_ORGTYPE_NAME1: TFVBFFCDBButtonEdit;
    cxbtnCopyToInvoice: TFVBFFCButton;
    cxlblF_INVOICE_STREET1: TFVBFFCLabel;
    cxlblF_INVOICE_NUMBER1: TFVBFFCLabel;
    cxlblF_INVOICE_POSTALCODE1: TFVBFFCLabel;
    cxlblF_INVOICE_CITY_NAME1: TFVBFFCLabel;
    cxlblF_INVOICE_COUNTRY_NAME1: TFVBFFCLabel;
    cxdbbeF_INVOICE_COUNTRY_NAME: TFVBFFCDBButtonEdit;
    cxdbteF_INVOICE_CITY_NAME: TFVBFFCDBTextEdit;
    cxdbbeF_INVOICE_POSTALCODE: TFVBFFCDBButtonEdit;
    cxdbteF_INVOICE_NUMBER: TFVBFFCDBTextEdit;
    cxdbteF_INVOICE_STREET: TFVBFFCDBTextEdit;
    cxlblF_INVOICE_MAILBOX1: TFVBFFCLabel;
    cxdbteF_INVOICE_MAILBOX: TFVBFFCDBTextEdit;
    cxlblF_STREET1: TFVBFFCLabel;
    cxdbteF_STREET1: TFVBFFCDBTextEdit;
    cxlblF_NUMBER1: TFVBFFCLabel;
    cxdbteF_NUMBER1: TFVBFFCDBTextEdit;
    cxlblF_MAILBOX1: TFVBFFCLabel;
    cxdbteF_MAILBOX1: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxdbbeF_POSTALCODE1: TFVBFFCDBButtonEdit;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdbteF_CITY_NAME1: TFVBFFCDBTextEdit;
    cxlblF_COUNTRY_NAME1: TFVBFFCLabel;
    cxdbbeF_COUNTRY_NAME1: TFVBFFCDBButtonEdit;
    acShowRoles: TAction;
    dxnbiRoles: TdxNavBarItem;
    dxnbiShowLogHistory: TdxNavBarItem;
    acShowHistory: TAction;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxdbteF_CONSTRUCT_ID: TFVBFFCDBTextEdit;
    cxdbcbF_MANUALLY_ADDED: TFVBFFCDBCheckBox;
    cxlblF_MANUALLY_ADDED: TFVBFFCLabel;
    acShowSessions: TAction;
    dxnbiShowSessions: TdxNavBarItem;
    FVBFFCLabel1: TFVBFFCLabel;
    FVBFFCDBMemo1: TFVBFFCDBMemo;
    FVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel;
    F_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel;
    F_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit;
    FVBFFCLabel2: TFVBFFCLabel;
    cxdbteF_EXTRA_INFO: TFVBFFCDBTextEdit;
    FVBFFCLabel3: TFVBFFCLabel;
    cxdbteF_INVOICE_EXTRA_INFO: TFVBFFCDBTextEdit;
    acShowInstructors: TAction;
    dxnbNavShowInstructors: TdxNavBarItem;
    cxlbF_CONFIRMATION_CONTACT3: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT3: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCLabel;
    F_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    F_EMAIL1: TFVBFFCDBHyperLinkEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_MEDIUM_NAME1: TFVBFFCLabel;
    cxdbbeF_MEDIUM_NAME1: TFVBFFCDBButtonEdit;
    FVBFFCLabel4: TFVBFFCLabel;
    cxdbteF_SYNERGY_ID: TFVBFFCDBTextEdit;
    acShowKMO_Portefeuille: TAction;
    dxnbiShowKMO_Portefeuille: TdxNavBarItem;
    cxlblF_PARTNER_WINTER: TFVBFFCLabel;
    cxdbcbF_PARTNER_WINTER: TFVBFFCDBCheckBox;
    FVBFFCLabel5: TFVBFFCLabel;
    F_AUTHORIZATION_NR: TFVBFFCDBTextEdit;
    cxlblF_ADMIN_COST_TYPE: TFVBFFCLabel;
    cxlblF_ADMIN_COST: TFVBFFCLabel;
    F_ADMIN_COST: TFVBFFCDBCurrencyEdit;
    cboAdminCostType: TFVBFFCComboBox;
    procedure cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_MEDIUM_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_ORGTYPE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_POSTALCODE1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_COUNTRY_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxbtnCopyToInvoiceClick(Sender: TObject);
    procedure acShowRolesExecute(Sender: TObject);
    procedure acShowHistoryExecute(Sender: TObject);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);

    procedure acShowSessionsExecute(Sender: TObject);
    procedure acShowKMO_PortefeuilleExecute(Sender: TObject);
    procedure srcMainDataChange(Sender: TObject; Field: TField);

    procedure srcMainStateChange(Sender: TObject);
    procedure cboAdminCostTypePropertiesEditValueChanged(Sender: TObject);
    procedure cxdbcbF_PARTNER_WINTERPropertiesEditValueChanged(
      Sender: TObject);
    procedure FVBFFCRecordViewShow(Sender: TObject);
  private
    FIsInitialized : Boolean;
    property IsInitialized: Boolean read FIsInitialized write FIsInitialized;
    procedure FillAdminCostType;
    procedure UpdateUIStatePartnerWinter(init: Boolean);
    procedure UpdateUIStateAdminCost;
    procedure UpdateAdminCostType;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Organisation, Data_EduMainClient,  unit_EdutecInterfaces;

{ TfrmOrganisation_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmOrganisation_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmOrganisation_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption;
end;

procedure TfrmOrganisation_Record.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectLanguage( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmOrganisation_Record.cxdbbeF_MEDIUM_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowMedium( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectMedium( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmOrganisation_Record.cxdbbeF_ORGTYPE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectOrgType( srcMain.DataSet );
  end;
end;

procedure TfrmOrganisation_Record.cxdbbeF_POSTALCODE1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowPostalCode( srcMain.DataSet, ( Sender = cxdbbeF_INVOICE_POSTALCODE ), rvmEdit );
      end;
      else aDataModule.SelectPostalCode( srcMain.DataSet, ( Sender = cxdbbeF_INVOICE_POSTALCODE ) );
    end;
  end;
end;

procedure TfrmOrganisation_Record.cxdbbeF_COUNTRY_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowCountry( srcMain.DataSet, ( Sender = cxdbbeF_INVOICE_COUNTRY_NAME ), rvmEdit );
      end;
      else aDataModule.SelectCountry( srcMain.DataSet, ( Sender = cxdbbeF_INVOICE_COUNTRY_NAME ) );
    end;
  end;
end;

procedure TfrmOrganisation_Record.cxbtnCopyToInvoiceClick(Sender: TObject);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    aDataModule.CopyAddressToInvoiceAddress( srcMain.DataSet );
  end;
end;

procedure TfrmOrganisation_Record.acShowRolesExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmRole', pnlRecordDetail );
end;

procedure TfrmOrganisation_Record.acShowHistoryExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmLogHistory', pnlRecordDetail );
end;

procedure TfrmOrganisation_Record.acShowSessionsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmSesSession', pnlRecordDetail );
end;

procedure TfrmOrganisation_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcbF_MANUALLY_ADDED );

  IsInitialized := True;
end;

procedure TfrmOrganisation_Record.acShowKMO_PortefeuilleExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmKMO_Portefeuille', pnlRecordDetail );
end;

procedure TfrmOrganisation_Record.srcMainDataChange(Sender: TObject;
  Field: TField);
const
  cOrgType_ID = 'F_ORGTYPE_ID';
  cAdminCostType = 'F_ADMIN_COST_TYPE';
  cPartnerWinter = 'F_PARTNER_WINTER';
begin
  inherited;

  if not IsInitialized then
  begin
    exit;
  end;

  if Field <> nil then
  begin
    if Field.FieldName = cOrgType_ID then
    begin
      UpdateUIStatePartnerWinter(False);
    end;
    if (Field.FieldName = cOrgType_ID) or (Field.FieldName = cAdminCostType) then
    begin
      UpdateUIStateAdminCost;
    end;
    if (Field.FieldName = cPartnerWinter) then
    begin
      UpdateAdminCostType;
    end;
  end;
end;

{ Update partner winter related ui state }
procedure TfrmOrganisation_Record.UpdateUIStatePartnerWinter(init: Boolean);
var
  aDataModule: IEDUOrganisationDataModule;
  isWritable: Boolean;
begin
  inherited;

  isWritable := False;

  if (Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule )) then
  begin
    if init then
    begin
      isWritable := aDataModule.IsEducationCenter(srcMain.DataSet);
    end else
    begin
      if ( Mode in [ rvmEdit, rvmAdd ] ) then
      begin
        isWritable := not aDataModule.ClearPartnerWinter(srcMain.DataSet);
      end;
    end;
  end;
  cxdbcbF_PARTNER_WINTER.Enabled := isWritable;
  F_AUTHORIZATION_NR.Enabled := isWritable;

  if not isWritable then
  begin
    cboAdminCostType.ItemIndex := cUnselectedAdminCostType; // Unselect
  end;
end;

{ Update admin cost related ui state }
procedure TfrmOrganisation_Record.UpdateUIStateAdminCost;
var
  aDataModule: IEDUOrganisationDataModule;
  isWritable: Boolean;
begin
  isWritable := cxdbcbF_PARTNER_WINTER.Checked;

  F_ADMIN_COST.Enabled := isWritable;
  cboAdminCostType.Enabled := isWritable;
  F_AUTHORIZATION_NR.Enabled := isWritable;

  if not self.IsInitialized then
  begin
    exit;
  end;
  if not isWritable then
  begin
    if srcMain.DataSet.State in [dsInsert, dsEdit] then
    begin
      if (Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule )) then
      begin
        aDataModule.ClearAuthorizationNr(srcMain.DataSet);
      end;
    end;
  end;
end;

procedure TfrmOrganisation_Record.srcMainStateChange(Sender: TObject);
begin
  inherited;

  if not IsInitialized then
  begin
    exit;
  end;

  if srcMain.DataSet <> nil then
  begin
    if srcMain.DataSet.State in [dsBrowse] then
    begin
      UpdateUIStatePartnerWinter(True);
      UpdateUIStateAdminCost;
    end;
  end;
end;

procedure TfrmOrganisation_Record.UpdateAdminCostType;
const
  cUnselectedAdminCostType = -1;
var
  aDataModule: IEDUOrganisationDataModule;
  i: Integer;
begin
  if (Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule )) then
  begin
    if (Length(self.cboAdminCostType.Text) > 0) and (cxdbcbF_PARTNER_WINTER.Checked) then
    begin
      i := self.cboAdminCostType.ItemIndex;
    end else
    begin
      i := cUnselectedAdminCostType;
    end;
    aDataModule.UpdateAdminCostType(srcMain.DataSet, i);
  end;
end;

procedure TfrmOrganisation_Record.cboAdminCostTypePropertiesEditValueChanged(
  Sender: TObject);
begin
  UpdateAdminCostType;
  inherited;
end;

procedure TfrmOrganisation_Record.cxdbcbF_PARTNER_WINTERPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  UpdateUIStateAdminCost;
end;

procedure TfrmOrganisation_Record.FillAdminCostType;
var
  aDataModule: IEDUOrganisationDataModule;
  idx: Integer;
begin
  if not self.IsInitialized then
  begin
    exit;
  end;
  idx := cUnselectedAdminCostType;
  if (Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule )) then
  begin
    idx := aDataModule.GetAdminCostTypeItemIndex(srcMain.DataSet);
  end;
  self.cboAdminCostType.ItemIndex := idx;
end;

procedure TfrmOrganisation_Record.FVBFFCRecordViewShow(Sender: TObject);
begin
  inherited;
  FillAdminCostType;
  UpdateUIStatePartnerWinter(True);
  UpdateUIStateAdminCost;
end;

end.
