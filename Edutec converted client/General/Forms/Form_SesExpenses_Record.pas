{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SES_EXPENSES records.


  @Name       Form_SesExpenses_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  02/08/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_SesExpenses_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxMemo, cxDBEdit,
  cxDropDownEdit, cxImageComboBox, cxCurrencyEdit, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, Menus, cxGraphics, cxSplitter, cxCalendar;

type
  TfrmSesExpenses_Record = class(TEDURecordView)
    cxlblF_SESSION_NAME1: TFVBFFCLabel;
    cxdblblF_SESSION_NAME: TFVBFFCDBLabel;
    cxlblF_EXPENSE_TYPE1: TFVBFFCLabel;
    cxdblblF_EXPENSE_TYPE: TFVBFFCDBLabel;
    cxlblF_EXPENSE_ID1: TFVBFFCLabel;
    cxdbseF_EXPENSE_ID: TFVBFFCDBSpinEdit;
    cxlblF_SESSION_NAME2: TFVBFFCLabel;
    cxdbbeF_SESSION_NAME: TFVBFFCDBButtonEdit;
    cxlblF_EXPENSE_TYPE2: TFVBFFCLabel;
    cxlblF_EXPENSE_AMOUNT1: TFVBFFCLabel;
    cxdbcueF_EXPENSE_AMOUNT: TFVBFFCDBCurrencyEdit;
    cxdbicbF_EXPENSE_TYPE: TFVBFFCDBImageComboBox;
    cxlblF_EXPENSE_DESC1: TFVBFFCLabel;
    cxdbmmoF_EXPENSE_DESC: TFVBFFCDBMemo;
    FVBFFCLabel1: TFVBFFCLabel;
    cxdbdeF_EXPENSE_DATE: TFVBFFCDBDateEdit;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SesExpenses, Data_EduMainClient, unit_EdutecInterfaces;

{ TfrmSesExpenses_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmSesExpenses_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmSesExpenses_Record.GetDetailName: String;
begin
  Result := 'Uitgaven ' + cxdblblF_SESSION_NAME.Caption;
end;

procedure TfrmSesExpenses_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUSessionDataModule ) ) then
    begin
      cxdbbeF_SESSION_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbicbF_EXPENSE_TYPE );
    end;
  end;
end;

end.
