{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Status_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Status_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  Menus;

type
  TfrmStatus_List = class(TEduListView)
    cxgrdtblvListF_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME_NL: TcxGridDBColumn;
    cxgrdtblvListF_NAME_FR: TcxGridDBColumn;
    cxgrdtblvListF_LONG_DESC_NL: TcxGridDBColumn;
    cxgrdtblvListF_LONG_DESC_FR: TcxGridDBColumn;
    cxgrdtblvListF_SESSION: TcxGridDBColumn;
    cxgrdtblvListF_ENROL: TcxGridDBColumn;
    cxgrdtblvListF_WAITING_LIST: TcxGridDBColumn;
    cxgrdtblvListF_FOREGROUNDCOLOR: TcxGridDBColumn;
    cxgrdtblvListF_BACKGROUNDCOLOR: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListF_LONG_DESC: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Status, Data_EduMainClient;

end.