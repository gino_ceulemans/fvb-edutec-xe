inherited frmPerson_List: TfrmPerson_List
  Left = 361
  Top = 224
  ActiveControl = cxdbteF_LASTNAME1
  Caption = 'Personen'
  ClientWidth = 784
  Constraints.MinWidth = 800
  ExplicitWidth = 800
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 784
    ExplicitWidth = 784
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 576
      ExplicitLeft = 576
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 688
      ExplicitLeft = 688
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 776
    ExplicitWidth = 776
    inherited cxgrdList: TcxGrid
      Top = 166
      Width = 776
      Height = 274
      ExplicitTop = 166
      ExplicitWidth = 776
      ExplicitHeight = 274
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.IncSearch = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Inactive = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        Styles.Selection = nil
        object cxgrdtblvListF_SYNERGY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SYNERGY_ID'
        end
        object cxgrdtblvListF_PERSON_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PERSON_ID'
          Width = 103
        end
        object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_SOCSEC_NR'
        end
        object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LASTNAME'
        end
        object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_FIRSTNAME'
        end
        object cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE_ID'
          Visible = False
        end
        object cxgrdtblvListF_POSTALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE'
        end
        object cxgrdtblvListF_CITY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_NAME'
        end
        object cxgrdtblvListF_COUNTRY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_ID'
          Visible = False
        end
        object cxgrdtblvListF_COUNTRY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_NAME'
        end
        object cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_ID'
          Visible = False
        end
        object cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_NAME'
        end
        object cxgrdtblvListF_MEDIUM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_MEDIUM_ID'
          Visible = False
        end
        object cxgrdtblvListF_MEDIUM_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_MEDIUM_NAME'
          Width = 245
        end
        object cxgrdtblvListF_ROLE: TcxGridDBColumn
          Caption = 'Rol'
          DataBinding.FieldName = 'F_ROLE'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          MinWidth = 35
        end
        object cxgrdtblvListF_MANUALLY_ADDED: TcxGridDBColumn
          DataBinding.FieldName = 'F_MANUALLY_ADDED'
          Width = 81
        end
        object cxgrdtblvListF_CONSTRUCT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_CONSTRUCT_ID'
          Width = 92
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 141
      Width = 776
      ExplicitTop = 141
      ExplicitWidth = 776
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 137
      Width = 776
      ExplicitTop = 137
      ExplicitWidth = 776
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Top = 0
      Width = 776
      Height = 137
      ExpandedHeight = 137
      ExplicitTop = 0
      ExplicitWidth = 776
      ExplicitHeight = 137
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 106
        Width = 774
        ExplicitTop = 106
        ExplicitWidth = 774
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 606
          ExplicitLeft = 606
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 694
          ExplicitLeft = 694
        end
      end
      object cxlblF_LASTNAME2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam'
        FocusControl = cxdbteF_LASTNAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_LASTNAME1: TFVBFFCDBTextEdit
        Left = 80
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_LASTNAME'
        DataBinding.DataField = 'F_LASTNAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 2
        Width = 216
      end
      object cxlblF_FIRSTNAME2: TFVBFFCLabel
        Left = 304
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Voornaam'
        FocusControl = cxdbteF_FIRSTNAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit
        Left = 376
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        DataBinding.DataField = 'F_FIRSTNAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 4
        Width = 216
      end
      object cxlblPostalCode: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Postcode'
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbtePostalCode: TFVBFFCDBTextEdit
        Left = 80
        Top = 80
        DataBinding.DataField = 'F_POSTALCODE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 9
        Width = 216
      end
      object cxlblCity: TFVBFFCLabel
        Left = 304
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Gemeente'
        FocusControl = cxdbteCity
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteCity: TFVBFFCDBTextEdit
        Left = 376
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        DataBinding.DataField = 'F_CITY_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 12
        Width = 216
      end
      object cxlblSocSecNr: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Rijksr. Nr'
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object SocSecNr: TFVBFFCDBMaskEdit
        Left = 80
        Top = 56
        DataBinding.DataField = 'F_SOCSEC_NR'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 5
        Width = 216
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 304
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        FocusControl = cxdbteF_FIRSTNAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 376
        Top = 56
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Items = <>
        TabOrder = 7
        Width = 216
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 784
    ExplicitWidth = 784
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 784
    ExplicitWidth = 784
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 780
    ExplicitLeft = 780
  end
  inherited srcMain: TFVBFFCDataSource
    AutoEdit = False
    DataSet = dtmPerson.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    object dxbbJoinPersons: TdxBarButton
      Action = acJoinPersons
      Category = 0
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxbbEdit'
      end
      item
        Visible = True
        ItemName = 'dxbbView'
      end
      item
        Visible = True
        ItemName = 'dxbbAdd'
      end
      item
        Visible = True
        ItemName = 'dxbbDelete'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbRefresh'
      end
      item
        Visible = True
        ItemName = 'dxBBCustomizeColumns'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbJoinPersons'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbPrintGrid'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbExportXLS'
      end
      item
        Visible = True
        ItemName = 'dxbbExportXML'
      end
      item
        Visible = True
        ItemName = 'dxbbExportHTML'
      end>
    OnPopup = dxbpmnGridPopup
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    Left = 184
    Top = 398
  end
  object alPersonList: TActionList
    Left = 300
    Top = 396
    object acJoinPersons: TAction
      Caption = 'Personen samenvoegen...'
      OnExecute = acJoinPersonsExecute
    end
  end
end
