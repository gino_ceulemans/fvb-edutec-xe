{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_DOC_DIPLOMA record.

  @Name       Form_Diploma_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Diploma_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmDiploma_Record = class(TEDURecordView)
    cxlblF_DIPLOMA_ID1: TFVBFFCLabel;
    cxdblblF_DIPLOMA_ID: TFVBFFCDBLabel;
    cxlblF_DIPLOMA_NAME1: TFVBFFCLabel;
    cxdblblF_DIPLOMA_NAME: TFVBFFCDBLabel;
    cxlblF_DIPLOMA_ID2: TFVBFFCLabel;
    cxdbseF_DIPLOMA_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  protected
    function GetDetailName : String; override;
  public
    { Public declarations }
  end;

var
  frmDiploma_Record: TfrmDiploma_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Diploma, Data_EDUMainClient;

{ TfrmDiploma_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmDiploma_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmDiploma_Record.GetDetailName: String;
begin
  Result := cxdblblF_DIPLOMA_NAME.Caption;
end;

end.