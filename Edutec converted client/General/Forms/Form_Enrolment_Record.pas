{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SES_ENROL records.


  @Name       Form_Enrolment_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  10/11/2005   sLesage              Changes for Attestation.
  28/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Enrolment_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxCurrencyEdit, cxDBEdit,
  cxDropDownEdit, cxImageComboBox, cxCalendar, cxCheckBox, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, cxMemo, Menus, cxGraphics, cxSplitter, cxGroupBox;

type
  TfrmEnrolment_Record = class(TEDURecordView)
    cxlblF_ENROL_ID1: TFVBFFCLabel;
    cxdblblF_ENROL_ID: TFVBFFCDBLabel;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdblblF_LASTNAME: TFVBFFCDBLabel;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdblblF_FIRSTNAME: TFVBFFCDBLabel;
    cxlblF_ENROL_ID: TFVBFFCLabel;
    cxdbseF_ENROL_ID: TFVBFFCDBSpinEdit;
    cxlblF_SESSION_NAME: TFVBFFCLabel;
    cxdbbeF_SESSION_NAME: TFVBFFCDBButtonEdit;
    cxlblF_LASTNAME: TFVBFFCLabel;
    cxdbbeF_LASTNAME: TFVBFFCDBButtonEdit;
    cxlblF_FIRSTNAME: TFVBFFCLabel;
    cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit;
    cxlblF_SOCSEC_NR: TFVBFFCLabel;
    cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit;
    cxlblF_STATUS_NAME: TFVBFFCLabel;
    cxdbbeF_STATUS_NAME: TFVBFFCDBButtonEdit;
    cxlblF_EVALUATION: TFVBFFCLabel;
    cxdbcbF_EVALUATION: TFVBFFCDBCheckBox;
    cxlblF_CERTIFICATE: TFVBFFCLabel;
    cxdbcbF_CERTIFICATE: TFVBFFCDBCheckBox;
    cxlblF_ENROLMENT_DATE: TFVBFFCLabel;
    cxdbdeF_ENROLMENT_DATE: TFVBFFCDBDateEdit;
    cxlblF_INVOICED: TFVBFFCLabel;
    cxdbcbF_INVOICED: TFVBFFCDBCheckBox;
    cxlblF_LAST_MINUTE: TFVBFFCLabel;
    cxdbcbF_LAST_MINUTE: TFVBFFCDBCheckBox;
    pnlPriceSchemeInfoSeperator: TFVBFFCPanel;
    cxlblF_COMMENT: TFVBFFCLabel;
    cxdbmmoF_COMMENT: TFVBFFCDBMemo;
    dxnbiShowLogHistory: TdxNavBarItem;
    acShowLogHistory: TAction;
    cxgbFVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlblF_PRICE_WORKER: TFVBFFCLabel;
    cxdbcueF_PRICE_WORKER: TFVBFFCDBCurrencyEdit;
    cxdbcueF_PRICE_CLERK: TFVBFFCDBCurrencyEdit;
    cxgbFVBFFCGroupBox2: TFVBFFCGroupBox;
    cxlblF_HOURS_ABSENT_ILLEGIT1: TFVBFFCLabel;
    cxdbseF_HOURS_ABSENT_ILLEGIT1: TFVBFFCDBSpinEdit;
    cxdbseF_HOURS_ABSENT_JUSTIFIED1: TFVBFFCDBSpinEdit;
    cxdbseF_HOURS_PRESENT1: TFVBFFCDBSpinEdit;
    cxlblF_HOURS_PRESENT1: TFVBFFCLabel;
    cxlblF_HOURS_ABSENT_JUSTIFIED1: TFVBFFCLabel;
    cxbtnF_CORRECT_PRICE: TFVBFFCButton;
    cxdbseF_HOURS1: TFVBFFCDBSpinEdit;
    cxlblF_HOURS1: TFVBFFCLabel;
    cxlblF_NAME: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxdbcueF_PRICE_OTHER: TFVBFFCDBCurrencyEdit;
    cxdbicbF_PRICE_REDUCTION: TFVBFFCDBImageComboBox;
    cxlblF_PRICE_REDUCTION: TFVBFFCLabel;
    cxlblCertificatePrintDate: TFVBFFCLabel;
    csdbedtCertificatePrintDate: TFVBFFCDBTextEdit;
    cxdbbef_PARTNERWINTER: TFVBFFCDBButtonEdit;
    FVBFFCLabel1: TFVBFFCLabel;
    cxlblF_SCORE: TFVBFFCLabel;
    cxdbseF_SCORE: TFVBFFCDBSpinEdit;
    cxdbseF_MAX_SCORE: TFVBFFCDBSpinEdit;
    FVBFFCLabel2: TFVBFFCLabel;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_SESSION_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_LASTNAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_STATUS_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acShowLogHistoryExecute(Sender: TObject);
    procedure cxbtnF_CORRECT_PRICEClick(Sender: TObject);
    procedure cxdbicbF_PRICE_REDUCTIONPropertiesChange(Sender: TObject);
    procedure FVBFFCRecordViewAllDetailsCreated(Sender: TObject);
    procedure srcMainDataChange(Sender: TObject; Field: TField);
    procedure FVBFFCRecordViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure cxdbbef_PARTNER_WINTERPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
    procedure CheckPricing;
    procedure UpdateStatusLookupbutton(const AEnabled: Boolean);
  public
    { Public declarations }
    function GetDetailName : String; override;
    procedure SetPrices;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Enrolment, Data_EduMainClient, unit_EdutecInterfaces,
  Unit_PPWFrameWorkRecordView;

{ TfrmEnrolment_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmEnrolment_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmEnrolment_Record.GetDetailName: String;
begin
  Result := cxdblblF_LASTNAME.Caption + ', ' + cxdblblF_FIRSTNAME.Caption;
end;

{*****************************************************************************
  This event will be executed when the Form is initialised and will be used
  to Enable or Disable some controls.

  @Name       TfrmEnrolment_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUSessionDataModule ) ) then
    begin
//      cxdbbeF_SESSION_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbbeF_LASTNAME );
    end;
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUPersonDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_LASTNAME );
    end;
  end;
  // Zet volgende velden steeds af zodat deze niet standaard gewijzigd kunnen worden
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbseF_HOURS1 );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbteF_NAME );

  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcueF_PRICE_WORKER );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcueF_PRICE_CLERK );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcueF_PRICE_OTHER );

  cxbtnF_CORRECT_PRICE.Enabled := ( Mode in [rvmAdd, rvmEdit] );

  CheckPricing;

  UpdateStatusLookupbutton( True ); // Enable lookup button (Mantis 2088).

  if Mode <> rvmView then
  begin
    if srcMain.Dataset.State = dsBrowse then srcMain.DataSet.Edit;
    if srcMain.DataSet.FieldByName('F_MAX_SCORE').AsInteger = 0 then
      srcMain.DataSet.FieldByName('F_MAX_SCORE').AsInteger := srcMain.DataSet.FieldByName('F_MAX_PUNTEN').AsInteger;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Session ButtonEdit.  In here we will call the appropriate method on the
  IEDUEnrolmentDataModule interface corresponding to the clicked button.

  @Name       TfrmEnrolment_Record.cxdbbeF_SESSION_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
  History                     Author                Description
  25/03/2008                Ivan Van den Bossche    Added rvmView check (Mantis 2088)
*************************************************************************************}
procedure TfrmEnrolment_Record.cxdbbeF_SESSION_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUEnrolmentDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd, rvmView ] ) and
     ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        if Mode = rvmView then
        begin
          // Workaround for mantis 2088.
          UpdateStatusLookupbutton( False ); // Make sure that lookupbutton in dtmEDUMainClient.cxeriShowSelectLookup is disabled before opening child Window.
          aDataModule.ShowSession( srcMain.DataSet, rvmView );
          UpdateStatusLookupbutton( True );
        end
        else begin
          aDataModule.ShowSession( srcMain.DataSet, rvmEdit );
        end;
      end;
      else aDataModule.SelectSession( srcMain.DataSet );
    end;
  end;

end;

{*********************************************************************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Person ButtonEdit.  In here we will call the appropriate method on the
  IEDUEnrolmentDataModule interface corresponding to the clicked button.

  @Name       TfrmEnrolment_Record.cxdbbeF_LASTNAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @History
    07/11/2008  Ivan Van den Bossche  Mantis 4205 (Leerlingen -> default Tussenkomst FVB, Leerkrachten -> default geen Tussenkomst
  @return     None
  @Exception  None
  @See        None
*********************************************************************************************************************************}

procedure TfrmEnrolment_Record.cxdbbeF_LASTNAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUEnrolmentDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd, rvmView ] ) and
     ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        if Mode = rvmView then
        begin
          UpdateStatusLookupbutton( False ); // Make sure that lookupbutton in dtmEDUMainClient.cxeriShowSelectLookup is disabled before opening child Window.
          aDataModule.ShowRole( srcMain.DataSet, rvmView );
          UpdateStatusLookupbutton( True );
        end
        else begin
          aDataModule.ShowRole( srcMain.DataSet, rvmEdit );
        end;
      end;
      1 :
      begin
        aDataModule.SelectRole( srcMain.DataSet );
      end
      else
      begin
        aDataModule.ShowRole( srcMain.DataSet, rvmAdd );
      end;
    end;

    //Default tussenkomst zetten op basis van de functie van de ingeschreven persoon
    //en ook factuurbedrag ophalen
    if Mode <> rvmView then
    begin
      if srcMain.Dataset.State = dsBrowse then srcMain.DataSet.Edit;
      case srcMain.DataSet.FieldByName('F_ROLEGROUP_ID').AsInteger of
        2: //arbeider PC 124
          begin
            srcMain.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger := 1; //tussenkomst FVB
          end;
        3: //bediende PC 218
          begin
            srcMain.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger := 2; //tussenkomst Cevora
          end;
        11: //Leerling
          begin
            srcMain.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger := 1; //tussenkomst FVB
          end;
        20: //leerkracht
          begin
            srcMain.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger := 0; //geen tussenkomst FVB
          end;
      end;
      SetPrices();
    end;
  end
  else assert (false, 'TfrmEnrolment_Record datamodule not of type IEDUEnrolmentDataModule');
end;

procedure TfrmEnrolment_Record.SetPrices;
var
  Factuurbedrag: double;
  aDataModule : IEDUEnrolmentDataModule;
begin
  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) then
  begin
    if srcMain.Dataset.State = dsBrowse then srcMain.DataSet.Edit;
    srcMain.DataSet.FieldByName('F_PRICE_WORKER').AsFloat := 0;
    srcMain.DataSet.FieldByName('F_PRICE_CLERK').AsFloat := 0;
    srcMain.DataSet.FieldByName('F_PRICE_OTHER').AsFloat := 0;

    Factuurbedrag := aDatamodule.GetDefaultPrice(srcMain.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger, srcMain.DataSet.FieldByName('F_SESSION_ID').AsInteger);

    case srcMain.DataSet.FieldByName('F_PRICING_SCHEME').AsInteger of
      0: //geen tussenkomst
        srcMain.DataSet.FieldByName('F_PRICE_OTHER').AsFloat := Factuurbedrag;
      1: //tussenkomst FVB
        srcMain.DataSet.FieldByName('F_PRICE_WORKER').AsFloat := Factuurbedrag;
      2: //tussenkomst Cevora
        srcMain.DataSet.FieldByName('F_PRICE_CLERK').AsFloat := Factuurbedrag;
    end;
  end
  else assert (false, 'TfrmEnrolment_Record datamodule not of type IEDUEnrolmentDataModule');
end;


{************************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Status ButtonEdit.  In here we will call the appropriate method on the
  IEDUEnrolmentDataModule interface corresponding to the clicked button.

  @Name       TfrmEnrolment_Record.cxdbbeF_STATUS_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
  History                     Author                Description
  25/03/2008                Ivan Van den Bossche    Added rvmView check (Mantis 2088)
*************************************************************************************}

procedure TfrmEnrolment_Record.cxdbbeF_STATUS_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUEnrolmentDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd, rvmView ] ) and
     ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        if Mode = rvmView then
        begin
          aDataModule.ShowStatus( srcMain.DataSet, rvmView );
        end else begin
          aDataModule.ShowStatus( srcMain.DataSet, rvmEdit );
        end;
      end;
      else aDataModule.SelectStatus( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  Procedure to show History-log.

  @Name       TfrmEnrolment_Record.acShowLogHistoryExecute
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_Record.acShowLogHistoryExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmLogHistory', pnlRecordDetail );
end;

{*****************************************************************************
  to make the field F_PRICE_WORKER/F_PRICE_CLERK/F_PRICE_OTHER editeable.

  @Name       TfrmEnrolment_Record.cxbtnF_CORRECT_PRICEClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmEnrolment_Record.cxbtnF_CORRECT_PRICEClick(Sender: TObject);
begin
  inherited;

  if Mode in [rvmAdd, rvmEdit] then
  begin
    if cxdbcueF_PRICE_WORKER.ActiveProperties.ReadOnly then
    begin
      dtmEDUMainClient.ResetRepositoryItem( cxdbcueF_PRICE_WORKER );
      dtmEDUMainClient.ResetRepositoryItem( cxdbcueF_PRICE_CLERK );
      dtmEDUMainClient.ResetRepositoryItem( cxdbcueF_PRICE_OTHER );
    end
    else
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcueF_PRICE_WORKER );
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcueF_PRICE_CLERK );
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbcueF_PRICE_OTHER );
    end;
    DoUpdateControls;
  end;
end;

procedure TfrmEnrolment_Record.cxdbicbF_PRICE_REDUCTIONPropertiesChange(
  Sender: TObject);
begin
  inherited;
  // Controleer even ...
  CheckPricing;
end;

procedure TfrmEnrolment_Record.CheckPricing;
begin
  // Wijzig het getoonde veld n.a.v wijziging tussenkomst
  cxdbcueF_PRICE_OTHER.Visible := (cxdbicbF_PRICE_REDUCTION.ItemIndex = 0);
  cxdbcueF_PRICE_WORKER.Visible:= (cxdbicbF_PRICE_REDUCTION.ItemIndex = 1);
  cxdbcueF_PRICE_CLERK.Visible := (cxdbicbF_PRICE_REDUCTION.ItemIndex = 2);
end;

procedure TfrmEnrolment_Record.UpdateStatusLookupbutton(
  const AEnabled: Boolean);
begin
  cxdbbeF_SESSION_NAME.RepositoryItem.Properties.Buttons[0].Enabled := AEnabled;
  cxdbbeF_LASTNAME.RepositoryItem.Properties.Buttons[0].Enabled := AEnabled;
end;

procedure TfrmEnrolment_Record.FVBFFCRecordViewAllDetailsCreated(
  Sender: TObject);
begin
  inherited;
  if not (srcMain.Dataset.FieldByName('F_CERTIFICATE').AsBoolean) then
    srcMain.Dataset.FieldByName ('F_CERTIFICATE').ReadOnly := true;
end;

procedure TfrmEnrolment_Record.srcMainDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if Field = srcMain.Dataset.FieldByName('F_PRICING_SCHEME') then
  begin
    //toon het bedrag
    SetPrices();
  end;
end;

procedure TfrmEnrolment_Record.FVBFFCRecordViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

    // Make sure that lookupbutton in dtmEDUMainClient.cxeriShowSelectLookup is disabled since we are in read-only mode.
    // This is a workaround for Mantis 2088 since we had to temporary enable the lookupbutton.
    if Mode = rvmView then
      UpdateStatusLookupbutton( False );

end;

procedure TfrmEnrolment_Record.cxdbbef_PARTNER_WINTERPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUEnrolmentDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd, rvmView ] ) and
     ( Supports( FrameWorkDataModule, IEDUEnrolmentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      1:
       aDataModule.RemovePartnerWinter ( srcMain.DataSet);
      0 :
       aDataModule.SelectPartnerWinter( srcMain.DataSet );
  end;
end;
end;


end.
