{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_RO_ROLE records.


  @Name       Form_Role_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Role_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxDBEdit,
  cxCheckBox, cxDropDownEdit, cxCalendar, cxButtonEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxLabel, cxControls, cxContainer, cxEdit,
  cxGroupBox, Menus, cxSplitter, cxMemo;

type
  TfrmRole_Record = class(TEDURecordView)
    cxgbRoleInfo: TFVBFFCGroupBox;
    cxgbOrganisationInfo: TFVBFFCGroupBox;
    cxlblF_ROLE_ID1: TFVBFFCLabel;
    cxdbseF_ROLE_ID1: TFVBFFCDBSpinEdit;
    cxlblF_ROLEGROUP_NAME1: TFVBFFCLabel;
    cxdbbeF_ROLEGROUP_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE1: TFVBFFCDBDateEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE1: TFVBFFCDBDateEdit;
    cxlblF_PRINCIPAL1: TFVBFFCLabel;
    cxdbcbF_PRINCIPAL1: TFVBFFCDBCheckBox;
    cxlblF_SCHOOLYEAR1: TFVBFFCLabel;
    cxdbbeF_SCHOOLYEAR1: TFVBFFCDBButtonEdit;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdbbeF_NAME: TFVBFFCDBButtonEdit;
    cxlblF_RSZ_NR1: TFVBFFCLabel;
    cxdbteF_RSZ_NR1: TFVBFFCDBTextEdit;
    cxgbPersonInfo: TFVBFFCGroupBox;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdbbeF_LASTNAME: TFVBFFCDBButtonEdit;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit;
    cxlblF_SOCSEC_NR1: TFVBFFCLabel;
    cxdbteF_SOCSEC_NR1: TFVBFFCDBTextEdit;
    cxlblF_LASTNAME2: TFVBFFCLabel;
    cxdblblF_LASTNAME: TFVBFFCDBLabel;
    cxlblF_FIRSTNAME2: TFVBFFCLabel;
    cxdblblF_FIRSTNAME1: TFVBFFCDBLabel;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxlblF_ACTIVE1: TFVBFFCLabel;
    cxdbcbF_ACTIVE1: TFVBFFCDBCheckBox;
    FVBFFCGroupBox1: TFVBFFCGroupBox;
    FVBFFCDBMemo1: TFVBFFCDBMemo;
    FVBFFCLabel1: TFVBFFCLabel;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCLabel3: TFVBFFCLabel;
    FVBFFCLabel4: TFVBFFCLabel;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    FVBFFCDBTextEdit2: TFVBFFCDBTextEdit;
    FVBFFCDBTextEdit3: TFVBFFCDBTextEdit;
    FVBFFCDBTextEdit4: TFVBFFCDBTextEdit;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_LASTNAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_SCHOOLYEAR1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCRecordViewClose(Sender: TObject;
      var Action: TCloseAction);
  private
    { Private declarations }
    procedure UpdateStatusLookupbutton(const AEnabled: Boolean);
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Role, Data_EduMainClient, unit_EdutecInterfaces;

{ TfrmRole_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmRole_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmRole_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption + '\' + cxdblblF_LASTNAME.Caption;
end;

procedure TfrmRole_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  aRoleDataModule : IEDURoleDataModule;
begin
  inherited;

  { Check if the DataModule was opened as a Detail of another DataModule }
  if ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUOrganisationDataModule ) ) then
    begin
      cxdbbeF_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
    end
    else if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUPersonDataModule ) ) then
    begin
      cxdbbeF_LASTNAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
    end;
  end;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Supports( aFrameWorkDataModule, IEDURoleDataModule, aRoleDataModule ) ) and
     ( aRoleDataModule.CreateForPerson ) then
  begin
    cxdbbeF_LASTNAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
  end;
  UpdateStatusLookupbutton( True );
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the RoleGroup ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmRole_Record.cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDURoleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDURoleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowRoleGroup( srcMain.DataSet, rvmEdit );
      end;
      else
      begin
        aDataModule.SelectRoleGroup( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Organisation ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_NAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmRole_Record.cxdbbeF_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDURoleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd, rvmView ] ) and
     ( Supports( FrameWorkDataModule, IEDURoleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        if Mode = rvmView then
        begin
          UpdateStatusLookupbutton( False ); // Make sure that lookupbutton in dtmEDUMainClient.cxeriShowSelectLookup is disabled before opening child Window.
          aDataModule.ShowOrganisation( srcMain.DataSet, rvmView );
          UpdateStatusLookupbutton( True );
        end
        else begin
          aDataModule.ShowOrganisation( srcMain.DataSet, rvmEdit );
        end;
      end;
      1 :
      begin
        aDataModule.SelectOrganisation( srcMain.DataSet );
      end
      else
      begin
        aDataModule.ShowOrganisation( srcMain.DataSet, rvmAdd );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Person ButtonEdit.  In here we will call the appropriate method on the
  IEDUPersonDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_LASTNAMEPropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmRole_Record.cxdbbeF_LASTNAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDURoleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDURoleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowPerson( srcMain.DataSet, rvmEdit );
      end;
      1 :
      begin
        aDataModule.SelectPerson( srcMain.DataSet );
      end
      else
      begin
        aDataModule.ShowPerson( srcMain.DataSet, rvmAdd );
      end;
    end;
  end;
end;

procedure TfrmRole_Record.cxdbbeF_SCHOOLYEAR1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDURoleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDURoleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowSchoolyear( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectSchoolyear( srcMain.DataSet );
    end;
  end;

end;

procedure TfrmRole_Record.UpdateStatusLookupbutton(
  const AEnabled: Boolean);
begin
  cxdbbeF_NAME.RepositoryItem.Properties.Buttons[0].Enabled := AEnabled;
end;

procedure TfrmRole_Record.FVBFFCRecordViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if Mode = rvmView then
    UpdateStatusLookupbutton( False );
end;

end.
