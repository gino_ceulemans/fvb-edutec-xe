inherited frmCountry_Record: TfrmCountry_Record
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Land'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      Width = 622
      ExplicitWidth = 622
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 622
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 622
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 622
        ExplicitWidth = 622
        object cxlblF_COUNTRY_ID2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_COUNTRY_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Land ( ID )'
          FocusControl = cxdblblF_COUNTRY_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COUNTRY_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_COUNTRY_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_COUNTRY_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.IsFontAssigned = True
          Height = 21
          Width = 121
        end
        object cxlblF_COUNTRY1: TFVBFFCLabel
          Left = 160
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_COUNTRY'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Land'
          FocusControl = cxdblblF_COUNTRY
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COUNTRY: TFVBFFCDBLabel
          Left = 160
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCountry_Record.F_COUNTRY'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_COUNTRY_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.IsFontAssigned = True
          Height = 21
          Width = 121
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 622
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 622
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 622
        ExplicitWidth = 622
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 622
        end
        inherited sbxMain: TScrollBox
          Width = 622
          ExplicitWidth = 622
          object cxlblF_COUNTRY_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_COUNTRY_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land ( ID )'
            FocusControl = cxdbseF_COUNTRY_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_COUNTRY_ID: TFVBFFCDBSpinEdit
            Left = 88
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_COUNTRY_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_COUNTRY_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 88
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 3
            Width = 400
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 88
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 5
            Width = 400
          end
          object cxlblF_ISO_CODE1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_ISO_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'ISO Code'
            FocusControl = cxdbteF_ISO_CODE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_ISO_CODE: TFVBFFCDBTextEdit
            Left = 88
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCountry_Record.F_ISO_CODE'
            DataBinding.DataField = 'F_ISO_CODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 7
            Width = 121
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiCountryPart
          end>
      end
      object dxnbiCountryPart: TdxNavBarItem
        Action = acCountryParts
      end
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acCountryParts: TAction
      Caption = 'Landsgedeeltes'
      OnExecute = acCountryPartsExecute
    end
  end
end
