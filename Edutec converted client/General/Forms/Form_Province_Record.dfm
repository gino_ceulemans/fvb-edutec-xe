inherited frmProvince_Record: TfrmProvince_Record
  Left = 569
  Top = 266
  Width = 811
  ActiveControl = cxdbbeF_COUNTRY_PART_NAME
  Caption = 'Provincie'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 803
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 467
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 803
    inherited pnlRecord: TFVBFFCPanel
      Width = 544
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 544
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 544
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 544
        object cxlblF_PROVINCE_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProvince_Record.F_PROVINCE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Provincie ( ID )'
          FocusControl = cxdblblF_PROVINCE_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROVINCE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProvince_Record.F_PROVINCE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROVINCE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 64
        end
        object cxlblF_PROVINCE_NAME1: TFVBFFCLabel
          Left = 144
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProvince_Record.F_PROVINCE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Provincie'
          FocusControl = cxdblblF_PROVINCE_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROVINCE_NAME: TFVBFFCDBLabel
          Left = 144
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProvince_Record.F_PROVINCE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_PROVINCE_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 484
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 544
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 544
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 544
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 544
        end
        inherited sbxMain: TScrollBox
          Width = 544
          object cxlblF_PROVINCE_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_PROVINCE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Provincie ( ID )'
            FocusControl = cxdbseF_PROVINCE_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PROVINCE_ID: TFVBFFCDBSpinEdit
            Left = 144
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_PROVINCE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PROVINCE_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Provincie ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 144
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 337
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Provincie ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 144
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 7
            Width = 337
          end
          object cxlblF_COUNTRY_PART_NAME1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_COUNTRY_PART_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Landsgedeelte'
            FocusControl = cxdbbeF_COUNTRY_PART_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COUNTRY_PART_NAME: TFVBFFCDBButtonEdit
            Left = 144
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProvince_Record.F_COUNTRY_PART_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_COUNTRY_PART_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COUNTRY_PART_NAME1PropertiesButtonClick
            TabOrder = 3
            Width = 337
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 799
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiPostCodes
          end>
      end
      object dxnbiPostCodes: TdxNavBarItem
        Action = acShowPostalCodes
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmProvince.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowPostalCodes: TAction
      Caption = 'Postcodes'
      OnExecute = acShowPostalCodesExecute
    end
  end
end
