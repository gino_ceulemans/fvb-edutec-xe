inherited frmRoleGroup_Record: TfrmRoleGroup_Record
  Width = 917
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Functie'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 909
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 573
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 909
    inherited pnlRecord: TFVBFFCPanel
      Width = 747
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 747
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 747
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 747
        object cxlblF_ROLEGROUP_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmRoleGroup_Record.F_ROLEGROUP_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Functie ( ID )'
          FocusControl = cxdblblF_ROLEGROUP_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ROLEGROUP_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmRoleGroup_Record.F_ROLEGROUP_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ROLEGROUP_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 105
        end
        object cxlblF_ROLEGROUP_NAME1: TFVBFFCLabel
          Left = 112
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmRoleGroup_Record.F_ROLEGROUP_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Functie'
          FocusControl = cxdblblF_ROLEGROUP_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ROLEGROUP_NAME: TFVBFFCDBLabel
          Left = 112
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmRoleGroup_Record.F_ROLEGROUP_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_ROLEGROUP_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 622
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 747
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 747
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 747
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 747
        end
        inherited sbxMain: TScrollBox
          Width = 747
          object cxlblF_ROLEGROUP_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmRoleGroup_Record.F_ROLEGROUP_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Functie ( ID )'
            FocusControl = cxdbseF_ROLEGROUP_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_ROLEGROUP_ID: TFVBFFCDBSpinEdit
            Left = 104
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmRoleGroup_Record.F_ROLEGROUP_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_ROLEGROUP_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmRoleGroup_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Functie ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 104
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmRoleGroup_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 401
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmRoleGroup_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Functie ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 104
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmRoleGroup_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 401
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 905
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Visible = False
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmRoleGroup.cdsRecord
  end
end
