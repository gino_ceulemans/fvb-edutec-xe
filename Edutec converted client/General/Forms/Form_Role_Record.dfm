inherited frmRole_Record: TfrmRole_Record
  Left = 518
  Top = 260
  Width = 808
  Height = 620
  Action = acShowRecordDetail
  ActiveControl = cxdbbeF_ROLEGROUP_NAME1
  Caption = 'Record Detail'
  Constraints.MinHeight = 512
  Constraints.MinWidth = 808
  OnClick = acShowRecordDetailExecute
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 561
    Width = 800
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 464
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 800
    Height = 561
    inherited pnlRecord: TFVBFFCPanel
      Width = 638
      Height = 561
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 638
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 638
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 638
        object cxlblF_LASTNAME2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmRole_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Familienaam'
          FocusControl = cxdblblF_LASTNAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_LASTNAME: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmRole_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_LASTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 177
        end
        object cxlblF_FIRSTNAME2: TFVBFFCLabel
          Left = 200
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmRole_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Voornaam'
          FocusControl = cxdblblF_FIRSTNAME1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_FIRSTNAME1: TFVBFFCDBLabel
          Left = 200
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmRole_Record.F_FIRSTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_FIRSTNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 145
        end
        object cxlblF_NAME2: TFVBFFCLabel
          Left = 360
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmRole_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Organisatie'
          FocusControl = cxdblblF_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 360
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmRole_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 273
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 638
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 557
        Width = 638
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 638
        Height = 487
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 638
        end
        inherited sbxMain: TScrollBox
          Width = 638
          Height = 462
          object cxgbRoleInfo: TFVBFFCGroupBox
            Left = 8
            Top = 8
            Caption = 'Functie Informatie'
            ParentColor = False
            TabOrder = 0
            Transparent = True
            Height = 129
            Width = 625
            object cxlblF_ROLE_ID1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_ROLE_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Functie ( ID )'
              FocusControl = cxdbseF_ROLE_ID1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_ROLE_ID1: TFVBFFCDBSpinEdit
              Left = 128
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_ROLE_ID'
              RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
              DataBinding.DataField = 'F_ROLE_ID'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 121
            end
            object cxlblF_ROLEGROUP_NAME1: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_ROLEGROUP_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Rol'
              FocusControl = cxdbbeF_ROLEGROUP_NAME1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_ROLEGROUP_NAME1: TFVBFFCDBButtonEdit
              Left = 128
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_ROLEGROUP_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_ROLEGROUP_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick
              TabOrder = 3
              Width = 473
            end
            object cxlblF_START_DATE1: TFVBFFCLabel
              Left = 8
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_START_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Start Datum'
              FocusControl = cxdbdeF_START_DATE1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_START_DATE1: TFVBFFCDBDateEdit
              Left = 128
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_START_DATE'
              DataBinding.DataField = 'F_START_DATE'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 180
            end
            object cxlblF_END_DATE1: TFVBFFCLabel
              Left = 344
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_END_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Eind Datum'
              FocusControl = cxdbdeF_END_DATE1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_END_DATE1: TFVBFFCDBDateEdit
              Left = 424
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_END_DATE'
              DataBinding.DataField = 'F_END_DATE'
              DataBinding.DataSource = srcMain
              TabOrder = 7
              Width = 177
            end
            object cxlblF_PRINCIPAL1: TFVBFFCLabel
              Left = 344
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_PRINCIPAL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Hoofdrol'
              FocusControl = cxdbcbF_PRINCIPAL1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcbF_PRINCIPAL1: TFVBFFCDBCheckBox
              Left = 424
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_PRINCIPAL'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'cxdbcbF_PRINCIPAL1'
              DataBinding.DataField = 'F_PRINCIPAL'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 9
              Width = 21
            end
            object cxlblF_SCHOOLYEAR1: TFVBFFCLabel
              Left = 8
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SCHOOLYEAR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Schooljaar'
              FocusControl = cxdbbeF_SCHOOLYEAR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_SCHOOLYEAR1: TFVBFFCDBButtonEdit
              Left = 128
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SCHOOLYEAR'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_SCHOOLYEAR'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_SCHOOLYEAR1PropertiesButtonClick
              TabOrder = 11
              Width = 200
            end
            object cxlblF_ACTIVE1: TFVBFFCLabel
              Left = 504
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_ACTIVE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Actief'
              FocusControl = cxdbcbF_ACTIVE1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcbF_ACTIVE1: TFVBFFCDBCheckBox
              Left = 579
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_ACTIVE'
              Caption = 'cxdbcbF_ACTIVE1'
              DataBinding.DataField = 'F_ACTIVE'
              DataBinding.DataSource = srcMain
              ParentColor = False
              Properties.NullStyle = nssInactive
              Properties.ReadOnly = True
              TabOrder = 13
              Width = 21
            end
          end
          object cxgbOrganisationInfo: TFVBFFCGroupBox
            Left = 8
            Top = 144
            Caption = 'Organisatie Informatie'
            ParentColor = False
            TabOrder = 1
            Transparent = True
            Height = 81
            Width = 624
            object cxlblF_NAME1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Organisatie'
              FocusControl = cxdbbeF_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_NAME: TFVBFFCDBButtonEdit
              Left = 128
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectAddLookup
              DataBinding.DataField = 'F_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_NAMEPropertiesButtonClick
              TabOrder = 1
              Width = 473
            end
            object cxlblF_RSZ_NR1: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_RSZ_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'RSZ Nr'
              FocusControl = cxdbteF_RSZ_NR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_RSZ_NR1: TFVBFFCDBTextEdit
              Left = 128
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_RSZ_NR'
              DataBinding.DataField = 'F_RSZ_NR'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 473
            end
          end
          object cxgbPersonInfo: TFVBFFCGroupBox
            Left = 8
            Top = 232
            Caption = 'Persoons Informatie'
            ParentColor = False
            TabOrder = 2
            Transparent = True
            Height = 145
            Width = 624
            object cxlblF_LASTNAME1: TFVBFFCLabel
              Left = 8
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_LASTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Familienaam'
              FocusControl = cxdbbeF_LASTNAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_LASTNAME: TFVBFFCDBButtonEdit
              Left = 128
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_LASTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectAddLookup
              DataBinding.DataField = 'F_LASTNAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_LASTNAMEPropertiesButtonClick
              TabOrder = 1
              Width = 473
            end
            object cxlblF_FIRSTNAME1: TFVBFFCLabel
              Left = 8
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_FIRSTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Voornaam'
              FocusControl = cxdbteF_FIRSTNAME1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit
              Left = 128
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_FIRSTNAME'
              DataBinding.DataField = 'F_FIRSTNAME'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 473
            end
            object cxlblF_SOCSEC_NR1: TFVBFFCLabel
              Left = 8
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Rijksr. Nr'
              FocusControl = cxdbteF_SOCSEC_NR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_SOCSEC_NR1: TFVBFFCDBTextEdit
              Left = 128
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              DataBinding.DataField = 'F_SOCSEC_NR'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 5
              Width = 473
            end
            object FVBFFCLabel1: TFVBFFCLabel
              Left = 8
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Telefoon'
              FocusControl = cxdbteF_SOCSEC_NR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCLabel2: TFVBFFCLabel
              Left = 8
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'GSM'
              FocusControl = cxdbteF_SOCSEC_NR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCLabel3: TFVBFFCLabel
              Left = 280
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Fax'
              FocusControl = cxdbteF_SOCSEC_NR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCLabel4: TFVBFFCLabel
              Left = 280
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              FocusControl = cxdbteF_SOCSEC_NR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
              Left = 128
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              DataBinding.DataField = 'F_PHONE'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 10
              Width = 137
            end
            object FVBFFCDBTextEdit2: TFVBFFCDBTextEdit
              Left = 128
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              DataBinding.DataField = 'F_GSM'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 11
              Width = 137
            end
            object FVBFFCDBTextEdit3: TFVBFFCDBTextEdit
              Left = 384
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              DataBinding.DataField = 'F_FAX'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 12
              Width = 137
            end
            object FVBFFCDBTextEdit4: TFVBFFCDBTextEdit
              Left = 383
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmRole_Record.F_SOCSEC_NR'
              DataBinding.DataField = 'F_EMAIL'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 13
              Width = 217
            end
          end
          object FVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 6
            Top = 384
            Caption = 'Opmerkingen'
            ParentColor = False
            TabOrder = 3
            Transparent = True
            Height = 65
            Width = 624
            object FVBFFCDBMemo1: TFVBFFCDBMemo
              Left = 8
              Top = 16
              DataBinding.DataField = 'F_REMARK'
              DataBinding.DataSource = srcMain
              TabOrder = 0
              Height = 44
              Width = 593
            end
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 796
      Height = 561
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 561
      LargeImages = dtmEDUMainClient.ilGlobalLarge
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 561
    end
  end
end
