{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_CenInfrastructure_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_CenInfrastructure_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons, cxDBLabel, cxControls, cxContainer, cxEdit, cxLabel,
  cxCheckBox, cxDBEdit, cxGroupBox, cxMemo, cxHyperLinkEdit, cxButtonEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, Menus, cxSplitter;

type
  TfrmCenInfrastructure_Record = class(TEDURecordView)
    cxlblF_INFRASTRUCTURE_ID1: TFVBFFCLabel;
    cxdblblF_INFRASTRUCTURE_ID: TFVBFFCDBLabel;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxlblF_INFRASTRUCTURE_ID2: TFVBFFCLabel;
    cxdbseF_INFRASTRUCTURE_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_RSZ_NR1: TFVBFFCLabel;
    F_RSZ_NR: TFVBFFCDBMaskEdit;
    cxlblF_VAT_NR1: TFVBFFCLabel;
    F_VAT_NR: TFVBFFCDBMaskEdit;
    cxlblF_STREET1: TFVBFFCLabel;
    cxdbteF_STREET: TFVBFFCDBTextEdit;
    cxlblF_NUMBER1: TFVBFFCLabel;
    cxdbteF_NUMBER: TFVBFFCDBTextEdit;
    cxlblF_MAILBOX1: TFVBFFCLabel;
    cxdbteF_MAILBOX: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxdbbeF_POSTALCODE: TFVBFFCDBButtonEdit;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdbteF_CITY_NAME: TFVBFFCDBTextEdit;
    cxlblF_PHONE1: TFVBFFCLabel;
    cxdbteF_PHONE: TFVBFFCDBTextEdit;
    cxlblF_FAX1: TFVBFFCLabel;
    cxdbteF_FAX: TFVBFFCDBTextEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    F_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlblF_WEBSITE1: TFVBFFCLabel;
    F_WEBSITE: TFVBFFCDBHyperLinkEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_ACC_NR1: TFVBFFCLabel;
    cxdbteF_ACC_NR: TFVBFFCDBTextEdit;
    cxlblF_ACC_HOLDER1: TFVBFFCLabel;
    cxdbteF_ACC_HOLDER: TFVBFFCDBTextEdit;
    cxlblF_MEDIUM_NAME1: TFVBFFCLabel;
    cxdbbeF_MEDIUM_NAME: TFVBFFCDBButtonEdit;
    cxlblF_COMMENT: TFVBFFCLabel;
    cxdbmmoF_COMMENT: TFVBFFCDBMemo;
    cxgbAvailability: TFVBFFCGroupBox;
    cxlblF_SATURDAY1: TFVBFFCLabel;
    csdbcbF_SATURDAY: TFVBFFCDBCheckBox;
    cxlblF_EVENING1: TFVBFFCLabel;
    csdbcbF_EVENING: TFVBFFCDBCheckBox;
    cxlblF_WEEKDAY1: TFVBFFCLabel;
    csdbcbF_WEEKDAY: TFVBFFCDBCheckBox;
    acShowCenRoom: TAction;
    dxnbiShowCenRoom: TdxNavBarItem;
    dxnbiShowLogHistory: TdxNavBarItem;
    acShowLogHistory: TAction;
    acShowDocCenter: TAction;
    FVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit;
    F_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel;
    F_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel;
    cxlbF_CONFIRMATION_CONTACT3: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT3: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCLabel;
    F_CONFIRMATION_CONTACT3_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlblF_INFO: TFVBFFCLabel;
    cxdbmmoF_INFO: TFVBFFCDBMemo;
    cxdbbeF_MAIL_ATTACHMENT: TFVBFFCDBButtonEdit;
    cxlblF_MAIL_ATTACHMENT: TFVBFFCLabel;
    procedure cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_MEDIUM_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_POSTALCODEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acShowCenRoomExecute(Sender: TObject);
    procedure acShowLogHistoryExecute(Sender: TObject);
    procedure cxdbbeF_MAIL_ATTACHMENTPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer); 
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_CenInfrastructure, Data_EduMainClient, Unit_FVBFFCInterfaces, unit_EdutecInterfaces,
  Form_FVBFFCBaseRecordView;

{ TfrmCenInfrastructure_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmCenInfrastructure_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmCenInfrastructure_Record.GetDetailName: String;
begin
  Result := stringReplace(cxdblblF_NAME.Caption, '&', '&&', [rfReplaceAll]);
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Language ButtonEdit.  It will be used to allow the User to Select or
  View a Language.

  @Name       TfrmCenInfrastructure_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmCenInfrastructure_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUCenInfrastructureDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUCenInfrastructureDataModule, aDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectLanguage( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Medium ButtonEdit.  It will be used to allow the User to Select or
  View a Medium.

  @Name       TfrmCenInfrastructure_Record.cxdbbeF_MEDIUM_NAMEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmCenInfrastructure_Record.cxdbbeF_MEDIUM_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUCenInfrastructureDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUCenInfrastructureDataModule, aDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowMedium( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectMedium( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the PostalCode ButtonEdit.  It will be used to allow the User to Select or
  View a PostalCode.

  @Name       TfrmCenInfrastructure_Record.cxdbbeF_POSTALCODEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmCenInfrastructure_Record.cxdbbeF_POSTALCODEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUCenInfrastructureDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUCenInfrastructureDataModule, aDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowPostalCode( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectPostalCode( srcMain.DataSet );
      end;
    end;
  end;
end;

procedure TfrmCenInfrastructure_Record.acShowCenRoomExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmCenRoom', pnlRecordDetail );
end;

procedure TfrmCenInfrastructure_Record.acShowLogHistoryExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmLogHistory', pnlRecordDetail );
end;

procedure TfrmCenInfrastructure_Record.cxdbbeF_MAIL_ATTACHMENTPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  od: TOpenDialog;
begin
  inherited;
   if AButtonIndex = 0 then
  begin
    od := TOpenDialog.Create( Self );
    od.InitialDir := ExtractFilePath(cxdbbeF_MAIL_ATTACHMENT.Text);
    if ( od.Execute ) then
    begin
        if not ( srcMain.DataSet.State in dsEditModes ) then
          srcMain.DataSet.Edit;
        srcMain.DataSet.FieldByName('F_MAIL_ATTACHMENT').AsString := od.FileName;
    end;
    od.Free;
  end;
  if AButtonIndex = 1 then
  begin
    if not ( srcMain.DataSet.State in dsEditModes ) then
      srcMain.DataSet.Edit;
    srcMain.DataSet.FieldByName('F_MAIL_ATTACHMENT').Clear;
  end;
end;

end.
