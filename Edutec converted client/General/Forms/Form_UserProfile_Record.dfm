inherited frmUserProfile_Record: TfrmUserProfile_Record
  Left = 133
  Width = 891
  Caption = 'GebruikersProfiel'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 883
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 547
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 883
    inherited pnlRecord: TFVBFFCPanel
      Width = 721
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 721
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 721
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 721
        object cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmUserProfile_Record.F_COMPLETE_USERNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Gebruiker'
          FocusControl = cxdblblF_COMPLETE_USERNAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COMPLETE_USERNAME: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmUserProfile_Record.F_COMPLETE_USERNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_COMPLETE_USERNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 296
        end
        object cxlblF_PROFILE_NAME1: TFVBFFCLabel
          Left = 312
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmUserProfile_Record.F_PROFILE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Profiel'
          FocusControl = cxdblblF_PROFILE_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROFILE_NAME: TFVBFFCDBLabel
          Left = 312
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmUserProfile_Record.F_PROFILE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROFILE_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 296
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 721
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 721
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 721
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 721
        end
        inherited sbxMain: TScrollBox
          Width = 721
          object cxlblF_COMPLETE_USERNAME2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmUserProfile_Record.F_COMPLETE_USERNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gebruiker'
            FocusControl = cxdbbeF_COMPLETE_USERNAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_LASTNAME1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmUser_Record.F_LASTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam'
            FocusControl = cxdbteF_LASTNAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_FIRSTNAME1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmUser_Record.F_FIRSTNAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Voornaam'
            FocusControl = cxdbteF_FIRSTNAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit
            Left = 120
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmUser_Record.F_FIRSTNAME'
            DataBinding.DataField = 'F_FIRSTNAME'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 3
            Width = 369
          end
          object cxdbteF_LASTNAME: TFVBFFCDBTextEdit
            Left = 120
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmUser_Record.F_LASTNAME'
            DataBinding.DataField = 'F_LASTNAME'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 4
            Width = 369
          end
          object cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit
            Left = 120
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmUserProfile_Record.F_COMPLETE_USERNAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_COMPLETE_USERNAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick
            TabOrder = 5
            Width = 369
          end
          object cxlblF_PROFILE_NAME2: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmUserProfile_Record.F_PROFILE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Profiel'
            FocusControl = cxdbbeF_PROFILE_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_PROFILE_NAME: TFVBFFCDBButtonEdit
            Left = 120
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmUserProfile_Record.F_PROFILE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PROFILE_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PROFILE_NAMEPropertiesButtonClick
            TabOrder = 7
            Width = 369
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 879
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
end
