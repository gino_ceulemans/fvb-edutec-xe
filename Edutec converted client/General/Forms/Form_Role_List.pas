{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Role_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Role_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxButtonEdit, cxDBEdit, cxMaskEdit, cxTextEdit, cxContainer, cxLabel,
  Menus, cxDropDownEdit, cxImageComboBox;

type
  TfrmRole_List = class(TEduListView)
    cxgrdtblvListF_ROLE_ID: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_RSZ_NR: TcxGridDBColumn;
    cxgrdtblvListF_ROLEGROUP_ID: TcxGridDBColumn;
    cxgrdtblvListF_ROLEGROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxgrdtblvListF_PRINCIPAL: TcxGridDBColumn;
    cxgrdtblvListF_SCHOOLYEAR_ID: TcxGridDBColumn;
    cxgrdtblvListF_SCHOOLYEAR: TcxGridDBColumn;
    cxlblF_LASTNAME2: TFVBFFCLabel;
    cxdbteF_LASTNAME1: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME2: TFVBFFCLabel;
    cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit;
    cxlblSocSecNr: TFVBFFCLabel;
    cxdbmeSocSecNr: TFVBFFCDBMaskEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_ROLEGROUP_NAME1: TFVBFFCLabel;
    cxdbbeF_ROLEGROUP_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
    cxgrdtblvListF_REMARK: TcxGridDBColumn;
    cxgrdtblvListF_PHONE: TcxGridDBColumn;
    cxgrdtblvListF_FAX: TcxGridDBColumn;
    cxgrdtblvListF_GSM: TcxGridDBColumn;
    cxgrdtblvListF_EMAIL: TcxGridDBColumn;
    procedure cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCListViewCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Role, Form_FVBFFCBaseListView, Data_EduMainClient, unit_EdutecInterfaces;

{ TfrmRole_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmRole_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmRole_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the condition for the FirstName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_FIRSTNAME', FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).AsString );
  end;

  { Add the condition for the LastName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_LASTNAME', FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).AsString );
  end;

  { Add the Condition for the SocSECNr }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SOCSEC_NR' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_SOCSEC_NR', FSearchCriteriaDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString );
  end;

  { Add the Condition for the Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_NAME', FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).AsString );
  end;

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ROLEGROUP_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_ROLEGROUP_ID', FSearchCriteriaDataSet.FieldByName( 'F_ROLEGROUP_ID' ).AsInteger );
  end;

  { Add the condition for the Active }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddBlnEqualCondition ( aFilterString, 'F_ACTIVE', FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;

  Result := aFilterString;
end;

procedure TfrmRole_List.cxdbbeF_ROLEGROUP_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDURoleDataModule;
begin
  inherited;

  if ( Supports( FrameWorkDataModule, IEDURoleDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectRoleGroup( srcSearchCriteria.DataSet );
  end;
end;

procedure TfrmRole_List.FVBFFCListViewCreate(Sender: TObject);
var lcv: integer;
begin
  inherited;

  for lcv := 0 to cxgrdtblvList.ViewData.RowCount - 1 do
  begin
    if (cxgrdtblvList.ViewData.Rows[lcv] is TcxGridGroupRow)
        and (TcxGridGroupRow(cxgrdtblvList.ViewData.Rows[lcv]).Value = 'Contactpersoon')
    then
      TcxGridGroupRow(cxgrdtblvList.ViewData.Rows[lcv]).Expand(true);
  end;
end;

end.
