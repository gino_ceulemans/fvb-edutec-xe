{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_SCHOOLYEAR record.

  @Name       Form_Schoolyear_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/10/2005   slesage              Fixed Captions.
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Schoolyear_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxCheckBox,
  cxDBEdit, cxDropDownEdit, cxCalendar, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxControls, cxContainer, cxEdit, cxLabel, Menus, cxSplitter;

type
  TfrmSchoolyear_Record = class(TEDURecordView)
    cxlblF_SCHOOLYEAR_ID1: TFVBFFCLabel;
    cxdbseF_SCHOOLYEAR_ID: TFVBFFCDBSpinEdit;
    cxlblF_SCHOOLYEAR1: TFVBFFCLabel;
    cxdbteF_SCHOOLYEAR: TFVBFFCDBTextEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE: TFVBFFCDBDateEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE: TFVBFFCDBDateEdit;
    cxlblF_ACTIVE1: TFVBFFCLabel;
    csdbcbF_ACTIVE: TFVBFFCDBCheckBox;
    cxlblF_SCHOOLYEAR_ID2: TFVBFFCLabel;
    cxdblblF_SCHOOLYEAR_ID: TFVBFFCDBLabel;
    cxlblF_SCHOOLYEAR2: TFVBFFCLabel;
    cxdblblF_SCHOOLYEAR: TFVBFFCDBLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmSchoolyear_Record: TfrmSchoolyear_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Schoolyear, Data_EDUMainClient;

{ TfrmSchoolyear_Record }

function TfrmSchoolyear_Record.GetDetailName: String;
begin
  Result := cxdblblF_SCHOOLYEAR.Caption;
end;

end.