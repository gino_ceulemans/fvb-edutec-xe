inherited frmOrganisation_List: TfrmOrganisation_List
  Left = 211
  Top = 169
  Width = 1024
  Height = 664
  ActiveControl = cxdbteF_NAME
  Caption = 'Organisaties'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 596
    Width = 1016
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 800
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 912
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 1008
    Height = 588
    inherited cxgrdList: TcxGrid
      Top = 193
      Width = 1008
      Height = 395
      inherited cxgrdtblvList: TcxGridDBTableView
        OptionsSelection.MultiSelect = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_SYNERGY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SYNERGY_ID'
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Width = 107
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 200
        end
        object cxgrdtblvListF_RSZ_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_RSZ_NR'
          Width = 143
        end
        object cxgrdtblvListF_VAT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_VAT_NR'
        end
        object cxgrdtblvListF_ORGTYPE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGTYPE_NAME'
          Width = 200
        end
        object cxgrdtblvListF_STREET: TcxGridDBColumn
          DataBinding.FieldName = 'F_STREET'
          Width = 200
        end
        object cxgrdtblvListF_NUMBER: TcxGridDBColumn
          DataBinding.FieldName = 'F_NUMBER'
          Width = 77
        end
        object cxgrdtblvListF_MAILBOX: TcxGridDBColumn
          DataBinding.FieldName = 'F_MAILBOX'
          Visible = False
        end
        object cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE_ID'
          Visible = False
        end
        object cxgrdtblvListF_POSTALCODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_POSTALCODE'
          Width = 78
        end
        object cxgrdtblvListF_CITY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_CITY_NAME'
          Width = 200
        end
        object cxgrdtblvListF_COUNTRY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_ID'
          Visible = False
        end
        object cxgrdtblvListF_COUNTRY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_COUNTRY_NAME'
          Width = 200
        end
        object cxgrdtblvListF_PHONE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PHONE'
        end
        object cxgrdtblvListF_FAX: TcxGridDBColumn
          DataBinding.FieldName = 'F_FAX'
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Width = 66
        end
        object cxgrdtblvListF_ORGTYPE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGTYPE_ID'
          Visible = False
        end
        object cxgrdtblvListF_MANUALLY_ADDED: TcxGridDBColumn
          DataBinding.FieldName = 'F_MANUALLY_ADDED'
          Width = 89
        end
        object cxgrdtblvListF_CONSTRUCT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_CONSTRUCT_ID'
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 168
      Width = 1008
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 164
      Width = 1008
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 1008
      Height = 164
      ExpandedHeight = 164
      object cxlblF_NAME2: TFVBFFCLabel [0]
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Naam'
        FocusControl = cxdbteF_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_NAME: TFVBFFCDBTextEdit [1]
        Left = 80
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_NAME'
        DataBinding.DataField = 'F_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 1
        Width = 513
      end
      object cxlblVATNr: TFVBFFCLabel [2]
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'BTW Nr'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteVATNr: TFVBFFCDBTextEdit [3]
        Left = 80
        Top = 56
        DataBinding.DataField = 'F_VAT_NR'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 3
        Width = 217
      end
      object cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit [4]
        Left = 376
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        DataBinding.DataField = 'F_RSZ_NR'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 4
        Width = 216
      end
      object cxlblSocSecNr: TFVBFFCLabel [5]
        Left = 304
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'RSZ nr'
        FocusControl = cxdbteF_SOCSEC_NR
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxlblF_ORGTYPE_NAME1: TFVBFFCLabel [6]
        Left = 8
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_LANGUAGE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Type'
        FocusControl = cxdbbeF_ORGTYPE_NAME
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_ORGTYPE_NAME: TFVBFFCDBButtonEdit [7]
        Left = 80
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_LANGUAGE_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_ORGTYPE_NAME'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_ORGTYPE_NAMEPropertiesButtonClick
        TabOrder = 7
        Width = 216
      end
      object cxlblPostalCode: TFVBFFCLabel [8]
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_POSTALCODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Postcode'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbtePostalCode: TFVBFFCDBTextEdit [9]
        Left = 80
        Top = 80
        DataBinding.DataField = 'F_POSTALCODE'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 9
        Width = 217
      end
      object cxlblCity: TFVBFFCLabel [10]
        Left = 304
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Gemeente'
        FocusControl = cxdbteCity
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteCity: TFVBFFCDBTextEdit [11]
        Left = 376
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmCenInfrastructure_Record.F_CITY_NAME'
        DataBinding.DataField = 'F_CITY_NAME'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 11
        Width = 216
      end
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 133
        Width = 1006
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 838
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 926
        end
      end
      object cxlblF_ACTIVE: TFVBFFCLabel
        Left = 304
        Top = 104
        HelpType = htKeyword
        HelpKeyword = 'frmInstructor_Record.F_FIRSTNAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox
        Left = 376
        Top = 104
        RepositoryItem = dtmEDUMainClient.cxeriActive
        DataBinding.DataField = 'F_ACTIVE'
        DataBinding.DataSource = srcSearchCriteria
        Properties.Items = <>
        TabOrder = 14
        Width = 216
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 1016
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 592
    Width = 1016
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 1012
    Height = 588
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 588
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmOrganisation.cdsList
    Left = 48
    Top = 328
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    Left = 344
    Top = 456
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxbbEdit: TdxBarButton
      ShortCut = 16497
    end
    inherited dxbbAdd: TdxBarButton
      ShortCut = 16429
    end
    inherited dxbbDelete: TdxBarButton
      ShortCut = 16430
    end
    inherited dxbbRefresh: TdxBarButton
      ShortCut = 16500
    end
    object dxBarButton1: TdxBarButton
      Action = actJoinOrganisations
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = actJoinPersons
      Category = 0
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBarButton1
        Visible = True
      end
      item
        Item = dxBarButton2
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
    OnPopup = dxbpmnGridPopup
    Left = 144
    Top = 360
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    Left = 160
    Top = 438
  end
  object alOrganisatie: TFVBFFCActionList
    Left = 240
    Top = 382
    object actJoinOrganisations: TAction
      Caption = 'Organisaties samenvoegen...'
      OnExecute = actJoinOrganisationsExecute
    end
    object actJoinPersons: TAction
      Caption = 'Functies samenvoegen ...'
      OnExecute = actJoinPersonsExecute
    end
  end
end
