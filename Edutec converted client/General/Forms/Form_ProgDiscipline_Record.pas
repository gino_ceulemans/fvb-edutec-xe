{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_PROG_PROFESSION record.

  @Name       Form_ProgDiscipline_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_ProgDiscipline_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmProgDiscipline_Record = class(TEDURecordView)
    cxlblF_PROFESSION_ID1: TFVBFFCLabel;
    cxdblblF_PROFESSION_ID: TFVBFFCDBLabel;
    cxlblF_PROFESSION_NAME1: TFVBFFCLabel;
    cxdblblF_PROFESSION_NAME: TFVBFFCDBLabel;
    cxlblF_PROFESSION_ID2: TFVBFFCLabel;
    cxdbseF_PROFESSION_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmProgDiscipline_Record: TfrmProgDiscipline_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ProgDiscipline, Data_EDUMainClient;

{ TfrmProgDiscipline_Record }

function TfrmProgDiscipline_Record.GetDetailName: String;
begin
  Result := cxdblblF_PROFESSION_NAME.Caption;
end;

end.