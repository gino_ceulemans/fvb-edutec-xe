{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_TITLE record.

  @Name       Form_Title_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Title_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDURecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmTitle_Record = class(TEDURecordView)
    cxlblF_TITLE_ID1: TFVBFFCLabel;
    cxdblblF_TITLE_ID: TFVBFFCDBLabel;
    cxlblF_TITLE_NAME1: TFVBFFCLabel;
    cxdblblF_TITLE_NAME: TFVBFFCDBLabel;
    cxlblF_TITLE_ID2: TFVBFFCLabel;
    cxdbseF_TITLE_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Title, Data_EduMainClient;

{ TfrmTitle_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmTitle_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmTitle_Record.GetDetailName: String;
begin
  Result := cxdblblF_TITLE_NAME.Caption;
end;

end.