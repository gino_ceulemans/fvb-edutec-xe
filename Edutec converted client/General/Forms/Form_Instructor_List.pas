{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  T_DOC_INSTRUCTOR records.

  
  @Name       Form_Instructor_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  20/07/2005   sLesage              Finished the Implementation of the
                                    Filtering.
  19/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Instructor_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxButtonEdit, cxDBEdit, cxMaskEdit, cxSpinEdit, cxTextEdit, cxContainer,
  cxLabel, Menus, cxDropDownEdit, cxImageComboBox;

type
  TfrmInstructor_List = class(TEduListView)
    cxgrdtblvListF_INSTRUCTOR_ID: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_STREET: TcxGridDBColumn;
    cxgrdtblvListF_NUMBER: TcxGridDBColumn;
    cxgrdtblvListF_MAILBOX: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE: TcxGridDBColumn;
    cxgrdtblvListF_CITY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ACC_NR: TcxGridDBColumn;
    cxlblF_LASTNAME2: TFVBFFCLabel;
    cxdbteF_LASTNAME1: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME2: TFVBFFCLabel;
    cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit;
    cxlblF_INSTRUCTOR_ID2: TFVBFFCLabel;
    cxdbseF_INSTRUCTOR_ID1: TFVBFFCDBSpinEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_CODE1: TFVBFFCLabel;
    cxdbteF_CODE1: TFVBFFCDBTextEdit;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
    cxgrdtblvListF_PHONE: TcxGridDBColumn;
    cxgrdtblvListF_FAX: TcxGridDBColumn;
    cxgrdtblvListF_EMAIL: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_NAME: TcxGridDBColumn;
    procedure cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  protected
    function GetFilterString : String; override;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Instructor, Unit_FVBFFCInterfaces, Data_EduMainClient,
  Form_FVBFFCBaseListView, unit_EdutecInterfaces;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Language ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_List.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_List.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
  begin
    aInstructorDataModule.SelectLanguage( srcSearchCriteria.DataSet );
  end;
end;

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmInstructor_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmInstructor_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the condition for the User ID }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_INSTRUCTOR_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_INSTRUCTOR_ID', FSearchCriteriaDataSet.FieldByName( 'F_INSTRUCTOR_ID' ).AsInteger );
  end;

  { Add the condition for the FirstName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_FIRSTNAME', FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).AsString );
  end;

  { Add the condition for the LastName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_LASTNAME', FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).AsString );
  end;

  { Add the condition for the Language }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_LANGUAGE_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_LANGUAGE_ID', srcSearchCriteria.DataSet.FieldByName( 'F_LANGUAGE_ID' ).AsInteger );
  end;

  { Add the condition for the Code }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_CODE' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_CODE', FSearchCriteriaDataSet.FieldByName( 'F_CODE' ).AsString );
  end;

  { Add the condition for the Active }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddBlnEqualCondition ( aFilterString, 'F_ACTIVE', FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;

  Result := aFilterString;
end;

end.
