inherited frmSesWizard_Record: TfrmSesWizard_Record
  Left = 9
  Top = 187
  Width = 1017
  Height = 600
  Caption = 'Wizard'
  Constraints.MinHeight = 600
  Constraints.MinWidth = 1000
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 541
    Width = 1009
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 673
      inherited cxbtnOK: TFVBFFCButton
        Left = 225
        Top = 16
        Height = 21
        Action = acWizardExecute
        Visible = False
      end
      inherited cxbtnCancel: TFVBFFCButton
        Left = 7
      end
      inherited cxbtnApply: TFVBFFCButton
        Left = 117
        Top = 12
        Action = acWizardForeCast
        Visible = False
      end
      object cxbtnFVBFFCButton8: TFVBFFCButton
        Left = 117
        Top = 4
        Width = 104
        Height = 25
        Action = acWizardForeCast
        TabOrder = 3
      end
      object cxbtnFVBFFCButton9: TFVBFFCButton
        Left = 225
        Top = 4
        Width = 104
        Height = 25
        Action = acWizardExecute
        TabOrder = 4
      end
    end
    object cxbtnFVBFFCButton7: TFVBFFCButton
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      Action = acWizardSaveData
      TabOrder = 1
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 1009
    Height = 541
    inherited pnlRecord: TFVBFFCPanel
      Left = 8
      Width = 997
      Height = 541
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 997
        Height = 0
        Visible = False
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 997
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Top = 4
        Width = 997
        Height = 0
        Visible = False
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Top = 4
        Width = 997
        Height = 0
        Visible = False
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 537
        Width = 997
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Top = 4
        Width = 997
        Height = 533
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 988
          Height = 0
          Align = alNone
          Visible = False
        end
        inherited sbxMain: TScrollBox
          Top = 0
          Width = 988
          Height = 0
          Align = alNone
          Visible = False
        end
        object cxpgcResult: TFVBFFCPageControl
          Left = 0
          Top = 0
          Width = 997
          Height = 533
          ActivePage = cxtbsPersonen
          Align = alClient
          ShowFrame = True
          TabOrder = 2
          OnChange = cxpgcResultChange
          OnPageChanging = cxpgcResultPageChanging
          ClientRectBottom = 532
          ClientRectLeft = 1
          ClientRectRight = 996
          ClientRectTop = 26
          object cxtbsPersonen: TcxTabSheet
            Caption = 'Personen'
            ImageIndex = 0
            object pnlFVBFFCPanel1: TFVBFFCPanel
              Left = 0
              Top = 449
              Width = 995
              Height = 57
              Align = alBottom
              BevelOuter = bvNone
              BorderWidth = 0
              ParentColor = True
              TabOrder = 0
              StyleBackGround.BackColor = clInactiveCaption
              StyleBackGround.BackColor2 = clInactiveCaption
              StyleBackGround.Font.Charset = DEFAULT_CHARSET
              StyleBackGround.Font.Color = clWindowText
              StyleBackGround.Font.Height = -11
              StyleBackGround.Font.Name = 'Verdana'
              StyleBackGround.Font.Style = []
              StyleClientArea.BackColor = clInactiveCaption
              StyleClientArea.BackColor2 = clInactiveCaption
              StyleClientArea.Font.Charset = DEFAULT_CHARSET
              StyleClientArea.Font.Color = clWindowText
              StyleClientArea.Font.Height = -11
              StyleClientArea.Font.Name = 'Verdana'
              StyleClientArea.Font.Style = [fsBold]
              object cxbtnFVBFFCButton1: TFVBFFCButton
                Left = 8
                Top = 2
                Width = 240
                Height = 25
                Action = acSelectPerson
                TabOrder = 0
              end
              object cxbtnFVBFFCButton2: TFVBFFCButton
                Left = 8
                Top = 26
                Width = 240
                Height = 25
                Action = acCreatePersonAndRole
                TabOrder = 1
              end
              object cxbtnFVBFFCButton3: TFVBFFCButton
                Left = 373
                Top = 26
                Width = 240
                Height = 25
                Action = acEditRole
                TabOrder = 2
              end
              object cxbtnFVBFFCButton4: TFVBFFCButton
                Left = 373
                Top = 2
                Width = 240
                Height = 25
                Action = acEditPerson
                TabOrder = 3
              end
              object cxbtnFVBFFCButton5: TFVBFFCButton
                Left = 736
                Top = 26
                Width = 240
                Height = 25
                Action = acRemoveAll
                TabOrder = 4
              end
              object cxbtnFVBFFCButton6: TFVBFFCButton
                Left = 736
                Top = 2
                Width = 240
                Height = 25
                Action = acRemoveSelected
                TabOrder = 5
              end
            end
            object cxgrdList: TcxGrid
              Left = 0
              Top = 0
              Width = 995
              Height = 449
              Align = alClient
              Constraints.MinHeight = 200
              TabOrder = 1
              object cxgrdtblvList: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                NavigatorButtons.Insert.Visible = False
                NavigatorButtons.Delete.Visible = False
                NavigatorButtons.Edit.Visible = False
                NavigatorButtons.Post.Visible = False
                NavigatorButtons.Cancel.Visible = False
                NavigatorButtons.Filter.Enabled = False
                NavigatorButtons.Filter.Visible = False
                DataController.DataModeController.GridMode = True
                DataController.DataSource = srcSelectedRoles
                DataController.Filter.Options = [fcoCaseInsensitive]
                DataController.KeyFieldNames = 'F_ROLE_ID'
                DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.ColumnFiltering = False
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderAutoHeight = True
                OptionsView.Indicator = True
                Styles.StyleSheet = dtmEDUMainClient.GridTableViewStyleSheetDevExpress
                object cxgrdtblvListF_ROLE_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ROLE_ID'
                  Visible = False
                end
                object cxgrdtblvListF_PERSON_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_PERSON_ID'
                  Visible = False
                end
                object cxgrdtblvListF_LASTNAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_LASTNAME'
                  Width = 200
                end
                object cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_FIRSTNAME'
                  Width = 200
                end
                object cxgrdtblvListF_RSZ_NR: TcxGridDBColumn
                  DataBinding.FieldName = 'F_RSZ_NR'
                  Visible = False
                end
                object cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SOCSEC_NR'
                  Width = 94
                end
                object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION_ID'
                  Visible = False
                end
                object cxgrdtblvListF_ORGANISATION_NAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION_NAME'
                  Visible = False
                end
                object cxgrdtblvListF_ROLEGROUP_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ROLEGROUP_ID'
                  Visible = False
                end
                object cxgrdtblvListF_ROLEGROUP_NAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ROLEGROUP_NAME'
                  Width = 147
                end
                object cxgrdtblvListF_START_DATE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_START_DATE'
                  RepositoryItem = dtmEDUMainClient.cxeriDate
                end
                object cxgrdtblvListF_END_DATE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_END_DATE'
                  RepositoryItem = dtmEDUMainClient.cxeriDate
                end
                object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ACTIVE'
                  RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
                  Width = 69
                end
              end
              object cxgrdlvlList: TcxGridLevel
                GridView = cxgrdtblvList
              end
            end
          end
          object cxtbsProgramma: TcxTabSheet
            Caption = 'Programma'
            ImageIndex = 1
            object cxlblF_NAME2: TFVBFFCLabel
              Left = 8
              Top = 8
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Programma'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel
              Left = 8
              Top = 32
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMPLETE_USERNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Verantwoordelijke'
              FocusControl = cxdbbeF_COMPLETE_USERNAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit
              Left = 136
              Top = 32
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMPLETE_USERNAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookupReadOnly
              DataBinding.DataField = 'F_COMPLETE_USERNAME'
              DataBinding.DataSource = srcProgram
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              TabOrder = 2
              Width = 400
            end
            object cxlblF_PROFESSION_NAME1: TFVBFFCLabel
              Left = 8
              Top = 56
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_PROFESSION_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Discipline'
              FocusControl = cxdbbeF_PROFESSION_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbbeF_PROFESSION_NAME: TFVBFFCDBButtonEdit
              Left = 136
              Top = 56
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_PROFESSION_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookupReadOnly
              DataBinding.DataField = 'F_PROFESSION_NAME'
              DataBinding.DataSource = srcProgram
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              TabOrder = 4
              Width = 400
            end
            object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
              Left = 8
              Top = 152
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Taal'
              FocusControl = cxdbbeF_LANGUAGE_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
              Left = 136
              Top = 152
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LANGUAGE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookupReadOnly
              DataBinding.DataField = 'F_LANGUAGE_NAME'
              DataBinding.DataSource = srcProgram
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              TabOrder = 6
              Width = 193
            end
            object cxlblF_DURATION1: TFVBFFCLabel
              Left = 8
              Top = 80
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_DURATION'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Duur ( totaal in uren )'
              FocusControl = cxdbseF_DURATION
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbseF_DURATION: TFVBFFCDBSpinEdit
              Left = 136
              Top = 80
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_DURATION'
              DataBinding.DataField = 'F_DURATION'
              DataBinding.DataSource = srcProgram
              Properties.ReadOnly = True
              TabOrder = 8
              Width = 121
            end
            object cxlblF_DURATION_DAYS1: TFVBFFCLabel
              Left = 288
              Top = 80
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_DURATION_DAYS'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Duur ( in dagen )'
              FocusControl = cxdbseF_DURATION_DAYS
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbseF_DURATION_DAYS: TFVBFFCDBSpinEdit
              Left = 400
              Top = 80
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_DURATION_DAYS'
              DataBinding.DataField = 'F_DURATION_DAYS'
              DataBinding.DataSource = srcProgram
              Properties.ReadOnly = True
              TabOrder = 10
              Width = 136
            end
            object cxlblF_ENROLMENT_DATE1: TFVBFFCLabel
              Left = 8
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_ENROLMENT_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Inschrijvingsdatum'
              FocusControl = cxdbseF_ENROLMENT_DATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbseF_ENROLMENT_DATE: TFVBFFCDBSpinEdit
              Left = 136
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_ENROLMENT_DATE'
              DataBinding.DataField = 'F_ENROLMENT_DATE'
              DataBinding.DataSource = srcProgram
              Properties.ReadOnly = True
              TabOrder = 12
              Width = 152
            end
            object cxlblF_CONTROL_DATE1: TFVBFFCLabel
              Left = 290
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_CONTROL_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Controledatum'
              FocusControl = cxdbseF_CONTROL_DATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbseF_CONTROL_DATE: TFVBFFCDBSpinEdit
              Left = 384
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_CONTROL_DATE'
              DataBinding.DataField = 'F_CONTROL_DATE'
              DataBinding.DataSource = srcProgram
              Properties.ReadOnly = True
              TabOrder = 14
              Width = 152
            end
            object cxlblF_SHORT_DESC1: TFVBFFCLabel
              Left = 8
              Top = 104
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_SHORT_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Korte Omschrijving'
              FocusControl = cxdbteF_SHORT_DESC
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbteF_SHORT_DESC: TFVBFFCDBTextEdit
              Left = 136
              Top = 104
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_SHORT_DESC'
              DataBinding.DataField = 'F_SHORT_DESC'
              DataBinding.DataSource = srcProgram
              Properties.ReadOnly = True
              TabOrder = 16
              Width = 401
            end
            object cxlblF_LONG_DESC1: TFVBFFCLabel
              Left = 8
              Top = 200
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Omschrijving'
              FocusControl = cxdbmmoF_LONG_DESC
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoF_LONG_DESC: TFVBFFCDBMemo
              Left = 8
              Top = 216
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_LONG_DESC'
              DataBinding.DataSource = srcProgram
              TabOrder = 18
              Height = 80
              Width = 320
            end
            object cxlblF_COMMENT1: TFVBFFCLabel
              Left = 336
              Top = 200
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Commentaar'
              FocusControl = cxdbmmoF_COMMENT
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoF_COMMENT: TFVBFFCDBMemo
              Left = 336
              Top = 216
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_COMMENT'
              DataBinding.DataSource = srcProgram
              TabOrder = 20
              Height = 80
              Width = 320
            end
            object cxlblVoorkennis: TFVBFFCLabel
              Left = 8
              Top = 392
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Voorkennis'
              FocusControl = cxdbmmoVoorkennis
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoVoorkennis: TFVBFFCDBMemo
              Left = 8
              Top = 408
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_KNOWLEDGE'
              DataBinding.DataSource = srcProgram
              TabOrder = 22
              Height = 80
              Width = 320
            end
            object cxlblVereisteInfrastructuur: TFVBFFCLabel
              Left = 336
              Top = 392
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Vereiste Infrastructuur'
              FocusControl = cxdbmmoVereisteInfrastructuur
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoVereisteInfrastructuur: TFVBFFCDBMemo
              Left = 336
              Top = 408
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_INFRASTRUCTURE'
              DataBinding.DataSource = srcProgram
              TabOrder = 24
              Height = 80
              Width = 320
            end
            object cxdbmmoVeiligheidsuitrusting: TFVBFFCDBMemo
              Left = 336
              Top = 312
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_SECURITY_EQ'
              DataBinding.DataSource = srcProgram
              TabOrder = 25
              Height = 80
              Width = 320
            end
            object cxlblVeilidgheidsuitrusting: TFVBFFCLabel
              Left = 336
              Top = 296
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Veiligheidsuitrusting'
              FocusControl = cxdbmmoVeiligheidsuitrusting
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoGrondstoffen: TFVBFFCDBMemo
              Left = 664
              Top = 312
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_RAW_MATERIAL'
              DataBinding.DataSource = srcProgram
              TabOrder = 27
              Height = 80
              Width = 320
            end
            object cxlblGrondstoffen: TFVBFFCLabel
              Left = 664
              Top = 296
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Grondstoffen'
              FocusControl = cxdbmmoGrondstoffen
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxlblGereedschap: TFVBFFCLabel
              Left = 664
              Top = 200
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Gereedschap'
              FocusControl = cxdbmmoGereedschap
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoGereedschap: TFVBFFCDBMemo
              Left = 664
              Top = 216
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_LONG_DESC'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_EQUIPMENT'
              DataBinding.DataSource = srcProgram
              TabOrder = 30
              Height = 80
              Width = 320
            end
            object cxlblDidactischeHulpmiddelen: TFVBFFCLabel
              Left = 8
              Top = 296
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Didactische Hulpmiddelen'
              FocusControl = cxdbmmoDidactischeHulpmiddelen
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Style.TransparentBorder = True
            end
            object cxdbmmoDidactischeHulpmiddelen: TFVBFFCDBMemo
              Left = 8
              Top = 312
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriMemoReadOnly
              DataBinding.DataField = 'F_DIDAC_MATERIAL'
              DataBinding.DataSource = srcProgram
              TabOrder = 32
              Height = 80
              Width = 320
            end
            object cxdbbeF_PROGRAM_NAME: TFVBFFCDBButtonEdit
              Left = 136
              Top = 8
              HelpType = htKeyword
              HelpKeyword = 'frmProgProgram_Record.F_COMPLETE_USERNAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_NAME'
              DataBinding.DataSource = srcProgram
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_PROGRAM_NAMEPropertiesButtonClick
              TabOrder = 33
              Width = 400
            end
          end
          object cxtbsSessie: TcxTabSheet
            Caption = 'Sessie / Inschrijving'
            ImageIndex = 2
            object bvlBevel1: TBevel
              Left = 0
              Top = 24
              Width = 995
              Height = 185
              Align = alTop
              Shape = bsSpacer
            end
            object cxlblF_INFRASTRUCTURE_NAME1: TFVBFFCLabel
              Left = 8
              Top = 31
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_INFRASTRUCTURE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Locatie'
              FocusControl = cxdbbeF_INFRASTRUCTURE_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_INFRASTRUCTURE_NAME: TFVBFFCDBButtonEdit
              Left = 112
              Top = 31
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_INFRASTRUCTURE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_INFRASTRUCTURE_NAME'
              DataBinding.DataSource = srcSession
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick
              TabOrder = 1
              Width = 432
            end
            object cxlblFVBFFCLabel1: TFVBFFCLabel
              Left = 8
              Top = 56
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Sessie Naam'
              FocusControl = cxdbteF_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_NAME: TFVBFFCDBTextEdit
              Left = 112
              Top = 56
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_NAME'
              DataBinding.DataField = 'F_SESSION_NAME'
              DataBinding.DataSource = srcSession
              TabOrder = 3
              Width = 432
            end
            object cxlblF_CODE1: TFVBFFCLabel
              Left = 8
              Top = 80
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_CODE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Referentie'
              FocusControl = cxdbteF_CODE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CODE: TFVBFFCDBTextEdit
              Left = 112
              Top = 80
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_CODE'
              DataBinding.DataField = 'F_SESSION_CODE'
              DataBinding.DataSource = srcSession
              TabOrder = 5
              Width = 432
            end
            object cxlblF_START_DATE1: TFVBFFCLabel
              Left = 8
              Top = 104
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_START_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Start Datum'
              FocusControl = cxdbdeF_START_DATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_START_DATE: TFVBFFCDBDateEdit
              Left = 112
              Top = 104
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_START_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriDate
              DataBinding.DataField = 'F_SESSION_START_DATE'
              DataBinding.DataSource = srcSession
              TabOrder = 7
              Width = 153
            end
            object cxlblF_END_DATE1: TFVBFFCLabel
              Left = 280
              Top = 104
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_END_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Eind Datum'
              FocusControl = cxdbdeF_END_DATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_END_DATE: TFVBFFCDBDateEdit
              Left = 390
              Top = 104
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_END_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriDate
              DataBinding.DataField = 'F_SESSION_END_DATE'
              DataBinding.DataSource = srcSession
              TabOrder = 9
              Width = 154
            end
            object cxlblF_START_HOUR1: TFVBFFCLabel
              Left = 8
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_START_HOUR'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Start uur'
              FocusControl = cxdbteF_START_HOUR
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_START_HOUR: TFVBFFCDBTextEdit
              Left = 112
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_START_HOUR'
              DataBinding.DataField = 'F_SESSION_START_HOUR'
              DataBinding.DataSource = srcSession
              TabOrder = 11
              Width = 153
            end
            object cxlblF_TRANSPORT_INS1: TFVBFFCLabel
              Left = 8
              Top = 152
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_TRANSPORT_INS'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Verplaatsing'
              FocusControl = cxdbteF_TRANSPORT_INS
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_TRANSPORT_INS: TFVBFFCDBTextEdit
              Left = 112
              Top = 152
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_TRANSPORT_INS'
              DataBinding.DataField = 'F_SESSION_TRANSPORT_INS'
              DataBinding.DataSource = srcSession
              TabOrder = 13
              Width = 432
            end
            object cxlblFVBFFCLabel2: TFVBFFCLabel
              Left = 552
              Top = 32
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Opmerking'
              FocusControl = cxdbmmoFVBFFCDBMemo1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbmmoFVBFFCDBMemo1: TFVBFFCDBMemo
              Left = 552
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmSesSession_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriMemo
              DataBinding.DataField = 'F_SESSION_COMMENT'
              DataBinding.DataSource = srcSession
              TabOrder = 15
              Height = 145
              Width = 417
            end
            object pnlSessionHeader: TFVBFFCPanel
              Left = 0
              Top = 0
              Width = 995
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              BorderWidth = 0
              Caption = 'Sessie Informatie'
              ParentColor = True
              TabOrder = 16
              Visible = False
              StyleBackGround.BackColor = clInactiveCaption
              StyleBackGround.BackColor2 = clInactiveCaption
              StyleBackGround.Font.Charset = DEFAULT_CHARSET
              StyleBackGround.Font.Color = clHighlightText
              StyleBackGround.Font.Height = -11
              StyleBackGround.Font.Name = 'Verdana'
              StyleBackGround.Font.Style = []
              StyleClientArea.BackColor = clMenuHighlight
              StyleClientArea.BackColor2 = clMenuHighlight
              StyleClientArea.Font.Charset = DEFAULT_CHARSET
              StyleClientArea.Font.Color = clHighlightText
              StyleClientArea.Font.Height = -11
              StyleClientArea.Font.Name = 'Verdana'
              StyleClientArea.Font.Style = [fsBold]
            end
            object pnlFVBFFCPanel2: TFVBFFCPanel
              Left = 0
              Top = 209
              Width = 995
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              BorderWidth = 0
              Caption = 'Inschrijving Informatie'
              ParentColor = True
              TabOrder = 17
              Visible = False
              StyleBackGround.BackColor = clInactiveCaption
              StyleBackGround.BackColor2 = clInactiveCaption
              StyleBackGround.Font.Charset = DEFAULT_CHARSET
              StyleBackGround.Font.Color = clHighlightText
              StyleBackGround.Font.Height = -11
              StyleBackGround.Font.Name = 'Verdana'
              StyleBackGround.Font.Style = []
              StyleClientArea.BackColor = clMenuHighlight
              StyleClientArea.BackColor2 = clMenuHighlight
              StyleClientArea.Font.Charset = DEFAULT_CHARSET
              StyleClientArea.Font.Color = clHighlightText
              StyleClientArea.Font.Height = -11
              StyleClientArea.Font.Name = 'Verdana'
              StyleClientArea.Font.Style = [fsBold]
            end
            object cxlblF_CERTIFICATE: TFVBFFCLabel
              Left = 216
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_CERTIFICATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Certificaat'
              FocusControl = cxdbcbF_CERTIFICATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_EVALUATION: TFVBFFCLabel
              Left = 112
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_EVALUATION'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Evaluatie'
              FocusControl = cxdbcbF_EVALUATION
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcbF_EVALUATION: TFVBFFCDBCheckBox
              Left = 184
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_EVALUATION'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'cxdbcbF_EVALUATION'
              DataBinding.DataField = 'F_ENROL_EVALUATION'
              DataBinding.DataSource = srcSession
              ParentColor = False
              TabOrder = 19
              Width = 21
            end
            object cxdbcbF_CERTIFICATE: TFVBFFCDBCheckBox
              Left = 288
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_CERTIFICATE'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'cxdbcbF_CERTIFICATE'
              DataBinding.DataField = 'F_ENROL_CERTIFICATE'
              DataBinding.DataSource = srcSession
              ParentColor = False
              TabOrder = 22
              Width = 21
            end
            object cxlblF_INVOICED: TFVBFFCLabel
              Left = 856
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_INVOICED'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Gefaktureerd'
              FocusControl = cxdbcbF_INVOICED
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Visible = False
            end
            object cxlblF_LAST_MINUTE: TFVBFFCLabel
              Left = 320
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_LAST_MINUTE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Last Minute'
              FocusControl = cxdbcbF_LAST_MINUTE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbcbF_INVOICED: TFVBFFCDBCheckBox
              Left = 944
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_INVOICED'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'cxdbcbF_INVOICED'
              DataBinding.DataField = 'F_ENROL_INVOICED'
              DataBinding.DataSource = srcSession
              ParentColor = False
              TabOrder = 21
              Visible = False
              Width = 21
            end
            object cxdbcbF_LAST_MINUTE: TFVBFFCDBCheckBox
              Left = 408
              Top = 240
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_LAST_MINUTE'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              Caption = 'cxdbcbF_LAST_MINUTE'
              DataBinding.DataField = 'F_ENROL_LAST_MINUTE'
              DataBinding.DataSource = srcSession
              ParentColor = False
              TabOrder = 25
              Width = 21
            end
            object cxdbmmoFVBFFCDBMemo2: TFVBFFCDBMemo
              Left = 8
              Top = 288
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriMemo
              DataBinding.DataField = 'F_ENROL_COMMENT'
              DataBinding.DataSource = srcSession
              TabOrder = 26
              Height = 201
              Width = 961
            end
            object cxlblF_COMMENT: TFVBFFCLabel
              Left = 8
              Top = 264
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_COMMENT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Commentaar'
              FocusControl = cxdbmmoFVBFFCDBMemo2
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_PRICE_REDUCTION: TFVBFFCLabel
              Left = 280
              Top = 128
              HelpType = htKeyword
              HelpKeyword = 'frmEnrolment_Record.F_PRICE_REDUCTION'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Tussenkomst'
              FocusControl = cxdbicbF_PRICE_REDUCTION
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbicbF_PRICE_REDUCTION: TFVBFFCDBImageComboBox
              Left = 390
              Top = 128
              DataBinding.DataField = 'F_PRICING_SCHEME'
              DataBinding.DataSource = srcProgram
              Properties.Alignment.Horz = taCenter
              Properties.Items = <
                item
                  Description = 'Geen'
                  ImageIndex = 0
                  Value = 0
                end
                item
                  Description = 'F.V.B.'
                  Value = 1
                end
                item
                  Description = 'Cevora'
                  Value = 2
                end>
              TabOrder = 29
              Width = 154
            end
          end
          object cxtbsForeCast: TcxTabSheet
            Caption = 'ForeCast'
            ImageIndex = 3
            object cxgrdForeCast: TcxGrid
              Left = 0
              Top = 0
              Width = 995
              Height = 506
              Align = alClient
              Constraints.MinHeight = 200
              TabOrder = 0
              object cxgrdtblvForeCast: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                NavigatorButtons.Insert.Visible = False
                NavigatorButtons.Delete.Visible = False
                NavigatorButtons.Edit.Visible = False
                NavigatorButtons.Post.Visible = False
                NavigatorButtons.Cancel.Visible = False
                NavigatorButtons.Filter.Enabled = False
                NavigatorButtons.Filter.Visible = False
                DataController.DataModeController.GridMode = True
                DataController.DataSource = srcForeCast
                DataController.Filter.Options = [fcoCaseInsensitive]
                DataController.KeyFieldNames = 'F_SESWIZARD_ID'
                DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.ColumnFiltering = False
                OptionsCustomize.ColumnGrouping = False
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsView.CellAutoHeight = True
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderAutoHeight = True
                OptionsView.Indicator = True
                Styles.StyleSheet = dtmEDUMainClient.GridTableViewStyleSheetDevExpress
                object cxgrdtblvForeCastF_SESSION_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_ID'
                  Options.Editing = False
                  Width = 45
                end
                object cxgrdtblvForeCastF_COMPANY_NAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_COMPANY_NAME'
                  Options.Editing = False
                  Width = 130
                end
                object cxgrdtblvForeCastF_FIRSTNAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_FIRSTNAME'
                  Options.Editing = False
                  Width = 135
                end
                object cxgrdtblvForeCastF_LASTNAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_LASTNAME'
                  Options.Editing = False
                  Width = 160
                end
                object cxgrdtblvForeCastF_SESSION_START_DATE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_START_DATE'
                  RepositoryItem = dtmEDUMainClient.cxeriDate
                  Options.Editing = False
                  Width = 66
                end
                object cxgrdtblvForeCastF_SESSION_END_DATE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_END_DATE'
                  RepositoryItem = dtmEDUMainClient.cxeriDate
                  Options.Editing = False
                  Width = 61
                end
                object cxgrdtblvForeCastF_ENDROL_ID: TcxGridDBColumn
                  Caption = 'Insch. ID'
                  DataBinding.FieldName = 'F_ENROL_ID'
                  Options.Editing = False
                  Width = 66
                end
                object cxgrdtblvForeCastF_SES_CREATE: TcxGridDBColumn
                  Caption = 'Creatie Sessie'
                  DataBinding.FieldName = 'F_SES_CREATE'
                  RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
                  Options.Filtering = False
                  Options.Grouping = False
                  Options.Sorting = False
                  Width = 50
                end
                object cxgrdtblvForeCastF_SES_UPDATE: TcxGridDBColumn
                  Caption = 'Update Sessie'
                  DataBinding.FieldName = 'F_SES_UPDATE'
                  RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
                  Options.Filtering = False
                  Options.Grouping = False
                  Options.Sorting = False
                  Width = 50
                end
                object cxgrdtblvForeCastF_ENR_CREATE: TcxGridDBColumn
                  Caption = 'Creatie Insch.'
                  DataBinding.FieldName = 'F_ENR_CREATE'
                  RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
                  Options.Filtering = False
                  Options.Grouping = False
                  Options.Sorting = False
                  Width = 50
                end
                object cxgrdtblvForeCastF_ENR_UPDATE: TcxGridDBColumn
                  Caption = 'Update Insch.'
                  DataBinding.FieldName = 'F_ENR_UPDATE'
                  RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
                  Options.Filtering = False
                  Options.Grouping = False
                  Options.Sorting = False
                  Width = 50
                end
                object cxgrdtblvForeCastF_FORECAST_COMMENT: TcxGridDBColumn
                  DataBinding.FieldName = 'F_FORECAST_COMMENT'
                  Options.Editing = False
                  Options.Filtering = False
                  Options.Grouping = False
                  Options.Sorting = False
                  Width = 69
                end
              end
              object cxgrdlvlForeCast: TcxGridLevel
                GridView = cxgrdtblvForeCast
              end
            end
          end
          object cxtbsResult: TcxTabSheet
            Caption = 'Result'
            ImageIndex = 4
            object cxgrdResult: TcxGrid
              Left = 0
              Top = 0
              Width = 995
              Height = 506
              Align = alClient
              Constraints.MinHeight = 200
              TabOrder = 0
              object cxgrdtblvCxGridDBTableView2: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                NavigatorButtons.Insert.Visible = False
                NavigatorButtons.Delete.Visible = False
                NavigatorButtons.Edit.Visible = False
                NavigatorButtons.Post.Visible = False
                NavigatorButtons.Cancel.Visible = False
                NavigatorButtons.Filter.Enabled = False
                NavigatorButtons.Filter.Visible = False
                DataController.DataModeController.GridMode = True
                DataController.DataSource = srcResult
                DataController.DetailKeyFieldNames = 'F_SESWIZARD_ID'
                DataController.Filter.Options = [fcoCaseInsensitive]
                DataController.KeyFieldNames = 'F_SESWIZARD_ID'
                DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsCustomize.ColumnFiltering = False
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsSelection.CellSelect = False
                OptionsView.ColumnAutoWidth = True
                OptionsView.GroupByBox = False
                OptionsView.HeaderAutoHeight = True
                OptionsView.Indicator = True
                Styles.StyleSheet = dtmEDUMainClient.GridTableViewStyleSheetDevExpress
                object cxgrdtblvCxGridDBTableView2F_SESSION_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_ID'
                  Width = 45
                end
                object cxgrdtblvCxGridDBTableView2F_FIRSTNAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_FIRSTNAME'
                  Width = 159
                end
                object cxgrdtblvCxGridDBTableView2F_LASTNAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_LASTNAME'
                  Width = 166
                end
                object cxgrdtblvCxGridDBTableView2F_SESSION_START_DATE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_START_DATE'
                  Width = 76
                end
                object cxgrdtblvCxGridDBTableView2F_SESSION_END_DATE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_END_DATE'
                  Width = 72
                end
                object cxgrdtblvCxGridDBTableView2F_ENROL_ID: TcxGridDBColumn
                  Caption = 'Insch. ID'
                  DataBinding.FieldName = 'F_ENROL_ID'
                  Width = 66
                end
                object cxgrdtblvCxGridDBTableView2F_ENROL_HOURS: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ENROL_HOURS'
                  Width = 33
                end
                object cxgrdtblvCxGridDBTableView2F_SES_CREATE: TcxGridDBColumn
                  Caption = 'Creatie Sessie'
                  DataBinding.FieldName = 'F_SES_CREATE'
                  Width = 89
                end
                object cxgrdtblvCxGridDBTableView2F_SES_UPDATE: TcxGridDBColumn
                  Caption = 'Update Sessie'
                  DataBinding.FieldName = 'F_SES_UPDATE'
                  Width = 87
                end
                object cxgrdtblvCxGridDBTableView2F_ENR_CREATE: TcxGridDBColumn
                  Caption = 'Creatie Insch.'
                  DataBinding.FieldName = 'F_ENR_CREATE'
                  Width = 87
                end
                object cxgrdtblvCxGridDBTableView2F_ENR_UPDATE: TcxGridDBColumn
                  Caption = 'Update Insch.'
                  DataBinding.FieldName = 'F_ENR_UPDATE'
                  Width = 85
                end
              end
              object cxgrdlvlCxGridLevel2: TcxGridLevel
                GridView = cxgrdtblvCxGridDBTableView2
              end
            end
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 1005
      Height = 541
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Left = 8
      Width = 0
      Height = 541
      Visible = False
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Left = 0
      Height = 541
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmSesWizard.cdsSessionWizard
    Left = 404
    Top = 536
  end
  inherited alRecordView: TFVBFFCActionList
    Left = 632
    Top = 536
    object acSelectPerson: TAction
      Category = 'Person'
      Caption = 'Selecteer een andere Persoon'
      OnExecute = acSelectPersonExecute
    end
    object acEditPerson: TAction
      Category = 'Person'
      Caption = 'Wijzig geselecteerde Persoon'
      OnExecute = acEditPersonExecute
    end
    object acEditRole: TAction
      Category = 'Person'
      Caption = 'Wijzig geselecteerde Rol'
      OnExecute = acEditRoleExecute
    end
    object acRemoveSelected: TAction
      Category = 'Person'
      Caption = 'Verwijder van te gebruiken'
      OnExecute = acRemoveSelectedExecute
    end
    object acRemoveAll: TAction
      Category = 'Person'
      Caption = 'Verwijder alle van te gebruiken'
      OnExecute = acRemoveAllExecute
    end
    object acCreatePersonAndRole: TAction
      Category = 'Person'
      Caption = 'Cre'#235'er een nieuwe Persoon en Rol'
      OnExecute = acCreatePersonAndRoleExecute
    end
    object acWizardForeCast: TAction
      Category = 'Wizard'
      Caption = 'ForeCast'
      OnExecute = acWizardForeCastExecute
      OnUpdate = acWizardForeCastUpdate
    end
    object acWizardExecute: TAction
      Category = 'Wizard'
      Caption = 'Execute'
      OnExecute = cxbtnOKClick
      OnUpdate = acWizardExecuteUpdate
    end
    object acWizardSaveData: TAction
      Category = 'Wizard'
      Caption = 'Save'
      OnExecute = acWizardSaveDataExecute
    end
  end
  object srcSelectedRoles: TFVBFFCDataSource
    DataSet = dtmSesWizard.cdsPersonen
    Left = 176
    Top = 534
  end
  object srcSession: TFVBFFCDataSource
    DataSet = dtmSesWizard.cdsSessie
    Left = 240
    Top = 534
  end
  object srcProgram: TFVBFFCDataSource
    DataSet = dtmSesWizard.cdsProgram
    Left = 208
    Top = 534
  end
  object srcForeCast: TFVBFFCDataSource
    DataSet = dtmSesWizard.cdsSessionWizard
    Left = 272
    Top = 534
  end
  object srcResult: TFVBFFCDataSource
    DataSet = dtmSesWizard.cdsSessionWizardResult
    Left = 304
    Top = 534
  end
  object dxcpGridPrinter: TFVBFFCComponentPrinter
    CurrentLink = dxcpGridPrinterLink1
    Version = 0
    Left = 600
    Top = 534
    object dxcpGridPrinterLink1: TdxGridReportLink
      Component = cxgrdResult
      PrinterPage.DMPaper = 1
      PrinterPage.Footer = 5080
      PrinterPage.Header = 5080
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 215900
      PrinterPage.PageSize.Y = 279400
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
end
