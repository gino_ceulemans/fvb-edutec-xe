{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SYS_USER record.

  @Name       Form_User_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/03/2008   ivdbossche           Added field F_DEFAULT_INFO_CONFIRMATION;
  24/07/2007   ivdbossche           Added field Path Confirmation reports and procedure OnChooseFolder
  18/07/2007   ivdbossche           Added fields 'Printer Cerficaten', 'Printer brieven' and procedures GetSelPrinter, OnChoosePrinter
  04/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_User_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDropDownEdit, cxCalendar,
  cxDBEdit, cxCheckBox, cxButtonEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxGroupBox, cxDBLabel, cxControls, cxContainer, cxEdit, cxLabel, Menus,
  cxSplitter, Printers, Unit_FVBFFCInterfaces, Unit_PPWFrameWorkClasses,
  dxPrnDev, dxPrnDlg, cxMemo;

type
  TfrmUser_Record = class(TEDURecordView)
    cxlblF_USER_ID1: TFVBFFCLabel;
    cxdblblF_USER_ID: TFVBFFCDBLabel;
    cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel;
    cxdblblF_COMPLETE_USERNAME: TFVBFFCDBLabel;
    cxgbAlgemeen: TFVBFFCGroupBox;
    cxlblF_USER_ID2: TFVBFFCLabel;
    cxdbseF_USER_ID: TFVBFFCDBSpinEdit;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdbteF_LASTNAME: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit;
    cxlblF_GENDER_ID1: TFVBFFCLabel;
    cxgbContactInfo: TFVBFFCGroupBox;
    cxlblF_PHONE_INT1: TFVBFFCLabel;
    cxdbteF_PHONE_INT: TFVBFFCDBTextEdit;
    cxlblF_PHONE_EXT1: TFVBFFCLabel;
    cxdbteF_PHONE_EXT: TFVBFFCDBTextEdit;
    cxlblF_GSM1: TFVBFFCLabel;
    cxdbteF_GSM: TFVBFFCDBTextEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    cxdbteF_EMAIL: TFVBFFCDBTextEdit;
    cxgbLogin: TFVBFFCGroupBox;
    cxlblF_DOMAIN1: TFVBFFCLabel;
    cxdbteF_DOMAIN: TFVBFFCDBTextEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxdbteF_LOGIN_ID: TFVBFFCDBTextEdit;
    cxlblF_LOGIN_ID1: TFVBFFCLabel;
    cxgbActivatie: TFVBFFCGroupBox;
    cxlblF_ACTIVE1: TFVBFFCLabel;
    csdbcbF_ACTIVE: TFVBFFCDBCheckBox;
    cxlblF_LOCKED1: TFVBFFCLabel;
    csdbcbF_LOCKED: TFVBFFCDBCheckBox;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE: TFVBFFCDBDateEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE: TFVBFFCDBDateEdit;
    cxlblF_LOGIN_TRY1: TFVBFFCLabel;
    cxdbseF_LOGIN_TRY: TFVBFFCDBSpinEdit;
    acShowUserProfiles: TAction;
    dxnbiProfielen: TdxNavBarItem;
    cxdbbeF_GENDER_NAME: TFVBFFCDBButtonEdit;
    cxlblF_PASSWORD: TFVBFFCLabel;
    cxdbteF_PASSWORD: TFVBFFCDBTextEdit;
    FVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlblF_PRINTER_CERTIFICATE: TFVBFFCLabel;
    cxlblF_PRINTER_LETTER: TFVBFFCLabel;
    PrinterSetupDialog: TPrinterSetupDialog;
    FVBFFCGroupBox2: TFVBFFCGroupBox;
    FVBFFCLabel4: TFVBFFCLabel;
    FVBFFCDBButtonEditF_PRINTER_LETTER: TFVBFFCDBButtonEdit;
    FVBFFCDBButtonEditF_PRINTER_CERTIFICATE: TFVBFFCDBButtonEdit;
    FVBFFCDBButtonEditF_PATH_CONFIRMATIONS: TFVBFFCDBButtonEdit;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCDBButtonEditF_PATH_INVOICES: TFVBFFCDBButtonEdit;
    cxlblF_PRINTER_EVALUATION: TFVBFFCLabel;
    FVBFFCDBButtonEditF_PRINTER_EVALUATION: TFVBFFCDBButtonEdit;
    cxlblF_PATH_CRREPORTS: TFVBFFCLabel;
    FVBFFCDBButtonEditF_PATH_CRREPORTS: TFVBFFCDBButtonEdit;
    FVBFFCGroupBox3: TFVBFFCGroupBox;
    cxlblNOTIFICATION: TFVBFFCLabel;
    cxdbbeF_EMAIL_PUBLIC_FOLDER: TFVBFFCDBTextEdit;
    FVBFFCGroupBox4: TFVBFFCGroupBox;
    FVBFFCLabel1: TFVBFFCLabel;
    cxdbbeF_DEFAULT_INFO_CONFIRMATION: TFVBFFCDBTextEdit;
    procedure acShowUserProfilesExecute(Sender: TObject);
    procedure cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_GENDER_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure OnChoosePrinter(Sender: TObject; AButtonIndex: Integer);
    procedure OnChooseFolder(Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
    function GetSelPrinter(ACurrentPrinter: string): string;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmUser_Record: TfrmUser_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_User, Data_EDUMainClient, unit_EdutecInterfaces, FileCtrl;

{ TfrmUser_Record }

function TfrmUser_Record.GetDetailName: String;
begin
  Result := cxdblblF_COMPLETE_USERNAME.Caption;
end;

procedure TfrmUser_Record.acShowUserProfilesExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmUserProfile', pnlRecordDetail );
end;

procedure TfrmUser_Record.cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUUserDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUUserDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectLanguage( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmUser_Record.cxdbbeF_GENDER_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUUserDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUUserDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowGender( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectGender( srcMain.DataSet );
    end;
  end;

end;

function TfrmUser_Record.GetSelPrinter(ACurrentPrinter: string): string;
var
  pr: string;
  i: integer;
begin
  // Set initial printer depending on already selected printer...
  for i:=0 to Printer.Printers.Count-1 do begin
    if Printer.Printers[i] = ACurrentPrinter then begin
      Printer.PrinterIndex := i;
      break;
    end;
  end;

  if PrinterSetupDialog.Execute then begin
    if Printer.PrinterIndex > -1 then
      pr := Printer.Printers[Printer.PrinterIndex];
  end;

  Result := pr;

end;

procedure TfrmUser_Record.OnChoosePrinter(Sender: TObject; AButtonIndex: Integer);
var
  txtEdit: TFVBFFCDBButtonEdit;
  pr,current_printer: string;
  datafield: string;
begin
    if (Sender is TFVBFFCDBButtonEdit) then begin
      txtEdit := (Sender as TFVBFFCDBButtonEdit);

      if not (txtEdit.DataBinding.DataSource.DataSet.State in [dsInsert, dsEdit]) then
        txtEdit.DataBinding.DataSource.Edit;

      datafield := txtEdit.DataBinding.DataField; // Field which contains printersetting

      if not VarIsNull(txtEdit.DataBinding.DataSource.DataSet.FieldByName(datafield).Value) then
        current_printer := txtEdit.DataBinding.DataSource.DataSet.FieldByName(datafield).Value;

      pr := GetSelPrinter(current_printer);

      if pr <> '' then
        txtEdit.DataBinding.DataSource.DataSet.FieldByName(datafield).Value := pr;
    end;

end;

procedure TfrmUser_Record.OnChooseFolder(Sender: TObject; AButtonIndex: Integer);
var
  txtEdit: TFVBFFCDBButtonEdit;
  folder: string;
  datafield: string;
begin
  if (Sender is TFVBFFCDBButtonEdit) then begin
    txtEdit := (Sender as TFVBFFCDBButtonEdit);

    if not (txtEdit.DataBinding.DataSource.DataSet.State in [dsInsert, dsEdit]) then
      txtEdit.DataBinding.DataSource.Edit;

    datafield := txtEdit.DataBinding.DataField; // Field which contains folder setting

    if not VarIsNull(txtEdit.DataBinding.DataSource.DataSet.FieldByName(datafield).Value) then
      folder := txtEdit.DataBinding.DataSource.DataSet.FieldByName(datafield).AsString;

    if SelectDirectory(folder, [sdAllowCreate, sdPerformCreate, sdPrompt], 0) then
      txtEdit.DataBinding.DataSource.DataSet.FieldByName(datafield).Value := folder;

  end;

end;

end.
