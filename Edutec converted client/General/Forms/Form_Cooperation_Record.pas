{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_COOPERATION record.

  @Name       Form_Cooperation_Record
  @Author     tclaesen
  @Copyright  (c) 2009 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  02/07/2009   tclaesen              Initial creation of the Unit.
******************************************************************************}

unit Form_Cooperation_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDURecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmCooperation_Record = class(TEDURecordView)
    cxlblF_COOPERATION_ID1: TFVBFFCLabel;
    cxdblblF_COOPERATION_ID: TFVBFFCDBLabel;
    cxlblF_COOPERATION_NAME1: TFVBFFCLabel;
    cxdblblF_COOPERATION_NAME: TFVBFFCDBLabel;
    cxlblF_COOPERATION_ID2: TFVBFFCLabel;
    cxdbseF_COOPERATION_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Cooperation, Data_EduMainClient;

{ TfrmCooperation_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmCooperation_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmCooperation_Record.GetDetailName: String;
begin
  Result := cxdblblF_COOPERATION_NAME.Caption;
end;

end.