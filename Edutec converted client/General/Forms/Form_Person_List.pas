{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <T_PE_PERSON> records.


  @Name       Form_Person_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/04/2008   ivdbossche           Added acJoinPersons. (Mantis 2073).
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Person_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxMaskEdit, cxDBEdit, cxTextEdit, cxContainer, cxLabel, Menus,
  cxDropDownEdit, cxImageComboBox, cxGridDBDataDefinitions, Unit_PPWFrameWorkInterfaces, ActnList,
  cxLookAndFeels, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter,
  System.Actions;

type
  TfrmPerson_List = class(TEduListView)
    cxlblF_LASTNAME2: TFVBFFCLabel;
    cxdbteF_LASTNAME1: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME2: TFVBFFCLabel;
    cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit;
    cxlblPostalCode: TFVBFFCLabel;
    cxdbtePostalCode: TFVBFFCDBTextEdit;
    cxlblCity: TFVBFFCLabel;
    cxdbteCity: TFVBFFCDBTextEdit;
    cxlblSocSecNr: TFVBFFCLabel;
    SocSecNr: TFVBFFCDBMaskEdit;
    cxgrdtblvListF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE: TcxGridDBColumn;
    cxgrdtblvListF_CITY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_COUNTRY_ID: TcxGridDBColumn;
    cxgrdtblvListF_COUNTRY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn;
    cxgrdtblvListF_LANGUAGE_NAME: TcxGridDBColumn;
    cxgrdtblvListF_MEDIUM_ID: TcxGridDBColumn;
    cxgrdtblvListF_MEDIUM_NAME: TcxGridDBColumn;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
    cxgrdtblvListF_MANUALLY_ADDED: TcxGridDBColumn;
    cxgrdtblvListF_CONSTRUCT_ID: TcxGridDBColumn;
    cxgrdtblvListF_ROLE: TcxGridDBColumn;
    alPersonList: TActionList;
    acJoinPersons: TAction;
    dxbbJoinPersons: TdxBarButton;
    cxgrdtblvListF_SYNERGY_ID: TcxGridDBColumn;
    procedure dxbpmnGridPopup(Sender: TObject);
    procedure acJoinPersonsExecute(Sender: TObject);
  private
    { Private declarations }
    FPersonIdFilter: String;
    procedure BuildPersonIdFilter;
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  DBClient, Data_Person, Data_EduMainClient, Form_FVBFFCBaseListView, unit_EdutecInterfaces;

{ TfrmPerson_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmPerson_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPerson_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the condition for the FirstName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_FIRSTNAME', FSearchCriteriaDataSet.FieldByName( 'F_FIRSTNAME' ).AsString );
  end;

  { Add the condition for the LastName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_LASTNAME', FSearchCriteriaDataSet.FieldByName( 'F_LASTNAME' ).AsString );
  end;

  { Add the Condition for the SocSECNr }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SOCSEC_NR' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_SOCSEC_NR', FSearchCriteriaDataSet.FieldByName( 'F_SOCSEC_NR' ).AsString );
  end;

  { Add the Condition for the City }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_CITY_NAME', FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).AsString );
  end;

  { Add the condition for the PostalCode }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_POSTALCODE', FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).AsString );
  end;

  { Add the condition for the Active }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddblnEqualCondition ( aFilterString, 'F_ACTIVE', FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;

  Result := aFilterString;
end;

procedure TfrmPerson_List.dxbpmnGridPopup(Sender: TObject);
begin
  inherited;

        // Only enable following action when user has selected 1 record.
        acJoinPersons.Enabled := ( srcMain.DataSet.Active ) and
                                  ( srcMain.DataSet.RecordCount > 0 ) and
                                  ( not AsSelection ) and // Disable action when TfrmPerson_List has been called by another TfrmPerson_List
                                  ( TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount = 1 ); // (Mantis 2073)


end;

{*****************************************************************************
  @Name       TfrmPerson_List.acJoinPersonsExecute
  @author     Ivan Van den Bossche (18/04/2008)
  @param      None
  @return     None
  @Exception  None
  @See        None
  @Descr      Procedure converts different persons to 1 person. (Mantis 2073).
******************************************************************************}
procedure TfrmPerson_List.acJoinPersonsExecute(Sender: TObject);
var
  aViewController : TcxCustomGridTableController;
  aDataController : TcxGridDBDataController;
  aDataModule     : IEDUPersonDataModule;
  aDataSet        : TClientDataSet;
  aSourcePersonIds: TStringList;
begin
  if ( Supports( FrameWorkDataModule, IEDUPersonDataModule, aDataModule ) ) and
     ( ActiveGridView.Controller is TcxCustomGridTableController ) and
     ( ActiveGridView.DataController is TcxGridDBDataController ) then
  begin
    aDataSet := TClientDataSet.Create( Self );

    try
      aViewController := TcxCustomGridTableController( ActiveGridView.Controller );
      aDataController := TcxGridDBDataController( ActiveGridView.DataController );

      if (aViewController.SelectedRecordCount = 1) then
      begin

        FPersonIdFilter := '';

        // Build filter of selected personid's
        LoopThroughSelectedRecords( BuildPersonIdFilter );

        if (FPersonIdFilter <> '') then FPersonIdFilter := Copy (FPersonIdFilter, 1, length (FPersonIdFilter) - 1); // Remove last ,

            srcMain.DataSet.Bookmark := aDataController.GetSelectedBookmark( 0 );

            aSourcePersonIds := TStringList.Create; // PersonId(s) selected by the user which must be transferred.
            try
                aSourcePersonIds.Delimiter := ',';

                aDataModule.RetrieveDoublePersons( StrToInt( FPersonIdFilter ), aSourcePersonIds );

                if aSourcePersonIds.DelimitedText <> '' then
                begin
                    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmPerson', Format('F_PERSON_ID IN ( %s )', [aSourcePersonIds.DelimitedText] )
                      , aDataSet, True) = mrOk) then
                    begin
                        aSourcePersonIds.Clear; // Clear original double personids which were found.

                        aDataSet.First;

                        // Retrieve selected personids which will be converted.
                        while not aDataSet.Eof do
                        begin
                            aSourcePersonIds.Add( aDataSet.FieldByName('F_PERSON_ID').AsString );
                            aDataSet.Next;
                        end;

                        // Convert selected personids to FPersonIdFilter.
                        if MessageDlg( Format(rsPersonJoinedWith, [ aSourcePersonIds.Text, FPersonIdFilter, FPersonIdFilter ]), mtConfirmation, [mbYes,mbNo], 0) = mrYes then
                          aDataModule.JoinPersons( aSourcePersonIds.DelimitedText, StrToInt( FPersonIdFilter ) );

                        RefreshData;
                        cxgrdtblvList.DataController.ClearSelection;
                    end;

                end else begin
                   // No duplicated persons have been found.
                   MessageDlg( Format(rsNoDuplicatesFound, [ srcMain.DataSet.FieldByName('F_FIRSTNAME').AsString, srcMain.DataSet.FieldByName('F_LASTNAME').AsString ]), mtInformation, [mbOk], 0);
                end;

            finally
                aSourcePersonIds.Free;
            end;

      end;
    finally
      FreeAndNil( aDataSet );
    end;
  end;

end;

procedure TfrmPerson_List.BuildPersonIdFilter;
begin
  FPersonIdFilter := FPersonIdFilter + srcMain.DataSet.FieldByName('F_PERSON_ID').AsString + ',';

end;

end.
