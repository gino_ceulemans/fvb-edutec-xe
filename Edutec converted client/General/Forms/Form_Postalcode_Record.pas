{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_GE_POSTALCODE record.

  @Name       Form_Postalcode_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  30/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Postalcode_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxButtonEdit, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer,
  cxEdit, cxLabel, Unit_PPWFrameWorkInterfaces, Menus, cxSplitter;

type
  TfrmPostalcode_Record = class(TEDURecordView)
    cxlblF_POSTALCODE_ID1: TFVBFFCLabel;
    cxdblblF_POSTALCODE_ID: TFVBFFCDBLabel;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxdblblF_POSTALCODE: TFVBFFCDBLabel;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdblblF_CITY_NAME: TFVBFFCDBLabel;
    cxlblF_POSTALCODE_ID2: TFVBFFCLabel;
    cxdbseF_POSTALCODE_ID: TFVBFFCDBSpinEdit;
    cxlblF_CITY_NL1: TFVBFFCLabel;
    cxdbteF_CITY_NL: TFVBFFCDBTextEdit;
    cxlblF_CITY_FR1: TFVBFFCLabel;
    cxdbteF_CITY_FR: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE2: TFVBFFCLabel;
    cxdbteF_POSTALCODE: TFVBFFCDBTextEdit;
    cxlblF_PROVINCE_NAME1: TFVBFFCLabel;
    cxdbbeF_PROVINCE_NAME: TFVBFFCDBButtonEdit;
    procedure cxdbbeF_PROVINCE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmPostalcode_Record: TfrmPostalcode_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Postalcode, Data_EDUMainClient, Unit_FVBFFCInterfaces, Unit_PPWFrameWorkClasses,
  unit_EdutecInterfaces;

{ TfrmPostalcode_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmPostalcode_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPostalcode_Record.GetDetailName: String;
begin
  Result := cxdblblF_CITY_NAME.Caption;
end;

{*****************************************************************************
  This method will be executed when the user clicks one of the Buttons in the
  ButtonEdit.  Depending on the clicked button it will execute the ShowXXX or
  SelectXXX methods.

  @Name       TfrmPostalcode_Record.cxdbbeF_PROVINCE_NAMEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPostalcode_Record.cxdbbeF_PROVINCE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUPostalCodeDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUPostalCodeDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowProvince( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectProvince( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the form gets initialised and will be used
  to Enable / Disable some components if necessary.

  @Name       TfrmPostalcode_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPostalcode_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUProvinceDataModule ) ) then
    begin
      cxdbbeF_PROVINCE_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbteF_CITY_NL );
//      aFrameWorkDataModule.RecordDataset.FieldByName( 'F_PROVINCE_NAME' ).ReadOnly := True;
//      cxdbbeF_PROVINCE_NAME.Properties.ReadOnly := True;
    end;
  end;
end;

end.