{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  T_LANGUAGE records.
  
  @Name       Form_Language_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Language_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, 
  StdCtrls, cxButtons, 
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, Unit_FVBFFCDevExpress, Unit_FVBFFCDBComponents,
  Unit_FVBFFCFoldablePanel, Menus;

type
  TfrmLanguage_List = class(TEduListView)
    cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME_NL: TcxGridDBColumn;
    cxgrdtblvListF_NAME_FR: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLanguage_List: TfrmLanguage_List;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Language;

end.
