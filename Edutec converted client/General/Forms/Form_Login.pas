{*****************************************************************************
  Name           : Form_Login
  Author         : Ren� Clerckx
  Copyright      : (c) 2001 Peopleware
  Description    : Login dialog.
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/07/2005  WL             Modified so it now supports switches -U and -P
  07/07/2005  WL             Copied from Construct client application
 *****************************************************************************}
unit Form_Login;

interface

uses
  Forms, StdCtrls, Controls, Graphics, ExtCtrls, Classes;

const
  LOGON_OK = $00000000;
  LOGON_CONNECTION_FAILED = $00000001;
  LOGON_APPLICATION_ROLE_FAILED = $00000002;
  LOGON_UNKNOWN_USER = $00000003;
  LOGON_USER_LOCKED = $00000004;
  LOGON_INVALID_PASSWORD = $00000005;

type
  TfrmLogin = class(TForm)
    btnOk: TButton;
    btnCancel: TButton;
    bvlLogin: TBevel;
    imgKeys: TImage;
    lblUserId: TLabel;
    lblPassword: TLabel;
    edtUserId: TEdit;
    edtPassword: TEdit;
    bvlLoginMessage: TBevel;
    mmLoginMessage: TMemo;
    procedure btnOkClick(Sender: TObject);
  private
    FTryCount: Integer;
    function GetSwitchParams(var UserName, PassWord: string): boolean;
    function GetSwitchValues(const SwitchName: string): TStringList;
    function GetParamCountForSwitch(const SwitchName: string): Integer;
    function TryToLogin(const UserName, PassWord: string; var aMessage: string): Boolean;
  protected
    procedure Loaded; override;
    procedure DoClose(var Action: TCloseAction); override;
    procedure DoShow; override;
  public
    constructor Create(AOwner: TComponent); override;
    function Login: TModalResult;
  end;

var
  frmLogin: TfrmLogin;

resourcestring
    rs_LOGON_OK = 'Logon succesvol.';
    rs_LOGON_CONNECTION_FAILED = 'Fout bij maken connectie';
    rs_LOGON_APPLICATION_ROLE_FAILED = 'Fout bij zetten ''Application role''';
    rs_LOGON_UNKNOWN_USER = 'Ongekende gebruiker';
    rs_LOGON_USER_LOCKED = 'Gebruiker geblokkeerd';
    rs_LOGON_INVALID_PASSWORD = 'Ongeldig passwoord';
    rs_LOGON_ACCESS_DENIED = 'Toegang geweigerd';
    rs_CONNECTION_FAILED = 'Connectie met de application server faalde: ';
    rs_InvalidSwitchStructure = 'Ongeldige opstartparameters gedefinieerd. Alleen -U <gebruikersnaam> en -P <passwoord> toegestaan.';
    rs_NoLoginWithSwitches = 'Login onmogelijk met gegeven opstartparameters: ';

implementation

uses SysUtils, Unit_PPWFrameworkClasses, Dialogs,
  //EdutecServerD7_TLB,
  Registry, Windows, Data_EduMainClient;

type
  TSwitchData = record
    SwitchName: string;
    ParamCount: Integer;
  end;

  TSwitches = array[0..1] of TSwitchData;

const
  MAX_TRYCOUNTS = 3;
  Switches: TSwitches = (
    (SwitchName: '-P'; ParamCount: 1),
    (SwitchName: '-U'; ParamCount: 1)
  );

{$R *.DFM}

{ TfrmLogin }

{*****************************************************************************
  Name           : TfrmLogin.GetSwitchParams 
  Author         : Wim Lambrechts                                           
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Returns the username and password as found as application
                   switches (-U ... -P) in the var params.
                   Returns TRUE if both are found

  History        :

  Date         By                   Description
  ----         --                   -----------
  11/07/2005   Wim Lambrechts       Initial creation of the Procedure.
 *****************************************************************************}
function TfrmLogin.GetSwitchParams(var UserName, PassWord: String): boolean;
var
  UserNameList: TStringList;
  PassWordList: TStringList;
begin
  Result := False;
  UserNameList := GetSwitchValues('-U');
  PasswordList := nil;
  if (UserNameList <> nil) and (UserNameList.Count = 1) then //valid usernameswitchname found
  begin
    UserName := UserNameList[0];
    PassWordList := GetSwitchValues ('-P');
    if (PassWordList <> nil) and (PassWordList.Count = 1) then //valid pwdswitch found
    begin
      PassWord := PassWordList[0];
      Result := True;
    end;
  end;
  FreeAndNil(UserNameList);
  FreeandNil(PasswordList);
end;

procedure TfrmLogin.btnOkClick(Sender: TObject);
var
  aMessage: String;
  CursorRestorer: IPPWRestorer;
begin
  ModalResult := mrOk;
{
  CursorRestorer := TPPWCursorRestorer.Create(crSQLWait);
  if not TryToLogin(edtUserId.Text, edtPassword.Text, aMessage) then
  begin
    mmLoginMessage.Lines.Text := aMessage;
    Inc(FTryCount);
    if FTryCount > MAX_TRYCOUNTS then
      ModalResult := mrCancel;
  end
  else
    ModalResult := mrOk;
}
end;

constructor TfrmLogin.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Self.Caption := Application.Title + ' Login';
  FTryCount := 0;
end;

function TfrmLogin.TryToLogin(const UserName, PassWord: String; var aMessage: String): Boolean;
var
  RetVal: Integer;
begin
  try
    dtmEDUMainClient.Connect;
  except
    on e:Exception do
    begin
      aMessage := rs_CONNECTION_FAILED + e.Message;
      Result := False;
      Exit;
    end;
  end;

  Result := False;
  aMessage := '';
  RetVal := dtmEDUMainClient.Login(UserName, PassWord);

  case RetVal of
    LOGON_OK:
      Result := True;
    LOGON_CONNECTION_FAILED:
      aMessage := rs_LOGON_CONNECTION_FAILED;
    LOGON_APPLICATION_ROLE_FAILED:
      aMessage := rs_LOGON_APPLICATION_ROLE_FAILED;
    LOGON_UNKNOWN_USER:
      aMessage := rs_LOGON_UNKNOWN_USER;
    LOGON_USER_LOCKED:
      aMessage := rs_LOGON_USER_LOCKED;
    LOGON_INVALID_PASSWORD:
      aMessage := rs_LOGON_INVALID_PASSWORD;
  end;
end;

procedure TfrmLogin.Loaded;
var
  LastUser: TRegistry;
begin
  inherited Loaded;
  LastUser := TRegistry.Create(KEY_READ);
  try
    LastUser.RootKey := HKEY_CURRENT_USER;
    if LastUser.OpenKey('\Software\PeopleWare\Convenants\Login', False) then
    begin
      edtUserId.Text := LastUser.ReadString('LastUser');
    end;
  finally
    LastUser.CloseKey;
    LastUser.Free;
  end;
end;

procedure TfrmLogin.DoClose(var Action: TCloseAction);
var
  LastUser: TRegistry;
begin
  LastUser := TRegistry.Create(KEY_READ);
  try
    LastUser.RootKey := HKEY_CURRENT_USER;
    LastUser.Access := KEY_WRITE;
    if LastUser.OpenKey('\Software\PeopleWare\Convenants\Login', True) then
    begin
      LastUser.WriteString('LastUser', edtUserId.Text);
    end;
  finally
    LastUser.CloseKey;
    LastUser.Free;
  end;
  Action := caFree;
  inherited DoClose(Action);
end;

procedure TfrmLogin.DoShow;
begin
  inherited DoShow;
  if edtUserId.Text = '' then
    edtUserId.SetFocus
  else
  begin
    edtPassword.SetFocus;
    edtPassword.SelectAll;
  end;
end;

function TfrmLogin.Login: TModalResult;
var
  UserName: string;
  PassWord: string;
  aMessage: string;
begin
  if GetSwitchParams(UserName, PassWord) then
    if TryToLogin(UserName, PassWord, aMessage) then //valid username + password
      Result := mrOk
    else //invalid username / password
    begin
      MessageDlg(rs_NoLoginWithSwitches + #13#10 + aMessage, mtError, [mbOk], 0);
      Result := mrCancel;
    end
  else
    Result := ShowModal;
end;

{*****************************************************************************
  Name           : GetSwitchValues
  Author         : Wim Lambrechts                                           
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Searches for the given switchname. If not found returns nil
                   otherwise it will return a stringlist with the values.
                   The stringlist may contain any number of items:
                   0: the switch has no extra paramvalue
                   1: the switch has only 1 paramvalue
                   >1: (would normally no occur it seems)
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/07/2005   Wim Lambrechts       Initial creation of the Procedure.
 *****************************************************************************}
function TfrmLogin.GetSwitchValues(const SwitchName: string): TStringList;
var
  lcv, lcv2: Integer;
  SwitchNameFound: String;
  ParamCountFound: Integer;
begin
  result := nil;
  lcv := 1;
  while (lcv < ParamCount) do
  begin
    SwitchNameFound := ParamStr(lcv);
    ParamCountFound := GetParamCountForSwitch (SwitchNameFound);
    if ParamCountFound < 0 then
      raise Exception.Create (rs_InvalidSwitchStructure);
    if UpperCase(SwitchNameFound) = UpperCase(SwitchName) then
    begin
      result := TStringList.Create;
      for lcv2 := lcv + 1 to lcv + ParamCountFound do
      begin
        result.Add (ParamStr(lcv2));
      end;
      break;
    end;
    lcv := lcv + ParamCountFound + 1;
  end;
end;

//returns the number of params for the given switch, -1 when switch not recognized
function TfrmLogin.GetParamCountForSwitch(const SwitchName: string): Integer;
var
  lcv: Integer;
begin
  Result := -1;
  for lcv := 0 to High(Switches) do
    if UpperCase(Switches[lcv].SwitchName) = UpperCase(SwitchName) then
    begin
      Result := Switches[lcv].ParamCount;
      Break;
    end;
end;

end.
