{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <T_OR_ORGANISATION> records.


  @Name       Form_Organisation_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Organisation_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxMaskEdit, cxButtonEdit, cxDBEdit, cxTextEdit, cxContainer, cxLabel,
  Menus, cxDropDownEdit, cxImageComboBox, ActnList, Unit_FVBFFCComponents;

type
  TfrmOrganisation_List = class(TEduListView)
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_RSZ_NR: TcxGridDBColumn;
    cxgrdtblvListF_VAT_NR: TcxGridDBColumn;
    cxgrdtblvListF_STREET: TcxGridDBColumn;
    cxgrdtblvListF_NUMBER: TcxGridDBColumn;
    cxgrdtblvListF_MAILBOX: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE: TcxGridDBColumn;
    cxgrdtblvListF_CITY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_COUNTRY_ID: TcxGridDBColumn;
    cxgrdtblvListF_COUNTRY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PHONE: TcxGridDBColumn;
    cxgrdtblvListF_FAX: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxgrdtblvListF_ORGTYPE_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORGTYPE_NAME: TcxGridDBColumn;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblVATNr: TFVBFFCLabel;
    cxdbteVATNr: TFVBFFCDBTextEdit;
    cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit;
    cxlblSocSecNr: TFVBFFCLabel;
    cxlblF_ORGTYPE_NAME1: TFVBFFCLabel;
    cxdbbeF_ORGTYPE_NAME: TFVBFFCDBButtonEdit;
    cxlblPostalCode: TFVBFFCLabel;
    cxdbtePostalCode: TFVBFFCDBTextEdit;
    cxlblCity: TFVBFFCLabel;
    cxdbteCity: TFVBFFCDBTextEdit;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
    cxgrdtblvListF_MANUALLY_ADDED: TcxGridDBColumn;
    cxgrdtblvListF_CONSTRUCT_ID: TcxGridDBColumn;
    alOrganisatie: TFVBFFCActionList;
    actJoinOrganisations: TAction;
    dxBarButton1: TdxBarButton;
    actJoinPersons: TAction;
    dxBarButton2: TdxBarButton;
    cxgrdtblvListF_SYNERGY_ID: TcxGridDBColumn;
    procedure cxdbbeF_ORGTYPE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure actJoinOrganisationsExecute(Sender: TObject);
    procedure actJoinPersonsExecute(Sender: TObject);
    procedure dxbpmnGridPopup(Sender: TObject);
  private
    FOrgIDFilter: String;
    procedure BuildOrgIDFilter;
    procedure JoinPersonsForCurrentOrganisation;
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Organisation, Unit_FVBFFCInterfaces, Data_EduMainClient, unit_EdutecInterfaces, Unit_PPWFrameWorkClasses;

{ TfrmOrganisation_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmOrganisation_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmOrganisation_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the Condition for the Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_NAME', FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).AsString );
  end;

  { Add the condition for the PostalCode }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_POSTALCODE', FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).AsString );
  end;

  { Add the Condition for the VATNr }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_VAT_NR' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_VAT_NR', FSearchCriteriaDataSet.FieldByName( 'F_VAT_NR' ).AsString );
  end;

  { Add the Condition for the SocSECNr }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_RSZ_NR' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_RSZ_NR', FSearchCriteriaDataSet.FieldByName( 'F_RSZ_NR' ).AsString );
  end;

  { Add the Condition for the City }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_CITY_NAME', FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).AsString );
  end;

  { Add the condition for the OrganisationType }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_ORGTYPE_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_ORGTYPE_ID', srcSearchCriteria.DataSet.FieldByName( 'F_ORGTYPE_ID' ).AsInteger );
  end;

  { Add the condition for the Active }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddBlnEqualCondition ( aFilterString, 'F_ACTIVE', FSearchCriteriaDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;
  
  Result := aFilterString;
end;

procedure TfrmOrganisation_List.cxdbbeF_ORGTYPE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectOrgType( FSearchCriteriaDataSet );
  end;


end;

//Voeg de organisaties dewelke geselecteerd zijn, samen
procedure TfrmOrganisation_List.actJoinOrganisationsExecute(
  Sender: TObject);
var
  aDataModule : IEDUOrganisationDataModule;
begin
  inherited;
  FOrgIDFilter := '';
  LoopThroughSelectedRecords (BuildOrgIDFilter);
  if (FOrgIDFilter <> '') then FOrgIDFilter := Copy (FOrgIDFilter, 1, length (FOrgIDFilter) - 1); //laatste , weghalen

  if (FOrgIDFilter <> '') and (Pos (',', FOrgIDFilter) > 0) then //ten minste 2 organisaties gekozen
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
    begin
      aDataModule.JoinOrganisations(FOrgIDFilter);
      cxgrdtblvList.DataController.ClearSelection;
    end
    else assert (false, 'internal error: FrameworkDatamodule does not support IEDUOrganisationDataModule');
  end
end;

procedure TfrmOrganisation_List.BuildOrgIDFilter;
begin
  FOrgIDFilter := FOrgIDFilter + srcMain.DataSet.FieldByName('F_ORGANISATION_ID').AsString + ',';
end;

procedure TfrmOrganisation_List.actJoinPersonsExecute(Sender: TObject);
var
  aCursor : IPPWRestorer;
begin
  inherited;
  aCursor := TPPWCursorRestorer.Create( crSQLWait );
  LoopThroughSelectedRecords (JoinPersonsForCurrentOrganisation);
  cxgrdtblvList.DataController.ClearSelection;
end;

procedure TfrmOrganisation_List.JoinPersonsForCurrentOrganisation;
var
  aDataModule : IEDUOrganisationDataModule;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUOrganisationDataModule, aDataModule ) ) then
  begin
    aDataModule.JoinPersons (srcMain.Dataset.FieldByName('F_ORGANISATION_ID').AsInteger);
  end
  else assert (false, 'internal error 2: FrameworkDatamodule does not support IEDUOrganisationDataModule');
end;

procedure TfrmOrganisation_List.dxbpmnGridPopup(Sender: TObject);
begin
  inherited;
    // Only enable following actions when grid contains sessions
    actJoinOrganisations.Enabled := ( srcMain.DataSet.Active ) and ( srcMain.DataSet.RecordCount > 0 );
    actJoinPersons.Enabled := ( srcMain.DataSet.Active ) and ( srcMain.DataSet.RecordCount > 0 );
    
end;

end.
