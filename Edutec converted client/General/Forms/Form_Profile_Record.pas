{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SYS_PROFILE record.
  
  @Name       Form_Profile_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  05/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Profile_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmProfile_Record = class(TEDURecordView)
    cxlblF_PROFILE_ID1: TFVBFFCLabel;
    cxdblblF_PROFILE_ID: TFVBFFCDBLabel;
    cxlblF_PROFILE_NAME1: TFVBFFCLabel;
    cxdblblF_PROFILE_NAME: TFVBFFCDBLabel;
    cxlblF_PROFILE_ID2: TFVBFFCLabel;
    cxdbseF_PROFILE_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    acShowProfileUsers: TAction;
    dxnbiUsers: TdxNavBarItem;
    procedure acShowProfileUsersExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmProfile_Record: TfrmProfile_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Profile, Data_EDUMainClient;

{ TfrmProfile_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmProfile_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmProfile_Record.GetDetailName: String;
begin
  Result := cxdblblF_PROFILE_NAME.Caption;
end;

procedure TfrmProfile_Record.acShowProfileUsersExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmUserProfile', pnlRecordDetail );
end;

end.