inherited frmLanguage_List: TfrmLanguage_List
  Left = 259
  Top = 250
  Width = 624
  Caption = 'Talen'
  Constraints.MinWidth = 624
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 616
  end
  inherited pnlList: TFVBFFCPanel
    Width = 608
    inherited cxgrdList: TcxGrid
      Width = 608
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_LANGUAGE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_LANGUAGE_ID'
          Width = 108
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Width = 240
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Width = 240
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Width = 608
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Width = 608
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 608
      Visible = False
      inherited pnlSearchCriteriaButtons: TPanel
        Width = 606
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 438
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 526
        end
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 616
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 616
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 612
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmLanguage.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmLanguage.cdsSearchCriteria
  end
end
