inherited frmProgramType_List: TfrmProgramType_List
  Left = 30
  Width = 624
  Caption = 'Talen'
  Constraints.MinWidth = 624
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 616
  end
  inherited pnlList: TFVBFFCPanel
    Width = 608
    inherited cxgrdList: TcxGrid
      Width = 608
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_PROGRAMTYPE_ID: TcxGridDBColumn
          Caption = 'Programmatype( ID )'
          DataBinding.FieldName = 'F_PROGRAMTYPE_ID'
          Width = 108
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          Caption = 'Programmatype( NL )'
          DataBinding.FieldName = 'F_NAME_NL'
          Width = 240
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          Caption = 'Programmatype ( FR )'
          DataBinding.FieldName = 'F_NAME_FR'
          Width = 240
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Width = 608
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Width = 608
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 608
      Visible = False
      inherited pnlSearchCriteriaButtons: TPanel
        Width = 606
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 438
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 526
        end
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 616
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 616
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 612
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
