inherited frmProgInstructor_Record: TfrmProgInstructor_Record
  Caption = 'Lesgever'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_PROG_INSTRUCTOR_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgInstructor_Record.F_PROG_INSTRUCTOR_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'ID'
          FocusControl = cxdblblF_PROG_INSTRUCTOR_ID1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROG_INSTRUCTOR_ID1: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgInstructor_Record.F_PROG_INSTRUCTOR_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROG_INSTRUCTOR_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 73
        end
        object cxlblF_LASTNAME1: TFVBFFCLabel
          Left = 88
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgInstructor_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Naam, Voornaam'
          FocusControl = cxdblblF_LASTNAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_LASTNAME: TFVBFFCDBLabel
          Left = 88
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgInstructor_Record.F_LASTNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_FULLNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 321
        end
        object cxlblF_NAME1: TFVBFFCLabel
          Left = 416
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProgInstructor_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Programma'
          FocusControl = cxdblblF_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 416
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProgInstructor_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 200
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_PROG_INSTRUCTOR_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_PROG_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'ID'
            FocusControl = cxdbseF_PROG_INSTRUCTOR_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PROG_INSTRUCTOR_ID: TFVBFFCDBSpinEdit
            Left = 128
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_PROG_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PROG_INSTRUCTOR_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 160
          end
          object cxlblF_INSTRUCTOR_ID1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Lesgever'
            FocusControl = cxdbbeF_FULLNAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_FULLNAME: TFVBFFCDBButtonEdit
            Left = 128
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_INSTRUCTOR_ID'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_FULLNAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_FULLNAMEPropertiesButtonClick
            TabOrder = 3
            Width = 400
          end
          object cxlblF_SOCSEC_NR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_SOCSEC_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Rijksregisternr.'
            FocusControl = cxdbteF_SOCSEC_NR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_SOCSEC_NR: TFVBFFCDBTextEdit
            Left = 128
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_SOCSEC_NR'
            DataBinding.DataField = 'F_SOCSEC_NR'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 5
            Width = 160
          end
          object cxlblF_PHONE1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_PHONE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Telefoon'
            FocusControl = cxdbteF_PHONE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_PHONE: TFVBFFCDBTextEdit
            Left = 128
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_PHONE'
            DataBinding.DataField = 'F_PHONE'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 7
            Width = 160
          end
          object cxlblF_FAX1: TFVBFFCLabel
            Left = 304
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_FAX'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Fax'
            FocusControl = cxdbteF_FAX
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_FAX: TFVBFFCDBTextEdit
            Left = 368
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_FAX'
            DataBinding.DataField = 'F_FAX'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 9
            Width = 160
          end
          object cxlblF_GSM1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_GSM'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'GSM'
            FocusControl = cxdbteF_GSM
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_GSM: TFVBFFCDBTextEdit
            Left = 128
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_GSM'
            DataBinding.DataField = 'F_GSM'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 11
            Width = 160
          end
          object cxlblF_EMAIL1: TFVBFFCLabel
            Left = 304
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_EMAIL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Email'
            FocusControl = cxdbteF_EMAIL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_EMAIL: TFVBFFCDBTextEdit
            Left = 368
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_EMAIL'
            DataBinding.DataField = 'F_EMAIL'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 13
            Width = 160
          end
          object cxlblF_NAME2: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programma'
            FocusControl = cxdbbeF_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_NAME: TFVBFFCDBButtonEdit
            Left = 128
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_NAMEPropertiesButtonClick
            TabOrder = 15
            Width = 400
          end
          object cxlblF_END_DATE1: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Eind Datum'
            FocusControl = cxdbdeF_END_DATE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_END_DATE: TFVBFFCDBDateEdit
            Left = 128
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmProgInstructor_Record.F_END_DATE'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_END_DATE'
            DataBinding.DataSource = srcMain
            TabOrder = 17
            Width = 160
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmProgInstructor.cdsRecord
  end
end
