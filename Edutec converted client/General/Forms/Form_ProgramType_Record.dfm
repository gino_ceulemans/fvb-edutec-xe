inherited frmProgramType_Record: TfrmProgramType_Record
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Taal'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_PROGRAMTYPE_ID2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmLanguage_Record.F_PROGRAMTYPE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Programmatype ( ID )'
          FocusControl = F_PROGRAMTYPE_ID2
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object F_PROGRAMTYPE_ID2: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmLanguage_Record.F_PROGRAMTYPE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_LANGUAGE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Height = 21
          Width = 121
        end
        object cxlblF_NAME_NL2: TFVBFFCLabel
          Left = 152
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmLanguage_Record.F_NAME_NL'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Programmatype ( NL )'
          FocusControl = F_NAME_NL2
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object F_NAME_NL2: TFVBFFCDBLabel
          Left = 152
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmLanguage_Record.F_NAME_NL'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME_NL'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Height = 21
          Width = 121
        end
        object cxlblF_NAME_FR2: TFVBFFCLabel
          Left = 344
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmLanguage_Record.F_NAME_FR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Programmatype ( FR )'
          FocusControl = F_NAME_FR2
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object F_NAME_FR2: TFVBFFCDBLabel
          Left = 344
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmLanguage_Record.F_NAME_FR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME_FR'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Height = 21
          Width = 121
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_PROGRAMTYPE_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmLanguage_Record.F_PROGRAMTYPE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programmatype ( ID )'
            FocusControl = cxdbseF_PROGRAMTYPE_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PROGRAMTYPE_ID: TFVBFFCDBSpinEdit
            Left = 144
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmLanguage_Record.F_PROGRAMTYPE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PROGRAMTYPE_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 120
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmLanguage_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programmatype ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 144
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmLanguage_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 160
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmLanguage_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Programmatype ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 144
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmLanguage_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 160
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Visible = False
        Links = <>
      end
    end
  end
end
