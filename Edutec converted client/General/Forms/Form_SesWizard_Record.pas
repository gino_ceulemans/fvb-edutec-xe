{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_SesWizard_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/08/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_SesWizard_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxPC, cxControls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxMemo, cxDBEdit,
  cxSpinEdit, cxMaskEdit, cxButtonEdit, cxTextEdit, cxContainer, cxLabel,
  cxCurrencyEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, cxCalendar,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Menus, cxSplitter, cxGroupBox;

type
  TfrmSesWizard_Record = class(TEDURecordView)
    cxpgcResult: TFVBFFCPageControl;
    cxtbsPersonen: TcxTabSheet;
    pnlFVBFFCPanel1: TFVBFFCPanel;
    cxtbsProgramma: TcxTabSheet;
    cxtbsSessie: TcxTabSheet;
    cxtbsForeCast: TcxTabSheet;
    cxtbsResult: TcxTabSheet;
    srcSelectedRoles: TFVBFFCDataSource;
    cxgrdList: TcxGrid;
    cxgrdtblvList: TcxGridDBTableView;
    cxgrdlvlList: TcxGridLevel;
    cxgrdtblvListF_ROLE_ID: TcxGridDBColumn;
    cxgrdtblvListF_PERSON_ID: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_RSZ_NR: TcxGridDBColumn;
    cxgrdtblvListF_SOCSEC_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ROLEGROUP_ID: TcxGridDBColumn;
    cxgrdtblvListF_ROLEGROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListF_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    acSelectPerson: TAction;
    cxbtnFVBFFCButton1: TFVBFFCButton;
    cxbtnFVBFFCButton2: TFVBFFCButton;
    cxbtnFVBFFCButton3: TFVBFFCButton;
    cxbtnFVBFFCButton4: TFVBFFCButton;
    cxbtnFVBFFCButton5: TFVBFFCButton;
    cxbtnFVBFFCButton6: TFVBFFCButton;
    acEditPerson: TAction;
    acEditRole: TAction;
    acRemoveSelected: TAction;
    acRemoveAll: TAction;
    acCreatePersonAndRole: TAction;
    srcSession: TFVBFFCDataSource;
    srcProgram: TFVBFFCDataSource;
    cxlblF_NAME2: TFVBFFCLabel;
    cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel;
    cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit;
    cxlblF_PROFESSION_NAME1: TFVBFFCLabel;
    cxdbbeF_PROFESSION_NAME: TFVBFFCDBButtonEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit;
    cxlblF_DURATION1: TFVBFFCLabel;
    cxdbseF_DURATION: TFVBFFCDBSpinEdit;
    cxlblF_DURATION_DAYS1: TFVBFFCLabel;
    cxdbseF_DURATION_DAYS: TFVBFFCDBSpinEdit;
    cxlblF_ENROLMENT_DATE1: TFVBFFCLabel;
    cxdbseF_ENROLMENT_DATE: TFVBFFCDBSpinEdit;
    cxlblF_CONTROL_DATE1: TFVBFFCLabel;
    cxdbseF_CONTROL_DATE: TFVBFFCDBSpinEdit;
    cxlblF_SHORT_DESC1: TFVBFFCLabel;
    cxdbteF_SHORT_DESC: TFVBFFCDBTextEdit;
    cxlblF_LONG_DESC1: TFVBFFCLabel;
    cxdbmmoF_LONG_DESC: TFVBFFCDBMemo;
    cxlblF_COMMENT1: TFVBFFCLabel;
    cxdbmmoF_COMMENT: TFVBFFCDBMemo;
    cxlblVoorkennis: TFVBFFCLabel;
    cxdbmmoVoorkennis: TFVBFFCDBMemo;
    cxlblVereisteInfrastructuur: TFVBFFCLabel;
    cxdbmmoVereisteInfrastructuur: TFVBFFCDBMemo;
    cxdbmmoVeiligheidsuitrusting: TFVBFFCDBMemo;
    cxlblVeilidgheidsuitrusting: TFVBFFCLabel;
    cxdbmmoGrondstoffen: TFVBFFCDBMemo;
    cxlblGrondstoffen: TFVBFFCLabel;
    cxlblGereedschap: TFVBFFCLabel;
    cxdbmmoGereedschap: TFVBFFCDBMemo;
    cxlblDidactischeHulpmiddelen: TFVBFFCLabel;
    cxdbmmoDidactischeHulpmiddelen: TFVBFFCDBMemo;
    cxdbbeF_PROGRAM_NAME: TFVBFFCDBButtonEdit;
    cxlblF_INFRASTRUCTURE_NAME1: TFVBFFCLabel;
    cxdbbeF_INFRASTRUCTURE_NAME: TFVBFFCDBButtonEdit;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_CODE1: TFVBFFCLabel;
    cxdbteF_CODE: TFVBFFCDBTextEdit;
    cxlblF_START_DATE1: TFVBFFCLabel;
    cxdbdeF_START_DATE: TFVBFFCDBDateEdit;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE: TFVBFFCDBDateEdit;
    cxlblF_START_HOUR1: TFVBFFCLabel;
    cxdbteF_START_HOUR: TFVBFFCDBTextEdit;
    cxlblF_TRANSPORT_INS1: TFVBFFCLabel;
    cxdbteF_TRANSPORT_INS: TFVBFFCDBTextEdit;
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    cxdbmmoFVBFFCDBMemo1: TFVBFFCDBMemo;
    pnlSessionHeader: TFVBFFCPanel;
    bvlBevel1: TBevel;
    pnlFVBFFCPanel2: TFVBFFCPanel;
    cxlblF_CERTIFICATE: TFVBFFCLabel;
    cxlblF_EVALUATION: TFVBFFCLabel;
    cxdbcbF_EVALUATION: TFVBFFCDBCheckBox;
    cxdbcbF_CERTIFICATE: TFVBFFCDBCheckBox;
    cxlblF_INVOICED: TFVBFFCLabel;
    cxlblF_LAST_MINUTE: TFVBFFCLabel;
    cxdbcbF_INVOICED: TFVBFFCDBCheckBox;
    cxdbcbF_LAST_MINUTE: TFVBFFCDBCheckBox;
    cxdbmmoFVBFFCDBMemo2: TFVBFFCDBMemo;
    cxlblF_COMMENT: TFVBFFCLabel;
    srcForeCast: TFVBFFCDataSource;
    srcResult: TFVBFFCDataSource;
    cxgrdResult: TcxGrid;
    cxgrdtblvCxGridDBTableView2: TcxGridDBTableView;
    cxgrdlvlCxGridLevel2: TcxGridLevel;
    cxgrdtblvCxGridDBTableView2F_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_LASTNAME: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_SESSION_END_DATE: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_ENROL_ID: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_ENROL_HOURS: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_SES_CREATE: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_SES_UPDATE: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_ENR_CREATE: TcxGridDBColumn;
    cxgrdtblvCxGridDBTableView2F_ENR_UPDATE: TcxGridDBColumn;
    cxgrdForeCast: TcxGrid;
    cxgrdtblvForeCast: TcxGridDBTableView;
    cxgrdtblvForeCastF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvForeCastF_COMPANY_NAME: TcxGridDBColumn;
    cxgrdtblvForeCastF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvForeCastF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvForeCastF_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvForeCastF_SESSION_END_DATE: TcxGridDBColumn;
    cxgrdtblvForeCastF_ENDROL_ID: TcxGridDBColumn;
    cxgrdtblvForeCastF_SES_CREATE: TcxGridDBColumn;
    cxgrdtblvForeCastF_SES_UPDATE: TcxGridDBColumn;
    cxgrdtblvForeCastF_ENR_CREATE: TcxGridDBColumn;
    cxgrdtblvForeCastF_ENR_UPDATE: TcxGridDBColumn;
    cxgrdtblvForeCastF_FORECAST_COMMENT: TcxGridDBColumn;
    cxgrdlvlForeCast: TcxGridLevel;
    acWizardForeCast: TAction;
    acWizardExecute: TAction;
    dxcpGridPrinter: TFVBFFCComponentPrinter;
    dxcpGridPrinterLink1: TdxGridReportLink;
    cxbtnFVBFFCButton7: TFVBFFCButton;
    acWizardSaveData: TAction;
    cxbtnFVBFFCButton8: TFVBFFCButton;
    cxbtnFVBFFCButton9: TFVBFFCButton;
    cxlblF_PRICE_REDUCTION: TFVBFFCLabel;
    cxdbicbF_PRICE_REDUCTION: TFVBFFCDBImageComboBox;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure acSelectPersonExecute(Sender: TObject);
    procedure acEditPersonExecute(Sender: TObject);
    procedure acEditRoleExecute(Sender: TObject);
    procedure acRemoveAllExecute(Sender: TObject);
    procedure acRemoveSelectedExecute(Sender: TObject);
    procedure acCreatePersonAndRoleExecute(Sender: TObject);
    procedure cxdbbeF_PROGRAM_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxbtnOKClick(Sender: TObject);
    procedure acWizardForeCastExecute(Sender: TObject);
    procedure acWizardForeCastUpdate(Sender: TObject);
    procedure acWizardExecuteUpdate(Sender: TObject);
    procedure acWizardSaveDataExecute(Sender: TObject);
    procedure FVBFFCRecordViewCloseQuery(Sender: TObject;
      var CanClose: Boolean);
    procedure cxpgcResultChange(Sender: TObject);
    procedure cxpgcResultPageChanging(Sender: TObject;
      NewPage: TcxTabSheet; var AllowChange: Boolean);
  private
    { Private declarations }
    FSaveAndClose : Boolean;
  protected
    function GetDataEntered: Boolean; virtual;

    property DataEntered : Boolean read GetDataEntered;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SesWizard, Data_EduMainClient, unit_EdutecInterfaces;

{ TfrmSesWizard_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmSesWizard_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmSesWizard_Record.GetDetailName: String;
begin
  Result := 'Sessie Wizard';
end;

{*****************************************************************************
  This method will be executed when the form gets initialised.

  @Name       TfrmSesWizard_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      aFrameWorkDataModule   The DataModule attached to the form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  inherited;

  if ( Supports( aFrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    cxpgcResult.ActivePage   := cxtbsPersonen;
    srcSelectedRoles.DataSet := aDataModule.SelectedRolesDataSet;
    srcForeCast.DataSet      := aDataModule.WizardDataSet;
    srcResult.DataSet        := aDataModule.WizardResultDataSet;
    srcProgram.DataSet       := aDataModule.ProgramDataSet;
    srcSession.DataSet       := aDataModule.SessionDataSet;
  end;

end;

{*****************************************************************************
  This method will be executed when the Select Person action is triggered.

  @Name       TfrmSesWizard_Record.acSelectPersonExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acSelectPersonExecute(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectRole( srcSelectedRoles.DataSet );
  end;
end;

{*****************************************************************************
  This method is executed when the Edit Person Action is Triggered.

  @Name       TfrmSesWizard_Record.acEditPersonExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acEditPersonExecute(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.ShowPerson( srcSelectedRoles.DataSet, rvmEdit );
  end;
end;

{*****************************************************************************
  This method is executed when the Edit role action is triggered.

  @Name       TfrmSesWizard_Record.acEditRoleExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acEditRoleExecute(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.ShowRole( srcSelectedRoles.DataSet, rvmEdit );
  end;
end;

{*****************************************************************************
  This method is executed when the RemoveAll action is executed.

  @Name       TfrmSesWizard_Record.acRemoveAllExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acRemoveAllExecute(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.RemoveAllRoles( srcSelectedRoles.DataSet );
  end;
end;

{*****************************************************************************
  This method is executed when the RemoveSelected action is executed.

  @Name       TfrmSesWizard_Record.acRemoveSelectedExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acRemoveSelectedExecute(Sender: TObject);
begin
  inherited;
  srcSelectedRoles.DataSet.Delete;
end;

{*****************************************************************************
  This method is executed when the CreatePersonAndRole action is executed.

  @Name       TfrmSesWizard_Record.acCreatePersonAndRoleExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acCreatePersonAndRoleExecute(
  Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.CreatePersonAndRole( srcSelectedRoles.DataSet );
  end;
end;

{*****************************************************************************
  This method is executed when the user clicks one of the buttons in the
  Program Name button Edit.

  @Name       TfrmSesWizard_Record.cxdbbeF_PROGRAM_NAMEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.cxdbbeF_PROGRAM_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowProgram( srcProgram.DataSet, rvmEdit );
      end;
      else aDataModule.SelectProgram( srcProgram.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method is executed when the user clicks one of the buttons in the
  Infrastructure Name button Edit.

  @Name       TfrmSesWizard_Record.cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.cxdbbeF_INFRASTRUCTURE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowInfrastructure( srcSession.DataSet, rvmEdit );
      end;
      else aDataModule.SelectInfrastructure( srcSession.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This method is executed when the user clicks the OK butotn on the Wizard.

  @Name       TfrmSesWizard_Record.cxbtnOKClick
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.cxbtnOKClick(Sender: TObject);
resourcestring
  rsPrintResults = 'Print Wizard Results ?';
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin


    aDataModule.Execute;
    cxpgcResult.ActivePage := cxtbsResult;

    if ( MessageDlg( rsPrintResults, mtConfirmation, [ mbYes, mbNo ], 0 ) = mrYes ) then
    begin
      dxcpGridPrinter.Print( True, Nil );
    end;
    ModalResult := mrOk;
  end;
end;

{*****************************************************************************
  This method is executed when the user clicks the ForeCast button in the Wizard.

  @Name       TfrmSesWizard_Record.acWizardForeCastExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acWizardForeCastExecute(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.ForeCast;
    cxpgcResult.ActivePage := cxtbsForeCast;
  end;
end;

procedure TfrmSesWizard_Record.acWizardForeCastUpdate(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  acWizardForeCast.Enabled := ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) and
     ( aDataModule.CanDoForeCast );
end;

procedure TfrmSesWizard_Record.acWizardExecuteUpdate(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  acWizardExecute.Enabled := ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) and
     ( aDataModule.CanDoExecute );
end;

{*****************************************************************************
  This method is executed when the SaveData Action is triggered.

  @Name       TfrmSesWizard_Record.acWizardSaveDataExecute
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.acWizardSaveDataExecute(Sender: TObject);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) then
  begin
    aDataModule.SaveWizardData;
    close;
  end;
end;

{*****************************************************************************
  This method is executed when the Wizard is closed.

  @Name       TfrmSesWizard_Record.FVBFFCRecordViewCloseQuery
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.FVBFFCRecordViewCloseQuery(Sender: TObject;
  var CanClose: Boolean);
resourcestring
  rsAreYouSure = 'Close the Session Wizard ?';
begin
  if not FSaveAndClose then
  begin
    if DataEntered then
    begin
      CanClose := ( MessageDlg( rsAreYouSure, mtConfirmation, [ mbYes, mbNo ], 0 ) = mrYes );
    end;
  end;
end;

function TfrmSesWizard_Record.GetDataEntered: Boolean;
begin
  Result := not ( ( srcSelectedRoles.DataSet.IsEmpty ) and
                  ( srcProgram.DataSet.IsEmpty ) and
                  ( srcSession.State in [ dsBrowse ] ) );
end;

{*****************************************************************************
  This method is executed when the user switches the page in the PageControl.
  It will be used to Activate some control based on the ActivePage.

  @Name       TfrmSesWizard_Record.cxpgcResultChange
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmSesWizard_Record.cxpgcResultChange(Sender: TObject);
begin
  inherited;
  if ( cxpgcResult.ActivePage = cxtbsProgramma ) then
  begin
    ActivateControl ( cxdbbeF_PROGRAM_NAME );
  end
  else if ( cxpgcResult.ActivePage = cxtbsSessie ) then
  begin
    ActivateControl ( cxdbbeF_INFRASTRUCTURE_NAME );
  end;
end;

procedure TfrmSesWizard_Record.cxpgcResultPageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
var
  aDataModule : IEDUSesWizardDataModule;
begin
  inherited;

  if ( NewPage = cxtbsResult ) then
  begin
    AllowChange := ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) and
     ( aDataModule.ExecuteDone );
  end
  else if ( NewPage = cxtbsForeCast ) then
  begin
    AllowChange := ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUSesWizardDataModule, aDataModule ) ) and
     ( aDataModule.ForeCastDone );
  end;
end;

end.
