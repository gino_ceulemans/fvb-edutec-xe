{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  T_CEN_INFRASTRUCTURE records.


  @Name       Form_CenInfrastructure_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/07/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_CenInfrastructure_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxMaskEdit, cxButtonEdit, cxDBEdit, cxTextEdit, cxContainer, cxLabel,
  Unit_FVBFFCInterfaces, Menus, cxDropDownEdit, cxImageComboBox;

type
  TfrmCenInfrastructure_List = class(TEduListView)
    cxgrdtblvListF_INFRASTRUCTURE_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_FVB_CENTER_ID: TcxGridDBColumn;
    cxgrdtblvListF_VAT_NR: TcxGridDBColumn;
    cxgrdtblvListF_STREET: TcxGridDBColumn;
    cxgrdtblvListF_NUMBER: TcxGridDBColumn;
    cxgrdtblvListF_MAILBOX: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE_ID: TcxGridDBColumn;
    cxgrdtblvListF_POSTALCODE: TcxGridDBColumn;
    cxgrdtblvListF_CITY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PHONE: TcxGridDBColumn;
    cxgrdtblvListF_FAX: TcxGridDBColumn;
    cxgrdtblvListF_WEBSITE: TcxGridDBColumn;
    cxgrdtblvListF_ACC_NR: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_COMMENT: TcxGridDBColumn;
    cxgrdtblvListF_LOCATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_LOCATION: TcxGridDBColumn;
    cxgrdtblvListF_PRECONDITION_ID: TcxGridDBColumn;
    cxgrdtblvListF_PRECONDITION: TcxGridDBColumn;
    cxdbtePostalCode: TFVBFFCDBTextEdit;
    cxlblF_NAME2: TFVBFFCLabel;
    cxdbteF_NAME: TFVBFFCDBTextEdit;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdbteF_CITY_NAME: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxlblF_ACTIVE: TFVBFFCLabel;
    cxdbicbF_ACTIVE: TFVBFFCDBImageComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_CenInfrastructure, Data_EduMainClient, unit_EdutecInterfaces;

{ TfrmCenInfrastructure_List }

{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmCenInfrastructure_List.GetFilterString
  @author     slesage
  @param      None
  @return     Returns a Where clause based on the Search Criteria entered by
  @Exception  None
  @See        None
******************************************************************************}

function TfrmCenInfrastructure_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the Condition for the Name }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_NAME', FSearchCriteriaDataSet.FieldByName( 'F_NAME' ).AsString );
  end;

  { Add the condition for the PostalCode }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_POSTALCODE', FSearchCriteriaDataSet.FieldByName( 'F_POSTALCODE' ).AsString );
  end;

  { Add the Condition for the City }
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition( aFilterString, 'F_CITY_NAME', FSearchCriteriaDataSet.FieldByName( 'F_CITY_NAME' ).AsString );
  end;

  { Add the condition for the Active }
  if not ( srcSearchCriteria.DataSet.FieldByName( 'F_ACTIVE' ).IsNull ) then
  begin
    AddBlnEqualCondition( aFilterString, 'F_ACTIVE', srcSearchCriteria.DataSet.FieldByName( 'F_ACTIVE' ).AsBoolean );
  end;


  Result := aFilterString;
end;

end.
