{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_DOC_INSTRUCTOR record.

  @Name       Form_Instructor_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  04/07/2008   ivdbossche           Enabled Active field (Mantis 3138).
  21/10/2005   sLesage              Fixed Typos and Form Layout.
  26/07/2005   sLesage              Added functionality to show all programs
                                    to which the Instructor is Linked.
  20/07/2005   slesage              Added the funcitonality for the
                                    SelectXXX and ShowXXX methods.
  19/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Instructor_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons, cxDBLabel, cxControls, cxContainer, cxEdit, cxLabel,
  cxMemo, cxDBEdit, cxGroupBox, cxCheckBox, cxDropDownEdit, cxCalendar,
  cxButtonEdit, cxTextEdit, cxMaskEdit, cxSpinEdit, Menus, cxSplitter,
  cxHyperLinkEdit;

type
  TfrmInstructor_Record = class(TEDURecordView)
    cxlblF_INSTRUCTOR_ID1: TFVBFFCLabel;
    cxdblblF_INSTRUCTOR_ID: TFVBFFCDBLabel;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxdblblF_LASTNAME: TFVBFFCDBLabel;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdblblF_FIRSTNAME: TFVBFFCDBLabel;
    cxlblF_INSTRUCTOR_ID2: TFVBFFCLabel;
    cxdbseF_INSTRUCTOR_ID1: TFVBFFCDBSpinEdit;
    cxlblF_TITLE_NAME1: TFVBFFCLabel;
    cxdbbeF_TITLE_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_LASTNAME2: TFVBFFCLabel;
    cxdbteF_LASTNAME1: TFVBFFCDBTextEdit;
    cxlblF_FIRSTNAME2: TFVBFFCLabel;
    cxdbteF_FIRSTNAME1: TFVBFFCDBTextEdit;
    cxlblF_STREET1: TFVBFFCLabel;
    cxdbteF_STREET1: TFVBFFCDBTextEdit;
    cxlblF_NUMBER1: TFVBFFCLabel;
    cxdbteF_NUMBER1: TFVBFFCDBTextEdit;
    cxlblF_MAILBOX1: TFVBFFCLabel;
    cxdbteF_MAILBOX1: TFVBFFCDBTextEdit;
    cxlblF_POSTALCODE1: TFVBFFCLabel;
    cxdbbeF_POSTALCODE1: TFVBFFCDBButtonEdit;
    cxlblF_CITY_NAME1: TFVBFFCLabel;
    cxdbteF_CITY_NAME1: TFVBFFCDBTextEdit;
    cxlblF_COUNTRY_NAME1: TFVBFFCLabel;
    cxdbteF_COUNTRY_NAME1: TFVBFFCDBTextEdit;
    cxlblF_PHONE1: TFVBFFCLabel;
    cxdbteF_PHONE1: TFVBFFCDBTextEdit;
    cxlblF_FAX1: TFVBFFCLabel;
    cxdbteF_FAX1: TFVBFFCDBTextEdit;
    cxlblF_GSM1: TFVBFFCLabel;
    cxdbteF_GSM1: TFVBFFCDBTextEdit;
    cxlblF_EMAIL1: TFVBFFCLabel;
    cxdbteF_EMAIL1: TFVBFFCDBHyperLinkEdit;
    cxlblF_LANGUAGE_NAME1: TFVBFFCLabel;
    cxdbbeF_LANGUAGE_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_GENDER_NAME1: TFVBFFCLabel;
    cxdbbeF_GENDER_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_MEDIUM_NAME1: TFVBFFCLabel;
    cxdbbeF_MEDIUM_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_ACC_NR1: TFVBFFCLabel;
    F_ACC_NR1: TFVBFFCDBMaskEdit;
    cxlblF_FOREIGN_ACC_NR1: TFVBFFCLabel;
    cxdbteF_FOREIGN_ACC_NR1: TFVBFFCDBTextEdit;
    cxlblF_SOCSEC_NR1: TFVBFFCLabel;
    F_SOCSEC_NR1: TFVBFFCDBMaskEdit;
    cxlblF_DATEBIRTH1: TFVBFFCLabel;
    cxdbdeF_DATEBIRTH1: TFVBFFCDBDateEdit;
    cxlblF_ACTIVE1: TFVBFFCLabel;
    csdbcbF_ACTIVE1: TFVBFFCDBCheckBox;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE1: TFVBFFCDBDateEdit;
    cxlblF_DIPLOMA_NAME1: TFVBFFCLabel;
    cxdbbeF_DIPLOMA_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_CODE1: TFVBFFCLabel;
    cxdbteF_CODE1: TFVBFFCDBTextEdit;
    cxgbFVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlblF_DUTCH1: TFVBFFCLabel;
    csdbcbF_DUTCH1: TFVBFFCDBCheckBox;
    cxlblF_FRENCH1: TFVBFFCLabel;
    csdbcbF_FRENCH1: TFVBFFCDBCheckBox;
    cxlblF_ENGLISH1: TFVBFFCLabel;
    csdbcbF_ENGLISH1: TFVBFFCDBCheckBox;
    cxlblF_GERMAN1: TFVBFFCLabel;
    csdbcbF_GERMAN1: TFVBFFCDBCheckBox;
    cxlblF_OTHER_LANGUAGE1: TFVBFFCLabel;
    cxdbteF_OTHER_LANGUAGE1: TFVBFFCDBTextEdit;
    cxlblF_COMMENT1: TFVBFFCLabel;
    acShowPrograms: TAction;
    dxnbiPrograms: TdxNavBarItem;
    cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit;
    cxlblF_BTW: TFVBFFCLabel;
    cxdbmeF_BTW: TFVBFFCDBMaskEdit;
    dxnbiDocCenter: TdxNavBarItem;
    acShowDocCenter: TAction;
    cxDBmmo_FCOMMENT: TcxDBMemo;
    FVBFFCLabel1: TFVBFFCLabel;
    cxdbteF_ORGANISATION_NAME: TFVBFFCDBButtonEdit;
    FVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlbF_CONFIRMATION_CONTACT1: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT1: TFVBFFCDBTextEdit;
    F_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlbF_CONFIRMATION_CONTACT1_EMAIL: TFVBFFCLabel;
    F_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCDBHyperLinkEdit;
    cxlbF_CONFIRMATION_CONTACT2_EMAIL: TFVBFFCLabel;
    cxdbteF_CONFIRMATION_CONTACT2: TFVBFFCDBTextEdit;
    cxlbF_CONFIRMATION_CONTACT2: TFVBFFCLabel;
    procedure cxdbbeF_TITLE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_POSTALCODE1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_MEDIUM_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_DIPLOMA_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_GENDER_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acShowProgramsExecute(Sender: TObject);
    procedure cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acShowDocCenterExecute(Sender: TObject);
    procedure cxdbteF_ORGANISATION_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbteF_ORGANISATION_NAMEKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_Instructor, Data_EduMainClient, Unit_FVBFFCInterfaces,
  Form_FVBFFCBaseRecordView, unit_EdutecInterfaces;

{ TfrmInstructor_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmInstructor_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmInstructor_Record.GetDetailName: String;
begin
  Result := cxdblblF_LASTNAME.Caption + ', ' +
            cxdblblF_FIRSTNAME.Caption;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Title ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_TITLE_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_TITLE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowTitle( srcMain.DataSet, rvmEdit );
        end;
        else aInstructorDataModule.SelectTitle( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the PostalCode ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_POSTALCODE1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_POSTALCODE1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowPostalCode( srcMain.DataSet, rvmEdit );
        end;
        else aInstructorDataModule.SelectPostalCode( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Language ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_LANGUAGE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowLanguage( srcMain.DataSet, rvmEdit );
        end;
        else aInstructorDataModule.SelectLanguage( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Medium ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_MEDIUM_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_MEDIUM_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowMedium( srcMain.DataSet, rvmEdit );
        end;
        else aInstructorDataModule.SelectMedium( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Diploma ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_DIPLOMA_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_DIPLOMA_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowDiploma( srcMain.DataSet, rvmEdit );
        end;
        else aInstructorDataModule.SelectDiploma( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Gender ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_GENDER_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_GENDER_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
      { Execute the method in the IEDUDocInstructorDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aInstructorDataModule.ShowGender( srcMain.DataSet, rvmEdit );
        end;
        else aInstructorDataModule.SelectGender( srcMain.DataSet );
      end;
    end;
  end;
end;

procedure TfrmInstructor_Record.acShowProgramsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Sender, 'TdtmProgInstructor', pnlRecordDetail );
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Gender ButtonEdit.  In here we will call the appropriate method on the
  IEDUDocInstructorDataModule interface corresponding to the clicked button.

  @Name       TfrmInstructor_Record.cxdbbeF_GENDER_NAME1PropertiesButtonClick
  @author     cheuten
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmInstructor_Record.cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aDataModule ) ) then
    begin
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowCountry( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aDataModule.SelectCountry( srcMain.DataSet );
        end;
      end;
    end;
  end;
end;

procedure TfrmInstructor_Record.acShowDocCenterExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Sender, 'TdtmDocCenter', pnlRecordDetail );
end;

procedure TfrmInstructor_Record.cxdbteF_ORGANISATION_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aInstructorDataModule : IEDUDocInstructorDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUDocInstructorDataModule, aInstructorDataModule ) ) then
    begin
        aInstructorDataModule.SelectOrganisation( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmInstructor_Record.cxdbteF_ORGANISATION_NAMEKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) and (Mode in [ rvmEdit, rvmAdd ]) then
  begin
    if (srcMain.DataSet.State = dsBrowse) then srcMain.Dataset.Edit;
    srcMain.DataSet.FieldByName('F_ORGANISATION_ID').Clear;
    srcMain.Dataset.FieldByName('F_ORGANISATION_NAME').Clear;
  end;
end;

end.