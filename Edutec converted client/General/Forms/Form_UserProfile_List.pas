{*****************************************************************************
  This unit contains the form that will be used to display a list of
  T_SYS_USER_PROF records.
  
  @Name       Form_UserProfile_List
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  05/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_UserProfile_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, StdCtrls, cxButtons, Unit_FVBFFCFoldablePanel,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, Menus;

type
  TfrmUserProfile_List = class(TEduListView)
    cxgrdtblvListF_USER_ID: TcxGridDBColumn;
    cxgrdtblvListF_LASTNAME: TcxGridDBColumn;
    cxgrdtblvListF_FIRSTNAME: TcxGridDBColumn;
    cxgrdtblvListF_COMPLETE_USERNAME: TcxGridDBColumn;
    cxgrdtblvListF_PROFILE_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROFILE_NAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmUserProfile_List: TfrmUserProfile_List;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_UserProfile;

end.