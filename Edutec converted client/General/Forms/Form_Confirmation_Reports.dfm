inherited frmConfirmationReports: TfrmConfirmationReports
  Left = 30
  Top = 83
  Height = 658
  Anchors = [akLeft, akTop, akBottom]
  BorderStyle = bsSingle
  Caption = ''
  ClientHeight = 629
  ClientWidth = 831
  Position = poDefault
  WindowState = wsMaximized
  OnActivate = FVBFFCRecordViewActivate
  Registered = True
  ExplicitHeight = 656
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 597
    Width = 831
    BorderStyle = bsSingle
    ExplicitTop = 597
    ExplicitWidth = 831
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 268
      Width = 559
      Height = 28
      ExplicitLeft = 268
      ExplicitWidth = 559
      ExplicitHeight = 28
      inherited cxbtnOK: TFVBFFCButton
        Left = 224
        Width = 137
        Caption = 'Export rapporten'
        Default = False
        ExplicitLeft = 224
        ExplicitWidth = 137
      end
      inherited cxbtnCancel: TFVBFFCButton
        Left = 367
        ExplicitLeft = 367
      end
      inherited cxbtnApply: TFVBFFCButton
        Left = 480
        Width = 69
        OnClick = cxbtnApplyClick
        ExplicitLeft = 480
        ExplicitWidth = 69
      end
      object chkMailing: TCheckBox
        Left = 104
        Top = 8
        Width = 97
        Height = 17
        Caption = 'Auto mailing'
        TabOrder = 3
      end
      object btnPreview: TFVBFFCButton
        Left = 18
        Top = 4
        Width = 69
        Height = 25
        Caption = 'Preview'
        OptionsImage.Margin = 10
        OptionsImage.Spacing = -1
        TabOrder = 4
        OnClick = btnPreviewClick
      end
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 831
    Height = 597
    ExplicitWidth = 831
    ExplicitHeight = 597
    inherited pnlRecord: TFVBFFCPanel
      Width = 669
      Height = 597
      ExplicitWidth = 669
      ExplicitHeight = 597
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 669
        ExplicitWidth = 669
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 669
        ExplicitWidth = 669
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 669
        Height = 4
        Visible = False
        ExplicitWidth = 669
        ExplicitHeight = 4
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Top = 33
        Width = 669
        ExplicitTop = 33
        ExplicitWidth = 669
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 593
        Width = 669
        ExplicitTop = 593
        ExplicitWidth = 669
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Top = 37
        Width = 669
        Height = 556
        ExplicitTop = 37
        ExplicitWidth = 669
        ExplicitHeight = 556
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 669
          ExplicitWidth = 669
        end
        inherited sbxMain: TScrollBox
          Width = 669
          Height = 531
          BorderStyle = bsSingle
          ParentBackground = True
          ExplicitWidth = 669
          ExplicitHeight = 531
          object grpCompanies: TGroupBox
            Left = 0
            Top = 8
            Width = 503
            Height = 201
            Align = alCustom
            Anchors = [akLeft, akTop, akRight]
            Caption = 'Bedrijven/scholen'
            TabOrder = 0
            DesignSize = (
              503
              201)
            object gbCompExtra: TGroupBox
              Left = 8
              Top = 128
              Width = 490
              Height = 63
              Align = alCustom
              Anchors = [akLeft, akRight, akBottom]
              Caption = 'Extra'
              TabOrder = 0
              object FVBFFCDBEXTRA_COMPANIES: TFVBFFCDBMemo
                Left = 8
                Top = 16
                Align = alCustom
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataBinding.DataField = 'F_EXTRA_COMPANIES'
                DataBinding.DataSource = srcMain
                ParentFont = False
                Properties.ScrollBars = ssVertical
                TabOrder = 0
                Height = 41
                Width = 475
              end
            end
            object cxGridComp: TcxGrid
              Left = 8
              Top = 34
              Width = 491
              Height = 90
              Align = alCustom
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 1
              LookAndFeel.Kind = lfOffice11
              object cxTVCompaniesAndSchools: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                OnEditing = cxTVCompaniesAndSchoolsEditing
                DataController.DataModeController.GridMode = True
                DataController.DataSource = srcCompanies
                DataController.Filter.AutoDataSetFilter = True
                DataController.KeyFieldNames = 'F_CONF_ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Kind = skCount
                    FieldName = 'Trademark'
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.FocusCellOnTab = True
                OptionsCustomize.ColumnFiltering = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnSorting = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.CellAutoHeight = True
                OptionsView.GroupByBox = False
                OptionsView.Indicator = True
                Preview.MaxLineCount = 1
                Preview.RightIndent = 10
                Styles.Background = dtmEDUMainClient.cxsBackground
                Styles.Content = dtmEDUMainClient.cxsContent
                Styles.ContentEven = dtmEDUMainClient.cxsContentEven
                Styles.ContentOdd = dtmEDUMainClient.cxsContentEven
                Styles.FilterBox = dtmEDUMainClient.cxsFilterBox
                Styles.IncSearch = dtmEDUMainClient.cxsIncSearch
                Styles.Footer = dtmEDUMainClient.cxsFooter
                Styles.Group = dtmEDUMainClient.cxsGroup
                Styles.GroupByBox = dtmEDUMainClient.cxsGroupByBox
                Styles.Header = dtmEDUMainClient.cxsHeader
                Styles.Inactive = dtmEDUMainClient.cxsInactive
                Styles.Indicator = dtmEDUMainClient.cxsIndicator
                Styles.Preview = dtmEDUMainClient.cxsPreview
                Styles.Selection = dtmEDUMainClient.cxsSelection
                Styles.StyleSheet = dtmEDUMainClient.GridTableViewStyleSheetDevExpress
                object cxGridDBF_PRINT_CONFIRMATION: TcxGridDBColumn
                  DataBinding.FieldName = 'F_PRINT_CONFIRMATION'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Width = 20
                  IsCaptionAssigned = True
                end
                object cxTVCompaniesAndSchoolsF_CONF_SUB_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_CONF_SUB_ID'
                  Visible = False
                end
                object cxTVCompaniesAndSchoolsF_CONF_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_CONF_ID'
                  Visible = False
                end
                object cxTVCompaniesAndSchoolsF_ORGANISATION: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  Options.Editing = False
                  Width = 150
                  IsCaptionAssigned = True
                end
                object cxTVCompaniesAndSchoolsF_ORGANISATION_TYPE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION_TYPE'
                  Visible = False
                end
                object cxTVCompaniesAndSchoolsF_NO_PARTICIPANTS: TcxGridDBColumn
                  Caption = '#deelnemers'
                  DataBinding.FieldName = 'F_NO_PARTICIPANTS'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  Options.Editing = False
                  Width = 89
                end
                object cxGridDBTableViewF_ORG_CONTACT1: TcxGridDBColumn
                  Caption = 'Contactpersoon 1'
                  DataBinding.FieldName = 'F_ORG_CONTACT1'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 180
                end
                object cxTVCompaniesAndSchoolsF_ORG_CONTACT1_EMAIL: TcxGridDBColumn
                  Caption = 'EMail'
                  DataBinding.FieldName = 'F_ORG_CONTACT1_EMAIL'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 159
                end
                object cxTVCompaniesAndSchoolsF_ORG_CONTACT2: TcxGridDBColumn
                  Caption = 'Contactpersoon 2'
                  DataBinding.FieldName = 'F_ORG_CONTACT2'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 180
                end
                object cxTVCompaniesAndSchoolsF_ORG_CONTACT2_EMAIL: TcxGridDBColumn
                  Caption = 'EMail'
                  DataBinding.FieldName = 'F_ORG_CONTACT2_EMAIL'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 159
                end
                object cxTVCompaniesAndSchoolsF_CONFIRMATION_DATE: TcxGridDBColumn
                  Caption = 'Datum bevestiging'
                  DataBinding.FieldName = 'F_CONFIRMATION_DATE'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Editing = False
                end
                object cxTVCompaniesAndSchoolsF_PARTNER_WINTER_NAME: TcxGridDBColumn
                  Caption = 'Partner winter'
                  DataBinding.FieldName = 'F_PARTNER_WINTER_NAME'
                  Options.Editing = False
                  Options.Focusing = False
                  Width = 350
                end
                object cxTVCompaniesAndSchoolsF_ISCCENABLED: TcxGridDBColumn
                  Caption = 'CC'
                  DataBinding.FieldName = 'F_ISCCENABLED'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                end
              end
              object cxGridLevel5: TcxGridLevel
                GridView = cxTVCompaniesAndSchools
                MaxDetailHeight = 300
              end
            end
            object chkMailInfraAttachCompanySchool: TCheckBox
              Left = 8
              Top = 16
              Width = 700
              Height = 17
              Caption = 
                'Voeg de e-mail bijlage van de infrastructuur toe aan de mail naa' +
                'r het bedrijf of de school'
              TabOrder = 2
            end
            object cxBtnEnlargeCompanies: TFVBFFCButton
              Left = 469
              Top = 8
              Width = 30
              Height = 25
              Anchors = [akTop, akRight]
              OptionsImage.Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                3303333333333333300033333333333300033333333333300033333333333300
                0333333700073B7033333307777700B33333307F8F8F7033333377F8F9F8F773
                3333078F898F8703333307F99999F7033333078F898F8703333377F8F9F8F773
                3333307F8F8F7033333333077777033333333337000733333333}
              OptionsImage.Margin = 5
              OptionsImage.Spacing = -1
              TabOrder = 3
              TabStop = False
              OnClick = cxBtnEnlargeCompaniesClick
            end
            object cxBtnNormalCompanies: TFVBFFCButton
              Left = 469
              Top = 8
              Width = 30
              Height = 25
              Anchors = [akTop, akRight]
              OptionsImage.Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                3303333333333333300033333333333300033333333333300033333333333300
                0333333700073B7033333307777700B333333078F8F870333333778F8F8F8773
                333307F8F8F8F7033333078999998703333307F8F8F8F7033333778F8F8F8773
                33333078F8F87033333333077777033333333337000733333333}
              OptionsImage.Margin = 5
              OptionsImage.Spacing = -1
              TabOrder = 4
              TabStop = False
              OnClick = cxBtnNormalCompaniesClick
            end
          end
          object grpInfrastructure: TGroupBox
            Left = 0
            Top = 216
            Width = 503
            Height = 161
            Align = alCustom
            Anchors = [akLeft, akTop, akRight]
            Caption = 'Infrastructuur'
            TabOrder = 1
            DesignSize = (
              503
              161)
            object cxGridInfra: TcxGrid
              Left = 8
              Top = 34
              Width = 490
              Height = 55
              Align = alCustom
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 0
              LookAndFeel.NativeStyle = False
              object cxGridDBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataModeController.GridMode = True
                DataController.DataSource = srcInfrastructure
                DataController.Filter.AutoDataSetFilter = True
                DataController.KeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Kind = skCount
                    FieldName = 'Trademark'
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.FocusCellOnTab = True
                OptionsCustomize.ColumnFiltering = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnSorting = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.ScrollBars = ssHorizontal
                OptionsView.CellAutoHeight = True
                OptionsView.GroupByBox = False
                OptionsView.Indicator = True
                Preview.MaxLineCount = 1
                Preview.RightIndent = 10
                Styles.Background = dtmEDUMainClient.cxsBackground
                Styles.Content = dtmEDUMainClient.cxsContent
                Styles.ContentEven = dtmEDUMainClient.cxsContentEven
                Styles.ContentOdd = dtmEDUMainClient.cxsContentOdd
                Styles.FilterBox = dtmEDUMainClient.cxsFilterBox
                Styles.IncSearch = dtmEDUMainClient.cxsIncSearch
                Styles.Footer = dtmEDUMainClient.cxsFooter
                Styles.Group = dtmEDUMainClient.cxsGroup
                Styles.GroupByBox = dtmEDUMainClient.cxsGroupByBox
                Styles.Header = dtmEDUMainClient.cxsHeader
                Styles.Inactive = dtmEDUMainClient.cxsInactive
                Styles.Indicator = dtmEDUMainClient.cxsIndicator
                Styles.Preview = dtmEDUMainClient.cxsPreview
                Styles.Selection = dtmEDUMainClient.cxsSelection
                Styles.StyleSheet = dtmEDUMainClient.GridTableViewStyleSheetDevExpress
                object cxGridDBTableView1F_CONF_SUB_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_CONF_SUB_ID'
                  Visible = False
                end
                object cxGridDBTableView1F_CONF_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_CONF_ID'
                  Visible = False
                end
                object cxGridDBTableView1F_LOCATION: TcxGridDBColumn
                  DataBinding.FieldName = 'F_LOCATION'
                  Width = 150
                  IsCaptionAssigned = True
                end
                object cxGridDBTableView1F_ORGANISATION_TYPE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION_TYPE'
                  Visible = False
                end
                object cxGridDBTableView1F_NO_PARTICIPANTS: TcxGridDBColumn
                  Caption = '#deelnemers'
                  DataBinding.FieldName = 'F_NO_PARTICIPANTS'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  Width = 82
                end
                object cxGridDBTableView1F_ORG_CONTACT1: TcxGridDBColumn
                  Caption = 'Contactpersoon 1'
                  DataBinding.FieldName = 'F_ORG_CONTACT1'
                  Width = 180
                end
                object cxGridDBTableView1F_ORG_CONTACT1_EMAIL: TcxGridDBColumn
                  Caption = 'EMail'
                  DataBinding.FieldName = 'F_ORG_CONTACT1_EMAIL'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 159
                end
                object cxGridDBTableView1F_ORG_CONTACT2: TcxGridDBColumn
                  Caption = 'Contactpersoon 2'
                  DataBinding.FieldName = 'F_ORG_CONTACT2'
                  Width = 180
                end
                object cxGridDBTableView1F_ORG_CONTACT2_EMAIL: TcxGridDBColumn
                  Caption = 'EMail'
                  DataBinding.FieldName = 'F_ORG_CONTACT2_EMAIL'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 159
                end
                object cxGridDBTableView1F_CONFIRMATION_DATE: TcxGridDBColumn
                  Caption = 'Datum bevestiging'
                  DataBinding.FieldName = 'F_CONFIRMATION_DATE'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Options.Editing = False
                  Width = 130
                end
                object cxGridDBTableView1F_SESSION_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_ID'
                  Visible = False
                end
              end
              object cxGridLevel1: TcxGridLevel
                GridView = cxGridDBTableView1
                MaxDetailHeight = 300
              end
            end
            object gbInfraExtra: TGroupBox
              Left = 8
              Top = 88
              Width = 490
              Height = 63
              Align = alCustom
              Anchors = [akLeft, akRight, akBottom]
              Caption = 'Extra'
              TabOrder = 1
              object FVBFFCDBExtra_Infrastructure: TFVBFFCDBMemo
                Left = 8
                Top = 16
                Align = alCustom
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataBinding.DataField = 'F_EXTRA_INFRASTRUCTURE'
                DataBinding.DataSource = srcMain
                ParentFont = False
                Properties.ScrollBars = ssVertical
                TabOrder = 0
                Height = 41
                Width = 474
              end
            end
            object FVBFFCDBCheckBox1: TFVBFFCDBCheckBox
              Left = 0
              Top = 14
              Caption = 'Infrastructuur'
              DataBinding.DataField = 'F_PRINT_CONFIRMATION'
              DataBinding.DataSource = srcInfrastructure
              ParentBackground = False
              ParentColor = False
              ParentFont = False
              Style.Color = 15914442
              TabOrder = 2
            end
            object cxBtnEnlargeInfra: TFVBFFCButton
              Left = 468
              Top = 8
              Width = 30
              Height = 25
              Anchors = [akTop, akRight]
              OptionsImage.Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                3303333333333333300033333333333300033333333333300033333333333300
                0333333700073B7033333307777700B33333307F8F8F7033333377F8F9F8F773
                3333078F898F8703333307F99999F7033333078F898F8703333377F8F9F8F773
                3333307F8F8F7033333333077777033333333337000733333333}
              OptionsImage.Margin = 5
              OptionsImage.Spacing = -1
              TabOrder = 3
              TabStop = False
              OnClick = cxBtnEnlargeInfraClick
            end
            object cxBtnNormalInfra: TFVBFFCButton
              Left = 468
              Top = 8
              Width = 30
              Height = 25
              Anchors = [akTop, akRight]
              OptionsImage.Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                3303333333333333300033333333333300033333333333300033333333333300
                0333333700073B7033333307777700B333333078F8F870333333778F8F8F8773
                333307F8F8F8F7033333078999998703333307F8F8F8F7033333778F8F8F8773
                33333078F8F87033333333077777033333333337000733333333}
              OptionsImage.Margin = 5
              OptionsImage.Spacing = -1
              TabOrder = 4
              TabStop = False
              OnClick = cxBtnNormalInfraClick
            end
          end
          object grpTeacher: TGroupBox
            Left = 0
            Top = 384
            Width = 503
            Height = 193
            Align = alCustom
            Anchors = [akLeft, akTop, akRight]
            Caption = '(organisatie van de) lesgever'
            TabOrder = 2
            DesignSize = (
              503
              193)
            object cxGridTeach: TcxGrid
              Left = 8
              Top = 34
              Width = 490
              Height = 75
              Align = alCustom
              Anchors = [akLeft, akTop, akRight]
              TabOrder = 0
              object cxGridDBTableView2: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataModeController.GridMode = True
                DataController.DataSource = srcOrganisation
                DataController.Filter.AutoDataSetFilter = True
                DataController.KeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Kind = skCount
                    FieldName = 'Trademark'
                  end>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.FocusCellOnTab = True
                OptionsCustomize.ColumnFiltering = False
                OptionsCustomize.ColumnGrouping = False
                OptionsCustomize.ColumnSorting = False
                OptionsData.Deleting = False
                OptionsData.DeletingConfirmation = False
                OptionsData.Inserting = False
                OptionsView.CellAutoHeight = True
                OptionsView.GroupByBox = False
                OptionsView.Indicator = True
                Preview.MaxLineCount = 1
                Preview.RightIndent = 10
                Styles.Background = dtmEDUMainClient.cxsBackground
                Styles.Content = dtmEDUMainClient.cxsContent
                Styles.ContentEven = dtmEDUMainClient.cxsContentEven
                Styles.ContentOdd = dtmEDUMainClient.cxsContentOdd
                Styles.FilterBox = dtmEDUMainClient.cxsFilterBox
                Styles.IncSearch = dtmEDUMainClient.cxsIncSearch
                Styles.Footer = dtmEDUMainClient.cxsFooter
                Styles.Group = dtmEDUMainClient.cxsGroup
                Styles.GroupByBox = dtmEDUMainClient.cxsGroupByBox
                Styles.Header = dtmEDUMainClient.cxsHeader
                Styles.Inactive = dtmEDUMainClient.cxsInactive
                Styles.Indicator = dtmEDUMainClient.cxsIndicator
                Styles.Preview = dtmEDUMainClient.cxsPreview
                Styles.Selection = dtmEDUMainClient.cxsSelection
                object cxGridDBTableView2F_PRINT_CONFIRMATION: TcxGridDBColumn
                  DataBinding.FieldName = 'F_PRINT_CONFIRMATION'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                  Width = 20
                  IsCaptionAssigned = True
                end
                object cxGridDBTableView2F_ORG_INSTRUCTOR_NAME: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORG_INSTRUCTOR_NAME'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 150
                end
                object cxGridDBTableView2F_INSTRUCTOR_NAME: TcxGridDBColumn
                  Caption = 'Naam lesgever'
                  DataBinding.FieldName = 'F_INSTRUCTOR_NAME'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  Width = 150
                end
                object cxGridDBTableView2F_CONF_SUB_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_CONF_SUB_ID'
                  Visible = False
                end
                object cxGridDBTableView2F_CONF_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_CONF_ID'
                  Visible = False
                end
                object cxGridDBTableView2F_ORGANISATION: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Visible = False
                end
                object cxGridDBTableView2F_ORGANISATION_TYPE: TcxGridDBColumn
                  DataBinding.FieldName = 'F_ORGANISATION_TYPE'
                  Visible = False
                end
                object cxGridDBTableView2F_NO_PARTICIPANTS: TcxGridDBColumn
                  Caption = '#deelnemers'
                  DataBinding.FieldName = 'F_NO_PARTICIPANTS'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  Width = 86
                end
                object cxGridDBTableView2F_ORG_CONTACT1: TcxGridDBColumn
                  Caption = 'Contactpersoon 1'
                  DataBinding.FieldName = 'F_ORG_CONTACT1'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 180
                end
                object cxGridDBTableView2F_ORG_CONTACT1_EMAIL: TcxGridDBColumn
                  Caption = 'EMail'
                  DataBinding.FieldName = 'F_ORG_CONTACT1_EMAIL'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 159
                end
                object cxGridDBTableView2F_ORG_CONTACT2: TcxGridDBColumn
                  Caption = 'Contactpersoon 2'
                  DataBinding.FieldName = 'F_ORG_CONTACT2'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 180
                end
                object cxGridDBTableView2F_ORG_CONTACT2_EMAIL: TcxGridDBColumn
                  Caption = 'EMail'
                  DataBinding.FieldName = 'F_ORG_CONTACT2_EMAIL'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Width = 159
                end
                object cxGridDBTableView2F_CONFIRMATION_DATE: TcxGridDBColumn
                  Caption = 'Datum bevestiging'
                  DataBinding.FieldName = 'F_CONFIRMATION_DATE'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                  Options.Editing = False
                  Width = 130
                end
                object cxGridDBTableView2F_SESSION_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'F_SESSION_ID'
                  Visible = False
                end
              end
              object cxGridLevel2: TcxGridLevel
                GridView = cxGridDBTableView2
                MaxDetailHeight = 300
              end
            end
            object gbTeachExtra: TGroupBox
              Left = 8
              Top = 112
              Width = 490
              Height = 71
              Align = alCustom
              Anchors = [akLeft, akRight, akBottom]
              Caption = 'Extra'
              TabOrder = 1
              object FVBFFCDBExtra_Organisation: TFVBFFCDBMemo
                Left = 8
                Top = 16
                Align = alCustom
                Anchors = [akLeft, akTop, akRight, akBottom]
                DataBinding.DataField = 'F_EXTRA_ORGANISATION'
                DataBinding.DataSource = srcMain
                ParentFont = False
                Properties.ScrollBars = ssVertical
                TabOrder = 0
                Height = 49
                Width = 474
              end
            end
            object chkMailInfraAttachTeacher: TCheckBox
              Left = 8
              Top = 16
              Width = 700
              Height = 17
              Caption = 
                'Voeg de e-mail bijlage van de infrastructuur toe aan de mail naa' +
                'r de (organisatie van de) lesgever'
              TabOrder = 2
            end
            object cxBtnEnlargeTeacher: TFVBFFCButton
              Left = 468
              Top = 8
              Width = 30
              Height = 25
              Anchors = [akTop, akRight]
              OptionsImage.Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                3303333333333333300033333333333300033333333333300033333333333300
                0333333700073B7033333307777700B33333307F8F8F7033333377F8F9F8F773
                3333078F898F8703333307F99999F7033333078F898F8703333377F8F9F8F773
                3333307F8F8F7033333333077777033333333337000733333333}
              OptionsImage.Margin = 5
              OptionsImage.Spacing = -1
              TabOrder = 3
              TabStop = False
              OnClick = cxBtnEnlargeTeacherClick
            end
            object cxBtnNormalTeacher: TFVBFFCButton
              Left = 468
              Top = 8
              Width = 30
              Height = 25
              Anchors = [akTop, akRight]
              OptionsImage.Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                0400000000008000000000000000000000001000000000000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                3303333333333333300033333333333300033333333333300033333333333300
                0333333700073B7033333307777700B333333078F8F870333333778F8F8F8773
                333307F8F8F8F7033333078999998703333307F8F8F8F7033333778F8F8F8773
                33333078F8F87033333333077777033333333337000733333333}
              OptionsImage.Margin = 5
              OptionsImage.Spacing = -1
              TabOrder = 4
              TabStop = False
              OnClick = cxBtnNormalTeacherClick
            end
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 827
      Height = 597
      ExplicitLeft = 827
      ExplicitHeight = 597
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 597
      Visible = False
      ExplicitHeight = 597
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 597
      Visible = False
      ExplicitHeight = 597
    end
  end
  inherited srcMain: TFVBFFCDataSource
    Left = 60
    Top = 120
  end
  object srcCompanies: TFVBFFCDataSource
    DataSet = dtmConfirmationReports.cdsRecordCompaniesSchools
    OnStateChange = srcMainStateChange
    OnDataChange = srcDataChanged
    Left = 308
    Top = 152
  end
  object srcInfrastructure: TFVBFFCDataSource
    DataSet = dtmConfirmationReports.cdsRecordInfrastructure
    OnStateChange = srcMainStateChange
    OnDataChange = srcDataChanged
    Left = 308
    Top = 432
  end
  object srcOrganisation: TFVBFFCDataSource
    DataSet = dtmConfirmationReports.cdsRecordOrganisation
    OnStateChange = srcMainStateChange
    OnDataChange = srcDataChanged
    Left = 380
    Top = 592
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clWhite
    end
  end
end
