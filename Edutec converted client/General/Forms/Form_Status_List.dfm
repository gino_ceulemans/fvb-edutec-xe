inherited frmStatus_List: TfrmStatus_List
  Caption = 'Statussen'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_ID'
          Width = 100
        end
        object cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          Width = 200
        end
        object cxgrdtblvListF_LONG_DESC: TcxGridDBColumn
          DataBinding.FieldName = 'F_LONG_DESC'
          Width = 150
        end
        object cxgrdtblvListF_SESSION: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION'
          Width = 100
        end
        object cxgrdtblvListF_ENROL: TcxGridDBColumn
          DataBinding.FieldName = 'F_ENROL'
          Width = 100
        end
        object cxgrdtblvListF_WAITING_LIST: TcxGridDBColumn
          DataBinding.FieldName = 'F_WAITING_LIST'
          Width = 100
        end
        object cxgrdtblvListF_FOREGROUNDCOLOR: TcxGridDBColumn
          DataBinding.FieldName = 'F_FOREGROUNDCOLOR'
          Visible = False
        end
        object cxgrdtblvListF_BACKGROUNDCOLOR: TcxGridDBColumn
          DataBinding.FieldName = 'F_BACKGROUNDCOLOR'
          Visible = False
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
        end
        object cxgrdtblvListF_LONG_DESC_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_LONG_DESC_NL'
          Visible = False
        end
        object cxgrdtblvListF_LONG_DESC_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_LONG_DESC_FR'
          Visible = False
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
