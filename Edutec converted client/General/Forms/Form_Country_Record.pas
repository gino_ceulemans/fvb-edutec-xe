{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_COUNTRY record.

  @Name       Form_Country_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_Country_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, DB,
  Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons, cxDBLabel, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxControls, cxContainer, cxEdit, cxLabel, ActnList,
  Unit_FVBFFCComponents, Menus, cxSplitter, cxGraphics, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, System.Actions, cxClasses;

type
  TfrmCountry_Record = class(TEDURecordView)
    cxlblF_COUNTRY_ID2: TFVBFFCLabel;
    cxdblblF_COUNTRY_ID: TFVBFFCDBLabel;
    acCountryParts: TAction;
    dxnbiCountryPart: TdxNavBarItem;
    cxlblF_COUNTRY1: TFVBFFCLabel;
    cxdblblF_COUNTRY: TFVBFFCDBLabel;
    cxlblF_COUNTRY_ID1: TFVBFFCLabel;
    cxdbseF_COUNTRY_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    cxlblF_ISO_CODE1: TFVBFFCLabel;
    cxdbteF_ISO_CODE: TFVBFFCDBTextEdit;
    procedure acCountryPartsExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmCountry_Record: TfrmCountry_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Country, Data_EDUMainClient;

{ TfrmCountry_Record }

function TfrmCountry_Record.GetDetailName: String;
begin
  Result := cxdblblF_COUNTRY.Caption;
end;

procedure TfrmCountry_Record.acCountryPartsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Sender, 'TdtmCountryPart', pnlRecordDetail );
end;

end.
