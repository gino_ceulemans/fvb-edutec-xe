inherited frmPostalcode_Record: TfrmPostalcode_Record
  Width = 945
  ActiveControl = cxdbbeF_PROVINCE_NAME
  Caption = 'Postcode'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 937
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 601
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 937
    inherited pnlRecord: TFVBFFCPanel
      Width = 678
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 678
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 678
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 678
        object cxlblF_POSTALCODE_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Postcode ( ID )'
          FocusControl = cxdblblF_POSTALCODE_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_POSTALCODE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_POSTALCODE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 105
        end
        object cxlblF_POSTALCODE1: TFVBFFCLabel
          Left = 112
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Postcode'
          FocusControl = cxdblblF_POSTALCODE
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_POSTALCODE: TFVBFFCDBLabel
          Left = 112
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_POSTALCODE'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object cxlblF_CITY_NAME1: TFVBFFCLabel
          Left = 240
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmPostalcode_Record.F_CITY_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Gemeente'
          FocusControl = cxdblblF_CITY_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_CITY_NAME: TFVBFFCDBLabel
          Left = 240
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmPostalcode_Record.F_CITY_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_CITY_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 522
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 678
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 678
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 678
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 678
        end
        inherited sbxMain: TScrollBox
          Width = 678
          object cxlblF_POSTALCODE_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Postcode ( ID )'
            FocusControl = cxdbseF_POSTALCODE_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_POSTALCODE_ID: TFVBFFCDBSpinEdit
            Left = 120
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_POSTALCODE_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_CITY_NL1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_CITY_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gemeente ( NL )'
            FocusControl = cxdbteF_CITY_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CITY_NL: TFVBFFCDBTextEdit
            Left = 120
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_CITY_NL'
            DataBinding.DataField = 'F_CITY_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 353
          end
          object cxlblF_CITY_FR1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_CITY_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Gemeente ( FR )'
            FocusControl = cxdbteF_CITY_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_CITY_FR: TFVBFFCDBTextEdit
            Left = 120
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_CITY_FR'
            DataBinding.DataField = 'F_CITY_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 7
            Width = 353
          end
          object cxlblF_POSTALCODE2: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Postcode'
            FocusControl = cxdbteF_POSTALCODE
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_POSTALCODE: TFVBFFCDBTextEdit
            Left = 120
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_POSTALCODE'
            DataBinding.DataField = 'F_POSTALCODE'
            DataBinding.DataSource = srcMain
            TabOrder = 9
            Width = 121
          end
          object cxlblF_PROVINCE_NAME1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_PROVINCE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Provincie'
            FocusControl = cxdbbeF_PROVINCE_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_PROVINCE_NAME: TFVBFFCDBButtonEdit
            Left = 120
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmPostalcode_Record.F_PROVINCE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PROVINCE_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PROVINCE_NAMEPropertiesButtonClick
            TabOrder = 3
            Width = 353
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 933
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmPostalcode.cdsRecord
  end
end
