unit form_Attest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Unit_FVBFFCDevExpress, ExtCtrls, Unit_FVBFFCFoldablePanel, cxControls,
  cxContainer, cxEdit, cxLabel, cxTextEdit, cxMaskEdit, cxSpinEdit,
  ActnList, Unit_FVBFFCComponents;

type
  TAttestData = record
    TotalSessionHours : Integer;
    HoursPresent: Integer;
    HoursAbsentJustified: Integer;
    HoursAbsentIllegit: Integer;
    EnableAction : boolean;
  end;

  TfrmAttest = class(TForm)
    FVBFFCPanel1: TFVBFFCPanel;
    pnlBottom: TFVBFFCPanel;
    pnlBottomButtons: TFVBFFCPanel;
    cxbtnOK: TFVBFFCButton;
    cxbtnCancel: TFVBFFCButton;
    cxlblF_HOURS_PRESENT1: TFVBFFCLabel;
    cxlblF_HOURS_ABSENT_JUSTIFIED1: TFVBFFCLabel;
    cxlblF_HOURS_ABSENT_ILLEGIT1: TFVBFFCLabel;
    cxseF_HOURS_PRESENT: TFVBFFCSpinEdit;
    cxseF_HOURS_ABSENT_JUSTIFIED: TFVBFFCSpinEdit;
    cxseF_HOURS_ABSENT_ILLEGIT: TFVBFFCSpinEdit;
    alButtons: TFVBFFCActionList;
    acOk: TAction;
    acCancel: TAction;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    seTotal: TFVBFFCSpinEdit;
    bvlBevel1: TBevel;
    seSUM: TFVBFFCSpinEdit;
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    procedure acOkUpdate(Sender: TObject);
    procedure acOkExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function GetAttestData (var AttestData: TAttestData): TModalResult;
{
resourcestring
  rs_RecordsSelected = '%d afspraken geselecteerd';
}
implementation

{$R *.dfm}


{*****************************************************************************
  Creates the Attest form and obtains the attest data.
  @Name       GetAttestData
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function GetAttestData (var AttestData: TAttestData): TModalResult;
var
  frmAttest: TfrmAttest;
begin
  frmAttest := TfrmAttest.Create(nil);
  try
    frmAttest.seTotal.Value := AttestData.TotalSessionHours;
    result := frmAttest.ShowModal;
    AttestData.HoursPresent         :=
      frmAttest.cxseF_HOURS_PRESENT.Value;
    AttestData.HoursAbsentJustified :=
      frmAttest.cxseF_HOURS_ABSENT_JUSTIFIED.Value;
    AttestData.HoursAbsentIllegit   :=
      frmAttest.cxseF_HOURS_ABSENT_ILLEGIT.Value;
  finally
    freeAndNil(frmAttest);
  end;
end;

{*****************************************************************************
  Procedure to check if the input equals the total of hours of the master
  ( session ) and the total not equals zero. ( last not needed any more but
  can't hurt to have it checked more than once ;)

  @Name       TfrmAttest.acOkUpdate
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmAttest.acOkUpdate(Sender: TObject);
begin
  seSUM.Value :=
    cxseF_HOURS_PRESENT.Value +
    cxseF_HOURS_ABSENT_JUSTIFIED.Value +
    cxseF_HOURS_ABSENT_ILLEGIT.Value;

  (Sender as TAction).Enabled :=
    (seSUM.Value = seTotal.Value) and (seTotal.Value <> 0);
end;

procedure TfrmAttest.acOkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfrmAttest.acCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
