inherited frmCooperation_Record: TfrmCooperation_Record
  Left = 340
  Top = 279
  ActiveControl = cxdbteF_NAME_NL1
  Caption = 'In samenwerking met'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlTop: TFVBFFCPanel
    inherited pnlRecord: TFVBFFCPanel
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_COOPERATION_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCooperation_Record.F_COOPERATION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'I.s.m. ( ID )'
          FocusControl = cxdblblF_COOPERATION_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COOPERATION_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCooperation_Record.F_COOPERATION_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_COOPERATION_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 81
        end
        object cxlblF_COOPERATION_NAME1: TFVBFFCLabel
          Left = 112
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCooperation_Record.F_COOPERATION_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'In samenwerking met'
          FocusControl = cxdblblF_COOPERATION_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COOPERATION_NAME: TFVBFFCDBLabel
          Left = 112
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCooperation_Record.F_COOPERATION_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_COOPERATION_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        inherited sbxMain: TScrollBox
          object cxlblF_COOPERATION_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCooperation_Record.F_COOPERATION_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'I.s.m. ( ID )'
            FocusControl = cxdbseF_COOPERATION_ID1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_COOPERATION_ID1: TFVBFFCDBSpinEdit
            Left = 112
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCooperation_Record.F_COOPERATION_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_COOPERATION_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCooperation_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'I.s.m. ( NL )'
            FocusControl = cxdbteF_NAME_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL1: TFVBFFCDBTextEdit
            Left = 112
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCooperation_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 400
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCooperation_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'I.s.m. ( FR )'
            FocusControl = cxdbteF_NAME_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR1: TFVBFFCDBTextEdit
            Left = 112
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCooperation_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 400
          end
        end
      end
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmCooperation.cdsRecord
  end
end
