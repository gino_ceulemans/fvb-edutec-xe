inherited frmUser_Record: TfrmUser_Record
  Left = 175
  Top = 254
  Width = 992
  Height = 575
  ActiveControl = cxdbteF_LASTNAME
  Caption = 'Gebruiker'
  Constraints.MinHeight = 432
  Constraints.MinWidth = 992
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 516
    Width = 984
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 648
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 984
    Height = 516
    inherited pnlRecord: TFVBFFCPanel
      Width = 822
      Height = 516
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 822
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 822
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 822
        object cxlblF_USER_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmUser_Record.F_USER_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Gebruiker ( ID )'
          FocusControl = cxdblblF_USER_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_USER_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmUser_Record.F_USER_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_USER_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 113
        end
        object cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel
          Left = 120
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmUser_Record.F_COMPLETE_USERNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Gebruiker'
          FocusControl = cxdblblF_COMPLETE_USERNAME
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COMPLETE_USERNAME: TFVBFFCDBLabel
          Left = 120
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmUser_Record.F_COMPLETE_USERNAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_COMPLETE_USERNAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 697
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 822
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 512
        Width = 822
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 822
        Height = 442
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 822
        end
        inherited sbxMain: TScrollBox
          Width = 822
          Height = 417
          object cxgbAlgemeen: TFVBFFCGroupBox
            Left = 8
            Top = 8
            Caption = 'Algemeen'
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = True
            Height = 121
            Width = 369
            object cxlblF_USER_ID2: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_USER_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Gebruiker ( ID )'
              FocusControl = cxdbseF_USER_ID
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_USER_ID: TFVBFFCDBSpinEdit
              Left = 120
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_USER_ID'
              RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
              DataBinding.DataField = 'F_USER_ID'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 1
              Width = 121
            end
            object cxlblF_LASTNAME1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LASTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Naam'
              FocusControl = cxdbteF_LASTNAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_LASTNAME: TFVBFFCDBTextEdit
              Left = 120
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LASTNAME'
              DataBinding.DataField = 'F_LASTNAME'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 233
            end
            object cxlblF_FIRSTNAME1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_FIRSTNAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Voornaam'
              FocusControl = cxdbteF_FIRSTNAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit
              Left = 120
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_FIRSTNAME'
              DataBinding.DataField = 'F_FIRSTNAME'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 233
            end
            object cxlblF_GENDER_ID1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_GENDER_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Geslacht'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_GENDER_NAME: TFVBFFCDBButtonEdit
              Left = 120
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_GENDER_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_GENDER_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_GENDER_NAMEPropertiesButtonClick
              TabOrder = 7
              Width = 233
            end
          end
          object cxgbContactInfo: TFVBFFCGroupBox
            Left = 384
            Top = 8
            Caption = 'Contact Info'
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = True
            Height = 121
            Width = 425
            object cxlblF_PHONE_INT1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_PHONE_INT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Telefoon ( int )'
              FocusControl = cxdbteF_PHONE_INT
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_PHONE_INT: TFVBFFCDBTextEdit
              Left = 144
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_PHONE_INT'
              DataBinding.DataField = 'F_PHONE_INT'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 265
            end
            object cxlblF_PHONE_EXT1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_PHONE_EXT'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Telefoon ( ext )'
              FocusControl = cxdbteF_PHONE_EXT
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_PHONE_EXT: TFVBFFCDBTextEdit
              Left = 144
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_PHONE_EXT'
              DataBinding.DataField = 'F_PHONE_EXT'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 265
            end
            object cxlblF_GSM1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_GSM'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'GSM'
              FocusControl = cxdbteF_GSM
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_GSM: TFVBFFCDBTextEdit
              Left = 144
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_GSM'
              DataBinding.DataField = 'F_GSM'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 265
            end
            object cxlblF_EMAIL1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_EMAIL'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail'
              FocusControl = cxdbteF_EMAIL
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_EMAIL: TFVBFFCDBTextEdit
              Left = 144
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_EMAIL'
              DataBinding.DataField = 'F_EMAIL'
              DataBinding.DataSource = srcMain
              TabOrder = 7
              Width = 265
            end
          end
          object cxgbLogin: TFVBFFCGroupBox
            Left = 384
            Top = 136
            Caption = 'Login Info'
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = True
            Height = 121
            Width = 425
            object cxlblF_DOMAIN1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_DOMAIN'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Domein'
              FocusControl = cxdbteF_DOMAIN
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_DOMAIN: TFVBFFCDBTextEdit
              Left = 144
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_DOMAIN'
              DataBinding.DataField = 'F_DOMAIN'
              DataBinding.DataSource = srcMain
              TabOrder = 3
              Width = 265
            end
            object cxlblF_LANGUAGE_NAME1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LANGUAGE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Taal'
              FocusControl = cxdbbeF_LANGUAGE_NAME
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_LANGUAGE_NAME: TFVBFFCDBButtonEdit
              Left = 144
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LANGUAGE_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_LANGUAGE_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_LANGUAGE_NAMEPropertiesButtonClick
              TabOrder = 5
              Width = 265
            end
            object cxdbteF_LOGIN_ID: TFVBFFCDBTextEdit
              Left = 144
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOGIN_ID'
              DataBinding.DataField = 'F_LOGIN_ID'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 265
            end
            object cxlblF_LOGIN_ID1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOGIN_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Login ID'
              FocusControl = cxdbteF_LOGIN_ID
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_PASSWORD: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_PASSWORD'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Paswoord'
              FocusControl = cxdbteF_PASSWORD
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
              Visible = False
            end
            object cxdbteF_PASSWORD: TFVBFFCDBTextEdit
              Left = 144
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_PASSWORD'
              DataBinding.DataField = 'F_PASSWORD'
              DataBinding.DataSource = srcMain
              ParentFont = False
              Properties.EchoMode = eemPassword
              Properties.MaxLength = 20
              Properties.PasswordChar = '*'
              Style.TextColor = clWindowText
              Style.TransparentBorder = True
              TabOrder = 6
              Visible = False
              Width = 265
            end
          end
          object cxgbActivatie: TFVBFFCGroupBox
            Left = 8
            Top = 136
            Caption = 'Activatie Info'
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = True
            Height = 121
            Width = 369
            object cxlblF_ACTIVE1: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_ACTIVE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Actief'
              FocusControl = csdbcbF_ACTIVE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object csdbcbF_ACTIVE: TFVBFFCDBCheckBox
              Left = 120
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_ACTIVE'
              DataBinding.DataField = 'F_ACTIVE'
              DataBinding.DataSource = srcMain
              ParentColor = False
              Properties.NullStyle = nssInactive
              Properties.ReadOnly = True
              TabOrder = 1
              Width = 21
            end
            object cxlblF_LOCKED1: TFVBFFCLabel
              Left = 184
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOCKED'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Geblokkeerd'
              FocusControl = csdbcbF_LOCKED
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object csdbcbF_LOCKED: TFVBFFCDBCheckBox
              Left = 296
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOCKED'
              RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
              DataBinding.DataField = 'F_LOCKED'
              DataBinding.DataSource = srcMain
              ParentColor = False
              TabOrder = 3
              Width = 21
            end
            object cxlblF_START_DATE1: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_START_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Start Datum'
              FocusControl = cxdbdeF_START_DATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_START_DATE: TFVBFFCDBDateEdit
              Left = 120
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_START_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriDate
              DataBinding.DataField = 'F_START_DATE'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 200
            end
            object cxlblF_END_DATE1: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_END_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Eind Datum'
              FocusControl = cxdbdeF_END_DATE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbdeF_END_DATE: TFVBFFCDBDateEdit
              Left = 120
              Top = 64
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_END_DATE'
              RepositoryItem = dtmEDUMainClient.cxeriDate
              DataBinding.DataField = 'F_END_DATE'
              DataBinding.DataSource = srcMain
              TabOrder = 7
              Width = 200
            end
            object cxlblF_LOGIN_TRY1: TFVBFFCLabel
              Left = 8
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOGIN_TRY'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Aanlogpogingen'
              FocusControl = cxdbseF_LOGIN_TRY
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_LOGIN_TRY: TFVBFFCDBSpinEdit
              Left = 120
              Top = 88
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOGIN_TRY'
              DataBinding.DataField = 'F_LOGIN_TRY'
              DataBinding.DataSource = srcMain
              TabOrder = 9
              Width = 200
            end
          end
          object FVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 8
            Top = 264
            Caption = 'Printer info'
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = True
            Height = 89
            Width = 371
            object cxlblF_PRINTER_CERTIFICATE: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Printer certificaten'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_PRINTER_LETTER: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Printer brieven'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBButtonEditF_PRINTER_LETTER: TFVBFFCDBButtonEdit
              Left = 136
              Top = 16
              DataBinding.DataField = 'F_PRINTER_LETTER'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = OnChoosePrinter
              TabOrder = 2
              Width = 233
            end
            object FVBFFCDBButtonEditF_PRINTER_CERTIFICATE: TFVBFFCDBButtonEdit
              Left = 136
              Top = 40
              DataBinding.DataField = 'F_PRINTER_CERTIFICATE'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = OnChoosePrinter
              TabOrder = 3
              Width = 233
            end
            object cxlblF_PRINTER_EVALUATION: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Printer evaluatiedoc'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBButtonEditF_PRINTER_EVALUATION: TFVBFFCDBButtonEdit
              Left = 136
              Top = 64
              DataBinding.DataField = 'F_PRINTER_EVALUATION'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = OnChoosePrinter
              TabOrder = 5
              Width = 233
            end
          end
          object FVBFFCGroupBox2: TFVBFFCGroupBox
            Left = 384
            Top = 264
            Caption = 'Folders'
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = True
            Height = 89
            Width = 425
            object FVBFFCLabel4: TFVBFFCLabel
              Left = 8
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOGIN_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Bevestigingsrapporten'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBButtonEditF_PATH_CONFIRMATIONS: TFVBFFCDBButtonEdit
              Left = 144
              Top = 16
              DataBinding.DataField = 'F_PATH_CONFIRMATIONS'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = OnChooseFolder
              TabOrder = 1
              Width = 273
            end
            object FVBFFCLabel2: TFVBFFCLabel
              Left = 8
              Top = 40
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LOGIN_ID'
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Factuurbestanden'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBButtonEditF_PATH_INVOICES: TFVBFFCDBButtonEdit
              Left = 144
              Top = 40
              DataBinding.DataField = 'F_PATH_INVOICES'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = OnChooseFolder
              TabOrder = 2
              Width = 273
            end
            object cxlblF_PATH_CRREPORTS: TFVBFFCLabel
              Left = 8
              Top = 64
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'CR Reports'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object FVBFFCDBButtonEditF_PATH_CRREPORTS: TFVBFFCDBButtonEdit
              Left = 144
              Top = 64
              DataBinding.DataField = 'F_PATH_CRREPORTS'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.ReadOnly = False
              Properties.OnButtonClick = OnChooseFolder
              TabOrder = 3
              Width = 273
            end
          end
          object FVBFFCGroupBox3: TFVBFFCGroupBox
            Left = 8
            Top = 360
            Caption = 'Notificatie Bevestigingsbrieven'
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = True
            Height = 49
            Width = 371
            object cxlblNOTIFICATION: TFVBFFCLabel
              Left = 6
              Top = 16
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'EMail public folder'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_EMAIL_PUBLIC_FOLDER: TFVBFFCDBTextEdit
              Left = 120
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_LASTNAME'
              DataBinding.DataField = 'F_EMAIL_PUBLIC_FOLDER'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 241
            end
          end
          object FVBFFCGroupBox4: TFVBFFCGroupBox
            Left = 384
            Top = 360
            Caption = 'Bevestigingsbrieven'
            ParentColor = False
            ParentFont = False
            TabOrder = 7
            Transparent = True
            Visible = False
            Height = 49
            Width = 425
            object FVBFFCLabel1: TFVBFFCLabel
              Left = 6
              Top = 16
              HelpType = htKeyword
              RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
              Caption = 'Standaard Info Deelnemers'
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_DEFAULT_INFO_CONFIRMATION: TFVBFFCDBTextEdit
              Left = 176
              Top = 16
              HelpType = htKeyword
              HelpKeyword = 'frmUser_Record.F_DEFAULT_INFO_CONFIRMATION'
              DataBinding.DataField = 'F_DEFAULT_INFO_CONFIRMATION'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 241
            end
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 980
      Height = 516
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 516
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiProfielen
          end>
      end
      object dxnbiProfielen: TdxNavBarItem
        Action = acShowUserProfiles
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 516
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmUser.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    Left = 56
    Top = 256
    object acShowUserProfiles: TAction
      Caption = 'Profielen'
      OnExecute = acShowUserProfilesExecute
    end
  end
  object PrinterSetupDialog: TPrinterSetupDialog
    Left = 64
    Top = 384
  end
end
