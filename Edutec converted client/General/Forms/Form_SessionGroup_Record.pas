{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SES_GROUP record.

  @Name       Form_SessionGroup_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  01/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_SessionGroup_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxDBLabel, cxControls, cxContainer, cxEdit,
  cxLabel, Menus, cxSplitter;

type
  TfrmSessionGroup_Record = class(TEDURecordView)
    cxlblF_SESSIONGROUP_ID1: TFVBFFCLabel;
    cxdblblF_SESSIONGROUP_ID: TFVBFFCDBLabel;
    cxlblF_SESSIONGROUP_NAME1: TFVBFFCLabel;
    cxdblblF_SESSIONGROUP_NAME: TFVBFFCDBLabel;
    cxlblF_SESSIONGROUP_ID2: TFVBFFCLabel;
    cxdbseF_SESSIONGROUP_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    acShowSessions: TAction;
    dxnbiSessions: TdxNavBarItem;
    procedure acShowSessionsExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmSessionGroup_Record: TfrmSessionGroup_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_SessionGroup, Data_EDUMainClient;

{ TfrmSessionGroup_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmSessionGroup_Record.GetDetailName
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmSessionGroup_Record.GetDetailName: String;
begin
  Result := cxdblblF_SESSIONGROUP_NAME.Caption;
end;

procedure TfrmSessionGroup_Record.acShowSessionsExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmSesSession', pnlRecordDetail ); 
end;

end.