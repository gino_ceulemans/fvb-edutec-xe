unit Form_Languages_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_Phoenix_Record, Form_EduRecordView, cxLookAndFeelPainters,
  DB, Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons;

type
  TfrmLanguages_Record = class(TEduRecordView)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLanguages_Record: TfrmLanguages_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Languages;

end.