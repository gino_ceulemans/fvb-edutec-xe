inherited frmProfile_Record: TfrmProfile_Record
  Width = 1003
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Profiel'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 995
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 659
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 995
    inherited pnlRecord: TFVBFFCPanel
      Width = 736
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 736
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 736
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 736
        object cxlblF_PROFILE_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProfile_Record.F_PROFILE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Profiel ( ID )'
          FocusControl = cxdblblF_PROFILE_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROFILE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProfile_Record.F_PROFILE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROFILE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 73
        end
        object cxlblF_PROFILE_NAME1: TFVBFFCLabel
          Left = 104
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmProfile_Record.F_PROFILE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Profiel'
          FocusControl = cxdblblF_PROFILE_NAME
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROFILE_NAME: TFVBFFCDBLabel
          Left = 104
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmProfile_Record.F_PROFILE_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_PROFILE_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 716
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 736
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 736
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 736
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 736
        end
        inherited sbxMain: TScrollBox
          Width = 736
          object cxlblF_PROFILE_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProfile_Record.F_PROFILE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Profiel ( ID )'
            FocusControl = cxdbseF_PROFILE_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PROFILE_ID: TFVBFFCDBSpinEdit
            Left = 104
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmProfile_Record.F_PROFILE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PROFILE_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProfile_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Profiel ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 104
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmProfile_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 369
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProfile_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Profiel ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 104
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmProfile_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 369
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 991
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiUsers
          end>
      end
      object dxnbiUsers: TdxNavBarItem
        Action = acShowProfileUsers
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmProfile.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowProfileUsers: TAction
      Caption = 'Gebruikers'
      OnExecute = acShowProfileUsersExecute
    end
  end
end
