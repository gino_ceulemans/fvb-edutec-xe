{*****************************************************************************
  This unit contains the form that will be  used for the maintenance of a
  T_GE_COUNTRY_PART Record.
  
  @Name       Form_CountryPart_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_CountryPart_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, DB,
  Unit_FVBFFCDBComponents, dxNavBarCollns, dxNavBarBase, dxNavBar,
  Unit_FVBFFCDevExpress, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel,
  StdCtrls, Buttons, cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxDBLabel, cxControls, cxContainer, cxEdit, cxLabel, ActnList,
  Unit_FVBFFCComponents, cxButtonEdit, Unit_PPWFrameWorkInterfaces, Menus,
  cxSplitter;

type
  TfrmCountryPart_Record = class(TEDURecordView)
    cxlblF_COUNTRYPART_ID1: TFVBFFCLabel;
    cxdblblF_COUNTRYPART_ID: TFVBFFCDBLabel;
    cxlblF_COUNTRY_PART1: TFVBFFCLabel;
    cxdblblF_COUNTRY_PART: TFVBFFCDBLabel;
    acShowProvinces: TAction;
    dxnbiShowProvinces: TdxNavBarItem;
    cxlblF_COUNTRYPART_ID2: TFVBFFCLabel;
    cxdbseF_COUNTRYPART_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    cxlblF_COUNTRY_NAME1: TFVBFFCLabel;
    cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit;
    procedure cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure acShowProvincesExecute(Sender: TObject);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmCountryPart_Record: TfrmCountryPart_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_CountryPart, Data_EDUMainClient, Unit_FVBFFCInterfaces, Unit_PPWFrameWorkClasses,
  Unit_FVBFFCRecordView, unit_EdutecInterfaces;

{ TfrmCountryPart_Record }

function TfrmCountryPart_Record.GetDetailName: String;
begin
  Result := cxdblblF_COUNTRY_PART.Caption;
end;

procedure TfrmCountryPart_Record.cxdbbeF_COUNTRY_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  adtmCountryPart : IEDUCountryPartDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUCountryPartDataModule, adtmCountryPart ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        adtmCountryPart.ShowCountry( srcMain.DataSet, rvmEdit );
      end;
      else
      begin
        adtmCountryPart.SelectCountry( srcMain.DataSet );
      end;
    end;
  end;
end;

procedure TfrmCountryPart_Record.acShowProvincesExecute(Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmProvince', pnlRecordDetail );
end;

{*****************************************************************************
  This method will be executed when the form is Initalised and will be used
  to set up some things.

  @Name       TfrmCountryPart_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      aFrameWorkDataModule   The DataModule associated to the Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmCountryPart_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUCountryDataModule ) ) then
    begin
      cxdbbeF_COUNTRY_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbteF_NAME_NL );
//      aFrameWorkDataModule.RecordDataset.FieldByName( 'F_COUNTRY_NAME' ).ReadOnly := True;
//      cxdbbeF_COUNTRY_NAME.Style.StyleController := ReadOnlyStyleController;
    end;
  end;
end;

end.
