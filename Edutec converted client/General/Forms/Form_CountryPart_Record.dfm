inherited frmCountryPart_Record: TfrmCountryPart_Record
  Left = 215
  Top = 249
  Width = 809
  ActiveControl = cxdbbeF_COUNTRY_NAME
  Caption = 'Landsgedeelte'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 801
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 465
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 801
    inherited pnlRecord: TFVBFFCPanel
      Width = 639
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 639
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 639
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 639
        object cxlblF_COUNTRYPART_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCountryPart_Record.F_COUNTRYPART_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Landsgedeelte ( ID )'
          FocusControl = cxdblblF_COUNTRYPART_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COUNTRYPART_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCountryPart_Record.F_COUNTRYPART_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_COUNTRYPART_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Height = 21
          Width = 129
        end
        object cxlblF_COUNTRY_PART1: TFVBFFCLabel
          Left = 144
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCountryPart_Record.F_COUNTRY_PART'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Landsgedeelte'
          FocusControl = cxdblblF_COUNTRY_PART
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_COUNTRY_PART: TFVBFFCDBLabel
          Left = 144
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCountryPart_Record.F_COUNTRY_PART'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_COUNTRY_PART_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'MS Sans Serif'
          Style.Font.Style = [fsBold]
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Height = 21
          Width = 482
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 639
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 639
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 639
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 639
        end
        inherited sbxMain: TScrollBox
          Width = 639
          object cxlblF_COUNTRYPART_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_COUNTRYPART_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Landsgedeelte ( ID )'
            FocusControl = cxdbseF_COUNTRYPART_ID
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_COUNTRYPART_ID: TFVBFFCDBSpinEdit
            Left = 144
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_COUNTRYPART_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_COUNTRYPART_ID'
            DataBinding.DataSource = srcMain
            Properties.ReadOnly = True
            TabOrder = 0
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Landsgedeelte ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 144
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 2
            Width = 337
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Landsgedeelte ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 144
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 337
          end
          object cxlblF_COUNTRY_NAME1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_COUNTRY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Land'
            FocusControl = cxdbbeF_COUNTRY_NAME
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_COUNTRY_NAME: TFVBFFCDBButtonEdit
            Left = 144
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCountryPart_Record.F_COUNTRY_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_COUNTRY_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_COUNTRY_NAMEPropertiesButtonClick
            TabOrder = 1
            Width = 337
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 797
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiShowProvinces
          end>
      end
      object dxnbiShowProvinces: TdxNavBarItem
        Action = acShowProvinces
      end
    end
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowProvinces: TAction
      Caption = 'Provincies'
      OnExecute = acShowProvincesExecute
    end
  end
end
