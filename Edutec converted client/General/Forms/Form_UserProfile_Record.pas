{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_SYS_USER_PROF record.

  @Name       Form_UserProfile_Record
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  05/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_UserProfile_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxMaskEdit, cxButtonEdit,
  cxDBEdit, cxTextEdit, cxDBLabel, cxControls, cxContainer, cxEdit, cxLabel,
  Unit_PPWFrameWorkInterfaces, Menus, cxSplitter;

type
  TfrmUserProfile_Record = class(TEDURecordView)
    cxlblF_COMPLETE_USERNAME1: TFVBFFCLabel;
    cxdblblF_COMPLETE_USERNAME: TFVBFFCDBLabel;
    cxlblF_PROFILE_NAME1: TFVBFFCLabel;
    cxdblblF_PROFILE_NAME: TFVBFFCDBLabel;
    cxlblF_COMPLETE_USERNAME2: TFVBFFCLabel;
    cxlblF_LASTNAME1: TFVBFFCLabel;
    cxlblF_FIRSTNAME1: TFVBFFCLabel;
    cxdbteF_FIRSTNAME: TFVBFFCDBTextEdit;
    cxdbteF_LASTNAME: TFVBFFCDBTextEdit;
    cxdbbeF_COMPLETE_USERNAME: TFVBFFCDBButtonEdit;
    cxlblF_PROFILE_NAME2: TFVBFFCLabel;
    cxdbbeF_PROFILE_NAME: TFVBFFCDBButtonEdit;
    procedure cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_PROFILE_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

var
  frmUserProfile_Record: TfrmUserProfile_Record;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_UserProfile, Data_EDUMainClient, Unit_FVBFFCRecordView,
  Unit_FVBFFCInterfaces, Unit_PPWFrameWorkClasses, Form_FVBFFCBaseRecordView,
  unit_EdutecInterfaces;

{ TfrmUserProfile_Record }

function TfrmUserProfile_Record.GetDetailName: String;
begin
  Result := cxdblblF_COMPLETE_USERNAME.Caption + '/' + cxdblblF_PROFILE_NAME.Caption;
end;

procedure TfrmUserProfile_Record.cxdbbeF_COMPLETE_USERNAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUUserProfileDataModule;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUUserProfileDataModule, aDataModule ) ) and
     ( Mode in [ rvmEdit ] ) and
     not ( cxdbbeF_COMPLETE_USERNAME.RepositoryItem.Properties.ReadOnly ) then
  begin
    case aButtonIndex of
      sltView :
      begin
        aDataModule.ShowUser( srcMain.DataSet, rvmEdit );
      end;
      sltAdd :
      begin
        aDataModule.ShowUser( srcMain.DataSet, rvmAdd );
      end;
      else
      begin
        aDataModule.SelectUser( srcMain.DataSet );
      end;
    end;
  end;
end;

procedure TfrmUserProfile_Record.cxdbbeF_PROFILE_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUUserProfileDataModule;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUUserProfileDataModule, aDataModule ) ) and
     ( Mode in [ rvmEdit ] ) and
     not ( cxdbbeF_PROFILE_NAME.RepositoryItem.Properties.ReadOnly ) then
  begin
    case aButtonIndex of
      sltView :
      begin
        aDataModule.ShowProfile( srcMain.DataSet, rvmEdit );
      end;
      sltAdd :
      begin
        aDataModule.ShowProfile( srcMain.DataSet, rvmAdd );
      end;
      else
      begin
        aDataModule.SelectProfile( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the form is initialise and will be used
  to initially Enable / Disable some components.

  @Name       TfrmUserProfile_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmUserProfile_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUUserDataModule ) ) then
    begin
      cxdbbeF_COMPLETE_USERNAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl( cxdbbeF_PROFILE_NAME );
    end
    else if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUProfileDataModule ) ) then
    begin
      cxdbbeF_PROFILE_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl( cxdbbeF_COMPLETE_USERNAME );
    end
  end;
end;

end.
