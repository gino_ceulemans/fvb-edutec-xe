inherited dtmParameterType: TdtmParameterType
  KeyFields = 'F_PARAMETER_TYPE_ID'
  ListViewClass = 'TfrmParameterType_List'
  RecordViewClass = 'TfrmParameterType_Record'
  Registered = True
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      ' F_PARAMETER_TYPE_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR,'
      ' F_NAME'
      'FROM  '
      ' V_DBW_PARAMETER_TYPE')
    AutoOpen = True
    object qryListF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 50
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 50
    end
    object qryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 50
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT     '
      ' F_PARAMETER_TYPE_ID, '
      ' F_NAME_NL, '
      ' F_NAME_FR,'
      ' F_NAME'
      'FROM  '
      ' V_DBW_PARAMETER_TYPE')
    object qryRecordF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 50
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 50
    end
    object qryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 50
    end
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsListF_PARAMETER_TYPE_ID: TIntegerField
      DisplayLabel = 'Type ( ID )'
      FieldName = 'F_PARAMETER_TYPE_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 50
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 50
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Size = 50
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsRecordF_PARAMETER_TYPE_ID: TIntegerField
      DisplayLabel = 'Type ( ID )'
      FieldName = 'F_PARAMETER_TYPE_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 50
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 50
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Visible = False
      Size = 50
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmParameterType'
  end
end
