{*****************************************************************************
  This DataModule will be used for the maintenance of REPORTS.

  @Name       Data_Report
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Report;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, Contnrs, Unit_PPWFrameWorkInterfaces, unit_EdutecInterfaces;

type TParamItem = class
  private
    { Private declarations }
    FParamName: string;
    FParamValue: variant;
    procedure SetParamName(const Value: string);
    procedure SetParamValue(const Value: variant);
  public
    { Public declarations }
    property ParamName: string read FParamName write SetParamName;
    property ParamValue : variant read FParamValue write SetParamValue;

end;

type
  TdtmReport = class(TEDUdatamodule, IEDUReportDataModule )
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure ChangeReport(aSqlStatement : string;aParamCollection : TObjectList ); virtual;
  public
    { Public declarations }
    class function OpenReport ( aSqlStatement : string; aParamCollection : TObjectList ) : TModalResult;

  end;

implementation

uses Data_EduMainClient, Data_FVBFFCBaseDataModule,
  Unit_PPWFrameWorkDataModule;

{$R *.dfm}

{ TdtmReport }

{*****************************************************************************
  Procedure used to change the CommandText of cdsList and populate the
  params and fill in the values of these params and execute the statement.

  @Name       TdtmReport.ChangeReport
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmReport.ChangeReport(aSqlStatement: string;
  aParamCollection: TObjectList);
var
   i : integer;
   aParam : TParam;
   aParamItem : TParamItem;
begin
  cdsList.Close;
//  cdsList.CommandText := aSqlStatement;

  ssckData.AppServer.SetSQLStatement( cdsList.ProviderName, aSqlStatement );

  cdsList.FetchParams;
  if ( cdsList.Params.Count > 0 ) then
  begin
    for i:= 0 to aParamCollection.Count -1 do
    begin
      aParamItem := TParamItem(aParamCollection.Items[i]);
      try
        aParam := cdsList.Params.FindParam(aParamItem.ParamName);
        if Assigned( aParam ) then
        begin
          aParam.Value := aParamItem.ParamValue;
        end;
      except
      end;
    end;
  end;
  cdsList.Open;
//  KeyFields := cdsList.FieldDefs[0].Name;
end;

{*****************************************************************************
  class function to Show the report modal.

  @Name       TdtmReport.OpenReport
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
  @History
  Date                Modified by             Descr
  18/09/2007          ivdbossche              Added crSQLWait cursor
******************************************************************************}

class function TdtmReport.OpenReport(aSqlStatement : string;
  aParamCollection: TObjectList): TModalResult;
var
  aReportDataModule    : IEDUReportDataModule;
  aDataModule          : IPPWFrameWorkDataModule;
  aClientDataSet       : TClientDataSet;
  aListView            : IPPWFrameWorkListView;
  oldcursor            : TCursor;
begin
  Result := mrCancel;

  aClientDataSet := TClientDataSet.Create( Nil );

  try
    aDataModule := dtmEDUMainClient.pfcMain.CreateDataModule( Nil, 'TdtmReport', False );

    { If necessary we should do some initialisations in here }
    if ( Supports( aDataModule, IEDUReportDataModule, aReportDataModule ) ) then
    begin
      // Initialisation as required ;)
      oldcursor := Screen.Cursor;
      try
        Screen.Cursor := crSQLWait;
        aReportDataModule.ChangeReport( aSqlStatement, aParamCollection );
        // Creation as requiered
        aListView := dtmEDUMainClient.pfcMain.CreateListViewForDataModule( aReportDataModule );
      finally
        Screen.Cursor := oldcursor;
      end;
      // Showing as required
      Result := aListView.ShowModal;
    end;
  finally
    FreeAndNil( aClientDataSet );
    aListView := Nil;
    aReportDataModule := Nil;
    aDataModule := Nil;
  end;
end;

{ TParamItem }

procedure TParamItem.SetParamName(const Value: string);
begin
  FParamName := Value;
end;

procedure TParamItem.SetParamValue(const Value: variant);
begin
  FParamValue := Value;
end;


end.
