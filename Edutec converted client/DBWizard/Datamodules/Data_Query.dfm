inherited dtmQuery: TdtmQuery
  KeyFields = 'F_QUERY_ID'
  ListViewClass = 'TfrmQuery_List'
  RecordViewClass = 'TfrmQuery_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Left = 294
  Top = 359
  DetailDataModules = <
    item
      Caption = 'Parameter'
      ForeignKeys = 'F_QUERY_ID'
      DataModuleClass = 'TdtmParameter'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' F_QUERY_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_QUERY_GROUP_ID,'
      ' F_TEXT,'
      ' F_DESCRIPTION_NL,'
      ' F_DESCRIPTION_FR,'
      ' F_DESCRIPTION,'
      ' F_QUERY_GROUP_NAME'
      'FROM'
      ' V_DBW_QUERY')
    object qryListF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 256
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 256
    end
    object qryListF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
    end
    object qryListF_TEXT: TMemoField
      FieldName = 'F_TEXT'
      BlobType = ftMemo
    end
    object qryListF_DESCRIPTION_NL: TMemoField
      FieldName = 'F_DESCRIPTION_NL'
      BlobType = ftMemo
    end
    object qryListF_DESCRIPTION_FR: TMemoField
      FieldName = 'F_DESCRIPTION_FR'
      BlobType = ftMemo
    end
    object qryListF_DESCRIPTION: TMemoField
      FieldName = 'F_DESCRIPTION'
      ReadOnly = True
      BlobType = ftMemo
    end
    object qryListF_QUERY_GROUP_NAME: TStringField
      FieldName = 'F_QUERY_GROUP_NAME'
      ReadOnly = True
      Size = 256
    end
    object qryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 256
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' F_QUERY_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_QUERY_GROUP_ID,'
      ' F_TEXT,'
      ' F_DESCRIPTION_NL,'
      ' F_DESCRIPTION_FR,'
      ' F_DESCRIPTION,'
      ' F_QUERY_GROUP_NAME'
      'FROM'
      ' V_DBW_QUERY')
    object qryRecordF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 256
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 256
    end
    object qryRecordF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
    end
    object qryRecordF_TEXT: TMemoField
      FieldName = 'F_TEXT'
      BlobType = ftMemo
    end
    object qryRecordF_DESCRIPTION_NL: TMemoField
      FieldName = 'F_DESCRIPTION_NL'
      BlobType = ftMemo
    end
    object qryRecordF_DESCRIPTION_FR: TMemoField
      FieldName = 'F_DESCRIPTION_FR'
      BlobType = ftMemo
    end
    object qryRecordF_DESCRIPTION: TMemoField
      FieldName = 'F_DESCRIPTION'
      ReadOnly = True
      BlobType = ftMemo
    end
    object qryRecordF_QUERY_GROUP_NAME: TStringField
      FieldName = 'F_QUERY_GROUP_NAME'
      ReadOnly = True
      Size = 256
    end
    object qryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 256
    end
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsListF_QUERY_ID: TIntegerField
      DisplayLabel = 'Query ( ID )'
      FieldName = 'F_QUERY_ID'
      Required = True
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Query ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Visible = False
      Size = 256
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Query ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Visible = False
      Size = 256
    end
    object cdsListF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsListF_TEXT: TMemoField
      DisplayLabel = 'Sql statement'
      FieldName = 'F_TEXT'
      Required = True
      BlobType = ftMemo
    end
    object cdsListF_DESCRIPTION_NL: TMemoField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_DESCRIPTION_NL'
      Visible = False
      BlobType = ftMemo
    end
    object cdsListF_DESCRIPTION_FR: TMemoField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_DESCRIPTION_FR'
      Visible = False
      BlobType = ftMemo
    end
    object cdsListF_DESCRIPTION: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_DESCRIPTION'
      BlobType = ftMemo
    end
    object cdsListF_QUERY_GROUP_NAME: TStringField
      DisplayLabel = 'Groep'
      FieldName = 'F_QUERY_GROUP_NAME'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 256
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Query'
      FieldName = 'F_NAME'
      Size = 256
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsRecordF_QUERY_ID: TIntegerField
      DisplayLabel = 'Query ( ID )'
      FieldName = 'F_QUERY_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Query ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 256
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Query ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 256
    end
    object cdsRecordF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
    end
    object cdsRecordF_TEXT: TMemoField
      DisplayLabel = 'Sql statement'
      FieldName = 'F_TEXT'
      Required = True
      BlobType = ftMemo
    end
    object cdsRecordF_DESCRIPTION_NL: TMemoField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_DESCRIPTION_NL'
      BlobType = ftMemo
    end
    object cdsRecordF_DESCRIPTION_FR: TMemoField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_DESCRIPTION_FR'
      BlobType = ftMemo
    end
    object cdsRecordF_DESCRIPTION: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_DESCRIPTION'
      Visible = False
      BlobType = ftMemo
    end
    object cdsRecordF_QUERY_GROUP_NAME: TStringField
      DisplayLabel = 'Groep'
      FieldName = 'F_QUERY_GROUP_NAME'
      Required = True
      Size = 256
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Query'
      FieldName = 'F_NAME'
      Visible = False
      Size = 256
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmQuery'
  end
end
