{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_Tablefield
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Tablefield;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Data_EduDataModule, Unit_PPWFrameWorkComponents,
  Unit_FVBFFCDBComponents, Provider, DB, ADODB, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, MConnect, unit_EdutecInterfaces;

type
  TdtmTableField = class(TEDUdatamodule, IEDUTableFieldDatamodule )
    qryListTableName: TWideStringField;
    qryListFieldName: TWideStringField;
    qryRecordTableName: TWideStringField;
    qryRecordFieldName: TWideStringField;
    cdsListTableName: TWideStringField;
    cdsListFieldName: TWideStringField;
    cdsRecordTableName: TWideStringField;
    cdsRecordFieldName: TWideStringField;
    cdsSearchCriteriaTableName: TWideStringField;
    cdsSearchCriteriaFieldName: TWideStringField;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure SelectTableField( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowTableField  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

uses Data_EduMainClient;

{$R *.dfm}


{ dtmTableField }

{*****************************************************************************
  This procedure will be used to copy TableField related fields from one
  DataSet to another DataSet.

  @Name       CopyTableFieldFieldValues
  @author     cheuten
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyTableFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      if aDestination.FieldByName( 'F_TABLE' ).AsString <> aSource.FieldByName( 'TableName' ).AsString then
      begin
        aDestination.FieldByName( 'F_KEY' ).Value := Null;
        aDestination.FieldByName('F_RESULT_NL').Value := Null;
        aDestination.FieldByName('F_RESULT_FR').Value := Null;
      end;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_TABLE' ).AsString :=
          aSource.FieldByName( 'TableName' ).AsString;
        aDestination.FieldByName( 'F_KEY' ).AsString :=
          aSource.FieldByName( 'FieldName' ).AsString;
      end;
      critResultNlToLookUp:
      begin
        aDestination.FieldByName('F_RESULT_NL').AsString :=
          aSource.FieldByName( 'FieldName' ).AsString;
      end;
      critResultFrToLookUp:
      begin
        aDestination.FieldByName('F_RESULT_FR').AsString :=
          aSource.FieldByName( 'FieldName' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more TableField Records.

  @Name       TdtmTableField.SelectTableField
  @author     cheuten
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}


class procedure TdtmTableField.SelectTableField(aDataSet: TDataSet;
  aWhere: String; aMultiSelect: Boolean;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmTableField', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyTableFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

procedure TdtmTableField.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_DBW_TABLEFIELD';
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Tablefield
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmTableField.ShowTableField
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmTableField.ShowTableField(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmTableField', 'TableName = ' + aDataSet.FieldByName( 'F_TABLE' ).AsString + ' AND FieldName ' + aDataSet.FieldByName( 'F_KEY' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyTableFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

end.