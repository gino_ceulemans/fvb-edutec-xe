inherited dtmTablefield: TdtmTablefield
  ListViewClass = 'TfrmTablefield_List'
  RecordViewClass = 'TfrmTablefield_Record'
  Registered = True
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' TableName,'
      ' FieldName'
      'FROM'
      ' V_DBW_TABLEFIELD'
      'Order by TableName, FieldName')
    object qryListTableName: TWideStringField
      FieldName = 'TableName'
      Size = 128
    end
    object qryListFieldName: TWideStringField
      FieldName = 'FieldName'
      Size = 128
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT'
      ' TableName,'
      ' FieldName'
      'FROM'
      ' V_DBW_TABLEFIELD'
      'ORDER BY TableName, FieldName')
    object qryRecordTableName: TWideStringField
      FieldName = 'TableName'
      Size = 128
    end
    object qryRecordFieldName: TWideStringField
      FieldName = 'FieldName'
      Size = 128
    end
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsListTableName: TWideStringField
      DisplayLabel = 'Tabelnaam'
      FieldName = 'TableName'
      Size = 128
    end
    object cdsListFieldName: TWideStringField
      DisplayLabel = 'Veldnaam'
      FieldName = 'FieldName'
      Size = 128
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsRecordTableName: TWideStringField
      DisplayLabel = 'Tabelnaam'
      FieldName = 'TableName'
      Size = 128
    end
    object cdsRecordFieldName: TWideStringField
      DisplayLabel = 'Veldnaam'
      FieldName = 'FieldName'
      Size = 128
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaTableName: TWideStringField
      DisplayLabel = 'Tabelnaam'
      FieldName = 'TableName'
      Size = 128
    end
    object cdsSearchCriteriaFieldName: TWideStringField
      DisplayLabel = 'Veldnaam'
      FieldName = 'FieldName'
      Size = 128
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmTableField'
  end
end
