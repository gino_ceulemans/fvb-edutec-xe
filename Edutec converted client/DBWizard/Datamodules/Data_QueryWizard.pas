{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_QueryWizard
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_QueryWizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, MConnect, Contnrs, Unit_PPWFrameWorkInterfaces, unit_EdutecInterfaces,
  Datasnap.DSConnect;

type TParamItem = class
  private
    { Private declarations }
    FParamName: string;
    FParamValue: variant;
    procedure SetParamName(const Value: string);
    procedure SetParamValue(const Value: variant);
  public
    { Public declarations }
    property ParamName: string read FParamName write SetParamName;
    property ParamValue : variant read FParamValue write SetParamValue;

end;

type
  TdtmQueryWizard = class( TEDUDataModule, IEDUQueryWizardDataModule )
    cdsGroup: TFVBFFCClientDataSet;
    qryListF_QUERY_GROUP_ID: TIntegerField;
    qryListF_NAME: TStringField;
    qryListF_PARENT_ID: TIntegerField;
    qryListF_PARENT_NAME: TStringField;
    qryListF_QUERY_COUNT: TIntegerField;
    cdsListF_QUERY_GROUP_ID: TIntegerField;
    cdsListF_QUERY_GROUP_NAME: TStringField;
    cdsListF_QUERY_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_DESCRIPTION: TMemoField;
    cdsListF_TEXT: TMemoField;
    cdsListF_PARAMETER_COUNT: TIntegerField;
    srcGroup: TFVBFFCDataSource;
    cdsGroupF_QUERY_GROUP_ID: TIntegerField;
    cdsGroupF_NAME: TStringField;
    cdsGroupF_PARENT_ID: TIntegerField;
    cdsGroupF_PARENT_NAME: TStringField;
    cdsGroupF_QUERY_COUNT: TIntegerField;
    cdsRecordF_QUERY_GROUP_ID: TIntegerField;
    cdsRecordF_QUERY_GROUP_NAME: TStringField;
    cdsRecordF_QUERY_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_DESCRIPTION: TMemoField;
    cdsRecordF_TEXT: TMemoField;
    cdsRecordF_PARAMETER_COUNT: TIntegerField;
    cdsParameter: TFVBFFCClientDataSet;
    cdsParameterF_QUERY_ID: TIntegerField;
    cdsParameterF_QUERY_NAME: TStringField;
    cdsParameterF_PARAMETER_ID: TIntegerField;
    cdsParameterF_NAME: TStringField;
    cdsParameterF_CAPTION: TStringField;
    cdsParameterF_PARAMETER_TYPE_ID: TIntegerField;
    cdsParameterF_PARAMETER_TYPE: TStringField;
    cdsParameterF_TABLE: TStringField;
    cdsParameterF_KEY: TStringField;
    cdsParameterF_RESULT: TStringField;
    cdsParameterF_QUERY_GROUP_ID: TIntegerField;
    srcQuery: TFVBFFCDataSource;
    cdsSelectLookUp: TFVBFFCClientDataSet;
    procedure prvListGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);

  private
    { Private declarations }
    fSqlStatement : string;
    fParamCount : integer;
    fParamCollection : TObjectList;

  protected
    { Protected declarations }
    function GetParameterDataSet: TClientDataSet; virtual;
    function GetTreeViewDataSet: TClientDataSet; virtual;

    property TreeViewDataSet       : TClientDataSet read GetTreeViewDataSet;
    property ParameterDataSet      : TClientDataSet read GetParameterDataSet;

    procedure FillSelectLookUp( aStringList : TStringList; aTable, aKey, aResult : string ); virtual;

    procedure FillSqlStatement( aSqlStatement : string; aParamCount : integer = 0 ); virtual;
    procedure AddParameter( aParamName : string; aParamValue : variant ); virtual;
    procedure ShowReport; virtual;

    function ShowRecordViewForCurrentRecord( aRecordViewMode: TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False): IPPWFrameWorkRecordView; override;


  public
    { Public declarations }
    class function OpenWizard          ( aDataSet : TDataSet ) : TModalResult;

  end;

implementation

uses Data_EduMainClient, Unit_PPWFrameWorkUtils,
  Data_Report, Data_FVBFFCBaseDataModule;

{$R *.dfm}


{ TdtmQueryWizard }

{*****************************************************************************
  Procedure will make a RecordView modal by default. ( Inherited feature )

  @Name       TdtmQueryWizard.ShowRecordViewForCurrentRecord
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmQueryWizard.ShowRecordViewForCurrentRecord(
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aShowModal: Boolean): IPPWFrameWorkRecordView;
begin
  Inherited ShowRecordViewForCurrentRecord( aRecordViewMode, True );
end;

{*****************************************************************************
  Procedure to retrieve a usefull dataset and load the data into a stringlist.

  @Name       TdtmQueryWizard.FillSelectLookUp
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryWizard.FillSelectLookUp(aStringList : TStringList; aTable,
  aKey, aResult: string);
begin
  // Zorg ervoor dat de dataset zo goed mogelijk wordt opgevuld met de
  // data die aangeleverd zal worden door de oproep van de stored proc
  with cdsSelectLookUp do
  begin
    Close;
    FetchParams;
    Params.ParamByName( '@TableName' ).AsString := aTable;
    Params.ParamByName( '@FieldId'  ).AsString := aKey;
    Params.ParamByName( '@FieldResult' ).AsString := aResult;
    // De hierboven staande drie parameters zijn noodzakelijk om een tabel
    // via een welbepaalde stored proc op te roepen. Indien er een parameter niet
    // aanwezig is dan zal dit resulteren in een lege dataset als resultaat
    // eerder dan een foutmelding en geen resultaat.
    // ( dit is opgevangen in de stored proc als extra controle )
    Open;
    First;
    while not Eof do
    begin
      // Door deze stringlist op deze wijze op te vullen kan men via Values en Names werken
      // bij het laden van de "gewone"combobox
      aStringList.Add(FieldByName('F_ID').AsString + '=' + FieldByName('F_RESULT').AsString );
      Next;
    end;
    Close;
  end;
end;

{*****************************************************************************
  Function to retrieve instance of the ParameterDataset.
  ( cdsList & cdsRecord are linked automatically )

  @Name       TdtmQueryWizard.GetParameterDataSet
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmQueryWizard.GetParameterDataSet: TClientDataSet;
var
  aDataModule : IEDUQueryWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUQueryWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.ParameterDataSet;
  end
  else
  begin
    Result := cdsParameter;
  end;
end;

{*****************************************************************************
  Function to retrieve instance of the TreeViewDataset.
  ( cdsList & cdsRecord are linked automatically )

  @Name       TdtmQueryWizard.GetTreeViewDataSet
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TdtmQueryWizard.GetTreeViewDataSet: TClientDataSet;
var
  aDataModule : IEDUQueryWizardDataModule;
begin
  if ( ( Assigned( ListViewDataModule ) ) and
       ( Supports( ListViewDataModule, IEDUQueryWizardDataModule, aDataModule ) ) ) then
  begin
    Result := aDataModule.TreeViewDataSet;
  end
  else
  begin
    Result := cdsGroup;
  end;
end;

{*****************************************************************************
  Class function to Open the Wizard modal.

  @Name       TdtmQueryWizard.OpenWizard
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class function TdtmQueryWizard.OpenWizard( aDataSet: TDataSet ): TModalResult;
var
  aQryWizardDataModule : IEDUQueryWizardDataModule;
  aDataModule          : IPPWFrameWorkDataModule;
  aClientDataSet       : TClientDataSet;
  aListView : IPPWFrameWorkListView;
begin
  Result := mrCancel;

  aClientDataSet := TClientDataSet.Create( Nil );

  try
    aDataModule := dtmEDUMainClient.pfcMain.CreateDataModule( Nil, 'TdtmQueryWizard', False );

    { If necessary we should do some initialisations in here }
    if ( Supports( aDataModule, IEDUQueryWizardDataModule, aQryWizardDataModule ) ) then
    begin
      aListView := dtmEDUMainClient.pfcMain.CreateListViewForDataModule( aQryWizardDataModule );
      Result := aListView.ShowModal;
    end;
  finally
    FreeAndNil( aClientDataSet );
    aListView := Nil;
    aQryWizardDataModule := Nil;
    aDataModule := Nil;
  end;
end;

procedure TdtmQueryWizard.prvListGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  Inherited;
  TableName := 'T_DBW_QUERY_WIZARD';
end;

{*****************************************************************************
  Procedure to fill in an Sql statement and the calculated parameter counter.

  @Name       TdtmQueryWizard.FillSqlStatement
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryWizard.FillSqlStatement( aSqlStatement : string; aParamCount : integer = 0 );
begin
  // Parameter sql en aantal opvullen
  fParamCount := aParamCount;
  fSqlStatement := aSqlStatement;
  freeAndNil(fParamcollection);
  fParamCollection := TObjectList.Create;
end;

{*****************************************************************************
  Procedure to fill in one parameters name and value.

  @Name       TdtmQueryWizard.AddParameter
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryWizard.AddParameter(aParamName: string;
  aParamValue: variant);
var
  aParamItem : TParamItem;
begin
  // Zorg voor een collectie om de gegevens tijdelijk in weg te steken
  aParamItem := TParamItem.Create;
  aParamItem.ParamName := aParamName;
  aParamItem.ParamValue := aParamValue;
  fParamCollection.Add(aParamItem);
end;

{*****************************************************************************
  Procedure to Show Report modal.

  @Name       TdtmQueryWizard.ShowReport
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryWizard.ShowReport;
begin
  // Toon rapport
  try

    TdtmReport.OpenReport(fSqlStatement, fParamCollection);
  finally
    // Verwijder daarna de tijdelijke gegevens alhier ...
    fParamCollection.Clear;
    freeAndNil(fParamCollection);
  end;
end;

{ TParamItem }

procedure TParamItem.SetParamName(const Value: string);
begin
  FParamName := Value;
end;

procedure TParamItem.SetParamValue(const Value: variant);
begin
  FParamValue := Value;
end;

end.
