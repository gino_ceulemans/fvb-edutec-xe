inherited dtmParameter: TdtmParameter
  KeyFields = 'F_PARAMETER_ID'
  ListViewClass = 'TfrmParameter_List'
  RecordViewClass = 'TfrmParameter_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    Connection = nil
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsListF_PARAMETER_ID: TIntegerField
      DisplayLabel = 'Parameter ( ID )'
      FieldName = 'F_PARAMETER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_QUERY_ID: TIntegerField
      DisplayLabel = 'Query  ( ID )'
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_QUERY_NAME: TStringField
      DisplayLabel = 'Query'
      FieldName = 'F_QUERY_NAME'
      ProviderFlags = []
      Size = 256
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Veldnaam'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsListF_CAPTION_NL: TStringField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_CAPTION_NL'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_CAPTION_FR: TStringField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_CAPTION_FR'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_CAPTION: TStringField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_CAPTION'
      ProviderFlags = []
      Size = 256
    end
    object cdsListF_PARAMETER_TYPE_ID: TIntegerField
      DisplayLabel = 'Type ( ID )'
      FieldName = 'F_PARAMETER_TYPE_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_PARAMETER_TYPE: TStringField
      DisplayLabel = 'Type'
      FieldName = 'F_PARAMETER_TYPE'
      ProviderFlags = []
      Size = 50
    end
    object cdsListF_TABLE: TStringField
      FieldName = 'F_TABLE'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_KEY: TStringField
      FieldName = 'F_KEY'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_RESULT_NL: TStringField
      FieldName = 'F_RESULT_NL'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_RESULT_FR: TStringField
      FieldName = 'F_RESULT_FR'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_RESULT: TStringField
      FieldName = 'F_RESULT'
      ProviderFlags = []
      Visible = False
      Size = 256
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsRecordF_PARAMETER_ID: TIntegerField
      DisplayLabel = 'Parameter ( ID )'
      FieldName = 'F_PARAMETER_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsRecordF_QUERY_ID: TIntegerField
      DisplayLabel = 'Query ( ID )'
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_QUERY_NAME: TStringField
      DisplayLabel = 'Query'
      FieldName = 'F_QUERY_NAME'
      ProviderFlags = []
      Required = True
      Size = 256
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'veldnaam'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 256
    end
    object cdsRecordF_CAPTION_NL: TStringField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_CAPTION_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 256
    end
    object cdsRecordF_CAPTION_FR: TStringField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_CAPTION_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 256
    end
    object cdsRecordF_CAPTION: TStringField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_CAPTION'
      ProviderFlags = []
      Visible = False
      Size = 256
    end
    object cdsRecordF_PARAMETER_TYPE_ID: TIntegerField
      DisplayLabel = 'Type ( ID )'
      FieldName = 'F_PARAMETER_TYPE_ID'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
    end
    object cdsRecordF_PARAMETER_TYPE: TStringField
      DisplayLabel = 'Type'
      FieldName = 'F_PARAMETER_TYPE'
      ProviderFlags = []
      Required = True
      Size = 50
    end
    object cdsRecordF_TABLE: TStringField
      DisplayLabel = 'Lookup tabel'
      FieldName = 'F_TABLE'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsRecordF_KEY: TStringField
      DisplayLabel = 'Lookup sleutel'
      FieldName = 'F_KEY'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsRecordF_RESULT_NL: TStringField
      DisplayLabel = 'Resultaat ( NL )'
      FieldName = 'F_RESULT_NL'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsRecordF_RESULT_FR: TStringField
      DisplayLabel = 'Resultaat ( FR )'
      FieldName = 'F_RESULT_FR'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsRecordF_RESULT: TStringField
      FieldName = 'F_RESULT'
      ProviderFlags = []
      Size = 256
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    Left = 240
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmParameter'
  end
end
