{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_Parameter
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Parameter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, DB, MConnect, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Provider, ADODB, unit_EdutecInterfaces;

type
  TdtmParameter = class(TEDUdatamodule, IEduParameterDatamodule)
    cdsListF_PARAMETER_ID: TIntegerField;
    cdsListF_QUERY_ID: TIntegerField;
    cdsListF_NAME: TStringField;
    cdsListF_CAPTION_NL: TStringField;
    cdsListF_CAPTION_FR: TStringField;
    cdsListF_CAPTION: TStringField;
    cdsListF_PARAMETER_TYPE_ID: TIntegerField;
    cdsListF_PARAMETER_TYPE: TStringField;
    cdsRecordF_PARAMETER_ID: TIntegerField;
    cdsRecordF_QUERY_ID: TIntegerField;
    cdsRecordF_NAME: TStringField;
    cdsRecordF_CAPTION_NL: TStringField;
    cdsRecordF_CAPTION_FR: TStringField;
    cdsRecordF_CAPTION: TStringField;
    cdsRecordF_PARAMETER_TYPE_ID: TIntegerField;
    cdsRecordF_PARAMETER_TYPE: TStringField;
    cdsListF_QUERY_NAME: TStringField;
    cdsRecordF_QUERY_NAME: TStringField;
    cdsRecordF_TABLE: TStringField;
    cdsRecordF_KEY: TStringField;
    cdsRecordF_RESULT_NL: TStringField;
    cdsRecordF_RESULT_FR: TStringField;
    cdsListF_TABLE: TStringField;
    cdsListF_KEY: TStringField;
    cdsListF_RESULT_NL: TStringField;
    cdsListF_RESULT_FR: TStringField;
    cdsListF_RESULT: TStringField;
    cdsRecordF_RESULT: TStringField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SelectQuery       ( aDataSet : TDataSet ); virtual;
    procedure ShowQuery         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure SelectType        ( aDataSet : TDataSet ); virtual;
    procedure ShowType          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure SelectTableField  ( aDataSet : TDataSet ); virtual;
    procedure SelectResultNL    ( aDataSet : TDataSet ); virtual;
    procedure SelectResultFR    ( aDataSet : TDataSet ); virtual;

  public
    { Public declarations }
    class procedure SelectParameter( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowParameter  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_Query, Data_ParameterType,
  Data_FVBFFCBaseDataModule, Unit_PPWFrameWorkDataModule, Data_Tablefield;

{*****************************************************************************
  This procedure will be used to copy Parameter related fields from one
  DataSet to another DataSet.

  @Name       CopyParameterFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyParameterFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_PARAMETER_ID' ).AsInteger :=
        aSource.FieldByName( 'F_PARAMETER_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_PARAMETER_NAME' ).AsString :=
          aSource.FieldByName( 'F_PARAMETER_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Parameter Records.

  @Name       TdtmParameter.SelectParameter
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmParameter.SelectParameter(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmParameter', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyParameterFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Parameter
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmParameter.ShowParameter
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmParameter.ShowParameter(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmParameter', 'F_PARAMETER_ID = ' + aDataSet.FieldByName( 'F_PARAMETER_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyParameterFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one Query Record.

  @Name       TdtmParameter.SelectQuery
  @author     cheuten
  @param      aDataSet   The dataset in which we should copy some information
                         from the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.SelectQuery(aDataSet: TDataSet);
begin
  TdtmQuery.SelectQuery ( aDataSet, '', False, critQuerytoParameter );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Query
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmParameter.ShowQuery
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.ShowQuery(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin

  TdtmQuery.ShowQuery ( aDataSet, aRecordViewMode );

end;

{*****************************************************************************
  This class procedure will be used to select one Type Record.

  @Name       TdtmParameter.SelectType
  @author     cheuten
  @param      aDataSet   The dataset in which we should copy some information
                         from the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.SelectType(aDataSet: TDataSet);
begin

  TdtmParameterType.SelectParameterType ( aDataSet, '', False, critParameterTypeToParameter );

end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Query
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmParameter.ShowType
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.ShowType(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin

  TdtmParameterType.ShowParameterType ( aDataSet, aRecordViewMode );

end;

{*****************************************************************************
  This method will be used when adding a new parameter to the recordset

  @Name       TdtmParameter.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_PARAMETER_ID' ).AsInteger :=
                        ssckData.AppServer.GetNewRecordID;
  // dtmEDUMainClient.GetNewRecordID( 'T_DBW_PARAMETER' );
end;

{*****************************************************************************
  This method will be used to select a value for the fields F_TABLE and F_KEY
  if F_PARAM_TYPE = LookUp else no action will be allowed

  @Name       TdtmParameter.SelectResultNL
  @author     cheuten
  @param      aDataSet
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.SelectTableField(aDataSet: TDataSet);
begin

  if ( aDataSet.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger = 10 ) then
  begin
    TdtmTableField.SelectTableField ( aDataSet, '', False, critDefault );
  end;

end;

{*****************************************************************************
  This method will be used to select a value for the field F_RESULT_FR after
  a check of F_TABLE already is filled with a value and F_PARAM_TYPE = LookUp

  @Name       TdtmParameter.SelectResultNL
  @author     cheuten
  @param      aDataSet
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.SelectResultFR(aDataSet: TDataSet);
var
  aWhere : string;
begin

  if ( not aDataSet.FieldByName('F_TABLE').IsNull ) And
     ( aDataSet.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger = 10 ) then
  begin
    aWhere := 'TableName = ' + chr(39) + aDataSet.FieldByName('F_TABLE').AsString + chr(39);
    TdtmTableField.SelectTableField ( aDataSet, aWhere, False, critResultFrToLookUp );
  end;

end;

{*****************************************************************************
  This method will be used to select a value for the field F_RESULT_NL  after
  a check of F_TABLE already is filled with a value and F_PARAM_TYPE = LookUp

  @Name       TdtmParameter.SelectResultNL
  @author     cheuten
  @param      aDataSet
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.SelectResultNL(aDataSet: TDataSet);
var
  aWhere : string;
begin

  if ( not aDataSet.FieldByName('F_TABLE').IsNull ) And
     ( aDataSet.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger = 10 ) then
  begin
    aWhere := 'TableName = ' + chr(39) + aDataSet.FieldByName('F_TABLE').AsString + chr(39);
    TdtmTableField.SelectTableField ( aDataSet, aWhere, False, critResultNlToLookUp );
  end;

end;

{*****************************************************************************
  This method will be used to fill in certain fields with defaults.

  @Name       TdtmParameter.FVBFFCDataModuleInitialiseAdditionalFields
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmParameter.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin

  inherited;
  // Standaard waarde zodat we hierop ALTIJD kunnen controleren.
  aDataSet.FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger := 1;
  aDataSet.FieldByName( 'F_PARAMETER_TYPE' ).AsString := 'String';

end;

end.
