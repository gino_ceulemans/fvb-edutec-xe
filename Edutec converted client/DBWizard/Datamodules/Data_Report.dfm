inherited dtmReport: TdtmReport
  ListViewClass = 'TfrmReport_List'
  RecordViewClass = 'TfrmReport_Record'
  Registered = True
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    Options = [poAllowCommandText]
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
    AutoOpen = False
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmReport'
  end
end
