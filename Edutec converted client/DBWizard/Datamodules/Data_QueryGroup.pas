{*****************************************************************************
  This DataModule will be used for the maintenance of T_DBW_QUERY_GROUP.

  @Name       Data_QueryGroup
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_QueryGroup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, DB, MConnect, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Provider, ADODB, unit_EdutecInterfaces;

type
  TdtmQueryGroup = class(TEDUdatamodule, IEDUQuerygroupDataModule)
    qryListF_QUERY_GROUP_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_PARENT_ID: TIntegerField;
    qryListF_PARENT_NAME: TStringField;
    qryRecordF_QUERY_GROUP_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_PARENT_ID: TIntegerField;
    qryRecordF_PARENT_NAME: TStringField;
    cdsListF_QUERY_GROUP_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_PARENT_ID: TIntegerField;
    cdsListF_PARENT_NAME: TStringField;
    cdsRecordF_QUERY_GROUP_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_PARENT_ID: TIntegerField;
    cdsRecordF_PARENT_NAME: TStringField;
    qryListF_NAME: TStringField;
    qryRecordF_NAME: TStringField;
    cdsListF_NAME: TStringField;
    cdsRecordF_NAME: TStringField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SelectParent ( aDataSet : TDataSet ); virtual;
    procedure ShowParent   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

  public
    { Public declarations }
    class procedure SelectQueryGroup( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowQueryGroup  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    class procedure ShowQueryParent ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;

  end;

  procedure CopyQueryGroupFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient;

{*****************************************************************************
  This procedure will be used to copy QueryGroup related fields from one
  DataSet to another DataSet.

  @Name       CopyQueryGroupFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyQueryGroupFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) and not ( aCopyTo = critParentToQueryGroup ) then
    begin
      aDestination.FieldByName( 'F_QUERY_GROUP_ID' ).AsInteger :=
        aSource.FieldByName( 'F_QUERY_GROUP_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_NAME_NL' ).AsString :=
          aSource.FieldByName( 'F_NAME_NL' ).AsString;

        aDestination.FieldByName( 'F_NAME_FR' ).AsString :=
          aSource.FieldByName( 'F_NAME_FR' ).AsString;

      end;
      critQueryGrouptoQuery :
        if aSource.Active then
        begin
          aDestination.FieldByName( 'F_QUERY_GROUP_NAME' ).AsString :=
            aSource.FieldByName( 'F_NAME_NL' ).AsString;
        end;
      critParentToQueryGroup :
      begin
        if aSource.Active then
        begin
          aDestination.FieldByName( 'F_PARENT_ID' ).AsInteger :=
            aSource.FieldByName( 'F_QUERY_GROUP_ID' ).AsInteger;

          aDestination.FieldByName( 'F_PARENT_NAME' ).AsString :=
            aSource.FieldByName( 'F_NAME_NL' ).AsString;
        end
        else
        begin
          aDestination.FieldByName( 'F_PARENT_ID' ).AsInteger := Null;
          aDestination.FieldByName( 'F_PARENT_NAME' ).AsString := '';

        end;

      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more QueryGroup Records.

  @Name       TdtmQueryGroup.SelectQueryGroup
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmQueryGroup.SelectQueryGroup( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmQueryGroup', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyQueryGroupFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single QueryGroup
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmQueryGroup.ShowQueryGroup
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmQueryGroup.ShowQueryGroup  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmQueryGroup', 'F_QUERY_GROUP_ID = ' + aDataSet.FieldByName( 'F_QUERY_GROUP_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyQueryGroupFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single QueryGroup
  Record based on the PK Field values ( "Parent" ) found in the given aDataSet.

  @Name       TdtmQueryGroup.ShowQueryParent
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmQueryGroup.ShowQueryParent ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault );
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmQueryGroup', 'F_QUERY_GROUP_ID = ' + aDataSet.FieldByName( 'F_PARENT_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyQueryGroupFieldValues( aSelectedRecords, aDataSet, False, aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Query Group
  Record based on the PK Field values ( "Parent" ) found in the given aDataSet.

  @Name       TdtmQueryGroup.ShowParent
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryGroup.ShowParent(aDataSet: TDataSet; aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  if not aDataSet.FieldByName('F_PARENT_ID').IsNull then
    TdtmQueryGroup.ShowQueryParent( aDataSet, aRecordViewMode, critParentToQueryGroup );
end;

{*****************************************************************************
  This class procedure will be used to select one Query group Record.

  @Name       TdtmQueryGroup.SelectParent
  @author     cheuten
  @param      aDataSet   The dataset in which we should copy some information
                         from the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryGroup.SelectParent(aDataSet: TDataSet);
var
   tWhere : string;
begin
  tWhere := '';
  // Controle of er wel een parent reeds geselecteerd is geworden
  if not aDataSet.FieldByName('F_PARENT_ID').IsNull then
    tWhere := 'F_QUERY_GROUP_ID <> ' + aDataset.FieldByName('F_PARENT_ID').AsString + ' AND ';
  // Zorg er ook voor dat er geen selectie mogelijk is van zichzelf ( circulaire verwijzing )
  tWhere := tWhere + 'F_QUERY_GROUP_ID <> ' + aDataset.FieldByName('F_QUERY_GROUP_ID').AsString;
  TdtmQueryGroup.SelectQueryGroup( aDataSet, tWhere, True, critParentToQueryGroup );
end;

{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmQueryGroup.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     cheuten
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQueryGroup.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  aDataSet.FieldByName( 'F_QUERY_GROUP_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;

end;

end.
