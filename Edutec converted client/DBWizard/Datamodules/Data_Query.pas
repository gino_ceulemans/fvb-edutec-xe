{*****************************************************************************
  This DataModule will be used for the maintenance of <TABLENAME>.

  @Name       Data_Query
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_Query;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, DB, MConnect, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Provider, ADODB, unit_EdutecInterfaces;

type
  TdtmQuery = class(TEDUdatamodule, IEDUQueryDataModule)
    qryListF_QUERY_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_QUERY_GROUP_ID: TIntegerField;
    qryListF_TEXT: TMemoField;
    qryListF_DESCRIPTION_NL: TMemoField;
    qryListF_DESCRIPTION_FR: TMemoField;
    qryListF_DESCRIPTION: TMemoField;
    qryListF_QUERY_GROUP_NAME: TStringField;
    qryRecordF_QUERY_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_QUERY_GROUP_ID: TIntegerField;
    qryRecordF_TEXT: TMemoField;
    qryRecordF_DESCRIPTION_NL: TMemoField;
    qryRecordF_DESCRIPTION_FR: TMemoField;
    qryRecordF_DESCRIPTION: TMemoField;
    qryRecordF_QUERY_GROUP_NAME: TStringField;
    cdsListF_QUERY_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_QUERY_GROUP_ID: TIntegerField;
    cdsListF_TEXT: TMemoField;
    cdsListF_DESCRIPTION_NL: TMemoField;
    cdsListF_DESCRIPTION_FR: TMemoField;
    cdsListF_DESCRIPTION: TMemoField;
    cdsListF_QUERY_GROUP_NAME: TStringField;
    cdsRecordF_QUERY_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_QUERY_GROUP_ID: TIntegerField;
    cdsRecordF_TEXT: TMemoField;
    cdsRecordF_DESCRIPTION_NL: TMemoField;
    cdsRecordF_DESCRIPTION_FR: TMemoField;
    cdsRecordF_DESCRIPTION: TMemoField;
    cdsRecordF_QUERY_GROUP_NAME: TStringField;
    cdsListF_NAME: TStringField;
    cdsRecordF_NAME: TStringField;
    qryListF_NAME: TStringField;
    qryRecordF_NAME: TStringField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);

  private

    { Private declarations }

  protected

    { Protected declarations }

    procedure SelectQuerygroup       ( aDataSet : TDataSet ); virtual;
    procedure ShowQuerygroup         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;

  public

    { Public declarations }

    class procedure SelectQuery ( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    class procedure ShowQuery   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;


  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_EduMainClient, Data_QueryGroup;

{*****************************************************************************
  This procedure will be used to copy Query related fields from one
  DataSet to another DataSet.

  @Name       CopyQueryFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyQueryFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;

    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_QUERY_ID' ).AsInteger :=
        aSource.FieldByName( 'F_QUERY_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      { Possibly we might need to copy different fields depending on the
        Destination }
      { By Default copy these Fields }
      critDefault :
      begin
        aDestination.FieldByName( 'F_QUERY_NAME' ).AsString :=
          aSource.FieldByName( 'F_QUERY_NAME' ).AsString;
      end;
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Query Records.

  @Name       TdtmQuery.SelectQuery
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmQuery.SelectQuery(aDataSet: TDataSet; aWhere: String;
  aMultiSelect: Boolean; aCopyTo : TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmQuery', aWhere , aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyQueryFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;


{*****************************************************************************
  This method will be used to show the RecordView for a single Query
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmQuery.ShowQuery
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmQuery.ShowQuery(aDataSet: TDataSet; aRecordViewMode: TPPWFrameWorkRecordViewMode; aCopyTo : TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmQuery', 'F_QUERY_ID = ' + aDataSet.FieldByName( 'F_QUERY_ID' ).AsString , aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyQueryFieldValues( aSelectedRecords, aDataSet, ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;


{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmQuery.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     cheuten
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQuery.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  aDataSet.FieldByName( 'F_QUERY_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;

end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmQuery.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     cheuten
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQuery.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  // Indien "master"module aanwezig dan ff controleren of deze van het juiste type is ...
  if ( Assigned( MasterDataModule ) ) then
  begin
    // Controle van het type gebeurt hier door functionaliteit van Support aan te roepen ...
    if ( Supports( MasterDataModule, IEDUQueryGroupDataModule ) ) then
    begin
      // Indien er dus een geldige mastermodule is dan even de gegevens overnemen van de masterdataset ...
      CopyQueryGroupFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critQuerygrouptoQuery );
    end;
  end;

end;

{*****************************************************************************
  This class procedure will be used to select one Query group Record.

  @Name       TdtmQuery.SelectQuerygroup
  @author     cheuten
  @param      aDataSet   The dataset in which we should copy some information
                         from the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQuery.SelectQuerygroup(aDataSet: TDataSet);
begin
  TdtmQueryGroup.SelectQueryGroup( aDataSet, '', False, critQueryGrouptoQuery );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Query Group
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmQuery.ShowQuerygroup
  @author     cheuten
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmQuery.ShowQuerygroup(aDataSet: TDataSet; aRecordViewMode: TPPWFrameWorkRecordViewMode);
begin
  TdtmQueryGroup.ShowQuerygroup ( aDataSet, aRecordViewMode );
end;

end.
