inherited dtmQueryGroup: TdtmQueryGroup
  KeyFields = 'F_QUERY_GROUP_ID'
  ListViewClass = 'TfrmQueryGroup_List'
  RecordViewClass = 'TfrmQueryGroup_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  DetailDataModules = <
    item
      Caption = 'Opvragingen'
      ForeignKeys = 'F_QUERY_GROUP_ID'
      DataModuleClass = 'TdtmQuery'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_QUERY_GROUP_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_PARENT_ID,'
      ' F_PARENT_NAME'
      'FROM'
      ' V_DBW_QUERYGROUP')
    object qryListF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      ReadOnly = True
      Required = True
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 256
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 256
    end
    object qryListF_PARENT_ID: TIntegerField
      FieldName = 'F_PARENT_ID'
    end
    object qryListF_PARENT_NAME: TStringField
      FieldName = 'F_PARENT_NAME'
      Size = 256
    end
    object qryListF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 256
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ' F_QUERY_GROUP_ID,'
      ' F_NAME_NL,'
      ' F_NAME_FR,'
      ' F_NAME,'
      ' F_PARENT_ID,'
      ' F_PARENT_NAME'
      'FROM'
      ' V_DBW_QUERYGROUP')
    object qryRecordF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
      Required = True
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 256
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 256
    end
    object qryRecordF_PARENT_ID: TIntegerField
      FieldName = 'F_PARENT_ID'
    end
    object qryRecordF_PARENT_NAME: TStringField
      FieldName = 'F_PARENT_NAME'
      Size = 256
    end
    object qryRecordF_NAME: TStringField
      FieldName = 'F_NAME'
      ReadOnly = True
      Size = 256
    end
  end
  inherited cdsList: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsListF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInKey]
      Required = True
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Groep ( NL )'
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 256
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Groep ( FR )'
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Visible = False
      Size = 256
    end
    object cdsListF_PARENT_ID: TIntegerField
      DisplayLabel = 'Parent ( ID )'
      FieldName = 'F_PARENT_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_PARENT_NAME: TStringField
      DisplayLabel = 'Parent'
      FieldName = 'F_PARENT_NAME'
      ProviderFlags = []
      Size = 256
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Size = 256
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsRecordF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInKey]
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Groep ( NL )'
      FieldName = 'F_NAME_NL'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 256
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Groep ( FR )'
      FieldName = 'F_NAME_FR'
      ProviderFlags = [pfInUpdate]
      Required = True
      Size = 256
    end
    object cdsRecordF_PARENT_ID: TIntegerField
      DisplayLabel = 'Parent ( ID )'
      FieldName = 'F_PARENT_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsRecordF_PARENT_NAME: TStringField
      DisplayLabel = 'Parent'
      FieldName = 'F_PARENT_NAME'
      ProviderFlags = []
      Size = 256
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      Visible = False
      Size = 256
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmQueryGroup'
  end
end
