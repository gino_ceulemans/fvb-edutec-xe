inherited dtmQueryWizard: TdtmQueryWizard
  KeyFields = 'F_QUERY_ID'
  ListViewClass = 'TfrmQueryWizard_List'
  RecordViewClass = 'TfrmQueryWizard_Record'
  Registered = True
  Height = 265
  Width = 538
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      ''
      ' F_QUERY_GROUP_ID,'
      ' F_QUERY_GROUP_NAME,'
      ''
      ' F_QUERY_ID,'
      ' F_NAME,'
      ' F_DESCRIPTION,'
      ' F_TEXT,'
      ' F_PARAMETER_COUNT'
      ' '
      'FROM'
      ' V_DBW_QUERY')
    object qryListF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
    end
    object qryListF_NAME: TStringField
      FieldName = 'F_NAME'
      Size = 256
    end
    object qryListF_PARENT_ID: TIntegerField
      FieldName = 'F_PARENT_ID'
    end
    object qryListF_PARENT_NAME: TStringField
      FieldName = 'F_PARENT_NAME'
      Size = 256
    end
    object qryListF_QUERY_COUNT: TIntegerField
      FieldName = 'F_QUERY_COUNT'
    end
  end
  inherited qryRecord: TFVBFFCQuery
    SQL.Strings = (
      'SELECT '
      ' F_QUERY_GROUP_ID,'
      ''
      ' F_QUERY_ID,'
      ' F_QUERY_NAME,'
      ''
      ' F_PARAMETER_ID,'
      ' F_NAME,'
      ' F_CAPTION,'
      ' F_PARAMETER_TYPE_ID,'
      ' F_PARAMETER_TYPE,'
      ' F_TABLE,'
      ' F_KEY,'
      ' F_RESULT'
      ''
      'FROM'
      ' V_DBW_PARAMETER')
  end
  inherited cdsList: TFVBFFCClientDataSet
    IndexFieldNames = 'F_QUERY_GROUP_ID'
    MasterFields = 'F_QUERY_GROUP_ID'
    MasterSource = srcGroup
    RemoteServer = ssckData
    object cdsListF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
    object cdsListF_QUERY_GROUP_NAME: TStringField
      DisplayLabel = 'Groep'
      FieldName = 'F_QUERY_GROUP_NAME'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 256
    end
    object cdsListF_QUERY_ID: TIntegerField
      DisplayLabel = 'Query ( ID )'
      FieldName = 'F_QUERY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsListF_DESCRIPTION: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_DESCRIPTION'
      ProviderFlags = [pfInUpdate]
      BlobType = ftMemo
    end
    object cdsListF_TEXT: TMemoField
      DisplayLabel = 'Sql statement'
      FieldName = 'F_TEXT'
      ProviderFlags = [pfInUpdate]
      Visible = False
      BlobType = ftMemo
    end
    object cdsListF_PARAMETER_COUNT: TIntegerField
      DisplayLabel = 'Aantal parameters'
      FieldName = 'F_PARAMETER_COUNT'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    RemoteServer = ssckData
    object cdsRecordF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
    end
    object cdsRecordF_QUERY_GROUP_NAME: TStringField
      DisplayLabel = 'Groep'
      FieldName = 'F_QUERY_GROUP_NAME'
      Size = 256
    end
    object cdsRecordF_QUERY_ID: TIntegerField
      DisplayLabel = 'Query ( ID )'
      FieldName = 'F_QUERY_ID'
    end
    object cdsRecordF_NAME: TStringField
      DisplayLabel = 'Query'
      FieldName = 'F_NAME'
      Size = 256
    end
    object cdsRecordF_DESCRIPTION: TMemoField
      DisplayLabel = 'Omschrijving'
      FieldName = 'F_DESCRIPTION'
      BlobType = ftMemo
    end
    object cdsRecordF_TEXT: TMemoField
      DisplayLabel = 'Sql statement'
      FieldName = 'F_TEXT'
      ReadOnly = True
      BlobType = ftMemo
    end
    object cdsRecordF_PARAMETER_COUNT: TIntegerField
      DisplayLabel = 'Aantal parameters'
      FieldName = 'F_PARAMETER_COUNT'
      ReadOnly = True
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    Left = 256
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmQueryWizard'
  end
  object cdsGroup: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <>
    ProviderName = 'prvGroup'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    Left = 256
    Top = 104
    object cdsGroupF_QUERY_GROUP_ID: TIntegerField
      DisplayLabel = 'Groep ( ID )'
      FieldName = 'F_QUERY_GROUP_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsGroupF_NAME: TStringField
      DisplayLabel = 'Groep'
      FieldName = 'F_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsGroupF_PARENT_ID: TIntegerField
      DisplayLabel = 'Parent ( ID )'
      FieldName = 'F_PARENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsGroupF_PARENT_NAME: TStringField
      DisplayLabel = 'Parent'
      FieldName = 'F_PARENT_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 256
    end
    object cdsGroupF_QUERY_COUNT: TIntegerField
      DisplayLabel = 'Aantal queries'
      FieldName = 'F_QUERY_COUNT'
      ProviderFlags = [pfInUpdate]
      Visible = False
    end
  end
  object srcGroup: TFVBFFCDataSource
    DataSet = cdsGroup
    Left = 256
    Top = 56
  end
  object cdsParameter: TFVBFFCClientDataSet
    Aggregates = <>
    IndexFieldNames = 'F_QUERY_ID;F_PARAMETER_ID'
    MasterFields = 'F_QUERY_ID'
    MasterSource = srcQuery
    PacketRecords = 25
    Params = <>
    ProviderName = 'prvParameter'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    Left = 352
    Top = 104
    object cdsParameterF_QUERY_ID: TIntegerField
      FieldName = 'F_QUERY_ID'
    end
    object cdsParameterF_QUERY_NAME: TStringField
      FieldName = 'F_QUERY_NAME'
      Size = 256
    end
    object cdsParameterF_PARAMETER_ID: TIntegerField
      FieldName = 'F_PARAMETER_ID'
    end
    object cdsParameterF_NAME: TStringField
      FieldName = 'F_NAME'
      Size = 256
    end
    object cdsParameterF_CAPTION: TStringField
      FieldName = 'F_CAPTION'
      ReadOnly = True
      Size = 256
    end
    object cdsParameterF_PARAMETER_TYPE_ID: TIntegerField
      FieldName = 'F_PARAMETER_TYPE_ID'
    end
    object cdsParameterF_PARAMETER_TYPE: TStringField
      FieldName = 'F_PARAMETER_TYPE'
      Size = 50
    end
    object cdsParameterF_TABLE: TStringField
      FieldName = 'F_TABLE'
      Size = 256
    end
    object cdsParameterF_KEY: TStringField
      FieldName = 'F_KEY'
      Size = 256
    end
    object cdsParameterF_RESULT: TStringField
      FieldName = 'F_RESULT'
      ReadOnly = True
      Size = 256
    end
    object cdsParameterF_QUERY_GROUP_ID: TIntegerField
      FieldName = 'F_QUERY_GROUP_ID'
    end
  end
  object srcQuery: TFVBFFCDataSource
    DataSet = cdsList
    Left = 352
    Top = 56
  end
  object cdsSelectLookUp: TFVBFFCClientDataSet
    Aggregates = <>
    PacketRecords = 25
    Params = <>
    ProviderName = 'prvSelectLookUp'
    RemoteServer = ssckData
    BeforePost = cdsListBeforePost
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 352
    Top = 160
  end
end
