inherited frmParameterType_Record: TfrmParameterType_Record
  Width = 940
  ActiveControl = cxdbteF_NAME_NL1
  Caption = 'Parameter Type'
  Constraints.MinWidth = 760
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 932
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 596
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 932
    inherited pnlRecord: TFVBFFCPanel
      Width = 673
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 673
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 673
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 673
        object cxlblF_PARAM_TYPE_ID: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmParameterType_Record.F_PARAMETER_TYPE_ID'
          Caption = 'Type ( ID )'
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxlblFVBFFCLabel1: TFVBFFCLabel
          Left = 240
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmParameterType_Record.F_NAME'
          Caption = 'Naam'
          FocusControl = cxdbteF_NAME_NL1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 240
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmParameterType_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 240
        end
        object cxdblblF_PARAMETER_TYPE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmParameterType_Record.F_PARAMETER_TYPE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PARAMETER_TYPE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 200
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 673
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 673
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 673
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 673
        end
        inherited sbxMain: TScrollBox
          Width = 673
          object cxlblF_PARAMETER_TYPE_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmParameterType_Record.F_PARAMETER_TYPE_ID'
            Caption = 'Type ( ID )'
            FocusControl = cxdbseF_PARAMETER_TYPE_ID1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_PARAMETER_TYPE_ID1: TFVBFFCDBSpinEdit
            Left = 96
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmParameterType_Record.F_PARAMETER_TYPE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_PARAMETER_TYPE_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmParameterType_Record.F_NAME_NL'
            Caption = 'Naam ( NL )'
            FocusControl = cxdbteF_NAME_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL1: TFVBFFCDBTextEdit
            Left = 96
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmParameterType_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 345
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmParameterType_Record.F_NAME_FR'
            Caption = 'Naam ( FR )'
            FocusControl = cxdbteF_NAME_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR1: TFVBFFCDBTextEdit
            Left = 96
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmParameterType_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 345
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 928
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmParameterType.cdsRecord
  end
end
