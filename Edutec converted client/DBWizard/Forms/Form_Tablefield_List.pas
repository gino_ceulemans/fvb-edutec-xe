{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Tablefield_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Tablefield_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxTextEdit, cxDBEdit, cxContainer, cxLabel;

type
  TfrmTablefield_List = class(TEduListview)
    cxgrdtblvListTableName: TcxGridDBColumn;
    cxgrdtblvListFieldName: TcxGridDBColumn;
    cxlblF_RESULT_NL: TFVBFFCLabel;
    cxdbteTableName: TFVBFFCDBTextEdit;
    cxdbteFieldName: TFVBFFCDBTextEdit;
    cxlblF_RESULT_FR: TFVBFFCLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Tablefield;

{ TfrmTablefield_List }

function TfrmTablefield_List.GetFilterString: String;
var
  aFilterString : String;
begin
  { Add the condition for the TableName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'TableName' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'TableName', FSearchCriteriaDataSet.FieldByName( 'TableName' ).AsString );
  end;

  { Add the condition for the FieldName }
  if not ( FSearchCriteriaDataSet.FieldByName( 'FieldName' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'FieldName', FSearchCriteriaDataSet.FieldByName( 'FieldName' ).AsString );
  end;

  Result := aFilterString;
end;

end.
