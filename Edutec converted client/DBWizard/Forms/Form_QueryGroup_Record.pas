{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_QueryGroup_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_QueryGroup_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, Menus, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, cxControls,
  cxSplitter, Unit_FVBFFCDevExpress, dxNavBarCollns, dxNavBarBase,
  dxNavBar, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel, StdCtrls,
  Buttons, cxDBLabel, cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit,
  cxContainer, cxEdit, cxLabel, cxButtonEdit;

type
  TfrmQueryGroup_Record = class(TeduRecordview)
    cxdblblF_QUERY_GROUP_ID: TFVBFFCDBLabel;
    cxlblF_QUERY_GROUP_ID2: TFVBFFCLabel;
    dxnbiShowQuery: TdxNavBarItem;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxlblF_QUERY_GROUP_ID1: TFVBFFCLabel;
    cxdbseF_QUERY_GROUP_ID1: TFVBFFCDBSpinEdit;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
    cxdbbeF_PARENT_NAME: TFVBFFCDBButtonEdit;
    cxlblF_PARENT_NAME1: TFVBFFCLabel;
    acShowQueries: TAction;
    procedure acShowQueriesExecute(Sender: TObject);
    procedure cxdbbeF_PARENT_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_QueryGroup, Data_EduMainClient, Unit_PPWFrameWorkRecordView, unit_EdutecInterfaces;

{ TfrmQueryGroup_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmQueryGroup_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmQueryGroup_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.caption; // Inherited GetDetailName;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Parent ButtonEdit.  It will be used to allow the User to Select or
  View an other QueryGroup.

  @Name       TfrmQueryGroup_Record.cxdbbeF_PARENT_NAMEPropertiesButtonClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryGroup_Record.acShowQueriesExecute(Sender: TObject);
begin
  inherited;

  ShowDetailListView( Self, 'TdtmQuery', pnlRecordDetail );

end;

procedure TfrmQueryGroup_Record.cxdbbeF_PARENT_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUQueryGroupDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUQueryGroupDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowParent( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectParent( srcMain.DataSet );
    end;
  end;

end;

end.