inherited frmQueryGroup_List: TfrmQueryGroup_List
  Caption = 'Query groepen'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlListLeftSpacer: TFVBFFCPanel [0]
  end
  inherited pnlSelection: TPanel [1]
  end
  inherited pnlList: TFVBFFCPanel [2]
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_QUERY_GROUP_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_GROUP_ID'
          Width = 100
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 400
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
        end
        object cxgrdtblvListF_PARENT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PARENT_ID'
          Visible = False
        end
        object cxgrdtblvListF_PARENT_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PARENT_NAME'
          Width = 400
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel [3]
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel [4]
  end
  inherited pnlListRightSpacer: TFVBFFCPanel [5]
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
