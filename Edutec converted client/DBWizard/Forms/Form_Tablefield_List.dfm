inherited frmTablefield_List: TfrmTablefield_List
  Caption = 'Tabel & veld'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListTableName: TcxGridDBColumn
          DataBinding.FieldName = 'TableName'
          Width = 400
        end
        object cxgrdtblvListFieldName: TcxGridDBColumn
          DataBinding.FieldName = 'FieldName'
          Width = 400
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      object cxlblF_RESULT_NL: TFVBFFCLabel
        Left = 16
        Top = 36
        HelpType = htKeyword
        Caption = 'Tabelnaam'
        FocusControl = cxdbteTableName
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteTableName: TFVBFFCDBTextEdit
        Left = 144
        Top = 36
        HelpType = htKeyword
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'TableName'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 2
        Width = 361
      end
      object cxdbteFieldName: TFVBFFCDBTextEdit
        Left = 144
        Top = 60
        HelpType = htKeyword
        Anchors = [akLeft, akTop, akRight]
        DataBinding.DataField = 'FieldName'
        DataBinding.DataSource = srcSearchCriteria
        TabOrder = 3
        Width = 361
      end
      object cxlblF_RESULT_FR: TFVBFFCLabel
        Left = 16
        Top = 60
        HelpType = htKeyword
        Caption = 'Veldnaam'
        FocusControl = cxdbteFieldName
        ParentColor = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmTablefield.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmTablefield.cdsSearchCriteria
  end
end
