{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_ParameterType_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  19/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_ParameterType_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, Menus, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, cxControls,
  cxSplitter, Unit_FVBFFCDevExpress, dxNavBarCollns, dxNavBarBase,
  dxNavBar, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel, StdCtrls,
  Buttons, cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit, cxContainer,
  cxEdit, cxLabel, cxDBLabel;

type
  TfrmParameterType_Record = class(TeduRecordview)
    cxlblF_PARAMETER_TYPE_ID1: TFVBFFCLabel;
    cxdbseF_PARAMETER_TYPE_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
    cxlblF_PARAM_TYPE_ID: TFVBFFCLabel;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    cxdblblF_PARAMETER_TYPE_ID: TFVBFFCDBLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_ParameterType, Data_EduMainClient;

{ TfrmParameterType_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmParameterType_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmParameterType_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption;
end;

end.