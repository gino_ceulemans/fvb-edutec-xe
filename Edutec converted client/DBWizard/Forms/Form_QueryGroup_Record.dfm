inherited frmQueryGroup_Record: TfrmQueryGroup_Record
  Left = 285
  Width = 939
  ActiveControl = cxdbteF_NAME_NL1
  Caption = 'Query groep'
  Constraints.MinWidth = 780
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 931
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 595
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 931
    inherited pnlRecord: TFVBFFCPanel
      Width = 672
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 672
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 672
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 672
        object cxdblblF_QUERY_GROUP_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCenInfrastructure_Record.F_INFRASTRUCTURE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_QUERY_GROUP_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 193
        end
        object cxlblF_QUERY_GROUP_ID2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmQueryGroup_Record.F_QUERY_GROUP_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Groep ( ID )'
          FocusControl = cxdblblF_QUERY_GROUP_ID
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxlblFVBFFCLabel1: TFVBFFCLabel
          Left = 200
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmQueryGroup_Record.F_NAME'
          Caption = 'Naam'
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 200
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmQueryGroup_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 672
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 672
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 672
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 672
        end
        inherited sbxMain: TScrollBox
          Width = 672
          object cxlblF_QUERY_GROUP_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_QUERY_GROUP_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Groep ( ID )'
            FocusControl = cxdbseF_QUERY_GROUP_ID1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_QUERY_GROUP_ID1: TFVBFFCDBSpinEdit
            Left = 120
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_QUERY_GROUP_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_QUERY_GROUP_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 120
          end
          object cxdbteF_NAME_NL1: TFVBFFCDBTextEdit
            Left = 120
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 2
            Width = 417
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 40
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_NAME_NL'
            Caption = 'Groep ( NL )'
            FocusControl = cxdbteF_NAME_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 72
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_NAME_FR'
            Caption = 'Groep ( FR )'
            FocusControl = cxdbteF_NAME_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR1: TFVBFFCDBTextEdit
            Left = 120
            Top = 72
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 417
          end
          object cxdbbeF_PARENT_NAME: TFVBFFCDBButtonEdit
            Left = 120
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_PARENT_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PARENT_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PARENT_NAMEPropertiesButtonClick
            TabOrder = 6
            Width = 417
          end
          object cxlblF_PARENT_NAME1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmQueryGroup_Record.F_PARENT_NAME'
            Caption = 'Parent'
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 927
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiShowQuery
          end>
      end
      object dxnbiShowQuery: TdxNavBarItem
        Action = acShowQueries
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmQueryGroup.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowQueries: TAction
      Caption = 'Queries'
      OnExecute = acShowQueriesExecute
    end
  end
end
