inherited frmQueryWizard_List: TfrmQueryWizard_List
  Left = 97
  Top = 99
  Caption = 'Query Wizard'
  Position = poScreenCenter
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Visible = True
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 667
      Visible = False
      ExplicitLeft = 667
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 779
      OptionsImage.Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      OptionsImage.NumGlyphs = 2
      ExplicitLeft = 779
    end
    object cxbtnSelect: TFVBFFCButton
      Left = 667
      Top = 8
      Width = 96
      Height = 25
      Action = dtmEDUMainClient.acListViewEdit
      Anchors = [akTop, akRight]
      OptionsImage.Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C805C805C805C805C805C805C805C805C805C805C805C
        805C1F7C1F7C1F7CA0687A7F7A7F5A7F597F597F387F387F387F177FF57E927E
        4F7E805C1F7C1F7CA0689B7FE37CE27CE27CC17CC17CA07CA07CA07CA07CA07C
        927E805C1F7C1F7CA0689B7F037DE37CE27CE27CC17CC17CA07CA07CA07CA07C
        F57E805C1F7C1F7CA0689C7F047D887D887D887D887D887D887D887D887DA07C
        177F805C1F7C1F7CA068BC7F257DF67AFF7FFF7FFF7FFF7FFF7FFF7FF67AA07C
        387F805C1F7C1F7CA068BD7F467D257DF67AFF7FFF7FFF7FFF7FF67AC17CA07C
        387F805C1F7C1F7CA068BD7F677D677D467DF67AFF7FFF7FF67AE27CC17CC17C
        597F805C1F7C1F7CA068DE7FA97D887D677D467DF67AF67A037DE27CE27CC17C
        597F805C1F7C1F7CA068DE7FEB7DCB7DA97D677D467D247D047D037DE27CE27C
        597F805C1F7C1F7CA068DE7F0D7EEC7DCB7D887D677D257D247D047D037DE27C
        7A7F805C1F7C1F7CA068FF7F2E7E0D7EEC7DA97D677D467D257D247D047D037D
        7A7F805C1F7C1F7CA068FF7FFF7FFF7FDE7FDE7FDD7FBD7FBD7F9C7F9B7F9B7F
        7B7F805C1F7C1F7C1F7CA068A068A068A068A068A068A068A068A068A068A068
        A0681F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      OptionsImage.Margin = 10
      OptionsImage.Spacing = -1
      TabOrder = 2
    end
  end
  inherited pnlList: TFVBFFCPanel
    Left = 296
    Width = 411
    ExplicitLeft = 296
    ExplicitWidth = 411
    inherited cxgrdList: TcxGrid
      Width = 411
      ExplicitTop = 182
      ExplicitWidth = 411
      ExplicitHeight = 258
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.IncSearch = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Inactive = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        Styles.Selection = nil
        object cxgrdtblvListF_QUERY_GROUP_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_GROUP_ID'
          Visible = False
        end
        object cxgrdtblvListF_QUERY_GROUP_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_GROUP_NAME'
          Visible = False
        end
        object cxgrdtblvListF_QUERY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_ID'
          Width = 85
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 300
        end
        object cxgrdtblvListF_DESCRIPTION: TcxGridDBColumn
          DataBinding.FieldName = 'F_DESCRIPTION'
          Width = 200
        end
        object cxgrdtblvListF_TEXT: TcxGridDBColumn
          DataBinding.FieldName = 'F_TEXT'
          Visible = False
        end
        object cxgrdtblvListF_PARAMETER_COUNT: TcxGridDBColumn
          DataBinding.FieldName = 'F_PARAMETER_COUNT'
          Visible = False
          Width = 120
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Width = 411
      ExplicitWidth = 411
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Width = 411
      ExplicitWidth = 411
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 411
      Visible = False
      ExplicitWidth = 411
      inherited pnlSearchCriteriaButtons: TPanel
        Width = 411
        ExplicitWidth = 411
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 243
          ExplicitLeft = 243
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 331
          ExplicitLeft = 331
        end
      end
    end
  end
  object pnlFVBFFCPanel1: TFVBFFCPanel [6]
    Left = 4
    Top = 4
    Width = 292
    Height = 440
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 4
    ParentColor = True
    TabOrder = 6
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object cxDBTreeList1: TcxDBTreeList
      Left = 4
      Top = 4
      Width = 284
      Height = 432
      BorderStyle = cxcbsNone
      Align = alClient
      Bands = <
        item
        end>
      DataController.DataSource = srcTreeView
      DataController.ParentField = 'F_PARENT_ID'
      DataController.KeyField = 'F_QUERY_GROUP_ID'
      LookAndFeel.Kind = lfUltraFlat
      Navigator.Buttons.CustomButtons = <>
      RootValue = -1
      TabOrder = 0
      ExplicitLeft = 16
      ExplicitTop = 96
      ExplicitWidth = 250
      ExplicitHeight = 150
      object cxDBTreeList1F_QUERY_GROUP_ID: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'F_QUERY_GROUP_ID'
        Width = 92
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object cxDBTreeList1F_NAME: TcxDBTreeListColumn
        DataBinding.FieldName = 'F_NAME'
        Width = 240
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object cxDBTreeList1F_PARENT_ID: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'F_PARENT_ID'
        Width = 92
        Position.ColIndex = 2
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object cxDBTreeList1F_PARENT_NAME: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'F_PARENT_NAME'
        Width = 186
        Position.ColIndex = 3
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object cxDBTreeList1F_QUERY_COUNT: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'F_QUERY_COUNT'
        Position.ColIndex = 4
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    inherited dxbbEdit: TdxBarButton
      Caption = 'Select'
      Hint = 'Select|Select the current Record'
    end
    inherited dxbbView: TdxBarButton
      Visible = ivNever
    end
    inherited dxbbAdd: TdxBarButton
      Visible = ivNever
    end
    inherited dxbbDelete: TdxBarButton
      Visible = ivNever
    end
  end
  object srcTreeView: TFVBFFCDataSource
    DataSet = dtmQueryWizard.cdsGroup
    Left = 168
    Top = 392
  end
end
