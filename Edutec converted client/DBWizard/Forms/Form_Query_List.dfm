inherited frmQuery_List: TfrmQuery_List
  Caption = 'Queries'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_QUERY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_ID'
          Width = 83
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 240
        end
        object cxgrdtblvListF_QUERY_GROUP_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_GROUP_ID'
          Visible = False
          Width = 100
        end
        object cxgrdtblvListF_TEXT: TcxGridDBColumn
          DataBinding.FieldName = 'F_TEXT'
          Width = 400
        end
        object cxgrdtblvListF_QUERY_GROUP_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_GROUP_NAME'
          Width = 240
        end
        object cxgrdtblvListF_DESCRIPTION: TcxGridDBColumn
          DataBinding.FieldName = 'F_DESCRIPTION'
          Width = 240
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
        end
        object cxgrdtblvListF_DESCRIPTION_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_DESCRIPTION_NL'
          Visible = False
        end
        object cxgrdtblvListF_DESCRIPTION_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_DESCRIPTION_FR'
          Visible = False
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
