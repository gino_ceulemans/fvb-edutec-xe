{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_QueryWizard_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_QueryWizard_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxPC, cxControls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxMemo, cxDBEdit,
  cxSpinEdit, cxMaskEdit, cxButtonEdit, cxTextEdit, cxContainer, cxLabel,
  cxCurrencyEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, cxCalendar,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Menus, cxSplitter, Form_EDUListView,
  //dxExEdtr, dxTL, dxDBCtrl, dxCntner, dxDBTL,
  dxBar, cxLookAndFeels,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxSkinsdxBarPainter, dxSkinsdxRibbonPainter, cxTL,
  cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTLData, cxDBTL,
  PPW_Components_Common_CollapsablePanel;

type
  TfrmQueryWizard_List = class(TEDUListView)
    srcTreeView: TFVBFFCDataSource;
    pnlFVBFFCPanel1: TFVBFFCPanel;
    cxgrdtblvListF_QUERY_GROUP_ID: TcxGridDBColumn;
    cxgrdtblvListF_QUERY_GROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListF_QUERY_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_DESCRIPTION: TcxGridDBColumn;
    cxgrdtblvListF_TEXT: TcxGridDBColumn;
    cxgrdtblvListF_PARAMETER_COUNT: TcxGridDBColumn;
    cxbtnSelect: TFVBFFCButton;
    cxDBTreeList1: TcxDBTreeList;
    cxDBTreeList1F_QUERY_GROUP_ID: TcxDBTreeListColumn;
    cxDBTreeList1F_NAME: TcxDBTreeListColumn;
    cxDBTreeList1F_PARENT_ID: TcxDBTreeListColumn;
    cxDBTreeList1F_PARENT_NAME: TcxDBTreeListColumn;
    cxDBTreeList1F_QUERY_COUNT: TcxDBTreeListColumn;
    procedure FVBFFCListViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure FVBFFCListViewShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_QueryWizard, Data_EduMainClient, unit_EdutecInterfaces,
  Form_FVBFFCBaseListView;

{ TfrmQueryWizard_List }

{*****************************************************************************
  Procedure that links the treeview to the dataset which has to be used.

  @Name       TfrmQueryWizard_List.FVBFFCListViewInitialise
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_List.FVBFFCListViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  aDataModule : IEDUQueryWizardDataModule;
begin
  inherited;

  if ( Supports( aFrameWorkDataModule, IEDUQueryWizardDataModule, aDataModule ) ) then
  begin
    srcTreeView.DataSet := aDataModule.TreeViewDataSet;
  end;
end;

{*****************************************************************************
  Procedure that will be executed once when showing the form. Needed to
  override some of the default behaviors of the ancestor screen.

  @Name       TfrmQueryWizard_List.FVBFFCListViewShow
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;
  // Noodzakelijk om ervoor te zorgen dat de action de caption niet
  // telkens opnieuw overschrijft.
  cxbtnSelect.Caption := dxbbEdit.Caption;
  cxbtnSelect.Hint := dxbbEdit.Hint;

  dxbbView.Visible := ivNever;
  dxbbAdd.Visible := ivNever;
  dxbbDelete.Visible := ivNever;
  dxbbEdit.Caption := 'Select';

end;

end.
