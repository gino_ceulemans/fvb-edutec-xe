{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Query_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Query_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, Menus,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk,
  Unit_FVBFFCDevExpress, Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls;

type
  TfrmQuery_List = class(TEduListview)
    cxgrdtblvListF_QUERY_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME_NL: TcxGridDBColumn;
    cxgrdtblvListF_NAME_FR: TcxGridDBColumn;
    cxgrdtblvListF_QUERY_GROUP_ID: TcxGridDBColumn;
    cxgrdtblvListF_TEXT: TcxGridDBColumn;
    cxgrdtblvListF_DESCRIPTION_NL: TcxGridDBColumn;
    cxgrdtblvListF_DESCRIPTION_FR: TcxGridDBColumn;
    cxgrdtblvListF_DESCRIPTION: TcxGridDBColumn;
    cxgrdtblvListF_QUERY_GROUP_NAME: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Query;

end.
