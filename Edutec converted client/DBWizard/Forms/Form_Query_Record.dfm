inherited frmQuery_Record: TfrmQuery_Record
  Left = 33
  Top = 72
  Width = 927
  Height = 560
  ActiveControl = cxdbbeF_QUERY_GROUP_NAME
  Caption = 'Query'
  Constraints.MinHeight = 560
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 501
    Width = 919
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 583
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 919
    Height = 501
    inherited pnlRecord: TFVBFFCPanel
      Width = 757
      Height = 501
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 757
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 757
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 757
        object cxlblFVBFFCLabel1: TFVBFFCLabel
          Left = 200
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_NAME'
          Caption = 'Query'
          FocusControl = cxdbteF_NAME_NL1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxlblFVBFFCLabel2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
          Caption = 'Query ( ID )'
          FocusControl = cxdbseF_QUERY_ID1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_QUERY_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_QUERY_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 193
        end
        object cxdblblF_NAME: TFVBFFCDBLabel
          Left = 200
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 400
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 757
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 497
        Width = 757
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 757
        Height = 427
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 757
        end
        inherited sbxMain: TScrollBox
          Width = 757
          Height = 402
          object cxlblF_QUERY_ID1: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
            Caption = 'Query ( ID )'
            FocusControl = cxdbseF_QUERY_ID1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_QUERY_ID1: TFVBFFCDBSpinEdit
            Left = 136
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_QUERY_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 0
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_NAME_NL'
            Caption = 'Query ( NL )'
            FocusControl = cxdbteF_NAME_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL1: TFVBFFCDBTextEdit
            Left = 136
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 2
            Width = 480
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_NAME_FR'
            Caption = 'Query ( FR )'
            FocusControl = cxdbteF_NAME_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR1: TFVBFFCDBTextEdit
            Left = 136
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 3
            Width = 480
          end
          object cxlblF_TEXT1: TFVBFFCLabel
            Left = 16
            Top = 280
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_TEXT'
            Caption = 'Sql statement'
            FocusControl = cxdbmmoF_TEXT1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_TEXT1: TFVBFFCDBMemo
            Left = 136
            Top = 280
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_TEXT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_TEXT'
            DataBinding.DataSource = srcMain
            TabOrder = 6
            Height = 105
            Width = 480
          end
          object cxlblF_DESCRIPTION_NL1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_DESCRIPTION_NL'
            Caption = 'Omschrijving ( NL )'
            FocusControl = cxdbmmoF_DESCRIPTION_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_DESCRIPTION_NL1: TFVBFFCDBMemo
            Left = 136
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_DESCRIPTION_NL'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_DESCRIPTION_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 4
            Height = 80
            Width = 480
          end
          object cxlblF_DESCRIPTION_FR1: TFVBFFCLabel
            Left = 8
            Top = 192
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_DESCRIPTION_FR'
            Caption = 'Omschrijving ( FR )'
            FocusControl = cxdbmmoF_DESCRIPTION_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_DESCRIPTION_FR1: TFVBFFCDBMemo
            Left = 136
            Top = 192
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_DESCRIPTION_FR'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_DESCRIPTION_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Height = 80
            Width = 480
          end
          object cxlblF_QUERY_GROUP_NAME1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_QUERY_GROUP_NAME'
            Caption = 'Groep'
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_QUERY_GROUP_NAME: TFVBFFCDBButtonEdit
            Left = 136
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_QUERY_GROUP_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_QUERY_GROUP_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_QUERY_GROUP_NAMEPropertiesButtonClick
            TabOrder = 1
            Width = 480
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 915
      Height = 501
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 501
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiShowParameters
          end>
      end
      object dxnbiShowParameters: TdxNavBarItem
        Action = acShowParameters
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 501
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmQuery.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    object acShowParameters: TAction
      Caption = 'Parameters'
      OnExecute = acShowParametersExecute
    end
  end
end
