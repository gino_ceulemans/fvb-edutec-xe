inherited frmParameter_List: TfrmParameter_List
  Caption = 'Parameters'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_PARAMETER_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PARAMETER_ID'
          Width = 120
        end
        object cxgrdtblvListF_QUERY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_ID'
          Visible = False
        end
        object cxgrdtblvListF_QUERY_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_QUERY_NAME'
          Width = 200
        end
        object cxgrdtblvListF_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME'
          Width = 200
        end
        object cxgrdtblvListF_CAPTION_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_CAPTION_NL'
          Visible = False
          Width = 200
        end
        object cxgrdtblvListF_CAPTION_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_CAPTION_FR'
          Visible = False
          Width = 200
        end
        object cxgrdtblvListF_CAPTION: TcxGridDBColumn
          DataBinding.FieldName = 'F_CAPTION'
          Width = 200
        end
        object cxgrdtblvListF_PARAMETER_TYPE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_PARAMETER_TYPE_ID'
          Visible = False
        end
        object cxgrdtblvListF_PARAMETER_TYPE: TcxGridDBColumn
          DataBinding.FieldName = 'F_PARAMETER_TYPE'
          Width = 300
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmParameter.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
