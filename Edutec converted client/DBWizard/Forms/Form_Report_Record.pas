{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_Report_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  28/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Report_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView, Menus,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, cxControls, cxSplitter, Unit_FVBFFCDevExpress,
  dxNavBarCollns, dxNavBarBase, dxNavBar, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons;

type
  TfrmReport_Record = class(TeduRecordview)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Report;

{ TfrmReport_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmReport_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmReport_Record.GetDetailName: String;
begin
  Result := Inherited GetDetailName;
end;

end.