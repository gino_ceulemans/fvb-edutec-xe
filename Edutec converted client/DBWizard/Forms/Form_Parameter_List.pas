{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Parameter_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Parameter_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, Menus,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk,
  Unit_FVBFFCDevExpress, Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  StdCtrls, cxButtons, ExtCtrls;

type
  TfrmParameter_List = class(TEduListview)
    cxgrdtblvListF_PARAMETER_ID: TcxGridDBColumn;
    cxgrdtblvListF_QUERY_ID: TcxGridDBColumn;
    cxgrdtblvListF_QUERY_NAME: TcxGridDBColumn;
    cxgrdtblvListF_NAME: TcxGridDBColumn;
    cxgrdtblvListF_CAPTION_NL: TcxGridDBColumn;
    cxgrdtblvListF_CAPTION_FR: TcxGridDBColumn;
    cxgrdtblvListF_CAPTION: TcxGridDBColumn;
    cxgrdtblvListF_PARAMETER_TYPE_ID: TcxGridDBColumn;
    cxgrdtblvListF_PARAMETER_TYPE: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Parameter;

end.