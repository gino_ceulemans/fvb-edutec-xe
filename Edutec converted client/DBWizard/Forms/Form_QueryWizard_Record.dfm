inherited frmQueryWizard_Record: TfrmQueryWizard_Record
  Left = 68
  Top = 187
  ActiveControl = cxbtnExecute
  Caption = 'Query wizard detail'
  Position = poScreenCenter
  Registered = True
  ExplicitWidth = 800
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    ExplicitWidth = 890
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 554
      ExplicitLeft = 554
      inherited cxbtnOK: TFVBFFCButton
        Caption = 'Ok'
        Visible = False
        OnClick = acExecuteExecute
      end
      inherited cxbtnCancel: TFVBFFCButton
        Left = 223
        ExplicitLeft = 223
      end
      inherited cxbtnApply: TFVBFFCButton
        Left = 117
        Visible = False
        ExplicitLeft = 117
      end
      object cxbtnExecute: TFVBFFCButton
        Left = 117
        Top = 4
        Width = 104
        Height = 25
        Action = acExecute
        Default = True
        OptionsImage.Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        OptionsImage.Margin = 10
        OptionsImage.NumGlyphs = 2
        OptionsImage.Spacing = -1
        TabOrder = 3
      end
    end
  end
  inherited pnlTop: TFVBFFCPanel
    ExplicitWidth = 890
    inherited pnlRecord: TFVBFFCPanel
      Width = 622
      ExplicitWidth = 728
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 622
        Visible = False
        ExplicitWidth = 728
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 622
        ExplicitWidth = 728
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 622
        Height = 44
        Visible = False
        ExplicitWidth = 622
        ExplicitHeight = 44
        object cxlblFVBFFCLabel2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
          Caption = 'Query ( ID )'
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_QUERY_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_QUERY_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 193
        end
        object cxdblblF_QUERY_NAME: TFVBFFCDBLabel
          Left = 200
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_QUERY_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Anchors = [akLeft, akTop, akRight]
          DataBinding.DataField = 'F_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 515
        end
        object cxlblFVBFFCLabel1: TFVBFFCLabel
          Left = 200
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmQuery_Record.F_NAME'
          Caption = 'Query'
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Top = 73
        Width = 622
        ExplicitTop = 73
        ExplicitWidth = 728
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 622
        ExplicitWidth = 728
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Top = 77
        Width = 622
        Height = 248
        ExplicitTop = 77
        ExplicitWidth = 728
        ExplicitHeight = 260
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 622
          Caption = 'Sql parameters'
          ExplicitWidth = 728
        end
        inherited sbxMain: TScrollBox
          Width = 622
          Height = 223
          ExplicitWidth = 728
          ExplicitHeight = 235
          object bvlParameters: TBevel
            Left = 0
            Top = 0
            Width = 622
            Height = 153
            Align = alTop
            Shape = bsBottomLine
            ExplicitWidth = 728
          end
          object cxlblF_PARAMETER_COUNT: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_QUERY_ID'
            Caption = 'Aantal parameters'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbteF_PARAMETER_COUNT: TFVBFFCDBTextEdit
            Left = 152
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_PARAMETER_COUNT'
            DataBinding.DataField = 'F_PARAMETER_COUNT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Visible = False
            Width = 73
          end
          object cxlblF_TEXT: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_TEXT'
            Caption = 'Sql statement'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            Visible = False
          end
          object cxdbmmoF_TEXT1: TFVBFFCDBMemo
            Left = 152
            Top = 35
            HelpType = htKeyword
            HelpKeyword = 'frmQuery_Record.F_TEXT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_TEXT'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 3
            Visible = False
            Height = 105
            Width = 401
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      ExplicitLeft = 886
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Visible = False
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    Left = 20
    Top = 128
  end
  inherited alRecordView: TFVBFFCActionList
    Left = 104
    Top = 128
    object acExecute: TAction
      Caption = 'Execute'
      OnExecute = acExecuteExecute
    end
  end
  object srcParameter: TFVBFFCDataSource
    DataSet = dtmQueryWizard.cdsParameter
    OnStateChange = srcMainStateChange
    OnDataChange = srcMainDataChange
    Left = 68
    Top = 184
  end
  object tmrMyTim: TTimer
    Enabled = False
    Interval = 10
    OnTimer = tmrMyTimTimer
    Left = 64
    Top = 240
  end
end
