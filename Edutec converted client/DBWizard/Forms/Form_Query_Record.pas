{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_Query_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Query_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, Menus, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, cxControls,
  cxSplitter, Unit_FVBFFCDevExpress, dxNavBarCollns, dxNavBarBase,
  dxNavBar, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel, StdCtrls,
  Buttons, cxDBLabel, cxButtonEdit, cxDBEdit, cxMemo, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxContainer, cxEdit, cxLabel,Unit_PPWFrameWorkInterfaces;

type
  TfrmQuery_Record = class(TeduRecordview)
    cxlblF_QUERY_ID1: TFVBFFCLabel;
    cxdbseF_QUERY_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
    cxlblF_TEXT1: TFVBFFCLabel;
    cxdbmmoF_TEXT1: TFVBFFCDBMemo;
    cxlblF_DESCRIPTION_NL1: TFVBFFCLabel;
    cxdbmmoF_DESCRIPTION_NL1: TFVBFFCDBMemo;
    cxlblF_DESCRIPTION_FR1: TFVBFFCLabel;
    cxdbmmoF_DESCRIPTION_FR1: TFVBFFCDBMemo;
    cxlblF_QUERY_GROUP_NAME1: TFVBFFCLabel;
    cxdbbeF_QUERY_GROUP_NAME: TFVBFFCDBButtonEdit;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    cxdblblF_QUERY_ID: TFVBFFCDBLabel;
    cxdblblF_NAME: TFVBFFCDBLabel;
    dxnbiShowParameters: TdxNavBarItem;
    acShowParameters: TAction;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_QUERY_GROUP_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure acShowParametersExecute(Sender: TObject);
    procedure cxbtnOKClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Query, Data_EduMainClient, Form_FVBFFCBaseRecordView,
  Unit_PPWFrameWorkRecordView, unit_EdutecInterfaces;

{ TfrmQuery_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmQuery_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmQuery_Record.GetDetailName: String;
begin
  Result := cxdblblF_NAME.Caption; // Inherited GetDetailName;
end;

procedure TfrmQuery_Record.cxdbbeF_QUERY_GROUP_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUQueryDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUQueryDataModule, aDataModule ) ) then
    begin
      { Execute the method in the IEDUQueryDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowQuerygroup ( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectQuerygroup( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be executed when the form is Initalised and will be used
  to set up some things.

  @Name       TfrmQuery_Record.FVBFFCRecordViewInitialise
  @author     cheuten
  @param      aFrameWorkDataModule   The DataModule associated to the Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQuery_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUQueryGroupDataModule ) ) then
    begin
      cxdbbeF_QUERY_GROUP_NAME.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbteF_NAME_NL1 );
    end;
  end;

end;

procedure TfrmQuery_Record.acShowParametersExecute(Sender: TObject);
begin
  inherited;

  ShowDetailListView( Self, 'TdtmParameter', pnlRecordDetail );

end;

{*****************************************************************************
  This procedure will be used to refresh Query related parameters.

  @Name       TfrmQuery_Record.cxbtnOKClick
  @author     cheuten
  @sender     TObject        This variable incdicates wich action is used.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQuery_Record.cxbtnOKClick(Sender: TObject);
begin
  // Eerst zeker bewaren vooraleer te vernieuwen ...
  inherited;
  if ( Sender = cxbtnApply ) then
  begin
    // Refresh de parameters als er op Apply gedrukt is geworden ...
    // omdat dit de enige keer is dat deze moeten worden vernieuwd
    // Bij Ok wordt het scherm afgesloten en nadien compleet terug
    // opgebouwd
    RefreshVisibleDetailListViews;
  end;
end;

end.