inherited frmParameter_Record: TfrmParameter_Record
  Left = 352
  Top = 305
  Height = 519
  Caption = 'Parameter'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 453
  end
  inherited pnlTop: TFVBFFCPanel
    Height = 453
    inherited pnlRecord: TFVBFFCPanel
      Height = 453
      inherited pnlRecordFixedData: TFVBFFCPanel
        object cxlblF_PARAMETER_ID: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmParameter_Record.F_PARAMETER_ID'
          Caption = 'Parameter ( ID )'
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PAR_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmParameter_Record.F_PARAMETER_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PARAMETER_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 129
        end
        object cxdblblF_CAPTION: TFVBFFCDBLabel
          Left = 136
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmParameter_Record.F_CAPTION'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_CAPTION'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 480
        end
        object cxlblF_CAPTION: TFVBFFCLabel
          Left = 136
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmParameter_Record.F_CAPTION'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Omschrijving'
          FocusControl = cxdblblF_CAPTION
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 449
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Height = 379
        inherited sbxMain: TScrollBox
          Height = 354
          object cxgbFVBFFCGroupBox1: TFVBFFCGroupBox
            Left = 8
            Top = 8
            Caption = 'Algemeen'
            ParentColor = False
            TabOrder = 0
            Transparent = True
            Height = 177
            Width = 457
            object cxlblF_PARAMETER_ID1: TFVBFFCLabel
              Left = 16
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_PARAMETER_ID'
              Caption = 'Parameter ( ID )'
              FocusControl = cxdbseF_PARAMETER_ID1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbseF_PARAMETER_ID1: TFVBFFCDBSpinEdit
              Left = 144
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_PARAMETER_ID'
              RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
              DataBinding.DataField = 'F_PARAMETER_ID'
              DataBinding.DataSource = srcMain
              TabOrder = 1
              Width = 121
            end
            object cxdbbeF_QUERY_NAME1: TFVBFFCDBButtonEdit
              Left = 144
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_QUERY_NAME'
              RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
              DataBinding.DataField = 'F_QUERY_NAME'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_QUERY_NAME1PropertiesButtonClick
              TabOrder = 2
              Width = 300
            end
            object cxlblF_QUERY_NAME1: TFVBFFCLabel
              Left = 16
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_QUERY_NAME'
              Caption = 'Query'
              FocusControl = cxdbbeF_QUERY_NAME1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_NAME1: TFVBFFCLabel
              Left = 16
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_NAME'
              Caption = 'Veldnaam'
              FocusControl = cxdbteF_NAME1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_NAME1: TFVBFFCDBTextEdit
              Left = 144
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_NAME'
              DataBinding.DataField = 'F_NAME'
              DataBinding.DataSource = srcMain
              TabOrder = 5
              Width = 300
            end
            object cxdbteF_CAPTION_NL1: TFVBFFCDBTextEdit
              Left = 144
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_CAPTION_NL'
              DataBinding.DataField = 'F_CAPTION_NL'
              DataBinding.DataSource = srcMain
              TabOrder = 6
              Width = 300
            end
            object cxlblF_CAPTION_NL1: TFVBFFCLabel
              Left = 16
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_CAPTION_NL'
              Caption = 'Omschrijving ( NL )'
              FocusControl = cxdbteF_CAPTION_NL1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_CAPTION_FR1: TFVBFFCLabel
              Left = 16
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_CAPTION_FR'
              Caption = 'Omschrijving ( FR )'
              FocusControl = cxdbteF_CAPTION_FR1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_CAPTION_FR1: TFVBFFCDBTextEdit
              Left = 144
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_CAPTION_FR'
              DataBinding.DataField = 'F_CAPTION_FR'
              DataBinding.DataSource = srcMain
              TabOrder = 9
              Width = 300
            end
            object cxdbbeF_PARAMETER_TYPE1: TFVBFFCDBButtonEdit
              Left = 144
              Top = 144
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_PARAMETER_TYPE'
              RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
              DataBinding.DataField = 'F_PARAMETER_TYPE'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_PARAMETER_TYPE1PropertiesButtonClick
              TabOrder = 10
              Width = 300
            end
            object cxlblF_PARAMETER_TYPE1: TFVBFFCLabel
              Left = 16
              Top = 144
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_PARAMETER_TYPE'
              Caption = 'Type'
              FocusControl = cxdbbeF_PARAMETER_TYPE1
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
          end
          object cxgbFVBFFCGroupBox2: TFVBFFCGroupBox
            Left = 8
            Top = 192
            Caption = 'Lookup'
            ParentColor = False
            TabOrder = 1
            Transparent = True
            Height = 153
            Width = 457
            object cxlblF_TABLE: TFVBFFCLabel
              Left = 16
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_TABLE'
              Caption = 'Tabel'
              FocusControl = cxdbteF_TABLE
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbteF_TABLE: TFVBFFCDBTextEdit
              Left = 144
              Top = 48
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_TABLE'
              DataBinding.DataField = 'F_TABLE'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 1
              Width = 300
            end
            object cxdbteF_KEY: TFVBFFCDBTextEdit
              Left = 144
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_KEY'
              DataBinding.DataField = 'F_KEY'
              DataBinding.DataSource = srcMain
              Properties.ReadOnly = True
              TabOrder = 2
              Width = 300
            end
            object cxlblF_KEY: TFVBFFCLabel
              Left = 16
              Top = 72
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_KEY'
              Caption = 'Sleutel'
              FocusControl = cxdbteF_KEY
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_RESULT_NL: TFVBFFCLabel
              Left = 16
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_RESULT_NL'
              Caption = 'Resultaat ( NL )'
              FocusControl = cxdbbeF_RESULT_NL
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_RESULT_NL: TFVBFFCDBButtonEdit
              Left = 144
              Top = 96
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_RESULT_NL'
              RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
              DataBinding.DataField = 'F_RESULT_NL'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_RESULT_NLPropertiesButtonClick
              TabOrder = 5
              Width = 300
            end
            object cxdbbeF_RESULT_FR: TFVBFFCDBButtonEdit
              Left = 144
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_RESULT_FR'
              RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
              DataBinding.DataField = 'F_RESULT_FR'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_RESULT_FRPropertiesButtonClick
              TabOrder = 6
              Width = 300
            end
            object cxlblF_RESULT_FR: TFVBFFCLabel
              Left = 16
              Top = 120
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_RESULT_FR'
              Caption = 'Resultaat ( FR )'
              FocusControl = cxdbbeF_RESULT_FR
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxlblF_TABLE_KEY: TFVBFFCLabel
              Left = 16
              Top = 24
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_TABLE'
              Caption = 'Tabel && veld'
              FocusControl = cxdbbeF_TABLE_KEY
              ParentColor = False
              Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
            end
            object cxdbbeF_TABLE_KEY: TFVBFFCDBButtonEdit
              Left = 144
              Top = 20
              HelpType = htKeyword
              HelpKeyword = 'frmParameter_Record.F_TABLE'
              RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
              DataBinding.DataField = 'F_TABLE'
              DataBinding.DataSource = srcMain
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Properties.OnButtonClick = cxdbbeF_TABLE_KEYPropertiesButtonClick
              TabOrder = 9
              Width = 300
            end
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Height = 453
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 453
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 453
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmParameter.cdsRecord
  end
end
