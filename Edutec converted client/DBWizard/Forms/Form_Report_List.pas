{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  <TABLENAME> records.


  @Name       Form_Report_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  27/03/2008   Ivan Van den Bossche Added UpdatePropertiesNumColumn
  28/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Report_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, Menus, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, cxControls,
  cxSplitter, Unit_FVBFFCDevExpress, dxNavBarCollns, dxNavBarBase,
  dxNavBar, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel, StdCtrls,
  Buttons, cxDBLabel, cxButtonEdit, cxDBEdit, cxMemo, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxContainer, cxEdit, cxLabel,Unit_PPWFrameWorkInterfaces,
  Form_EDUListView, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, DBCtrls, cxCalc;

type
  TfrmReport_List = class(TEduListview)
    cxbtnPrint: TFVBFFCButton;
    cxbtnExportHTML: TFVBFFCButton;
    cxbtnExportXLS: TFVBFFCButton;
    cxbtnExportXML: TFVBFFCButton;
    cxgrdtblvListColumn1: TcxGridDBColumn;
    procedure FVBFFCListViewShow(Sender: TObject);
  private
    { Private declarations }
    procedure UpdatePropertiesNumColumn;
  protected
    { Protected declarations }
    procedure StoreActiveViewToIni; override;
    procedure RestoreActiveViewFromIni; override;

  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Report, Form_FVBFFCBaseListView, Unit_FVBFFCListView,
  Data_EduMainClient;

{*****************************************************************************
  Overriden Procedure to keep the ancestor of loading settings from inifile.
  Also needed to create all columns in the grid.

  @Name       TfrmReport_List.RestoreActiveViewFromIni
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmReport_List.RestoreActiveViewFromIni;
begin
  // inherited;
  cxgrdtblvList.DataController.CreateAllItems;
end;

{*****************************************************************************
  Overrriden Procedure to keep the ancestor of writing settings to an inifile.

  @Name       TfrmReport_List.StoreActiveViewToIni
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmReport_List.StoreActiveViewToIni;
begin
  // inherited;
end;

procedure TfrmReport_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;

    // Update properties for numeric columns. (ivdbossche 27/03/2008).
    UpdatePropertiesNumColumn;

    // Apply fist for all columns (ivdbossche 19/09/2007)
    cxgrdtblvList.ApplyBestFit();

end;


{*********************************************************************************
  Loops through all columns in Grid and update properties for all numeric columns
  (Mantis 2095).
  @Name       TfrmReport_List.UpdatePropertiesNumColumn
  @author     Ivan Van den Bossche
  @date       27/03/2008
  @param      None
  @return     None
  @Exception  None
  @See        None
**********************************************************************************}
procedure TfrmReport_List.UpdatePropertiesNumColumn;
var
  aIdx: Integer;
  aColumn: TcxGridDBColumn;
  aValueType: String;
begin
  for aIdx:=0 to cxgrdtblvList.ColumnCount-1 do
  begin
    aColumn := cxgrdtblvList.Columns[aIdx];
    aValueType := aColumn.DataBinding.ValueType;

    if (aValueType = 'Smallint') or (aValueType = 'Largeint') or (aValueType = 'Integer') or (aValueType = 'Float') or (aValueType = 'Currency') then
        aColumn.PropertiesClass := TcxCalcEditProperties;

  end;

end;

end.
