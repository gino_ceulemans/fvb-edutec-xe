{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_QueryWizard_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_QueryWizard_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxPC, cxControls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, cxMemo, cxDBEdit,
  cxSpinEdit, cxMaskEdit, cxButtonEdit, cxTextEdit, cxContainer, cxLabel,
  cxCurrencyEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, cxCalendar,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Menus, cxSplitter, Form_EDUListView,
  //dxExEdtr, dxTL, dxDBCtrl, dxCntner, dxDBTL,
  dxBar, cxDBLabel,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxDateUtils, Contnrs,
  Unit_PPWFrameWorkActions, unit_EdutecInterfaces, cxLookAndFeels, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxNavBarPainter,
  dxSkinsdxNavBarAccordionViewPainter, System.Actions;

resourcestring
  rsEmptyValue='Gelieve parameter %s te voorzien van een geldige waarde.';

type
  TfrmQueryWizard_Record = class(TeduRecordview)
    cxlblFVBFFCLabel2: TFVBFFCLabel;
    cxdblblF_QUERY_ID: TFVBFFCDBLabel;
    cxdblblF_QUERY_NAME: TFVBFFCDBLabel;
    cxlblFVBFFCLabel1: TFVBFFCLabel;
    srcParameter: TFVBFFCDataSource;
    cxlblF_PARAMETER_COUNT: TFVBFFCLabel;
    cxdbteF_PARAMETER_COUNT: TFVBFFCDBTextEdit;
    cxlblF_TEXT: TFVBFFCLabel;
    cxdbmmoF_TEXT1: TFVBFFCDBMemo;
    bvlParameters: TBevel;
    acExecute: TAction;
    cxbtnExecute: TFVBFFCButton;
    tmrMyTim: TTimer;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure FVBFFCRecordViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure acExecuteExecute(Sender: TObject);
    procedure FVBFFCRecordViewShow(Sender: TObject);
    procedure tmrMyTimTimer(Sender: TObject);
  private
    { Private declarations }
    aParaCollection : TObjectList;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_QueryWizard, Data_EduMainClient, Unit_PPWFrameWorkForm;

{ TfrmQueryWizard_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmQueryWizard_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmQueryWizard_Record.GetDetailName: String;
begin
  Result := cxdblblF_QUERY_NAME.Caption;
end;

{*****************************************************************************
  Procedure used to create parameters at runtime and create a collection
  of controls containing the data ( values ) for the referenced parameters.

  @Name       TfrmQueryWizard_Record.FVBFFCRecordViewInitialise
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  aDataModule : IEDUQueryWizardDataModule;
  aLabel   : TFVBFFCLabel;
  aTxtEdit : TFVBFFCTextEdit;
  aIntEdit : TFVBFFCSpinEdit;
  aChkEdit : TFVBFFCCheckBox;
  aDteEdit : TFVBFFCDateEdit;
  aCboEdit : TFVBFFCImageComboBox;
  aCboItem : TcxImageComboBoxItem;
  aNextTop, i, aWidth : integer;
  aStringList : TStringList;
begin
  inherited;

  if ( Supports( aFrameWorkDataModule, IEDUQueryWizardDataModule, aDataModule ) ) then
  begin
    bvlParameters.Height := 0;
    srcParameter.DataSet := aDataModule.ParameterDataSet;
    // Nu gaan we voor alle parameters ( als er zijn uiteraard ) de nodige
    // componenten at runtime opbouwen
    // Indien er geen componenten te tonen zijn dan kan men direct op de actie
    // klikken om de SQL statement uit te voeren.
    aParaCollection := TObjectList.Create;
    aWidth := 320;
    with srcParameter.DataSet do
    begin
      First;
      aNextTop :=  bvlParameters.Height + 6;
      while not Eof do
      begin
        aLabel :=  TFVBFFCLabel.Create(sbxMain);
        aLabel.Parent := sbxMain;
        aLabel.AutoSize := True;
        aLabel.Style.StyleController := dtmEDUMainClient.cxescSearchCriteria;
        aLabel.Top := aNextTop;
        aLabel.Left := 8;
        aLabel.Caption := FieldByName( 'F_CAPTION' ).AsString;
        case FieldByName( 'F_PARAMETER_TYPE_ID' ).AsInteger of
          1 :
          begin
            // String parameter
            aTxtEdit := TFVBFFCTextEdit.Create(sbxMain);
            aTxtEdit.Name := 'par' + FieldByName( 'F_NAME' ).AsString;
            aTxtEdit.Parent := sbxMain;
            aTxtEdit.Top := aNextTop;
            aTxtEdit.Left := cxdbteF_PARAMETER_COUNT.Left;
            aTxtEdit.Width := aWidth;
            aTxtedit.Anchors := [akLeft, akTop];
            aTxtEdit.Text := '';
            aParaCollection.Add(aTxtEdit);
          end;
          2 :
          begin
            // Float parameter
            aIntEdit := TFVBFFCSpinEdit.Create(sbxMain);
            aIntEdit.Name := 'par' + FieldByName( 'F_NAME' ).AsString;
            aIntEdit.Parent := sbxMain;
            aIntEdit.Top := aNextTop;
            aIntEdit.Left := cxdbteF_PARAMETER_COUNT.Left;
            aIntEdit.Width := aWidth;
            aIntedit.Anchors := [akLeft, akTop];
            aIntedit.Properties.ValueType := TcxSpinEditValueType(vtFloat);
            aParaCollection.Add(aIntEdit);
          end;
          3 :
          begin
            // Integer parameter
            aIntEdit := TFVBFFCSpinEdit.Create(sbxMain);
            aIntEdit.Name := 'par' + FieldByName( 'F_NAME' ).AsString;
            aIntEdit.Parent := sbxMain;
            aIntEdit.Top := aNextTop;
            aIntEdit.Left := cxdbteF_PARAMETER_COUNT.Left;
            aIntEdit.Width := aWidth;
            aIntedit.Anchors := [akLeft, akTop];
            aParaCollection.Add(aIntEdit);
          end;
          5 :
          begin
            // Boolean parameter
            aChkEdit  := TFVBFFCCheckBox.Create(sbxMain);
            aChkEdit.Name := 'par' + FieldByName( 'F_NAME' ).AsString;
            aChkEdit.Parent := sbxMain;
            aChkEdit.Top := aNextTop;
            aChkEdit.Left := cxdbteF_PARAMETER_COUNT.Left;
            aChkEdit.Width := aChkEdit.Height;
            aParaCollection.Add(aChkEdit);
          end;
          10 :
          begin
            // Lookup parameter
            aCboEdit := TFVBFFCImageComboBox.Create(sbxMain);
            aCboEdit.Name := 'par' + FieldByName( 'F_NAME' ).AsString;
            aCboEdit.Parent := sbxMain;
            aCboEdit.Top := aNextTop;
            aCboEdit.Left := cxdbteF_PARAMETER_COUNT.Left;
            aCboEdit.Width := aWidth;
            aCboEdit.Anchors := [akLeft, akTop];
            // Roep stored proc met de correcte parameters ( 3 stuks )
            aStringList := TStringList.Create;
            aDataModule.FillSelectLookUp(
              aStringList,
              FieldByName( 'F_TABLE'  ).AsString,
              FieldByName( 'F_KEY'    ).AsString,
              FieldByName( 'F_RESULT' ).AsString  );
            // Koppel daarna de resultset ( dataset ) aan deze LookUpCombobox
            for i:= 0 to aStringList.Count -1 do
            begin
              aCboItem := acboEdit.Properties.Items.Add;
              aCboItem.Description := aStringList.Values[aStringList.Names[i]];
              aCboItem.Value := aStringList.Names[i];
            end;
            aStringList.Clear;
            freeAndNil( aStringList );
            aParaCollection.Add(aCboEdit);
          end;
          11 :
          begin
            // DateTime parameter
            aDteEdit := TFVBFFCDateEdit.Create(sbxMain);
            aDteEdit.Name := 'par' + FieldByName( 'F_NAME' ).AsString;
            aDteEdit.Parent := sbxMain;
            aDteEdit.Top := aNextTop;
            aDteEdit.Left := cxdbteF_PARAMETER_COUNT.Left;
            aDteEdit.Width := aWidth;
            aDteEdit.Anchors := [akLeft, akTop];
            aParaCollection.Add(aDteEdit);
          end;
        end;
        Next;
        // Correctere berekening van de volgende positie
        // ( om mooi op grid van 8 telkens te beginnen )
        aNextTop := aLabel.Top + ( ( ( aLabel.Height div 8 ) + 1 ) * 8 );
      end;
    end;
    // Zet ActiveControl indien mogelijk naar eerste parameter
    // anders zal deze nog steeds naar de Execute button verwijzen.
    if aParaCollection.Count > 0 then
    begin
      self.ActiveControl := TWinControl(aParaCollection.Items[0]);
    end;
  end;
end;

{*****************************************************************************
  Procedure used to clean up after the form is closed.

  @Name       TfrmQueryWizard_Record.FVBFFCRecordViewClose
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_Record.FVBFFCRecordViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // Alle controls worden automatisch opgekuist, enkel de collectie
  // moet nog leeggemaakt worden.
  aParaCollection.Clear;
  freeAndNil(aParaCollection);
end;

{*****************************************************************************
  Procedure to create a new "Report" entity. Sql statement and Parameter
  collection will be passed to this newly created entity

  @Name       TfrmQueryWizard_Record.acExecuteExecute
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_Record.acExecuteExecute(Sender: TObject);
var
  i : integer;
  aDataModule : IEDUQueryWizardDataModule;
  aParamName : string;
  aParamValue : Variant;
  aTxtEdit : TFVBFFCTextEdit;
  aIntEdit : TFVBFFCSpinEdit;
  aChkEdit : TFVBFFCCheckBox;
  aDteEdit : TFVBFFCDateEdit;
  aCboEdit : TFVBFFCImageComboBox;
begin
  inherited;
  // Controleer hier eerst of alle gegevens ingevuld zijn en er dus verder
  // gegaan kan worden met de afhandeling van de query resulterend in een Report
  // Zorg ervoor dus voor dat we hier de nodige acties ondernemen om door te
  // geven aan een report entiteit ( SQL-Text en de nodige parameters )
  try
    if Assigned( self.FrameWorkDataModule ) then
    begin
      if ( Supports( self.FrameWorkDataModule, IEDUQueryWizardDataModule, aDataModule ) ) then
      begin
        aDataModule.FillSqlStatement(cxdbmmoF_TEXT1.Text, strToIntDef(cxdbteF_PARAMETER_COUNT.Text, 0 ) );

        for i := 0 to aParaCollection.Count -1 do
        begin
          if aParaCollection.Items[i] is TFVBFFCImageComboBox then
          begin
            aCboEdit := TFVBFFCImageComboBox(aParaCollection.Items[i]);
            aParamName := copy(aCboEdit.Name,4,length(aCboEdit.Name ));
            aParamValue := aCboEdit.Properties.Items[aCboEdit.SelectedItem].Value;
          end
          else if aParaCollection.Items[i] is TFVBFFCDateEdit then
          begin
            // Forces modified text in parameter field to be saved
            // Solves pressing Enter-key issue after edited Date param field
            TFVBFFCDateEdit(aParaCollection.Items[i]).PostEditValue;
            aDteEdit := TFVBFFCDateEdit(aParaCollection.Items[i]);
            aParamName := copy(aDteEdit.Name,4,length(aDteEdit.Name ));
            aParamValue := aDteEdit.Date;

            { Added check (ivdbossche 18/09/2007) }
            if aParamValue = NullDate then
              raise Exception.Create(Format(rsEmptyValue, [aParamName]));
          end
          else if aParaCollection.Items[i] is TFVBFFCCheckBox then
          begin
            aChkEdit := TFVBFFCCheckBox(aParaCollection.Items[i]);
            aParamName := copy(aChkEdit.Name,4,length(aChkEdit.Name ));
            aParamValue := ( aChkEdit.State = TcxCheckBoxState(cbsChecked) );
          end
          else if aParaCollection.Items[i] is TFVBFFCSpinEdit then
          begin
            aIntEdit := TFVBFFCSpinEdit(aParaCollection.Items[i]);
            aParamName := copy(aIntEdit.Name,4,length(aIntEdit.Name ));
            aParamValue := aIntEdit.Value;
          end
          else if aParaCollection.Items[i] is TFVBFFCTextEdit then
          begin
            aTxtEdit := TFVBFFCTextEdit(aParaCollection.Items[i]);
            aParamName := copy(aTxtEdit.Name,4,length(aTxtEdit.Name ));
            aParamValue := aTxtEdit.Text;
          end;

          aDataModule.AddParameter(aParamName, aParamValue);
        end;

        aDataModule.ShowReport;
      end;
    end;
  except
      on E: Exception do begin
          MessageDlg(E.Message, mtWarning, [mbOk], 0);
      end;
  end;
end;

{*****************************************************************************
  Procedure to enable timer after the form is showed.

  @Name       TfrmQueryWizard_Record.FVBFFCRecordViewShow
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_Record.FVBFFCRecordViewShow(Sender: TObject);
begin
  inherited;
  // Indien er meedere controls zijn dan even deze gelijktrekken
  if aParaCollection.Count > 1 then
  begin
    TcxCustomEdit(aParaCollection.Items[0]).Width := cxdbmmoF_TEXT1.Width;
  end;

  tmrMyTim.Enabled := True;

end;

{*****************************************************************************
  Timer event used to automate response when no parameters are specified.
  Auto execute and close after execution. ( Will run only once )

  @Name       TfrmQueryWizard_Record.tmrMyTimTimer
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmQueryWizard_Record.tmrMyTimTimer(Sender: TObject);
begin
  inherited;
  tmrMyTim.Enabled := False;
  if aParaCollection.Count = 0 then
  begin
    cxbtnExecute.Click;
    ModalResult := mrCancel;
  end;
end;

end.
