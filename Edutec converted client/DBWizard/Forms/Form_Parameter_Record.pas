{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  <TABLENAME> records.


  @Name       Form_Parameter_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_Parameter_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, Menus, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, cxControls,
  cxSplitter, Unit_FVBFFCDevExpress, dxNavBarCollns, dxNavBarBase,
  dxNavBar, cxButtons, ExtCtrls, Unit_FVBFFCFoldablePanel, StdCtrls,
  Buttons, cxDBLabel, cxSpinEdit, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxContainer, cxEdit, cxLabel, Unit_PPWFrameWorkInterfaces,
  cxGraphics, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxGroupBox;

type
  TfrmParameter_Record = class(TeduRecordview)
    cxlblF_PARAMETER_ID: TFVBFFCLabel;
    cxdblblF_PAR_ID: TFVBFFCDBLabel;
    cxdblblF_CAPTION: TFVBFFCDBLabel;
    cxlblF_CAPTION: TFVBFFCLabel;
    cxgbFVBFFCGroupBox1: TFVBFFCGroupBox;
    cxlblF_PARAMETER_ID1: TFVBFFCLabel;
    cxdbseF_PARAMETER_ID1: TFVBFFCDBSpinEdit;
    cxdbbeF_QUERY_NAME1: TFVBFFCDBButtonEdit;
    cxlblF_QUERY_NAME1: TFVBFFCLabel;
    cxlblF_NAME1: TFVBFFCLabel;
    cxdbteF_NAME1: TFVBFFCDBTextEdit;
    cxdbteF_CAPTION_NL1: TFVBFFCDBTextEdit;
    cxlblF_CAPTION_NL1: TFVBFFCLabel;
    cxlblF_CAPTION_FR1: TFVBFFCLabel;
    cxdbteF_CAPTION_FR1: TFVBFFCDBTextEdit;
    cxdbbeF_PARAMETER_TYPE1: TFVBFFCDBButtonEdit;
    cxlblF_PARAMETER_TYPE1: TFVBFFCLabel;
    cxgbFVBFFCGroupBox2: TFVBFFCGroupBox;
    cxlblF_TABLE: TFVBFFCLabel;
    cxdbteF_TABLE: TFVBFFCDBTextEdit;
    cxdbteF_KEY: TFVBFFCDBTextEdit;
    cxlblF_KEY: TFVBFFCLabel;
    cxlblF_RESULT_NL: TFVBFFCLabel;
    cxdbbeF_RESULT_NL: TFVBFFCDBButtonEdit;
    cxdbbeF_RESULT_FR: TFVBFFCDBButtonEdit;
    cxlblF_RESULT_FR: TFVBFFCLabel;
    cxlblF_TABLE_KEY: TFVBFFCLabel;
    cxdbbeF_TABLE_KEY: TFVBFFCDBButtonEdit;
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxdbbeF_QUERY_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_PARAMETER_TYPE1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_TABLE_KEYPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_RESULT_NLPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxdbbeF_RESULT_FRPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_Parameter, Data_EduMainClient, Form_FVBFFCBaseRecordView, unit_EdutecInterfaces;

{ TfrmParameter_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmParameter_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmParameter_Record.GetDetailName: String;
begin
  Result := cxdbteF_NAME1.text;
end;

procedure TfrmParameter_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;
  
  // Zorg ervoor dat als dit scherm w geopend vanuit Query we het veld
  // Query niet toegankelijk maken en de focus verzetten naar het volgende veld
  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUQueryDataModule ) ) then
    begin
      cxdbbeF_QUERY_NAME1.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbteF_NAME1 );
    end;
  end;
end;

procedure TfrmParameter_Record.cxdbbeF_QUERY_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUParameterDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUParameterDataModule, aDataModule ) ) then
    begin
      { Execute the method in the IEDUParameterDataModule corresponding to
        the button on which the user clicked. }
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowQuery ( srcMain.DataSet, rvmEdit );
        end;
        else aDataModule.SelectQuery ( srcMain.DataSet );
      end;
    end;
  end;
end;

procedure TfrmParameter_Record.cxdbbeF_PARAMETER_TYPE1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUParameterDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUParameterDataModule, aDataModule ) ) then
    begin
      // User can only click on one button ... hence no selection possible
      aDataModule.SelectType ( srcMain.DataSet );
{
      // Execute the method in the IEDUParameterDataModule corresponding to
      //  the button on which the user clicked.
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowType ( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aDataModule.SelectType ( srcMain.DataSet );
        end;
      end;
}
    end;
  end;
end;

procedure TfrmParameter_Record.cxdbbeF_TABLE_KEYPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUParameterDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUParameterDataModule, aDataModule ) ) then
    begin
      // User can only click on one button ... hence no selection possible
      aDataModule.SelectTableField ( srcMain.DataSet );
{
      // Execute the method in the IEDUParameterDataModule corresponding to
      //  the button on which the user clicked.
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowType ( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aDataModule.SelectType ( srcMain.DataSet );
        end;
      end;
}
    end;
  end;
end;

procedure TfrmParameter_Record.cxdbbeF_RESULT_NLPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUParameterDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUParameterDataModule, aDataModule ) ) then
    begin
      // User can only click on one button ... hence no selection possible
      aDataModule.SelectResultNL ( srcMain.DataSet );
{
      // Execute the method in the IEDUParameterDataModule corresponding to
      //  the button on which the user clicked.
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowType ( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aDataModule.SelectType ( srcMain.DataSet );
        end;
      end;
}
    end;
  end;
end;

procedure TfrmParameter_Record.cxdbbeF_RESULT_FRPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUParameterDataModule;
begin
  inherited;

  { The action may only be executed if the form is in Edit or Add Mode }
  if ( Mode in [ rvmEdit, rvmAdd ] ) then
  begin
    if ( Assigned( FrameWorkDataModule ) ) and
       ( Supports( FrameWorkDataModule, IEDUParameterDataModule, aDataModule ) ) then
    begin
      // User can only click on one button ... hence no selection possible
      aDataModule.SelectResultFR ( srcMain.DataSet );
{
      // Execute the method in the IEDUParameterDataModule corresponding to
      //  the button on which the user clicked.
      case aButtonIndex of
        0 :
        begin
          aDataModule.ShowType ( srcMain.DataSet, rvmEdit );
        end;
        else
        begin
          aDataModule.SelectType ( srcMain.DataSet );
        end;
      end;
}
    end;
  end;
end;

end.
