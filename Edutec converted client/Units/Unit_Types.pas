unit Unit_Types;

interface

uses
  Controls, StdCtrls, cxGrid;

type
  TgbDisplay = record
    GroupBox  : TGroupBox;
    btnPlus   : TButton;
    btnMinus  : TButton;
    Grid      : TcxGrid;
    GridHeight: integer;
    Align     : TAlign;
    Anchors   : TAnchors;
    Top       : integer;
    Left      : integer;
    Width     : integer;
    Height    : integer;
    gbExtra   : TGroupBox;
    gbExtraHeight : integer;
    gbExtraTop : integer;
  end;

implementation

end.
