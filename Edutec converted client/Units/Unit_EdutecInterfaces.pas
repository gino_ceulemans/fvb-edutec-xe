unit Unit_EdutecInterfaces;

interface

uses unit_FVBFFCInterfaces,  Unit_PPWFrameWorkInterfaces, Unit_PPWFrameWorkClasses, cxGrid,
  Unit_PPWFrameWorkActions, cxGridCustomView, Controls, DB, DBClient, classes, Contnrs;

type
  IEDULanguageDataModule = Interface( IFVBFFCBaseDataModule )
  ['{7218937D-2078-4491-8F72-E5E5FC559D61}']
  end;

  IEDUProgramTypeDataModule = Interface( IFVBFFCBaseDataModule )
  ['{ACD33FF4-DE93-4A49-8B7E-065AED4C68E9}']
  end;

  IEDUCountryDataModule = Interface( IFVBFFCBaseDataModule )
  ['{113B55D6-78D8-4AC9-B9D8-509577D2B5FD}']
  end;

  IEDUCountryPartDataModule = Interface( IFVBFFCBaseDataModule )
  ['{F174D7E8-AF82-4338-9246-0E82B5A81DFF}']
    procedure SelectCountry( aDataSet : TDataSet );
    procedure ShowCountry  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUProvinceDataModule = Interface( IFVBFFCBaseDataModule )
  ['{B9DE0E47-C770-4E10-9861-94734779B5FC}']
    procedure SelectCountryPart( aDataSet : TDataSet );
    procedure ShowCountryPart  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;              

  IEDUPostalCodeDataModule = Interface( IFVBFFCBaseDataModule )
  ['{7A627EC9-E6DF-4F9F-8171-7ACC71104D2A}']
    procedure SelectProvince( aDataSet : TDataSet );
    procedure ShowProvince  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUSessionGroupDataModule = Interface( IFVBFFCBaseDataModule )
  ['{BA538ADE-7511-4E70-BFCA-29FE0CC9A93C}']
  end;

  IEDUUserDataModule = Interface( IFVBFFCBaseDataModule )
  ['{B388F2A0-3DF0-4D79-A690-A2DECFFF53CF}']
    procedure SelectGender  ( aDataSet : TDataSet );
    procedure SelectLanguage( aDataSet : TDataSet );
    procedure ShowLanguage  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowGender    ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUProfileDataModule = Interface( IFVBFFCBaseDataModule )
  ['{5EF1C0DF-923B-463B-8914-7763481E254F}']
  end;

  IEDUUserProfileDataModule = Interface( IFVBFFCBaseDataModule )
  ['{A5B6FF0D-99A4-42D5-9029-00127944A5C3}']
    procedure SelectProfile( aDataSet : TDataSet );
    procedure SelectUser   ( aDataSet : TDataSet );
    procedure ShowProfile  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowUser     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUProgDisciplineDataModule = Interface( IFVBFFCBaseDataModule )
  ['{4317C34E-3780-4031-8DBE-349B664AD4AC}']
  end;

  IEDUProgProgramDataModule = Interface( IFVBFFCBaseDataModule )
  ['{AA709FDB-1AA6-403A-92C7-E468335E2BC4}']
    procedure SelectDiscipline ( aDataSet : TDataSet );
    procedure SelectLanguage   ( aDataSet : TDataSet );
    procedure SelectProgramType( aDataSet : TDataSet );
    procedure SelectUser       ( aDataSet : TDataSet );
    procedure ShowDiscipline   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowProgramType  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowUser         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUDocInstructorDataModule = Interface( IFVBFFCBaseDataModule )
  ['{0ECDDE4E-BEF6-4486-80C9-F63E28478BC1}']
    procedure SelectDiploma      ( aDataSet : TDataSet );
    procedure SelectGender       ( aDataSet : TDataSet );
    procedure SelectLanguage     ( aDataSet : TDataSet );
    procedure SelectMedium       ( aDataSet : TDataSet );
    procedure SelectPostalCode   ( aDataSet : TDataSet );
    procedure SelectCountry      ( aDataSet : TDataSet );
    procedure SelectTitle        ( aDataSet : TDataSet );
    procedure SelectOrganisation ( aDataSet : TDataSet );
    procedure ShowDiploma        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowGender         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowLanguage       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowMedium         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowPostalCode     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowCountry        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowTitle          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowOrganisation   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUProgInstructorDataModule = interface( IFVBFFCBaseDataModule )
  ['{1ABCEF55-B3FA-494A-A599-9AEB9012D1F4}']
    procedure SelectInstructor ( aDataSet : TDataSet );
    procedure SelectProgram    ( aDataSet : TDataSet );
    procedure ShowInstructor   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowProgram      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUCenInfrastructureDataModule = interface ( IFVBFFCBaseDataModule )
  ['{15422097-F755-480F-BEE2-EFD4CA49D101}']
    procedure SelectLanguage   ( aDataSet : TDataSet );
    procedure SelectMedium     ( aDataSet : TDataSet );
    procedure SelectPostalCode ( aDataSet : TDataSet );
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowPostalCode   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUOrgTypeDataModule = interface( IFVBFFCBaseDataModule )
  ['{226F9215-6115-4237-9B5A-314C02F5150E}']
  end;

  IEDUOrganisationDataModule = interface( IFVBFFCBaseDataModule )
  ['{EE472E87-5814-4B51-A62D-C636725E3B9B}']
    procedure SelectCountry    ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False );
    procedure SelectLanguage   ( aDataSet : TDataSet );
    procedure SelectMedium     ( aDataSet : TDataSet );
    procedure SelectOrgType    ( aDataSet : TDataSet );
    procedure SelectPostalCode ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False );
    procedure ShowCountry      ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowOrgType      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowPostalCode   ( aDataSet : TDataSet; aInvoiceAddress : Boolean = False; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure CopyAddressToInvoiceAddress( aDataSet : TDataSet );
    procedure JoinOrganisations( OrganisationFilter: String);
    procedure JoinPersons (OrganisationID: Integer);

    function ClearPartnerWinter ( aDataSet: TDataSet ): Boolean;
    procedure ClearAuthorizationNr ( aDataSet: TDataSet );
    function IsEducationCenter ( aDataSet: TDataSet ): Boolean;

    procedure UpdateAdminCostType ( aDataSet: TDataSet; adminCostTypeIndex: Integer );
    function GetAdminCostTypeItemIndex ( aDataSet: TDataSet ): Integer;

  end;

  IEDUPersonDataModule = interface( IFVBFFCBaseDataModule )
  ['{5FD26FC1-5793-454A-AA97-3AD98FF89294}']
    procedure SelectCountry    ( aDataSet : TDataSet );
    procedure SelectGender     ( aDataSet : TDataSet );
    procedure SelectLanguage   ( aDataSet : TDataSet );
    procedure SelectMedium     ( aDataSet : TDataSet );
    procedure SelectPostalCode ( aDataSet : TDataSet );
    procedure SelectTitle      ( aDataSet : TDataSet );
    procedure SelectDiploma    ( aDataSet : TDataSet );
    procedure ShowCountry      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowGender       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowLanguage     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowMedium       ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowPostalCode   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowTitle        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowDiploma      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure RetrieveDoublePersons ( aPersonId: Integer; aDoublePersonIds: TStringList);
    procedure JoinPersons(const APersonList: WideString; ACopyToPersonID: Integer);
  end;

  IEDURoleDataModule = interface( IFVBFFCBaseDataModule )
  ['{E79B9AE5-8805-4688-B5C9-7D9919FB7BC0}']
    function GetCreateForPerson: Boolean; 
    function GetCreateForPersonFirstName: String;
    function GetCreateForPersonID: Integer;
    function GetCreateForPersonLastName: String;
    function GetCreateForPersonSocSecNr: String;

    procedure AddForPerson        ( aDataSet : TDataSet );
    procedure InitialisePersonInfo( aDataSet : TDataSet );
    procedure SelectOrganisation ( aDataSet : TDataSet );
    procedure SelectPerson       ( aDataSet : TDataSet );
    procedure SelectRoleGroup    ( aDataSet : TDataSet );
    procedure SelectSchoolyear   ( aDataSet : TDataSet );
    procedure ShowOrganisation   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowPerson         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowRoleGroup      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowSchoolyear     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );

    property CreateForPerson          : Boolean read GetCreateForPerson;
    property CreateForPersonID        : Integer read GetCreateForPersonID;
    property CreateForPersonFirstName : String  read GetCreateForPersonFirstName;
    property CreateForPersonLastName  : String  read GetCreateForPersonLastName;
    property CreateForPersonSocSecNr  : String  read GetCreateForPersonSocSecNr;
  end;

  IEDUSessionDataModule = Interface( IFVBFFCBaseDataModule )
  ['{4DDFB8C2-AE43-442C-87FD-1EB954E2AD57}']
    procedure SelectInfrastructure ( aDataSet : TDataSet );
    procedure SelectLanguage       ( aDataSet : TDataSet );
    procedure SelectProgram        ( aDataSet : TDataSet );
    procedure SelectSessionGroup   ( aDataSet : TDataSet );
    procedure SelectStatus         ( aDataSet : TDataSet );
    procedure SelectStatusFilter   ( aDataSet : TDataSet );
    procedure SelectUser           ( aDataSet : TDataSet );
    procedure SelectCooperation    ( aDataSet : TDataSet );
    procedure SelectInstructor     ( aDataSet : TDataSet );
    procedure ShowInfrastructure   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowLanguage         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowProgram          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowSessionGroup     ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowStatus           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowUser             ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowCooperation      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );

    function CountEnrolments       ( aDataSet : TDataSet ) : integer;
    function UpdateEnrolments      ( aDataSet : TDataSet; NewWorkerValue, NewClerkValue, NewOtherValue : double): integer;
    function CheckStatus           ( aDataSet : TDataSet ) : integer;
    procedure ResetStatus          ( aDataSet : TDataSet; aStatus : integer );
    procedure ResetSessionDays     ( aDataSet : TDataSet );
    procedure SetStatusFacturated  ( aDataSet : TDataSet ); 

    procedure MarkAanwezigheidslijstPrinted ( aSessionList: String );
    procedure MarkAsCertificatesPrinted (EnrollmentList: String); // This datamodule should also support this procedure since it is needed in Form_SesSession_List (ivdbossche 17-07-2007)
    procedure ShowConfirmationRpts( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowSessionWizard ( aDataSet : TDataSet);
    procedure CreateOCR(ASessionID: Integer);
  end;

  IEDUStatusDataModule = Interface( IFVBFFCBaseDataModule )
  ['{1D9269AA-4043-4253-9B24-5AAD304312A1}']
  end;

  IEDUSesWizardDataModule = Interface( IFVBFFCBaseDataModule )
  ['{1A77E560-FEAD-4320-80FB-11A9A05D9E4A}']
    function GetDataChanged          : boolean;
    function GetExecuteDone          : Boolean;
    function GetForeCastDone         : Boolean;
    function GetSelectedRolesDataSet : TClientDataSet;
    function GetSessionDataSet       : TClientDataSet;
    function GetProgramDataSet       : TClientDataSet;
    function GetWizardDataSet        : TClientDataSet;
    function GetWizardResultDataSet  : TClientDataSet;
    function GetCanDoForeCast        : Boolean;
    function GetCanDoExecute         : Boolean;
    function ShouldRestoreWizardData : Boolean;

    procedure CreatePersonAndRole   ( aDataSet : TDataSet );
    procedure Execute;
    procedure ForeCast;
    procedure RemoveAllRoles        ( aDataSet : TDataSet );
    procedure RestoreWizardData;
    procedure SaveWizardData;
    procedure SetDataChanged(const Value: Boolean);

    procedure SelectInfrastructure ( aDataSet : TDataSet );
    procedure SelectProgram      ( aDataSet : TDataSet );
    procedure SelectRole         ( aDataSet : TDataSet );

    procedure ShowInfrastructure   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowPerson         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowProgram        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowRole           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );


    property CanDoExecute         : Boolean        read GetCanDoExecute;
    property CanDoForeCast        : Boolean        read GetCanDoForeCast;
    property DataChanged          : Boolean        read GetDataChanged write SetDataChanged;
    property SelectedRolesDataSet : TClientDataSet read GetSelectedRolesDataSet;
    property SessionDataSet       : TClientDataSet read GetSessionDataSet;
    property ProgramDataSet       : TClientDataSet read GetProgramDataSet;
    property WizardDataSet        : TClientDataSet read GetWizardDataSet;
    property WizardResultDataSet  : TClientDataSet read GetWizardResultDataSet;
    property ForeCastDone         : Boolean        read GetForeCastDone;
    property ExecuteDone          : Boolean        read GetExecuteDone;
  end;

  IEDUEnrolmentDataModule = Interface( IFVBFFCBaseDataModule )
  ['{3A0428A2-99E6-4728-A313-BE428E8148AC}']
    procedure MoveToSession      ( const aEnrollmentID, aNewSessionID : Integer );
    procedure SelectPerson       ( aDataSet : TDataSet );
    procedure SelectRole         ( aDataSet : TDataSet );
    procedure SelectStatus       ( aDataSet : TDataSet );
    procedure SelectSession      ( aDataSet : TDataSet );
    procedure ShowPerson         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowRole           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowStatus         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowSession        ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    function  CheckStatus        ( aDataSet : TDataSet ) : integer;
    procedure MarkAsCertificatesPrinted (EnrollmentList: String);
    function  GetDefaultPrice    ( TussenkomstType: Integer; SessieID: Integer): double;
    procedure SelectPartnerWinter ( aDataSet : TDataSet );
    procedure ShowPartnerWinter ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure RemovePartnerWinter ( aDataSet : TDataSet);
  end;

  IEDUCenRoomDataModule = Interface( IFVBFFCBaseDataModule )
  ['{AE739E33-D0AE-4636-9F62-33A683EE1023}']
    procedure SelectCenInfrastructure       ( aDataSet : TDataSet );
    procedure ShowCenInfrastructure         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUQueryGroupDataModule = Interface( IFVBFFCBaseDataModule )
  ['{1CD70F2B-7C8C-4F82-8C60-6ABB5B16BFC3}']
    procedure SelectParent ( aDataSet : TDataSet );
    procedure ShowParent   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUQueryDataModule = Interface( IFVBFFCBaseDataModule )
  ['{F1FB1AD8-3803-4CF2-A1BC-13C887F35FCB}']
    procedure SelectQueryGroup( aDataSet : TDataSet );
    procedure ShowQueryGroup  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUParameterDataModule = Interface( IFVBFFCBaseDataModule )
  ['{4C9066CB-3640-4E68-8E43-24BD36722524}']
    procedure SelectQuery ( aDataSet : TDataSet );
    procedure ShowQuery   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectType  ( aDataSet : TDataSet );
    procedure ShowType    ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectTableField  ( aDataSet : TDataSet );
    procedure SelectResultNL    ( aDataSet : TDataSet );
    procedure SelectResultFR    ( aDataSet : TDataSet );
  end;

  IEDUParameterTypeDataModule = Interface( IFVBFFCBaseDataModule )
  ['{98371C0D-1AB0-4806-878C-109EBC1745BC}']
  end;

  IEDULogHistoryDataModule = Interface( IFVBFFCBaseDataModule )
  ['{08B55208-7CF7-4E1C-9749-C912AF0545CB}']
  end;

  IEDUSchoolyearDataModule = Interface( IFVBFFCBaseDataModule )
  ['{B8083CD0-6780-47E9-9A6E-6B111BB949F4}']
  end;

  IEDUTableFieldDataModule = Interface( IFVBFFCBaseDataModule )
  ['{0775BE87-CCBE-49BB-86B6-C8AD5424AD97}']
  end;

  IEDUQuerywizardDataModule = Interface( IFVBFFCBaseDataModule )
  ['{08B83CA2-A760-4A7F-B284-0559570F9AA9}']
    function GetParameterDataSet: TClientDataSet;
    function GetTreeViewDataSet: TClientDataSet;

    property TreeViewDataSet       : TClientDataSet read GetTreeViewDataSet;
    property ParameterDataSet      : TClientDataSet read GetParameterDataSet;

    procedure FillSelectLookUp( aStringList : TStringList; aTable, aKey, aResult : string );

    procedure FillSqlStatement( aSqlStatement : string; aParamCount : integer = 0 );
    procedure AddParameter( aParamName : string; aParamValue : variant );
    procedure ShowReport;

    function ShowRecordViewForCurrentRecord( aRecordViewMode: TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False): IPPWFrameWorkRecordView;

  end;

  IEDUReportDataModule = Interface( IFVBFFCBaseDataModule )
  ['{4597EBB8-FFDC-45D7-BA4D-96F537DC285D}']
    procedure ChangeReport(aSqlStatement : string;aParamCollection : TObjectList );
  end;

  IEDUImportOverviewModule = Interface (IFVBFFCBaseDataModule)
  ['{72B0D45D-9B1A-404C-83BB-F3302600ADC2}']
    function ProcessSingleRecord(EducationIdentity: Integer): String;
    function GetStatusLookupDataset: TClientDataset;
    function GetDescriptionForStatus(const status: String): String;
  end;

  IEDUDocCenterDataModule = Interface (IFVBFFCBaseDataModule)
  ['{FA2EF567-C347-4273-A080-EFE670DBEACE}']
    procedure SelectInstructor ( aDataSet : TDataSet );
    procedure SelectSession    ( aDataSet : TDataSet );
    procedure ShowInstructor   ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowSession      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUSesFactDatamodule = Interface (IFVBFFCBaseDataModule)
  ['{BC9C359D-5819-462A-AA95-3F5409D7915A}']
    function RetrieveOldMaximum      : integer;
    function WriteRecordsToDatabase  ( aDataSet : TDataSet ) : integer;
    procedure StartNewBookYear       ( aFactNr  : integer );
  end;

  IEDUInvoicingDatamodule = Interface ( IFVBFFCBaseDataModule )
  ['{13A21002-4BC1-4EF5-AB72-8F04CDFECFBB}']
    procedure CollectInvoicingData;
    procedure MarkExported( aFileName : String; aSessionId : Integer; aStartDate, aEndDate: TDateTime; aReference: String;
      var NumberOfSessions: Integer; var NumberOfInvoices: Integer; var NumberOfEnrollments: Integer);
    function GetInvoicePathCurUser: String;
  end;

  IEDUImportDataModule = Interface (IFVBFFCBaseDataModule)
  ['{40CB05E6-7CDA-40BA-9E10-7C1901A91591}']
    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;
    
    procedure XmlEducationSetProcessed(AImportId: Integer; const AMsgType: WideString);

    procedure CloseDefSearchCriteriaDS;
    function GetDefaultSearchCriteria: TClientDataSet;
    procedure GetSessionCandidateForImpRecord(const AImportId: Integer; const AMsgType: string; const AStartDate: TDateTime; var ASessionId: string);
    property DefaultSearchCriteria: TClientDataSet read GetDefaultSearchCriteria;
  end;

  IEDUImport_ExcelDataModule = Interface (IFVBFFCBaseDataModule)
  ['{061F866B-4FBB-440B-B031-49DF3341FD64}']
    procedure XLSSetProcessed(AImportId: Integer);
    procedure XLSSetSession(AImportId: Integer; ASession: Integer);
    procedure XLSAutCreate(AImportId: Integer);
    procedure ProcessValidateKeys;

    function ProcessSingleRecord(EducationIdentity, SessionIdentity: Integer): String;

    procedure CloseDefSearchCriteriaDS;
    function GetDefaultSearchCriteria: TClientDataSet;
    property DefaultSearchCriteria: TClientDataSet read GetDefaultSearchCriteria;
  end;

  IEDUConfirmationReportsDataModule = Interface (IFVBFFCBaseDataModule)
    ['{1837EDAA-1A7E-4FAA-8257-22A46187E1D9}']
    procedure OpenDetails;
    procedure CreateConfirmations( const AMailingList,AInfraMailAttachCompSchool,AInfraMailAttachTeacher:Boolean );
    procedure CreateConfirmationsForPreview( const AMailingList,AInfraMailAttachCompSchool,AInfraMailAttachTeacher:Boolean );
    function GetConfirmationPathCurUser: String;
    function GetProgramNameFromSessionId(SessionId: Integer): String;
    procedure ApplyChanges;
    procedure RecalcRolegroups(SessionId, TotalParticipants, OrganisationId, PartnerWinterId: Integer);
    procedure UpdateContactCompany(AOrganisationId, ANoContact: Integer; const AContactName, AContactEmail: WideString; AConfSubId: Integer; AIsCCEnabled: Integer);
    procedure UpdateContactInfrastruct(AInfrastructure_Id, ANoContact: Integer; const AContactName, AContactEmail: WideString);
    procedure UpdateContactInstructorOrg(AInstructorId, ANoContact: Integer; const AContactName, AContactEmail: WideString);
    function AttestRegistered(aDataSet: TDataSet): integer; 
  end;

  IEDUMailing = interface(IUnknown)
    ['{266CC47B-D7EC-4A2B-B613-52D4BA5708C6}']
    function GetRecipients: TStrings;
    procedure SetRecipients(const Value: TStrings);
    function GetCarbonCopy: TStrings;
    procedure SetCarbonCopy(const Value: TStrings);
    function GetBlindCarbonCopy: TStrings;
    procedure SetBlindCarbonCopy(const Value: TStrings);
    function GetSubject: string;
    procedure SetSubject(const Value: string);
    function GetBody: TStrings;
    procedure SetBody(const Value: TStrings);
    function GetAttachments: TStrings;
    procedure SetAttachments(const Value: TStrings);

    property Recipients: TStrings read GetRecipients write SetRecipients;
    property CarbonCopy: TStrings read GetCarbonCopy write SetCarbonCopy;
    property BlindCarbonCopy: TStrings read GetBlindCarbonCopy write SetBlindCarbonCopy;
    property Subject: string read GetSubject write SetSubject;
    property Body: TStrings read GetBody write SetBody;
    property Attachments: TStrings read GetAttachments write SetAttachments;

    procedure Send;
  end;

  IEDUCooperationDataModule = Interface( IFVBFFCBaseDataModule )
    ['{E588DF58-2662-4F02-9D84-E4DDE49DFA61}']
  end;

  IEDUKMO_PORT_StatusDataModule = Interface( IFVBFFCBaseDataModule )
    ['{1BFB991B-D98C-4324-8B6F-7E9665EDBF58}']
  end;

  IEDUKMO_PortefeuilleDataModule = Interface( IFVBFFCBaseDataModule )
    ['{92CDF2C5-E683-4B76-9A8C-D666C371E1EA}']
    procedure SelectStatus              ( aDataSet : TDataSet );
    procedure SelectStatus2              ( aDataSet : TDataSet );
    procedure SelectStatusFilter        ( aDataSet : TDataSet );
    procedure SelectOrganisation        ( aDataSet : TDataSet );
    procedure ShowStatus                ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure ShowOrganisation          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectOrganisationFilter  ( aDataSet : TDataSet );
    procedure SelectSession        ( aDataSet : TDataSet );
    procedure ShowSession          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectSessionFilter  ( aDataSet : TDataSet );
  end;

  IEDUKMO_PORT_PaymentDataModule = Interface( IFVBFFCBaseDataModule )
    ['{EB07D7AF-FD13-4DFB-926E-5AE9007ABD7F}']
    procedure SelectSession        ( aDataSet : TDataSet );
    procedure ShowSession          ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectSessionFilter  ( aDataSet : TDataSet );
  end;

  IEDUKMO_PORT_Link_Port_PaymentDataModule = Interface( IFVBFFCBaseDataModule )
    ['{BA10E4B9-6583-4555-928C-E6095B07942F}']
    procedure SelectKMO_PORT_Payment    ( aDataSet : TDataSet );
    procedure ShowKMO_PORT_Payment      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectKMO_Portefeuille    ( aDataSet : TDataSet );
    procedure ShowKMO_Portefeuille      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

  IEDUKMO_PORT_Link_Port_SessionDataModule = Interface( IFVBFFCBaseDataModule )
    ['{43B14739-6476-4C07-AF31-ECCC4ADE827D}']
    procedure SelectSession             ( aDataSet : TDataSet );
    procedure ShowSession               ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
    procedure SelectKMO_Portefeuille    ( aDataSet : TDataSet );
    procedure ShowKMO_Portefeuille      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView );
  end;

implementation

end.
