{*****************************************************************************
  This unit contains globally used functions used by the Edutec application

  @Name       Unit_Global
  @Author     ivdbossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  25/02/2008  ivdbossche            Added OpenExplorer
  08/08/2007  ivdbossche            Initial creation of the Unit.
******************************************************************************}

unit Unit_Global;

interface

uses StrUtils, SysUtils, Windows, ShellApi;

function RemoveSpecialChars(const AValue: String): String;
procedure OpenExplorer(const APath: String);

implementation

{*****************************************************************************
  This method will remove special characters which could not be used in paths

  @Name       RemoveSpecialChars
  @author     ivdbossche
  @param      AValue
  @return     AValue without special chars
  @Exception  None
  @See        None
******************************************************************************}
function RemoveSpecialChars(const AValue: String): String;
var
  Value: String;
begin
    Value := AnsiReplaceStr(AValue, '?', '');
    Value := AnsiReplaceStr(Value, ':', '');
    Value := AnsiReplaceStr(Value, '*', '');
    Value := AnsiReplaceStr(Value, '/', '');
    Value := AnsiReplaceStr(Value, '\', '');
    Value := AnsiReplaceStr(Value, '"', '');
    Value := AnsiReplaceStr(Value, '<', '');
    Value := AnsiReplaceStr(Value, '>', '');
    Value := AnsiReplaceStr(Value, '|', '');

    Result := Value;

end;

{*****************************************************************************
  This method will open APath in Windows explorer

  @Name       OpenExplorer
  @author     ivdbossche
  @param      APath
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
procedure OpenExplorer(const APath: String);
begin
  ShellExecute(0, 'explore', PChar(IncludeTrailingPathDelimiter(APath)), nil, nil, SW_SHOWNORMAL);

end;

end.
