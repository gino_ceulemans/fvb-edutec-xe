{*****************************************************************************
  This unit contains constants used by Edutec

  @Name       Unit_const
  @Author     ivdbossche
  @Copyright  (c) 2007 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/07/2007  ivdbossche            Initial creation of the Unit.
******************************************************************************}

unit Unit_const;

interface

const
  REP_CERTIFICATE='certificaat.rpt';
  REP_LETTER_CERTIFICATE='brief_certificaat.rpt';
  REP_CONFIRMATION_COMPANY='bevestiging_bedrijf.rpt';

  REP_CONFIRMATION_EDUTEC_WEEK='bevestiging_edutec_week.rpt';
  REP_CONFIRMATION_WORKERS_CONSTRUCTION='bevestiging_arbeiders_confederatie_bouw.rpt';
  REP_CONFIRMATION_WORKERS_OTHER='bevestiging_arbeiders_andere.rpt';

  {REP_CONFIRMATION_EDUTEC_WEEK='bevestiging_edutec_week.rpt';
  REP_CONFIRMATION_WORKERS_CONSTRUCTION='bevestiging_edutec_week.rpt';
  REP_CONFIRMATION_WORKERS_OTHER='bevestiging_edutec_week.rpt';}


  REP_CONFIRMATION_VCA_SCHOOL='bevestiging_VCA_school.rpt';
  REP_CONFIRMATION_VCA_BEDRIJF='bevestiging_VCA_bedrijf.rpt';
  REP_CONFIRMATION_VCA_INFRASTRUCTURE='bevestiging_VCA_infrastructuur.rpt';
  REP_CONFIRMATION_VCA_EXAMINATOR='bevestiging_VCA_examinator.rpt';
  REP_REPORT_VCA_EXAMINATOR='verslag_VCA_examinator.rpt';
  REP_REPORT_VCA_CONTROLELIJST='controlelijst_VCA.rpt';
  REP_REPORT_VCA_RESULTS='resultatenlijst_VCA.rpt';
  REP_LETTER_VCA_ATTEST='brief_VCA_certificaat.rpt';
  REP_LETTER_VCA_GLOBAL='global_attest_VCA.rpt';

  REP_CONFIRMATION_SCHOOL='bevestiging_school.rpt';
  REP_CONFIRMATION_SCHOOL_STUDENTS='bevestiging_school_leerlingen.rpt';
  REP_CONFIRMATION_ORGANISATION='bevestiging_organisatie.rpt';
  REP_CONFIRMATION_INFRASTRUCTURE='bevestiging_infrastructuur.rpt';
  REP_PRESENCE_LIST='aanwezigheidslijst.rpt';
  REP_EVALUATION_DOC_ENROLMENTS='evaluatie_cursist.rpt';
  REP_EVALUATION_DOC_INSTRUCTORS='evaluatie_lesgever.rpt';
  REP_LETTER_KMO='brief_VCA_certificaat.rpt';

  STATUS_INVOICED='Gefaktureerd';
  STATUS_ATTESTED='Geattesteerd';

  FLDR_Attestatie           = 'attestatie';
  FLDR_Bestelbonnen         = 'bestelbonnen';
  FLDR_Bevestigingsbrieven  = 'bevestigingsbrieven';
  FLDR_Communicatie         = 'communicatie';

  { Status codes used in Edutec DB }
  GEATTESTEERD=10;
  INVOICED=12;
  CANCELLED=2;

resourcestring
  rsTitleRptConfirmationCompany='Bevestiging Bedrijf';
  rsTitleRptConfirmationPartnerWinter='Bevestiging partner winter';
  rsTitleRptConfirmationWinterCC='Bevestiging winter cc';

  rsTitleRptConfirmationInfrastructure='Bevestiging lokaal';
  rsTitleRptConfirmationOrganisation='Bevestiging (Organisatie) lesgever';
  rsTitleRptConfirmationSchool='Bevestiging School';
  rsTitleRptPresenceList='Aanwezigheidslijst';
  rsTitleRptReportExaminer='Verslag_examinator.pdf';
  rsTitleRptControlelijst='Controlelijst.pdf';
  rsTitleRptResultatenlijst='Resultatenlijst.pdf';
  rsTitleEvaluationDocEnrolment='Evaluatie van de opleiding door de cursist.';
  rsTitleEvaluationDocInstructor='Evaluatie van de opleiding door de lesgever.';
  rsPdfPresenceList='Aanwezigheidslijst.pdf';
  rsTitleRptBriefVCA='Brief_VCA_attesten.pdf';
  rsTitleRptGlobalAttestVCA='global_attest_VCA.pdf';

  eUnableToCreateDir='Directory %s kan niet gemaakt worden!!!';
  eUnableToPrintConfirmationReports='Bevestigingsrapporten kunnen voor deze sessie niet geprint worden.' + #10#13 +'%s';
  eErrorInSessionWizard='Er is een fout opgetreden tijdens de sessie wizard.' + #10#13 +'%s';
  eNoInstructors='Voor de gekozen sessie zijn er nog geen lesgevers ingebracht.  Gelieve dit eerst te doen.';
  eNoEnrolments='Er zijn nog geen inschrijvingen voor deze sessie.';
  eNoInfrastructureSpecified='Er is nog geen infrastructuur bepaald voor deze sessie!';
  eNoConfirmationSelected='Er zal geen bevestigingsrapport worden afgedrukt aangezien er niks is geselecteerd.';
  eNoRecord2Export='De door u geselecteerde sessies zijn ondertussen door iemand anders gefaktureerd.';

implementation

end.
