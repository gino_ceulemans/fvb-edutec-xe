inherited dtmCenRoom: TdtmCenRoom
  KeyFields = 'F_ROOM_ID'
  ListViewClass = 'TfrmCenRoom_List'
  RecordViewClass = 'TfrmCenRoom_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Height = 327
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROOM_ID, '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_DESCRIPTION_NL, '
      '  F_DESCRIPTION_FR, '
      '  F_CUBIC_METRES, '
      '  F_HEIGHT, '
      '  F_EQUIPMENT, '
      '  F_TECH_FICHE, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_ROOM_NAME, '
      '  F_INFRASTRUCTURE_NAME '
      'FROM '
      '  V_CEN_ROOM'
      '')
    object qryListF_ROOM_ID: TIntegerField
      FieldName = 'F_ROOM_ID'
    end
    object qryListF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
    end
    object qryListF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryListF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryListF_DESCRIPTION_NL: TStringField
      FieldName = 'F_DESCRIPTION_NL'
      Size = 128
    end
    object qryListF_DESCRIPTION_FR: TStringField
      FieldName = 'F_DESCRIPTION_FR'
      Size = 128
    end
    object qryListF_CUBIC_METRES: TSmallintField
      FieldName = 'F_CUBIC_METRES'
    end
    object qryListF_HEIGHT: TSmallintField
      FieldName = 'F_HEIGHT'
    end
    object qryListF_EQUIPMENT: TMemoField
      FieldName = 'F_EQUIPMENT'
      BlobType = ftMemo
    end
    object qryListF_TECH_FICHE: TStringField
      FieldName = 'F_TECH_FICHE'
      Size = 64
    end
    object qryListF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
    end
    object qryListF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object qryListF_ROOM_NAME: TStringField
      FieldName = 'F_ROOM_NAME'
      ReadOnly = True
      Size = 64
    end
    object qryListF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Size = 64
    end
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT '
      '  F_ROOM_ID, '
      '  F_INFRASTRUCTURE_ID, '
      '  F_NAME_NL, '
      '  F_NAME_FR, '
      '  F_DESCRIPTION_NL, '
      '  F_DESCRIPTION_FR, '
      '  F_CUBIC_METRES, '
      '  F_HEIGHT, '
      '  F_EQUIPMENT, '
      '  F_TECH_FICHE, '
      '  F_ACTIVE, '
      '  F_END_DATE, '
      '  F_ROOM_NAME, '
      '  F_INFRASTRUCTURE_NAME '
      'FROM '
      '  V_CEN_ROOM'
      '')
    object qryRecordF_ROOM_ID: TIntegerField
      FieldName = 'F_ROOM_ID'
    end
    object qryRecordF_INFRASTRUCTURE_ID: TIntegerField
      FieldName = 'F_INFRASTRUCTURE_ID'
    end
    object qryRecordF_NAME_NL: TStringField
      FieldName = 'F_NAME_NL'
      Size = 64
    end
    object qryRecordF_NAME_FR: TStringField
      FieldName = 'F_NAME_FR'
      Size = 64
    end
    object qryRecordF_DESCRIPTION_NL: TStringField
      FieldName = 'F_DESCRIPTION_NL'
      Size = 128
    end
    object qryRecordF_DESCRIPTION_FR: TStringField
      FieldName = 'F_DESCRIPTION_FR'
      Size = 128
    end
    object qryRecordF_CUBIC_METRES: TSmallintField
      FieldName = 'F_CUBIC_METRES'
    end
    object qryRecordF_HEIGHT: TSmallintField
      FieldName = 'F_HEIGHT'
    end
    object qryRecordF_EQUIPMENT: TMemoField
      FieldName = 'F_EQUIPMENT'
      BlobType = ftMemo
    end
    object qryRecordF_TECH_FICHE: TStringField
      FieldName = 'F_TECH_FICHE'
      Size = 64
    end
    object qryRecordF_ACTIVE: TBooleanField
      FieldName = 'F_ACTIVE'
    end
    object qryRecordF_END_DATE: TDateTimeField
      FieldName = 'F_END_DATE'
    end
    object qryRecordF_ROOM_NAME: TStringField
      FieldName = 'F_ROOM_NAME'
      ReadOnly = True
      Size = 64
    end
    object qryRecordF_INFRASTRUCTURE_NAME: TStringField
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Size = 64
    end
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_ROOM_ID: TIntegerField
      DisplayLabel = 'Lokaal ( ID )'
      FieldName = 'F_ROOM_ID'
    end
    object cdsListF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Infrastructuur ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
      Visible = False
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 64
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 64
    end
    object cdsListF_DESCRIPTION_NL: TStringField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_DESCRIPTION_NL'
      Visible = False
      Size = 128
    end
    object cdsListF_DESCRIPTION_FR: TStringField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_DESCRIPTION_FR'
      Visible = False
      Size = 128
    end
    object cdsListF_CUBIC_METRES: TSmallintField
      DisplayLabel = 'Kubieke Meters'
      FieldName = 'F_CUBIC_METRES'
      Visible = False
    end
    object cdsListF_HEIGHT: TSmallintField
      DisplayLabel = 'Hoogte'
      FieldName = 'F_HEIGHT'
      Visible = False
    end
    object cdsListF_EQUIPMENT: TMemoField
      DisplayLabel = 'Uitrusting'
      FieldName = 'F_EQUIPMENT'
      Visible = False
      BlobType = ftMemo
    end
    object cdsListF_TECH_FICHE: TStringField
      DisplayLabel = 'Technische Fiche'
      FieldName = 'F_TECH_FICHE'
      Visible = False
      Size = 64
    end
    object cdsListF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsListF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsListF_ROOM_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_ROOM_NAME'
      Size = 64
    end
    object cdsListF_INFRASTRUCTURE_NAME: TStringField
      DisplayLabel = 'Infrastructuur'
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_ROOM_ID: TIntegerField
      DisplayLabel = 'Lokaal ( ID )'
      FieldName = 'F_ROOM_ID'
      Required = True
    end
    object cdsRecordF_INFRASTRUCTURE_ID: TIntegerField
      DisplayLabel = 'Infrastructuur ( ID )'
      FieldName = 'F_INFRASTRUCTURE_ID'
      Required = True
      Visible = False
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 64
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 64
    end
    object cdsRecordF_DESCRIPTION_NL: TStringField
      DisplayLabel = 'Omschrijving ( NL )'
      FieldName = 'F_DESCRIPTION_NL'
      Size = 128
    end
    object cdsRecordF_DESCRIPTION_FR: TStringField
      DisplayLabel = 'Omschrijving ( FR )'
      FieldName = 'F_DESCRIPTION_FR'
      Size = 128
    end
    object cdsRecordF_CUBIC_METRES: TSmallintField
      DisplayLabel = 'Kubieke Meters'
      FieldName = 'F_CUBIC_METRES'
    end
    object cdsRecordF_HEIGHT: TSmallintField
      DisplayLabel = 'Hoogte'
      FieldName = 'F_HEIGHT'
    end
    object cdsRecordF_EQUIPMENT: TMemoField
      DisplayLabel = 'Uitrusting'
      FieldName = 'F_EQUIPMENT'
      BlobType = ftMemo
    end
    object cdsRecordF_TECH_FICHE: TStringField
      DisplayLabel = 'Technische Fiche'
      FieldName = 'F_TECH_FICHE'
      Size = 64
    end
    object cdsRecordF_ACTIVE: TBooleanField
      DisplayLabel = 'Actief'
      FieldName = 'F_ACTIVE'
    end
    object cdsRecordF_END_DATE: TDateTimeField
      DisplayLabel = 'Eind Datum'
      FieldName = 'F_END_DATE'
    end
    object cdsRecordF_ROOM_NAME: TStringField
      DisplayLabel = 'Naam'
      FieldName = 'F_ROOM_NAME'
      ReadOnly = True
      Required = True
      Size = 64
    end
    object cdsRecordF_INFRASTRUCTURE_NAME: TStringField
      DisplayLabel = 'Infrastructuur'
      FieldName = 'F_INFRASTRUCTURE_NAME'
      Required = True
      Size = 64
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmCenRoom'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmCenRoom'
  end
end
