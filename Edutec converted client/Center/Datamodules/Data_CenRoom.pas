{*****************************************************************************
  This DataModule will be used for the maintenance of T_CEN_ROOM.

  @Name       Data_CenRoom
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Data_CenRoom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Data_EDUDataModule, DB,
  MConnect, Unit_FVBFFCDBComponents, Unit_PPWFrameWorkComponents, Provider,
  ADODB, Unit_FVBFFCInterfaces, unit_EdutecInterfaces, Datasnap.DSConnect;

type
  TdtmCenRoom = class(TEDUDataModule, IEDUCenRoomDataModule)
    qryListF_ROOM_ID: TIntegerField;
    qryListF_INFRASTRUCTURE_ID: TIntegerField;
    qryListF_NAME_NL: TStringField;
    qryListF_NAME_FR: TStringField;
    qryListF_DESCRIPTION_NL: TStringField;
    qryListF_DESCRIPTION_FR: TStringField;
    qryListF_CUBIC_METRES: TSmallintField;
    qryListF_HEIGHT: TSmallintField;
    qryListF_EQUIPMENT: TMemoField;
    qryListF_TECH_FICHE: TStringField;
    qryListF_ACTIVE: TBooleanField;
    qryListF_END_DATE: TDateTimeField;
    qryListF_ROOM_NAME: TStringField;
    qryListF_INFRASTRUCTURE_NAME: TStringField;
    qryRecordF_ROOM_ID: TIntegerField;
    qryRecordF_INFRASTRUCTURE_ID: TIntegerField;
    qryRecordF_NAME_NL: TStringField;
    qryRecordF_NAME_FR: TStringField;
    qryRecordF_DESCRIPTION_NL: TStringField;
    qryRecordF_DESCRIPTION_FR: TStringField;
    qryRecordF_CUBIC_METRES: TSmallintField;
    qryRecordF_HEIGHT: TSmallintField;
    qryRecordF_EQUIPMENT: TMemoField;
    qryRecordF_TECH_FICHE: TStringField;
    qryRecordF_ACTIVE: TBooleanField;
    qryRecordF_END_DATE: TDateTimeField;
    qryRecordF_ROOM_NAME: TStringField;
    qryRecordF_INFRASTRUCTURE_NAME: TStringField;
    cdsListF_ROOM_ID: TIntegerField;
    cdsListF_INFRASTRUCTURE_ID: TIntegerField;
    cdsListF_NAME_NL: TStringField;
    cdsListF_NAME_FR: TStringField;
    cdsListF_DESCRIPTION_NL: TStringField;
    cdsListF_DESCRIPTION_FR: TStringField;
    cdsListF_CUBIC_METRES: TSmallintField;
    cdsListF_HEIGHT: TSmallintField;
    cdsListF_EQUIPMENT: TMemoField;
    cdsListF_TECH_FICHE: TStringField;
    cdsListF_ACTIVE: TBooleanField;
    cdsListF_END_DATE: TDateTimeField;
    cdsListF_ROOM_NAME: TStringField;
    cdsListF_INFRASTRUCTURE_NAME: TStringField;
    cdsRecordF_ROOM_ID: TIntegerField;
    cdsRecordF_INFRASTRUCTURE_ID: TIntegerField;
    cdsRecordF_NAME_NL: TStringField;
    cdsRecordF_NAME_FR: TStringField;
    cdsRecordF_DESCRIPTION_NL: TStringField;
    cdsRecordF_DESCRIPTION_FR: TStringField;
    cdsRecordF_CUBIC_METRES: TSmallintField;
    cdsRecordF_HEIGHT: TSmallintField;
    cdsRecordF_EQUIPMENT: TMemoField;
    cdsRecordF_TECH_FICHE: TStringField;
    cdsRecordF_ACTIVE: TBooleanField;
    cdsRecordF_END_DATE: TDateTimeField;
    cdsRecordF_ROOM_NAME: TStringField;
    cdsRecordF_INFRASTRUCTURE_NAME: TStringField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
  private
    { Private declarations }
  protected
    procedure SelectCenInfrastructure       ( aDataSet : TDataSet ); virtual;

    procedure ShowCenInfrastructure         ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
  end;

implementation

uses Data_CenInfrastructure;

{$R *.dfm}


{ TdtmCenRoom }

{*****************************************************************************
  This class procedure will be used to select one or more CenInfrastructure Records.

  @Name       TdtmCenRoom.SelectCenInfrastructure
  @author     slesage
  @param      aDataSet   The dataset in which we should copy some information
                         fromt the Selected Records.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenRoom.SelectCenInfrastructure(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_INFRASTRUCTURE_ID' );
  aCopyTo  := critInfrastructureToCenRoom;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := 'F_INFRASTRUCTURE_ID <> ' + aIDField.AsString;
  end;

  TdtmCenInfrastructure.SelectCenInfrastructure( aDataSet, aWhere, False, aCopyTo );
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Centre Room
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmCenRoom.ShowCenInfrastructure
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenRoom.ShowCenInfrastructure(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critInfrastructureToCenRoom;
  aIDField := aDataSet.FindField( 'F_INFRASTRUCTURE_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmCenInfrastructure.ShowCenInfrastructure( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;


{*****************************************************************************
  This method will be executed when the Primary Key Fields are initialised.

  @Name       TdtmCenRoom.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which Primary Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenRoom.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ROOM_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This method will be used to initialise some Additional Fields on the DataSet.

  @Name       TdtmCenRoom.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenRoom.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_ACTIVE' ).AsBoolean := True;
end;

{*****************************************************************************
  This method will be executed when the Foreign Key Fields are initialised.

  @Name       TdtmCenRoom.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Foreign Key fields should be
                         initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmCenRoom.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;

  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUCenInfrastructureDataModule ) ) then
    begin
      CopyCenInfrastructureFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critInfrastructureToCenRoom );
    end;
  end;
end;

end.
