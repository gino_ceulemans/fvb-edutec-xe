{*****************************************************************************
  This unit contains the form that will be used for the maintenance of a
  T_CEN_ROOM records.


  @Name       Form_CenRoom_Record
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_CenRoom_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Form_EDURecordView, Menus,
  cxLookAndFeelPainters, ActnList, Unit_FVBFFCComponents, DB,
  Unit_FVBFFCDBComponents, cxControls, cxSplitter, Unit_FVBFFCDevExpress,
  dxNavBarCollns, dxNavBarBase, dxNavBar, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxButtonEdit, cxDBEdit,
  cxDropDownEdit, cxCalendar, cxCheckBox, cxMemo, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxDBLabel, cxContainer, cxEdit, cxLabel, Unit_PPWFrameWorkInterfaces;

type
  TfrmCenRoom_Record = class(TEDURecordView)
    cxlblF_ROOM_ID1: TFVBFFCLabel;
    cxdblblF_ROOM_ID1: TFVBFFCDBLabel;
    cxlblF_ROOM_NAME1: TFVBFFCLabel;
    cxdblblF_ROOM_NAME1: TFVBFFCDBLabel;
    cxlblF_ROOM_ID2: TFVBFFCLabel;
    cxdbseF_ROOM_ID1: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL1: TFVBFFCLabel;
    cxdbteF_NAME_NL1: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR1: TFVBFFCLabel;
    cxdbteF_NAME_FR1: TFVBFFCDBTextEdit;
    cxlblF_DESCRIPTION_NL1: TFVBFFCLabel;
    cxdbteF_DESCRIPTION_NL1: TFVBFFCDBTextEdit;
    cxlblF_DESCRIPTION_FR1: TFVBFFCLabel;
    cxdbteF_DESCRIPTION_FR1: TFVBFFCDBTextEdit;
    cxlblF_CUBIC_METRES1: TFVBFFCLabel;
    cxdbseF_CUBIC_METRES1: TFVBFFCDBSpinEdit;
    cxlblF_HEIGHT1: TFVBFFCLabel;
    cxdbseF_HEIGHT1: TFVBFFCDBSpinEdit;
    cxlblF_EQUIPMENT1: TFVBFFCLabel;
    cxdbmmoF_EQUIPMENT1: TFVBFFCDBMemo;
    cxlblF_TECH_FICHE1: TFVBFFCLabel;
    cxdbteF_TECH_FICHE1: TFVBFFCDBTextEdit;
    cxlblF_ACTIVE1: TFVBFFCLabel;
    cxdbcbF_ACTIVE1: TFVBFFCDBCheckBox;
    cxlblF_END_DATE1: TFVBFFCLabel;
    cxdbdeF_END_DATE1: TFVBFFCDBDateEdit;
    cxlblF_INFRASTRUCTURE_NAME1: TFVBFFCLabel;
    cxdbbeF_INFRASTRUCTURE_NAME1: TFVBFFCDBButtonEdit;
    procedure cxdbbeF_INFRASTRUCTURE_NAME1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_CenRoom, Data_EduMainClient, Unit_FVBFFCInterfaces, unit_EdutecInterfaces;

{ TfrmCenRoom_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmCenRoom_Record.GetDetailName
  @author     PIFWizard Generated
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmCenRoom_Record.GetDetailName: String;
begin
  Result := cxdblblF_ROOM_NAME1.Caption;
end;

procedure TfrmCenRoom_Record.cxdbbeF_INFRASTRUCTURE_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUCenRoomDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUCenRoomDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowCenInfrastructure( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectCenInfrastructure( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmCenRoom_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;

  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUCenInfrastructureDataModule ) ) then
    begin
      cxdbbeF_INFRASTRUCTURE_NAME1.RepositoryItem := dtmEDUMainClient.cxeriShowSelectLookupReadOnly;
      ActivateControl ( cxdbteF_NAME_NL1 );
    end;
  end;
end;

end.
