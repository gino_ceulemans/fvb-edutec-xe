{*****************************************************************************
  This Unit contains the form that will be used to display a list of
  T_CEN_ROOM records.


  @Name       Form_CenRoom_List
  @Author     PIFWizard Generated
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/10/2005   PIFWizard Generated  Initial creation of the Unit.
******************************************************************************}

unit Form_CenRoom_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EDUListView, Menus, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar,
  dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls;

type
  TfrmCenRoom_List = class(TEDUListView)
    cxgrdtblvListF_ROOM_ID: TcxGridDBColumn;
    cxgrdtblvListF_INFRASTRUCTURE_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME_NL: TcxGridDBColumn;
    cxgrdtblvListF_NAME_FR: TcxGridDBColumn;
    cxgrdtblvListF_DESCRIPTION_NL: TcxGridDBColumn;
    cxgrdtblvListF_DESCRIPTION_FR: TcxGridDBColumn;
    cxgrdtblvListF_CUBIC_METRES: TcxGridDBColumn;
    cxgrdtblvListF_HEIGHT: TcxGridDBColumn;
    cxgrdtblvListF_EQUIPMENT: TcxGridDBColumn;
    cxgrdtblvListF_TECH_FICHE: TcxGridDBColumn;
    cxgrdtblvListF_ACTIVE: TcxGridDBColumn;
    cxgrdtblvListF_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_ROOM_NAME: TcxGridDBColumn;
    cxgrdtblvListF_INFRASTRUCTURE_NAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_CenRoom, Data_EduMainClient;

end.