inherited frmCenRoom_Record: TfrmCenRoom_Record
  Left = 299
  Top = 253
  Width = 954
  Height = 576
  ActiveControl = cxdbbeF_INFRASTRUCTURE_NAME1
  Caption = 'Education Center Room'
  Constraints.MinHeight = 576
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 517
    Width = 946
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 610
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 946
    Height = 517
    inherited pnlRecord: TFVBFFCPanel
      Width = 784
      Height = 517
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 784
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 784
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 784
        object cxlblF_ROOM_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCenRoom_Record.F_ROOM_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Lokaal ( ID )'
          FocusControl = cxdblblF_ROOM_ID1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ROOM_ID1: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCenRoom_Record.F_ROOM_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ROOM_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object cxlblF_ROOM_NAME1: TFVBFFCLabel
          Left = 144
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmCenRoom_Record.F_ROOM_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Naam'
          FocusControl = cxdblblF_ROOM_NAME1
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_ROOM_NAME1: TFVBFFCDBLabel
          Left = 144
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmCenRoom_Record.F_ROOM_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_ROOM_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 473
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 784
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 513
        Width = 784
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 784
        Height = 443
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 784
        end
        inherited sbxMain: TScrollBox
          Width = 784
          Height = 418
          object cxlblF_ROOM_ID2: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_ROOM_ID'
            Caption = 'Lokaal ( ID )'
            FocusControl = cxdbseF_ROOM_ID1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_ROOM_ID1: TFVBFFCDBSpinEdit
            Left = 144
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_ROOM_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_ROOM_ID'
            DataBinding.DataSource = srcMain
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL1: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_NAME_NL'
            Caption = 'Naam ( NL )'
            FocusControl = cxdbteF_NAME_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL1: TFVBFFCDBTextEdit
            Left = 144
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 5
            Width = 460
          end
          object cxlblF_NAME_FR1: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_NAME_FR'
            Caption = 'Naam ( FR )'
            FocusControl = cxdbteF_NAME_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR1: TFVBFFCDBTextEdit
            Left = 144
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 7
            Width = 460
          end
          object cxlblF_DESCRIPTION_NL1: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_DESCRIPTION_NL'
            Caption = 'Omschrijving ( NL )'
            FocusControl = cxdbteF_DESCRIPTION_NL1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_DESCRIPTION_NL1: TFVBFFCDBTextEdit
            Left = 144
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_DESCRIPTION_NL'
            DataBinding.DataField = 'F_DESCRIPTION_NL'
            DataBinding.DataSource = srcMain
            TabOrder = 9
            Width = 460
          end
          object cxlblF_DESCRIPTION_FR1: TFVBFFCLabel
            Left = 8
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_DESCRIPTION_FR'
            Caption = 'Omschrijving ( FR )'
            FocusControl = cxdbteF_DESCRIPTION_FR1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_DESCRIPTION_FR1: TFVBFFCDBTextEdit
            Left = 144
            Top = 128
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_DESCRIPTION_FR'
            DataBinding.DataField = 'F_DESCRIPTION_FR'
            DataBinding.DataSource = srcMain
            TabOrder = 11
            Width = 460
          end
          object cxlblF_CUBIC_METRES1: TFVBFFCLabel
            Left = 8
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_CUBIC_METRES'
            Caption = 'Kubieke Meters'
            FocusControl = cxdbseF_CUBIC_METRES1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_CUBIC_METRES1: TFVBFFCDBSpinEdit
            Left = 144
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_CUBIC_METRES'
            DataBinding.DataField = 'F_CUBIC_METRES'
            DataBinding.DataSource = srcMain
            TabOrder = 13
            Width = 160
          end
          object cxlblF_HEIGHT1: TFVBFFCLabel
            Left = 360
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_HEIGHT'
            Caption = 'Hoogte'
            FocusControl = cxdbseF_HEIGHT1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_HEIGHT1: TFVBFFCDBSpinEdit
            Left = 440
            Top = 152
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_HEIGHT'
            DataBinding.DataField = 'F_HEIGHT'
            DataBinding.DataSource = srcMain
            TabOrder = 15
            Width = 160
          end
          object cxlblF_EQUIPMENT1: TFVBFFCLabel
            Left = 8
            Top = 224
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_EQUIPMENT'
            Caption = 'Uitrusting'
            FocusControl = cxdbmmoF_EQUIPMENT1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_EQUIPMENT1: TFVBFFCDBMemo
            Left = 144
            Top = 224
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_EQUIPMENT'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_EQUIPMENT'
            DataBinding.DataSource = srcMain
            TabOrder = 23
            Height = 160
            Width = 460
          end
          object cxlblF_TECH_FICHE1: TFVBFFCLabel
            Left = 8
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_TECH_FICHE'
            Caption = 'Technische Fiche'
            FocusControl = cxdbteF_TECH_FICHE1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_TECH_FICHE1: TFVBFFCDBTextEdit
            Left = 144
            Top = 200
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_TECH_FICHE'
            DataBinding.DataField = 'F_TECH_FICHE'
            DataBinding.DataSource = srcMain
            TabOrder = 18
            Width = 460
          end
          object cxlblF_ACTIVE1: TFVBFFCLabel
            Left = 8
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_ACTIVE'
            Caption = 'Actief'
            FocusControl = cxdbcbF_ACTIVE1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbcbF_ACTIVE1: TFVBFFCDBCheckBox
            Left = 144
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_ACTIVE'
            Caption = 'cxdbcbF_ACTIVE1'
            DataBinding.DataField = 'F_ACTIVE'
            DataBinding.DataSource = srcMain
            ParentColor = False
            Properties.NullStyle = nssInactive
            Properties.ReadOnly = True
            TabOrder = 16
            Width = 24
          end
          object cxlblF_END_DATE1: TFVBFFCLabel
            Left = 360
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_END_DATE'
            Caption = 'Eind Datum'
            FocusControl = cxdbdeF_END_DATE1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_END_DATE1: TFVBFFCDBDateEdit
            Left = 440
            Top = 176
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_END_DATE'
            DataBinding.DataField = 'F_END_DATE'
            DataBinding.DataSource = srcMain
            TabOrder = 17
            Width = 160
          end
          object cxlblF_INFRASTRUCTURE_NAME1: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_INFRASTRUCTURE_NAME'
            Caption = 'Infrastructuur'
            FocusControl = cxdbbeF_INFRASTRUCTURE_NAME1
            ParentColor = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_INFRASTRUCTURE_NAME1: TFVBFFCDBButtonEdit
            Left = 144
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmCenRoom_Record.F_INFRASTRUCTURE_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_INFRASTRUCTURE_NAME'
            DataBinding.DataSource = srcMain
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_INFRASTRUCTURE_NAME1PropertiesButtonClick
            TabOrder = 3
            Width = 460
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 942
      Height = 517
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 517
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 517
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmCenRoom.cdsRecord
  end
end
