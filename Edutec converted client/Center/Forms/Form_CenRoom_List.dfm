inherited frmCenRoom_List: TfrmCenRoom_List
  Caption = 'Education Center Room'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlList: TFVBFFCPanel
    inherited cxgrdList: TcxGrid
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_ROOM_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROOM_ID'
          Width = 101
        end
        object cxgrdtblvListF_INFRASTRUCTURE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_ID'
          Visible = False
        end
        object cxgrdtblvListF_INFRASTRUCTURE_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_INFRASTRUCTURE_NAME'
        end
        object cxgrdtblvListF_ROOM_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ROOM_NAME'
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
        end
        object cxgrdtblvListF_DESCRIPTION_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_DESCRIPTION_NL'
          Visible = False
        end
        object cxgrdtblvListF_DESCRIPTION_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_DESCRIPTION_FR'
          Visible = False
        end
        object cxgrdtblvListF_CUBIC_METRES: TcxGridDBColumn
          DataBinding.FieldName = 'F_CUBIC_METRES'
          Visible = False
        end
        object cxgrdtblvListF_HEIGHT: TcxGridDBColumn
          DataBinding.FieldName = 'F_HEIGHT'
          Visible = False
        end
        object cxgrdtblvListF_EQUIPMENT: TcxGridDBColumn
          DataBinding.FieldName = 'F_EQUIPMENT'
          Visible = False
        end
        object cxgrdtblvListF_TECH_FICHE: TcxGridDBColumn
          DataBinding.FieldName = 'F_TECH_FICHE'
          Visible = False
        end
        object cxgrdtblvListF_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_END_DATE'
          RepositoryItem = dtmEDUMainClient.cxeriDate
        end
        object cxgrdtblvListF_ACTIVE: TcxGridDBColumn
          DataBinding.FieldName = 'F_ACTIVE'
          Width = 38
        end
      end
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Visible = False
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmCenRoom.cdsList
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
