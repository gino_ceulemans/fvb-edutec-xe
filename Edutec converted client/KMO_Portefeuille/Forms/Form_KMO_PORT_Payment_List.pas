unit Form_KMO_PORT_Payment_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxDropDownEdit, cxCalendar, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxContainer, cxLabel, Menus, ActnList,
  Unit_FVBFFCComponents, Unit_PPWFrameWorkActions, Grids, DBGrids,
  cxCheckBox, cxCalc;

type
  TfrmKMO_PORT_Payment_List = class(TEDUListView)
    cxlblF_INVOICE_NR2: TFVBFFCLabel;
    cxdbteF_INVOICE_NR1: TFVBFFCDBTextEdit;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    prdDialog: TPrintDialog;
    cxlblF_SESSION_CODE: TFVBFFCLabel;
    cxdbbeF_SESSION_CODE: TFVBFFCDBButtonEdit;
    cxdbteF_SESSION_NAME1: TFVBFFCDBTextEdit;
    cxlblF_SESSION_START_DATE1: TFVBFFCLabel;
    cxdbdeF_SESSION_START_DATE1: TFVBFFCDBDateEdit;
    cxlblF_SESSION_END_DATE1: TFVBFFCLabel;
    cxdbdeF_SESSION_END_DATE1: TFVBFFCDBDateEdit;
    cxlblF_DATE_PAYED_FROM: TFVBFFCLabel;
    cxdbdeF_DATE_PAYED_FROM: TFVBFFCDBDateEdit;
    cxlblF_DATE_PAYED_TO: TFVBFFCLabel;
    cxdbdeF_DATE_PAYED_TO: TFVBFFCDBDateEdit;
    cxgrdtblvListF_KMO_PORT_PAYMENT_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_INVOICE_NR: TcxGridDBColumn;
    cxgrdtblvListF_AMOUNT_PAYED: TcxGridDBColumn;
    cxgrdtblvListF_DATE_PAYED: TcxGridDBColumn;
    cxgrdtblvListF_REMARK: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListSUM_AMOUNT_LINKED: TcxGridDBColumn;
    procedure cxgrdtblvListStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FVBFFCListViewShow(Sender: TObject);
    procedure FVBFFCListViewCreate(Sender: TObject);
    procedure FVBFFCListViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure cxdbbeF_SESSION_CODEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
  private
    { Private declarations }
  protected
    procedure OnCanFocusRecord(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; var AAllow: Boolean);
    procedure StoreActiveViewToIni; override;
    procedure RestoreActiveViewFromIni; override;
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;


implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_KMO_PORT_Payment, Data_EduMainClient, Form_FVBFFCBaseListView,
  Unit_FVBFFCInterfaces, unit_EdutecInterfaces, unit_PPWFrameworkClasses,
  unit_Const, printers, Unit_FVBFFCUtils, Unit_Global;

{ TfrmKMO_PORT_Payment_List }


{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmKMO_PORT_Payment_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmKMO_PORT_Payment_List.GetFilterString: String;
var
  aFilterString : String;
  aDateString   : String;
  startdatefilter, enddatefilter: TDatetime;
begin
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_INVOICE_NR' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_INVOICE_NR', FSearchCriteriaDataSet.FieldByName( 'F_INVOICE_NR' ).AsString );
  end;

  startdatefilter := EncodeDate (1900, 1, 1);
  enddatefilter   := EncodeDate (2039, 12,31);
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_DATE_PAYED_FROM' ).IsNull ) then
    startdatefilter := FSearchCriteriaDataSet.FieldByName( 'F_DATE_PAYED_FROM' ).AsDateTime;
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_DATE_PAYED_TO' ).IsNull ) then
    enddatefilter := FSearchCriteriaDataSet.FieldByName( 'F_DATE_PAYED_TO' ).AsDateTime;
  AddDateBetweenCondition (aDateString, 'F_DATE_PAYED', startdatefilter, enddatefilter);
  AddFilterCondition( aFilterString, aDateString, 'AND' );

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SESSION_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_SESSION_ID', FSearchCriteriaDataSet.FieldByName( 'F_SESSION_ID' ).AsInteger );
  end;

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SESSION_NAME' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_SESSION_NAME', FSearchCriteriaDataSet.FieldByName( 'F_SESSION_NAME' ).AsString );
  end;

  startdatefilter := EncodeDate (1900, 1, 1);
  enddatefilter   := EncodeDate (2039, 12,31);
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SESSION_START_DATE' ).IsNull ) then
    startdatefilter := FSearchCriteriaDataSet.FieldByName( 'F_SESSION_START_DATE' ).AsDateTime;
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_SESSION_END_DATE' ).IsNull ) then
    enddatefilter := FSearchCriteriaDataSet.FieldByName( 'F_SESSION_END_DATE' ).AsDateTime;
  AddDateBetweenCondition (aDateString, 'F_SESSION_START_DATE', startdatefilter, enddatefilter);
  AddFilterCondition( aFilterString, aDateString, 'AND' );

  Result := aFilterString;
end;

procedure TfrmKMO_PORT_Payment_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;
    with cxgrdtblvList.Controller do
    if (FocusedRecord <> nil) and FocusedRecord.Selected then
        FocusedRecord.Selected := False;
end;

procedure TfrmKMO_PORT_Payment_List.FVBFFCListViewCreate(Sender: TObject);
begin
  inherited;
  // Add OnCanFocusRecord event to grid
  cxgrdtblvList.OnCanFocusRecord := OnCanFocusRecord;
  cxbtnEDUButton1.Enabled := False;
end;

procedure TfrmKMO_PORT_Payment_List.OnCanFocusRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  AAllow := True;
  cxbtnEDUButton1.Enabled := True;
end;

procedure TfrmKMO_PORT_Payment_List.RestoreActiveViewFromIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].RestoreFromIniFile( LocalSettingsIniFileName, True, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;

procedure TfrmKMO_PORT_Payment_List.StoreActiveViewToIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].StoreToIniFile( LocalSettingsIniFileName, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;


procedure TfrmKMO_PORT_Payment_List.FVBFFCListViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

    // Make sure that changes made to the grid will be applied to the database.
    if (srcMain.DataSet.Active) then
    begin
        if srcMain.DataSet.State in [dsInsert, dsEdit] then
          srcMain.DataSet.Post;

    end;
end;

procedure TfrmKMO_PORT_Payment_List.cxdbbeF_SESSION_CODEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PORT_PaymentDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PORT_PaymentDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectSessionFilter( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmKMO_PORT_Payment_List.cxgrdtblvListStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if not (aRecord.Expandable) then
  begin
    // kleur
    {
    aStyle := dtmEDUMainClient.cxsKMOPortefeuille;
    aStyle.Color := aRecord.Values[ cxgrdtblvListF_BACKGROUNDCOLOR.Index ];
    aStyle.TextColor := aRecord.Values[ cxgrdtblvListF_FOREGROUNDCOLOR.Index ];
    }
  end;
end;

end.
