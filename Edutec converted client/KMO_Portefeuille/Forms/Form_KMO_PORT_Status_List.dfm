inherited frmKMO_PORT_Status_List: TfrmKMO_PORT_Status_List
  Left = 389
  Top = 282
  Width = 769
  Caption = 'Statussen KMO Portefeuille'
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Width = 761
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 545
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 657
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 753
    inherited cxgrdList: TcxGrid
      Width = 753
      inherited cxgrdtblvList: TcxGridDBTableView
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_KMO_PORT_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORT_STATUS_ID'
          Width = 100
        end
        object cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          Width = 600
        end
        object cxgrdtblvListF_FOREGROUNDCOLOR: TcxGridDBColumn
          DataBinding.FieldName = 'F_FOREGROUNDCOLOR'
          Visible = False
        end
        object cxgrdtblvListF_BACKGROUNDCOLOR: TcxGridDBColumn
          DataBinding.FieldName = 'F_BACKGROUNDCOLOR'
          Visible = False
        end
        object cxgrdtblvListF_NAME_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_NL'
          Visible = False
        end
        object cxgrdtblvListF_NAME_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_NAME_FR'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Width = 753
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Width = 753
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 753
      Visible = False
      inherited pnlSearchCriteriaButtons: TPanel
        Width = 751
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 583
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 671
        end
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 761
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Width = 761
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 757
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
end
