unit Form_KMO_Portefeuille_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxDropDownEdit, cxCalendar, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxContainer, cxLabel, Menus, ActnList,
  Unit_FVBFFCComponents, Unit_PPWFrameWorkActions, Grids, DBGrids,
  cxCheckBox, cxCalc;

type
  TfrmKMO_Portefeuille_List = class(TEDUListView)
    cxlblF_PROJECT_NR2: TFVBFFCLabel;
    cxdbteF_PROJECT_NR1: TFVBFFCDBTextEdit;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    cxdbbeF_STATUS_NAME1: TFVBFFCDBButtonEdit;
    prdDialog: TPrintDialog;
    cxlblF_ORG_NAME: TFVBFFCLabel;
    cxdbbeF_ORG_NAME: TFVBFFCDBButtonEdit;
    cxgrdtblvListF_KMO_PORTEFEUILLE_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_PROJECT_NR: TcxGridDBColumn;
    cxgrdtblvListF_KMO_PORT_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvListF_AMOUNT_REQUESTED: TcxGridDBColumn;
    cxgrdtblvListF_PAY_TO_SODEXO_OK: TcxGridDBColumn;
    cxgrdtblvListF_REMARK: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NL: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_FR: TcxGridDBColumn;
    cxgrdtblvListF_ORG_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ORG_ORG_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORG_VAT_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORG_SYNERGY_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORG_RSZ_NR: TcxGridDBColumn;
    cxgrdtblvListF_FOREGROUNDCOLOR: TcxGridDBColumn;
    cxgrdtblvListF_BACKGROUNDCOLOR: TcxGridDBColumn;
    cxgrdtblvListF_PAYMENT_OK: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_PAYMENT_AMOUNT1: TcxGridDBColumn;
    cxgrdtblvListF_PAYMENT_AMOUNT2: TcxGridDBColumn;
    cxgrdtblvListF_PAYMENT_AMOUNT3: TcxGridDBColumn;
    cxgrdtblvListF_SESSION: TcxGridDBColumn;
    cxgrdtblvListF_PROGRAM: TcxGridDBColumn;
    cxgrdtblvListF_DEMAND_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_DATE: TcxGridDBColumn;
    cxgrdtblvListF_PAYMENT_STATUS_NAME: TcxGridDBColumn;
    dxBarButton2: TdxBarButton;
    procedure cxdbbeF_STATUS_NAME1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxgrdtblvListStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FVBFFCListViewShow(Sender: TObject);
    procedure FVBFFCListViewCreate(Sender: TObject);
    procedure FVBFFCListViewClose(Sender: TObject;
      var Action: TCloseAction);
    procedure cxdbbeF_ORG_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure dxBarButton2Click(Sender: TObject);
  private
    { Private declarations }
    FSelectedSessions: TStringList;
    FSelectedSessionsCommaText: String;
    FSelectedEnrolsCommaText: String;
    procedure GetSessions;
    procedure OnReportPreviewClosedNoAction (Sender: TObject);

  protected
    procedure OnCanFocusRecord(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; var AAllow: Boolean);
    procedure StoreActiveViewToIni; override;
    procedure RestoreActiveViewFromIni; override;
    procedure PrintLetter(const APreview: Boolean);
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;


implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_KMO_Portefeuille, Data_EduMainClient, Form_FVBFFCBaseListView,
  Unit_FVBFFCInterfaces, unit_EdutecInterfaces, unit_PPWFrameworkClasses,
  unit_Const, printers, Unit_FVBFFCUtils, Unit_Global;

{ TfrmKMO_Portefeuille_List }

resourcestring
  rsSessionReportName = 'Letters';
  rsReportNoCertificateRecords = 'Er kan geen enkel certificaat worden afgedrukt.' + #13#10 + 'Thomas.';
  rsReportPrinteLetters = 'Do you want to print KMO letters ?';


{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmKMO_Portefeuille_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmKMO_Portefeuille_List.GetFilterString: String;
var
  aFilterString : String;
begin
  if not ( FSearchCriteriaDataSet.FieldByName( 'F_ORGANISATION_ID' ).IsNull ) then
  begin
    AddIntEqualCondition( aFilterString, 'F_ORGANISATION_ID', FSearchCriteriaDataSet.FieldByName( 'F_ORGANISATION_ID' ).AsInteger );
  end;

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_KMO_PORT_STATUS_ID' ).IsNull ) then
  begin
    AddIntEqualCondition ( aFilterString, 'F_KMO_PORT_STATUS_ID', FSearchCriteriaDataSet.FieldByName( 'F_KMO_PORT_STATUS_ID' ).AsInteger );
  end;

  if not ( FSearchCriteriaDataSet.FieldByName( 'F_PROJECT_NR' ).IsNull ) then
  begin
    AddLikeFilterCondition ( aFilterString, 'F_PROJECT_NR', FSearchCriteriaDataSet.FieldByName( 'F_PROJECT_NR' ).AsString );
  end;

  Result := aFilterString;
end;

{*****************************************************************************
  This method will be executed when the user clicks on one of the buttons in
  the Status ButtonEdit.  In here we will call the appropriate method on the
  IEDUSessionDataModule interface corresponding to the clicked button.

  @Name       TfrmPerson_Record.cxdbbeF_STATUS_NAME1PropertiesButtonClick
  @author     slesage
  @param      Sender         The Object from which the method is invoked.
  @param      AButtonIndex   The Index of the button on which the user
                             clicked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_Portefeuille_List.cxdbbeF_STATUS_NAME1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PortefeuilleDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PortefeuilleDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectStatusFilter( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmKMO_Portefeuille_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;
    with cxgrdtblvList.Controller do
    if (FocusedRecord <> nil) and FocusedRecord.Selected then
        FocusedRecord.Selected := False;
end;

procedure TfrmKMO_Portefeuille_List.FVBFFCListViewCreate(Sender: TObject);
begin
  inherited;
  // Add OnCanFocusRecord event to grid
  cxgrdtblvList.OnCanFocusRecord := OnCanFocusRecord;
  cxbtnEDUButton1.Enabled := False;
end;

{*****************************************************************************

  @Name       TfrmKMO_Portefeuille_List.OnCanFocusRecord
  @author     Ivan Van den Bossche
  @param      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
              var AAlow: Boolean
  @return     None
  @Exception  None
  @See        None
  @Description  Use the OnCanFocusRecord event to control focusing of a record.
                The Sender parameter identifies the affected view and
                the ARecord parameter specifies the record to be focused.
                To focus the record, set the AAllow parameter to True.
                Otherwise, the record will not be focused and as a result,
                not selected also. Select button will be enabled as a result.
******************************************************************************}
procedure TfrmKMO_Portefeuille_List.OnCanFocusRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  AAllow := True;
  cxbtnEDUButton1.Enabled := True;
end;

procedure TfrmKMO_Portefeuille_List.RestoreActiveViewFromIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].RestoreFromIniFile( LocalSettingsIniFileName, True, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;

procedure TfrmKMO_Portefeuille_List.StoreActiveViewToIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].StoreToIniFile( LocalSettingsIniFileName, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;


procedure TfrmKMO_Portefeuille_List.FVBFFCListViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

    // Make sure that changes made to the grid will be applied to the database.
    if (srcMain.DataSet.Active) then
    begin
        if srcMain.DataSet.State in [dsInsert, dsEdit] then
          srcMain.DataSet.Post;

    end;
end;

procedure TfrmKMO_Portefeuille_List.cxdbbeF_ORG_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PortefeuilleDataModule;
begin
  inherited;

  if ( Assigned( FrameWorkDataModule ) ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PortefeuilleDataModule, aDataModule ) ) then
  begin
    aDataModule.SelectOrganisationFilter( FSearchCriteriaDataSet );
  end;
end;

procedure TfrmKMO_Portefeuille_List.cxgrdtblvListStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if not (aRecord.Expandable) then
  begin
    // kleur
    aStyle := dtmEDUMainClient.cxsKMOPortefeuille;
    aStyle.Color := aRecord.Values[ cxgrdtblvListF_BACKGROUNDCOLOR.Index ];
    aStyle.TextColor := aRecord.Values[ cxgrdtblvListF_FOREGROUNDCOLOR.Index ];
  end;
end;

procedure TfrmKMO_Portefeuille_List.PrintLetter(const APreview: Boolean);
var
  lReportName, lReportTitle: string;
  ParamNames, ParamValues: TStringList;
  EnrolIds: TStringList;
  i: integer;
  oldCursor: TCursor;
  recordcount: Integer;
  PrinterCertificates: string;
  PrinterLetter: string;
begin
  // Get selected session_id(s)
  FSelectedSessions := TStringList.Create;

  try
    if TcxCustomGridTableController( ActiveGridView.Controller ).SelectedRecordCount > 1 then
    begin //multiple sessions selected
      LoopThroughSelectedRecords(GetSessions);
    end
    else begin //only one session selected
      FSelectedSessions.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
    end;

    FSelectedSessionsCommaText := FSelectedSessions.CommaText;

    //Check whether the report will at least be returning 1 record !
    recordcount := dtmEDUMainClient.ReportRecordCount('V_REP_KMO_PORTEFEUILLE', 'F_SESSION_ID',
    'F_SESSION_ID', FSelectedSessions);
    if (recordcount > 0) then
    begin
      if MessageDlg(rsReportPrinteLetters, mtConfirmation,[mbYes,mbNo],0)=mrNo then
      begin
        FSelectedSessions.Free;
        Exit;
      end;
    end
    else
    begin
      FSelectedSessions.Free;
      exit;
    end;

    PrinterCertificates := dtmEDUMainClient.GetPrinterCertificates;
    PrinterLetter := dtmEDUMainClient.GetPrinterLetters;

    lReportName := REP_LETTER_KMO;
    lReportTitle := 'Report Letter Kmo';
    ParamNames := TStringList.Create;
    ParamValues := TStringList.Create;
    oldCursor := screen.cursor;
    screen.cursor := crSQLWait;
    try
      ParamNames.Add('p_session_id');
      ParamValues.AddObject ('', FSelectedSessions);
      dtmEDUMainClient.ExecuteCrystal(lReportName, lReportTitle, ParamNames, ParamValues, APreview, True, OnReportPreviewClosedNoAction, PrinterLetter );
    finally
      screen.cursor := oldCursor;
      FreeAndNil (ParamValues);
      FreeAndNil (ParamNames);
      //FreeAndNil(dtm);
    end;

  finally
      FSelectedSessions := nil; // has been freed in TdtmCrystal.PrintReport
      EnrolIds := nil; // EnrolIds has been freed in TdtmCrystal.PrintReport
      //have to look at this when more time !
  end;
end;

procedure TfrmKMO_Portefeuille_List.GetSessions;
begin
  FSelectedSessions.Add(srcMain.Dataset.FieldByName ('F_SESSION_ID').AsString);
end;

procedure TfrmKMO_Portefeuille_List.OnReportPreviewClosedNoAction(
  Sender: TObject);
begin
//
end;

procedure TfrmKMO_Portefeuille_List.dxBarButton2Click(Sender: TObject);
begin
  inherited;
  PrintLetter(True); // Preview letter(s) and certificate(s) (ivdbossche 17-07-2007)
end;

end.
