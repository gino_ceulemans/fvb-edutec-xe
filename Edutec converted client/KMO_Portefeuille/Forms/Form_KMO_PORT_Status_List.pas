unit Form_KMO_PORT_Status_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_FVBFFCInterfaces, Form_EDUListView, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd,
  dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxBar, dxPSCore, dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  Menus;

type
  TfrmKMO_PORT_Status_List = class(TEduListView)
    cxgrdtblvListF_KMO_PORT_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvListF_NAME_NL: TcxGridDBColumn;
    cxgrdtblvListF_NAME_FR: TcxGridDBColumn;
    cxgrdtblvListF_FOREGROUNDCOLOR: TcxGridDBColumn;
    cxgrdtblvListF_BACKGROUNDCOLOR: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_KMO_PORT_Status, Data_EduMainClient;

end.