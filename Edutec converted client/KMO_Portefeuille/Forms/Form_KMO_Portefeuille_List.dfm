inherited frmKMO_Portefeuille_List: TfrmKMO_Portefeuille_List
  Left = 35
  Top = 137
  Width = 1076
  Height = 598
  ActiveControl = cxdbteF_PROJECT_NR1
  Caption = 'KMO Portefeuilles'
  Constraints.MinWidth = 812
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 530
    Width = 1068
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 852
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 964
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 1060
    Height = 522
    inherited cxgrdList: TcxGrid
      Top = 174
      Width = 1060
      Height = 348
      inherited cxgrdtblvList: TcxGridDBTableView
        DataController.DataModeController.DetailInSQLMode = True
        DataController.DataModeController.GridMode = False
        DataController.DataModeController.SmartRefresh = True
        DataController.DetailKeyFieldNames = 'F_KMO_PORTEFEUILLE_ID'
        DataController.KeyFieldNames = 'F_KMO_PORTEFEUILLE_ID'
        OptionsSelection.MultiSelect = True
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.OnGetContentStyle = cxgrdtblvListStylesGetContentStyle
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_KMO_PORTEFEUILLE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORTEFEUILLE_ID'
          Width = 143
        end
        object cxgrdtblvListF_PROJECT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROJECT_NR'
          Width = 126
        end
        object cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          Width = 99
        end
        object cxgrdtblvListF_ORG_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_NAME'
          Width = 135
        end
        object cxgrdtblvListF_AMOUNT_REQUESTED: TcxGridDBColumn
          DataBinding.FieldName = 'F_AMOUNT_REQUESTED'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Width = 155
        end
        object cxgrdtblvListF_PAY_TO_SODEXO_OK: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAY_TO_SODEXO_OK'
          Width = 158
        end
        object cxgrdtblvListF_PAYMENT_OK: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAYMENT_OK'
          Width = 89
        end
        object cxgrdtblvListF_ORG_VAT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_VAT_NR'
          Width = 106
        end
        object cxgrdtblvListF_ORG_RSZ_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_RSZ_NR'
          Width = 99
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Visible = False
        end
        object cxgrdtblvListF_KMO_PORT_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORT_STATUS_ID'
          Visible = False
        end
        object cxgrdtblvListF_REMARK: TcxGridDBColumn
          DataBinding.FieldName = 'F_REMARK'
          Visible = False
        end
        object cxgrdtblvListF_STATUS_NL: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NL'
          Visible = False
        end
        object cxgrdtblvListF_STATUS_FR: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_FR'
          Visible = False
        end
        object cxgrdtblvListF_ORG_ORG_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_ORG_NR'
          Visible = False
        end
        object cxgrdtblvListF_ORG_SYNERGY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_SYNERGY_ID'
          Visible = False
        end
        object cxgrdtblvListF_FOREGROUNDCOLOR: TcxGridDBColumn
          DataBinding.FieldName = 'F_FOREGROUNDCOLOR'
          Visible = False
        end
        object cxgrdtblvListF_BACKGROUNDCOLOR: TcxGridDBColumn
          DataBinding.FieldName = 'F_BACKGROUNDCOLOR'
          Visible = False
        end
        object cxgrdtblvListF_DEMAND_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_DEMAND_DATE'
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
        end
        object cxgrdtblvListF_SESSION: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION'
          Width = 215
        end
        object cxgrdtblvListF_PROGRAM: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROGRAM'
          Width = 202
        end
        object cxgrdtblvListF_SESSION_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_DATE'
        end
        object cxgrdtblvListF_PAYMENT_AMOUNT1: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAYMENT_AMOUNT1'
        end
        object cxgrdtblvListF_PAYMENT_AMOUNT2: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAYMENT_AMOUNT2'
        end
        object cxgrdtblvListF_PAYMENT_AMOUNT3: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAYMENT_AMOUNT3'
        end
        object cxgrdtblvListF_PAYMENT_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAYMENT_STATUS_NAME'
          Width = 315
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 149
      Width = 1060
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 145
      Width = 1060
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 1060
      Height = 145
      ExpandedHeight = 145
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 112
        Width = 1058
        Height = 32
        TabOrder = 3
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 890
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 978
        end
      end
      object cxlblF_PROJECT_NR2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_Portefeuille_List.F_PROJECT_NR'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Project nummer'
        FocusControl = cxdbteF_PROJECT_NR1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_PROJECT_NR1: TFVBFFCDBTextEdit
        Left = 112
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_Portefeuille_List.F_PROJECT_NR'
        DataBinding.DataField = 'F_PROJECT_NR'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 0
        Width = 320
      end
      object cxlblF_STATUS_NAME1: TFVBFFCLabel
        Left = 8
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_Portefeuille_List.F_STATUS_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Status'
        FocusControl = cxdbbeF_STATUS_NAME1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_STATUS_NAME1: TFVBFFCDBButtonEdit
        Left = 112
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_Portefeuille_List.F_STATUS_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_STATUS_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_STATUS_NAME1PropertiesButtonClick
        TabOrder = 2
        Width = 216
      end
      object cxlblF_ORG_NAME: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_Portefeuille_List.F_ORG_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Organisatie'
        FocusControl = cxdbbeF_ORG_NAME
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_ORG_NAME: TFVBFFCDBButtonEdit
        Left = 112
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_Portefeuille_List.F_ORG_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_ORG_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_ORG_NAMEPropertiesButtonClick
        TabOrder = 1
        Width = 216
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 1068
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 526
    Width = 1068
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 1064
    Height = 522
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 522
  end
  inherited srcMain: TFVBFFCDataSource
    Left = 48
    Top = 264
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarButton2: TdxBarButton
      Caption = 'Afdrukken KMO brief'
      Category = 0
      Hint = 'Afdrukken KMO brief'
      Visible = ivAlways
      ShortCut = 16464
      OnClick = dxBarButton2Click
    end
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        Item = dxBarButton2
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    Left = 144
    Top = 254
  end
  object prdDialog: TPrintDialog
    Left = 332
    Top = 388
  end
end
