inherited frmKMO_PORT_Payment_Record: TfrmKMO_PORT_Payment_Record
  Left = 383
  Top = 99
  Width = 812
  Height = 600
  Caption = 'KMO Portefeuille - Faktuur'
  Constraints.MinHeight = 600
  Constraints.MinWidth = 812
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 543
    Width = 804
    Height = 30
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 468
      Height = 30
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 804
    Height = 543
    inherited pnlRecord: TFVBFFCPanel
      Width = 642
      Height = 543
      inherited pnlRecordHeader: TFVBFFCPanel
        Top = 0
        Width = 642
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Top = 25
        Width = 642
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 642
        object cxlblF_KMO_PORT_PAYMENT_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_KMO_PORT_PAYMENT_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Faktuur ( ID )'
          FocusControl = cxdblblF_KMO_PORT_PAYMENT_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_KMO_PORT_PAYMENT_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_KMO_PORT_PAYMENT_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_KMO_PORT_PAYMENT_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object cxlblF_INVOICE_NR1: TFVBFFCLabel
          Left = 158
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_INVOICE_NR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Faktuur nummer'
          FocusControl = cxdblblF_INVOICE_NR
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_INVOICE_NR: TFVBFFCDBLabel
          Left = 158
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_INVOICE_NR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_INVOICE_NR'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 289
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 642
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 537
        Width = 642
        Height = 6
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 642
        Height = 467
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 642
        end
        inherited sbxMain: TScrollBox
          Width = 642
          Height = 438
          object cxlblF_KMO_PORT_PAYMENT_ID2: TFVBFFCLabel
            Left = 8
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_KMO_PORT_PAYMENT_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Faktuur ( ID )'
            FocusControl = cxdbseF_KMO_PORT_PAYMENT_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_KMO_PORT_PAYMENT_ID: TFVBFFCDBSpinEdit
            Left = 150
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_KMO_PORT_PAYMENT_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_KMO_PORT_PAYMENT_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 137
          end
          object cxlblF_SESSION_CODE1: TFVBFFCLabel
            Left = 8
            Top = 99
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_SESSION_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie'
            FocusControl = cxdbbeF_SESSION_CODE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_SESSION_CODE: TFVBFFCDBButtonEdit
            Left = 150
            Top = 99
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_SESSION_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_SESSION_CODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_SESSION_CODEPropertiesButtonClick
            TabOrder = 5
            Width = 264
          end
          object cxlblF_INVOICE_NR2: TFVBFFCLabel
            Left = 8
            Top = 27
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_INVOICE_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Faktuur nummer'
            FocusControl = cxdbteF_INVOICE_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_INVOICE_NR: TFVBFFCDBTextEdit
            Left = 150
            Top = 27
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_INVOICE_NR'
            DataBinding.DataField = 'F_INVOICE_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 2
            Width = 264
          end
          object cxlblF_REMARK1: TFVBFFCLabel
            Left = 8
            Top = 123
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Opmerking'
            FocusControl = cxdbmmoF_REMARK
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_REMARK: TFVBFFCDBMemo
            Left = 8
            Top = 144
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_REMARK'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 6
            Height = 108
            Width = 409
          end
          object cxlblF_AMOUNT_PAYED2: TFVBFFCLabel
            Left = 8
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_PAYED'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Betaald bedrag'
            FocusControl = F_AMOUNT_PAYED1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_AMOUNT_PAYED1: TFVBFFCDBCurrencyEdit
            Left = 150
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_PAYED'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_AMOUNT_PAYED'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 3
            Width = 121
          end
          object cxlblF_DATE_PAYED1: TFVBFFCLabel
            Left = 8
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_DATE_PAYED'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Betaald op'
            FocusControl = cxdbdeF_DATE_PAYED
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbdeF_DATE_PAYED: TFVBFFCDBDateEdit
            Left = 150
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmSesSession_Record.F_DATE_PAYED'
            RepositoryItem = dtmEDUMainClient.cxeriDate
            DataBinding.DataField = 'F_DATE_PAYED'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 4
            Width = 137
          end
        end
        object pnlPriceSchemeInfoSeperator: TFVBFFCPanel
          Left = 0
          Top = 463
          Width = 642
          Height = 4
          Align = alBottom
          BevelOuter = bvNone
          BorderWidth = 4
          ParentColor = True
          TabOrder = 2
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clInactiveCaption
          StyleClientArea.BackColor2 = clInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 800
      Height = 543
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 543
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiLinkedPayments
          end>
      end
      object dxnbiLinkedPayments: TdxNavBarItem
        Action = acShow_KMO_PORT_Link_Port_Payment
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 543
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Payment.cdsRecord
  end
  inherited alRecordView: TFVBFFCActionList
    object acShow_KMO_PORT_Link_Port_Payment: TAction
      Caption = 'Gekoppelde betalingen'
      OnExecute = acShow_KMO_PORT_Link_Port_PaymentExecute
    end
  end
end
