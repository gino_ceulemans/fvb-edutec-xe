inherited frmKMO_Portefeuille_Record: TfrmKMO_Portefeuille_Record
  Left = 34
  Top = 90
  Width = 1075
  Height = 671
  Caption = 'KMO Portefeuille'
  Constraints.MinHeight = 600
  Constraints.MinWidth = 812
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 614
    Width = 1067
    Height = 30
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 731
      Height = 30
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 1067
    Height = 614
    inherited pnlRecord: TFVBFFCPanel
      Width = 905
      Height = 614
      inherited pnlRecordHeader: TFVBFFCPanel
        Top = 0
        Width = 905
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Top = 25
        Width = 905
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 905
        object cxlblF_KMO_PORTEFEUILLE_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_Portefeuille_Record.F_KMO_PORTEFEUILLE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'KMO Portefeuille ( ID )'
          FocusControl = cxdblblF_KMO_PORTEFEUILLE_ID
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_KMO_PORTEFEUILLE_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_Portefeuille_Record.F_KMO_PORTEFEUILLE_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_KMO_PORTEFEUILLE_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object cxlblF_PROJECT_NR1: TFVBFFCLabel
          Left = 158
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_Portefeuille_Record.F_PROJECT_NR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Project nummer'
          FocusControl = cxdblblF_PROJECT_NR
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROJECT_NR: TFVBFFCDBLabel
          Left = 158
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_Portefeuille_Record.F_PROJECT_NR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROJECT_NR'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 289
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 905
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 608
        Width = 905
        Height = 6
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 905
        Height = 538
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 905
        end
        inherited sbxMain: TScrollBox
          Width = 905
          Height = 509
          object cxlblF_KMO_PORTEFEUILLE_ID2: TFVBFFCLabel
            Left = 8
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_KMO_PORTEFEUILLE_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'KMO Portefeuille ( ID )'
            FocusControl = cxdbseF_KMO_PORTEFEUILLE_ID
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_KMO_PORTEFEUILLE_ID: TFVBFFCDBSpinEdit
            Left = 150
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_KMO_PORTEFEUILLE_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_KMO_PORTEFEUILLE_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 137
          end
          object cxlblF_ORG_NAME1: TFVBFFCLabel
            Left = 8
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_ORG_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Organisatie'
            FocusControl = cxdbbeF_ORG_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_ORG_NAME: TFVBFFCDBButtonEdit
            Left = 150
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_ORG_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_ORG_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_ORG_NAMEPropertiesButtonClick
            TabOrder = 3
            Width = 264
          end
          object cxlblF_PROJECT_NR2: TFVBFFCLabel
            Left = 8
            Top = 27
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PROJECT_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Project nummer'
            FocusControl = cxdbteF_PROJECT_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_PROJECT_NR: TFVBFFCDBTextEdit
            Left = 150
            Top = 27
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PROJECT_NR'
            DataBinding.DataField = 'F_PROJECT_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 2
            Width = 264
          end
          object cxlblF_STATUS_NAME1: TFVBFFCLabel
            Left = 8
            Top = 213
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Status'
            FocusControl = cxdbbeF_STATUS_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_STATUS_NAME: TFVBFFCDBButtonEdit
            Left = 150
            Top = 213
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_STATUS_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_STATUS_NAMEPropertiesButtonClick
            TabOrder = 10
            Width = 264
          end
          object cxlblF_REMARK1: TFVBFFCLabel
            Left = 8
            Top = 261
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Opmerking'
            FocusControl = cxdbmmoF_REMARK
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbmmoF_REMARK: TFVBFFCDBMemo
            Left = 8
            Top = 282
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_REMARK'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 12
            Height = 89
            Width = 497
          end
          object cxlblF_AMOUNT_REQUESTED2: TFVBFFCLabel
            Left = 8
            Top = 237
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Aangevraagd bedrag'
            FocusControl = F_AMOUNT_REQUESTED1
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object F_AMOUNT_REQUESTED1: TFVBFFCDBCurrencyEdit
            Left = 150
            Top = 237
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_AMOUNT_REQUESTED'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 11
            Width = 121
          end
          object dbcbF_PAY_TO_SODEXO_OK: TFVBFFCDBCheckBox
            Left = 150
            Top = 165
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PAY_TO_SODEXO_OK'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_PAY_TO_SODEXO_OK'
            DataBinding.DataField = 'F_PAY_TO_SODEXO_OK'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 8
            Width = 21
          end
          object F_PAY_TO_SODEXO_OK4: TFVBFFCLabel
            Left = 8
            Top = 165
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PAY_TO_SODEXO_OK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Storting OK @ Sodexo'
            FocusControl = dbcbF_PAY_TO_SODEXO_OK
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object F_PAYMENT_OK1: TFVBFFCLabel
            Left = 8
            Top = 189
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PAYMENT_OK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Betaling OK'
            FocusControl = dbcbF_PAYMENT_OK
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object dbcbF_PAYMENT_OK: TFVBFFCDBCheckBox
            Left = 150
            Top = 189
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PAYMENT_OK'
            RepositoryItem = dtmEDUMainClient.cxeriBooleanLeftAlign
            Caption = 'cxdbcbF_PAYMENT_OK'
            DataBinding.DataField = 'F_PAYMENT_OK'
            DataBinding.DataSource = srcMain
            ParentColor = False
            ParentFont = False
            TabOrder = 9
            Width = 21
          end
          object FVBFFCLabel1: TFVBFFCLabel
            Left = 8
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_ORG_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Datum aanvraag'
            FocusControl = cxdbbeF_ORG_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCLabel2: TFVBFFCLabel
            Left = 8
            Top = 99
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_ORG_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessienummer'
            FocusControl = cxdbbeF_ORG_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCLabel3: TFVBFFCLabel
            Left = 8
            Top = 123
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_ORG_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Opleiding'
            FocusControl = cxdbbeF_ORG_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCLabel5: TFVBFFCLabel
            Left = 560
            Top = 5
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Betaling 1'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCLabel6: TFVBFFCLabel
            Left = 560
            Top = 29
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Betaling 2'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCLabel7: TFVBFFCLabel
            Left = 560
            Top = 53
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Betaling 3'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCLabel8: TFVBFFCLabel
            Left = 560
            Top = 77
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Verschil'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCDBDateEdit1: TFVBFFCDBDateEdit
            Left = 150
            Top = 75
            DataBinding.DataField = 'F_DEMAND_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 4
            Width = 139
          end
          object FVBFFCDBButtonEdit1: TFVBFFCDBButtonEdit
            Left = 150
            Top = 98
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_SESSION'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_SESSION'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = FVBFFCDBButtonEdit1PropertiesButtonClick
            TabOrder = 5
            Width = 139
          end
          object FVBFFCLabel9: TFVBFFCLabel
            Left = 9
            Top = 144
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_ORG_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Datum'
            FocusControl = cxdbbeF_ORG_NAME
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object FVBFFCDBTextEdit1: TFVBFFCDBTextEdit
            Left = 150
            Top = 121
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_PROJECT_NR'
            DataBinding.DataField = 'F_PROGRAM'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 6
            Width = 264
          end
          object FVBFFCDBDateEdit2: TFVBFFCDBDateEdit
            Left = 150
            Top = 143
            DataBinding.DataField = 'F_SESSION_DATE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.ReadOnly = True
            TabOrder = 7
            Width = 139
          end
          object FVBFFCDBMemo1: TFVBFFCDBMemo
            Left = 10
            Top = 393
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
            RepositoryItem = dtmEDUMainClient.cxeriMemo
            DataBinding.DataField = 'F_ANNULATION_REASON'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 13
            Height = 89
            Width = 497
          end
          object FVBFFCDBCurrencyEdit1: TFVBFFCDBCurrencyEdit
            Left = 630
            Top = 5
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_PAYMENT_AMOUNT1'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 14
            Width = 121
          end
          object FVBFFCDBCurrencyEdit2: TFVBFFCDBCurrencyEdit
            Left = 630
            Top = 29
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_PAYMENT_AMOUNT2'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 15
            Width = 121
          end
          object FVBFFCDBCurrencyEdit3: TFVBFFCDBCurrencyEdit
            Left = 630
            Top = 53
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_PAYMENT_AMOUNT3'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 16
            Width = 121
          end
          object FVBFFCDBButtonEdit2: TFVBFFCDBButtonEdit
            Left = 630
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_Portefeuille_Record.F_STATUS_NAME'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PAYMENT_STATUS_NAME'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.ReadOnly = True
            Properties.OnButtonClick = FVBFFCDBButtonEdit2PropertiesButtonClick
            TabOrder = 17
            Width = 275
          end
        end
        object pnlPriceSchemeInfoSeperator: TFVBFFCPanel
          Left = 0
          Top = 534
          Width = 905
          Height = 4
          Align = alBottom
          BevelOuter = bvNone
          BorderWidth = 4
          ParentColor = True
          TabOrder = 2
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clInactiveCaption
          StyleClientArea.BackColor2 = clInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 1063
      Height = 614
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 614
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 614
    end
  end
  object FVBFFCLabel4: TFVBFFCLabel [3]
    Left = 166
    Top = 468
    HelpType = htKeyword
    HelpKeyword = 'frmKMO_Portefeuille_Record.F_REMARK'
    RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
    Caption = 'Reden annulatie'
    ParentColor = False
    ParentFont = False
    Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmKMO_Portefeuille.cdsRecord
  end
end
