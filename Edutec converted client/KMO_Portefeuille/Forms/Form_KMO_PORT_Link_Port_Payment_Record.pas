unit Form_KMO_PORT_Link_Port_Payment_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxControls,
  cxContainer, cxEdit, cxLabel, cxDropDownEdit, cxCalc, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxImageComboBox, cxMemo, cxCalendar,
  cxButtonEdit, cxSpinEdit, cxCurrencyEdit, Unit_PPWFrameWorkInterfaces,
  Menus, cxGraphics, cxSplitter, cxGroupBox, cxCheckBox;

type
  TfrmKMO_PORT_Link_Port_Payment_Record = class(TEDURecordView)
    cxlblF_KMO_PORT_LNK_PORT_PAY_ID1: TFVBFFCLabel;
    cxdblblF_KMO_PORT_LNK_PORT_PAY_ID: TFVBFFCDBLabel;
    pnlPriceSchemeInfoSeperator: TFVBFFCPanel;
    cxlblF_KMO_PORT_LNK_PORT_PAY_ID2: TFVBFFCLabel;
    cxdbseF_KMO_PORT_LNK_PORT_PAY_ID: TFVBFFCDBSpinEdit;
    cxlblF_INVOICE_NR1: TFVBFFCLabel;
    cxdbbeF_INVOICE_NR: TFVBFFCDBButtonEdit;
    cxlblF_AMOUNT_PAYED2: TFVBFFCLabel;
    cxdbceF_AMOUNT_PAYED: TFVBFFCDBCurrencyEdit;
    cxlblF_PROJECT_NR: TFVBFFCLabel;
    cxdbbeF_PROJECT_NR: TFVBFFCDBButtonEdit;
    cxlblF_AMOUNT_REQUESTED: TFVBFFCLabel;
    cxdbceF_AMOUNT_REQUESTED: TFVBFFCDBCurrencyEdit;
    cxlblF_AMOUNT_LINKED: TFVBFFCLabel;
    cxdbceF_AMOUNT_LINKED: TFVBFFCDBCurrencyEdit;
    FVBFFCLabel1: TFVBFFCLabel;
    cxdblblF_INVOICE_NR: TFVBFFCDBLabel;
    FVBFFCLabel2: TFVBFFCLabel;
    cxdblblF_PROJECT_NR: TFVBFFCDBLabel;
    procedure cxdbbeF_INVOICE_NRPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxbtnOKClick(Sender: TObject);
    procedure acShowRecordDetailExecute(Sender: TObject);
    procedure srcMainDataChange(Sender: TObject; Field: TField);
    procedure acShow_KMO_PORT_Link_Port_PaymentExecute(Sender: TObject);
    procedure cxdbbeF_PROJECT_NRPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure ShowRecordDetail( Sender : TObject ); override;
    procedure ShowDetailListView( Sender : TObject;
                                  aDataModuleClassName : String;
                                  aDockSite            : TWinControl ); override;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_KMO_PORT_Link_Port_Payment, Data_EduMainClient, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, unit_EdutecInterfaces,
  Form_FVBFFCBaseRecordView, Unit_PPWFrameWorkRecordView;

{ TfrmKMO_PORT_Link_Port_Payment_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmKMO_PORT_Link_Port_Payment_Record.GetDetailName
  @author     PIFWizard Generated
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmKMO_PORT_Link_Port_Payment_Record.GetDetailName: String;
begin
  Result := cxdblblF_INVOICE_NR.Caption + ' - ' + cxdblblF_PROJECT_NR.Caption; 
end;

{*****************************************************************************
  Overriden ShowDetailListView in which we will also hide Pricing Scheme info.

  @Name       TfrmKMO_PORT_Link_Port_Payment_Record.ShowDetailListView
  @author     slesage
  @param      Sender                 The Object from which the method was
                                     invoked.
  @param      aDataModuleClassName   The ClassName of the DetailDataModule.
  @param      aDockSite              The control in which the DetailListview
                                     should be docked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_PORT_Link_Port_Payment_Record.ShowDetailListView(Sender: TObject;
  aDataModuleClassName: String; aDockSite: TWinControl);
begin
  inherited;
end;

{*****************************************************************************
  Overriden ShowDetailListView in which we will also Show Pricing Scheme info.

  @Name       TfrmKMO_PORT_Link_Port_Payment_Record.ShowRecordDetail
  @author     slesage
  @param      Sender   The Object from which the method was invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_PORT_Link_Port_Payment_Record.ShowRecordDetail(Sender: TObject);
begin
  inherited;
end;


procedure TfrmKMO_PORT_Link_Port_Payment_Record.cxdbbeF_INVOICE_NRPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PORT_Link_Port_PaymentDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PORT_Link_Port_PaymentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowKMO_PORT_Payment( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectKMO_PORT_Payment( srcMain.DataSet );
    end;
  end;
end;

{*****************************************************************************
  This event will be executed when the Form is initialised and will be used
  to Enable or Disable some controls.

  @Name       TfrmKMO_PORT_Link_Port_Payment_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      aFrameWorkDataModule   The DataModule assiciated to the Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_PORT_Link_Port_Payment_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbceF_AMOUNT_PAYED );
  dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbceF_AMOUNT_REQUESTED );
  // komende vanaf een andere datamodule -> velden op read-only zetten
  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUKMO_PortefeuilleDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_PROJECT_NR );
    end;
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUKMO_PORT_PaymentDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_INVOICE_NR );
    end;
  end;
end;

{*******************************************************************************
  Check if a recalculation is needed in order to have an accurate "base-price"

  @Name       TfrmProgProgram_Record.cxbtnOKClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
  @History                  Author                    Description
  28/03/2008          Ivan Van den Bossche            Extra checks (mantis 2345)
********************************************************************************}

procedure TfrmKMO_PORT_Link_Port_Payment_Record.cxbtnOKClick(Sender: TObject);
begin
  Inherited;

  if ( Sender = cxbtnApply ) then
  begin
    RefreshVisibleDetailListViews;
  end;

end;

procedure TfrmKMO_PORT_Link_Port_Payment_Record.acShowRecordDetailExecute(Sender: TObject);
begin
  inherited;
//  self.FrameWorkDataModule.ListViewDataModule.RefreshListDataSet;
end;


procedure TfrmKMO_PORT_Link_Port_Payment_Record.srcMainDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
//
end;

procedure TfrmKMO_PORT_Link_Port_Payment_Record.acShow_KMO_PORT_Link_Port_PaymentExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmKMO_PORT_Link_Port_Payment', pnlRecordDetail );
end;

procedure TfrmKMO_PORT_Link_Port_Payment_Record.cxdbbeF_PROJECT_NRPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PORT_Link_Port_PaymentDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PORT_Link_Port_PaymentDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowKMO_Portefeuille( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectKMO_Portefeuille( srcMain.DataSet );
    end;
  end;
end;

end.
