inherited frmKMO_PORT_Status_Record: TfrmKMO_PORT_Status_Record
  Left = 394
  Top = 257
  Width = 1003
  ActiveControl = cxdbteF_NAME_NL
  Caption = 'Status KMO Portefeuille'
  Constraints.MinWidth = 1000
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Width = 995
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 659
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 995
    inherited pnlRecord: TFVBFFCPanel
      Width = 833
      inherited pnlRecordHeader: TFVBFFCPanel
        Width = 833
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Width = 833
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 833
        object cxlblF_KMO_PORT_STATUS_ID2: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Status_Record.F_KMO_PORT_STATUS_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Status ( ID )'
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_KMO_PORT_STATUS_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Status_Record.F_KMO_PORT_STATUS_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_KMO_PORT_STATUS_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 73
        end
        object cxlblF_STATUS_NAME1: TFVBFFCLabel
          Left = 96
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Status_Record.F_STATUS_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Status'
          FocusControl = cxdblblF_STATUS_NAME
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_STATUS_NAME: TFVBFFCDBLabel
          Left = 96
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Status_Record.F_STATUS_NAME'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_STATUS_NAME'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.Font.Charset = ANSI_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Verdana'
          Style.Font.Style = []
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 750
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 833
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Width = 833
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 833
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 833
        end
        inherited sbxMain: TScrollBox
          Width = 833
          object cxlblF_KMO_PORT_STATUS_ID: TFVBFFCLabel
            Left = 8
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_KMO_PORT_STATUS_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Status ( ID )'
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_KMO_PORT_STATUS_ID: TFVBFFCDBSpinEdit
            Left = 168
            Top = 8
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_KMO_PORT_STATUS_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_KMO_PORT_STATUS_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 1
            Width = 121
          end
          object cxlblF_NAME_NL: TFVBFFCLabel
            Left = 8
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_NAME_NL'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam ( NL )'
            FocusControl = cxdbteF_NAME_NL
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_NL: TFVBFFCDBTextEdit
            Left = 168
            Top = 32
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_NAME_NL'
            DataBinding.DataField = 'F_NAME_NL'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 3
            Width = 650
          end
          object cxlblF_NAME_FR: TFVBFFCLabel
            Left = 8
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_NAME_FR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Naam ( FR )'
            FocusControl = cxdbteF_NAME_FR
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbteF_NAME_FR: TFVBFFCDBTextEdit
            Left = 168
            Top = 56
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_NAME_FR'
            DataBinding.DataField = 'F_NAME_FR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            TabOrder = 5
            Width = 650
          end
          object btnColorBG: TFVBFFCButton
            Left = 168
            Top = 80
            Width = 21
            Height = 21
            TabOrder = 6
            OnClick = btnColorBGClick
          end
          object btnColorFG: TFVBFFCButton
            Left = 168
            Top = 104
            Width = 21
            Height = 21
            TabOrder = 7
            OnClick = btnColorFGClick
          end
          object cxlblF_BACKGROUNDCOLOR: TFVBFFCLabel
            Left = 8
            Top = 80
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_BACKGROUNDCOLOR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Achtergrondkleur'
            FocusControl = btnColorBG
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxlblF_FOREGROUNDCOLOR: TFVBFFCLabel
            Left = 8
            Top = 104
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Status_Record.F_FOREGROUNDCOLOR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Tekstkleur'
            FocusControl = btnColorFG
            ParentColor = False
            ParentFont = False
            Style.Font.Charset = ANSI_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Verdana'
            Style.Font.Style = []
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxteColorExample: TFVBFFCTextEdit
            Left = 200
            Top = 91
            ParentFont = False
            TabOrder = 10
            Text = 'Kleurvoorbeeld voor tekst en achtergrond'
            Width = 618
          end
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 991
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Status.cdsRecord
  end
  object dlgColor: TColorDialog
    Left = 350
    Top = 231
  end
end
