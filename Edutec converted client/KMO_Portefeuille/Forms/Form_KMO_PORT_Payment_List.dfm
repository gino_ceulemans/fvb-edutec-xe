inherited frmKMO_PORT_Payment_List: TfrmKMO_PORT_Payment_List
  Left = 349
  Top = 168
  Width = 900
  Height = 598
  ActiveControl = cxdbteF_INVOICE_NR1
  Caption = 'KMO Portefeuille - Fakturen'
  Constraints.MinWidth = 900
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 530
    Width = 892
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 676
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 788
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 884
    Height = 522
    inherited cxgrdList: TcxGrid
      Top = 174
      Width = 884
      Height = 348
      inherited cxgrdtblvList: TcxGridDBTableView
        DataController.DataModeController.DetailInSQLMode = True
        DataController.DataModeController.GridMode = False
        DataController.DataModeController.SmartRefresh = True
        DataController.DetailKeyFieldNames = 'F_KMO_PORT_PAYMENT_ID'
        DataController.KeyFieldNames = 'F_KMO_PORT_PAYMENT_ID'
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.OnGetContentStyle = cxgrdtblvListStylesGetContentStyle
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_KMO_PORT_PAYMENT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORT_PAYMENT_ID'
          Width = 50
        end
        object cxgrdtblvListF_INVOICE_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_INVOICE_NR'
          Width = 127
        end
        object cxgrdtblvListF_AMOUNT_PAYED: TcxGridDBColumn
          DataBinding.FieldName = 'F_AMOUNT_PAYED'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Width = 111
        end
        object cxgrdtblvListSUM_AMOUNT_LINKED: TcxGridDBColumn
          DataBinding.FieldName = 'SUM_AMOUNT_LINKED'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Width = 149
        end
        object cxgrdtblvListF_DATE_PAYED: TcxGridDBColumn
          DataBinding.FieldName = 'F_DATE_PAYED'
        end
        object cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_CODE'
        end
        object cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_NAME'
          Width = 188
        end
        object cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_START_DATE'
          Width = 140
        end
        object cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_END_DATE'
          Width = 136
        end
        object cxgrdtblvListF_SESSION_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_STATUS_NAME'
          Width = 173
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
          Width = 105
        end
        object cxgrdtblvListF_REMARK: TcxGridDBColumn
          DataBinding.FieldName = 'F_REMARK'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 149
      Width = 884
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 145
      Width = 884
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 884
      Height = 145
      ExpandedHeight = 145
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 112
        Width = 882
        Height = 32
        TabOrder = 7
        DesignSize = (
          882
          32)
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 714
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 802
        end
      end
      object cxlblF_INVOICE_NR2: TFVBFFCLabel
        Left = 8
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_PORT_Payment_List.F_INVOICE_NR'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Faktuur nummer'
        FocusControl = cxdbteF_INVOICE_NR1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbteF_INVOICE_NR1: TFVBFFCDBTextEdit
        Left = 112
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_PORT_Payment_List.F_INVOICE_NR'
        DataBinding.DataField = 'F_INVOICE_NR'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 0
        Width = 320
      end
      object cxlblF_STATUS_NAME1: TFVBFFCLabel
        Left = 448
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_PORT_Payment_List.F_STATUS_NAME'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessie'
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxlblF_SESSION_CODE: TFVBFFCLabel
        Left = 448
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_PORT_Payment_List.F_SESSION_CODE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessie Referentie'
        FocusControl = cxdbbeF_SESSION_CODE
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbbeF_SESSION_CODE: TFVBFFCDBButtonEdit
        Left = 552
        Top = 32
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_PORT_Payment_List.F_SESSION_CODE'
        RepositoryItem = dtmEDUMainClient.cxeriSelectLookup
        DataBinding.DataField = 'F_SESSION_CODE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = cxdbbeF_SESSION_CODEPropertiesButtonClick
        TabOrder = 3
        Width = 216
      end
      object cxdbteF_SESSION_NAME1: TFVBFFCDBTextEdit
        Left = 552
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmKMO_PORT_Payment_List.F_SESSION_NAME'
        DataBinding.DataField = 'F_SESSION_NAME'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 4
        Width = 320
      end
      object cxlblF_SESSION_START_DATE1: TFVBFFCLabel
        Left = 448
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSION_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Sessie tussen'
        FocusControl = cxdbdeF_SESSION_START_DATE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_SESSION_START_DATE1: TFVBFFCDBDateEdit
        Left = 552
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSION_START_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_SESSION_START_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 5
        Width = 136
      end
      object cxlblF_SESSION_END_DATE1: TFVBFFCLabel
        Left = 704
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSION_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'En'
        FocusControl = cxdbdeF_SESSION_END_DATE1
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_SESSION_END_DATE1: TFVBFFCDBDateEdit
        Left = 736
        Top = 80
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSION_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_SESSION_END_DATE'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 6
        Width = 136
      end
      object cxlblF_DATE_PAYED_FROM: TFVBFFCLabel
        Left = 8
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_DATE_PAYED_FROM'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'Betaald tussen'
        FocusControl = cxdbdeF_DATE_PAYED_FROM
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_DATE_PAYED_FROM: TFVBFFCDBDateEdit
        Left = 112
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_DATE_PAYED_FROM'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_DATE_PAYED_FROM'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 1
        Width = 136
      end
      object cxlblF_DATE_PAYED_TO: TFVBFFCLabel
        Left = 264
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_SESSION_END_DATE'
        RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
        Caption = 'En'
        FocusControl = cxdbdeF_DATE_PAYED_TO
        ParentColor = False
        ParentFont = False
        Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
      end
      object cxdbdeF_DATE_PAYED_TO: TFVBFFCDBDateEdit
        Left = 296
        Top = 56
        HelpType = htKeyword
        HelpKeyword = 'frmSesSession_List.F_DATE_PAYED_TO'
        RepositoryItem = dtmEDUMainClient.cxeriDate
        DataBinding.DataField = 'F_DATE_PAYED_TO'
        DataBinding.DataSource = srcSearchCriteria
        ParentFont = False
        TabOrder = 2
        Width = 136
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 892
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 526
    Width = 892
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 888
    Height = 522
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 522
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Payment.cdsList
    Left = 48
    Top = 264
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Payment.cdsSearchCriteria
    Left = 144
    Top = 254
  end
  object prdDialog: TPrintDialog
    Left = 332
    Top = 388
  end
end
