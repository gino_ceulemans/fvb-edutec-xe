inherited frmKMO_PORT_Link_Port_Session_Record: TfrmKMO_PORT_Link_Port_Session_Record
  Left = 383
  Top = 99
  Width = 812
  Height = 600
  Caption = 'KMO Portefeuille - Sessie'
  Constraints.MinHeight = 600
  Constraints.MinWidth = 812
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TFVBFFCPanel
    Top = 543
    Width = 804
    Height = 30
    inherited pnlBottomButtons: TFVBFFCPanel
      Left = 468
      Height = 30
    end
  end
  inherited pnlTop: TFVBFFCPanel
    Width = 804
    Height = 543
    inherited pnlRecord: TFVBFFCPanel
      Width = 642
      Height = 543
      inherited pnlRecordHeader: TFVBFFCPanel
        Top = 0
        Width = 642
      end
      inherited pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Top = 25
        Width = 642
      end
      inherited pnlRecordFixedData: TFVBFFCPanel
        Width = 642
        object cxlblF_KMO_PORT_LNK_PORT_SES_ID1: TFVBFFCLabel
          Left = 8
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Link_Port_Session_Record.F_KMO_PORT_LNK_PORT_SES_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'ID'
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_KMO_PORT_LNK_PORT_SES_ID: TFVBFFCDBLabel
          Left = 8
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Link_Port_Session_Record.F_KMO_PORT_LNK_PORT_SES_ID'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_KMO_PORT_LNK_PORT_SES_ID'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 121
        end
        object FVBFFCLabel2: TFVBFFCLabel
          Left = 160
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_PROJECT_NR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Project nummer'
          FocusControl = cxdblblF_PROJECT_NR
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_PROJECT_NR: TFVBFFCDBLabel
          Left = 160
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_PROJECT_NR'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_PROJECT_NR'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 163
        end
        object FVBFFCLabel1: TFVBFFCLabel
          Left = 360
          Top = 0
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_SESSION_CODE'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          Caption = 'Sessie'
          FocusControl = cxdblblF_SESSION_CODE
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
        end
        object cxdblblF_SESSION_CODE: TFVBFFCDBLabel
          Left = 360
          Top = 16
          HelpType = htKeyword
          HelpKeyword = 'frmKMO_PORT_Payment_Record.F_SESSION_CODE'
          RepositoryItem = dtmEDUMainClient.cxeriRecordHeader
          DataBinding.DataField = 'F_SESSION_CODE'
          DataBinding.DataSource = srcMain
          ParentColor = False
          ParentFont = False
          Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          Style.TextStyle = [fsBold]
          Height = 21
          Width = 163
        end
      end
      inherited pnlRecordDetailSeperator: TFVBFFCPanel
        Width = 642
      end
      inherited pnlEDUPanel1: TFVBFFCPanel
        Top = 537
        Width = 642
        Height = 6
      end
      inherited pnlRecordDetail: TFVBFFCPanel
        Width = 642
        Height = 467
        inherited pnlRecordDetailTitle: TFVBFFCPanel
          Width = 642
        end
        inherited sbxMain: TScrollBox
          Width = 642
          Height = 438
          object cxlblF_KMO_PORT_LNK_PORT_SES_ID2: TFVBFFCLabel
            Left = 8
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Link_Port_Session_Record.F_KMO_PORT_LNK_PORT_SES_ID'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'ID'
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbseF_KMO_PORT_LNK_PORT_SES_ID: TFVBFFCDBSpinEdit
            Left = 150
            Top = 3
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Link_Port_Session_Record.F_KMO_PORT_LNK_PORT_SES_ID'
            RepositoryItem = dtmEDUMainClient.cxediPrimaryKey
            DataBinding.DataField = 'F_KMO_PORT_LNK_PORT_SES_ID'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 1
            Width = 137
          end
          object cxlblF_PROJECT_NR: TFVBFFCLabel
            Left = 8
            Top = 27
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Link_Port_Session_Record.F_PROJECT_NR'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Project nummer'
            FocusControl = cxdbbeF_PROJECT_NR
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_PROJECT_NR: TFVBFFCDBButtonEdit
            Left = 150
            Top = 27
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Link_Port_Session_Record.F_PROJECT_NR'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_PROJECT_NR'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_PROJECT_NRPropertiesButtonClick
            TabOrder = 2
            Width = 264
          end
          object cxlblF_AMOUNT_REQUESTED: TFVBFFCLabel
            Left = 8
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Aangevraagd bedrag'
            FocusControl = cxdbceF_AMOUNT_REQUESTED
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
          object cxdbceF_AMOUNT_REQUESTED: TFVBFFCDBCurrencyEdit
            Left = 150
            Top = 51
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_REQUESTED'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_AMOUNT_REQUESTED'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 3
            Width = 121
          end
          object cxlblF_SESSION_CODE1: TFVBFFCLabel
            Left = 8
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_SESSION_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Sessie'
            FocusControl = cxdbbeF_SESSION_CODE
            ParentColor = False
            ParentFont = False
            Style.StyleController = dtmEDUMainClient.cxescSearchCriteria
          end
          object cxdbbeF_SESSION_CODE: TFVBFFCDBButtonEdit
            Left = 150
            Top = 75
            HelpType = htKeyword
            HelpKeyword = 'frmKMO_PORT_Payment_Record.F_SESSION_CODE'
            RepositoryItem = dtmEDUMainClient.cxeriShowSelectLookup
            DataBinding.DataField = 'F_SESSION_CODE'
            DataBinding.DataSource = srcMain
            ParentFont = False
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            Properties.OnButtonClick = cxdbbeF_SESSION_CODEPropertiesButtonClick
            TabOrder = 4
            Width = 264
          end
          object cxdbceF_AMOUNT_TO_PAY: TFVBFFCDBCurrencyEdit
            Left = 150
            Top = 99
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_TO_PAY'
            RepositoryItem = dtmEDUMainClient.cxeriBedrag
            DataBinding.DataField = 'F_AMOUNT_TO_PAY'
            DataBinding.DataSource = srcMain
            ParentFont = False
            TabOrder = 5
            Width = 121
          end
          object cxlblF_AMOUNT_TO_PAY: TFVBFFCLabel
            Left = 8
            Top = 99
            HelpType = htKeyword
            HelpKeyword = 'frmProgProgram_Record.F_AMOUNT_TO_PAY'
            RepositoryItem = dtmEDUMainClient.cxeriSearchCriteriaLabels
            Caption = 'Te betalen'
            FocusControl = cxdbceF_AMOUNT_TO_PAY
            ParentColor = False
            ParentFont = False
            Transparent = True
          end
        end
        object pnlPriceSchemeInfoSeperator: TFVBFFCPanel
          Left = 0
          Top = 463
          Width = 642
          Height = 4
          Align = alBottom
          BevelOuter = bvNone
          BorderWidth = 4
          ParentColor = True
          TabOrder = 2
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clInactiveCaption
          StyleClientArea.BackColor2 = clInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
      end
    end
    inherited pnlRightSpacer: TFVBFFCPanel
      Left = 800
      Height = 543
    end
    inherited dxnbNavBar: TFVBFFCNavBar
      Height = 543
      inherited dxnbgGeneral: TdxNavBarGroup
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      inherited dxnbgRelatedInfo: TdxNavBarGroup
        Links = <>
      end
    end
    inherited FVBFFCSplitter1: TFVBFFCSplitter
      Height = 543
    end
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Link_Port_Session.cdsRecord
  end
end
