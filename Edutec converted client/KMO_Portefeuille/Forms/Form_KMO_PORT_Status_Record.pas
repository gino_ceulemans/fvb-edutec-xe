unit Form_KMO_PORT_Status_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Form_EDURecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxCheckBox,
  cxDBEdit, cxTextEdit, cxMaskEdit, cxSpinEdit, cxControls, cxContainer,
  cxEdit, cxLabel, Menus, cxSplitter, Unit_PPWFrameWorkInterfaces;

type
  TfrmKMO_PORT_Status_Record = class(TEDURecordView)
    cxlblF_KMO_PORT_STATUS_ID: TFVBFFCLabel;
    cxdbseF_KMO_PORT_STATUS_ID: TFVBFFCDBSpinEdit;
    cxlblF_NAME_NL: TFVBFFCLabel;
    cxdbteF_NAME_NL: TFVBFFCDBTextEdit;
    cxlblF_NAME_FR: TFVBFFCLabel;
    cxdbteF_NAME_FR: TFVBFFCDBTextEdit;
    cxlblF_KMO_PORT_STATUS_ID2: TFVBFFCLabel;
    cxdblblF_KMO_PORT_STATUS_ID: TFVBFFCDBLabel;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    cxdblblF_STATUS_NAME: TFVBFFCDBLabel;
    dlgColor: TColorDialog;
    btnColorBG: TFVBFFCButton;
    btnColorFG: TFVBFFCButton;
    cxlblF_BACKGROUNDCOLOR: TFVBFFCLabel;
    cxlblF_FOREGROUNDCOLOR: TFVBFFCLabel;
    cxteColorExample: TFVBFFCTextEdit;
    procedure btnColorBGClick(Sender: TObject);
    procedure btnColorFGClick(Sender: TObject);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
  private
    procedure SetColors(BGColor: TColor; FGColor: TColor);
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_KMO_PORT_Status, Data_EduMainClient;

{ TfrmKMO_PORT_Status_Record }

function TfrmKMO_PORT_Status_Record.GetDetailName: String;
begin
  Result := cxdblblF_STATUS_NAME.Caption;
end;

procedure TfrmKMO_PORT_Status_Record.btnColorBGClick(Sender: TObject);
begin
  inherited;
  dlgColor.Color := srcMain.DataSet.FieldByName('F_BACKGROUNDCOLOR').Value;
  if dlgColor.Execute then
  begin
    srcMain.DataSet.Edit;
    srcMain.DataSet.FieldByName('F_BACKGROUNDCOLOR').Value := dlgColor.Color;
    SetColors(srcMain.DataSet.FieldByName('F_BACKGROUNDCOLOR').Value, srcMain.DataSet.FieldByName('F_FOREGROUNDCOLOR').Value);
  end;
end;

procedure TfrmKMO_PORT_Status_Record.btnColorFGClick(Sender: TObject);
begin
  inherited;
  dlgColor.Color := srcMain.DataSet.FieldByName('F_FOREGROUNDCOLOR').Value;
  if dlgColor.Execute then
  begin
    srcMain.DataSet.Edit;
    srcMain.DataSet.FieldByName('F_FOREGROUNDCOLOR').Value := dlgColor.Color;
    SetColors(srcMain.DataSet.FieldByName('F_BACKGROUNDCOLOR').Value, srcMain.DataSet.FieldByName('F_FOREGROUNDCOLOR').Value);
  end;
end;

procedure TfrmKMO_PORT_Status_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;
  SetColors(srcMain.DataSet.FieldByName('F_BACKGROUNDCOLOR').Value, srcMain.DataSet.FieldByName('F_FOREGROUNDCOLOR').Value);
end;

procedure TfrmKMO_PORT_Status_Record.SetColors(BGColor: TColor; FGColor: TColor);
begin
  btnColorFG.Colors.Default := FGColor;
  btnColorFG.Colors.Disabled := FGColor;
  btnColorFG.Colors.Hot := FGColor;
  btnColorFG.Colors.Normal := FGColor;
  btnColorFG.Colors.Pressed := FGColor;
  btnColorFG.Invalidate;

  btnColorBG.Colors.Default := BGColor;
  btnColorBG.Colors.Disabled := BGColor;
  btnColorFG.Colors.Hot := BGColor;
  btnColorBG.Colors.Normal := BGColor;
  btnColorBG.Colors.Pressed := BGColor;
  btnColorBG.Invalidate;

  cxteColorExample.Style.Font.Color := FGColor;
  cxteColorExample.Style.Color := BGColor;
  cxteColorExample.Invalidate;
end;

end.