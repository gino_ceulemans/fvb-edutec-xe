unit Form_KMO_Portefeuille_Record;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduRecordView, cxLookAndFeelPainters, ActnList,
  Unit_FVBFFCComponents, DB, Unit_FVBFFCDBComponents, dxNavBarCollns,
  dxNavBarBase, dxNavBar, Unit_FVBFFCDevExpress, cxButtons, ExtCtrls,
  Unit_FVBFFCFoldablePanel, StdCtrls, Buttons, cxDBLabel, cxControls,
  cxContainer, cxEdit, cxLabel, cxDropDownEdit, cxCalc, cxDBEdit,
  cxTextEdit, cxMaskEdit, cxImageComboBox, cxMemo, cxCalendar,
  cxButtonEdit, cxSpinEdit, cxCurrencyEdit, Unit_PPWFrameWorkInterfaces,
  Menus, cxGraphics, cxSplitter, cxGroupBox, cxCheckBox;

type
  TfrmKMO_Portefeuille_Record = class(TEDURecordView)
    cxlblF_KMO_PORTEFEUILLE_ID1: TFVBFFCLabel;
    cxdblblF_KMO_PORTEFEUILLE_ID: TFVBFFCDBLabel;
    cxlblF_PROJECT_NR1: TFVBFFCLabel;
    cxdblblF_PROJECT_NR: TFVBFFCDBLabel;
    pnlPriceSchemeInfoSeperator: TFVBFFCPanel;
    cxlblF_KMO_PORTEFEUILLE_ID2: TFVBFFCLabel;
    cxdbseF_KMO_PORTEFEUILLE_ID: TFVBFFCDBSpinEdit;
    cxlblF_ORG_NAME1: TFVBFFCLabel;
    cxdbbeF_ORG_NAME: TFVBFFCDBButtonEdit;
    cxlblF_PROJECT_NR2: TFVBFFCLabel;
    cxdbteF_PROJECT_NR: TFVBFFCDBTextEdit;
    cxlblF_STATUS_NAME1: TFVBFFCLabel;
    cxdbbeF_STATUS_NAME: TFVBFFCDBButtonEdit;
    cxlblF_REMARK1: TFVBFFCLabel;
    cxdbmmoF_REMARK: TFVBFFCDBMemo;
    cxlblF_AMOUNT_REQUESTED2: TFVBFFCLabel;
    F_AMOUNT_REQUESTED1: TFVBFFCDBCurrencyEdit;
    dbcbF_PAY_TO_SODEXO_OK: TFVBFFCDBCheckBox;
    F_PAY_TO_SODEXO_OK4: TFVBFFCLabel;
    F_PAYMENT_OK1: TFVBFFCLabel;
    dbcbF_PAYMENT_OK: TFVBFFCDBCheckBox;
    FVBFFCLabel1: TFVBFFCLabel;
    FVBFFCLabel2: TFVBFFCLabel;
    FVBFFCLabel3: TFVBFFCLabel;
    FVBFFCLabel4: TFVBFFCLabel;
    FVBFFCLabel5: TFVBFFCLabel;
    FVBFFCLabel6: TFVBFFCLabel;
    FVBFFCLabel7: TFVBFFCLabel;
    FVBFFCLabel8: TFVBFFCLabel;
    FVBFFCDBDateEdit1: TFVBFFCDBDateEdit;
    FVBFFCDBButtonEdit1: TFVBFFCDBButtonEdit;
    FVBFFCLabel9: TFVBFFCLabel;
    FVBFFCDBTextEdit1: TFVBFFCDBTextEdit;
    FVBFFCDBDateEdit2: TFVBFFCDBDateEdit;
    FVBFFCDBMemo1: TFVBFFCDBMemo;
    FVBFFCDBCurrencyEdit1: TFVBFFCDBCurrencyEdit;
    FVBFFCDBCurrencyEdit2: TFVBFFCDBCurrencyEdit;
    FVBFFCDBCurrencyEdit3: TFVBFFCDBCurrencyEdit;
    FVBFFCDBButtonEdit2: TFVBFFCDBButtonEdit;
    procedure cxdbbeF_ORG_NAMEPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxdbbeF_STATUS_NAMEPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCRecordViewInitialise(
      aFrameWorkDataModule: IPPWFrameWorkDataModule);
    procedure cxbtnOKClick(Sender: TObject);
    procedure acShowRecordDetailExecute(Sender: TObject);
    procedure srcMainDataChange(Sender: TObject; Field: TField);
    procedure acShowKMO_PORT_Link_Port_PaymentExecute(Sender: TObject);
    procedure acShowKMO_PORT_Link_Port_SessionExecute(Sender: TObject);
    procedure FVBFFCDBButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure FVBFFCDBButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure ShowRecordDetail( Sender : TObject ); override;
    procedure ShowDetailListView( Sender : TObject;
                                  aDataModuleClassName : String;
                                  aDockSite            : TWinControl ); override;
  public
    { Public declarations }
    function GetDetailName : String; override;
  end;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Data_KMO_Portefeuille, Data_EduMainClient, Unit_FVBFFCInterfaces,
  Unit_PPWFrameWorkClasses, unit_EdutecInterfaces,
  Form_FVBFFCBaseRecordView, Unit_PPWFrameWorkRecordView;

{ TfrmKMO_Portefeuille_Record }

{*****************************************************************************
  This method will return some key info for the current record which will be
  used in the Caption of the Form.

  @Name       TfrmKMO_Portefeuille_Record.GetDetailName
  @author     PIFWizard Generated
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmKMO_Portefeuille_Record.GetDetailName: String;
begin
  Result := cxdblblF_PROJECT_NR.Caption;
end;

{*****************************************************************************
  Overriden ShowDetailListView in which we will also hide Pricing Scheme info.

  @Name       TfrmKMO_Portefeuille_Record.ShowDetailListView
  @author     slesage
  @param      Sender                 The Object from which the method was
                                     invoked.
  @param      aDataModuleClassName   The ClassName of the DetailDataModule.
  @param      aDockSite              The control in which the DetailListview
                                     should be docked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_Portefeuille_Record.ShowDetailListView(Sender: TObject;
  aDataModuleClassName: String; aDockSite: TWinControl);
begin
  inherited;
end;

{*****************************************************************************
  Overriden ShowDetailListView in which we will also Show Pricing Scheme info.

  @Name       TfrmKMO_Portefeuille_Record.ShowRecordDetail
  @author     slesage
  @param      Sender   The Object from which the method was invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_Portefeuille_Record.ShowRecordDetail(Sender: TObject);
begin
  inherited;
end;


procedure TfrmKMO_Portefeuille_Record.cxdbbeF_ORG_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PortefeuilleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PortefeuilleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowOrganisation( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectOrganisation( srcMain.DataSet );
    end;
  end;
end;

procedure TfrmKMO_Portefeuille_Record.cxdbbeF_STATUS_NAMEPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PortefeuilleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PortefeuilleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowStatus( srcMain.DataSet, rvmEdit );
      end
      else
      begin
        aDataModule.SelectStatus( srcMain.DataSet );
      end;
    end;
  end;
end;

{*****************************************************************************
  This event will be executed when the Form is initialised and will be used
  to Enable or Disable some controls.

  @Name       TfrmKMO_Portefeuille_Record.FVBFFCRecordViewInitialise
  @author     slesage
  @param      aFrameWorkDataModule   The DataModule assiciated to the Form.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmKMO_Portefeuille_Record.FVBFFCRecordViewInitialise(
  aFrameWorkDataModule: IPPWFrameWorkDataModule);
begin
  inherited;
  // komende vanaf een andere datamodule -> velden op read-only zetten
  if ( Assigned( aFrameWorkDataModule ) ) and
     ( Assigned( aFrameWorkDataModule.MasterDataModule ) ) then
  begin
    if ( Supports( aFrameWorkDataModule.MasterDataModule, IEDUOrganisationDataModule ) ) then
    begin
      dtmEDUMainClient.SetReadOnlyRepositoryItem( cxdbbeF_ORG_NAME );
    end;
  end;
end;

{*******************************************************************************
  Check if a recalculation is needed in order to have an accurate "base-price"

  @Name       TfrmProgProgram_Record.cxbtnOKClick
  @author     cheuten
  @param      None
  @return     None
  @Exception  None
  @See        None
  @History                  Author                    Description
  28/03/2008          Ivan Van den Bossche            Extra checks (mantis 2345)
********************************************************************************}

procedure TfrmKMO_Portefeuille_Record.cxbtnOKClick(Sender: TObject);
begin
  Inherited;

  if ( Sender = cxbtnApply ) then
  begin
    RefreshVisibleDetailListViews;
  end;

end;

procedure TfrmKMO_Portefeuille_Record.acShowRecordDetailExecute(Sender: TObject);
begin
  inherited;
//  self.FrameWorkDataModule.ListViewDataModule.RefreshListDataSet;
end;


procedure TfrmKMO_Portefeuille_Record.srcMainDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
//
end;

procedure TfrmKMO_Portefeuille_Record.acShowKMO_PORT_Link_Port_PaymentExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmKMO_PORT_Link_Port_Payment', pnlRecordDetail );
end;

procedure TfrmKMO_Portefeuille_Record.acShowKMO_PORT_Link_Port_SessionExecute(
  Sender: TObject);
begin
  inherited;
  ShowDetailListView( Self, 'TdtmKMO_PORT_Link_Port_Session', pnlRecordDetail );
end;

procedure TfrmKMO_Portefeuille_Record.FVBFFCDBButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
  var
  aDataModule : IEDUKMO_PortefeuilleDataModule;
begin
  inherited;
  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PortefeuilleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowSession( srcMain.DataSet, rvmEdit );
      end;
      else aDataModule.SelectSession( srcMain.DataSet );
    end;
  end;

end;

procedure TfrmKMO_Portefeuille_Record.FVBFFCDBButtonEdit2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  aDataModule : IEDUKMO_PortefeuilleDataModule;
begin
  inherited;

  if ( Mode in [ rvmEdit, rvmAdd ] ) and
     ( Supports( FrameWorkDataModule, IEDUKMO_PortefeuilleDataModule, aDataModule ) ) then
  begin
    case aButtonIndex of
      0 :
      begin
        aDataModule.ShowStatus( srcMain.DataSet, rvmEdit );
      end
      else
      begin
        aDataModule.SelectStatus2( srcMain.DataSet );
      end;
    end;
  end;
end;

end.
