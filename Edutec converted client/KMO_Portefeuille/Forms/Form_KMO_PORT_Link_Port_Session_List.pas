unit Form_KMO_PORT_Link_Port_Session_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Form_EduListView, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxBar, dxPSCore,
  dxPScxCommon, dxPScxGridLnk, Unit_FVBFFCDevExpress,
  Unit_FVBFFCDBComponents, Unit_FVBFFCFoldablePanel, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, StdCtrls, cxButtons, ExtCtrls,
  cxDropDownEdit, cxCalendar, cxDBEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxContainer, cxLabel, Menus, ActnList,
  Unit_FVBFFCComponents, Unit_PPWFrameWorkActions, Grids, DBGrids,
  cxCheckBox, cxCalc;

type
  TfrmKMO_PORT_Link_Port_Session_List = class(TEDUListView)
    prdDialog: TPrintDialog;
    cxgrdtblvListF_KMO_PORT_LNK_PORT_SES_ID: TcxGridDBColumn;
    cxgrdtblvListF_KMO_PORTEFEUILLE_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_ID: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn;
    cxgrdtblvListF_SESSION_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListF_PROJECT_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn;
    cxgrdtblvListF_KMO_PORT_STATUS_ID: TcxGridDBColumn;
    cxgrdtblvListF_AMOUNT_REQUESTED: TcxGridDBColumn;
    cxgrdtblvListF_PAY_TO_SODEXO_OK: TcxGridDBColumn;
    cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ORG_NAME: TcxGridDBColumn;
    cxgrdtblvListF_ORG_ORG_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORG_VAT_NR: TcxGridDBColumn;
    cxgrdtblvListF_ORG_SYNERGY_ID: TcxGridDBColumn;
    cxgrdtblvListF_ORG_RSZ_NR: TcxGridDBColumn;
    cxgrdtblvListF_AMOUNT_TO_PAY: TcxGridDBColumn;
    cxgrdtblvListF_PAYMENT_OK: TcxGridDBColumn;
    procedure cxgrdtblvListStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FVBFFCListViewShow(Sender: TObject);
    procedure FVBFFCListViewCreate(Sender: TObject);
    procedure FVBFFCListViewClose(Sender: TObject;
      var Action: TCloseAction);
  private
    { Private declarations }
  protected
    procedure OnCanFocusRecord(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; var AAllow: Boolean);
    procedure StoreActiveViewToIni; override;
    procedure RestoreActiveViewFromIni; override;
  public
    { Public declarations }
    function GetFilterString : String; override;
  end;


implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Data_KMO_PORT_Link_Port_Session, Data_EduMainClient, Form_FVBFFCBaseListView,
  Unit_FVBFFCInterfaces, unit_EdutecInterfaces, unit_PPWFrameworkClasses,
  unit_Const, printers, Unit_FVBFFCUtils, Unit_Global;

{ TfrmKMO_PORT_Link_Port_Session_List }


{*****************************************************************************
  This method will be used to build the Where clause based on the Search
  Criteria entered by the  user.

  @Name       TfrmKMO_PORT_Link_Port_Session_List.GetFilterString
  @author     slesage
  @return     Returns a Where clause based on the Search Criteria entered by
              the  user.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmKMO_PORT_Link_Port_Session_List.GetFilterString: String;
var
  aFilterString : String;
begin
  aFilterString := '';
  Result := aFilterString;
end;

procedure TfrmKMO_PORT_Link_Port_Session_List.FVBFFCListViewShow(Sender: TObject);
begin
  inherited;
    with cxgrdtblvList.Controller do
    if (FocusedRecord <> nil) and FocusedRecord.Selected then
        FocusedRecord.Selected := False;
end;

procedure TfrmKMO_PORT_Link_Port_Session_List.FVBFFCListViewCreate(Sender: TObject);
begin
  inherited;
  // Add OnCanFocusRecord event to grid
  cxgrdtblvList.OnCanFocusRecord := OnCanFocusRecord;
  cxbtnEDUButton1.Enabled := False;
end;

procedure TfrmKMO_PORT_Link_Port_Session_List.OnCanFocusRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  AAllow := True;
  cxbtnEDUButton1.Enabled := True;
end;

procedure TfrmKMO_PORT_Link_Port_Session_List.RestoreActiveViewFromIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].RestoreFromIniFile( LocalSettingsIniFileName, True, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;

procedure TfrmKMO_PORT_Link_Port_Session_List.StoreActiveViewToIni;
var
  aStorageName : String;
  lcv: Integer;
begin
  if ( Assigned( MasterRecordView ) ) then
  begin
    aStorageName := MasterRecordView.Form.ClassName + '.' + Self.ClassName;
  end
  else
  begin
    aStorageName := Self.ClassName;
  end;

  for lcv := 0 to cxgrdList.ViewCount - 1 do
  begin
    cxgrdList.Views[lcv].StoreToIniFile( LocalSettingsIniFileName, False, [], aStorageName + '_' + IntToStr(lcv));
  end;
end;


procedure TfrmKMO_PORT_Link_Port_Session_List.FVBFFCListViewClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

    // Make sure that changes made to the grid will be applied to the database.
    if (srcMain.DataSet.Active) then
    begin
        if srcMain.DataSet.State in [dsInsert, dsEdit] then
          srcMain.DataSet.Post;

    end;
end;

procedure TfrmKMO_PORT_Link_Port_Session_List.cxgrdtblvListStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if not (aRecord.Expandable) then
  begin
    // kleur
    {
    aStyle := dtmEDUMainClient.cxsKMOPortefeuille;
    aStyle.Color := aRecord.Values[ cxgrdtblvListF_BACKGROUNDCOLOR.Index ];
    aStyle.TextColor := aRecord.Values[ cxgrdtblvListF_FOREGROUNDCOLOR.Index ];
    }
  end;
end;

end.
