inherited frmKMO_PORT_Link_Port_Session_List: TfrmKMO_PORT_Link_Port_Session_List
  Left = 349
  Top = 168
  Width = 900
  Height = 598
  Caption = 'KMO Portefeuille - Sessie'
  Constraints.MinWidth = 900
  Registered = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlSelection: TPanel
    Top = 530
    Width = 892
    inherited cxbtnEDUButton1: TFVBFFCButton
      Left = 676
    end
    inherited cxbtnEDUButton2: TFVBFFCButton
      Left = 788
    end
  end
  inherited pnlList: TFVBFFCPanel
    Width = 884
    Height = 522
    inherited cxgrdList: TcxGrid
      Top = 174
      Width = 884
      Height = 348
      inherited cxgrdtblvList: TcxGridDBTableView
        DataController.DataModeController.DetailInSQLMode = True
        DataController.DataModeController.GridMode = False
        DataController.DataModeController.SmartRefresh = True
        DataController.DetailKeyFieldNames = 'F_KMO_PORT_LNK_PORT_SES_ID'
        DataController.KeyFieldNames = 'F_KMO_PORT_LNK_PORT_SES_ID'
        Styles.Background = nil
        Styles.Content = nil
        Styles.ContentEven = nil
        Styles.ContentOdd = nil
        Styles.FilterBox = nil
        Styles.Inactive = nil
        Styles.IncSearch = nil
        Styles.Selection = nil
        Styles.OnGetContentStyle = cxgrdtblvListStylesGetContentStyle
        Styles.Footer = nil
        Styles.Group = nil
        Styles.GroupByBox = nil
        Styles.Header = nil
        Styles.Indicator = nil
        Styles.Preview = nil
        object cxgrdtblvListF_KMO_PORT_LNK_PORT_SES_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORT_LNK_PORT_SES_ID'
        end
        object cxgrdtblvListF_PROJECT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_PROJECT_NR'
          Width = 136
        end
        object cxgrdtblvListF_AMOUNT_REQUESTED: TcxGridDBColumn
          DataBinding.FieldName = 'F_AMOUNT_REQUESTED'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
          Width = 143
        end
        object cxgrdtblvListF_AMOUNT_TO_PAY: TcxGridDBColumn
          DataBinding.FieldName = 'F_AMOUNT_TO_PAY'
          RepositoryItem = dtmEDUMainClient.cxeriBedrag
        end
        object cxgrdtblvListF_SESSION_CODE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_CODE'
        end
        object cxgrdtblvListF_KMO_PORTEFEUILLE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORTEFEUILLE_ID'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_ID'
          Visible = False
        end
        object cxgrdtblvListF_SESSION_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_NAME'
          Width = 184
        end
        object cxgrdtblvListF_SESSION_START_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_START_DATE'
          Width = 132
        end
        object cxgrdtblvListF_SESSION_END_DATE: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_END_DATE'
          Width = 129
        end
        object cxgrdtblvListF_SESSION_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_SESSION_STATUS_NAME'
          Width = 112
        end
        object cxgrdtblvListF_ORGANISATION_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORGANISATION_ID'
          Visible = False
        end
        object cxgrdtblvListF_KMO_PORT_STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_KMO_PORT_STATUS_ID'
          Visible = False
        end
        object cxgrdtblvListF_PAY_TO_SODEXO_OK: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAY_TO_SODEXO_OK'
          Visible = False
        end
        object cxgrdtblvListF_PAYMENT_OK: TcxGridDBColumn
          DataBinding.FieldName = 'F_PAYMENT_OK'
          Visible = False
        end
        object cxgrdtblvListF_STATUS_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_STATUS_NAME'
          Visible = False
        end
        object cxgrdtblvListF_ORG_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_NAME'
          Visible = False
        end
        object cxgrdtblvListF_ORG_ORG_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_ORG_NR'
          Visible = False
        end
        object cxgrdtblvListF_ORG_VAT_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_VAT_NR'
          Visible = False
        end
        object cxgrdtblvListF_ORG_SYNERGY_ID: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_SYNERGY_ID'
          Visible = False
        end
        object cxgrdtblvListF_ORG_RSZ_NR: TcxGridDBColumn
          DataBinding.FieldName = 'F_ORG_RSZ_NR'
          Visible = False
        end
      end
    end
    inherited pnlFormTitle: TFVBFFCPanel
      Top = 149
      Width = 884
    end
    inherited pnlSearchCriteriaSpacer: TFVBFFCPanel
      Top = 145
      Width = 884
    end
    inherited pnlSearchCriteria: TPPWCollapsablePanel
      Width = 884
      Height = 145
      ExpandedHeight = 145
      inherited pnlSearchCriteriaButtons: TPanel
        Top = 112
        Width = 882
        Height = 32
        DesignSize = (
          882
          32)
        inherited cxbtnApplyFilter: TFVBFFCButton
          Left = 714
        end
        inherited cxbtnClearFilter: TFVBFFCButton
          Left = 802
        end
      end
    end
  end
  inherited pnlListTopSpacer: TFVBFFCPanel
    Width = 892
  end
  inherited pnlListBottomSpacer: TFVBFFCPanel
    Top = 526
    Width = 892
  end
  inherited pnlListRightSpacer: TFVBFFCPanel
    Left = 888
    Height = 522
  end
  inherited pnlListLeftSpacer: TFVBFFCPanel
    Height = 522
  end
  inherited srcMain: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Link_Port_Session.cdsList
    Left = 48
    Top = 264
  end
  inherited dxcpGridPrinter: TFVBFFCComponentPrinter
    inherited dxgrptlGridLink: TdxGridReportLink
      BuiltInReportLink = True
    end
  end
  inherited dxbmPopupMenu: TdxBarManager
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    DockControlHeights = (
      0
      0
      0
      0)
  end
  inherited dxbpmnGrid: TdxBarPopupMenu
    ItemLinks = <
      item
        Item = dxbbEdit
        Visible = True
      end
      item
        Item = dxbbView
        Visible = True
      end
      item
        Item = dxbbAdd
        Visible = True
      end
      item
        Item = dxbbDelete
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbRefresh
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxBBCustomizeColumns
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbPrintGrid
        Visible = True
      end
      item
        BeginGroup = True
        Item = dxbbExportXLS
        Visible = True
      end
      item
        Item = dxbbExportXML
        Visible = True
      end
      item
        Item = dxbbExportHTML
        Visible = True
      end>
  end
  inherited srcSearchCriteria: TFVBFFCDataSource
    DataSet = dtmKMO_PORT_Link_Port_Session.cdsSearchCriteria
    Left = 144
    Top = 254
  end
  object prdDialog: TPrintDialog
    Left = 332
    Top = 388
  end
end
