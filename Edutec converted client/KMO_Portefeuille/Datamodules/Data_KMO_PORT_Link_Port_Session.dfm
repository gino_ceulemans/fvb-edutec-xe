inherited dtmKMO_PORT_Link_Port_Session: TdtmKMO_PORT_Link_Port_Session
  OnCreate = FVBFFCDataModuleCreate
  AutoOpenDataSets = False
  KeyFields = 'F_KMO_PORT_LNK_PORT_SES_ID'
  ListViewClass = 'TfrmKMO_PORT_Link_Port_Session_List'
  RecordViewClass = 'TfrmKMO_PORT_Link_Port_Session_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Height = 433
  Width = 455
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_KMO_PORT_LNK_PORT_SES_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_KMO_PORT_LNK_PORT_SES_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object cdsListF_KMO_PORTEFEUILLE_ID: TIntegerField
      DisplayLabel = 'KMO Portefeuille (ID)'
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie (ID)'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_AMOUNT_TO_PAY: TFloatField
      DisplayLabel = 'Te betalen'
      FieldName = 'F_AMOUNT_TO_PAY'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie referentie'
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Sessie Startdatum'
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Sessie Einddatum'
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = []
    end
    object cdsListF_SESSION_STATUS_NAME: TStringField
      DisplayLabel = 'Sessie status'
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_PROJECT_NR: TStringField
      DisplayLabel = 'Project nummer'
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = []
      Size = 30
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie (ID)'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = []
    end
    object cdsListF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Portefeuille Status (ID)'
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = []
    end
    object cdsListF_AMOUNT_REQUESTED: TFloatField
      DisplayLabel = 'Bedrag aangevraagd'
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = []
    end
    object cdsListF_PAY_TO_SODEXO_OK: TBooleanField
      DisplayLabel = 'Storting OK @ Sodexo'
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = []
    end
    object cdsListF_PAYMENT_OK: TBooleanField
      DisplayLabel = 'Betaling OK'
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = []
    end
    object cdsListF_STATUS_NAME: TStringField
      DisplayLabel = 'Portefeuille Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_ORG_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORG_NAME'
      ProviderFlags = []
      Size = 64
    end
    object cdsListF_ORG_ORG_NR: TStringField
      DisplayLabel = 'Ondernemingsnummer'
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = []
      Size = 10
    end
    object cdsListF_ORG_VAT_NR: TStringField
      DisplayLabel = 'BTW nummer'
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = []
    end
    object cdsListF_ORG_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = []
    end
    object cdsListF_ORG_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nummer'
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = []
      Size = 14
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    AfterInsert = cdsRecordAfterInsert
    object cdsRecordF_KMO_PORT_LNK_PORT_SES_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_KMO_PORT_LNK_PORT_SES_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_KMO_PORTEFEUILLE_ID: TIntegerField
      DisplayLabel = 'KMO Portefeuille (ID)'
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie (ID)'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_AMOUNT_TO_PAY: TFloatField
      DisplayLabel = 'Te betalen'
      FieldName = 'F_AMOUNT_TO_PAY'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie referentie'
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Sessie Startdatum'
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Sessie Einddatum'
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_STATUS_NAME: TStringField
      DisplayLabel = 'Sessie status'
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_PROJECT_NR: TStringField
      DisplayLabel = 'Project nummer'
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie (ID)'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Portefeuille Status (ID)'
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_AMOUNT_REQUESTED: TFloatField
      DisplayLabel = 'Bedrag aangevraagd'
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PAY_TO_SODEXO_OK: TBooleanField
      DisplayLabel = 'Storting OK @ Sodexo'
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PAYMENT_OK: TBooleanField
      DisplayLabel = 'Betaling OK'
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_STATUS_NAME: TStringField
      DisplayLabel = 'Portefeuille Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_ORG_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_ORG_ORG_NR: TStringField
      DisplayLabel = 'Ondernemingsnummer'
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsRecordF_ORG_VAT_NR: TStringField
      DisplayLabel = 'BTW nummer'
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ORG_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ORG_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nummer'
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmKMO_PORT_Link_Port_Session'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmKMO_PORT_Link_Port_Session'
  end
end
