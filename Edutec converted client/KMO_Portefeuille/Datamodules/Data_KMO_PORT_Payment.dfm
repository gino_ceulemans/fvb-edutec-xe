inherited dtmKMO_PORT_Payment: TdtmKMO_PORT_Payment
  OnCreate = FVBFFCDataModuleCreate
  AutoOpenDataSets = False
  KeyFields = 'F_KMO_PORT_PAYMENT_ID'
  ListViewClass = 'TfrmKMO_PORT_Payment_List'
  RecordViewClass = 'TfrmKMO_PORT_Payment_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Height = 433
  Width = 455
  DetailDataModules = <
    item
      Caption = 'Gekoppelde betalingen'
      ForeignKeys = 'F_KMO_PORT_PAYMENT_ID'
      DataModuleClass = 'TdtmKMO_PORT_Link_Port_Payment'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    Connection = dtmEDUMainClient.adocnnMain
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_KMO_PORT_PAYMENT_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_KMO_PORT_PAYMENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_INVOICE_NR: TStringField
      DisplayLabel = 'Faktuur nummer'
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsListF_AMOUNT_PAYED: TFloatField
      DisplayLabel = 'Bedrag betaald'
      FieldName = 'F_AMOUNT_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_DATE_PAYED: TDateTimeField
      DisplayLabel = 'Datum betaald'
      FieldName = 'F_DATE_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_REMARK: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsListSUM_AMOUNT_LINKED: TFloatField
      DisplayLabel = 'Bedrag toegewezen'
      FieldName = 'SUM_AMOUNT_LINKED'
      ReadOnly = True
    end
    object cdsListF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie Referentie'
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Sessie Start Datum'
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Sessie Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_STATUS_NAME: TStringField
      DisplayLabel = 'Sessie Status'
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    AfterInsert = cdsRecordAfterInsert
    object cdsRecordF_KMO_PORT_PAYMENT_ID: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'F_KMO_PORT_PAYMENT_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_INVOICE_NR: TStringField
      DisplayLabel = 'Faktuur nummer'
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsRecordF_AMOUNT_PAYED: TFloatField
      DisplayLabel = 'Bedrag betaald'
      FieldName = 'F_AMOUNT_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_DATE_PAYED: TDateTimeField
      DisplayLabel = 'Datum betaald'
      FieldName = 'F_DATE_PAYED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_REMARK: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsRecordF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie Referentie'
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Sessie Start Datum'
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Sessie Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_STATUS_NAME: TStringField
      DisplayLabel = 'Sessie Status'
      FieldName = 'F_SESSION_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaF_INVOICE_NR: TStringField
      DisplayLabel = 'Faktuur nummer'
      FieldName = 'F_INVOICE_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsSearchCriteriaF_DATE_PAYED: TDateTimeField
      DisplayLabel = 'Datum betaald van'
      FieldName = 'F_DATE_PAYED_FROM'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_DATE_PAYED2: TDateTimeField
      DisplayLabel = 'Datum betaald tot'
      FieldName = 'F_DATE_PAYED_TO'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessie ( ID )'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_SESSION_CODE: TStringField
      DisplayLabel = 'Sessie Referentie'
      FieldName = 'F_SESSION_CODE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_SESSION_NAME: TStringField
      DisplayLabel = 'Sessie'
      FieldName = 'F_SESSION_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsSearchCriteriaF_SESSION_START_DATE: TDateTimeField
      DisplayLabel = 'Sessie Start Datum'
      FieldName = 'F_SESSION_START_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_SESSION_END_DATE: TDateTimeField
      DisplayLabel = 'Sessie Eind Datum'
      FieldName = 'F_SESSION_END_DATE'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmKMO_PORT_Payment'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmKMO_PORT_Payment'
  end
end
