unit Data_KMO_Portefeuille;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, Unit_PPWFrameWorkActions, MConnect, unit_EdutecInterfaces, Unit_PPWFrameWorkInterfaces,
  Datasnap.DSConnect;

type
  TdtmKMO_Portefeuille = class(TEDUDataModule, IEDUKMO_PortefeuilleDataModule, IPPWFrameWorkDataModuleDefaultFilter )
    cdsListF_KMO_PORTEFEUILLE_ID: TIntegerField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_PROJECT_NR: TStringField;
    cdsListF_KMO_PORT_STATUS_ID: TIntegerField;
    cdsListF_AMOUNT_REQUESTED: TFloatField;
    cdsListF_PAY_TO_SODEXO_OK: TBooleanField;
    cdsListF_REMARK: TStringField;
    cdsListF_STATUS_NAME: TStringField;
    cdsListF_STATUS_NL: TStringField;
    cdsListF_STATUS_FR: TStringField;
    cdsListF_FOREGROUNDCOLOR: TIntegerField;
    cdsListF_BACKGROUNDCOLOR: TIntegerField;
    cdsListF_ORG_NAME: TStringField;
    cdsListF_ORG_ORG_NR: TStringField;
    cdsListF_ORG_VAT_NR: TStringField;
    cdsListF_ORG_SYNERGY_ID: TLargeintField;
    cdsListF_ORG_RSZ_NR: TStringField;
    cdsRecordF_KMO_PORTEFEUILLE_ID: TIntegerField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsRecordF_PROJECT_NR: TStringField;
    cdsRecordF_KMO_PORT_STATUS_ID: TIntegerField;
    cdsRecordF_AMOUNT_REQUESTED: TFloatField;
    cdsRecordF_PAY_TO_SODEXO_OK: TBooleanField;
    cdsRecordF_REMARK: TStringField;
    cdsRecordF_STATUS_NAME: TStringField;
    cdsRecordF_STATUS_NL: TStringField;
    cdsRecordF_STATUS_FR: TStringField;
    cdsRecordF_FOREGROUNDCOLOR: TIntegerField;
    cdsRecordF_BACKGROUNDCOLOR: TIntegerField;
    cdsRecordF_ORG_NAME: TStringField;
    cdsRecordF_ORG_ORG_NR: TStringField;
    cdsRecordF_ORG_VAT_NR: TStringField;
    cdsRecordF_ORG_SYNERGY_ID: TLargeintField;
    cdsRecordF_ORG_RSZ_NR: TStringField;
    cdsSearchCriteriaF_KMO_PORTEFEUILLE_ID: TIntegerField;
    cdsSearchCriteriaF_ORGANISATION_ID: TIntegerField;
    cdsSearchCriteriaF_ORG_NAME: TStringField;
    cdsSearchCriteriaF_ORG_VAT_NR: TStringField;
    cdsSearchCriteriaF_ORG_RSZ_NR: TStringField;
    cdsSearchCriteriaF_PROJECT_NR: TStringField;
    cdsSearchCriteriaF_KMO_PORT_STATUS_ID: TIntegerField;
    cdsSearchCriteriaF_STATUS_NAME: TStringField;
    cdsSearchCriteriaF_PAY_TO_SODEXO_OK: TBooleanField;
    cdsListF_PAYMENT_OK: TBooleanField;
    cdsRecordF_PAYMENT_OK: TBooleanField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_ANNULATION_REASON: TStringField;
    cdsListF_PAYMENT_AMOUNT1: TBCDField;
    cdsListF_PAYMENT_AMOUNT2: TBCDField;
    cdsListF_PAYMENT_AMOUNT3: TBCDField;
    cdsListF_SESSION: TStringField;
    cdsListF_PROGRAM: TStringField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_ANNULATION_REASON: TStringField;
    cdsRecordF_PAYMENT_AMOUNT1: TBCDField;
    cdsRecordF_PAYMENT_AMOUNT2: TBCDField;
    cdsRecordF_PAYMENT_AMOUNT3: TBCDField;
    cdsRecordF_SESSION: TStringField;
    cdsRecordF_PROGRAM: TStringField;
    cdsListF_DEMAND_DATE: TDateTimeField;
    cdsRecordF_DEMAND_DATE: TDateTimeField;
    cdsListF_SESSION_DATE: TDateTimeField;
    cdsRecordF_SESSION_DATE: TDateTimeField;
    cdsListF_PAYMENT_STATUS_ID: TIntegerField;
    cdsListF_PAYMENT_STATUS_NAME: TStringField;
    cdsRecordF_PAYMENT_STATUS_ID: TIntegerField;
    cdsRecordF_PAYMENT_STATUS_NAME: TStringField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure prvRecordGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleCreate(Sender: TObject);
    procedure cdsRecordAfterInsert(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
  private
  protected
    { Protected declarations }
    function IsListViewActionAllowed         ( aAction         : TPPWFrameWorkListViewAction ) : Boolean; override;

    procedure SelectStatus         ( aDataSet : TDataSet ); virtual;
    procedure SelectStatus2         ( aDataSet : TDataSet ); virtual;
    procedure SelectStatusFilter   ( aDataSet : TDataSet ); virtual;
    procedure SelectOrganisationFilter   ( aDataSet : TDataSet ); virtual;
    procedure SelectOrganisation ( aDataSet : TDataSet ); virtual;
    procedure SelectSession         ( aDataSet : TDataSet ); virtual;
    procedure SelectSessionFilter   ( aDataSet : TDataSet ); virtual;
    procedure ShowStatus           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowOrganisation ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure ShowSession           ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectKMO_Portefeuille( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowKMO_Portefeuille  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    procedure InitDefaultFilter(ASearchCriteria: TClientDataSet);
  end;

  procedure CopyKMO_PortefeuilleFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

uses
  Data_EduMainClient, Data_FVBFFCBaseDataModule, Data_KMO_PORT_Status,
  DateUtils, Unit_PPWFrameWorkDataModule, Data_Organisation, Data_SesSession;

{$R *.dfm}

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmKMO_Portefeuille.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmKMO_Portefeuille.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_KMO_PORTEFEUILLE_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This procedure will be used to copy KMO Portefeuille related fields from one
  DataSet to another DataSet.

  @Name       CopyKMO_PortefeuilleFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyKMO_PortefeuilleFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_KMO_PORTEFEUILLE_ID' ).AsInteger :=
        aSource.FieldByName( 'F_KMO_PORTEFEUILLE_ID' ).AsInteger;
    end;

    { Copy the Other Fields }
    case aCopyTo of
      critKMO_Portefeuille_To_Link_Payment :
      begin
        aDestination.FieldByName( 'F_PROJECT_NR' ).AsString :=
          aSource.FieldByName( 'F_PROJECT_NR' ).AsString;
        aDestination.FieldByName( 'F_AMOUNT_REQUESTED' ).AsFloat :=
          aSource.FieldByName( 'F_AMOUNT_REQUESTED' ).AsFloat;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to select one or more Status Records.

  @Name       TdtmKMO_Portefeuille.SelectStatus
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmKMO_Portefeuille.SelectStatus(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_KMO_PORT_STATUS_ID' );
  // Controleer of deze collectie van statussen gerelateerd zijn aan
  // Sessie door boolean te controleren "F_SESSION"
  aWhere   := '(F_KMO_PORT_STATUS_ID < 20)';
  aCopyTo  := critDefault;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := aWhere +
      'AND (F_KMO_PORT_STATUS_ID <> ' + aIDField.AsString + ') ';
  end;

  TdtmKMO_PORT_Status.SelectStatus( aDataSet, aWhere, False, aCopyTo );

end;

{*****************************************************************************
  This method will be used to allow the user to Show a User.

  @Name       TdtmKMO_Portefeuille.ShowStatus
  @author     slesage
  @param      aDataSet         The DataSet in which we should copy some
                               information from the Shown Record.
  @param      aRecordViewMode  The Mode in which the record should be shown.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmKMO_Portefeuille.ShowStatus(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
  aCopyTo  : TCopyRecordInformationTo;
begin
  aCopyTo := critDefault;
  aIDField := aDataSet.FindField( 'F_KMO_PORT_STATUS_ID' );

  { Only execute the ShowXXX Method if the ID Field was found and it
    didn't contain a NULL Value or the entity should be shown in Add Mode. }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmKMO_PORT_Status.ShowStatus( aDataSet, aRecordViewMode, aCopyTo );
  end;
end;

procedure TdtmKMO_Portefeuille.prvRecordGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_KMO_PORTEFEUILLE';
end;

{*****************************************************************************************
  This method will be used to initialise some Additional Fields on the DataSet.

  @Name       TdtmKMO_Portefeuille.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
  @History          Author                  Description
  11/07/2008       Ivan Van den Bossche     Added initialization of F_VIRTUAL_SESSION field
*******************************************************************************************}

procedure TdtmKMO_Portefeuille.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_KMO_PORT_STATUS_ID' ).AsInteger := 10;
  aDataSet.FieldByName( 'F_STATUS_NAME' ).AsString :=
    dtmEDUMainClient.GetNameForKMO_Portefeuille_State( 10 );
  aDataSet.FieldByName( 'F_AMOUNT_REQUESTED' ).AsFloat := 0;
  aDataSet.FieldByName( 'F_PAY_TO_SODEXO_OK' ).AsBoolean := False;
  aDataSet.FieldByName( 'F_PAYMENT_OK' ).AsBoolean := False;
end;

{*****************************************************************************
  This method will be used to initialise some Foreign Key Fields on the DataSet.

  @Name       TdtmKMO_Portefeuille.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmKMO_Portefeuille.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUOrganisationDataModule ) ) then
    begin
      CopyOrganisationFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critOrganisationToKMOPortefeuille );
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Session Records.

  @Name       TdtmKMO_Portefeuille.SelectKMO_Portefeuille
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmKMO_Portefeuille.SelectKMO_Portefeuille(aDataSet: TDataSet;
  aWhere: String; aMultiSelect: Boolean;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmKMO_Portefeuille', aWhere ,
       aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyKMO_PortefeuilleFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Session
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmKMO_Portefeuille.ShowKMO_Portefeuille
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmKMO_Portefeuille.ShowKMO_Portefeuille(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmKMO_Portefeuille',
       'F_KMO_PORTEFEUILLE_ID = ' + aDataSet.FieldByName( 'F_KMO_PORTEFEUILLE_ID' ).AsString ,
       aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyKMO_PortefeuilleFieldValues( aSelectedRecords, aDataSet,
        ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  Name           : IsListViewActionAllowed
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Listview action "Add" is not allowed when the session entity
                   is docked as a detail of the organisation recordview.
                   This is needed because when an "Add" action was allowed, then
                   an errormessage "F_ORGANISATION_ID does not exist" occurs.
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/09/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TdtmKMO_Portefeuille.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
begin
  Result := Inherited IsListViewActionAllowed( aAction );
end;


procedure TdtmKMO_Portefeuille.FVBFFCDataModuleCreate(Sender: TObject);
begin
  inherited;
end;

{*****************************************************************************
  Name           : TdtmKMO_Portefeuille.InitDefaultFilter
  Author         : Ivan Van den Bossche
  Arguments      : ASearchCriteria
  Return Values  : None
  Exceptions     : None
  Description    : This method is supplied so we can initialize a temp filter
                    before the form is activated. The form will show the default
                    filter as well.  The user could undo the filter...
  History        :

  Date         By                   Description
  ----         --                   -----------
  07/09/2007   ivdbossche           Initial creation of the procedure.
 *****************************************************************************}
procedure TdtmKMO_Portefeuille.InitDefaultFilter(
  ASearchCriteria: TClientDataSet);
begin
  cdsSearchCriteria.Edit;
  cdsSearchCriteriaF_ORG_NAME.Value := ASearchCriteria.FieldByName('F_ORG_NAME').AsString;

  cdsSearchCriteria.Post;

end;

procedure TdtmKMO_Portefeuille.cdsRecordAfterInsert(DataSet: TDataSet);
begin
  inherited;
//
end;

procedure TdtmKMO_Portefeuille.cdsListAfterPost(DataSet: TDataSet);
begin
  cdsList.Tag := 1; // Make sure that changes will be posted to the database.
  inherited;

end;

procedure TdtmKMO_Portefeuille.SelectStatusFilter(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_KMO_PORT_STATUS_ID' );
  aWhere   := '';
  aCopyTo  := critDefault;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
{
  The status 'Geattesteerd' can not be selected manually !
  it will be set automatically if all of the enrolments are changed to 'Geattesteerd'
}
    aWhere := aWhere +
      ' (F_KMO_PORT_STATUS_ID <> ' + aIDField.AsString + ') ';
  end;

  TdtmKMO_PORT_Status.SelectStatus( aDataSet, aWhere, False, aCopyTo );

end;

procedure TdtmKMO_Portefeuille.SelectOrganisationFilter(aDataSet: TDataSet);
var
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aWhere   := '';
  aCopyTo  := critOrganisationToKMOPortefeuille;

  TdtmOrganisation.SelectOrganisation( aDataSet, aWhere, False, aCopyTo );

end;

procedure TdtmKMO_Portefeuille.SelectOrganisation(aDataSet: TDataSet);
var
  aWhere : String;
begin
  aWhere := '';

  TdtmOrganisation.SelectOrganisation (aDataSet, aWhere, false, critOrganisationToKMOPortefeuille, false);
end;

procedure TdtmKMO_Portefeuille.ShowOrganisation(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_ORGANISATION_ID' );
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmOrganisation.ShowOrganisation( aDataSet, aRecordViewMode, critOrganisationToKMOPortefeuille );
  end;
end;

procedure TdtmKMO_Portefeuille.SelectSession(aDataSet: TDataSet);
var
  aWhere : String;
begin
  aWhere := '';

  TdtmSesSession.SelectSession (aDataSet, aWhere, false, critSessionToKMOPortefeuille);
end;

procedure TdtmKMO_Portefeuille.SelectSessionFilter(aDataSet: TDataSet);
var
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aWhere   := '';
  aCopyTo  := critSessionToKMOPortefeuille;

  TdtmSesSession.SelectSession( aDataSet, aWhere, False, aCopyTo );
end;

procedure TdtmKMO_Portefeuille.ShowSession(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_SESSION_ID' );
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmSesSession.ShowSession( aDataSet, aRecordViewMode, critSessionToKMOPortefeuille );
  end;
end;

procedure TdtmKMO_Portefeuille.SelectStatus2(aDataSet: TDataSet);
var
  aIDField : TField;
  aWhere   : String;
  aCopyTo  : TCopyRecordInformationTo;
begin
  { Get the ID Field }
  aIDField := aDataSet.FindField( 'F_KMO_PORT_STATUS_ID' );
  // Controleer of deze collectie van statussen gerelateerd zijn aan
  // Sessie door boolean te controleren "F_SESSION"
  aWhere   := '(F_KMO_PORT_STATUS_ID >= 20)';
  aCopyTo  := critDefault;

  { If the ID Field was found and it didn't contain a NULL Value, then we
    can build a Where clause so the current Language isn't shown in the List }
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) ) then
  begin
    aWhere := aWhere +
      'AND (F_KMO_PORT_STATUS_ID <> ' + aIDField.AsString + ') ';
  end;

  TdtmKMO_PORT_Status.SelectStatus( aDataSet, aWhere, False, critKMO_PORT_PAYMENT_STATUS );

end;

end.
