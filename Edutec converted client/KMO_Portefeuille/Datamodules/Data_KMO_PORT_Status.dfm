inherited dtmKMO_PORT_Status: TdtmKMO_PORT_Status
  KeyFields = 'F_KMO_PORT_STATUS_ID'
  ListViewClass = 'TfrmKMO_PORT_Status_List'
  RecordViewClass = 'TfrmKMO_PORT_Status_Record'
  Registered = True
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  inherited qryList: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvListGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_KMO_PORT_STATUS_ID'
    end
    object cdsListF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Visible = False
      Size = 128
    end
    object cdsListF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Visible = False
      Size = 128
    end
    object cdsListF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
      Visible = False
    end
    object cdsListF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
      Visible = False
    end
    object cdsListF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      Size = 128
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    object cdsRecordF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Status ( ID )'
      FieldName = 'F_KMO_PORT_STATUS_ID'
      Required = True
    end
    object cdsRecordF_NAME_NL: TStringField
      DisplayLabel = 'Naam ( NL )'
      FieldName = 'F_NAME_NL'
      Required = True
      Size = 128
    end
    object cdsRecordF_NAME_FR: TStringField
      DisplayLabel = 'Naam ( FR )'
      FieldName = 'F_NAME_FR'
      Required = True
      Size = 128
    end
    object cdsRecordF_FOREGROUNDCOLOR: TIntegerField
      FieldName = 'F_FOREGROUNDCOLOR'
    end
    object cdsRecordF_BACKGROUNDCOLOR: TIntegerField
      FieldName = 'F_BACKGROUNDCOLOR'
    end
    object cdsRecordF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Visible = False
      Size = 128
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmKMO_PORT_Status'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmKMO_PORT_Status'
  end
end
