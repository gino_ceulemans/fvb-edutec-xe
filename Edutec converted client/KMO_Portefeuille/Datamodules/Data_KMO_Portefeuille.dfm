inherited dtmKMO_Portefeuille: TdtmKMO_Portefeuille
  OnCreate = FVBFFCDataModuleCreate
  AutoOpenDataSets = False
  KeyFields = 'F_KMO_PORTEFEUILLE_ID'
  ListViewClass = 'TfrmKMO_Portefeuille_List'
  RecordViewClass = 'TfrmKMO_Portefeuille_Record'
  Registered = True
  AutoOpenWhenSelection = False
  OnInitialisePrimaryKeyFields = FVBFFCDataModuleInitialisePrimaryKeyFields
  Height = 433
  Width = 455
  DetailDataModules = <
    item
      Caption = 'Gekoppelde betalingen'
      ForeignKeys = 'F_KMO_PORTEFEUILLE_ID'
      DataModuleClass = 'TdtmKMO_PORT_Link_Port_Payment'
      AutoCreate = False
    end
    item
      Caption = 'Sessies'
      ForeignKeys = 'F_KMO_PORTEFEUILLE_ID'
      DataModuleClass = 'TdtmKMO_PORT_Link_Port_Session'
      AutoCreate = False
    end>
  inherited qryList: TFVBFFCQuery
    Connection = nil
    CursorType = ctStatic
  end
  inherited qryRecord: TFVBFFCQuery
    CursorType = ctStatic
  end
  inherited prvList: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited prvRecord: TFVBFFCDataSetProvider
    OnGetTableName = prvRecordGetTableName
  end
  inherited cdsList: TFVBFFCClientDataSet
    object cdsListF_KMO_PORTEFEUILLE_ID: TIntegerField
      DisplayLabel = 'KMO Portefeuille (ID)'
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsListF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie (ID)'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PROJECT_NR: TStringField
      DisplayLabel = 'Project nummer'
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsListF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Status (ID)'
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_AMOUNT_REQUESTED: TFloatField
      DisplayLabel = 'Bedrag aangevraagd'
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PAY_TO_SODEXO_OK: TBooleanField
      DisplayLabel = 'Storting OK @ Sodexo'
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PAYMENT_OK: TBooleanField
      DisplayLabel = 'Betaling OK'
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_REMARK: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsListF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsListF_STATUS_NL: TStringField
      DisplayLabel = 'Status ( NL )'
      FieldName = 'F_STATUS_NL'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_STATUS_FR: TStringField
      DisplayLabel = 'Status ( FR )'
      FieldName = 'F_STATUS_FR'
      ProviderFlags = []
      Size = 128
    end
    object cdsListF_FOREGROUNDCOLOR: TIntegerField
      DisplayLabel = 'Kleur FG'
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = []
    end
    object cdsListF_BACKGROUNDCOLOR: TIntegerField
      DisplayLabel = 'Kleur BG'
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = []
    end
    object cdsListF_ORG_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsListF_ORG_ORG_NR: TStringField
      DisplayLabel = 'Ondernemingsnummer'
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsListF_ORG_VAT_NR: TStringField
      DisplayLabel = 'BTW nummer'
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ORG_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ORG_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nummer'
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object cdsListF_SESSION_ID: TIntegerField
      DisplayLabel = 'Sessienummer'
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_ANNULATION_REASON: TStringField
      DisplayLabel = 'Reden annulatie'
      FieldName = 'F_ANNULATION_REASON'
      Size = 1024
    end
    object cdsListF_PAYMENT_AMOUNT1: TBCDField
      DisplayLabel = 'Betaling 1'
      FieldName = 'F_PAYMENT_AMOUNT1'
      Precision = 19
    end
    object cdsListF_PAYMENT_AMOUNT2: TBCDField
      DisplayLabel = 'Betaling 2'
      FieldName = 'F_PAYMENT_AMOUNT2'
      Precision = 19
    end
    object cdsListF_PAYMENT_AMOUNT3: TBCDField
      DisplayLabel = 'Betaling 3'
      FieldName = 'F_PAYMENT_AMOUNT3'
      Precision = 19
    end
    object cdsListF_SESSION: TStringField
      DisplayLabel = 'Sessienummer'
      FieldName = 'F_SESSION'
      ProviderFlags = []
      Size = 73
    end
    object cdsListF_PROGRAM: TStringField
      DisplayLabel = 'Opleiding'
      FieldName = 'F_PROGRAM'
      ProviderFlags = []
      Size = 75
    end
    object cdsListF_DEMAND_DATE: TDateTimeField
      DisplayLabel = 'Datum aanvraag'
      FieldName = 'F_DEMAND_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_SESSION_DATE: TDateTimeField
      DisplayLabel = 'Datum opleiding'
      FieldName = 'F_SESSION_DATE'
      ProviderFlags = []
    end
    object cdsListF_PAYMENT_STATUS_ID: TIntegerField
      DisplayLabel = 'Betaling Status ID'
      FieldName = 'F_PAYMENT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsListF_PAYMENT_STATUS_NAME: TStringField
      DisplayLabel = 'Betaling Status'
      FieldName = 'F_PAYMENT_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited cdsRecord: TFVBFFCClientDataSet
    AfterInsert = cdsRecordAfterInsert
    object cdsRecordF_KMO_PORTEFEUILLE_ID: TIntegerField
      DisplayLabel = 'KMO Portefeuille (ID)'
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsRecordF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie (ID)'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PROJECT_NR: TStringField
      DisplayLabel = 'Project nummer'
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsRecordF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Status (ID)'
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_AMOUNT_REQUESTED: TFloatField
      DisplayLabel = 'Bedrag aangevraagd'
      FieldName = 'F_AMOUNT_REQUESTED'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PAY_TO_SODEXO_OK: TBooleanField
      DisplayLabel = 'Storting OK @ Sodexo'
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PAYMENT_OK: TBooleanField
      DisplayLabel = 'Betaling OK'
      FieldName = 'F_PAYMENT_OK'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_REMARK: TStringField
      DisplayLabel = 'Opmerking'
      FieldName = 'F_REMARK'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsRecordF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsRecordF_STATUS_NL: TStringField
      DisplayLabel = 'Status ( NL )'
      FieldName = 'F_STATUS_NL'
      ProviderFlags = []
      Size = 128
    end
    object cdsRecordF_STATUS_FR: TStringField
      DisplayLabel = 'Status ( FR )'
      FieldName = 'F_STATUS_FR'
      ProviderFlags = []
      Size = 128
    end
    object cdsRecordF_FOREGROUNDCOLOR: TIntegerField
      DisplayLabel = 'Kleur FG'
      FieldName = 'F_FOREGROUNDCOLOR'
      ProviderFlags = []
    end
    object cdsRecordF_BACKGROUNDCOLOR: TIntegerField
      DisplayLabel = 'Kleur BG'
      FieldName = 'F_BACKGROUNDCOLOR'
      ProviderFlags = []
    end
    object cdsRecordF_ORG_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsRecordF_ORG_ORG_NR: TStringField
      DisplayLabel = 'Ondernemingsnummer'
      FieldName = 'F_ORG_ORG_NR'
      ProviderFlags = [pfInUpdate]
      Size = 10
    end
    object cdsRecordF_ORG_VAT_NR: TStringField
      DisplayLabel = 'BTW nummer'
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ORG_SYNERGY_ID: TLargeintField
      DisplayLabel = 'Synergy ID'
      FieldName = 'F_ORG_SYNERGY_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ORG_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nummer'
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object cdsRecordF_SESSION_ID: TIntegerField
      FieldName = 'F_SESSION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_ANNULATION_REASON: TStringField
      FieldName = 'F_ANNULATION_REASON'
      ProviderFlags = [pfInUpdate]
      Size = 1024
    end
    object cdsRecordF_PAYMENT_AMOUNT1: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT1'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object cdsRecordF_PAYMENT_AMOUNT2: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT2'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object cdsRecordF_PAYMENT_AMOUNT3: TBCDField
      FieldName = 'F_PAYMENT_AMOUNT3'
      ProviderFlags = [pfInUpdate]
      Precision = 19
    end
    object cdsRecordF_SESSION: TStringField
      FieldName = 'F_SESSION'
      ProviderFlags = []
      Size = 73
    end
    object cdsRecordF_PROGRAM: TStringField
      FieldName = 'F_PROGRAM'
      ProviderFlags = []
      Size = 75
    end
    object cdsRecordF_DEMAND_DATE: TDateTimeField
      FieldName = 'F_DEMAND_DATE'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_SESSION_DATE: TDateTimeField
      FieldName = 'F_SESSION_DATE'
      ProviderFlags = []
    end
    object cdsRecordF_PAYMENT_STATUS_ID: TIntegerField
      FieldName = 'F_PAYMENT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsRecordF_PAYMENT_STATUS_NAME: TStringField
      FieldName = 'F_PAYMENT_STATUS_NAME'
      ProviderFlags = []
      Size = 128
    end
  end
  inherited cdsSearchCriteria: TFVBFFCClientDataSet
    object cdsSearchCriteriaF_KMO_PORTEFEUILLE_ID: TIntegerField
      DisplayLabel = 'KMO Portefeuille (ID)'
      FieldName = 'F_KMO_PORTEFEUILLE_ID'
      ProviderFlags = [pfInUpdate, pfInKey]
    end
    object cdsSearchCriteriaF_PROJECT_NR: TStringField
      DisplayLabel = 'Project nummer'
      FieldName = 'F_PROJECT_NR'
      ProviderFlags = [pfInUpdate]
      Size = 30
    end
    object cdsSearchCriteriaF_KMO_PORT_STATUS_ID: TIntegerField
      DisplayLabel = 'Status (ID)'
      FieldName = 'F_KMO_PORT_STATUS_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_STATUS_NAME: TStringField
      DisplayLabel = 'Status'
      FieldName = 'F_STATUS_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 128
    end
    object cdsSearchCriteriaF_ORGANISATION_ID: TIntegerField
      DisplayLabel = 'Organisatie (ID)'
      FieldName = 'F_ORGANISATION_ID'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_ORG_NAME: TStringField
      DisplayLabel = 'Organisatie'
      FieldName = 'F_ORG_NAME'
      ProviderFlags = [pfInUpdate]
      Size = 64
    end
    object cdsSearchCriteriaF_ORG_VAT_NR: TStringField
      DisplayLabel = 'BTW nummer'
      FieldName = 'F_ORG_VAT_NR'
      ProviderFlags = [pfInUpdate]
    end
    object cdsSearchCriteriaF_ORG_RSZ_NR: TStringField
      DisplayLabel = 'RSZ nummer'
      FieldName = 'F_ORG_RSZ_NR'
      ProviderFlags = [pfInUpdate]
      Size = 14
    end
    object cdsSearchCriteriaF_PAY_TO_SODEXO_OK: TBooleanField
      DisplayLabel = 'Storting OK @ Sodexo'
      FieldName = 'F_PAY_TO_SODEXO_OK'
      ProviderFlags = [pfInUpdate]
    end
  end
  inherited ssckData: TFVBFFCSharedConnection
    ChildName = 'rdtmKMO_Portefeuille'
  end
  inherited dspConnection: TFVBFFCDSPConnection
    ServerClassName = 'TrdtmKMO_Portefeuille'
  end
end
