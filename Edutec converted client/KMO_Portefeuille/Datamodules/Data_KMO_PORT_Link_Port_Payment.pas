unit Data_KMO_PORT_Link_Port_Payment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBClient, Unit_PPWFrameWorkClasses, Unit_FVBFFCInterfaces,
  Data_EDUDataModule, Unit_PPWFrameWorkComponents, Unit_FVBFFCDBComponents,
  Provider, DB, ADODB, Unit_PPWFrameWorkActions, MConnect, unit_EdutecInterfaces, 
  Unit_PPWFrameWorkInterfaces, Datasnap.DSConnect;

type
  TdtmKMO_PORT_Link_Port_Payment = class(TEDUDataModule, IEDUKMO_PORT_Link_Port_PaymentDataModule, IPPWFrameWorkDataModuleDefaultFilter )
    cdsListF_KMO_PORT_LNK_PORT_PAY_ID: TIntegerField;
    cdsListF_KMO_PORTEFEUILLE_ID: TIntegerField;
    cdsListF_KMO_PORT_PAYMENT_ID: TIntegerField;
    cdsListF_AMOUNT_LINKED: TFloatField;
    cdsListF_INVOICE_NR: TStringField;
    cdsListF_AMOUNT_PAYED: TFloatField;
    cdsListF_DATE_PAYED: TDateTimeField;
    cdsListF_SESSION_ID: TIntegerField;
    cdsListF_SESSION_CODE: TStringField;
    cdsListF_SESSION_NAME: TStringField;
    cdsListF_SESSION_START_DATE: TDateTimeField;
    cdsListF_SESSION_END_DATE: TDateTimeField;
    cdsListF_SESSION_STATUS_NAME: TStringField;
    cdsListF_PROJECT_NR: TStringField;
    cdsListF_ORGANISATION_ID: TIntegerField;
    cdsListF_KMO_PORT_STATUS_ID: TIntegerField;
    cdsListF_AMOUNT_REQUESTED: TFloatField;
    cdsListF_PAY_TO_SODEXO_OK: TBooleanField;
    cdsListF_STATUS_NAME: TStringField;
    cdsListF_ORG_NAME: TStringField;
    cdsListF_ORG_ORG_NR: TStringField;
    cdsListF_ORG_VAT_NR: TStringField;
    cdsListF_ORG_SYNERGY_ID: TLargeintField;
    cdsListF_ORG_RSZ_NR: TStringField;
    cdsRecordF_KMO_PORT_LNK_PORT_PAY_ID: TIntegerField;
    cdsRecordF_KMO_PORTEFEUILLE_ID: TIntegerField;
    cdsRecordF_KMO_PORT_PAYMENT_ID: TIntegerField;
    cdsRecordF_AMOUNT_LINKED: TFloatField;
    cdsRecordF_INVOICE_NR: TStringField;
    cdsRecordF_AMOUNT_PAYED: TFloatField;
    cdsRecordF_DATE_PAYED: TDateTimeField;
    cdsRecordF_SESSION_ID: TIntegerField;
    cdsRecordF_SESSION_CODE: TStringField;
    cdsRecordF_SESSION_NAME: TStringField;
    cdsRecordF_SESSION_START_DATE: TDateTimeField;
    cdsRecordF_SESSION_END_DATE: TDateTimeField;
    cdsRecordF_SESSION_STATUS_NAME: TStringField;
    cdsRecordF_PROJECT_NR: TStringField;
    cdsRecordF_ORGANISATION_ID: TIntegerField;
    cdsRecordF_KMO_PORT_STATUS_ID: TIntegerField;
    cdsRecordF_AMOUNT_REQUESTED: TFloatField;
    cdsRecordF_PAY_TO_SODEXO_OK: TBooleanField;
    cdsRecordF_STATUS_NAME: TStringField;
    cdsRecordF_ORG_NAME: TStringField;
    cdsRecordF_ORG_ORG_NR: TStringField;
    cdsRecordF_ORG_VAT_NR: TStringField;
    cdsRecordF_ORG_SYNERGY_ID: TLargeintField;
    cdsRecordF_ORG_RSZ_NR: TStringField;
    cdsListF_PAYMENT_OK: TBooleanField;
    cdsRecordF_PAYMENT_OK: TBooleanField;
    procedure FVBFFCDataModuleInitialisePrimaryKeyFields(
      aDataSet: TDataSet);
    procedure prvRecordGetTableName(Sender: TObject; DataSet: TDataSet;
      var TableName: String);
    procedure FVBFFCDataModuleInitialiseAdditionalFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleCreate(Sender: TObject);
    procedure cdsRecordAfterInsert(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
  private
  protected
    { Protected declarations }
    function IsListViewActionAllowed         ( aAction         : TPPWFrameWorkListViewAction ) : Boolean; override;

    procedure SelectKMO_PORT_Payment    ( aDataSet : TDataSet ); virtual;
    procedure ShowKMO_PORT_Payment      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
    procedure SelectKMO_Portefeuille    ( aDataSet : TDataSet ); virtual;
    procedure ShowKMO_Portefeuille      ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView ); virtual;
  public
    { Public declarations }
    class procedure SelectKMO_PORT_Link_Port_Payment( aDataSet : TDataSet; aWhere : String = ''; aMultiSelect : Boolean = False; aCopyTo : TCopyRecordInformationTo = critDefault  ); virtual;
    class procedure ShowKMO_PORT_Link_Port_Payment  ( aDataSet : TDataSet; aRecordViewMode : TPPWFrameWorkRecordViewMode = rvmView; aCopyTo : TCopyRecordInformationTo = critDefault ); virtual;
    procedure InitDefaultFilter(ASearchCriteria: TClientDataSet);
  end;

  procedure CopyKMO_PORT_Link_Port_PaymentFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );

implementation

uses
  Data_EduMainClient, Data_FVBFFCBaseDataModule, 
  DateUtils, Unit_PPWFrameWorkDataModule, 
  Data_KMO_Portefeuille, Data_KMO_PORT_Payment;

{$R *.dfm}

{*****************************************************************************
  This event will be use to initialise the Primary Key Fields.

  @Name       TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields should be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleInitialisePrimaryKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_KMO_PORT_LNK_PORT_PAY_ID' ).AsInteger := ssckData.AppServer.GetNewRecordID;
end;

{*****************************************************************************
  This procedure will be used to copy KMO Portefeuille related fields from one
  DataSet to another DataSet.

  @Name       CopyKMO_PORT_Link_Port_PaymentFieldValues
  @author     slesage
  @param      aSource        The Source DataSet.
  @param      aDestination   The Destination DataSet.
  @param      aIncludeID     Flag indicating if the PK Field values should be
                             copied as well.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure CopyKMO_PORT_Link_Port_PaymentFieldValues( aSource, aDestination : TDataSet; IncludeID : Boolean; aCopyTo : TCopyRecordInformationTo = critDefault );
begin
  { Check if both DataSets are assigned }
  if ( Assigned( aSource ) ) and
     ( Assigned( aDestination ) ) then
  begin
    { Make sure the Destination DataSet is in Edit Mode }
    if not ( aDestination.State in dsEditModes ) then
    begin
      aDestination.Edit;
    end;
    
    { Copy the ID if necessary }
    if ( IncludeID ) then
    begin
      aDestination.FieldByName( 'F_KMO_PORT_LNK_PORT_PAY_ID' ).AsInteger :=
        aSource.FieldByName( 'F_KMO_PORT_LNK_PORT_PAY_ID' ).AsInteger;
    end;
    
    { Copy the Other Fields }
    case aCopyTo of
      critDefault :
      begin
      //
      end;
    end;
  end;
end;

procedure TdtmKMO_PORT_Link_Port_Payment.prvRecordGetTableName(Sender: TObject;
  DataSet: TDataSet; var TableName: String);
begin
  inherited;
  TableName := 'T_KMO_PORT_LNK_PORT_PAY';
end;

{*****************************************************************************************
  This method will be used to initialise some Additional Fields on the DataSet.

  @Name       TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
  @History          Author                  Description
  11/07/2008       Ivan Van den Bossche     Added initialization of F_VIRTUAL_SESSION field
*******************************************************************************************}

procedure TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleInitialiseAdditionalFields(
  aDataSet: TDataSet);
begin
  inherited;
  aDataSet.FieldByName( 'F_AMOUNT_LINKED' ).AsFloat := 0;
end;

{*****************************************************************************
  This method will be used to initialise some Foreign Key Fields on the DataSet.

  @Name       TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which the Fields must be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
begin
  inherited;
  if ( Assigned( MasterDataModule ) ) then
  begin
    if ( Supports( MasterDataModule, IEDUKMO_PortefeuilleDataModule ) ) then
    begin
      CopyKMO_PortefeuilleFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critKMO_Portefeuille_To_Link_Payment );
    end;
    if ( Supports( MasterDataModule, IEDUKMO_PORT_PaymentDataModule ) ) then
    begin
      CopyKMO_PORT_PaymentFieldValues( MasterDataModule.RecordDataset, aDataSet, False, critKMO_PORT_Payment_To_Link_Payment );
    end;
  end;
end;

{*****************************************************************************
  This class procedure will be used to select one or more Session Records.

  @Name       TdtmKMO_PORT_Link_Port_Payment.SelectKMO_PORT_Link_Port_Payment
  @author     slesage
  @param      aDataSet       The dataset in which we should copy some
                             information from the Selected Records.
  @param      aWhere         A possible where clause that should be used to
                             filter the list of records available for selection.
  @param      aMultiSelect   A boolean indicating if the  user is allowed to
                             select multiple records or not.
  @param      aCopyTo        This variable incdicates to which entity the
                             Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmKMO_PORT_Link_Port_Payment.SelectKMO_PORT_Link_Port_Payment(aDataSet: TDataSet;
  aWhere: String; aMultiSelect: Boolean;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show the ListView for selection purposes, passing in a WHERE clause
      as second parameter to limit the resultset if necessary.
      The selected records should be added to our aSelectedRecords dataset,
      and the user should only be able to select one record }
    if ( dtmEduMainClient.pfcMain.SelectRecords( 'TdtmKMO_PORT_Link_Port_Payment', aWhere ,
       aSelectedRecords, aMultiSelect ) = mrOk ) then
    begin
      { Make sure our DataSet is in Edit mode }
      if not ( aDataSet.State in dsEditModes ) then
      begin
        aDataSet.Edit;
      end;
      { Now we can copy whatever fields from the aSelectedRecords to our
        aDataSet }
      CopyKMO_PORT_Link_Port_PaymentFieldValues( aSelectedRecords, aDataSet, True, aCopyTo );
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a single Session
  Record based on the PK Field values found in the given aDataSet.

  @Name       TdtmKMO_PORT_Link_Port_Payment.ShowKMO_PORT_Link_Port_Payment
  @author     slesage
  @param      aDataSet          The dataset which will contain the PK Field
                                value for the record we need to show.
  @param      aRecordViewMode   The mode in which the RecordView must be
                                shown.
  @param      aCopyTo           This variable incdicates to which entity the
                                Data should be copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

class procedure TdtmKMO_PORT_Link_Port_Payment.ShowKMO_PORT_Link_Port_Payment(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aCopyTo: TCopyRecordInformationTo);
var
  aSelectedRecords : TClientDataSet;
begin
  aSelectedRecords := TClientDataSet.Create( Application );
  try
    { Show a Modal RecordView.  Make sure it is filtered on PK Field, and show
      it in the given RecordView Mode.  If the user modfies data in that Modal
      RecordView, we can still update our DataSet if needed.  The Modified
      record will be returned in the aSelectedRecords DataSet }
    if ( dtmEduMainClient.pfcMain.ShowModalRecord( 'TdtmKMO_PORT_Link_Port_Payment',
       'F_KMO_PORT_LNK_PORT_PAY_ID = ' + aDataSet.FieldByName( 'F_KMO_PORT_LNK_PORT_PAY_ID' ).AsString ,
       aSelectedRecords, aRecordViewMode ) = mrOk ) then
    begin
      { The user has closed the form, so we can copy some fields if we want }
      if ( aSelectedRecords.RecordCount = 1 ) then
      begin
        CopyKMO_PORT_Link_Port_PaymentFieldValues( aSelectedRecords, aDataSet,
        ( aRecordViewMode = rvmAdd ), aCopyTo );
      end;
    end;
  finally
    FreeAndNil( aSelectedRecords );
  end;
end;

{*****************************************************************************
  Name           : IsListViewActionAllowed
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Listview action "Add" is not allowed when the session entity
                   is docked as a detail of the organisation recordview.
                   This is needed because when an "Add" action was allowed, then
                   an errormessage "F_ORGANISATION_ID does not exist" occurs.
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/09/2006   Wim Lambrechts      Initial creation of the Procedure.
 *****************************************************************************}
function TdtmKMO_PORT_Link_Port_Payment.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
begin
  Result := Inherited IsListViewActionAllowed( aAction );
end;


procedure TdtmKMO_PORT_Link_Port_Payment.FVBFFCDataModuleCreate(Sender: TObject);
begin
  inherited;
end;

{*****************************************************************************
  Name           : TdtmKMO_PORT_Link_Port_Payment.InitDefaultFilter
  Author         : Ivan Van den Bossche
  Arguments      : ASearchCriteria
  Return Values  : None
  Exceptions     : None
  Description    : This method is supplied so we can initialize a temp filter
                    before the form is activated. The form will show the default
                    filter as well.  The user could undo the filter...
  History        :

  Date         By                   Description
  ----         --                   -----------
  07/09/2007   ivdbossche           Initial creation of the procedure.
 *****************************************************************************}
procedure TdtmKMO_PORT_Link_Port_Payment.InitDefaultFilter(
  ASearchCriteria: TClientDataSet);
begin
  //cdsSearchCriteria.Edit;

  //cdsSearchCriteria.Post;

end;

procedure TdtmKMO_PORT_Link_Port_Payment.cdsRecordAfterInsert(DataSet: TDataSet);
begin
  inherited;
//
end;

procedure TdtmKMO_PORT_Link_Port_Payment.cdsListAfterPost(DataSet: TDataSet);
begin
  cdsList.Tag := 1; // Make sure that changes will be posted to the database.
  inherited;

end;

procedure TdtmKMO_PORT_Link_Port_Payment.SelectKMO_PORT_Payment(aDataSet: TDataSet);
var
  aWhere : String;
begin
  aWhere := '';

  TdtmKMO_PORT_Payment.SelectKMO_PORT_Payment(aDataSet, aWhere, false, critKMO_PORT_Payment_To_Link_Payment);
end;

procedure TdtmKMO_PORT_Link_Port_Payment.ShowKMO_PORT_Payment(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_KMO_PORT_PAYMENT_ID' );
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmKMO_PORT_Payment.ShowKMO_PORT_Payment( aDataSet, aRecordViewMode, critKMO_PORT_Payment_To_Link_Payment );
  end;
end;

procedure TdtmKMO_PORT_Link_Port_Payment.SelectKMO_Portefeuille(aDataSet: TDataSet);
var
  aWhere : String;
begin
  aWhere := '';

  TdtmKMO_Portefeuille.SelectKMO_Portefeuille(aDataSet, aWhere, false, critKMO_Portefeuille_To_Link_Payment);
end;

procedure TdtmKMO_PORT_Link_Port_Payment.ShowKMO_Portefeuille(aDataSet: TDataSet;
  aRecordViewMode: TPPWFrameWorkRecordViewMode);
var
  aIDField : TField;
begin
  aIDField := aDataSet.FindField( 'F_KMO_PORTEFEUILLE_ID' );
  if ( Assigned( aIDField ) ) and
     ( not ( aIDField.IsNull ) or ( aRecordViewMode = rvmAdd ) ) then
  begin
    TdtmKMO_Portefeuille.ShowKMO_Portefeuille( aDataSet, aRecordViewMode, critKMO_Portefeuille_To_Link_Payment );
  end;
end;

end.
