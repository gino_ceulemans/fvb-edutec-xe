{*****************************************************************************
  Name           : Unit_PPWFrameWorkClasses
  Author         : Lesage Stefaan
  Copyright      : (c) 2002 Peopleware
  Description    : This unit will contain some Custom Classes that are used
                   in the FrameWork.
  History        :

  Date         By                   Description
  ----         --                   -----------
  18/11/2004   sLesage              Added an AutoCreate property to the
                                    FrameWorkDataModuleDetail.  This property
                                    can be used to indicate if the detail should
                                    be created automatically or not.
  28/10/2004   sLesage              Removed some code which was specific to
                                    the BRC Project.
  17/03/2003   sLesage              Added some Messages that will be used
                                    in the FrameWork.
  3/12/2002    sLesage              Added a component which will be used to
                                    keep track of components receiving / loosing
                                    the focus.
  20/11/2002   sLesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkClasses;

interface

uses
  { Delphi Units }
  SysUtils, Messages, Controls, Classes, Forms, DBClient;

const
  WM_BASEFRAMEWORK               = ( WM_APP + 100 );

type
  TPPWDataModuleClassName = string;
  TPPWListViewClassName   = string;
  TPPWRecordViewClassName = string;

  TPPWFrameWorkRecordViewMode = ( rvmAdd, rvmDelete, rvmEdit, rvmView, rvmDefault );

  TPPWFrameWorkBeforeOpenRecordViewEvent = procedure ( Sender : TObject; RecordViewMode : TPPWFrameWorkRecordViewMode; var AllowRecordView : Boolean ) of Object;
  TPPWFrameWorkFilterDetailEvent         = procedure ( Sender : TObject; Fields : String; Values : Variant; Expression : String ) of Object;
  TPPWFrameWorkFilterEvent               = procedure ( Sender : TObject; Fields : String ) of Object;
  TPPWFrameWorkSortEvent                 = procedure ( Sender : TObject; Fields : String ) of Object;
  TPPWFrameWorkOnFilterEvent             = procedure ( Sender : TObject; var FilterApplied : Boolean ) of object;
  TPPWFrameWorkSaveGridMethod            = procedure ( const FileName : String; aSaveAll : Boolean = True ) of Object;
  TPPWFrameWorkBeforeCreateDetailEvent   = procedure ( Sender : TObject; DetailModuleClass : String; var CanCreate : Boolean ) of object;
  TPPWFrameWorkAfterCreateDetailevent    = procedure ( Sender : TObject; DetailModuleClass : String; DetailInterface: IUnknown) of object;
  //added WL 18/12: because of circular reference a parameter of IUnknown was needed
  TPPWFrameWorkOnDetailListViewCreatedEvent = procedure (Sender: TObject; DetailListViewClass: String; DetailListView: IUnknown) of object;

  TPPWFrameWorkControlOption  = ( coDataSetReadOnly, coFieldReadOnly,
                                  coControlReadOnly, coFieldRequired,
                                  coControlDisabled, coControlHasFocus );
  TPPWFrameWorkControlOptions = set of TPPWFrameWorkControlOption;

  TPPWFrameWorkFocusChangeEvent = Procedure( Sender : TControl ) of Object;

  TPPWFrameWorkFocusControl = class( TComponent )
  private
    FHostForm        : TCustomForm;
    FHostWindowProc  : TWndMethod;
    FLastFocusedCtrl : TControl;
    FOnLostFocus     : TPPWFrameWorkFocusChangeEvent;
    FOnReceivedFocus : TPPWFrameWorkFocusChangeEvent;
  protected
    function RunTime : Boolean;

    procedure AlterCtrlState; virtual;
    procedure DoAfterFocus( Control : TControl ); virtual;
    procedure DoBeforeFocus; virtual;
    procedure FocusChanged; Virtual;
    procedure RestoreCtrlState; virtual;

    procedure CtlFocusWndProc( var Message : TMessage ); virtual;
  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;
    property OnLostFocus     : TPPWFrameWorkFocusChangeEvent read FOnLostFocus
                                                             write FOnLostFocus;
    property OnReceivedFocus : TPPWFrameWorkFocusChangeEvent read FOnReceivedFocus
                                                             write FOnReceivedFocus;
  end;

  TPPWDetailState = ( dstDefault, dstCustomised );

  TPPWFrameWorkDataModuleDetail = class( TCollectionItem )
  private
    FForeignKeys     : String;
    FCaption         : string;
    FDataModuleClass : TPPWDataModuleClassName;
    FStored: Boolean;
    FAutoCreate: Boolean;
  protected
    function GetDisplayName : String; override;

    procedure SetCaption(const Value: string); virtual;
    procedure SetForeignKeys(const Value: String); virtual;

    property IsStored: Boolean read FStored write FStored default True;
  public
    procedure Assign(Source: TPersistent); override;
    function IsEqual(Value: TPPWFrameWorkDataModuleDetail): Boolean;
  published
    property Caption : string read FCaption write SetCaption;
    property ForeignKeys : String read FForeignKeys write SetForeignKeys;
    property DataModuleClass : TPPWDataModuleClassName read FDataModuleClass
                                                       write FDataModuleClass;
    property AutoCreate : Boolean read  FAutoCreate
                                  write FAutoCreate;
  end;

  TPPWFrameWorkDataModuleDetails = class( TOwnedCollection )
  private
    function GetState: TPPWDetailState;
    procedure SetState(const Value: TPPWDetailState);
  protected
    function GetItem( Index : Integer ) : TPPWFrameWorkDataModuleDetail;
    procedure SetItem( Index : Integer; Value : TPPWFrameWorkDataModuleDetail );
  public
    function ItemByDetailClassName( aDetailClassName : String ) : TPPWFrameWorkDataModuleDetail;
    function IsEqual(Value: TPPWFrameWorkDataModuleDetails): Boolean;
    property Items[ index : Integer ] : TPPWFrameWorkDataModuleDetail
                                                        read GetItem
                                                        write SetItem;
    property State: TPPWDetailState read GetState write SetState;
  end;

  IPPWRestorer = interface
  ['{27C75881-7A08-4147-A290-66F969E17805}']
  end;

  TPPWCursorRestorer = class( TInterfacedObject, IPPWRestorer )
  private
    FCursor : TCursor;
  public
    constructor Create; overload;
    constructor Create( aNewCursor : TCursor ); overload;
    destructor Destroy; override;
  end;

  TPPWFrameWorkFormStyle   = ( pfsNormal, pfsMDIChild, pfsModal );

implementation

{ TPPWCursorRestorer }

{*****************************************************************************
  Name           : TPPWCursorRestorer.Create
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None
  Exceptions     : None
  Description    : Constructor for the Object.  It will store the current
                   screen cursor in a private variable.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

constructor TPPWCursorRestorer.Create;
begin
  Inherited Create;
  FCursor := Screen.Cursor;
end;

{*****************************************************************************
  Name           : TPPWCursorRestorer.Create
  Author         : Stefaan Lesage
  Arguments      : aNewCursor - The New cursor that should be used.
  Return Values  : None
  Exceptions     : None
  Description    : Constructor for the Object.  It will store the current
                   screen cursor in a private variable and initialise the
                   screen cursor to the one passed to the constructor.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  22/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

constructor TPPWCursorRestorer.Create(aNewCursor: TCursor);
begin
  Inherited Create;
  FCursor := Screen.Cursor;
  Screen.Cursor := aNewCursor;
end;

{*****************************************************************************
  Name           : TPPWCursorRestorer.Destroy 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Destructor for the Object.  Destroying the Object will
                   restore the Screen Cursor to the one that has been
                   stored when the Object was Created.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

destructor TPPWCursorRestorer.Destroy;
begin
  Screen.Cursor := FCursor;
  inherited Destroy;
end;

{ TPPWFrameWorkFocusControl }

procedure TPPWFrameWorkFocusControl.AlterCtrlState;
begin
  DoBeforeFocus;
  if ( Assigned( FHostForm ) ) then
  begin
    FLastFocusedCtrl := FHostForm.ActiveControl;
  end
  else
  begin
    FLastFocusedCtrl := Nil;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkFocusControl.Create 
  Author         : Stefaan Lesage
  Arguments      : AOwner - The Owner of the Component                                                     
  Return Values  : None
  Exceptions     : None
  Description    : Overridden Constructor in which we will add our own
                   WindowProc method so our component gets notified of
                   componets getting and losing the Focus.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

constructor TPPWFrameWorkFocusControl.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );

  FHostForm := TCustomForm( AOwner );

  if ( Runtime ) then
  begin
    FHostWindowProc := FHostForm.WindowProc;
    FHostForm.WindowProc := CtlFocusWndProc;
    FLastFocusedCtrl := FHostForm.ActiveControl;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkFocusControl.CtlFocusWndProc 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Our Custom WindowProc method which will be used to call
                   some of our methods when a control receives or looses the
                   focus.                                                          
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkFocusControl.CtlFocusWndProc(var Message: TMessage);
begin
  Case Message.Msg of
    CM_FOCUSCHANGED :
    begin
      FocusChanged;
    end;
    CM_DEACTIVATE :
    begin
      RestoreCtrlState;
    end;
    CM_ACTIVATE :
    begin
      FocusChanged;
    end;
  end;
  FHostWindowProc( Message );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkFocusControl.Destroy 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Overridded Destructor in which we will clean up some
                   things.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

destructor TPPWFrameWorkFocusControl.Destroy;
begin
  if ( RunTime ) then
  begin
    FHostForm.WindowProc := FHostWindowProc;
  end;

  inherited;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkFocusControl.DoAfterFocus
  Author         : Stefaan Lesage
  Arguments      : Control - The control which is losing the Focus.
  Return Values  : None
  Exceptions     : None
  Description    : This is the OnLostFocus triggering method.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkFocusControl.DoAfterFocus( Control : TControl );
begin
  if ( Assigned( FOnLostFocus ) ) then
  begin
    FOnLostFocus( Control );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkFocusControl.DoBeforeFocus
  Author         : Stefaan Lesage
  Arguments      : Control - The control which is receiving the Focus.
  Return Values  : None
  Exceptions     : None
  Description    : This is the OnReceivedFocus triggering method.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkFocusControl.DoBeforeFocus;
begin
  if ( Assigned( FHostForm ) ) and
     ( Assigned( FHostForm.ActiveControl ) ) then
  begin
    if ( Assigned( FOnReceivedFocus ) ) then
    begin
      FOnReceivedFocus( FHostForm.ActiveControl );
    end;
  end;
end;

procedure TPPWFrameWorkFocusControl.FocusChanged;
begin
  RestoreCtrlState;
  AlterCtrlState;
end;

procedure TPPWFrameWorkFocusControl.RestoreCtrlState;
begin
  if ( FLastFocusedCtrl <> Nil ) then
  begin
    DoAfterFocus( FLastFocusedCtrl ); 
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkFocusControl.RunTime 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Returns True if we are in DesignTime, otherwise it
                   returns False.
  Exceptions     : None
  Description    : This function is used to determine if we are in Design
                   Mode or not.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkFocusControl.RunTime: Boolean;
begin
  Result := not ( csDesigning in ComponentState );
end;

{ TPPWFrameWorkDataModuleDetail }

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetail.Assign
  Author         : Stefaan Lesage
  Arguments      : Source - The TPersistent to which our CollectionItem must
                            be Assigned.
  Return Values  : None
  Exceptions     : None
  Description    : Overridden Assing method, in which we will set all
                   properties of the CollectionItem to the one supplied in
                   the Source parameter ( if the Source is a descendant of
                   TPPWFrameWorkDataModuleDetail ).
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModuleDetail.Assign(Source: TPersistent);
begin
  if ( Source is TPPWFrameWorkDataModuleDetail ) then
  begin
    Caption := TPPWFrameWorkDataModuleDetail( Source ).Caption;
    DataModuleClass := TPPWFrameWorkDataModuleDetail( Source ).DataModuleClass;
    ForeignKeys := TPPWFrameWorkDataModuleDetail( Source ).ForeignKeys;
    AutoCreate := TPPWFrameWorkDataModuleDetail( Source ).AutoCreate;
  end
  else
  begin
    Inherited Assign( Source );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetail.GetDisplayName
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the Name of the Collectoin item which is used
                   to display the CollectoinItem in the Collection Editor.
  Exceptions     : None
  Description    : Ovverridden GetDisplayName method.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModuleDetail.GetDisplayName: String;
begin
  Result := DataModuleClass;
  if Result = '' then
    Result := inherited GetDisplayName;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetail.IsEqual 
  Author         : Stefaan Lesage
  Arguments      : Value - The collection Item to which we want to compare.
  Return Values  : None
  Exceptions     : Returns True if the properties of the CollectonItem are the
                   same as the properties of the CollectionItem supplied with
                   the Value Parameter.
  Description    : This method will be used to determine if our collection
                   item is exactly the same as the one Supplied in the Value
                   parameter.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModuleDetail.IsEqual(Value: TPPWFrameWorkDataModuleDetail): Boolean;
begin
  Result := ( ( Caption ) = ( Value.Caption ) ) and
            ( ( ForeignKeys ) = ( Value.ForeignKeys ) ) and
            ( ( DataModuleClass ) = ( Value.DataModuleClass ) ) and
            ( ( AutoCreate ) = ( Value.AutoCreate ) );

end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetail.SetCaption
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the Caption Property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the Caption Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModuleDetail.SetCaption(const Value: string);
begin
  if ( Value <> FCaption ) then
  begin
    FCaption := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetail.SetForeignKeys 
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the ForeingKeys Property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the ForeignKeys Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModuleDetail.SetForeignKeys(
  const Value: String);
begin
  if ( Value <> FForeignKeys ) then
  begin
    FForeignKeys := Value;
  end;
end;

{ TPPWFrameWorkDataModuleDetails }

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetails.GetItem 
  Author         : Stefaan Lesage
  Arguments      : Index - The Index of the CollectionItem that must be
                           returned.                                                     
  Return Values  : The CollectonItem which is at the Given Index in our
                   Collection.                                                     
  Exceptions     : None
  Description    : This method is used to return a specific CollectoinItem
                   from our Collection.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModuleDetails.GetItem(
  Index: Integer): TPPWFrameWorkDataModuleDetail;
begin
  Result := TPPWFrameWorkDataModuleDetail( inherited GetItem( Index ) );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetails.IsEqual 
  Author         : Stefaan Lesage
  Arguments      : Value - The Collection to which you want to compare the
                           current Collecton.                                                     
  Return Values  : None                                                     
  Exceptions     : Returns true if both collections are the same ( which means
                   all CollectionItems from both Collections have the same
                   properties ), or False otherwise.
  Description    : This method will be used to Check if the collection is
                   the same as the collection passed in the Value Parameter.
                   To determine if they are the same, all Collection Items from
                   both Collections will be compared to eachother.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModuleDetails.GetState: TPPWDetailState;
begin
  Result := TPPWDetailState((Count > 0) and Items[0].IsStored);
end;

function TPPWFrameWorkDataModuleDetails.IsEqual(
  Value: TPPWFrameWorkDataModuleDetails): Boolean;
var
  I: Integer;
begin
  Result := Count = Value.Count;
  if Result then
    for I := 0 to Count - 1 do
    begin
      Result := Items[I].IsEqual(Value.Items[I]);
      if not Result then Break;
    end
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModuleDetails.SetItem 
  Author         : Stefaan Lesage
  Arguments      : Index - The index of the CollectonItem for which you want
                           to set all the Properties.
                   Value - The CollectionItem from which the Properties must be
                           copied.
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to Copy the properties of another
                   item to a specified item in the collection.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  03/12/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

function TPPWFrameWorkDataModuleDetails.ItemByDetailClassName(
  aDetailClassName: String): TPPWFrameWorkDataModuleDetail;
var
  lcv : Integer;
begin
  Result := Nil;
  for lcv := 0 to Pred( Count ) do
  begin
    if ( Items[ lcv ].DataModuleClass = aDetailClassName ) then
    begin
      Result := Items[ lcv ];
      Break;
    end;
  end;
end;

procedure TPPWFrameWorkDataModuleDetails.SetItem(Index: Integer;
  Value: TPPWFrameWorkDataModuleDetail);
begin
  Items[ Index ].Assign( Value );
//  inherited SetItem( Index, Value );
end;

procedure TPPWFrameWorkDataModuleDetails.SetState(
  const Value: TPPWDetailState);
begin
  if ( Value <> State ) then
  begin
    Clear;
  end;
end;

end.





