{*****************************************************************************
  Name           : Unit_PPWFrameWorkPropertyEditors 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2002 Peopleware                                      
  Description    : This unit will contain all Property Editors that will
                   be used by the different components of our FrameWork.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  21/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkPropertyEditors;

interface

uses
  DesignEditors, DesignIntf, Classes;

type
  TPPWDataModuleClassProperty = class( TStringProperty )
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TPPWActionDataModuleClassProperty = class( TStringProperty )
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TPPWListViewClassProperty = class( TStringProperty )
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TPPWDataModuleListViewClassProperty = class( TStringProperty )
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TPPWRecordViewClassProperty = class( TStringProperty )
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TPPWDataModuleRecordViewClassProperty = class( TStringProperty )
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TDBStringProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValueList(List: TStrings); virtual;
    procedure GetValues(Proc: TGetStrProc); override;
  end;

  TPPWDataModuleKeyFieldsProperty = class(TDBStringProperty)
  public
    procedure GetValueList(List: TStrings); override;
  end;

implementation

uses
  Unit_PPWFrameWorkController, Unit_PPWFrameWorkDataModule;

{ TPPWDataModuleClassProperty }

function TPPWDataModuleClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList, paRevertable];
end;

procedure TPPWDataModuleClassProperty.GetValues(Proc: TGetStrProc);
var
  lcv : Integer;
begin
  for lcv := 0 to pred( TPPWFrameWorkController( GetComponent( 0 ) ).RegisteredDataModuleCount ) do
  begin
    Proc( TPPWFrameWorkController( GetComponent( 0 ) ).RegisteredDataModule[ lcv ] );
  end;
end;

{ TPPWListViewClassProperty }

function TPPWListViewClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList, paRevertable];
end;

procedure TPPWListViewClassProperty.GetValues(Proc: TGetStrProc);
var
  lcv : Integer;
begin
  for lcv := 0 to pred( TPPWFrameWorkController( GetComponent( 0 ) ).RegisteredListViewCount ) do
  begin
    Proc( TPPWFrameWorkController( GetComponent( 0 ) ).RegisteredListView[ lcv ] );
  end;
end;

{ TPPWRecordViewClassProperty }

function TPPWRecordViewClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList, paRevertable];
end;

procedure TPPWRecordViewClassProperty.GetValues(Proc: TGetStrProc);
var
  lcv : Integer;
begin
  for lcv := 0 to pred( TPPWFrameWorkController( GetComponent( 0 ) ).RegisteredRecordViewCount ) do
  begin
    Proc( TPPWFrameWorkController( GetComponent( 0 ) ).RegisteredRecordView[ lcv ] );
  end;
end;

{ TPPWDataModuleListViewClassProperty }

function TPPWDataModuleListViewClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList, paRevertable];
end;

procedure TPPWDataModuleListViewClassProperty.GetValues(Proc: TGetStrProc);
var
  lcv : Integer;
begin
  if ( Assigned( PPWController ) ) then
  begin
    for lcv := 0 to pred( PPWController.RegisteredListViewCount ) do
    begin
      Proc( PPWController.RegisteredListView[ lcv ] );
    end;
  end;
end;

{ TPPWDataModuleRecordViewClassProperty }

function TPPWDataModuleRecordViewClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList, paRevertable];
end;

procedure TPPWDataModuleRecordViewClassProperty.GetValues(
  Proc: TGetStrProc);
var
  lcv : Integer;
begin
  if ( Assigned( PPWController ) ) then
  begin
    for lcv := 0 to pred( PPWController.RegisteredRecordViewCount ) do
    begin
      Proc( PPWController.RegisteredRecordView[ lcv ] );
    end;
  end;
end;

{ TPPWActionDataModuleClassProperty }

function TPPWActionDataModuleClassProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paValueList, paSortList, paRevertable];
end;

procedure TPPWActionDataModuleClassProperty.GetValues(Proc: TGetStrProc);
var
  lcv : Integer;
begin
  if ( Assigned( PPWController ) ) then
  begin
    for lcv := 0 to pred( PPWController.RegisteredDataModuleCount ) do
    begin
      Proc( PPWController.RegisteredDataModule[ lcv ] );
    end;
  end;
end;

{ TPPWDataModuleKeyFieldsProperty }

procedure TPPWDataModuleKeyFieldsProperty.GetValueList(List: TStrings);
begin
  with ( TPPWFrameWorkDataModule( GetComponent( 0 ) ) ) do
  begin
    if DataSet <> Nil then
    begin
      DataSet.GetFieldNames( List );
    end;
  end;
end;

{ TDBStringProperty }

function TDBStringProperty.GetAttributes: TPropertyAttributes;
begin

  Result := [paValueList, paSortList, paMultiSelect];
end;

procedure TDBStringProperty.GetValueList(List: TStrings);
begin

end;

procedure TDBStringProperty.GetValues(Proc: TGetStrProc);
var
  I: Integer;
  Values: TStringList;
begin
  Values := TStringList.Create;
  try
    GetValueList(Values);
    for I := 0 to Values.Count - 1 do Proc(Values[I]);
  finally
    Values.Free;
  end;
end;

end.
