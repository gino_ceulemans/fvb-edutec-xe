{*****************************************************************************
  This Unit Contains the PIFController Class which is the main Controller for
  our FrameWork.

  @Name       Unit_PPWFrameWorkController
  @Author     slesage
  @Copyright  (c) 2004 USB
  @History

  Date         By                   Description
  ----         --                   -----------
  08/07/2005   sLesage              Fix for the Values parameter in the
                                    CreateDetailListView not being initialised.
  07/07/2005   sLesage              Reworked the ShowModalRecord and added
                                    a ShowRecord which is in fact the non
                                    Modal version of ShowModalRecord.
                                    A Listview opened for Selection purposes
                                    will now be shown with windowstate wsNormal
                                    and centered on the screen.
  23/05/2005   sLesage              The SelectRecords method on the Controller
                                    will now use the OpenDataSets method when
                                    opening the ListDataSet instead of simply
                                    opening the DataSet.  This will ensure that
                                    the same logic is used for a Normal
                                    ListView and for the Selection ListView.
  27/12/2004   sLesage              Added a MaxEntityRecordViews property, which
                                    will be used to indicate the maximum number
                                    of RecordViews that can be opened for a
                                    DataModule.
  22/12/2004   sLesage              When creating a Detail ListView and DataModule
                                    you can now also pass the Master RecordView
                                    so it will be linked to the Detail ListView.
  09/12/2004   sLesage              Fixed a little bug in the
                                    CreateDetailListView method.
  18/11/2004   sLesage              Added a function which will allow you to
                                    create a DetailListView without explicitly
                                    docking it in another form.
  04/03/2004   sLesage              Added a CursorRestorer to some procedures
                                    to indicate that the system is busy.
  27/10/2003   sLesage              Fixed a problem with the ShowModalRecordView
                                    being opened and only showing part of the
                                    screen. The RecordView form needs to receive
                                    a resize so all information is shown.
  08/10/2003   sLesage              Most of the processor intesitive routines
                                    will now change the Screen Cursor to
                                    indicate that the System is Busy.
  25/09/2003   sLesage              Added some code which will set the focus
                                    on a Detail ListView if the user changes
                                    the Active Detail Page in the PageControl.
  29/08/2003   sLesage              When a Detail List View gets docked into
                                    a RecordView, its interface will get added
                                    to the DetailListViews InterfaceList.
                                    Added functionality to select multiple
                                    records using the SelectRecords method.
  07/08/2003   sLesage              Added a ShowModalRecord method which can
                                    be used to show a RecordView for one
                                    specific record Modally.
  31/07/2003   sLesage              Fixed an important bug.  Setting the
                                    ReuseRecordView to true should prevent you
                                    to open a record view ( for a same record )
                                    more than once.  This worked find when
                                    opening the RecordView from the same
                                    ListView.  It didnt't work if they were
                                    being opened from a different ListView
                                    ( eg once opening from the list of orders,
                                      and again from the list of orders that is
                                      docked into a customer detail ).
                                    This bug has now been fixed.
  22/07/2003   sLesage              Removed the GetFilterExprForDetail function
                                    it is being replaced by a
                                    BuildFilterExpressionForDetail method on
                                    the Actual DataModule.
  14/07/2003   sLesage              Fixed a bug with the resizing of a Detail
                                    form when it was Maximized and shown as a
                                    MDI Child.
  29/01/2003   Wim Lambrechts       Added class function GetFilterExprForDetail
  23/12/2002   sLesage              Fixed a Bug in the method
                                    CreateAndDockDetailsInRecordView.  After
                                    all Details have been created and docked
                                    in the RecordView, we should make a call to
                                    the Resize method to make sure the form is
                                    invalidated.
  10/12/2002   sLesage              Added support for sowing a ListView form
                                    for selection of records.
  6/12/2002    sLesage              Fixed a bug in the method
                                    CreateRecordViewForDataModule.
  3/12/2002    sLesage              Added code for createing DetailDatamodules.
                                    Added support for Keys which are based on
                                    Multiple Fields.
  20/11/2002   sLesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkController;

interface

uses
  { Delphi Units }
  Classes, Graphics, contnrs, Forms, Controls, DBClient,
  { Framework Units }
  Unit_PPWFrameWorkClasses,
  Unit_PPWFrameWorkInterfaces,
  Unit_PPWFrameWorkDataModule,
  Unit_PPWFrameWorkListView,
  Unit_PPWFrameWorkRecordView;

type
  TPPWFrameWorkController = class( TComponent )
  private
    FRegisteredDataModules : TStrings;
    FRegisteredListViews   : TStrings;
    FRegisteredRecordViews : TStrings;

    FDataModuleClass       : TPPWDataModuleClassName;
    FListViewClass         : TPPWListViewClassName;
    FRecordViewClass       : TPPWRecordViewClassName;

    FReadOnlyColor         : TColor;
    FFocusColor            : TColor;
    FRequiredColor         : TColor;

    FFocusFont             : TFont;
    FRequiredFont          : TFont;
    FReadOnlyFont          : TFont;
    FListViewStyle         : TFormStyle;
    FRecordViewStyle       : TFormStyle;
    FReuseRecordView       : Boolean;
    FEnableWhenReadOnly    : Boolean;
    FMaxEntityRecordViews  : Integer;
  protected
    { Property Getters }
    function GetActiveDataModule    : IPPWFrameWorkDataModule; virtual;
    function GetActiveFrameWorkForm : IPPWFrameWorkForm; virtual;
    function GetActiveListViewForm  : IPPWFrameWorkListView; virtual;
    function GetActiveRecordViewForm: IPPWFrameWorkRecordView; virtual;
    
    function GetRegisteredDataModule(Index: Integer): TPPWDataModuleClassName; virtual;
    function GetRegisteredDataModuleCount: integer; virtual;
    function GetRegisteredListView(Index: Integer): TPPWListViewClassName; virtual;
    function GetRegisteredListViewCount: integer; virtual;
    function GetRegisteredRecordView(Index: Integer): TPPWRecordViewClassName; virtual;
    function GetRegisteredRecordViewCount: integer; virtual;

    { Property Setters }
    procedure SetFocusFont(const Value: TFont); virtual;
    procedure SetListViewStyle(const Value: TFormStyle); virtual;
    procedure SetReadOnlyFont(const Value: TFont); virtual;
    procedure SetRecordViewStyle(const Value: TFormStyle); virtual;
    procedure SetRegisteredDataModules(const Value: TStrings); virtual;
    procedure SetRegisteredListViews(const Value: TStrings); virtual;
    procedure SetRegisteredRecordViews(const Value: TStrings); virtual;
    procedure SetRequiredFont(const Value: TFont); virtual;

    { FrameWork DataModule, ListView and RecordView Registration and
      UnRegistration procedures }
    procedure RegisterDataModuleClass  ( const ADataModuleClass : TPPWFrameWorkDataModuleClass ); virtual;
    procedure RegisterListViewClass    ( const AListViewClass   : TPPWFrameWorkListViewClass   ); virtual;
    procedure RegisterRecordViewClass  ( const ARecordViewClass : TPPWFrameWorkRecordViewClass ); virtual;
    procedure UnRegisterDataModuleClass( const ADataModuleClass : TPPWFrameWorkDataModuleClass ); virtual;
    procedure UnRegisterListViewClass  ( const AListViewClass   : TPPWFrameWorkListViewClass   ); virtual;
    procedure UnRegisterRecordViewClass( const ARecordViewClass : TPPWFrameWorkRecordViewClass ); virtual;

    procedure DoPageChanged( Sender : TObject ); virtual;
    
    { Other Methods }
    procedure DockDetailDataModule( aRecordView : IPPWFrameWorkRecordView;
                                    aDetailDataModule : IPPWFrameWorkDataModule;
                                    var aControl : TWinControl;
                                    aFields  : String;
                                    aCaption : String;
                                    aDetailIndex : Integer;
                                    var aListView: IPPWFrameWorkListView ); virtual;

  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;
    procedure Loaded; override;

    { FrameWork Form and DataModule creation procedures }
    function CreateDetailDataModule( const AFrameWorkDataModule : IPPWFrameWorkDataModule; const AFrameWorkDataModuleClassName : string;
      const ARecordView: IPPWFrameWorkRecordView ) : IPPWFrameWorkDataModule; virtual; //added WL 17/12: added parameter
    function CreateDataModule( AOwner : TComponent; const AFrameWorkDataModuleClassName : string; AsSelection : Boolean = False ) : IPPWFrameWorkDataModule; virtual;
    function CreateListViewForDataModule( const aFrameWorkDataModule : IPPWFrameWorkDataModule; const AsDetail : Boolean = False; const ARecordView : IPPWFrameWorkRecordView = nil ) : IPPWFrameWorkListView; virtual;
    function CreateRecordViewForDataModule( const aFrameWorkDataModule : IPPWFrameWorkDataModule; const RecordViewMode : TPPWFrameWorkRecordViewMode ) : IPPWFrameWorkRecordView; virtual;

    function CreateDetailListView( aMasterDataModule          : IPPWFrameWorkDataModule;
                                   aDetailDataModuleClassName : string;
                                   aRecordView                : IPPWFrameWorkRecordView = Nil ) : IPPWFrameWorkListView;


    function ShowModalRecord( const AFrameWorkDataModuleClassName, AWhereClause : string; aClientDataSet : TClientDataSet; const RecordViewMode : TPPWFrameWorkRecordViewMode; aWithAutoOpen : Boolean = True ) : TModalResult; virtual;
    procedure ShowRecord( const AFrameWorkDataModuleClassName, AWhereClause : string; const aRecordViewMode: TPPWFrameWorkRecordViewMode; aWithAutoOpen : Boolean = True ); virtual;

    function SelectRecords( const AFrameWorkDataModuleClassName, AWhereClause : string; aClientDataSet : TClientDataSet; aMultiSelect : Boolean = False ) : TModalResult;overload;
    function SelectRecords( const AFrameWorkDataModuleClassName, AWhereClause : string; aClientDataSet, aSearchCriteria : TClientDataSet; aMultiSelect : Boolean = False ) : TModalResult;overload;



    procedure CreateAndDockDetailsInRecordView( const AFrameWorkDataModule : IPPWFrameWorkDataModule; const ARecordView : IPPWFrameWorkRecordView ); virtual;

    procedure CreateLinkAndShowDetailListView( const aMasterDataModule : IPPWFrameWorkDataModule;
                                               const aDetailDataModuleClassName : String;
                                               const aRecordView                : IPPWFrameWorkRecordView );

    function CreateRecordDataModule( const AFrameWorkDataModuleClassName, AWhereClause : string; const RecordViewMode: TPPWFrameWorkRecordViewMode ) : IPPWFrameWorkDataModule;

    procedure ShowListViewForDataModule( const aFrameWorkDataModule : IPPWFrameWorkDataModule ); virtual;
    procedure ShowRecordViewForDataModule( const aFrameWorkDataModule : IPPWFrameWorkDataModule; const RecordViewMode : TPPWFrameWorkRecordViewMode ); virtual;
    function RecordViewForCurrentRecord ( aFrameWorkDataModule : IPPWFrameWorkDataModule ) : IPPWFrameWorkRecordView; Virtual;

    { FrameWork Form and DataModule registration procedure }
    procedure RegisterFrameWorkClass   ( const AFrameWorkClass : TPPWFrameWorkDataModuleClass ); overload; virtual;
    procedure RegisterFrameWorkClass   ( const AFrameWorkClass : TPPWFrameWorkListViewClass   ); overload; virtual;
    procedure RegisterFrameWorkClass   ( const AFrameWorkClass : TPPWFrameWorkRecordViewClass ); overload; virtual;

    { FrameWork Form and DataModule unregistration procedure }
    procedure UnRegisterFrameWorkClass   ( const AFrameWorkClass : TPPWFrameWorkDataModuleClass ); overload; virtual;
    procedure UnRegisterFrameWorkClass   ( const AFrameWorkClass : TPPWFrameWorkListViewClass   ); overload; virtual;
    procedure UnRegisterFrameWorkClass   ( const AFrameWorkClass : TPPWFrameWorkRecordViewClass ); overload; virtual;

    { Properties for the Registered FrameWork Forms and DataModules }
    property RegisteredDataModuleCount : integer read GetRegisteredDataModuleCount;
    property RegisteredListViewCount   : integer read GetRegisteredListViewCount;
    property RegisteredRecordViewCount : integer read GetRegisteredRecordViewCount;
    property RegisteredDataModule[ Index : Integer ] : TPPWDataModuleClassName read GetRegisteredDataModule;
    property RegisteredListView  [ Index : Integer ] : TPPWListViewClassName   read GetRegisteredListView;
    property RegisteredRecordView[ Index : Integer ] : TPPWRecordViewClassName read GetRegisteredRecordView;
  published
    { ClassName properties which will be used to set a default DataModule,
      ListView and RecordView class }
    property DataModuleClass : TPPWDataModuleClassName read FDataModuleClass
                                                       write FDataModuleClass;
    property ListViewClass   : TPPWListViewClassName   read FListViewClass
                                                       write FListViewClass;
    property ListViewStyle   : TFormStyle read FListViewStyle write SetListViewStyle;
    property RecordViewClass : TPPWRecordViewClassName read FRecordViewClass
                                                       write FRecordViewClass;
    property RecordViewStyle   : TFormStyle read FRecordViewStyle write SetRecordViewStyle;

    { Active Forms and Datamodules Properties }
    property ActiveDataModule     : IPPWFrameWorkDataModule read GetActiveDataModule;
    property ActiveFrameWorkForm  : IPPWFrameWorkForm       read GetActiveFrameWorkForm;
    property ActiveListViewForm   : IPPWFrameWorkListView   read GetActiveListViewForm;
    property ActiveRecordViewForm : IPPWFrameWorkRecordView read GetActiveRecordViewForm;

    { Font and Color Properties }
    property EnableWhenReadOnly : Boolean              read FEnableWhenReadOnly
                                                       write FEnableWhenReadOnly;
    property FocusColor      : TColor                  read FFocusColor
                                                       write FFocusColor
                                                       default clWindow;
    property FocusFont       : TFont                   read FFocusFont
                                                       write SetFocusFont;
    property ReadOnlyColor   : TColor                  read FReadOnlyColor
                                                       write FReadOnlyColor
                                                       default clWindow;
    property ReadOnlyFont    : TFont                   read FReadOnlyFont
                                                       write SetReadOnlyFont;
    property RequiredColor   : TColor                  read FRequiredColor
                                                       write FRequiredColor
                                                       default clWindow;
    property RequiredFont    : TFont                   read FRequiredFont
                                                       write SetRequiredFont;
    property ReuseRecordView : Boolean                 read FReuseRecordView
                                                       write FReuseRecordView;

    { Registered Forms and DataModules }
    property RegisteredDataModules : TStrings          read FRegisteredDataModules
                                                       write SetRegisteredDataModules;
    property RegisteredListViews   : TStrings          read FRegisteredListViews
                                                       write SetRegisteredListViews;
    property RegisteredRecordViews : TStrings          read FRegisteredRecordViews
                                                       write SetRegisteredRecordViews;
    property MaxEntityRecordViews : Integer            read FMaxEntityRecordViews
                                                       write FMaxEntityRecordViews;
  end;

var
  PPWController     : TPPWFrameWorkController;

implementation

uses
  {$IFDEF CODESITE}
  csIntf, Unit_PPWFrameWorkCodeSiteObjects,
  {$ENDIF}
  { Delphi Units }
  SysUtils, extCtrls, ComCtrls, Variants, DBConsts, Windows, Math, DB,
  { FrameWork Units }
  Unit_PPWFrameWorkExceptions, Unit_PPWFrameWorkUtils;


type
  THackField = class( TField );

{ TPPWFrameWorkController }

{*****************************************************************************
  Overridden Constructor in Which we will do some Initialisations.

  @Name       TPPWFrameWorkController.Create
  @author     slesage
  @param      AOwner   The owner of the Component.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


constructor TPPWFrameWorkController.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );

  { Set the standerd colors and fonts }
  FFocusColor    := clWindow;
  FFocusFont     := TFont.Create;
  FReadOnlyColor := clWindow;
  FReadOnlyFont  := TFont.Create;
  FRequiredColor := clWindow;
  FRequiredFont  := TFont.Create;
  FEnableWhenReadOnly := True;

  FRegisteredDataModules := TStringList.Create;
  FRegisteredListViews   := TStringList.Create;
  FRegisteredRecordViews := TStringList.Create;

  PPWController := Self;
end;

{*****************************************************************************
  This method will be used to create all Details for a PIFDataModule.
  Internally it will create all Detail datamodules for the specified
  aFrameWorkDataModule, create their respective ListViewForm and dock
  it in the supplied aRecordView From.

  @Name       TPPWFrameWorkController.CreateAndDockDetailsInRecordView
  @author     slesage
  @param      aFrameWorkDataModule - The FrameWorkDataModule for which
                                     the Details must be created and
                                     Docked
  @param      aRecordView          - The RecordView form in which the
                                     ListViews of all Details must be
                                     Docked.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.CreateAndDockDetailsInRecordView(
  const AFrameWorkDataModule: IPPWFrameWorkDataModule;
  const ARecordView: IPPWFrameWorkRecordView);
var
  CursorRestorer : IPPWRestorer;
  lcv          : Integer;
  aDetailDataModule : IPPWFrameWorkDataModule;
  aControl          : TWinControl;
  aForeignKeys      : String;
  //aFieldList        : TList;
  aFieldValues      : Variant;
  //Value             : Variant;
  //ValStr            : String;
  Expr      : String;
  CanCreate         : Boolean;
  aListView         : IPPWFrameWorkListView;
  foo               : TScrollBox;
begin
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  if ( Assigned( AFrameWorkDataModule ) ) and
     ( Assigned( ARecordView ) ) then
  begin
    LockWindowUpdate( ARecordView.Form.Handle );

    aControl := Nil;

    if (AFrameWorkDataModule.DetailDataModules.Count = 0) then
    begin //in this case the Scrollbox should be set alClient
      foo := TScrollBox( aRecordView.FindComponent( 'sbxMain' ) );
      foo.Align := alClient;
    end;

    for lcv := 0 to pred( AFrameWorkDataModule.DetailDataModules.Count ) do
    begin
      if ( AFrameWorkDataModule.DetailDataModules.Items[ lcv ].AutoCreate ) then
      begin
        CanCreate := True;

        if ( Assigned( AFrameWorkDataModule.OnBeforeCreateDetailDataModule ) ) then
        begin
          AFrameWorkDataModule.OnBeforeCreateDetailDataModule( Self, AFrameWorkDataModule.DetailDataModules.Items[ lcv ].DataModuleClass, CanCreate );
        end;

        if ( CanCreate ) then
        begin

          aDetailDataModule := CreateDetailDataModule( AFrameWorkDataModule,
                                                       AFrameWorkDataModule.DetailDataModules.Items[ lcv ].DataModuleClass,
                                                       ARecordView ); //added WL 17/12: added parameter aRecordView

          aForeignKeys := AFrameWorkDataModule.DetailDataModules.Items[ lcv ].ForeignKeys;
          aDetailDataModule.ForeignKeys := aForeignKeys;

          if ( Assigned( AFrameWorkDataModule.OnAfterCreateDetailDataModule ) ) then
          begin //added WL 18/12
            AFrameWorkDataModule.OnAfterCreateDetailDataModule( self, AFrameWorkDataModule.DetailDataModules.Items[ lcv ].DataModuleClass, aDetailDataModule);
          end;

          if ( Assigned( aDetailDataModule ) ) then
          begin
            Expr := aDetailDataModule.BuildFilterExpressionForDetail( aFrameWorkDataModule,
                                                                      aDetailDataModule,
                                                                      aForeignKeys,
                                                                      True );

            aDetailDataModule.DoFilterDetail( aForeignKeys, aFieldValues, Expr );

            DockDetailDataModule( aRecordView, aDetailDataModule, aControl,
                                  AFrameWorkDataModule.DetailDataModules.Items[ lcv ].ForeignKeys,
                                  AFrameWorkDataModule.DetailDataModules.Items[ lcv ].Caption,
                                  lcv, aListView );
            if assigned (ARecordView.OnDetailListViewCreated) then
              ARecordView.OnDetailListViewCreated (self, aDetailDataModule.ListViewClass, aListView);
          end;
        end;
      end;
    end;

    LockWindowUpdate( 0 );

    { Now Broadcast the WM_MASTERRECORDVIEWMODECHANGED so the detail
      ListView forms can react to that }
    aRecordView.DoModeChanged;

    if ( Assigned( ARecordView.OnAllDetailsCreated ) ) then
    begin
      ARecordView.OnAllDetailsCreated( Self );
    end;
  end;
end;

{*****************************************************************************
  This function will be used to create an instance of a FrameWorkDataModule.

  @Name       TPPWFrameWorkController.CreateDataModule
  @author     slesage
  @param      AOwner                        - The Owner of the DataModule
  @param      AFrameWorkDataModuleClassName - The ClassName of the DataModule
                                              that must be created.
  @return     Returns the Interface to the new PIFDataModule.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.CreateDataModule( AOwner : TComponent; const AFrameWorkDataModuleClassName : string; AsSelection : Boolean = False ) : IPPWFrameWorkDataModule;
var
  CursorRestorer       : IPPWRestorer;
  aClass               : TPersistentClass;
  Index                : Integer;
begin
  { Change the cursor to crHourGlass so the user knows the system is Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  Index := RegisteredDataModules.IndexOf( AFrameWorkDataModuleClassName );

  if ( Index <> - 1 ) then
  begin
    aClass := FindClass( AFrameWorkDataModuleClassName );

    Result := TPPWFrameWorkDataModuleClass( aClass ).Create( AOwner, AsSelection );
  end
  else
  begin
    raise EPPWUnregisteredDataModuleError.CreateFmt(
            rsPPWFrameWorkUnRegisteredDataModule, [ AFrameWorkDataModuleClassName ] );
  end;
end;

{*****************************************************************************
  This function will be used to create an instance of a FrameWorkDataModule
  which will be used as a detail of another DataModule.

  @Name       TPPWFrameWorkController.CreateDetailDataModule
  @author     slesage
  @param      aFrameWorkDataModule          - The PIFDataModule which will act
                                              as the MasterDataModule.
  @param      aFrameWorkDataModuleClassName - The ClassName of the PIFDataModule
                                              which should be created as a Detai-
                                              DataModule.
  @param      aRecordView                   - The RecordView in which the Detail
                                              Should be Docked.
  @return     Returns an interface to the Created PIFDataModule
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.CreateDetailDataModule(
  const AFrameWorkDataModule : IPPWFrameWorkDataModule;
  const AFrameWorkDataModuleClassName: string;
  const ARecordView: IPPWFrameworkRecordView): IPPWFrameWorkDataModule;
var
  CursorRestorer : IPPWRestorer;
  aClass         : TPersistentClass;
  Index          : Integer;
begin
  { Change the cursor to crHourGlass so the user knows the system is
    Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  Index := RegisteredDataModules.IndexOf( AFrameWorkDataModuleClassName );

  if ( Index <> - 1 ) then
  begin
    aClass := FindClass( AFrameWorkDataModuleClassName );
    Result := TPPWFrameWorkDataModuleClass( aClass ).CreateAsDetail( Nil, AFrameWorkDataModule, ARecordView );
  end
  else
  begin
    raise EPPWUnregisteredDataModuleError.CreateFmt(
            rsPPWFrameWorkUnRegisteredDataModule, [ AFrameWorkDataModuleClassName ] );
  end;
end;

{*****************************************************************************
  This method will be used to create a PIFListView form for the supplied
  PIFDatamodule and return a reference to it.

  @Name       TPPWFrameWorkController.CreateListViewForDataModule
  @author     slesage
  @param      aFrameWorkDataModule - Reference to the DataModule for which we
                                     want to create a ListView.
  @param      AsDetail             - Will be used to indicate if the ListView
                                     will be used as a Detail of another
                                     RecordView.
  @param      aRecordView          - The RecordView in which the new ListView
                                     should be docked ( in case AsDetail is
                                     true )
  @return     Returns a reference to the ListView form if it was created,
              otherwise it returns Nil.
  @Exception  EPPWUnregisteredListViewError - This Exception will be
                raised if the ListViewClass supplied on the DataModule
                hasn't been Registered.
              EPPWListViewClassNotSupplied  - This Exception will be
                raised if there was no ListView class supplied on the
                DataModule.
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.CreateListViewForDataModule(
  const aFrameWorkDataModule: IPPWFrameWorkDataModule;
  const AsDetail : Boolean = False;
  const ARecordView : IPPWFrameWorkRecordView = nil ): IPPWFrameWorkListView;
var
  CursorRestorer     : IPPWRestorer;
  aListViewClassName : TPPWListViewClassName;
  aClass             : TPersistentClass;
  aIndex             : Integer;
begin
  {$IFDEF CODESITE}
  csFWController.EnterMethod( Self, 'CreateListViewForDataModule' );
  {$ENDIF}

  { Change the cursor to crHourGlass so the user knows the system is
    Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  Result := Nil;

  if ( Assigned( aFrameWorkDataModule ) ) then
  begin
    aListViewClassName := aFrameWorkDataModule.ListViewClass;

    if ( aListViewClassName <> '' ) then
    begin
      aIndex := RegisteredListViews.IndexOf( aListViewClassName );

      if ( aIndex <> - 1 ) then
      begin
        aClass := FindClass( aListViewClassName );
        if ( AsDetail ) then
        begin
          Result := TPPWFrameWorkListViewClass( aClass ).Create( Self, aFrameWorkDataModule, True, aRecordView );
        end
        else
        begin
          Result := TPPWFrameWorkListViewClass( aClass ).Create( Self, aFrameWorkDataModule );
        end;
      end
      else
      begin
        Raise EPPWUnregisteredListViewError.CreateFmt(
                rsPPWFrameWorkUnRegisteredListView, [ aListViewClassName ] );
      end;

      aFrameWorkDataModule.ListView := Result;
      aFrameWorkDataModule.LinkToListView( Result );
    end
    else
    begin
      Raise EPPWListViewClassNotSupplied.CreateFmt(
               rsPPWFrameWorkNoListViewClassSupplied, [ aFrameWorkDataModule.ClassType.ClassName ] );
    end;
  end;

  {$IFDEF CODESITE}
  csFWController.ExitMethod( Self, 'CreateListViewForDataModule' );
  {$ENDIF}
end;

{*****************************************************************************
  This function will be used to display a RecordView modally for one record in
  the  specific DataModule.
  The record that was displayed in the recordview will be copied into the
  ClientDataSet that is passed to the function ( with possible changes the user
  might have applied ).

  @Name       TPPWFrameWorkController.ShowModalRecord
  @author     slesage
  @param      AFrameWorkDataModuleClassName - The ClassName of the DataModule
                                              that must be created.
  @param      AWhereClause                  - The WhereClause that should be
                                              used to filter the records that
                                              should be available for selection.
  @param      AClientDataSet                - The ClientDataSet in which the
                                              selected records will be copied
  @param      RecordViewMode                - The Mode in which the RecordView
                                              must be shown.
  @param      aWithAutoOpen                 - Will be used to indicate if the
                                              DataSets should be automatically
                                              Opened.  
  @return     The result of this function will be the ModalResult of the
              RecordView form.
  @Exception  None
  @See        None
 *****************************************************************************}

function TPPWFrameWorkController.ShowModalRecord(
  const AFrameWorkDataModuleClassName, AWhereClause: string;
  aClientDataSet: TClientDataSet;
  const RecordViewMode: TPPWFrameWorkRecordViewMode
  ; aWithAutoOpen : Boolean = True): TModalResult;
Var
  aFrameWorkDataModule : IPPWFrameWorkDataModule;
  aRecordView  : IPPWFrameWorkRecordView;
begin
  { Set the Result to mrCancel by Default }
  Result := mrCancel;

  if ( Assigned( aClientDataSet ) ) then
  begin
    aFrameWorkDataModule := CreateRecordDataModule( aFrameWorkDataModuleClassName, aWhereClause, RecordViewMode );

    if ( Assigned( aFrameWorkDataModule ) ) then
    begin
      aRecordView := CreateRecordViewForDataModule( aFrameWorkDataModule, RecordViewMode );
      if ( Assigned( aRecordView ) ) then
      begin
        if not (aRecordView.SettingsSaved) then
        begin
          aRecordView.WindowState := wsNormal;
          aRecordView.Position := poScreenCenter;
        end;

        if ( aRecordView.ShowModal = mrOK ) then
        begin
          CloneStructur( aRecordView.RecordViewDatamodule.RecordDataset,
                         aClientDataSet );
          CloneRecord  ( aRecordView.RecordViewDatamodule.RecordDataset,
                         aClientDataSet );
          Result := mrOK;
        end;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to create a RecordView form for the supplied
  PIFDatamodule and return a reference to it.

  @Name       TPPWFrameWorkController.CreateRecordViewForDataModule
  @author     slesage
  @param      aFrameWorkDataModule - Reference to the DataModule for which we
                                     want to create a ListView.
  @param      RecordViewMode       - The Mode in which the RecordView must be
                                     opened ( Edit, Add, Delete, or View ).
  @return     Returns a reference to the RecordView form if it was created,
              otherwise it returns Nil.
  @Exception  EPPWUnregisteredRecordViewError - This Exception will be raised
                 if the RecordViewClass supplied on the DataModule hasn't been
                 Registered.
              EPPWRecordViewClassNotSupplied  - This Exception will be raised
                 if there was no RecordView class supplied on the DataModule.
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.CreateRecordViewForDataModule(
  const aFrameWorkDataModule: IPPWFrameWorkDataModule;
  const RecordViewMode: TPPWFrameWorkRecordViewMode): IPPWFrameWorkRecordView;
var
  CursorRestorer       : IPPWRestorer;
  aRecordViewClassName : TPPWRecordViewClassName;
  aClass               : TPersistentClass;
  aIndex               : Integer;
begin
  {$IFDEF CODESITE}
  csFWController.EnterMethod( Self, 'CreateRecordViewForDataModule' );
  {$ENDIF}

  { Change the cursor to crHourGlass so the user knows the system is
    Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  Result := Nil;

  if ( Assigned( aFrameWorkDataModule ) ) then
  begin
    { First Check if the ReuseRecordView is set, if it is and we are
      not openeing a RecordView in Add mode, then we should check if
      we don't already have a RecordView open for this record }
    if ( RecordViewMode <> rvmAdd ) and
       ( ReuseRecordView ) then
    begin
      Result := RecordViewForCurrentRecord( aFrameWorkDataModule );

      if ( Assigned( Result ) ) then
      begin
        if ( Result.Mode <> RecordViewMode ) then
        begin
          { RecordView already Exsists }
          Result := Nil;
          raise EPPWRecordViewAlreadyOpen.Create;
        end
        else
        begin
          if ( Result.WindowState = wsMinimized ) then
          begin
            Result.WindowState := wsNormal;
          end;
        end;
      end;
    end;

    if not ( Assigned( Result ) ) then
    begin
      aRecordViewClassName := aFrameWorkDataModule.RecordViewClass;

      if ( aRecordViewClassName <> '' ) then
      begin
        aIndex := RegisteredRecordViews.IndexOf( aRecordViewClassName );

        if ( aIndex <> - 1 ) then
        begin
          aClass := FindClass( aRecordViewClassName );
          Result := TPPWFrameWorkRecordViewClass( aClass ).Create( Self, aFrameWorkDataModule{, RecordViewMode} );

          if ( Assigned( Result ) ) then
          begin
            LockWindowUpdate( Result.Form.Handle );
            aFrameWorkDataModule.RecordViews.Add( Result );
            aFrameWorkDataModule.LinkToRecordView( Result, RecordViewMode );
            CreateAndDockDetailsInRecordView( aFrameWorkDataModule, Result );
            LockWindowUpdate( 0 );

            Result.DoModeChanged;
          end;
        end
        else
        begin
          Raise EPPWUnregisteredRecordViewError.CreateFmt(
                  rsPPWFrameWorkUnRegisteredRecordView, [ aRecordViewClassName ] );
        end;
      end
      else
      begin
        Raise EPPWRecordViewClassNotSupplied.CreateFmt(
                 rsPPWFrameWorkNoRecordViewClassSupplied, [ aFrameWorkDataModule.ClassType.ClassName ] );
      end;
    end;
  end;

  {$IFDEF CODESITE}
  csFWController.ExitMethod( Self, 'CreateRecordViewForDataModule' );
  {$ENDIF}
end;

{*****************************************************************************
  The destructor for our component in which we will clean up some things.

  @Name       TPPWFrameWorkController.Destroy
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


destructor TPPWFrameWorkController.Destroy;
begin
  FreeAndNil( FRequiredFont );
  FreeAndNil( FReadOnlyFont );
  FreeAndNil( FFocusFont );

  FreeAndNil( FRegisteredDataModules );
  FreeAndNil( FRegisteredListViews );
  FreeAndNil( FRegisteredRecordViews );

//  if ( csDesigning in ComponentState ) then
//  begin
    PPWController := Nil;
//  end;

  inherited Destroy;
end;

{*****************************************************************************
  This method will be used to actually Dock the Detail ListView in the Given
  Control.

  @Name       TPPWFrameWorkController.DockDetailDataModule
  @author     slesage
  @param      aRecordView       - A reference to the RecordView
  @param      aDetailDataModule - A reference to the DetailDataModule for which
                                  we want to create a ListView and Dock it in
                                  the given Control
  @param      aControl          - The Control in which the Detail ListView must
                                  be docked.
  @param      aFields           -
  @param      aCaption          - The caption that must be used for the Docked
                                  form.
  @param      aDetailIndex      - The index of the Docked form in the given
                                  Control ( its position in the PageControl )
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.DockDetailDataModule(
  aRecordView : IPPWFrameWorkRecordView;
  aDetailDataModule: IPPWFrameWorkDataModule;
  var aControl: TWinControl;
  aFields, aCaption: String;
  aDetailIndex: Integer;
  var aListView: IPPWFrameWorkListView);
var
  aScroll      : TScrollBox;
  aPageControl : TPageControl;
  foo          : TComponent;
begin
  {$IFDEF CODESITE}
  csFWController.EnterMethod( Self, 'DockDetailDataModule' );
  {$ENDIF}

  if ( Assigned( aRecordView ) ) then
  begin
    if not (Assigned (aControl)) then //added WL 21/12
    begin
      foo := aRecordView.FindComponent('pctrlMain');
      if foo is TPageControl then aControl := TWinControl(foo);
    end;

    if Not ( Assigned( aControl ) ) then
    begin
      aScroll := TScrollBox( aRecordView.FindComponent( 'sbxMain' ) );

      if ( Assigned( aScroll ) ) and (not(aRecordView.SettingsSaved)) then
      begin
        aScroll.Align := alNone;
        if ( aScroll.VertScrollBar.Visible ) then
        begin
            aScroll.Height := Min (aScroll.Constraints.MinHeight + 80, aScroll.Height);
        end
        else
        begin
            aScroll.Height := aScroll.Constraints.MinHeight;
        end;
        if ( aScroll.HorzScrollBar.Visible ) then
        begin
            aScroll.Width := aScroll.Constraints.MinWidth + 80;
        end
        else
        begin
            aScroll.Width := aScroll.Constraints.MinWidth;
        end;
      end;

      if Assigned(aScroll) then
      begin
        aScroll.Align := alTop;
        aScroll.Realign;
      end;


      with TSplitter.Create( aRecordView.Form ) do
      begin
        if ( Assigned( aScroll ) ) then
        begin
          Parent := aScroll.Parent;
          Top := AScroll.Top + AScroll.Height;
        end
        else
        begin
          Parent := ARecordView.Form;
        end;
        Align := alTop;
      end;

      aPageControl := TPageControl.Create( aRecordView.Form );
      with APageControl do
      begin
        if ( Assigned( aScroll ) ) then
        begin
          Parent := aScroll.Parent;
          Top := AScroll.Top + AScroll.Height;
        end
        else
        begin
          Parent := ARecordView.Form;
        end;
        Align := alClient;
        Height := 130;
        Constraints.MinHeight := 120;
      end;
      aControl := APageControl;

      TPageControl( aControl ).OnChange := DoPageChanged;
    end;

    { Filter the Detail DataModule }
    aListView := CreateListViewForDataModule( aDetailDataModule, True, aRecordView );
    aListView.MasterRecordView := aRecordView;
    aListView.UseDockManager := False;
    aListView.Caption := aCaption;
    aListView.Align := alClient;
    aListView.ManualDock( aControl );
//    aListView.Broadcast( WM_MASTERRECORDVIEWMODECHANGED );
    aListView.Show;

    { Add the Detail List to the DetailListViews of the RecordView }
    ARecordView.DetailListViews.Add( aListView );
  end;
  {$IFDEF CODESITE}
  csFWController.ExitMethod( Self, 'DockDetailDataModule' );
  {$ENDIF}
end;

{*****************************************************************************
  This method returns the Active FrameWork datamodule.

  @Name       TPPWFrameWorkController.GetActiveDataModule
  @author     slesage
  @param      None
  @return     This method will call the GetActiveFrameWorkForm and if an
              ActiveFrameWorkForm was found, it will return its reference
              to the FrameWork DataModule. If no ActiveFrameWorkForm was found,
              it will return Nil.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetActiveDataModule: IPPWFrameWorkDataModule;
begin
  Result := Nil;

  if Assigned( ActiveFrameWorkForm ) then
  begin
    Result := ActiveFrameWorkForm.FrameWorkDataModule;
  end;
end;

{*****************************************************************************
  This method will return the currently active FrameWork Form.

  @Name       TPPWFrameWorkController.GetActiveFrameWorkForm
  @author     slesage
  @param      None
  @return     Returns the Currently active FrameWorkForm if one was found,
              otherwise it will Return Nil.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetActiveFrameWorkForm: IPPWFrameWorkForm;
var
  tmpControl : TControl;
begin
  Result := Nil;

  tmpControl := Screen.ActiveControl;

  if ( Assigned( tmpControl ) ) then
  begin
    repeat
      if Supports( tmpControl, IPPWFrameWorkForm, Result ) then
      begin
        Break;
      end
      else
      begin
        tmpControl := tmpControl.Parent;
      end;
    until ( tmpControl = nil ); //( tmpControl.Parent = Nil ) ;
  end;
end;

{*****************************************************************************
  This method will return the currently active ListView Form.

  @Name       TPPWFrameWorkController.GetActiveListViewForm
  @author     slesage
  @param      None
  @return     Returns the Currently active ListViewForm if one was found,
              otherwise it will Return Nil.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetActiveListViewForm: IPPWFrameWorkListView;
begin
  Supports( ActiveFrameWorkForm, IPPWFrameWorkListView, Result );
end;

{*****************************************************************************
  This method will return the currently active RecordView Form.

  @Name       TPPWFrameWorkController.GetActiveRecordViewForm
  @author     slesage
  @param      None
  @return     Returns the Currently active RecordViewForm if one was found,
              otherwise it will Return Nil.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetActiveRecordViewForm: IPPWFrameWorkRecordView;
begin
  Supports( ActiveFrameWorkForm, IPPWFrameWorkRecordView, Result );
end;

{*****************************************************************************
  This method will be used to retun the ClassName of the RegisteredDatamodule
  at the given index in the RegisteredDataModules list.

  @Name       TPPWFrameWorkController.GetRegisteredDataModule
  @author     slesage
  @param      Index - Position of the DataModule in the List for which you
                      want to return the ClassName.
  @return     Returns the ClassName of the Corresponding DataModule.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetRegisteredDataModule(
  Index: Integer): TPPWDataModuleClassName;
begin
  Result := FRegisteredDataModules.Strings[ Index ];
end;

{*****************************************************************************
  This method will be used to return the number of RegisteredDataModules.

  @Name       TPPWFrameWorkController.GetRegisteredDataModuleCount
  @author     slesage
  @param      None
  @return     Returns the Number of RegisteredDatamodules.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetRegisteredDataModuleCount: integer;
begin
  Result := FRegisteredDataModules.Count;
end;

{*****************************************************************************
  This method will be used to retun the ClassName of the RegisteredListView at
  the given index in the RegisteredListViews list.

  @Name       TPPWFrameWorkController.GetRegisteredListView
  @author     slesage
  @param      Index - Position of the ListView in the List for which you
                      want to return the ClassName.
  @return     Returns the ClassName of the Corresponding ListView.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetRegisteredListView(
  Index: Integer): TPPWListViewClassName;
begin
  Result := FRegisteredListViews.Strings[ Index ];
end;

{*****************************************************************************
  This method will be used to return the number of RegisteredListViews.

  @Name       TPPWFrameWorkController.GetRegisteredListViewCount
  @author     slesage
  @param      None
  @return     Returns the Number of RegisteredListViews.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetRegisteredListViewCount: integer;
begin
  Result := FRegisteredListViews.Count;
end;

{*****************************************************************************
  his method will be used to retun the ClassName of the RegisteredRecordView
  at the given index in the RegisteredRecordViews list.

  @Name       TPPWFrameWorkController.GetRegisteredRecordView
  @author     slesage
  @param      Index - Position of the RecordView in the List for which you
                      want to return the ClassName.
  @return     Returns the ClassName of the Corresponding RecordView.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetRegisteredRecordView(
  Index: Integer): TPPWRecordViewClassName;
begin
  Result := FRegisteredRecordViews.Strings[ Index ];
end;

{*****************************************************************************
  This method will be used to return the number of RegisteredRecordViews.

  @Name       TPPWFrameWorkController.GetRegisteredRecordViewCount
  @author     slesage
  @param      None
  @return     Returns the Number of RegisteredRecordViews.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.GetRegisteredRecordViewCount: integer;
begin
  Result := FRegisteredRecordViews.Count;
end;

procedure TPPWFrameWorkController.Loaded;
begin
  inherited loaded;
end;

{*****************************************************************************
  This Function will be used to check if there is already a RecordView open
  for the current record of the given FrameWorkDataModule.  If that was the
  case, it will return the interface to that RecordView, otherwise it returns
  Nil.

  @Name       TPPWFrameWorkController.RecordViewForCurrentRecord
  @author     slesage
  @param      aFrameWorkDataModule - The datamodule for which you want to check
                                     if there is already a RecordView open for
                                     the current record.
  @return     Returns the first open RecordView for the current record of the
              FrameWorkDataModule if any was found.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.RecordViewForCurrentRecord(
  aFrameWorkDataModule: IPPWFrameWorkDataModule): IPPWFrameWorkRecordView;
var
  CursorRestorer       : IPPWRestorer;
  aOpenFWDataModule    : IPPWFrameWorkDataModule;
  aOpenFWRecordView    : IPPWFrameWorkRecordView;
  aDataSet             : TDataSet;
  Fields               : TList;
  SameKeyFields        : Boolean;
  lcvdtm, lcvrvw, lcv  : Integer;
begin
  { Change the cursor to crHourGlass so the user knows the system is
    Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  { Loop over all Open DataModules }
  for lcvdtm := 0 to Pred( Screen.DataModuleCount ) do
  begin
    { Check if it is a IPPWFrameWorkDataModule and if it is of the same
      ClassType as the aFrameWorkDataModule }
    if ( Supports( Screen.DataModules[ lcvdtm ], IPPWFrameWorkDataModule,
                   aOpenFWDataModule ) ) and
       ( aOpenFWDataModule.ClassType = aFrameWorkDataModule.ClassType ) then
    begin
      if ( Assigned ( aFrameWorkDataModule.ListViewDataModule ) ) then
      begin
        aDataSet := aFrameWorkDataModule.RecordDataset;
      end
      else
      begin
        aDataSet := aFrameWorkDataModule.DataSet;
      end;

      { Now loop over the RecordViews for the FrameWorkDataModule we just found }
      for lcvrvw := 0 to Pred( aOpenFWDataModule.RecordViews.Count ) do
      begin
        aOpenFWRecordView := IPPWFrameWorkRecordView( aOpenFWDataModule.RecordViews.Items[ lcvrvw ] );

        with aOpenFWRecordView do
        begin
          if ( Assigned( DataSource ) ) and
             ( Assigned( DataSource.DataSet ) ) and
             ( Assigned( aDataSet ) ) then
          begin
            Fields := TList.Create;
            SameKeyFields := True;

            Try
              { Get the KeyFields from the RecordView }
              DataSource.DataSet.GetFieldList( Fields, aFrameWorkDataModule.KeyFields );

              { Check if all values from all KeyFields are equal }
              for lcv := 0 to Pred( Fields.Count ) do
              begin
                if ( TField( Fields[ lcv ] ).Value <>
                     aDataSet.FieldByName( TField( Fields[ lcv ] ).FieldName ).Value ) then
                begin
                  SameKeyFields := False;
                end;
              end;
            finally
              FreeAndNil( Fields );
            end;

            { If we have the Same KeyFields we found our RecordView }
            if ( SameKeyFields ) then
            begin
              Result := aOpenFWRecordView;
            end;
          end;
        end; // With aOpenFWRecordView

        { If we found a RecordView with matching KeyFields, then there is no
          need to continue with the loop }
        if ( Assigned( Result ) ) then
        begin
          Break;
        end;
      end; // Loop over RecordViews
    end; // Check same datamodule type

    { If we found a RecordView with matching KeyFields, then there is no
      need to continue with the loop }
    If ( Assigned( Result ) ) then
    begin
      Break;
    end;
  end;  // loop over datamodules
end;

{*****************************************************************************
  This method will be used to register the given FrameWorkDataModule with the
  PPWController.

  @Name       TPPWFrameWorkController.RegisterDataModuleClass
  @author     slesage
  @param      ADataModuleClass - The Class of the FrameWorkDataModule that must
                                 be registered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.RegisterDataModuleClass( const
  ADataModuleClass: TPPWFrameWorkDataModuleClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'RegisterDataModuleCalss' );
  csFWRegister.SendString( 'aDataModuleClass.ClassName', aDataModuleClass.ClassName );
  {$ENDIF}

  if ( RegisteredDataModules.IndexOf( ADataModuleClass.ClassName ) ) = -1 then
  begin
    RegisteredDataModules.Add( ADataModuleClass.ClassName );
  end;

  if not ( csDesigning in ComponentState ) then
  begin
    Classes.RegisterClass( ADataModuleClass );
  end;

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'RegisterDataModuleCalss' );
  {$ENDIF}
end;

{*****************************************************************************
  This is actually a general method so we can Register all the different
  FrameWork classes using the Same Method.
  Internally it will call one of the other registration methods depending on
  the Type of Class that has been supplied.

  @Name       TPPWFrameWorkController.RegisterFrameWorkClass
  @author     slesage
  @param      AFrameWorkClass - The FrameWorkDataModuleClass that must be
                                registered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.RegisterFrameWorkClass(
  const AFrameWorkClass: TPPWFrameWorkDataModuleClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'RegisterFrameWorkClass' );
  csFWRegister.SendString( 'AFrameWorkClass.ClassName', AFrameWorkClass.ClassName );
  {$ENDIF}

  RegisterDataModuleClass( AFrameWorkClass );

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'RegisterFrameWorkClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This is actually a general method so we can Register all the different
  FrameWork classes using the Same Method.
  Internally it will call one of the other registration methods depending on
  the Type of Class that has been supplied.

  @Name       TPPWFrameWorkController.RegisterFrameWorkClass
  @author     slesage
  @param      AFrameWorkClass - The FrameWorkDataModuleClass that must be
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.RegisterFrameWorkClass(
  const AFrameWorkClass: TPPWFrameWorkListViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'RegisterFrameWorkClass' );
  csFWRegister.SendString( 'AFrameWorkClass.ClassName', AFrameWorkClass.ClassName );
  {$ENDIF}

  RegisterListViewClass( AFrameWorkClass );

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'RegisterFrameWorkClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This is actually a general method so we can Register all the different
  FrameWork classes using the Same Method.
  Internally it will call one of the other registration methods depending on
  the Type of Class that has been supplied.

  @Name       TPPWFrameWorkController.RegisterFrameWorkClass
  @author     slesage
  @param      AFrameWorkClass - The FrameWorkDataModuleClass that must be
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.RegisterFrameWorkClass(
  const AFrameWorkClass: TPPWFrameWorkRecordViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'RegisterFrameWorkClass' );
  csFWRegister.SendString( 'AFrameWorkClass.ClassName', AFrameWorkClass.ClassName );
  {$ENDIF}

  RegisterRecordViewClass( AFrameWorkClass );

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'RegisterFrameWorkClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to register the given FrameWorkListView with the
  PPWController.

  @Name       TPPWFrameWorkController.RegisterListViewClass
  @author     slesage
  @param      AListViewClass - The Class of the FrameWorkListView that must be
                               registered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.RegisterListViewClass(
  const AListViewClass: TPPWFrameWorkListViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'RegisterListViewClass' );
  csFWRegister.SendString( 'AListViewClass.ClassName', AListViewClass.ClassName );
  {$ENDIF}

  if ( RegisteredListViews.IndexOf( AListViewClass.ClassName ) ) = -1 then
  begin
    RegisteredListViews.Add( AListViewClass.ClassName );
  end;
  if not ( csDesigning in ComponentState ) then
  begin
    Classes.RegisterClass( AListViewClass );
  end;

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'RegisterListViewClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to register the given FrameWorkRecordView with the
  PPWController.

  @Name       TPPWFrameWorkController.RegisterRecordViewClass
  @author     slesage
  @param      ARecordViewClass - The Class of the FrameWorkRecordView that
                                 must be registered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.RegisterRecordViewClass(
  const ARecordViewClass: TPPWFrameWorkRecordViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'RegisterRecordViewClass' );
  csFWRegister.SendString( 'ARecordViewClass.ClassName', ARecordViewClass.ClassName );
  {$ENDIF}

  if ( RegisteredRecordViews.IndexOf( ARecordViewClass.ClassName ) ) = -1 then
  begin
    RegisteredRecordViews.Add( ARecordViewClass.ClassName );
  end;
  if not ( csDesigning in ComponentState ) then
  begin
    Classes.RegisterClass( ARecordViewClass );
  end;

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'RegisterRecordViewClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This function will be used to display a selection list for the specific DataModule.
  The user will be able to select one or more records, the selected records
  will be copied into the ClientDataSet that is passed to the function.

  @Name       TPPWFrameWorkController.SelectRecords
  @author     slesage
  @param      AFrameWorkDataModuleClassName - The ClassName of the DataModule
                                              that must be created.
  @param      AWhereClause                  - The WhereClause that should be
                                              used to filter the records that
                                              should be available for selection.
  @param      AClientDataSet                - The ClientDataSet in which the
                                              selected records will be copied
  @param      AMultiSelect                  - Boolean indicating if the user
                                              can select more than one record.
  @return     The result of this function will be the ModalResult of the
              Selection form.
  @Exception  None
  @See        None
 *****************************************************************************}


function TPPWFrameWorkController.SelectRecords(
  const AFrameWorkDataModuleClassName, AWhereClause: string;
  aClientDataSet: TClientDataSet;
  aMultiSelect : Boolean = False): TModalResult;
begin
  Result := SelectRecords(AFrameWorkDataModuleClassName, AWhereClause, aClientDataSet, nil, aMultiSelect);

end;

{*****************************************************************************
  This function will be used to display a selection list for the specific DataModule.
  The user will be able to select one or more records, the selected records
  will be copied into the ClientDataSet that is passed to the function.

  @Name       TPPWFrameWorkController.SelectRecords
  @author     slesage
  @param      AFrameWorkDataModuleClassName - The ClassName of the DataModule
                                              that must be created.
  @param      AWhereClause                  - The WhereClause that should be
                                              used to filter the records that
                                              should be available for selection.
  @param      AClientDataSet                - The ClientDataSet in which the
                                              selected records will be copied
  @param      AMultiSelect                  - Boolean indicating if the user
                                              can select more than one record.
  @return     The result of this function will be the ModalResult of the
              Selection form.
  @Exception  None
  @Modifications:
  Date        Author          Description
  07-SEP-2007 Ivdbossche      Added ASearchCriteria
  @See        None
 *****************************************************************************}

function TPPWFrameWorkController.SelectRecords(
  const AFrameWorkDataModuleClassName, AWhereClause : string;
  aClientDataSet, aSearchCriteria : TClientDataSet;
  aMultiSelect : Boolean = False) : TModalResult;
Var
  aCursorRestorer      : IPPWRestorer;
  aFrameWorkDataModule : IPPWFrameWorkDataModule;
  aFrameWorkListView   : IPPWFrameWorkListView;
  aDexExpressListView  : IPIFDevExpressListView;
  aFrameWorkDataModuleDefaultFilter: IPPWFrameWorkDataModuleDefaultFilter;

//  aDxDBGridListView    : IPPWFrameWorkdxDBGridListView;
//  BookMarkString       : String;
//  lcv                  : Integer;
begin
  { Set the Result to mrCancel by Default }
  Result := mrCancel;

  if ( Assigned( aClientDataSet ) ) then
  begin
    aCursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

    aFrameWorkDataModule := CreateDataModule( Nil, AFrameWorkDataModuleClassName, True );

    if ( Assigned( aFrameWorkDataModule ) ) then
    begin
      { Apply our Where Clause if necessary }
      if ( aWhereClause <> '' ) then
      begin
        aFrameWorkDataModule.DoFilterDetail( '', '', aWhereClause );
      end;

      aFrameWorkListView := CreateListViewForDataModule( aFrameWorkDataModule );

      aFrameWorkDataModule.OpenDataSets;

      // Did we provide initial search criteria?
      if ASearchCriteria <> nil then begin
        if ( Supports( aFrameWorkDataModule, IPPWFrameWorkDataModuleDefaultFilter, aFrameWorkDataModuleDefaultFilter)) then
        begin
          aFrameWorkDataModuleDefaultFilter.InitDefaultFilter(ASearchCriteria);
          aFrameWorkListView.ApplyFilter;
        end;
      end;

//      if ( Assigned( aFrameWorkDataModule.DataSet ) ) then
//      begin
//        aFrameWorkDataModule.DataSet.Open;
//      end;

      if ( Assigned( aFrameWorkListView ) ) then
      begin
        aFrameWorkListView.AsSelection := True;

        { For the DevExpress ListViews we have some Special Processing }
        if ( Supports( aFrameWorkListView, IPIFDevExpressListView, aDexExpressListView ) ) then
        begin
          aDexExpressListView.AllowMultiSelect := aMultiSelect;

          aFrameWorkListView.WindowState := wsNormal;
          aFrameWorkListView.Position := poScreenCenter;

          if ( aFrameWorkListView.ShowModal = mrOK ) then
          begin
            CloneStructur( aFrameWorkDataModule.DataSet, aClientDataSet );

            if ( aDexExpressListView.AllowMultiSelect ) then
            begin
              aDexExpressListView.CloneSelectedRecords( aFrameWorkDataModule.DataSet, aClientDataSet );
              Result := mrOK;
            end
            else
            begin
              CloneRecord  ( aFrameWorkDataModule.DataSet, aClientDataSet );
              Result := mrOK;
            end;
          end;
        end
        else
        { Other Grids currently only allow to select a single record }
        begin
          if ( aFrameWorkListView.ShowModal = mrOK ) then
          begin
            CloneStructur( aFrameWorkDataModule.DataSet, aClientDataSet );
            CloneRecord  ( aFrameWorkDataModule.DataSet, aClientDataSet );
            Result := mrOK;
          end;
        end;
      end;
    end;
  end;
end;

{*****************************************************************************
  This is the property setter for the FocusFont property.

  @Name       TPPWFrameWorkController.SetFocusFont
  @author     slesage
  @param      None
  @return     Value - The font that must be used.
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetFocusFont(const Value: TFont);
begin
  FFocusFont.Assign( Value );
end;

{*****************************************************************************
  This is the property setter for the ListViewStyle property.

  @Name       TPPWFrameWorkController.SetListViewStyle
  @author     slesage
  @param      Value - The new value for the ListViewStyle Property.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetListViewStyle(
  const Value: TFormStyle);
begin
  if ( Value <> FListViewStyle ) then
  begin
    FListViewStyle := Value;
  end;
end;

{*****************************************************************************
  This is the property setter for the ReadOnlyFont property.

  @Name       TPPWFrameWorkController.SetReadOnlyFont
  @author     slesage
  @param      Value - The font that must be used.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetReadOnlyFont(const Value: TFont);
begin
  FReadOnlyFont.Assign( Value );
end;

{*****************************************************************************
  This is the property setter for the RecordViewStyle property.

  @Name       TPPWFrameWorkController.SetRecordViewStyle
  @author     slesage
  @param      Value - The new value for the RecordViewStyle Property.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetRecordViewStyle(
  const Value: TFormStyle);
begin
  if ( Value <> FRecordViewStyle ) then
  begin
    FRecordViewStyle := Value;
  end;
end;

{*****************************************************************************
  This is the property setter for the RegisteredDataModules property.

  @Name       TPPWFrameWorkController.SetRegisteredDataModules
  @author     slesage
  @param      Value - The new value for the RegisteredDataModules Property.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetRegisteredDataModules(
  const Value: TStrings);
begin
  FRegisteredDataModules.Assign( Value );
end;

{*****************************************************************************
  This is the property setter for the RegisteredListViews property.

  @Name       TPPWFrameWorkController.SetRegisteredListViews
  @author     slesage
  @param      Value - The new value for the RegisteredListViews Property.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetRegisteredListViews(
  const Value: TStrings);
begin
  FRegisteredListViews.Assign( Value );
end;

{*****************************************************************************
  This is the property setter for the RegisteredRecordViews property.

  @Name       TPPWFrameWorkController.SetRegisteredRecordViews
  @author     slesage
  @param      Value - The new value for the RegisteredRecordViews Property.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetRegisteredRecordViews(
  const Value: TStrings);
begin
  FRegisteredRecordViews.Assign( Value );
end;

{*****************************************************************************
  This is the property setter for the RequiredFont property.

  @Name       TPPWFrameWorkController.SetRequiredFont
  @author     slesage
  @param      Value - The font that must be used.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.SetRequiredFont(const Value: TFont);
begin
  FRequiredFont.Assign( Value );
end;

{*****************************************************************************
  This method will be used to show the ListView for a specified DataModule.

  @Name       TPPWFrameWorkController.ShowListViewForDataModule
  @author     slesage
  @param      aFrameWorkDataModule - The Reference to the FrameWork DataModule
                                     for which you want to Show the ListView.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.ShowListViewForDataModule(
  const aFrameWorkDataModule: IPPWFrameWorkDataModule);
var
  CursorRestorer       : IPPWRestorer;
  aListView            : IPPWFrameWorkListView;
begin
  { Change the cursor to crHourGlass so the user knows the system is
    Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  if ( Assigned( aFrameWorkDataModule ) ) then
  begin
    if ( Assigned( aFrameWorkDataModule.ListView ) ) then
    begin
      aListView := aFrameWorkDataModule.ListView;
      if ( aListView.WindowState = wsMinimized ) then
      begin
        aListView.WindowState := wsNormal;
      end;
      aListView.Show;
    end
    else
    begin
      aListView := CreateListViewForDataModule( aFrameWorkDataModule );
      if ( Assigned( aListview ) ) then
      begin
        if ( aFrameWorkDataModule.FilterFirst ) then
        begin
          aFrameWorkDataModule.DoApplyFilter;
        end;
        aListView.FormStyle   := Self.ListViewStyle;
        aListView.WindowState := wsMaximized;
//        aListView.Show;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to show the RecordView for a specified DataModule
  in the specified RecordViewMode.

  @Name       TPPWFrameWorkController.ShowRecordViewForDataModule
  @author     slesage
  @param      aFrameWorkDataModule - The Reference to the FrameWork DataModule
                                     for which you want to Show the RecordView.
  @param      RecordViewMode       - The Mode in which the RecordView must be
                                     shown ( Edit, Add, View, Delete )
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.ShowRecordViewForDataModule(
  const aFrameWorkDataModule: IPPWFrameWorkDataModule;
  const RecordViewMode : TPPWFrameWorkRecordViewMode);
var
  aRecordDataModule    : IPPWFrameWorkDataModule;
  aCurrentKeyValues    : Variant;
  aCurrentKeyExpression: String;

  CursorRestorer       : IPPWRestorer;
  aRecordView          : IPPWFrameWorkRecordView;
begin
  { Change the cursor to crHourGlass so the user knows the system is
    Busy }
  CursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  if ( Assigned( aFrameWorkDataModule ) ) then
  begin
    { First check if the ReuseRecordView property is set.  If it is set
      to true we should check if we already have a recordview open for
      the current record. }
    if ( RecordViewMode <> rvmAdd ) and
       ( ReuseRecordView ) then
    begin
      aRecordView := RecordViewForCurrentRecord( aFrameWorkDataModule );

      if ( Assigned( aRecordView ) ) then
      begin
        if ( aRecordView.Mode <> RecordViewMode ) then
        begin
          aRecordView := Nil;
          Raise EPPWRecordViewAlreadyOpen.Create;
        end
        else
        begin
          if ( aRecordView.WindowState = wsMinimized ) then //<> aRecordView.FrameWorkDataModule.ListView.WindowState ) then
          begin
            aRecordView.WindowState := wsNormal; //aRecordView.FrameWorkDataModule.ListView.WindowState
          end
          else
          begin
            aRecordView.Show;
          end;
        end;
      end
    end;

    if not ( Assigned( aRecordView ) ) then
    begin
      aRecordDataModule := CreateDataModule( Nil, aFrameWorkDataModule.FrameWorkDatamoduleClassName, False );
      aRecordDataModule.AutoOpenDataSets := False;
      aRecordDataModule.ListViewDataModule := aFrameWorkDataModule;
      aFrameWorkDataModule.RecordViewDataModules.Add( aRecordDataModule );

      aCurrentKeyValues     := aFrameWorkDataModule.CreateFieldValuesVarArray( aFrameWorkDataModule.DataSet, aFrameWorkDataModule.KeyFields );
      aCurrentKeyExpression := aFrameWorkDataModule.BuildFilterExpressionForDetail( aFrameWorkDataModule, aRecordDataModule, aFrameWorkDataModule.KeyFields, False );

      aRecordDataModule.DoFilterRecord( aFrameWorkDataModule.KeyFields,
                                        aCurrentKeyValues, aCurrentKeyExpression );
//      end;

      if ( Assigned( aRecordDataModule ) ) then
      begin
        Try
          aRecordView := CreateRecordViewForDataModule( aRecordDataModule, RecordViewMode );

          if ( Assigned( aRecordView ) ) then
          begin
            if not (aRecordView.SettingsSaved) then
            begin
              if not ( fsShowing in aRecordView.Form.FormState ) and
                 not ( fsCreatedMDIChild in aRecordView.Form.FormState ) and
                 ( aRecordView.WindowState = wsNormal ) then
              begin
                aRecordView.Height := aRecordView.MinHeight + 1;
              end;
            end;

            aRecordView.FormStyle := RecordViewStyle;
    //        aRecordView.Show;
    //        aRecordView.DoUpdateControls;
    //        aFrameWorkDataModule.RecordViews.Add( aRecordView );
          end;
        Except
          Raise;
        end;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to UnRegister the given FrameWorkDataModule with the
  PPWController.

  @Name       TPPWFrameWorkController.UnRegisterDataModuleClass
  @author     slesage
  @param      ADataModuleClass - The Class of the FrameWorkDataModule that must
                                 be Unregistered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.UnRegisterDataModuleClass( const
  ADataModuleClass: TPPWFrameWorkDataModuleClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'UnRegisterDataModuleClass' );
  csFWRegister.SendString( 'aDataModuleClass.ClassName', aDataModuleClass.ClassName );
  {$ENDIF}

  if ( RegisteredDataModules.IndexOf( ADataModuleClass.ClassName ) ) <> -1 then
  begin
    RegisteredDataModules.Delete( RegisteredDataModules.IndexOf( ADataModuleClass.ClassName ) );
  end;
  if not ( csDesigning in ComponentState ) then
  begin
    Classes.UnRegisterClass( ADataModuleClass );
  end;

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'UnRegisterDataModuleClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This is actually a general method so we can UnRegister all the different
  FrameWork classes using the Same Method.
  Internally it will call one of the other unregistration methods depending on
  the Type of Class that has been supplied.

  @Name       TPPWFrameWorkController.UnRegisterFrameWorkClass
  @author     slesage
  @param      AFrameWorkClass - The FrameWorkDataModuleClass that must be
                                UnRegistered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}

procedure TPPWFrameWorkController.UnRegisterFrameWorkClass(
  const AFrameWorkClass: TPPWFrameWorkDataModuleClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'UnRegisterFrameWorkClass' );
  csFWRegister.SendString( 'AFrameWorkClass.ClassName', AFrameWorkClass.ClassName );
  {$ENDIF}

  UnRegisterDataModuleClass( AFrameWorkClass );

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'UnRegisterFrameWorkClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This is actually a general method so we can UnRegister all the different
  FrameWork classes using the Same Method.
  Internally it will call one of the other unregistration methods depending on
  the Type of Class that has been supplied.

  @Name       TPPWFrameWorkController.UnRegisterFrameWorkClass
  @author     slesage
  @param      AFrameWorkClass - The FrameWorkDataModuleClass that must be
                                UnRegistered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}

procedure TPPWFrameWorkController.UnRegisterFrameWorkClass(
  const AFrameWorkClass: TPPWFrameWorkListViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'UnRegisterFrameWorkClass' );
  csFWRegister.SendString( 'AFrameWorkClass.ClassName', AFrameWorkClass.ClassName );
  {$ENDIF}

  UnRegisterListViewClass( AFrameWorkClass );

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'UnRegisterFrameWorkClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This is actually a general method so we can UnRegister all the different
  FrameWork classes using the Same Method.
  Internally it will call one of the other unregistration methods depending on
  the Type of Class that has been supplied.

  @Name       TPPWFrameWorkController.UnRegisterFrameWorkClass
  @author     slesage
  @param      AFrameWorkClass - The FrameWorkDataModuleClass that must be
                                UnRegistered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}

procedure TPPWFrameWorkController.UnRegisterFrameWorkClass(
  const AFrameWorkClass: TPPWFrameWorkRecordViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'UnRegisterFrameWorkClass' );
  csFWRegister.SendString( 'AFrameWorkClass.ClassName', AFrameWorkClass.ClassName );
  {$ENDIF}

  UnRegisterRecordViewClass( AFrameWorkClass );

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'UnRegisterFrameWorkClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to UnRegister the given FrameWorkListView with the
  PPWController.

  @Name       TPPWFrameWorkController.UnRegisterListViewClass
  @author     slesage
  @param      AListViewClass - The Class of the FrameWorkListView that must be
                               UnRegistered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}

procedure TPPWFrameWorkController.UnRegisterListViewClass(
  const AListViewClass: TPPWFrameWorkListViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'UnRegisterListViewClass' );
  csFWRegister.SendString( 'AListViewClass.ClassName', AListViewClass.ClassName );
  {$ENDIF}

  if ( RegisteredListViews.IndexOf( AListViewClass.ClassName ) ) <> -1 then
  begin
    RegisteredListViews.Delete( RegisteredListViews.IndexOf( AListViewClass.ClassName ) );
  end;
  if not ( csDesigning in ComponentState ) then
  begin
    Classes.UnRegisterClass( AListViewClass );
  end;

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'UnRegisterListViewClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to UnRegister the given FrameWorkRecordView with the
  PPWController.

  @Name       TPPWFrameWorkController.UnRegisterRecordViewClass
  @author     slesage
  @param      ARecordViewClass - The Class of the FrameWorkRecordView that must
                                 be UnRegistered with the Controller.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.UnRegisterRecordViewClass(
  const ARecordViewClass: TPPWFrameWorkRecordViewClass);
begin
  {$IFDEF CODESITE}
  csFWRegister.EnterMethod( Self, 'UnRegisterRecordViewClass' );
  csFWRegister.SendString( 'ARecordViewClass.ClassName', ARecordViewClass.ClassName );
  {$ENDIF}

  if ( RegisteredRecordViews.IndexOf( ARecordViewClass.ClassName ) ) <> -1 then
  begin
    RegisteredRecordViews.Delete( RegisteredRecordViews.IndexOf( ARecordViewClass.ClassName ) );

  end;
  if not ( csDesigning in ComponentState ) then
  begin
    Classes.UnRegisterClass( ARecordViewClass );
  end;

  {$IFDEF CODESITE}
  csFWRegister.ExitMethod( Self, 'UnRegisterRecordViewClass' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be executed when we change the Active Page in the Details
  PageControl.  It will be used to activate the ListView in the Active TabSheet
  if a ListView can be found.

  @Name       TPPWFrameWorkController.DoPageChanged
  @author     slesage
  @param      Sender - The Object from which the Method is invoked.
  @return     None
  @Exception  None
  @See        None
 *****************************************************************************}


procedure TPPWFrameWorkController.DoPageChanged(Sender: TObject);
var
  aParentForm : TCustomForm;
  aRecordView : IPPWFrameWorkRecordView;
  lcv         : Integer;
begin
  { If Our Sender was a PageControl, try to get it's Parent Form }
  if ( Sender is TPageControl ) then
  begin
    aParentForm := GetParentForm( TPageControl( Sender ) );

    { If the ParentForm was a FrameWorkRecordView, then we can check if
      if have a FrameWorkListView docked in the TabSheet }
    if ( Supports( aParentForm, IPPWFrameWorkRecordView, aRecordView ) ) then
    begin
      for lcv := 0 to Pred( TPageControl( Sender ).ActivePage.ControlCount ) do
      begin
        { If we do have a FrameWorkListView docked in the TabSheet, we can
          set the ActiveControl of the RecordViewForm to the Docked
          ListViewForm }
        if ( Supports( TPageControl( Sender ).ActivePage.Controls[ lcv ] , IPPWFrameWorkListView ) ) then
        begin
          aRecordView.Form.ActiveControl := TPPWFrameWorkListView( TPageControl( Sender ).ActivePage.Controls[ lcv ] );
        end;
      end;
    end;
  end;
end;

procedure TPPWFrameWorkController.CreateLinkAndShowDetailListView(
  const aMasterDataModule: IPPWFrameWorkDataModule;
  const aDetailDataModuleClassName: String;
  const aRecordView                : IPPWFrameWorkRecordView);
var
  aDetail           : TPPWFrameWorkDataModuleDetail;
  CanCreate         : Boolean;
  aDetailDataModule : IPPWFrameWorkDataModule;
  aForeignKeys      : String;
  aExpression       : String;
  aFieldValues      : Variant;
begin
  if ( Assigned( aMasterDataModule ) ) then
  begin
    aDetail := aMasterDataModule.DetailDataModules.ItemByDetailClassName( aDetailDataModuleClassName );

    if ( Assigned( aDetail ) ) then
    begin
      CanCreate := True;

      if ( Assigned( aMasterDataModule.OnBeforeCreateDetailDataModule ) ) then
      begin
        aMasterDataModule.OnBeforeCreateDetailDataModule( Self, aDetail.ClassName, CanCreate );
      end;

      if ( CanCreate ) then
      begin
        aDetailDataModule := CreateDetailDataModule( aMasterDataModule,
                                                     aDetail.DataModuleClass,
                                                     aRecordView );
        aForeignKeys := aDetail.ForeignKeys;
        aDetailDataModule.ForeignKeys := aForeignKeys;

        if ( Assigned( aMasterDataModule.OnAfterCreateDetailDataModule ) ) then
        begin
          aMasterDataModule.OnAfterCreateDetailDataModule( Self, aDetail.DataModuleClass, aDetailDataModule );
        end;

        if ( Assigned( aDetailDataModule ) ) then
        begin
          aExpression := aDetailDataModule.BuildFilterExpressionForDetail( aMasterDataModule,
                                                                           aDetailDataModule,
                                                                           aForeignKeys,
                                                                           True );
          aDetailDataModule.DoFilterDetail( aForeignKeys, aFieldValues, aExpression );
          ShowListViewForDataModule( aDetailDataModule );
        end;
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to Create a DetailListView and return its Interface.

  @Name       TPPWFrameWorkController.CreateDetailListView
  @author     slesage
  @param      aMasterDataModule            The DataModule that is the Master in
                                           the Master / Detail relation.
  @param      aDetailDataModuleClassName   The ClassName of the DataModule that
                                           will be the Detail in the Master /
                                           Detail relation.
  @return     Returns an interface to the Newly created Detail ListView.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkController.CreateDetailListView(aMasterDataModule : IPPWFrameWorkDataModule;
  aDetailDataModuleClassName : string; aRecordView : IPPWFrameWorkRecordView = Nil ): IPPWFrameWorkListView;
var
  aDetailDataModule : IPPWFrameWorkDataModule;
  aDetail           : TPPWFrameWorkDataModuleDetail;
  CanCreate         : Boolean;
  aForeignKeys      : String;
  aExpression       : String;
  aFieldValues      : Variant;
begin
  if ( Assigned( aMasterDataModule ) ) then
  begin
    aDetail := aMasterDataModule.DetailDataModules.ItemByDetailClassName( aDetailDataModuleClassName );

    if ( Assigned( aDetail ) ) then
    begin
      CanCreate := True;

      if ( Assigned( aMasterDataModule.OnBeforeCreateDetailDataModule ) ) then
      begin
        aMasterDataModule.OnBeforeCreateDetailDataModule( Self, aDetail.ClassName, CanCreate );
      end;

      if ( CanCreate ) then
      begin
        aDetailDataModule := CreateDetailDataModule( aMasterDataModule,
                                                     aDetail.DataModuleClass,
                                                     aRecordView );
        aForeignKeys := aDetail.ForeignKeys;
        aDetailDataModule.ForeignKeys := aForeignKeys;

        if ( Assigned( aMasterDataModule.OnAfterCreateDetailDataModule ) ) then
        begin
          aMasterDataModule.OnAfterCreateDetailDataModule( Self, aDetail.DataModuleClass, aDetailDataModule );
        end;

        if ( Assigned( aDetailDataModule ) ) then
        begin
          aFieldValues := aDetailDataModule.CreateFieldValuesVarArray( aMasterDataModule.RecordDataSet, aForeignKeys );
          aExpression  := aDetailDataModule.BuildFilterExpressionForDetail( aMasterDataModule,
                                                                           aDetailDataModule,
                                                                           aForeignKeys,
                                                                           True );
          aDetailDataModule.DoFilterDetail( aForeignKeys, aFieldValues, aExpression );


            if ( Assigned( aDetailDataModule.ListView ) ) then
            begin
              Result := aDetailDataModule.ListView;
            end
            else
            begin
              Result := CreateListViewForDataModule( aDetailDataModule, True, aRecordView );
              if ( Assigned( Result ) ) then
              begin
                if ( aDetailDataModule.FilterFirst ) then
                begin
                  aDetailDataModule.DoApplyFilter;
                end;
              end;
            end;

        end;
      end;
    end
    else
    begin
      raise EPPWUnregisteredDataModuleError.CreateFmt(
          rsPPWFrameWorkUnRegisteredDetailDataModule, [ aDetailDataModuleClassName, aMasterDataModule.FrameWorkDatamoduleClassName ] );
    end;
  end;
end;

{*****************************************************************************
  This method will be used to show a RecordView for a specific record without
  the need to pass through a List FrameWorkDataModule.

  @Name       TPPWFrameWorkController.ShowRecord
  @author     slesage
  @param      AFrameWorkDataModuleClassName  The ClassName of the DataModule
                                             that should be created.
              AWhereClause                   The Where Clause that should be
                                             applied to the Record DataModules
                                             RecordDataSet.
              aRecordViewMode
              aWithAutoOpen                  Boolean flag indicating if the
                                             RecordDataSet should be opened.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkController.ShowRecord(
  const AFrameWorkDataModuleClassName, AWhereClause: string;
  const aRecordViewMode: TPPWFrameWorkRecordViewMode;
  aWithAutoOpen: Boolean);
Var
  aFrameWorkDataModule : IPPWFrameWorkDataModule;
  aFrameWorkRecordView : IPPWFrameWorkRecordView;
begin
  { Create the RecordViewDataModule }
  aFrameWorkDataModule := CreateRecordDataModule( aFrameWorkDataModuleClassName, aWhereClause, aRecordViewMode );

  if ( Assigned( aFrameWorkDataModule ) ) then
  begin
    aFrameWorkRecordView := CreateRecordViewForDataModule( aFrameWorkDataModule, aRecordViewMode );

    if ( Assigned( aFrameWorkRecordView ) ) then
    begin
      aFrameWorkRecordView.FormStyle := RecordViewStyle;
    end;
  end;
end;

{*****************************************************************************
  This function will be used to create a Record FrameWorkDataModule without
  creating a List FrameWorkDataModule first.

  @Name       TPPWFrameWorkController.CreateRecordDataModule
  @author     slesage
  @param      AFrameWorkDataModuleClassName  The ClassName of the DataModule
                                             that should be created.
              AWhereClause                   The Where Clause that should be
                                             applied to the Record DataModules
                                             RecordDataSet.
              aWithAutoOpen                  Boolean flag indicating if the
                                             RecordDataSet should be opened.
  @return     Returns an Interface to the Record FrameWorkDataModule.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkController.CreateRecordDataModule(
  const AFrameWorkDataModuleClassName, AWhereClause: string;
  const RecordViewMode: TPPWFrameWorkRecordViewMode): IPPWFrameWorkDataModule;
begin
  { Create the DataModule }
  Result := CreateDataModule( Nil, AFrameWorkDataModuleClassName, False );

  if ( Assigned( Result ) ) then
  begin
    { Apply our Where Clause if necessary }
    if ( aWhereClause <> '' ) then
    begin
      Result.DoFilterRecord( '', '', aWhereClause );
    end;

    { Open the RecordDataSet just in case the DoFilterRecord only modifies the
      SQL and doesn't open the DataSet itself }
    if ( Assigned( Result.RecordDataset ) ) then
    begin
      Result.RecordDataSet.Open;

      if ( RecordViewMode <> rvmAdd ) then
      begin
        { Raise an Exception if the RecordDataSet has more than one record, or
          no records at all }
        if ( Result.RecordDataset.RecordCount <> 1 ) then
        begin
          Raise EPPWFrameWorkError.Create( 'The RecordDataSet of the FrameWorkDataModule doens''t contain one single record !' );
        end;
      end;
    end;
  end;
end;

end.
