{*****************************************************************************
  This unit will contain the CustomModule descendants for the PPWFrameWork.

  @Name       Unit_PPWFrameWorkCustomModules
  @Author     slesage
  @Copyright  (c) 2004 USB
  @History

  Date         By                   Description
  ----         --                   -----------
  09/12/2004   sLesage              Added functionality to set the KeyFields
                                    using the Wizard.
  18/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_PPWFrameWorkCustomModules;

interface

uses
  DesignEditors, DesignMenus, Classes, DB, Controls,
  Form_PPWFrameWorkAddDataAwareControlsWizard, DesignIntf;

type
  { Custom PPWFrameWork RecordView Module which adds functionality to
    create the DB Aware Controls for the RecordView }
  TPPWFrameWorkRecordViewModule = class( TCustomModule )
  protected
    procedure CreateDBAwareComponents( aParent : TComponent; aDataSource : TDataSource; aFields : TFields; aFieldDefs : TFieldDefs ); virtual;
    function DefaultWizardClass : TDBAwareControlWizardClass; virtual;
    function DefaultLabelClass  : TComponentClass; virtual;
    function  MaxFieldCaptionLength  ( aFields : TFields ) : Integer; virtual;
  protected
    function GetSelectedComponents : IDesignerSelections;
    function GetSelectedControl    : TControl;

    property SelectedControl    : TControl            read GetSelectedControl;
    property SelectedComponents : IDesignerSelections read GetSelectedComponents;
  public
    procedure PIFAddDBAwareComponentsWizard( aParent : TControl ); virtual;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

  TPPWFrameWorkDataModuleModule = class( TCustomModule )
  protected
    procedure CreatePersistentFields( aDataSet : TDataSet ); virtual;
    procedure InitialiseSQLAndPersistentFields; virtual;
  public
    procedure PIFAddPersistentFieldsWizard; virtual;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects, csIntf,
  {$ENDIF}
  SysUtils, DSDesign, cxDBEdit, TypInfo, cxDropDownEdit, StdCtrls, Graphics,
  Unit_PPWFrameWorkInterfaces, Form_PPWFrameWorkPersistentFieldsWizard,
  Unit_PPWFrameWorkRecordView, ConvUtils;

{ TPPWFrameWorkRecordViewModule }

procedure TPPWFrameWorkRecordViewModule.CreateDBAwareComponents(
  aParent : TComponent; aDataSource : TDataSource; aFields : TFields; aFieldDefs : TFieldDefs );
var
  lcv : Integer;
  aLabel : TControl;
  aEdit  : TWinControl;

  aDataBinding : TcxDBEditDataBinding;

  aTop   , aLeft  : Integer;
  aWidth          : Integer;
  aMaxCaptionWidth: Integer;
  aDBLeft         : Integer;
  aRecordView     : IPPWFrameWorkRecordView;
  aDBAwareClass   : TComponentClass;
  aDBAwareVisible : Boolean;
  aWizardForm     : TfrmPPWFrameWorkAddDataAwareControlsWizard;
begin
  { First make sure the procedure was triggered on a FrameWorkRecordView }
  if ( Supports( Root, IPPWFrameWorkRecordView, aRecordView ) ) then
  begin
    { Now Create and Show the wizard so the user can specify all the options }
    aWizardForm := DefaultWizardClass.Create( Nil );
    try
      aWizardForm.RecordDataSet := aRecordView.DataSource.DataSet;
      aWizardForm.InitialiseSettings;

      { If the user closed the Wizard  using the OK button, we can continue the
        process }
      if ( aWizardForm.ShowModal = mrOK ) then
      begin
        { By default the label components should start at 8,8 in the Parent Container }
        aTop  := 8;
        aLeft := 8;
        aWidth:= 121;
        aMaxCaptionWidth := MaxFieldCaptionLength( aFields );

        { Now set the intial Left Position for our DBAware controls according
          to the MaxCaptionWidth }
        aDBLeft := 24 + ( ( ( aMaxCaptionWidth div 8 ) + 1 ) * 8 );

        { Loop over all fields to create the Label and DBAwareComponent }
        for lcv := 0 to Pred( aFields.Count ) do
        begin
          { Get some settings from the Wizard form }
          aDBAwareClass := aWizardForm.GetDBAwareComponentClass( aFields[ lcv ] );
          aDBAwareVisible := aWizardForm.GetDBAwareComponentVisible( aFields[ lcv ] );

          { Only create the components if the user indicated he wants to see them }
          if aDBAwareVisible then
          begin
            { Now create the Label and the DBAware Control }
            aLabel := TControl   ( Designer.CreateComponent( DefaultLabelClass, aParent, aLeft , aTop, aMaxCaptionWidth, 17 ) );
            aEdit  := TWinControl( Designer.CreateComponent( aDBAwareClass, aParent, aDBLeft, aTop, aWidth, 21 ) );

            { Now Set the Label Properties }
            aLabel.Name        := Designer.UniqueName( 'cxlbl' + aFields[ lcv ].FieldName );
            aLabel.HelpType    := htKeyWord;
            aLabel.HelpKeyword := Root.Name + '.' + aFields[ lcv ].FieldName;

            { Set the additional properties using RTTI }
            if ( IsPublishedProp( aLabel, 'FocusControl' ) ) then
            begin
              SetObjectProp( aLabel, 'FocusControl', aEdit );
            end;
            if ( IsPublishedProp( aLabel, 'Caption' ) ) then
            begin
              SetStrProp( aLabel, 'Caption', aFields[ lcv ].DisplayLabel );
            end;

            { Now set the Edit Properites }

            aEdit.Name        := Designer.UniqueName( {'cxlbl' +} aFields[ lcv ].FieldName );
            aEdit.HelpType    := htKeyWord;
            aEdit.HelpKeyword := Root.Name + '.' + aFields[ lcv ].FieldName;

            { Set the additional properties using RTTI }
            if ( IsPublishedProp( aEdit, 'DataBinding' ) ) then
            begin
              aDataBinding := TcxDBEditDataBinding( GetObjectProp( aEdit, 'DataBinding' ) );
              SetObjectProp( aDataBinding, 'DataSource', aDataSource );
              SetStrProp   ( aDataBinding, 'DataField' , aFields[ lcv ].FieldName );
            end;

            if ( aEdit is TcxCustomDropDownEdit ) then
            begin
              aEdit.Width := aWidth + 16;
            end;

            { Now increment the Top position for the next control }
            inc( aTop, ( ( ( aEdit.Height div 8 ) + 1 ) * 8 ) );
          end;
        end;
      end;
    finally
      FreeAndNil( aWizardForm );
    end;
  end;
end;

function TPPWFrameWorkRecordViewModule.DefaultLabelClass: TComponentClass;
begin
  Result := TLabel;
end;

function TPPWFrameWorkRecordViewModule.DefaultWizardClass: TDBAwareControlWizardClass;
begin
  Result := TfrmPPWFrameWorkAddDataAwareControlsWizard;
end;

procedure TPPWFrameWorkRecordViewModule.ExecuteVerb(Index: Integer);
var
  aSelections : IDesignerSelections;
  lcv         : Integer;
begin
  aSelections := TDesignerSelections.Create;
  Designer.GetSelections( aSelections );
  for lcv := 0 to Pred( aSelections.Count ) do
  begin
    {$IFDEF CODESITE}
    CodeSite.SendObject( 'aSelection.Items[ lcv ]', aSelections.Items[ lcv ] );
    {$ENDIF}
  end;

  Case Index of
    0 : PIFAddDBAwareComponentsWizard( SelectedControl );
    else Inherited ExecuteVerb( Index );
  end;
end;

{*****************************************************************************
  This function will be used to return a list of selected components on the
  current designer.

  @Name       TPPWFrameWorkRecordViewModule.GetSelectedComponents
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkRecordViewModule.GetSelectedComponents: IDesignerSelections;
begin
  Result := TDesignerSelections.Create;
  Designer.GetSelections( Result );
end;

function TPPWFrameWorkRecordViewModule.GetSelectedControl: TControl;
var
  lcv : Integer;
begin
  Result := Nil;
  if ( Assigned( SelectedComponents ) ) then
  begin
    if ( SelectedComponents.Count <> 0 ) then
    begin
      for lcv := 0 to Pred( SelectedComponents.Count ) do
      begin
        if ( SelectedComponents.Items[ lcv ] is TControl ) then
        begin
          Result := TControl( SelectedComponents.Items[ lcv ] );
          Break;
        end;
      end;
    end;
  end;
end;

function TPPWFrameWorkRecordViewModule.GetVerb(Index: Integer): string;
begin
  Case Index of
    0 : Result := 'PIF.AddDataAwareComponents';
  end;
end;

function TPPWFrameWorkRecordViewModule.GetVerbCount: Integer;
begin
  Result := 1;
end;

{*****************************************************************************
  This function will determine the length of the Longest field's caption.

  @Name       TPPWFrameWorkRecordViewModule.MaxFieldCaptionLength
  @author     slesage
  @param      None
  @return     Returns the length of the longest field's catpion.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkRecordViewModule.MaxFieldCaptionLength(
  aFields: TFields): Integer;
var
  aMaxCaptionWidth : Integer;
  aCanvas          : TCanvas;
  lcv              : Integer;
  aCaption         : String;
begin
  aMaxCaptionWidth := 0;

  { First Determine how long the largest caption will be }
  aCanvas := TPPWFrameWorkRecordView( Root ).Canvas;

  { Loop over each field to dertermin which caption is the longest one }
  for lcv := 0 to Pred( aFields.Count ) do
  begin
    if ( aFields[ lcv ].DisplayLabel <> '' ) then
    begin
      aCaption := aFields[ lcv ].DisplayLabel;
    end
    else
    begin
      aCaption := aFields[ lcv ].FieldName;
    end;
    if ( aCanvas.TextWidth( aCaption ) >
         aMaxCaptionWidth ) then
    begin
      aMaxCaptionWidth := aCanvas.TextWidth( aCaption );
    end;
  end;

  { Return the Length of the Longest Caption }
  Result := aMaxCaptionWidth;
end;

procedure TPPWFrameWorkRecordViewModule.PIFAddDBAwareComponentsWizard( aParent : TControl );
var
  aRecordView : IPPWFrameWorkRecordView;
  aDataSource : TDataSource;
begin
  {$IFDEF CODESITE}
  csFWRecordView.EnterMethod( Self, 'PIFAddDBAwareComponentsWizard' );
  {$ENDIF}

  if ( Supports( Root, IPPWFrameWorkRecordView, aRecordView ) ) then
  begin
    {$IFDEF CODESITE}
    csFWRecordView.SendMsg( csmInfo, 'Root supports IPPWFrameWorkRecordView' );
    {$ENDIF}

    aDataSource := TDataSource( Designer.GetComponent( 'srcMain' ) );

    if ( Assigned( aDataSource ) ) and
       ( Assigned( aDataSource.DataSet ) ) then
    begin
      {$IFDEF CODESITE}
      csFWRecordView.SendMsg( csmInfo, 'aRecordView.DataSource Assigned' );
      csFWRecordView.SendMsg( csmInfo, 'aRecordView.DataSource.DataSet Assigned' );
      {$ENDIF}

      CreateDBAwareComponents( aParent, aDataSource, aDataSource.DataSet.Fields, aDataSource.DataSet.FieldDefs );
    end;
  end;

  {$IFDEF CODESITE}
  csFWRecordView.ExitMethod( Self, 'PIFAddDBAwareComponentsWizard' );
  {$ENDIF}
end;

{ TPPWFrameWorkDataModuleModule }

procedure TPPWFrameWorkDataModuleModule.InitialiseSQLAndPersistentFields;
var
  aDataModule    : IPPWFrameWorkDataModule;
  aRecordDataSet : TDataSet;
  aListDataSet   : TDataSet;
  aDataSet       : TDataSet;
  lcv            : Integer;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'InitialiseSQLAndPersistentFields' );
  {$ENDIF}

  aDataSet := TDataSet( Root.FindComponent( 'qryList' ) );
  CreatePersistentFields( aDataSet );

  aDataSet := TDataSet( Root.FindComponent( 'qryRecord' ) );
  CreatePersistentFields( aDataSet );

  aListDataSet := TDataSet( Root.FindComponent( 'cdsList' ) );
  CreatePersistentFields( aListDataSet );

  aRecordDataSet := TDataSet( Root.FindComponent( 'cdsRecord' ) );
  CreatePersistentFields( aRecordDataSet );

  frmPPWFrameWorkPersistentFieldsWizard := TfrmPPWFrameWorkPersistentFieldsWizard.Create( Nil );
  Try
    frmPPWFrameWorkPersistentFieldsWizard.ListDataSet   := aListDataSet;
    frmPPWFrameWorkPersistentFieldsWizard.RecordDataSet := aRecordDataSet;

    if ( frmPPWFrameWorkPersistentFieldsWizard.ShowModal = mrOK ) then
    begin
      { Set some Properties on the Persistent fields of the List DataSet }
      for lcv := 0 to Pred( aListDataSet.Fields.Count ) do
      begin
        if ( frmPPWFrameWorkPersistentFieldsWizard.cdsListFieldSettings.Locate( 'F_FIELD_NAME', aListDataSet.Fields[ lcv ].FieldName, [ loCaseInsensitive ] ) ) then
        begin
          aListDataSet.Fields[ lcv ].DisplayLabel := frmPPWFrameWorkPersistentFieldsWizard.cdsListFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString;
          aListDataSet.Fields[ lcv ].Visible      := frmPPWFrameWorkPersistentFieldsWizard.cdsListFieldSettings.FieldByName( 'F_FIELD_VISIBLE' ).AsBoolean;
        end;
      end;
      { Set some Properties on the Persistent fields of the List DataSet }
      for lcv := 0 to Pred( aRecordDataSet.Fields.Count ) do
      begin
        if ( frmPPWFrameWorkPersistentFieldsWizard.cdsRecordFieldSettings.Locate( 'F_FIELD_NAME', aRecordDataSet.Fields[ lcv ].FieldName, [ loCaseInsensitive ] ) ) then
        begin
          aRecordDataSet.Fields[ lcv ].DisplayLabel := frmPPWFrameWorkPersistentFieldsWizard.cdsRecordFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString;
          aRecordDataSet.Fields[ lcv ].Visible      := frmPPWFrameWorkPersistentFieldsWizard.cdsRecordFieldSettings.FieldByName( 'F_FIELD_VISIBLE' ).AsBoolean;
          aRecordDataSet.Fields[ lcv ].ReadOnly     := frmPPWFrameWorkPersistentFieldsWizard.cdsRecordFieldSettings.FieldByName( 'F_FIELD_READONLY' ).AsBoolean;
          aRecordDataSet.Fields[ lcv ].Required     := frmPPWFrameWorkPersistentFieldsWizard.cdsRecordFieldSettings.FieldByName( 'F_FIELD_REQUIRED' ).AsBoolean;
        end;
      end;

      { Try to get the KeyFields and initialise that property on the DataModule }
      if ( Supports( Root, IPPWFrameWorkDataModule, aDataModule ) ) then
      begin
        aDataModule.KeyFields := frmPPWFrameWorkPersistentFieldsWizard.KeyFields;
      end
      else
      begin
        {$IFDEF CODESITE}
        CodeSite.SendMsg( csmInfo, 'Root does not support IPPWFrameWorkDataModule' );
        {$ENDIF}

      end;
    end;
  Finally
    FreeAndNil( frmPPWFrameWorkPersistentFieldsWizard );
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'InitialiseSQLAndPersistentFields' );
  {$ENDIF}
end;

procedure TPPWFrameWorkDataModuleModule.ExecuteVerb(Index: Integer);
begin
  Case Index of
    0 : PIFAddPersistentFieldsWizard;
    else Inherited ExecuteVerb( Index );
  end;
end;

function TPPWFrameWorkDataModuleModule.GetVerb(Index: Integer): string;
begin
  Case Index of
    0 : Result := 'PIF.AddPersistentFields';
  end;
end;

function TPPWFrameWorkDataModuleModule.GetVerbCount: Integer;
begin
  Result := 1;
end;

procedure TPPWFrameWorkDataModuleModule.PIFAddPersistentFieldsWizard;
var
  aDataModule : IPPWFrameWorkDataModule;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'PIFAddPersistentFieldsWizard' );
  {$ENDIF}

  if ( Supports( Root, IPPWFrameWorkDataModule, aDataModule ) ) then
  begin
    {$IFDEF CODESITE}
    csFWDataModule.SendMsg( csmInfo, 'Root supports IPPWFrameWorkDataModule' );
    {$ENDIF}

    InitialiseSQLAndPersistentFields;
  end;

  {$IFDEF CODESITE}
   csFWDataModule.ExitMethod( Self, 'PIFAddPersistentFieldsWizard' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to open the FieldsEditor and add all persistent
  fields for the given aDataSet.

  @Name       TPPWFrameWorkDataModuleModule.CreatePersistentFields
  @author     slesage
  @param      aDataSet  The DataSet for which you want to add the Persistent
                        Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkDataModuleModule.CreatePersistentFields(
  aDataSet: TDataSet);
begin
  {$IFDEF CODESITE}
   csFWDataModule.EnterMethod( Self, 'CreatePersistentFields' );
  {$ENDIF}

  if ( Assigned( aDataSet ) ) then
  begin
    Designer.SelectComponent( aDataSet );
    Designer.Edit( aDataSet );
    TDSDesigner( aDataSet.Designer ).FieldsEditor.DoAddFields( True );
    TDSDesigner( aDataSet.Designer ).FieldsEditor.Hide;
  end;

  {$IFDEF CODESITE}
   csFWDataModule.ExitMethod( Self, 'CreatePersistentFields' );
  {$ENDIF}
end;

end.
