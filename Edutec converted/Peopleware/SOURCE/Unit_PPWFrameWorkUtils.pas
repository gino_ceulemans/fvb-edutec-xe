{*****************************************************************************
  Name           : Unit_PPWFrameWorkUtils
  Author         : Lesage Stefaan
  Copyright      : (c) 2002 Peopleware
  Description    : This unit will contain some Utility functions which are
                   used in our framework.
  History        :

  Date         By                   Description
  ----         --                   -----------
  16/09/2003   sLesage              Updated the CloneRecord method to work
                                    around a problem with the TLargeintField,
                                    apparently that TLargeintField doesn't
                                    allow you to change the Value property.
  12/02/2003   wlambrec             Modification CloneStructur
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkUtils;

interface

uses
  DBClient, DB, TypInfo;

procedure CloneStructur( aSource, aDest : TClientDataSet );
procedure CloneRecord( aSource, aDest : TClientDataSet );
procedure CopyFieldProperties( aSource, aDest : TField );
procedure CopyFieldClassProperties( aSource, aDest : TField );

function CloneAutoIncField( const Sourcefield : TField; aDest : TClientDataSet ) : TField;
function CloneField( const SourceField : TField; aDest : TClientDataSet ) : TField;

function IsReadOnlyProperty( info : PPropInfo ) : Boolean;

implementation

uses
  SysUtils, Dialogs;

{*****************************************************************************
  Name           : CloneStructur
  Author         : Stefaan Lesage
  Arguments      : aSource - The DataSet from which you want to copy the
                             structure.
                   aDest   - The DataSet to which the structure must be
                             copied.          
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to copy the structure from the
                   source dataset to the destination dataset.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  12/02/2003   wlambrec             Datasetfields no longer cloned, because
                                    these can not be cloned !
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure CloneStructur( aSource, aDest : TClientDataSet );
var
  lcv                    : Integer;
  SourceField, DestField : TField;
begin
  if ( Assigned( aSource ) ) and
     ( Assigned( aDest ) ) then
  begin
    aDest.Close;
    aDest.Fields.Clear;
    for lcv := 0 to pred( aSource.FieldCount ) do
    begin
        if not (aSource.Fields[lcv] is TDatasetField) then
        begin
          SourceField := aSource.Fields[ lcv ];

          if ( SourceField is TAutoIncField ) then
          begin
            DestField := CloneAutoIncField( SourceField, aDest );
          end
          else
          begin
            DestField := CloneField( SourceField, aDest );
          end;

          DestField.DataSet := aDest;
        end; //if not (aSource.Fields[lcv] is TDatasetField)
    end;
    aDest.CreateDataSet;
  end;
end;

{*****************************************************************************
  Name           : CloneAutoIncField 
  Author         : Stefaan Lesage
  Arguments      : SourceField - The field which must be copied
                   aDest       - The DataSet to which the field must be
                                 copied.
  Return Values  : None
  Exceptions     : None
  Description    : Duplicaties the Sourcefield with all its properties to the
                   aDest DataSet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function CloneAutoIncField( const Sourcefield : TField; aDest : TClientDataSet ) : TField;
begin
  Result := TIntegerField.Create( aDest );
  CopyFieldProperties( SourceField, Result );
  CopyFieldClassProperties( SourceField, Result );
end;

{*****************************************************************************
  Name           : CloneField 
  Author         : Stefaan Lesage
  Arguments      : SourceField - The field which must be copied
                   aDest       - The DataSet to which the field must be
                                 copied.
  Return Values  : None
  Exceptions     : None
  Description    : Duplicaties the Sourcefield with all its properties to the
                   aDest DataSet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function CloneField( const SourceField : TField; aDest : TClientDataSet ) : TField;
var
  FieldClassType : TFieldClass;
begin
  FieldClassType := TFieldClass( SourceField.ClassType );

  if ( FieldClassType = TWideStringField ) then
  begin
    FieldClassType := TStringField;
  end;

  Result := FieldClassType.Create( aDest );

  CopyFieldProperties( SourceField, Result );
  CopyFieldClassProperties( SourceField, Result );
end;

{*****************************************************************************
  Name           : CopyFieldProperties 
  Author         : Stefaan Lesage
  Arguments      : aSource - The field from which the properties must be
                             copied.
                   aDest   - The field to which the properties must be
                             copied.
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Copies all properties from one TField to another.                                                         
  History        :

  Date         By                   Description
  ----         --                   -----------
  03/02/2003   wlambrec             Added last part so that no constraints
                                    are on the copied field. See "CloneRecord"
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure CopyFieldProperties( aSource, aDest : TField );
var
  aPropList : TPropList;
  aKinds    : TTypeKinds;
  lcv       : Integer;
begin
  aKinds := tkProperties - [ tkClass ];

  for lcv := 0 to Pred( GetPropList( aSource.ClassInfo, aKinds, @aPropList ) ) do
  begin
    try
      if ( aPropList[ lcv ].Name = 'Name' ) then
      begin
        continue;
      end
      else
      begin
        if not ( IsReadOnlyProperty( aPropList[ lcv ] ) ) {and
               ( IsPublishedProp( aDest, aPropList[ lcv ].Name ) )} then
        begin
          SetPropValue( aDest, aPropList[ lcv ].Name,
                        GetPropValue( aSource, aPropList[ lcv ].Name ) );
        end;
      end;
    except
      on Exception do ShowMessage( aPropList[ lcv ].Name );
    end;
  end;

  //also see TPPWFrameWorkClientDataset.GetFieldConstraints
  aDest.Required := false;
  aDest.ReadOnly := false;
  aDest.CustomConstraint := '';
  if aDest is TIntegerField then
  begin
    TIntegerField(aDest).MinValue := 0;
    TIntegerField(aDest).MaxValue := 0;
  end;
end;

{*****************************************************************************
  Name           : CopyFieldClassProperties 
  Author         : Stefaan Lesage
  Arguments      : aSource - The field from which the properties must be
                             copied.
                   aDest   - The field to which the properties must be
                             copied.
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Copies all Class properties from one TField to another.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  10/12/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure CopyFieldClassProperties( aSource, aDest : TField );
var
  aPropList : TPropList;
  aKinds    : TTypeKinds;
  lcv       : Integer;
begin
  aKinds := [ tkClass ];

  for lcv := 0 to Pred( GetPropList( aSource.ClassInfo, aKinds, @aPropList ) ) do
  begin
    try
      if Not( IsReadOnlyProperty( aPropList[ lcv ] ) ) then
      begin
        SetObjectProp( aDest, aPropList[ lcv ].Name,
                       GetObjectProp( aSource, aPropList[ lcv ].Name ) );
      end;
    except
      on Exception do ShowMessage( aPropList[ lcv ].Name );
    end;
  end;
end;

{*****************************************************************************
  Name           : IsReadOnlyProperty 
  Author         : Stefaan Lesage
  Arguments      : Info - The reference to the Property Info for the property.                                                     
  Return Values  : Returns true or false, indicating if the property is
                   ReadOnly or not.                                                     
  Exceptions     : None
  Description    : This function is used to determine if the Given property
                   can be modified.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  10/12/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

function IsReadOnlyProperty( info : PPropInfo ) : Boolean;
begin
  Result := not ( Assigned( Info ) and ( Info^.SetProc <> nil ) );
end;

{*****************************************************************************
  Name           : CloneRecord
  Author         : Stefaan Lesage
  Arguments      : aSource - The DataSet from which the active record must be
                             copied.
                   aDest   - The DataSet to which the record must be copied.                                                               
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to copy a record from the Source
                   DataSet to the Destination DataSet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  16/09/2003   sLesage              Special processing for LargeInt fields
                                    since apparently you can not chainge the
                                    .Value property of a LargeInt field.
  03/02/2003   wlambrec             Disabled all constraints in the destination
                                    dataset
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure CloneRecord( aSource, aDest : TClientDataSet );
var
  aSourceField : TField;
  aDestField   : TField;
  aReadOnly    : Boolean;
  lcv          : Integer;
begin
  if ( Assigned( aSource ) ) and
     ( Assigned( aDest   ) ) then
  begin
    { Add a new record if necessary, if the Destination dataset is in
      edit Mode or Append mode, the current record will be overwritten
      with the contents of the Source Record }
    if Not ( aDest.State in dsEditModes ) then
    begin
      aDest.Append;
    end;

    //When the Source clientdataset contains some required calculated/lookup
    //fields, the values are not copied in the destination dataset so that
    //the errormessage "FieldValue required" may appear (Customers: Move all badges)
    for lcv := 0 to Pred(aDest.FieldCount) do
    begin
      aDestField := aDest.Fields[lcv];
      aReadOnly := aDestField.ReadOnly;

      aDestField.ReadOnly := False;
      aSourceField := aSource.FindField(aDestField.FieldName);

      if (Assigned (aSourceField)) and
         (aDestField.FieldKind = fkData) and
         (not(aSourceField.IsNull)) then
      begin
        if ( aSourceField.DataType = ftLargeInt ) then
        begin
          aDestField.AsInteger := aSourceField.AsInteger;
        end
        else
        begin
          aDestField.Value := aSourceField.Value;
        end;
      end;

      aDestField.ReadOnly := aReadOnly;
    end;

    aDest.Post;
  end;
end;

end.
