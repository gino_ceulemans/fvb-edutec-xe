{*****************************************************************************
  This unit will contain some IOTA related procedures / functions.

  @Name       Unit_PIFOTAUtils
  @Author     slesage
  @Copyright  (c) 2004 PeopleWare.
  @History

  Date         By                   Description
  ----         --                   -----------
  05/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_PPWFrameWorkOTAUtils;

interface

uses
  ToolsApi, Classes;

type
  TEditorStrings = class(TStrings)
  private
    fStrings: TStrings;
  protected
    function Get(Index: Integer): string; override;
    function GetCount: Integer; override;
    function GetObject(Index: Integer): TObject; override;
    procedure Put(Index: Integer; const Str: string); override;
    procedure PutObject(Index: Integer; Obj: TObject); override;
    function GetPosition(Index: Integer): Longint; virtual;
    function GetCharPos(Index: Integer): TOTACharPos; virtual;
    property Strings: TStrings read fStrings;
  public
    constructor Create(Editor: IOTASourceEditor);
    destructor Destroy; override;
    procedure LoadFromEditor(Editor: IOTASourceEditor);
    procedure SaveToEditor(Editor: IOTASourceEditor);
    procedure Clear; override;
    procedure Delete(Index: Integer); override;
    procedure Insert(Index: Integer; const Str: string); override;
    function PosToCharPos(Pos: Longint): TOTACharPos;
    function CharPosToPos(CharPos: TOTACharPos): Longint;
    property Position[Index: Integer]: Longint read GetPosition;
    property CharPos[Index: Integer]: TOTACharPos read GetCharPos;
  end;

  Function ActiveProjectFileName : String;
  Function ActiveProjectName     : String;
  Function ActiveProjectPath     : String;
  Function ActiveProjectSourceEditor : IOTASourceEditor;
  Function SourceEditorForModule( aModule : IOTAModule ) : IOTASourceEditor;
  Function OtaOpenFile(const FileName: string): Boolean;
  Function GetFormEditorFromModule(IModule: IOTAModule): IOTAFormEditor;
  procedure OpenFormEditorForModule( IModule : IOTAModule );

implementation

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  SysUtils;

procedure GxOtaSaveReaderToStream(EditReader: IOTAEditReader; Stream: TStream);
const
  // Leave typed constant as is - needed for streaming code.
  TerminatingNulChar: Char = #0;
  BufferSize = 1024 * 24;
var
  Buffer: PAnsiChar;
  EditReaderPos: Integer;
  ReadDataSize: Integer;
begin
  Assert(EditReader <> nil);
  Assert(Stream <> nil);

  GetMem(Buffer, BufferSize);
  try
    EditReaderPos := 0;
    ReadDataSize := EditReader.GetText(EditReaderPos, Buffer, BufferSize);
    Inc(EditReaderPos, ReadDataSize);
    while ReadDataSize = BufferSize do
    begin
      Stream.Write(Buffer^, ReadDataSize);
      ReadDataSize := EditReader.GetText(EditReaderPos, Buffer, BufferSize);
      Inc(EditReaderPos, ReadDataSize);
    end;
    Stream.Write(Buffer^, ReadDataSize);
    Stream.Write(TerminatingNulChar, SizeOf(TerminatingNulChar));
  finally
    FreeMem(Buffer);
  end;
end;

//function GxOtaGetCurrentSourceEditor: IOTASourceEditor;
//var
//  EditBuffer: IOTAEditBuffer;
//begin
//  Result := nil;
//  EditBuffer := GxOtaGetTopMostEditBuffer;
//  if Assigned(EditBuffer) and (EditBuffer.FileName <> '') then
//    Result := GxOtaGetSourceEditorFromModule(GxOtaGetCurrentModule, EditBuffer.FileName);
//  if Result = nil then
//    Result := GxOtaGetSourceEditorFromModule(GxOtaGetCurrentModule);
//end;
//
procedure GxOtaAssertSourceEditorNotReadOnly(SourceEditor: IOTASourceEditor);
begin
  Assert(Assigned(SourceEditor));
  if Supports(SourceEditor, IOTAEditBuffer) then
    if (SourceEditor as IOTAEditBuffer).IsReadOnly then
      raise Exception.CreateFmt('%s is read only', [ExtractFileName(SourceEditor.FileName)]);
end;

function GxOtaGetEditWriterForSourceEditor(SourceEditor: IOTASourceEditor = nil): IOTAEditWriter;
resourcestring
  SEditWriterNotAvail = 'Edit writer not available';
begin
//  if not Assigned(SourceEditor) then
//    SourceEditor := GxOtaGetCurrentSourceEditor;
  if Assigned(SourceEditor) then
  begin
    GxOtaAssertSourceEditorNotReadOnly(SourceEditor);
    Result := SourceEditor.CreateUndoableWriter;
  end;
  Assert(Assigned(Result), SEditWriterNotAvail);
end;

{*****************************************************************************
  This function will be used to return the FileName of the Active Project.

  @Name       ActiveProjectFileName
  @author     slesage
  @param      None
  @return     Returns the Path and FileName of the currently active Project.
  @Exception  None
  @See        None
******************************************************************************}

function ActiveProjectFileName : String;
var
  aProject : IOTAProject;
begin
  aProject := GetActiveProject;

  if ( Assigned( aProject ) ) then
  begin
    Result := aProject.FileName;
  end
  else
  begin
    Result := '';
  end;
end;

{*****************************************************************************
  This function will be used to return the Name of the Project.

  @Name       ActiveProjectName
  @author     slesage
  @param      None
  @return     Returns the Name of the project ( FileName without path and
              FileExtension )
  @Exception  None
  @See        None
******************************************************************************}

function ActiveProjectName : String;
begin
  Result := ExtractFileName( ChangeFileExt( ActiveProjectFileName, '' ) );
end;

{*****************************************************************************
  This function will be used to return the Path of the Project.

  @Name       ActiveProjectPath
  @author     slesage
  @param      None
  @return     Returns the Pat of the Project.
  @Exception  None
  @See        None
******************************************************************************}

function ActiveProjectPath : String;
begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Nil, 'ActiveProjectPath' );
  {$ENDIF}

  Result := {ExcludeTrailingPathDelimiter(} ExtractFilePath( ActiveProjectFileName {)} );

  {$IFDEF CODESITE}
  CodeSite.SendString( 'Result', Result );
  CodeSite.ExitMethod( Nil, 'ActiveProjectPath' );
  {$ENDIF}

end;

function ActiveProjectSourceEditor : IOTASourceEditor;
var
  aProject      : IOTAProject;
  lcv           : Integer;
  aSourceEditor : IOTASourceEditor;
begin
  Result := Nil;
  aProject := GetActiveProject;
  if ( Assigned( AProject ) ) then
  begin
    for lcv := 0 to Pred( aProject.ModuleFileCount ) do
    begin
      if ( Supports( aProject.ModuleFileEditors[ lcv ], IOTASourceEditor, aSourceEditor ) ) then
      begin
        Result := aSourceEditor;
      end;
    end;
  end;
end;

{ TEditorStrings }

{ Create an edit reader string list. }
constructor TEditorStrings.Create(Editor: IOTASourceEditor);
begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Self, 'Create' );
  {$ENDIF}

  inherited Create;
  fStrings := TStringList.Create;
  LoadFromEditor(Editor);
  
  {$IFDEF CODESITE}
  CodeSite.ExitMethod( Self, 'Create' );
  {$ENDIF}
end;

destructor TEditorStrings.Destroy;
begin
  FreeAndNil(fStrings);

  inherited Destroy;
end;

{ Load a string list from an editor interface. Read the edit
  reader as a stream. As each line is added to the string list,
  remember the position of that line in the stream. }
procedure TEditorStrings.LoadFromEditor(Editor: IOTASourceEditor);
var
  StrStream: TStringStream;
  Str: PChar;
  Pos, I: Integer;
begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Self, 'LoadFromEditor' );
  {$ENDIF}

  StrStream := TStringStream.Create('');
  try
    { Read the entire buffer into StrStream. }
    GxOtaSaveReaderToStream(Editor.CreateReader, StrStream);
    { Copy every line from StrStream to the string list. }
    Strings.Text := StrStream.DataString;

    { Scan the string to find the buffer position of each line. }
    Str := PChar(StrStream.DataString);
    Pos := 0;
    for I := 0 to Count-1 do
    begin
      Strings.Objects[I] := TObject(Pos);
      Inc(Pos, Length(Strings[I]));
      if Str[Pos] = #13 then // TODO -oStefan -cCRLF: Check
        Inc(Pos);
      if Str[Pos] = #10 then
        Inc(Pos);
    end;
  finally
    FreeAndNil(StrStream);
  end;
  
  {$IFDEF CODESITE}
  CodeSite.ExitMethod( Self, 'LoadFromEditor' );
  {$ENDIF}
end;

{ Save the string list to an editor interface. The string list
  does not keep track of specific changes, so replace the entire
  file with the text of the string list. }
procedure TEditorStrings.SaveToEditor(Editor: IOTASourceEditor);
var
  Writer: IOTAEditWriter;
begin
  Writer := GxOtaGetEditWriterForSourceEditor(Editor);
  try
    Writer.DeleteTo(High(Longint));
    Writer.Insert(PAnsiChar(fStrings.Text));
  finally
    Writer := nil;
  end;
end;

{ Get a string. }
function TEditorStrings.Get(Index: Integer): string;
begin
  Result := Strings[Index];
end;

{ Get an object, which is really the string position. }
function TEditorStrings.GetObject(Index: Integer): TObject;
begin
  Result := Strings.Objects[Index];
end;

{ Set a string. }
procedure TEditorStrings.Put(Index: Integer; const Str: string);
begin
  Strings[Index] := Str;
end;

{ Set a string's position. }
procedure TEditorStrings.PutObject(Index: Integer; Obj: TObject);
begin
  Objects[Index] := Obj;
end;

{ Return the number of lines in the list. }
function TEditorStrings.GetCount: Integer;
begin
  Result := Strings.Count;
end;

procedure TEditorStrings.Clear;
begin
  Strings.Clear;
end;

procedure TEditorStrings.Delete(Index: Integer);
begin
  Strings.Delete(Index);
end;

procedure TEditorStrings.Insert(Index: Integer; const Str: string);
begin
  Strings.Insert(Index, Str);
end;

{ For convenience, return a position as an Integer. }
function TEditorStrings.GetPosition(Index: Integer): Longint;
begin
  Result := Longint(Strings.Objects[Index]);
end;

{ Return a position as a character position. }
function TEditorStrings.GetCharPos(Index: Integer): TOTACharPos;
begin
  Result := PosToCharPos(GetPosition(Index));
end;

{ Get the buffer position given a character position.
  The character position position specifies a line of text.
  Retrieve the buffer position for the start of that line,
  and add the character index. If the character index is
  past the end of line, return the position of the line
  ending. }
function TEditorStrings.CharPosToPos(CharPos: TOTACharPos): Longint;
var
  Text: string;
begin
  { CharPos.Line is 1-based; Strings list is 0-based. }
  Text := Strings[CharPos.Line-1];
  if CharPos.CharIndex > Length(Text) then
    Result := Position[CharPos.Line-1] + Length(Text)
  else
    Result := Position[CharPos.Line-1] + CharPos.CharIndex;
end;

{ Convert a buffer position to a character position.
  Search for the line such that Pos is between the start
  and end positions of the line. That specifies the line
  number. The char index is the offset within the line.
  If Pos lies within a line ending, return the character
  index of the end of the line.

  Line indices are 1-based, and string list indices are
  0-based, so add 1 to get the real line number.

  Use binary search to locate the desired line quickly. }
function TEditorStrings.PosToCharPos(Pos: Longint): TOTACharPos;
var
  Lo, Mid, Hi: Integer;
begin
  Lo := 0;
  Hi := Strings.Count-1;
  while Lo <= Hi do
  begin
    Mid := (Lo + Hi) div 2;
    if Position[Mid] <= Pos then
      Lo := Mid+1
    else
      Hi := Mid-1
  end;

  Result.Line := Lo;
  if Pos >= Position[Lo-1]+Length(Strings[Lo-1]) then
    Result.CharIndex := Length(Strings[Lo-1])
  else
    Result.CharIndex := Pos - Position[Lo-1];
end;

Function SourceEditorForModule( aModule : IOTAModule ) : IOTASourceEditor;
var
//  aProject      : IOTAProject;
  lcv           : Integer;
  aSourceEditor : IOTASourceEditor;
begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Nil, 'SourceEditorForModule' );
  {$ENDIF}

  Result := Nil;

  if ( Assigned( aModule ) ) then
  begin
    for lcv := 0 to Pred( aModule.ModuleFileCount ) do
    begin
      if ( Supports( aModule.ModuleFileEditors[ lcv ], IOTASourceEditor, aSourceEditor ) ) then
      begin
        Result := aSourceEditor;
      end;
    end;
  end;
  
  {$IFDEF CODESITE}
  CodeSite.ExitMethod( Nil, 'SourceEditorForModule' );
  {$ENDIF}
end;

function OtaOpenFile(const FileName: string): Boolean;
var
  ActionServices: IOTAActionServices;
begin
  ActionServices := BorlandIDEServices as IOTAActionServices;
  Assert(Assigned(ActionServices));

  Result := ActionServices.OpenFile(FileName);
end;

function GetFormEditorFromModule(IModule: IOTAModule): IOTAFormEditor;
var
  i: Integer;
  IEditor: IOTAEditor;
begin
  Result := nil;
  if IModule = nil then
     Exit;
  for i := 0 to IModule.GetModuleFileCount - 1 do
  begin
    IEditor := IModule.GetModuleFileEditor(i);
    if Supports(IEditor, IOTAFormEditor, Result) then
      Break;
  end;
end;

procedure OpenFormEditorForModule( IModule : IOTAModule );
var
  aFormEditor : IOTAFormEditor;
begin
  aFormEditor := GetFormEditorFromModule( IModule );
  if ( Assigned( aFormEditor ) ) then
  begin
    aFormEditor.Show;
  end;
end;

end.
