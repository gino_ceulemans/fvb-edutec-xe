unit Unit_PPWFrameWorkIOTA;

interface

uses
  ToolsAPI,
  Unit_PPWFrameWorkController;

function FindControllerInProject : TPPWFrameWorkController;
function FindModuleInterface(AInterface: TGUID): IUnknown;
function GetFormEditorFromModule(IModule: IOTAModule): IOTAFormEditor;

implementation

{*****************************************************************************
  Name           : FindModuleInterface
  Author         : Lesage Stefaan
  Arguments      : AInterface - The IOTAModuleInterface you are looking for
  Return Values  : Returns the IOTAModuleInterface if it was found
  Exceptions     : This routine loops over all IOTAModuleServices.Modules to
                   search for the given IOTAModuleInterface and returns it if
                   it has been found.
  Description    :
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/05/2001   Lesage Stefaan       Initial creation of the Unit.
 *****************************************************************************}

function FindModuleInterface(AInterface: TGUID): IUnknown;
var
  lcv : Integer;
begin
  Result := nil;

  // Loop over all ModuleInterfaces to find the ModuleInterface
  with BorlandIDEServices as IOTAModuleServices do
  begin
    for lcv := 0 to ModuleCount - 1 do
    begin
      if ( Modules[ lcv ].QueryInterface( AInterface, Result ) = S_OK ) then
      begin
        Break;
      end;
    end;
  end;
end;

function FindControllerInProject : TPPWFrameWorkController;
var
  lcv         : Integer;
  aProject    : IOTAProject;
  aModule     : IOTAModule;
  aFileName   : String;
  aFormEditor : IOTAFormEditor;
begin
  Result := Nil;

  aProject := GetActiveProject;

  if ( Assigned( aProject ) ) then
  begin
    for lcv := 0 to Pred( aProject.GetModuleCount ) do
    begin
      aFileName := aProject.GetModule( lcv ).FormName;

      if ( aFileName <> '' ) then
      begin
        aModule := ( BorlandIDEServices as IOTAModuleServices ).FindFormModule( aFileName )

        if ( Assigned( aModule ) ) then
        begin
          aFormEditor := GetFormEditorFromModule( aModule );

          if Assigned( aFormEditor.f
        end;
      end;


    end;
  end;
end;

{*****************************************************************************
  Name           : GetFormEditorFromModule
  Author         : Perevoznyk Serhiy
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    :
  History        :

  Date         By                   Description
  ----         --                   -----------
  08/08/2001   Perevoznyk Serhiy       Initial creation of the procedure.
 *****************************************************************************}

function GetFormEditorFromModule(IModule: IOTAModule): IOTAFormEditor;
var
  i: Integer;
  IEditor: IOTAEditor;
begin
  Result := nil;
  if IModule = nil then
     Exit;
  for i := 0 to IModule.GetModuleFileCount - 1 do
  begin
    IEditor := IModule.GetModuleFileEditor(i);
    if Supports(IEditor, IOTAFormEditor, Result) then
      Break;
  end;
end;

end.
