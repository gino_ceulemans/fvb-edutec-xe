{*****************************************************************************
  Name           : Unit_PPWFrameWorkDataModule
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2002 Peopleware                                      
  Description    : This unit Contains the Base FrameWork DataModule.  This
                   DataModule can then be used as a template to Create
                   Project / Clients specific versions of the Base DataModule.                                                          
  History        :                                                          
                                                                            
  Date         By                   Description
  ----         --                   -----------
  27/12/2004   sLesage              Added a GetCanOpenNewRecord property, which
                                    will return a boolean indicating if a new
                                    RecordView can be opened for the DataModule.
  18/11/2004   sLesage              Added a OnFilterRecord method which will
                                    be called whenever the DataModule needs
                                    to filter the RecordDataSet so it contains
                                    only one record.
  04/03/2004   sLesage              Added a CursorRestorer to some procedures
                                    to indicate that the system is busy.
  06/10/2003   sLesage              Fixed a bug in the RefreshRecordDataSet
                                    method.  It was causing problems with the
                                    multiple fields in the KeyFields property.
  01/09/2003   sLesage              Fixed some bugs with the Refreshing of
                                    the List DataSet when the DataModule is
                                    shown as a detail of anohter DataModule.
  29/08/2003   sLesage              The OpenDataSet method will now raise an
                                    exception when an error occured during the
                                    opening of the DataSet.  Previously the
                                    exception was simply ignored.
  23/07/2003   mBellekens           Making "BuildFilterExpressionForDetail" virtual
  22/07/2003   sLesage              Added a TableName property.
                                    Added the BuildFilterExpressionForDetail
                                    method which will be used to build the
                                    filter expression that must be used in a
                                    Master / Detail relationship.
  10/12/2002   sLesage              Added support for sowing a ListView form
                                    for selection of records.
                                    Fixed a little bug in the Streaming of the
                                    Collections.
  6/12/2002    sLesage              Added a RefreshData method which can be
                                    used to refresh the Main DataSet.                                    
  29/11/2002   sLesage              Cleaned up unnecessary Code and added
                                    Unit / Procedure Headers.
  21/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

unit Unit_PPWFrameWorkDataModule;

interface

uses
  { Delphi Units }
  SysUtils, Classes, DB, DBClient,
  { FrameWork Units }
  Unit_PPWFrameWorkClasses,
  Unit_PPWFrameWorkInterfaces;

type
  TPPWFrameWorkDataModule = class(TDataModule, IPPWFrameWorkDataModule)
  private
    FAutoDestroy        : Boolean;
    FAutoOpenDataSets   : Boolean;
    FDataSet            : TClientDataSet;
    FRecordDataSet      : TClientDataSet;
    FListViewClass      : TPPWListViewClassName;
    FRecordViewClass    : TPPWRecordViewClassName;
    FRefCount           : Integer;
    FRegistered         : Boolean;
    FVisible            : Boolean;
    FMasterDataModule   : IPPWFrameWorkDataModule;
    FListViewDataModule : IPPWFrameWorkDataModule;
    FMasterRecordView   : IPPWFrameWorkRecordView; //added WL 17/12
    FFetchOnDemand      : Boolean;
    FOnCreateListView   : TPPWFrameWorkCreateFormEvent;
    FOnCreateRecordView : TPPWFrameWorkCreateFormEvent;
    FOnFilter           : TPPWFrameWorkOnFilterEvent;
    FOnLinkListView     : TPPWFrameWorkLinkListViewEvent;
    FOnLinkRecordView   : TPPWFrameWorkLinkRecordViewEvent;
    FOnSort             : TPPWFrameWorkSortEvent;
    FFilterFirst        : Boolean;
    FOnCancelFilter     : TNotifyEvent;
    FKeyFields          : string;
    FFiltered           : Boolean;
    FDetailDataModules  : TPPWFrameWorkDataModuleDetails;
    FRecordViews        : TInterfaceList;
    FRecordViewDataModules : TInterfaceList;
    FListView           : IPPWFrameWorkListView;
    FOnFilterDetail     : TPPWFrameWorkFilterDetailEvent;
    FOnFilterRecord     : TPPWFrameWorkFilterDetailEvent;
    FOnBeforeOpenRecordView : TPPWFrameWorkBeforeOpenRecordViewEvent;
    FAsSelection            : Boolean;
    FOnBeforeCreateDetailDataModule : TPPWFrameWorkBeforeCreateDetailEvent;
    FOnAfterCreateDetailDataModule: TPPWFrameWorkAfterCreateDetailEvent;
    FForeignKeys: String;
    FTableName : String;

    procedure ReadParamData(Reader: TReader);
    procedure WriteParamData(Writer: TWriter);
    { Private declarations }
  protected
    { Property Getters }
    function GetAsSelection: Boolean; virtual;
    function GetAutoDestroy: Boolean; virtual;
    function GetAutoOpenDataSets: Boolean; virtual;
    function GetCanOpenNewRecordView: Boolean; virtual;
    function GetDataSet: TClientDataSet; virtual;
    function GetRecordDataset: TClientDataset; virtual;
    function GetDetailDataModules: TPPWFrameWorkDataModuleDetails; virtual;
    function GetFiltered: Boolean; virtual;
    function GetKeyFields : string; virtual;
    function GetFetchOnDemand : Boolean; virtual;
    function GetFilterFirst: Boolean; virtual;
    function GetListView : IPPWFrameWorkListView; virtual;
    function GetListViewClass: TPPWListViewClassName; virtual;
    function GetMasterDataModule: IPPWFrameWorkDataModule; virtual;
    function GetName : TComponentName; virtual;
    function GetListViewDataModule: IPPWFrameWorkDataModule; virtual;
    function GetRecordViewClass: TPPWRecordViewClassName; virtual;
    function GetRecordViews: TInterfaceList; virtual;
    function GetRecordViewDataModules: TInterfaceList; virtual;
    function GetRegistered: Boolean; virtual;
    function GetTableName: String; virtual;
    function GetVisible: Boolean; virtual;
    function GetForeignKeys: String; virtual;

    function GetOnBeforeOpenRecordView : TPPWFrameWorkBeforeOpenRecordViewEvent; virtual;
    function GetOnBeforeCreateDetailDataModule: TPPWFrameWorkBeforeCreateDetailEvent; virtual;
    function GetOnAfterCreateDetailDataModule: TPPWFrameWorkAfterCreateDetailEvent; virtual; //added WL 18/12
    function GetOnCancelFilter: TNotifyEvent; virtual;
    function GetOnCreateListView : TPPWFrameWorkCreateFormEvent; virtual;
    function GetOnCreateRecordView : TPPWFrameWorkCreateFormEvent; virtual;
    function GetOnFilterDetail: TPPWFrameWorkFilterDetailEvent; virtual;
    function GetOnFilterRecord: TPPWFrameWorkFilterDetailEvent; virtual;
    function GetOnFilter: TPPWFrameWorkOnFilterEvent;
    function GetOnLinkRecordView : TPPWFrameWorkLinkRecordViewEvent; virtual;
    function GetOnLinkListView : TPPWFrameWorkLinkListViewEvent; virtual;
    function GetOnSort: TPPWFrameWorkSortEvent;

    { Property Setters }
    procedure SetAsSelection(const Value: Boolean); virtual;
    procedure SetAutoDestroy( const Value: Boolean ); virtual;
    procedure SetAutoOpenDataSets( const Value: Boolean ); virtual;
    procedure SetDataSet( const Value: TClientDataSet ); virtual;
    procedure SetDetailDataModules( const Value: TPPWFrameWorkDataModuleDetails ); virtual;
    procedure SetFetchOnDemand( const Value : Boolean ); virtual;
    procedure SetFilterFirst( const Value: Boolean ); virtual;
    procedure SetKeyFields( const Value : string ); virtual;
    procedure SetListView( const Value : IPPWFrameWorkListView ); virtual;
    procedure SetListViewClass( const Value: TPPWListViewClassName ); virtual;
    procedure SetMasterDataModule( const Value: IPPWFrameWorkDataModule ); virtual;
    procedure SetListViewDataModule (const Value: IPPWFrameWorkDataModule); virtual; //WLUPDATE
    procedure SetMasterRecordView( const Value: IPPWFrameWorkRecordView); virtual; //WLUPDATE
    procedure SetName( const Value: TComponentName ); override;
    procedure SetRecordViewClass( const Value: TPPWRecordViewClassName ); virtual;
    procedure SetRegistered( const Value: Boolean ); virtual;
    procedure SetVisible( const Value: Boolean ); virtual;
    procedure SetForeignKeys( const Value: String); virtual;
    procedure SetRecordDataSet(const Value: TClientDataset); virtual;
    procedure SetTableName(const Value: String); virtual;

    procedure SetOnBeforeOpenRecordView( const Value : TPPWFrameWorkBeforeOpenRecordViewEvent ); virtual;
    procedure SetOnCancelFilter( const Value: TNotifyEvent ); virtual;
    procedure SetOnBeforeCreateDetailDataModule( const Value: TPPWFrameWorkBeforeCreateDetailEvent); virtual;
    procedure SetOnAfterCreateDetailDataModule (const Value: TPPWFrameWorkAfterCreateDetailEvent); virtual;
    procedure SetOnCreateListView( const Value: TPPWFrameWorkCreateFormEvent ); virtual;
    procedure SetOnCreateRecordView( const Value: TPPWFrameWorkCreateFormEvent ); virtual;
    procedure SetOnFilterDetail( const Value: TPPWFrameWorkFilterDetailEvent ); virtual;
    procedure SetOnFilterRecord( const Value: TPPWFrameWorkFilterDetailEvent ); virtual;
    procedure SetOnFilter( const Value: TPPWFrameWorkOnFilterEvent ); virtual;
    procedure SetOnLinkListView( const Value: TPPWFrameWorkLinkListViewEvent ); virtual;
    procedure SetOnLinkRecordView( const Value: TPPWFrameWorkLinkRecordViewEvent ); virtual;
    procedure SetOnSort( const Value: TPPWFrameWorkSortEvent ); virtual;

    { Methods for Reference Counting }
    function _AddRef  : Integer; stdcall;
    function _Release : Integer; stdcall;

    procedure Notification( aComponent : TComponent; Operation : TOperation ); Override;
    procedure DefineProperties(Filer: TFiler); override;

    { Event Dispatcher Methods }
    procedure DoApplyPendingUpdates; virtual;
    procedure DoApplyFilter; virtual;
    procedure DoBeforeOpenRecordView( Sender : TObject; RecordViewMode : TPPWFrameWorkRecordViewMode; var AllowRecordView : Boolean ); virtual;
    procedure DoCancelFilter; virtual;
    procedure DoFilterDetail( KeyFields : String; KeyValues : Variant; Expression : String ); virtual;
    procedure DoFilterRecord( KeyFields : String; KeyValues : Variant; Expression : String ); virtual;
    procedure DoSort( Fields : String ); virtual;
    procedure OpenDataSet( aDataSet : TDataSet ); virtual;

    { Registration / Unregistration Methods }
    procedure RegisterSelf; virtual;
    procedure UnRegisterSelf; virtual;

    function CreateFieldValuesVarArray( aDataSet : TDataSet; aFields : String ) : Variant; virtual;

    { Others }
    Function BuildFilterExpressionForDetail( AMasterDataModule : IPPWFrameWorkDataModule;
                                             ADetailDataModule : IPPWFrameWorkDataModule;
                                             aForeignKeys      : String;
                                             aUseRecordDataSet : Boolean = False ) : String; virtual;

    property Filtered         : Boolean                 read GetFiltered;
    property MasterDataModule : IPPWFrameWorkDataModule read GetMasterDataModule
                                                        write SetMasterDataModule;
    property MasterRecordView : IPPWFrameWorkRecordview read FMasterRecordView;
    property Visible          : Boolean                 read GetVisible
                                                        write SetVisible;
  public
    { Public declarations }
    constructor Create( AOwner : TComponent; AAsSelection : Boolean = False ); reintroduce; virtual;
    constructor CreateAsDetail( AOwner : TComponent;
      AMasterDataModule : IPPWFrameWorkDataModule; AMasterRecordView: IPPWFrameworkRecordView ); virtual;
      //added WL 17/12, added parameter
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); override;
    destructor Destroy; override;

    procedure Add; virtual;
    procedure Delete; virtual;
    procedure Edit; virtual;
    procedure LinkToListView( var aListView : IPPWFrameWorkListView ); virtual;
    procedure LinkToRecordView( var aRecordView : IPPWFrameWorkRecordView; const aRecordViewMode : TPPWFrameWorkRecordViewMode ); virtual;
    procedure OpenDataSets; virtual;
    procedure Sort( const Fields : String ); virtual;
    procedure View; virtual;

    procedure RefreshRecordDataSet; virtual;
    procedure RefreshListDataSet; virtual;



    function GetMasterRecordView: IPPWFrameWorkRecordView; virtual; //added WL 17/12
    function ClientDatasetByName (const Name : String): TClientDataset; //WLUPDATE
    function FrameWorkDatamoduleClassName: String; //WLUPDATE

    property AsSelection      : Boolean                read GetAsSelection
                                                       write SetAsSelection;
    property CanOpenNewRecordView : Boolean            read GetCanOpenNewRecordView;
    property RecordViews      : TInterfaceList         read GetRecordViews;
    property RecordViewDataModules : TInterfaceList    read GetRecordViewDataModules;
    property ForeignKeys      : String                 read GetForeignKeys
                                                       write SetForeignKeys;
    property ListViewDataModule : IPPWFrameWorkDataModule read GetListViewDataModule
                                                          write SetListViewDataModule;
  published
    property AutoDestroy      : Boolean                read GetAutoDestroy
                                                       write SetAutoDestroy;
    property AutoOpenDataSets : Boolean                read GetAutoOpenDataSets
                                                       write SetAutoOpenDataSets;
    property DataSet          : TClientDataSet         read GetDataSet
                                                       write SetDataSet;
    property RecordDataset    : TClientDataset         read GetRecordDataset
                                                       write SetRecordDataSet;
    property DetailDataModules: TPPWFrameWorkDataModuleDetails
                                                       read GetDetailDataModules
                                                       write SetDetailDataModules
                                                       stored false;

    property FetchOnDemand    : Boolean                read GetFetchOnDemand
                                                       write SetFetchOnDemand;
    property FilterFirst      : Boolean                read GetFilterFirst
                                                       write SetFilterFirst;
    property KeyFields        : string                 read GetKeyFields
                                                       write SetKeyFields;
    property ListViewClass    : TPPWListViewClassName  read GetListViewClass
                                                       write SetListViewClass;
    property RecordViewClass  : TPPWRecordViewClassName read GetRecordViewClass
                                                        write SetRecordViewClass;
    property Registered       : Boolean                 read GetRegistered
                                                        write SetRegistered;

    property OnBeforeOpenRecordView : TPPWFrameWorkBeforeOpenRecordViewEvent
                                                       read GetOnBeforeOpenRecordView
                                                       write SetOnBeforeOpenRecordView;
    property OnCancelFilter : TNotifyEvent              read GetOnCancelFilter
                                                        write SetOnCancelFilter;
    property OnBeforeCreateDetailDataModule : TPPWFrameWorkBeforeCreateDetailEvent
                                                        read GetOnBeforeCreateDetailDataModule
                                                        write SetOnBeforeCreateDetailDataModule;
    property OnAfterCreateDetailDataModule : TPPWFrameWorkAfterCreateDetailEvent
                                                        read GetOnAfterCreateDetailDataModule
                                                        write SetOnAfterCreateDetailDataModule;
    property OnCreateListView : TPPWFrameWorkCreateFormEvent
                                                       read GetOnCreateListView
                                                       write SetOnCreateListView;
    property OnCreateRecordView : TPPWFrameWorkCreateFormEvent
                                                       read GetOnCreateRecordView
                                                       write SetOnCreateRecordView;
    property OnFilter         : TPPWFrameWorkOnFilterEvent
                                                       read GetOnFilter
                                                       write SetOnFilter;
    property OnFilterDetail   : TPPWFrameWorkFilterDetailEvent
                                                       read GetOnFilterDetail
                                                       write SetOnFilterDetail;
    property OnFilterRecord   : TPPWFrameWorkFilterDetailEvent
                                                       read GetOnFilterRecord
                                                       write SetOnFilterRecord;
    property OnLinkListView   : TPPWFrameWorkLinkListViewEvent
                                                       read GetOnLinkListView
                                                       write SetOnLinkListView;
    property OnLinkRecordView : TPPWFrameWorkLinkRecordViewEvent
                                                       read GetOnLinkRecordView
                                                       write SetOnLinkRecordView;
    property OnSort           : TPPWFrameWorkSortEvent read GetOnSort
                                                       write SetOnSort;
    property TableName          : String               read GetTableName
                                                       write SetTableName;
  end;

  TPPWFrameWorkDataModuleClass = Class of TPPWFrameWorkDataModule;

implementation

uses
  { Delphi Units }
  Windows, Controls, RTLConsts, TypInfo, Variants, DBConsts,
  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects,
  {$ENDIF}
  { FrameWork Units }
  //Unit_PPWFrameWorkObjects,
  Unit_PPWFrameWorkController, Forms,
  Unit_PPWFrameWorkExceptions;

{$R *.dfm}

{ TPPWFrameWorkDataModule }

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.Add
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to open the RecordView form and
                   allow the user to Add a new record.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.Add;
var
  Allow : Boolean;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'Add' );
  {$ENDIF}

  Allow := CanOpenNewRecordView;

  DoBeforeOpenRecordView( Self, rvmAdd, Allow );

  if ( Allow ) then
  begin
    if ( Assigned( PPWController ) ) then
    begin
      PPWController.ShowRecordViewForDataModule( Self, rvmAdd );
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'Add' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.DoApplyFilter
  Author         : Stefaan Lesage
  Arguments      : FilterAppliee - Theis method will be used to return if a
                                   Filter was applied or not.
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to trigger the Filter Event.
                   The actual code for filtering the dataset will be found
                   in the OnFilter Event.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.DoApplyFilter;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'ApplyFilter' );
  {$ENDIF}

  if ( Assigned( FOnFilter ) ) then
  begin
    FOnFilter( Self, FFiltered );
  end
  else
  begin
    DataSet.Filtered := True;
    FFiltered := True;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'ApplyFilter' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.DoCancelFilter
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to trigger the CancelFilter Event.
                   The actual code for canceling the Filter will be found
                   in the OnCancelFilter Event.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.DoCancelFilter;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'CancelFilter' );
  {$ENDIF}

  if ( Assigned( FOnCancelFilter ) ) then
  begin
    FOnCancelFilter( Self );
  end
  else
  begin
    DataSet.Filtered := False;
  end;

  FFiltered := False;
  
  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'CancelFilter' );
  {$ENDIF}
end;

constructor TPPWFrameWorkDataModule.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
  inherited CreateNew(AOwner);
  FRecordViews := TInterfaceList.Create;
  FRecordViewDataModules := TInterfaceList.Create;
  FDetailDataModules := TPPWFrameWorkDataModuleDetails.Create( Self, TPPWFrameWorkDataModuleDetail );


  FAutoDestroy      := True;
  FAutoOpenDataSets := True;
  FFetchOnDemand    := True;
  FFilterFirst      := False;
  FAsSelection       := False;
  FForeignKeys      := '';
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.Create
  Author         : Stefaan Lesage
  Arguments      : AOwner      - The Owner of the DataModule
                   AsSelection - Will be used to indicate that the DataModule
                                 was created simply for selection purposes.
  Return Values  : None
  Exceptions     : None
  Description    : This is the Constructor of the DataModule in which we
                   will initialise some properties.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

constructor TPPWFrameWorkDataModule.Create(AOwner: TComponent; AAsSelection : Boolean = False);
begin
  CreateNew(AOwner);
  if (ClassType <> TPPWFrameWorkDataModule) and not (csDesigning in ComponentState) then
  begin
    if not InitInheritedComponent(Self, TPPWFrameWorkDataModule) then
      raise EResNotFound.CreateFmt(SResNotFound, [ClassName]);
    try
      if Assigned(OnCreate) and OldCreateOrder then OnCreate(Self);
    except
      if Assigned(ApplicationHandleException) then
        ApplicationHandleException(Self);
    end;
  end;
  FAsSelection     := AAsSelection;
  FMasterRecordView:=nil;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.CreateAsDetail 
  Author         : Wim Lambrechts                                          
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    :                                                          
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------
  07/04/2003   Wim Lambrechts       FMasterDatamodule now referes to the datamodule
                                    of the opened recordview
  ??/??/????   Stefaan Lesage       Initial creation
 *****************************************************************************}

constructor TPPWFrameWorkDataModule.CreateAsDetail(AOwner: TComponent;
  AMasterDataModule : IPPWFrameWorkDataModule; AMasterRecordView: IPPWFrameWorkRecordView);
begin
  {Inherited }Create( AOwner );

  { Initialise some Properties }
  FAsSelection      := False;
  FMasterRecordView := AMasterRecordView;
  if ( Assigned( AMasterRecordView ) ) then
  begin
    FMasterDataModule := AMasterRecordView.RecordViewDatamodule; //WLUPDATE
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.Delete
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to open the RecordView form and
                   allow the user to Delete the Current record.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.Delete;
var
  Allow : Boolean;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'Delete' );
  {$ENDIF}

  Allow := CanOpenNewRecordView;

  DoBeforeOpenRecordView( Self, rvmDelete, Allow );

  if ( Allow ) then
  begin
    if ( Assigned( PPWController ) ) then
    begin
      PPWController.ShowRecordViewForDataModule( Self, rvmDelete );
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'Delete' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.Destroy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : The destructor of the DataModule which will be used
                   to CleanUp some things.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

destructor TPPWFrameWorkDataModule.Destroy;
begin
  MasterDataModule := Nil;
  FListView := Nil;

  FreeAndNil( FDetailDataModules );
  FreeAndNil( FRecordViewDataModules );
  FreeAndNil( FRecordViews );

  inherited Destroy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.Edit
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to open the RecordView form and
                   allow the user to Edit the Current record.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.Edit;
Var
  Allow : Boolean;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'Edit' );
  {$ENDIF}

  Allow := CanOpenNewRecordView;

  DoBeforeOpenRecordView( Self, rvmEdit, Allow );

  if ( Allow ) then
  begin
    if ( Assigned( PPWController ) ) then
    begin
      PPWController.ShowRecordViewForDataModule( Self, rvmEdit );
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'Edit' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetAutoDestroy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the AutoDestroy Property.
  Exceptions     : None
  Description    : Property Getter for the AutoDestroy Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetAutoDestroy: Boolean;
begin
  Result := FAutoDestroy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetAutoOpenDataSets 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the AutoOpenDataSets Property.
  Exceptions     : None
  Description    : Property Getter for the AutoOpenDataSets Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetAutoOpenDataSets: Boolean;
begin
  Result := FAutoOpenDataSets;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetDataSet 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the DataSet Property.
  Exceptions     : None
  Description    : Property Getter for the DataSet Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetDataSet: TClientDataSet;
begin
  Result := FDataSet;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetFetchOnDemand 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the FetchOnDemand Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the FetchOnDemand property.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetFetchOnDemand: Boolean;
begin
  Result := FFetchOnDemand;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetFilterFirst 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the FilterFirst Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the FilterFirst property.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  25/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetFilterFirst: Boolean;
begin
  Result := FFilterFirst;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetListViewClass
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the ListViewClass Property.
  Exceptions     : None
  Description    : Property Getter for the ListViewClass Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetListViewClass: TPPWListViewClassName;
begin
  Result := FListViewClass;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetMasterDataModule 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the MasterDataModule Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the MasterDataModule property.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetMasterDataModule: IPPWFrameWorkDataModule;
begin
  Result := FMasterDataModule;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnCreateListView 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the OnCreateListView Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the OnCreateListView Property
  History        :                                                          
                                                                            
  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnCreateListView: TPPWFrameWorkCreateFormEvent;
begin
  Result := FOnCreateListView;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnCreateRecordView
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the OnCreateRecordView Property.
  Exceptions     : None
  Description    : Property Getter for the OnCreateRecordView Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnCreateRecordView: TPPWFrameWorkCreateFormEvent;
begin
  Result := FOnCreateRecordView;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnFilter
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the OnFilter Property.
  Exceptions     : None
  Description    : Property Getter for the OnFilter Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnFilter: TPPWFrameWorkOnFilterEvent;
begin
  Result := FOnFilter;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnLinkListView 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the OnLinkListView Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the OnLinkListView Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnLinkListView: TPPWFrameWorkLinkListViewEvent;
begin
  Result := FOnLinkListView;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnLinkRecordView
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the OnLinkRecordView Property.
  Exceptions     : None
  Description    : Property Getter for the OnLinkRecordView Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnLinkRecordView: TPPWFrameWorkLinkRecordViewEvent;
begin
  Result := FOnLinkRecordView;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnSort
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the OnSort Property.
  Exceptions     : None
  Description    : Property Getter for the OnSort Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnSort: TPPWFrameWorkSortEvent;
begin
  Result := FOnSort;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetRecordViewClass
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the RecordViewClass Property.
  Exceptions     : None
  Description    : Property Getter for the RecordViewClass Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetRecordViewClass: TPPWRecordViewClassName;
begin
  Result := FRecordViewClass;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetRegistered 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the Registered Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the Registered Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetRegistered: Boolean;
begin
  Result := FRegistered;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetVisible 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : The Current Value of the Visible Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the Visible Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetVisible: Boolean;
begin
  Result := fVisible;
end;

procedure TPPWFrameWorkDataModule.LinkToListView(
  var aListView: IPPWFrameWorkListView);
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'LinkToListView' );
  {$ENDIF}

  if ( Assigned( aListView ) ) then
  begin
    if ( Assigned( aListView.DataSource ) ) then
    begin
      aListView.DataSource.DataSet := Self.DataSet;
    end;

    if ( Assigned( FOnLinkListView ) ) then
    begin
      FOnLinkListView( Self, aListView );
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'LinkToListView' );
  {$ENDIF}
end;

procedure TPPWFrameWorkDataModule.LinkToRecordView(
  var aRecordView : IPPWFrameWorkRecordView;
  const aRecordViewMode : TPPWFrameWorkRecordViewMode );
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'LinkToRecordView' );
  {$ENDIF}

  if ( Assigned( aRecordView ) ) then
  begin
    aRecordView.CloneDataSetFromDataModule;
    aRecordView.SetMode( aRecordViewMode );

    if ( Assigned( FOnLinkRecordView ) ) then
    begin
      FOnLinkRecordView( Self, aRecordView );
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'LinkToRecordView' );
  {$ENDIF}
end;

procedure TPPWFrameWorkDataModule.Notification(aComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification( aComponent, Operation );

  if ( Operation = opRemove ) and ( aComponent = DataSet ) then
  begin
    DataSet := Nil;
  end
  else if ( Operation = opRemove ) and ( aComponent = RecordDataSet ) then
  begin
    RecordDataSet := Nil;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.OpenDataSet 
  Author         : Stefaan Lesage
  Arguments      : aDataSet - The DataSet that must be Opened.
  Return Values  : None
  Exceptions     : If an Exception occored when opening the DataSet, this
                   procedure will raise an EPPWFrameWorkError exception with
                   the message generated by the Exception.
  Description    : This method will open the DataSet passed to the method
                   and will show an SQLWait cursor to indicate that it
                   is busy with the process.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.OpenDataSet(aDataSet: TDataSet);
var
  CursorRestorer : IPPWRestorer;
begin
  CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );
  try
    try
      aDataSet.Open;
    except
      on E: Exception do
      begin
        Raise EPPWFrameWorkError.Create( Format( rsPPWFrameWorkOpenDataSetFailed, [ aDataSet.Name, E.Message ] ) );
      end;
    end;
  finally
    CursorRestorer := Nil;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.OpenDataSets 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to open all the necessary
                   DataSets which are on our DataModule.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.OpenDataSets;
var
  CursorRestorer    : IPPWRestorer;
  aFrameWorkDataSet : IPPWFrameWorkDataSet;
  lcv               : Integer;
  IsVisibleDetail   : Boolean;
begin
  {$IFDEF CODESITE}
  csFWController.EnterMethod( Self, 'OpenDataSets' );
  {$ENDIF}

  CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  if Not ( AsSelection ) then
  begin
    IsVisibleDetail := ( Visible ) and ( MasterDataModule <> Nil );
  
    { DataSets should only be openend at Run Time }
    if not ( csLoading   in ComponentState ) and
       not ( csDesigning in ComponentState ) then
    begin
      for lcv := 0 to Pred( ComponentCount ) do
      begin
        { If the DataSet implements the IPPWFrameWorkDataSet we can
          is that interface to check for the AutoOpen property, otherwise
          we should use the RTTI information }
        if ( Supports( Components[ lcv ], IPPWFrameWorkDataSet, aFrameWorkDataSet ) ) then
        begin
          if ( aFrameWorkDataSet.AutoOpen ) then
          begin
            OpenDataSet( TDataSet( Components[ lcv ] ) );
          end
          else
          begin
            { With FetchOnDemand we should check if this datamodule
              is't a visible detail datamodule, in that case we should
              open the main dataset even if its AutoOpen property is
              False }
            if ( IsVisibleDetail ) and
               ( Components[ lcv ] = DataSet ) and
               ( FetchOnDemand ) then
            begin
              OpenDataSet( TDataSet( Components[ lcv ] ) );
            end;
          end;
        end

        { Check if the component is a TDataSet descendant }
        else if ( Components[ lcv ] is TDataSet ) then
        begin
          { Now check if it has an AutoOpen property }
          if ( IsPublishedProp( Components[ lcv ], 'AutoOpen' ) ) then
          begin
            { call the OpenDataSet method if that property is True }
            if ( GetOrdProp( Components[ lcv ], 'AutoOpen' ) = 1 ) then
            begin
              OpenDataSet( TDataSet( Components[ lcv ] ) );
            end
            else
            begin
              { With FetchOnDemand we should check if this datamodule
                is't a visible detail datamodule, in that case we should
                open the main dataset even if its AutoOpen property is
                False } 
              if ( IsVisibleDetail ) and
                 ( Components[ lcv ] = DataSet ) and
                 ( FetchOnDemand ) then
              begin
                OpenDataSet( TDataSet( Components[ lcv ] ) );
              end;
            end;
          end
          else
          begin
            { If the DataSet has no AutoOpen property only call the
              OpenDataSet method if it isn't a StoredProcedure }
            if Not ( IsPublishedProp( Components[ lcv ], 'ProcedureName' ) ) and
               Not ( IsPublishedProp( Components[ lcv ], 'StoredProcName' ) ) then
            begin
              OpenDataSet( TDataSet( Components[ lcv ] ) );
            end;
          end;
        end;
      end;
    end;
  end;

  {$IFDEF CODESITE}
  csFWController.ExitMethod( Self, 'OpenDataSets' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.RegisterSelf 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to Register the DataModule
                   with the PPWController.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.RegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'TPPWFrameWorkDataModule.RegisterSelf' );
  csFWDataModule.SendString( 'DataModule.ClassName', Self.ClassName );
  {$ENDIF}
  if ( Assigned( PPWController ) ) then
  begin
    PPWController.RegisterFrameWorkClass( TPPWFrameWorkDataModuleClass( Self.Classtype ) );
  end;
  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'TPPWFrameWorkDataModule.RegisterSelf' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetAutoDestroy
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the AutoDestroy Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AutoDestroy Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetAutoDestroy(const Value: Boolean);
begin
  if ( Value <> FAutoDestroy ) then
  begin
    FAutoDestroy := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetAutoOpenDataSets 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the AutoOpenDataSets Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AutoOpenDataSets Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetAutoOpenDataSets(const Value: Boolean);
begin
  if ( Value <> FAutoOpenDataSets ) then
  begin
    FAutoOpenDataSets := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetDataSet 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the DataSet Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the DataSet Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetDataSet(const Value: TClientDataSet);
begin
  if ( Value <> FDataSet ) then
  begin
    if ( FDataSet <> nil ) then
    begin
      FDataSet.RemoveFreeNotification( Self );
    end;

    FDataSet := Value;

    if ( FDataSet <> nil ) then
    begin
      FDataSet.FreeNotification( Self );
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetFetchOnDemand
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the FetchOnDemand Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the FetchOnDemand Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetFetchOnDemand(const Value: Boolean);
begin
  if ( Value <> FetchOnDemand ) then
  begin
    FFetchOnDemand := Value;

    if ( FetchOnDemand = False ) and
       ( AutoOpenDataSets ) then
    begin
      OpenDataSets;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetFilterFirst 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the FilterFirst Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the FilterFirst Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetFilterFirst(const Value: Boolean);
begin
  if ( Value <> FFilterFirst ) then
  begin
    FFilterFirst := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetListViewClass
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the ListViewClass Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the ListViewClass Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetListViewClass(
  const Value: TPPWListViewClassName);
begin
  if ( Value <> FListViewClass ) then
  begin
    FListViewClass := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetMasterDataModule 
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the MasterDataModule property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the MasterDataModule property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetMasterDataModule(
  const Value: IPPWFrameWorkDataModule);
begin
  if ( Value <> FMasterDataModule ) then
  begin
    FMasterDataModule := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetName
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the Name Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the Name Property.  We needed this
                   since changing the name should unregister the old name
                   and register the new one with the PPWController.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetName(const Value: TComponentName);
begin
  if ( Value <> Name ) then
  begin
    if Not ( csLoading in ComponentState ) and
           ( csDesigning in ComponentState ) and
           ( Registered ) then
    begin
      UnRegisterSelf;
    end;

    Inherited SetName( Value );

    if Not ( csLoading in ComponentState ) and
           ( csDesigning in ComponentState ) and
           ( Registered ) then
    begin
      RegisterSelf;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnCreateListView
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnCreateListView Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnCreateListView Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnCreateListView(
  const Value: TPPWFrameWorkCreateFormEvent);
begin
  FOnCreateListView := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnCreateRecordView
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnCreateRecordView Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnCreateRecordView Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnCreateRecordView(
  const Value: TPPWFrameWorkCreateFormEvent);
begin
  FOnCreateRecordView := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnFilter
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnFilter Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnFilter Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnFilter(const Value: TPPWFrameWorkOnFilterEvent);
begin
  FOnFilter := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnLinkListView
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnLinkListView Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnLinkListView Property
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  25/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnLinkListView(
  const Value: TPPWFrameWorkLinkListViewEvent);
begin
  FOnLinkListView := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnLinkRecordView
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnLinkRecordView Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnLinkRecordView Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnLinkRecordView(
  const Value: TPPWFrameWorkLinkRecordViewEvent);
begin
  FOnLinkRecordView := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnSort
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnSort Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnSort Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnSort(const Value: TPPWFrameWorkSortEvent);
begin
  FOnSort := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetRecordViewClass
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the RecordViewClass Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the RecordViewClass Property
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetRecordViewClass(
  const Value: TPPWRecordViewClassName);
begin
  if ( Value <> FRecordViewClass ) then
  begin
    FRecordViewClass := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetRegistered
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the Registered Property
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the Registered Property
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  22/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetRegistered(const Value: Boolean);
begin
  if ( Value <> FRegistered ) then
  begin
    FRegistered := Value;

    if Not ( csLoading in ComponentState ) and
           ( csDesigning in ComponentState ) then
    begin
      if ( Registered ) then
      begin
        RegisterSelf;
      end
      else
      begin
        UnRegisterSelf;
      end;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetVisible
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the Visible Property.                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Property Setter for the Visible Property.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetVisible(const Value: Boolean);
begin
  if ( Value <> FVisible ) then
  begin
    FVisible := Value;

    if ( Visible and AutoOpenDataSets ) then
    begin
      Try
        OpenDataSets;
      Except
        Raise;
      end;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.Sort 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will trigger the OnSort event, so the developper
                   can put the code for the acutal sorting in that event.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.Sort( const Fields : String );
begin
  if ( Assigned ( FOnSort ) ) then
  begin
    FOnSort( Self, Fields );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.UnRegisterSelf
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to Unregister the DataModule
                   with the PPWController.
  History        :

  Date         By                   Description
  ----         --                   -----------
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.UnRegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'UnRegisterSelf' );
  csFWDataModule.SendString( 'DataModule.ClassName', Self.ClassName );
  {$ENDIF}

  if ( Assigned( PPWController ) ) then
  begin
    PPWController.UnRegisterFrameWorkClass( TPPWFrameWorkDataModuleClass( Self.Classtype ) );
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'UnRegisterSelf' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.View 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to open the RecordView form and
                   allow the user to View the Current record.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.View;
var
  Allow : Boolean;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'View' );
  {$ENDIF}

  Allow := CanOpenNewRecordView;

  DoBeforeOpenRecordView( Self, rvmView, Allow );

  if ( Allow ) then
  begin
    if ( Assigned( PPWController ) ) then
    begin
      PPWController.ShowRecordViewForDataModule( Self, rvmView );
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'View' );
  {$ENDIF}
end;

function TPPWFrameWorkDataModule._AddRef: Integer;
begin
  Result := InterlockedIncrement( FRefCount );
end;

function TPPWFrameWorkDataModule._Release: Integer;
{$IFDEF CODESITE}
const
  csRefCountRechedZero : String =
    'Reference Count for %s %s(%s) reached 0, so it is getting Destroyed ';
var
  sDataModuleType : String;
{$ENDIF}
begin
  Result := InterlockedDecrement( FRefCount );

  if not ( csLoading in ComponentState ) and
     not ( csDesigning in ComponentState ) then
  begin
    if ( Result = 0 ) and ( AutoDestroy ) then
    begin
      {$IFDEF CODESITE}
      if ( Assigned( Self.ListViewDataModule ) ) then
      begin
        sDataModuleType := 'RecordViewDataModule';
      end
      else
      begin
        sDataModuleType := 'ListViewDataModule';
      end;
      csFWDataModule.SendMsg   ( Format( csRefCountRechedZero, [ sDataModuleType,
                                                                 Self.ClassName,
                                                                 Self.Name ] ) );
      csFWDataModule.SendObject( 'Instance', Self );
      {$ENDIF}
      Free;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnCancelFilter
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Current Value for the OnCancelFilter Property.                                                     
  Exceptions     : None
  Description    : Property Getter for the OnCancelFilter Property.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  29/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnCancelFilter: TNotifyEvent;
begin
  Result := FOnCancelFilter;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnCancelFilter 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnCancelFilter property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnCancelFilter Property.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  29/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnCancelFilter(
  const Value: TNotifyEvent);
begin
  FOnCancelFilter := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetKeyFields 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Current Value for the KeyFields Property.
  Exceptions     : None
  Description    : Property Getter for the KeyFields Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetKeyFields: string;
begin
  Result := FKeyFields;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetKeyFields 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the KeyFields property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the KeyFields Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetKeyFields(const Value: string);
begin
  if ( Value <> FKeyFields ) then
  begin
    FKeyFields := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetFiltered 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Current Value for the Filtered Property.
  Exceptions     : None
  Description    : Property Getter for the Filtered Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetFiltered: Boolean;
begin
  Result := FFiltered;   
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetDetailDataModules 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the DetailDataModules property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the DetailDataModules Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetDetailDataModules(
  const Value: TPPWFrameWorkDataModuleDetails);
begin
  FDetailDataModules.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetDetailDataModules 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Current Value for the DetailDataModules Property.
  Exceptions     : None
  Description    : Property Getter for the DetailDataModules Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetDetailDataModules: TPPWFrameWorkDataModuleDetails;
begin
  Result := FDetailDataModules;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetRecordViews 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Current Value for the RecordViews Property.
  Exceptions     : None
  Description    : Property Getter for the RecordViews Property.
  History        :

  Date         By                   Description                                    
  ----         --                   -----------                                    
  29/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetRecordViews: TInterfaceList;
begin
  Result := FRecordViews;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetListView 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Current Value for the ListView Property.
  Exceptions     : None
  Description    : Property Getter for the ListView Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetListView: IPPWFrameWorkListView;
begin
  Result := FListView;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetListView 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the ListView property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the ListView Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetListView(
  const Value: IPPWFrameWorkListView);
begin
  if ( Value <> FListView ) then
  begin
    FListView := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetOnFilterDetail 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Current Value for the OnFilterDetail Property.
  Exceptions     : None
  Description    : Property Getter for the OnFilterDetail Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetOnFilterDetail: TPPWFrameWorkFilterDetailEvent;
begin
  Result := FOnFilterDetail;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetOnFilterDetail 
  Author         : Stefaan Lesage
  Arguments      : Value - New Value for the OnFilterDetail property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OnFilterDetail Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetOnFilterDetail(
  const Value: TPPWFrameWorkFilterDetailEvent);
begin
  FOnFilterDetail := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.DoFilterDetail
  Author         : Stefaan Lesage
  Arguments      : KeyFields  - The fields on which the Detail must be
                                firltered
                   Values     - The Values that must be used to filter the
                                Detail.
                   Expression - The Expression that can be used to filter the
                                Details.
  Return Values  : None
  Exceptions     : None
  Description    : This method will actually call the OnFilterDetail event
                   if one was assigned.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.DoFilterDetail( KeyFields: String;
  KeyValues: Variant; Expression : String );
var
  aCursorRestorer : IPPWRestorer;
begin
  { Change the cursor to crHourGlass so the user knows the system is Busy }
  aCursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  if ( Assigned( FOnFilterDetail ) ) then
  begin
    FOnFilterDetail( Self, KeyFields, KeyValues, Expression );
  end
  else
  begin
    { If No Event assigned then we should build our own custom filter }
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.DoSort 
  Author         : Stefaan Lesage
  Arguments      : Fields - The Fields on which the data must be sorted.                                                     
  Return Values  : None
  Exceptions     : None
  Description    : OnSort event dispatcher method.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  03/12/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.DoSort(Fields: String);
begin
  if ( Assigned( FOnSort ) ) then
  begin
    FOnSort( Self, Fields );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.DefineProperties 
  Author         : Stefaan Lesage
  Arguments      : Filer - The Filer that is used for streaming in or out
                           the properties.                                                     
  Return Values  : None
  Exceptions     : None
  Description    : This method Designates methods for storing an object's
                   unpublished data on a stream such as a form file.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  29/11/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.DefineProperties(Filer: TFiler);
var
  Ancestor: TPPWFrameWorkDataModule;

  function WriteData: Boolean;
  begin
    if ( Ancestor = Nil ) then
    begin
      { The ancestor of the DataModule also has a DetailDataModules Collection }
      Result := True;
//      Result := DetailDataModules.Count > 0;
    end
    else
    begin
      Result := //( State = dstCustomised ) and
        ( Not CollectionsEqual(DetailDataModules, Ancestor.DetailDataModules, Self, Ancestor ) );
    end
  end;

begin
  inherited DefineProperties(Filer);

  Ancestor := TPPWFrameWorkDataModule(Filer.Ancestor);

  Filer.DefineProperty('DetailDataModules', ReadParamData, WriteParamData, WriteData);
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.ReadParamData 
  Author         : Stefaan Lesage
  Arguments      : Reader - The Reader object that is used to Read in the DFM.                                                     
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to read in the DetailDataModules
                   from the DFM.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.ReadParamData(Reader: TReader);
begin
  DetailDataModules.Clear;
  Reader.ReadValue;
  Reader.ReadCollection(DetailDataModules);
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.WriteParamData
  Author         : Stefaan Lesage
  Arguments      : Writer - The Writer object that is used to wrote out the
                            DFM.
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to write out the DetailDataModules
                   from the DFM.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.WriteParamData(Writer: TWriter);
begin
  Writer.WriteCollection(DetailDataModules);
end;

function TPPWFrameWorkDataModule.GetOnBeforeOpenRecordView: TPPWFrameWorkBeforeOpenRecordViewEvent;
begin
  Result := FOnBeforeOpenRecordView;
end;

procedure TPPWFrameWorkDataModule.setOnBeforeOpenRecordView(
  const Value: TPPWFrameWorkBeforeOpenRecordViewEvent);
begin
  FOnBeforeOpenRecordView := Value;
end;

procedure TPPWFrameWorkDataModule.DoBeforeOpenRecordView(Sender: TObject;
  RecordViewMode : TPPWFrameWorkRecordViewMode; var AllowRecordView: Boolean);
begin
  if ( Assigned( FOnBeforeOpenRecordView ) ) then
  begin
    FOnBeforeOpenRecordView( Self, RecordViewMode, AllowRecordView );
  end
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetAsSelection
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The current Value of the AsSelection property.
  Exceptions     : None
  Description    : Property Getter for the AsSelection property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkDataModule.GetAsSelection: Boolean;
begin
  Result := FAsSelection;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.SetAsSelection
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the AsSelection property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AsSelection property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.SetAsSelection(const Value: Boolean);
begin
  if ( Value <> FAsSelection ) then
  begin
    FAsSelection := False;
  end;
end;

function TPPWFrameWorkDataModule.GetOnBeforeCreateDetailDataModule: TPPWFrameWorkBeforeCreateDetailEvent;
begin
  Result := FOnBeforeCreateDetailDataModule;
end;

procedure TPPWFrameWorkDataModule.SetOnBeforeCreateDetailDataModule(
  const Value: TPPWFrameWorkBeforeCreateDetailEvent);
begin
  FOnBeforeCreateDetailDataModule := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.GetMasterRecordView 
  Author         : Wim Lambrechts                                          
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This function returns an interface to the recordview for which
                   this datamodule was created as detail. It allows to obtain
                   values from the master record to be added in the detail
                   (AfterInsert event of cdsMain)
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/12/2002   Wim Lambrechts       Initial creation of the Procedure.                  
 *****************************************************************************}
function TPPWFrameWorkDataModule.GetMasterRecordView: IPPWFrameWorkRecordView;
begin
  result := FMasterRecordView;
end;


function TPPWFrameWorkDataModule.GetOnAfterCreateDetailDataModule: TPPWFrameWorkAfterCreateDetailEvent;
begin
  result := FOnAfterCreateDetailDataModule;
end;

procedure TPPWFrameWorkDataModule.SetOnAfterCreateDetailDataModule(
  const Value: TPPWFrameWorkAfterCreateDetailEvent);
begin
  FOnAfterCreateDetailDataModule := Value;
end;

function TPPWFrameWorkDataModule.GetForeignKeys: String;
begin
  result := FForeignKeys;
end;

procedure TPPWFrameWorkDataModule.SetForeignKeys(const Value: String);
begin
  FForeignKeys := Value;
end;

function TPPWFrameWorkDataModule.ClientDatasetByName(
  const Name: String): TClientDataset; //WLUPDATE
var
  comp :TComponent;
begin
  comp := Self.FindComponent (Name);
  if Assigned(comp) and (comp is TClientDataset) then
    result := TClientDataset (comp)
  else
    result := nil;
end;

function TPPWFrameWorkDataModule.FrameWorkDatamoduleClassName: String;
begin
  result := self.classname;
end;

function TPPWFrameWorkDataModule.GetRecordDataset: TClientDataset;
begin
  Result := FRecordDataSet;
end;

function TPPWFrameWorkDataModule.GetListViewDataModule: IPPWFrameWorkDataModule;
begin
  result := FListViewDataModule;
end;

procedure TPPWFrameWorkDataModule.SetListViewDataModule(
  const Value: IPPWFrameWorkDataModule);
begin
  FListViewDataModule := Value;
end;

procedure TPPWFrameWorkDataModule.SetMasterRecordView(
  const Value: IPPWFrameWorkRecordView);
begin
  FMasterRecordView := Value;
end;

procedure TPPWFrameWorkDataModule.SetRecordDataSet(
  const Value: TClientDataset);
begin
  if ( Value <> FRecordDataSet ) then
  begin
    if ( FRecordDataSet <> nil ) then
    begin
      FRecordDataSet.RemoveFreeNotification( Self );
    end;

    FRecordDataSet := Value;

    if ( FRecordDataSet <> nil ) then
    begin
      FRecordDataSet.FreeNotification( Self );
    end;
  end;
end;

function TPPWFrameWorkDataModule.GetTableName: String;
begin
  Result := FTableName;
end;

procedure TPPWFrameWorkDataModule.SetTableName(const Value: String);
begin
  if ( Value <> FTableName ) then
  begin
    FTableName := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.BuildFilterExpressionForDetail
  Author         : Stefaan Lesage
  Arguments      : aMasterDataModule - Interface to the DataModule that will
                                       be used as the Master in a
                                       Master-Detail RelationShip.
                   aDetailDataModule - Interface to the DataModule that will
                                       be used as the Detail in a
                                       Master-Detail RelationShip.
                   aForeignKeys      - A string representing the List of
                                       Foreign keys that must be used to
                                       build the RelationShip.
                   aUseRecordDataSet - A boolean which will be used to
                                       determine if the KeyField Values
                                       must be taken from the RecordDataSet.
  Return Values  : The Filter expression that must be used to filter the
                   Detail records in case of a Master - Detail relation.
  Exceptions     : None
  Description    : This function will be used to build the Filter Expression
                   that must be used when filtering the Detail Records.
  History        :

  Date         By                   Description
  ----         --                   -----------
  01/09/2003   sLesage              Fixed a bug.  The KeyField Values should
                                    be taken from the RecordDataSet and not
                                    from the List DataSet.
  22/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkDataModule.BuildFilterExpressionForDetail(
  AMasterDataModule : IPPWFrameWorkDataModule;
  ADetailDataModule : IPPWFrameWorkDataModule;
  aForeignKeys      : String;
  aUseRecordDataSet : Boolean = False ): String;
var
  aExpression    : String;
  aMasterDataSet : TClientDataSet;
  aFieldList     : TList;
  aFieldValues   : Variant;
  aTableName     : String;
  lcv            : Integer;
  Value          : Variant;
  ValStr         : String;
begin
  aExpression := '';

  if ( Assigned( aMasterDataModule ) ) and
     ( Assigned( aDetailDataModule ) ) then
  begin
    if ( aUseRecordDataSet ) then
    begin
      aMasterDataSet := AMasterDataModule.RecordDataSet;
    end
    else
    begin
      aMasterDataSet := AMasterDataModule.DataSet;
    end;

    aTableName     := ADetailDataModule.TableName;

    if ( aTableName <> '' ) then
    begin
      aTableName := aTableName + '.';
    end;

    aFieldList := TList.Create;
    Try
      aMasterDataSet.GetFieldList( aFieldList, aForeignKeys );

      { Create the VarArray which will be used to pass the values of all the
        fields to the Event Handler }
      aFieldValues := VarArrayCreate( [ 0, aFieldList.Count ], varVariant );

      { Add the Value of the Field to the VarArray }
      for lcv := 0 to Pred( aFieldList.Count ) do
      begin
        aFieldValues[ lcv ] := TField( aFieldList[ lcv ] ).Value;
      end;

      for lcv := 0 to Pred( aFieldList.Count ) do
      begin
        if ( aFieldList.Count = 1 ) and not VarIsArray( aFieldValues ) then
        begin
          Value := aFieldValues;
        end
        else
        begin
          Value := aFieldValues[lcv];
        end;

        case TField(aFieldList[lcv]).DataType of

          ftString, ftFixedChar, ftWideString, ftGUID:
              ValStr := Format('''%s''',[VarToStr(Value)]);

          ftDate, ftTime, ftDateTime, ftTimeStamp:
            ValStr := Format('''%s''',[VarToStr(Value)]);

          ftSmallint, ftInteger, ftWord, ftAutoInc, ftBoolean, ftFloat, ftCurrency, ftBCD, ftLargeInt, ftFMTBcd:
            ValStr := VarToStr(Value);
        else
          DatabaseErrorFmt(SBadFieldType, [TField(aFieldList[lcv]).FieldName]);
        end;

        if aExpression <> '' then
          aExpression := aExpression + ' and ';    { Do not localize }
        if VarIsNull(Value) then
          aExpression := aExpression + Format('%s%s IS NULL',[aTableName, TField(aFieldList[lcv]).FieldName])  { Do not localize }
        else
          aExpression := aExpression + Format('%s%s=%s',[aTableName, TField(aFieldList[lcv]).FieldName, ValStr]);
      end;

      result := aExpression;

      {$IFDEF CODESITE}
      csFWDataModule.SendString( 'DetailExpression', Result );
      {$ENDIF}
    finally
      FreeAndNil (aFieldList);
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.RefreshListDataSet
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to refresh the Data in the
                   List DataSet.  If the List is used aa Detail of another
                   DataModule we should make sure that the DetailFilter
                   gets Rebuild.
  History        :

  Date         By                   Description
  ----         --                   -----------
  01/09/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.RefreshListDataSet;
var
  CursorRestorer : IPPWRestorer;
  lcv            : Integer;
  aForeignKeys   : String;
  aForeignKeyValues : Variant;
  Expression     : String;
begin
  {$IFDEF CODESITE}
  csFWDataModule.EnterMethod( Self, 'RefreshListDataSet' );
  csFWDataModule.SendString( 'Self.ClassName', Self.ClassName );
  csFWDataModule.SendString( 'Self.Name', Self.Name );
  {$ENDIF}

  if ( Assigned( DataSet ) ) and
     ( DataSet.ChangeCount = 0 ) then
  begin
    CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );
    DataSet.DisableControls;
    try
      DataSet.Close;
      if ( Assigned( MasterDataModule ) ) then
      begin
        { If the DataModule was a detail of another datamodule, we should
          rebuild the Detail Filter.  In case a new master record was added
          and the detail were refreshed, the DetailFilter was empty.  This
          approach will solve that problem. }
        for lcv := 0 to Pred( MasterDataModule.DetailDataModules.Count ) do
        begin
          if ( MasterDataModule.DetailDataModules.Items[ lcv ].DataModuleClass = Self.ClassName ) then
          begin
            aForeignKeys := MasterDataModule.DetailDataModules.Items[ lcv ].ForeignKeys;
          end;
        end;

        if ( aForeignKeys <> '' ) then
        begin
          aForeignKeyValues := CreateFieldValuesVarArray( MasterDataModule.RecordDataSet, aForeignKeys );
        end;

        Expression := BuildFilterExpressionForDetail( MasterDataModule, Self, aForeignKeys, True );
        DoFilterDetail( aForeignKeys, aForeignKeyValues, Expression );
      end;
      DataSet.Open;
    finally
      DataSet.EnableControls;
    end;
  end;

  {$IFDEF CODESITE}
  csFWDataModule.ExitMethod( Self, 'RefreshListDataSet' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkDataModule.RefreshRecordDataSet
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to refresh the record in the
                   RecordDataSet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/10/2003   sLesage              Modified the code so it now supports
                                    multiple fields in the KeyFields property.
  01/09/2003   sLesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkDataModule.RefreshRecordDataSet;
var
  CursorRestorer  : IPPWRestorer;
  CurrentKeyValue : Variant;
  Expression      : String;
begin
  if ( Assigned( RecordDataSet ) ) and
     ( RecordDataset.Active ) then
  begin
    CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

    { Get the Value of the KeyField from the current record }
    CurrentKeyValue := CreateFieldValuesVarArray( RecordDataSet, KeyFields );


    Expression := BuildFilterExpressionForDetail( Self, Self, KeyFields, True );

    DoFilterRecord( KeyFields, CurrentKeyValue, Expression );
  end;
end;

function TPPWFrameWorkDataModule.CreateFieldValuesVarArray(
  aDataSet: TDataSet; aFields: String): Variant;
var
  aFieldList : TList;
  lcv        : Integer;
begin
  aFieldList := TList.Create;
  try
    aDataSet.GetFieldList( aFieldList, aFields );

    Result := VarArrayCreate( [ 0, Pred( aFieldList.Count ) ], varVariant );

    for lcv := 0 to Pred( aFieldList.Count ) do
    begin
      Result[ lcv ] := TField( aFieldList[ lcv ] ).Value;
    end;

  finally
    FreeAndNil( aFieldList );
  end;
end;

procedure TPPWFrameWorkDataModule.DoApplyPendingUpdates;
begin
  //
end;

procedure TPPWFrameWorkDataModule.DoFilterRecord(KeyFields: String;
  KeyValues: Variant; Expression: String);
var
  aCursorRestorer : IPPWRestorer;
begin
  { Change the cursor to crHourGlass so the user knows the system is Busy }
  aCursorRestorer := TPPWCursorRestorer.Create( crHourGlass );

  if ( Assigned( FOnFilterRecord ) ) then
  begin
    FOnFilterRecord( Self, KeyFields, KeyValues, Expression );
  end
  else
  begin
    { If No Event assigned then we should build our own custom filter }
  end;
end;

function TPPWFrameWorkDataModule.GetOnFilterRecord: TPPWFrameWorkFilterDetailEvent;
begin
  Result := FOnFilterRecord;
end;

procedure TPPWFrameWorkDataModule.SetOnFilterRecord(
  const Value: TPPWFrameWorkFilterDetailEvent);
begin
  FOnFilterRecord := Value;
end;

function TPPWFrameWorkDataModule.GetName: TComponentName;
begin
  Result := Inherited Name;
end;

function TPPWFrameWorkDataModule.GetRecordViewDataModules: TInterfaceList;
begin
  Result := FRecordViewDataModules;
end;

{*****************************************************************************
  Property Getter for the CanOpenNewRecordView property.

  @Name       TPPWFrameWorkDataModule.GetCanOpenNewRecordView
  @author     slesage
  @param      None
  @return     Returns true if MaxEntityRecordViews on the PPWController is set
              to 0.  If it is set to any other number, then it will return
              true if there are less RecordViews open for the DataModule than
              indicated by the MaxEntityRecordViews property, otherwise it
              will return False.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkDataModule.GetCanOpenNewRecordView: Boolean;
begin
  if PPWController.MaxEntityRecordViews = 0 then
  begin
    Result := True;
  end
  else
  begin
    Result := Self.RecordViewDataModules.Count < PPWController.MaxEntityRecordViews;
  end;
end;

end.
