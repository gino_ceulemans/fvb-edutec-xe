object frmPPWFrameWorkPersistentFieldsWizard: TfrmPPWFrameWorkPersistentFieldsWizard
  Left = 192
  Top = 162
  ActiveControl = cxgrdFieldOptions
  BorderStyle = bsDialog
  Caption = 'PIF Persistent Fields Wizard'
  ClientHeight = 453
  ClientWidth = 706
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 412
    Width = 706
    Height = 41
    Align = alBottom
    TabOrder = 0
    object cxbtnCancel: TcxButton
      Left = 512
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object cxbtnFinish: TcxButton
      Left = 600
      Top = 8
      Width = 75
      Height = 25
      Action = acNextFinish
      TabOrder = 1
    end
  end
  object cxgrdFieldOptions: TcxGrid
    Left = 0
    Top = 0
    Width = 706
    Height = 412
    Align = alClient
    BevelKind = bkFlat
    BorderStyle = cxcbsNone
    TabOrder = 1
    RootLevelOptions.DetailTabsPosition = dtpTop
    OnFocusedViewChanged = cxgrdFieldOptionsFocusedViewChanged
    object cxgrdtblvListViewOptions: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = srcListFieldSettings
      DataController.KeyFieldNames = 'F_FIELD_ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      Styles.StyleSheet = cxssDefaultTableView
      object cxgrdtblvListViewOptionsF_FIELD_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_ID'
        Visible = False
        Options.Filtering = False
      end
      object cxgrdtblvListViewOptionsF_FIELD_NAME: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_NAME'
        Options.Editing = False
        Options.Filtering = False
        Options.Focusing = False
        Width = 140
      end
      object cxgrdtblvListViewOptionsF_FIELD_CAPTION: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_CAPTION'
        Options.Filtering = False
        Width = 427
      end
      object cxgrdtblvListViewOptionsF_FIELD_VISIBLE: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_VISIBLE'
        Options.Filtering = False
        Width = 56
      end
      object cxgrdtblvListViewOptionsF_FIELDINPK: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELDINPK'
        Options.Filtering = False
        Width = 67
      end
    end
    object cxgrdtblvRecordViewOptions: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = srcRecordFieldSettings
      DataController.KeyFieldNames = 'F_FIELD_ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      Styles.StyleSheet = cxssDefaultTableView
      object cxgrdtblvRecordViewOptionsF_FIELD_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_ID'
        Visible = False
        Options.Filtering = False
        Width = 84
      end
      object cxgrdtblvRecordViewOptionsF_FIELD_NAME: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_NAME'
        Options.Editing = False
        Options.Filtering = False
        Options.Focusing = False
        Width = 140
      end
      object cxgrdtblvRecordViewOptionsF_FIELD_CAPTION: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_CAPTION'
        Options.Filtering = False
        Width = 373
      end
      object cxgrdtblvRecordViewOptionsF_FIELD_VISIBLE: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_VISIBLE'
        Options.Filtering = False
        Width = 56
      end
      object cxgrdtblvRecordViewOptionsF_FIELD_REQUIRED: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_REQUIRED'
        Options.Filtering = False
        Width = 56
      end
      object cxgrdtblvRecordViewOptionsF_FIELD_READONLY: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_READONLY'
        Options.Filtering = False
        Width = 56
      end
    end
    object cxgrdlvlListFieldOptions: TcxGridLevel
      Caption = 'List Field Options'
      GridView = cxgrdtblvListViewOptions
      Options.DetailTabsPosition = dtpTop
    end
    object cxgrdlvlRecordFieldOptions: TcxGridLevel
      Caption = 'Record Field Options'
      GridView = cxgrdtblvRecordViewOptions
      Options.DetailTabsPosition = dtpTop
    end
  end
  object cxlfMain: TcxLookAndFeelController
    Kind = lfOffice11
    Left = 8
    Top = 416
  end
  object cdsListFieldSettings: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 416
    object cdsListFieldSettingsF_FIELD_ID: TAutoIncField
      FieldName = 'F_FIELD_ID'
    end
    object cdsListFieldSettingsF_FIELD_NAME: TStringField
      DisplayLabel = 'FieldName'
      FieldName = 'F_FIELD_NAME'
      Size = 50
    end
    object cdsListFieldSettingsF_FIELD_CAPTION: TStringField
      DisplayLabel = 'Caption'
      FieldName = 'F_FIELD_CAPTION'
      Size = 255
    end
    object cdsListFieldSettingsF_FIELD_VISIBLE: TBooleanField
      DisplayLabel = 'Visible'
      FieldName = 'F_FIELD_VISIBLE'
    end
    object cdsListFieldSettingsF_FIELDINPK: TBooleanField
      DisplayLabel = 'In PK ?'
      FieldName = 'F_FIELDINPK'
    end
  end
  object srcListFieldSettings: TDataSource
    DataSet = cdsListFieldSettings
    Left = 112
    Top = 416
  end
  object cdsRecordFieldSettings: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 416
    object cdsRecordFieldSettingsF_FIELD_ID: TAutoIncField
      FieldName = 'F_FIELD_ID'
    end
    object cdsRecordFieldSettingsF_FIELD_NAME: TStringField
      DisplayLabel = 'FieldName'
      FieldName = 'F_FIELD_NAME'
      Size = 50
    end
    object cdsRecordFieldSettingsF_FIELD_CAPTION: TStringField
      DisplayLabel = 'Caption'
      FieldName = 'F_FIELD_CAPTION'
      Size = 255
    end
    object BooleanField1: TBooleanField
      DisplayLabel = 'Visible'
      FieldName = 'F_FIELD_VISIBLE'
    end
    object cdsRecordFieldSettingsF_FIELD_REQUIRED: TBooleanField
      DisplayLabel = 'Required'
      FieldName = 'F_FIELD_REQUIRED'
    end
    object cdsRecordFieldSettingsF_FIELD_READONLY: TBooleanField
      DisplayLabel = 'ReadOnly'
      FieldName = 'F_FIELD_READONLY'
    end
  end
  object srcRecordFieldSettings: TDataSource
    DataSet = cdsRecordFieldSettings
    Left = 216
    Top = 416
  end
  object cxsrCxStyleRepository1: TcxStyleRepository
    Left = 40
    Top = 416
    PixelsPerInch = 96
    object cxsContent: TcxStyle
    end
    object cxsContentOdd: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
    object cxssDefaultTableView: TcxGridTableViewStyleSheet
      Styles.Content = cxsContent
      Styles.ContentEven = cxsContent
      Styles.ContentOdd = cxsContentOdd
      BuiltIn = True
    end
  end
  object alActionList1: TActionList
    Left = 152
    Top = 416
    object acNextFinish: TAction
      Caption = 'Next'
      OnExecute = acNextFinishExecute
      OnUpdate = acNextFinishUpdate
    end
  end
end
