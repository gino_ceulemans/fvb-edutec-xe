{*****************************************************************************
  Name           : Unit_PPWFrameWorkRecordView
  Author         : Lesage Stefaan
  Copyright      : (c) 2002 Peopleware
  Description    : This unit contains the Basic FrameWork RecordView.  This
                   form doesn't have any controls on it, so it should be fairly
                   easy to use it as a basic RecordView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  14/12/2004   sLesage              Completely reworked the UpdateControls part.
  29/11/2004   sLesage              Reworked the Constructor of the form.
  29/11/2004   sLesage              Added a property UseFocusController which
                                    can be used to Enable / Disalbe the default
                                    FrameWork FocusController.
  14/11/2003   sLesage              Fixed a bug in the UpdateControls.  When
                                    the form was shown for Delete purpose, it
                                    was still possible to change the contents
                                    of the controls.
  08/10/2003   sLesage              Added a PostDetailListViews method wich 
            g                        can be used to make sure the main DataSet on
                                    each DetalListView will get posted if this
                                    hasn't been done yet.
  16/09/2003   sLesage              Added a RefreshVisibleDetailIstViews
                                    method to the RecordView and its Interface.
  29/08/2003   sLesage              Added a DetailListViews property ( an
                                    InterfaceList ) which will contain a list
                                    of interfaces to the ListViews that have
                                    been docked as details into the RecordView.
  25/08/2003   sLesage              The UpdateControls method has been
                                    changed.  It will now also update all
                                    controls that the given Container control
                                    contains.
  15/07/2003   sLesage              UpdateControlLayout now also applies the
                                    ReadOnlyFont, RequiredFont and FocusFont
                                    to the current control.
  14/07/2003   sLesage              Fixed a bug in the CreateForDataModule
                                    method.
  17/03/2003   sLesage              Added code that will dispatch the
                                    WM_MASTERRECORDVIEWMODECHANGED Message
                                    when the mode of the recordview is changed.
  6/12/2002    sLesage              Added the BeforeUpdateControls event which
                                    can be used to set some fields to ReadOnly
                                    right before the UpdateControls method is
                                    executed.
  29/11/2002   sLesage              Cleaned up unnecessary Code and added
                                    Unit / Procedure Headers.
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkRecordView;

interface

uses
  { Delphi Units }
  Forms, Controls, Classes,
  { FrameWork Units }
  Unit_PPWFrameWorkClasses,
  Unit_PPWFrameWorkForm,
  Unit_PPWFrameWorkInterfaces,
  db, ExtCtrls, Contnrs,
  Unit_PPWFrameWorkDataModule, unit_PPWFrameWorkExceptions,
  dialogs, dbclient, windows;

type
  TPPWFrameWorkRecordView = class(TPPWFrameWorkForm, IPPWFrameWorkRecordView, IPPWFrameWorkControlledLayout )
  private
    { Private declarations }
    FReadOnlyComponents   : TComponentList;
    FBeforeUpdateControls : TNotifyEvent;
    FAfterUpdateControls  : TNotifyEvent; //added WL 27/12
    FOnAllDetailsCreated  : TNotifyEvent;
    FOnDetailListViewCreated: TPPWFrameWorkOnDetailListViewCreatedEvent;
    FMode                 : TPPWFrameWorkRecordViewMode;
    FFocusCtrl            : TPPWFrameWorkFocusControl;
    FRecordViewDatamodule : IPPWFrameWorkDataModule;

    FDetailListViews      : IInterfaceList;
    FDockedRecordViews    : IInterfaceList;
    FUseFocusController   : Boolean;
    function GetDockedRecordViews: IInterfaceList;
  protected
    { Property Getters }
    function GetAfterUpdateControls     : TNotifyEvent; virtual; //added WL 27/12
    function GetBeforeUpdateControls    : TNotifyEvent; virtual;
    function GetDetailListViews         : IInterfaceList; virtual;
    function GetFocusController         : TPPWFrameWorkFocusControl; virtual;
    function GetMode                    : TPPWFrameWorkRecordViewMode; virtual;
    function GetDetailName              : string; virtual;
    function GetForm                    : TCustomForm; virtual;
    function GetRecordViewDatamodule    : IPPWFrameWorkDatamodule; //WLUPDATE
    function GetOnAllDetailsCreated     : TNotifyEvent; virtual; //added WL 21/12
    function GetOnDetailListViewCreated : TPPWFrameWorkOnDetailListViewCreatedEvent; virtual; //added WL 28/01

    { Property Setters }
    procedure SetBeforeUpdateControls( const Value : TNotifyEvent ); virtual;
    procedure SetAfterUpdateControls( const Value : TNotifyEvent ); virtual; //added WL 27/12
    procedure SetOnAllDetailsCreated (const Value: TNotifyEvent); virtual; //added WL 21/12
    procedure SetOnDetailListViewCreated( const Value: TPPWFrameWorkOnDetailListViewCreatedEvent); virtual; //added WL 28/01
    procedure SetMode( const Value : TPPWFrameWorkRecordViewMode ); virtual;
    procedure SetUseFocusController(const Value: Boolean); virtual;

    { Methods }
    procedure AddReadOnlyComponent   ( aComponent : TComponent; aHandled : Boolean = False ); virtual;
    procedure DoModeChanged; virtual;
    procedure DoUpdateControls; virtual;
    procedure DoShow; override;
    procedure RegisterSelf; override;
    procedure ResetReadOnlyComponents; virtual;
    procedure UnRegisterSelf; override;
    procedure UpdateControlColor( Control : TControl ); virtual;
    procedure RemoveReadOnlyComponent( aComponent : TComponent; aHandled : Boolean = False ); virtual;
  public
    { Public declarations }
    constructor Create( aOwner : TComponent; aDataModule : IPPWFrameWorkDataModule = Nil  ); override;

    destructor Destroy; override;

    procedure Loaded; override;

    procedure LinkDataSourcesToDataSets; virtual;
    procedure CloneDataSetFromDataModule; virtual;
    procedure UpdateControls( Container : TWinControl ); virtual;
    procedure UpdateControlLayout( Control : TControl; ControlOptions : TPPWFrameWorkControlOptions ); virtual;
    procedure UpdateFormCaption; override;
    procedure RefreshRecordView (RecordViewMode: TPPWFrameWorkRecordViewMode); Virtual;//WLUPDATE
    procedure RefreshVisibleDetailListViews; virtual;
    procedure PostDetailListViews; virtual;

    property DetailName : String                        read GetDetailName;
    property Form       : TCustomForm                   read GetForm;
    property Mode       : TPPWFrameWorkRecordViewMode   read GetMode;
    property RecordViewDatamodule: IPPWFrameWorkDatamodule   read GetRecordViewDatamodule;
    property DetailListViews     : IInterfaceList            read GetDetailListViews;
    property DockedRecordViews   : IInterfaceList            read GetDockedRecordViews;
    property FocusController     : TPPWFrameWorkFocusControl read GetFocusController;
  published
    property Align;
    property Caption;
    property DataSource;
    property FormStyle;
    property Registered;

    property BeforeUpdateControls : TNotifyEvent  read GetBeforeUpdateControls
                                                  write SetBeforeUpdateControls;
    property AfterUpdateControls : TNotifyEvent   read GetAfterUpdateControls
                                                  write SetAfterUpdateControls;
    property OnAllDetailsCreated : TNotifyEvent   read GetOnAllDetailsCreated
                                                  write SetOnAllDetailsCreated;
    property OnDetailListViewCreated: TPPWFrameWorkOnDetailListViewCreatedEvent
      read GetOnDetailListViewCreated
      write SetOnDetailListViewCreated;

    property UseFocusController  : Boolean        read  FUseFocusController
                                                  write SetUseFocusController;
  end;

  TPPWFrameWorkRecordViewClass = class of TPPWFrameWorkRecordView;

var
  PPWFrameWorkRecordView: TPPWFrameWorkRecordView;

implementation

uses
  { Delphi Units }
  SysUtils, TypInfo, Graphics, StdCtrls, Messages,
  { FrameWork Units }
  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects, csIntf,
  {$ENDIF}
  Unit_PPWFrameWorkController, DBCtrls;

{$R *.dfm}

{ TPPWFrameWorkRecordView }

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.CloneDataSetFromDataModule 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to Clone the cursor of the DataSet
                   which is supplied on the associated DataModule.  The Main
                   DataSet on the DataModule is a TClientDataSet descendant so
                   it supports the CloneCursor method.  Still we had to
                   implement this method because we also needed to clone field
                   properties and we might need to clone events in the future
                   as well.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.CloneDataSetFromDataModule;
begin
  {$IFDEF CODESITE}
  csFWRecordView.EnterMethod( Self, 'CloneDataSetFromDataModule' );
  {$ENDIF}

  if ( Assigned( DataSource ) ) then
  begin
    DataSource.DataSet := FRecordViewDatamodule.RecordDataset;
  end;

  {$IFDEF CODESITE}
  csFWRecordView.ExitMethod( Self, 'CloneDataSetFromDataModule' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.Destroy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Overridden destructor in which we will Clean Up everyting.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

destructor TPPWFrameWorkRecordView.Destroy;
var
  Index       : Integer;
  aRecordView : IPPWFrameWorkRecordView;
begin
  {$IFDEF CODESITE}
  csFWRecordView.EnterMethod( Self, 'Destroy' );
  {$ENDIF}

  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    if Supports( Self, IPPWFrameWorkRecordView, aRecordView ) then
    begin
      Index := FrameWorkDataModule.RecordViews.IndexOf( aRecordView );
      if Index <> - 1 then
      begin
        FrameWorkDataModule.RecordViews.Delete( Index );
      end;
      if ( Assigned( FrameWorkDataModule.ListViewDataModule ) ) then
      begin
        Index := FrameWorkDataModule.ListViewDataModule.RecordViewDataModules.IndexOf( FrameWorkDataModule );
        if Index <> -1 then
        begin
          FrameWorkDataModule.ListViewDataModule.RecordViewDataModules.Remove( FrameWorkDataModule );
        end;
      end;
    end;
  end;

  if ( Assigned( FFocusCtrl ) ) then
  begin
    FFocusCtrl.OnLostFocus := Nil;
    FFocusCtrl.OnReceivedFocus := Nil;
    FreeAndNil( FFocusCtrl );
  end;

  { Clear the DetailListViews list, and set the Interface to nil, so that
    the TInterfaceList itself gets destroyed as well }
  if ( Assigned( FDetailListViews ) ) then
  begin
    FDetailListViews.Clear;
    FDetailListViews := Nil;
  end;

  if ( Assigned( FDockedRecordViews ) ) then
  begin
    FDockedRecordViews.Clear;
    FDockedRecordViews := Nil;
  end;

  FreeAndNil( FReadOnlyComponents );

  //FRecordViewDatamodule.ListView := nil; //to avoid an access violation when
  //closing a listview before closing the recordview: this is caused by the fact
  //that reference counting is not implemented in the listviewform
  //FreeAndNil( FDataSet ); //WLUPDATE

  inherited Destroy;

  {$IFDEF CODESITE}
  csFWRecordView.ExitMethod( Self, 'Destroy' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.DoShow
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Overridden DoShow method in which we will call the
                   UpdateFormCaption method, to display a descent caption
                   on the form.

  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.DoShow;
begin
  inherited DoShow;
//  UpdateFormCaption;
//  DoUpdateControls;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.GetDetailName 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : This function should return a string which will be
                   displayed in the caption of the form.                                                     
  Exceptions     : None
  Description    : This function will be used to add a custom string to the
                   caption of the form.  Implementation should be done in
                   descendant classes.  Most of the time, we will return the
                   value of some of the controls on the form, so the user knows
                   what information is displayed on the form based on the
                   caption of it.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkRecordView.GetDetailName: string;
begin
  Result := 'GetDetailName not implemented !';
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.GetMode 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Returns the Mode in which the Form has been opened
                           ( rvmView, rvmEdit, rvmAdd, rvmDelete ).
  Exceptions     : None
  Description    : Property Getter for the Mode Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkRecordView.GetMode: TPPWFrameWorkRecordViewMode;
begin
  Result := FMode;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.RegisterSelf 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to register the current form
                   with our FrameWork controller.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.RegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWRecordView.EnterMethod( Self, 'TPPWFrameWorkRecordView.RegisterSelf' );
  csFWRecordView.SendString( 'RecordView.ClassName', Self.ClassName );
  {$ENDIF}

  if ( Assigned( PPWController ) ) then
  begin
    PPWController.RegisterFrameWorkClass( TPPWFrameWorkRecordViewClass( Self.Classtype ) );
  end;

  {$IFDEF CODESITE}
  csFWRecordView.ExitMethod( Self, 'TPPWFrameWorkRecordView.RegisterSelf' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.SetMode 
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the Mode property.                                                     
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the Mode property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.SetMode(
  const Value: TPPWFrameWorkRecordViewMode);
begin
  if ( Value <> FMode ) then
  begin
    FMode := Value;

    if ( FMode = rvmEdit ) or ( FMode = rvmAdd ) then
    begin
      ResetReadOnlyComponents;
    end;

    DoUpdateControls;

//    if ( Mode = rvmAdd ) and
//       ( Assigned( ClonedDataset ) ) then
//    begin
//      ClonedDataset.Insert;
//    end;

    { Now broadcast a message that our Mode has changed, so we can
      eventually react on this in the Docked ListView forms }
    DoModeChanged;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.UnRegisterSelf 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to unregister the current form
                   with our FrameWork controller.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.UnRegisterSelf;
begin
  if Not ( csLoading in ComponentState ) and
         ( csDesigning in ComponentState ) then
  begin
    {$IFDEF CODESITE}
    csFWRecordView.EnterMethod( Self, 'TPPWFrameWorkRecordView.UnRegisterSelf' );
    csFWRecordView.SendString( 'RecordView.ClassName', Self.ClassName );
    {$ENDIF}

    if ( Assigned( PPWController ) ) then
    begin
      PPWController.UnRegisterFrameWorkClass( TPPWFrameWorkRecordViewClass( Self.Classtype ) );
    end;

    {$IFDEF CODESITE}
    csFWRecordView.ExitMethod( Self, 'TPPWFrameWorkRecordView.UnRegisterSelf' );
    {$ENDIF}
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.UpdateControls
  Author         : Stefaan Lesage
  Arguments      : Container - The control which contains all the controls
                               that must be updated.                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to loop over all controls on the
                   Container and call UpdateControlColor for each one of them
                   to update their layout based on some settings.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/08/2003   slesage              The UpdateControls method has been
                                    changed.  It will now also update all
                                    controls that the given Container
                                    control contains.
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.UpdateControls( Container : TWinControl );
var
  lcv         : Integer;
begin
  {$IFDEF CODESITE}
  csFWControlLayout.EnterMethod( Self, 'UpdateControls' );
  csFWControlLayout.SendObject( 'Container', Container );
  {$ENDIF}

  for lcv := 0 to Pred( Container.ControlCount ) do
  begin
    if ( (IsPublishedProp ( Container.Controls[ lcv ], 'DataSource')) and
         (IsPublishedProp ( Container.Controls[ lcv ], 'DataField')) )
    or ( IsPublishedProp  ( Container.Controls[ lcv ], 'ReadOnly') ) then
    begin
      UpdateControlColor( Container.Controls[ lcv ] );
    end;

    if ( Container.Controls[ lcv ] is TWinControl ) and
       ( TWinControl( Container.Controls[ lcv ] ).ControlCount <> 0 ) then
    begin
      UpdateControls( TWinControl( Container.Controls[ lcv ] ) );
    end;
  end;

  {$IFDEF CODESITE}
  csFWControlLayout.ExitMethod( Self, 'UpdateControls' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.UpdateControlLayout
  Author         : Stefaan Lesage
  Arguments      : Control - The Control for which the Lay-Out must be
                             updated.
                   ControlOptions - Contains a set of options which will
                                    be used to figure out what the Lay-Out
                                    of the Controls should be like.
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to Update the Lay-Out of the
                   specified control.  The Lay-Out it will receive depends on
                   the ControlOptions that will be supplied to the procedure.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/09/2003   sLesage              RadioButtons that belong to a RadioGroup
                                    will no longer be colored white when they
                                    loose the focus.
  15/07/2003   sLesage              Added support for the Fonts as well.
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.UpdateControlLayout(Control: TControl;
  ControlOptions: TPPWFrameWorkControlOptions);
var
  aDefaultFont : TFont;
begin
  {$IFDEF CODESITE}
  csFWControlLayout.EnterMethod( Self, 'UpdateControlLayout' );
  csFWControlLayout.SendObject( 'Control', Control );
  csFWControlLayout.SendBoolean( 'coDataSetReadOnly', coDataSetReadOnly in ControlOptions );
  csFWControlLayout.SendBoolean( 'coFieldReadOnly', coFieldReadOnly in ControlOptions );
  csFWControlLayout.SendBoolean( 'coControlReadOnly', coControlReadOnly in ControlOptions );
  csFWControlLayout.SendBoolean( 'coFieldRequired', coFieldRequired in ControlOptions );
  csFWControlLayout.SendBoolean( 'coControlDisabled', coControlDisabled in ControlOptions );
  csFWControlLayout.SendBoolean( 'coControlHasFocus', coControlHasFocus in ControlOptions );
  {$ENDIF}

  if ( coDataSetReadOnly in ControlOptions ) or
     ( coFieldReadOnly in ControlOptions ) or
     ( coControlReadOnly in ControlOptions ) or
     ( coControlDisabled in ControlOptions ) then
  begin

    { Set the Color of the Control }
    if ( IsPublishedProp( Control, 'Color' ) ) then
    begin
      SetOrdProp( Control, 'Color', TColor( ppwController.ReadOnlyColor ) );
    end;

    { Disable the Control ( If the EnableWhenReadOnly property was set
      to False on the Controller }
    if ( IsPublishedProp( Control, 'Enabled' ) ) then
    begin
      if not PPWController.EnableWhenReadOnly then
      begin
        SetOrdProp( Control, 'Enabled', Ord( False ) );
      end;
    end;

    if ( IsPublishedProp( Control, 'Font' ) ) then
    begin
      if not PPWController.EnableWhenReadOnly then
      begin
        SetObjectProp( Control, 'Font', PPWController.ReadOnlyFont );
      end;
    end;

//    if ( Mode in [ rvmView, rvmDelete ] ) then
//    begin
//      if ( IsPublishedProp( Control, 'ReadOnly' ) ) then
//      begin
//        AddReadOnlyComponent( Control );
//      end;
//    end;
  end
  else if ( coControlHasFocus in ControlOptions ) then
  begin
    { Set the Color of the Control }
    if ( IsPublishedProp( Control, 'Color' ) ) then
    begin
      SetOrdProp( Control, 'Color', TColor( ppwController.FocusColor ) );
    end;

    if ( IsPublishedProp( Control, 'Font' ) ) then
    begin
      SetObjectProp( Control, 'Font', PPWController.FocusFont );
    end;
  end
  else if ( coFieldRequired in ControlOptions ) then
  begin
    if ( IsPublishedProp( Control, 'Color' ) ) then
    begin
      SetOrdProp( Control, 'Color', TColor( ppwController.RequiredColor ) );
    end;

    if ( IsPublishedProp( Control, 'Font' ) ) then
    begin
      SetObjectProp( Control, 'Font', PPWController.RequiredFont );
    end;
  end
  else
  begin
    if ( IsPublishedProp( Control, 'ValueChecked' ) and
         IsPublishedProp( Control, 'ValueUnChecked' ) ) or
       ( Control is TCustomGroupBox ) then
    begin
      if ( IsPublishedProp( Control, 'ParentColor' ) ) then
      begin
        SetOrdProp( Control, 'ParentColor', Ord( True ) );
      end;
    end
    else
    begin
      if ( IsPublishedProp( Control, 'Color' ) ) then
      begin
        if //( Control is TRadioButton ) and
           ( Control.Parent is TCustomRadioGroup ) then
        begin
          TRadioButton( Control ).ParentColor := True;
        end
        else
        begin
          SetOrdProp( Control, 'Color', TColor( clWindow ) );
        end;
      end;
      if ( IsPublishedProp( Control, 'Font' ) ) then
      begin
        aDefaultFont := TFont.Create;
        SetObjectProp( Control, 'Font', aDefaultFont );
        FreeAndNil( aDefaultFont );
      end;
    end;
  end;
  {$IFDEF CODESITE}
  csFWControlLayout.SendObject( 'Control', Control );
  csFWControlLayout.ExitMethod( Self, 'UpdateControlLayout' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.UpdateFormCaption 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to combine the original caption
                   of the form with the value returned by GetDetailName.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.UpdateFormCaption;
begin
  if ( GetDetailName <> '' ) then
  begin
    Caption := OriginalCaption + ' : ' + GetDetailName;
  end
  else
  begin
    Caption := OriginalCaption;
  end;
end;

{*****************************************************************************
  This method will look at some properties of the supplied control and
  depending on those set up some ControlOptions.  Once it has done that it
  will call the UpdateControlLayout method passing the Control and
  ControlOptions to it.

  @Name       TPPWFrameWorkRecordView.UpdateControlColor
  @author     slesage
  @param      Control   The Control for which the Lay-Out might need
                        to be changed.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkRecordView.UpdateControlColor(Control: TControl);
var
  aOptions    : TPPWFrameWorkControlOptions;
  aReadOnly   : Boolean;
  aEnabled    : Boolean;
  aField      : TField;
  aFieldName  : string;
  aDataSource : TDataSource;
begin
  {$IFDEF CODESITE}
  csFWControlLayout.EnterMethod( Self, 'UpdateControlColor' );
  csFWControlLayout.SendObject( 'Control', Control );
  {$ENDIF}

  aOptions := [];
  aField   := Nil;

  if ( Assigned( Control ) ) then
  begin
    if ( IsPublishedProp( Control, 'ReadOnly' ) ) then
    begin
      aReadOnly := Boolean( GetOrdProp( Control, 'Enabled' ) );
    end
    else
    begin
      aReadOnly := False;
    end;

    if ( IsPublishedProp( Control, 'Enabled' ) ) then
    begin
      aEnabled := Boolean( GetOrdProp( Control, 'Enabled' ) );
    end
    else
    begin
      aEnabled := True;
    end;

    if ( IsPublishedProp( Control, 'DataSource' ) ) then
    begin
      aDataSource := TDataSource( GetObjectProp( Control, 'DataSource' ) );
      if ( IsPublishedProp( Control, 'DataField' ) ) then
      begin
        aFieldName := GetStrProp( Control, 'DataField' );
        if ( Assigned( aDataSource ) ) and
           ( Assigned( aDataSource.DataSet ) ) then
        begin
          aField := aDataSource.DataSet.FindField( aFieldName );
        end;
      end;
    end
    else
    begin
      aFieldName  := '';
    end;

    { Check if the Control was set to ReadOnly at Design Time }
    if ( aReadOnly ) or not aEnabled then
    begin
      aOptions := aOptions + [ coControlReadOnly ];
    end
    { Check if the form is in Delete or View Mode }
    else if ( Mode = rvmDelete ) or ( Mode = rvmView ) then
    begin
      aOptions := aOptions + [ coDataSetReadOnly ];
      AddReadOnlyComponent( Control );
    end
    { Check if the Field is ReadOnly }
    else if ( Assigned( aField ) and
            ( aField.ReadOnly ) ) then
    begin
      aOptions := aOptions + [ coFieldReadOnly ];
      AddReadOnlyComponent( Control );
    end
    { Check if the control has the focus }
    else if ( ActiveControl = Control ) then
    begin
      aOptions := aOptions + [ coControlHasFocus ];
    end
    { Check if the Field is Required }
    else if ( Assigned( aField ) and
            ( aField.Required ) ) then
    begin
      aOptions := aOptions + [ coFieldRequired ];
      AddReadOnlyComponent( Control );
    end;

    UpdateControlLayout( Control, aOptions );
  end;

  {$IFDEF CODESITE}
  csFWControlLayout.ExitMethod( Self, 'UpdateControlColor' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.GetForm
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the Current Form.
  Exceptions     : None
  Description    : This Method is used by the Interfaces to get access to the
                   actual form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkRecordView.GetForm: TCustomForm;
begin
  Result := Self;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.GetBeforeUpdateControls
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The current Value for the BeforeUpdateControls property.
  Exceptions     : None
  Description    : Property Getter for the BeforeUpdateControls property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}
function TPPWFrameWorkRecordView.GetBeforeUpdateControls: TNotifyEvent;
begin
  Result := FBeforeUpdateControls;
end;

function TPPWFrameWorkRecordView.GetAfterUpdateControls: TNotifyEvent;
begin
  result := FAfterUpdateControls;
end;


{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.GetClonedDataset
  Author         : Wim Lambrechts                                          
  Arguments      : None                                                     
  Return Values  : None
  Exceptions     : None
  Description    : Returns an instance of the cloned dataset
  History        :

  Date         By                   Description
  ----         --                   -----------
  16/12/2002   Wim Lambrechts       Initial creation of the Procedure.
 *****************************************************************************}
//function TPPWFrameWorkRecordView.GetClonedDataset: TPPWFrameWorkClientDataSet;
//begin
//  result := nil;
//  if assigned (FRecordViewDatamodule) and
//     assigned (FRecordViewDatamodule.RecordDataset ) and
//     (FRecordViewDatamodule.RecordDataset  is TPPWFrameWorkClientDataSet)
//  then
//  result := TPPWFrameworkClientDataset(FRecordViewDatamodule.Recorddataset);
//end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.SetBeforeUpdateControls
  Author         : Stefaan Lesage
  Arguments      : Value - The New Value for the BeforeUpdateControls property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the BeforeUpdateControls property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.SetBeforeUpdateControls(
  const Value: TNotifyEvent);
begin
  FBeforeUpdateControls := Value;
end;

procedure TPPWFrameWorkRecordView.SetAfterUpdateControls(
  const Value: TNotifyEvent);
begin
  FAfterUpdateControls := Value;
end;


{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.DoUpdateControls 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : The DoUpdateControls is actually the Event Dispatcher for
                   the BeforeUpdateControls event.  It will execute that event
                   if it was supplied and then execute the UpdateControls
                   method.
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.DoUpdateControls;
begin
  if ( Assigned( FBeforeUpdateControls ) ) then
  begin
    FBeforeUpdateControls( Self );
  end;

  ResetReadOnlyComponents;
  UpdateControls( Self );
  
  if ( Assigned( FAfterUpdateControls ) ) then
  begin
    FAfterUpdateControls( Self );
  end;
end;

function TPPWFrameWorkRecordView.GetOnAllDetailsCreated: TNotifyEvent;
begin
  result := FOnAllDetailsCreated;
end;

procedure TPPWFrameWorkRecordView.SetOnAllDetailsCreated(
  const Value: TNotifyEvent);
begin
  FOnAllDetailsCreated := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.Loaded
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Performs fix-ups when the form is first loaded into memory.
  History        :

  Date         By                   Description
  ----         --                   -----------
  24/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.Loaded;
begin
  inherited Loaded;
end;

function TPPWFrameWorkRecordView.GetOnDetailListViewCreated: TPPWFrameWorkOnDetailListViewCreatedEvent;
begin
  result := FOnDetailListViewCreated;
end;

procedure TPPWFrameWorkRecordView.SetOnDetailListViewCreated(
  const Value: TPPWFrameWorkOnDetailListViewCreatedEvent);
begin
  FOnDetailListViewCreated := Value;
end;

procedure TPPWFrameWorkRecordView.DoModeChanged;
begin
//
end;

function TPPWFrameWorkRecordView.GetRecordViewDatamodule: IPPWFrameWorkDatamodule;
begin
  result := FRecordViewDatamodule;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.RefreshRecordView
  Author         : Wim Lambrechts
  Arguments      : Source: can be the cdsMain (inital opening of a recordview)
                   or a cdsRecord (refreshing after having posted)
  Return Values  : None
  Exceptions     : None
  Description    : Closes and reopens the cdsRecord clientdataset. This is needed
                   for initially opening the recordview, as well as refreshing the
                   data when the recordview is posted
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/04/2003   Wim Lambrechts       Initial creation of the Procedure.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.RefreshRecordView (RecordViewMode: TPPWFrameWorkRecordViewMode);
var
  source: TClientDataset;
  RecordCDS: TClientDataset;
  lcv: Integer;
begin
  Source := FrameWorkDataModule.DataSet;
  RecordCDS := FRecordViewDatamodule.RecordDataset;
  RecordCDS.close;
  for lcv := 0 to RecordCDS.Params.Count - 1 do
  begin
    if Source.FindField(RecordCDS.Params[lcv].Name) <> nil then
    begin //to avoid that program params are being cleared or copied
      if RecordViewMode <> rvmAdd then
      begin
        RecordCDS.Params[lcv].value := Source.FieldByName (RecordCDS.Params[lcv].Name).Value;
      end;
    end;
  end;

//  Try
    RecordCDS.open;
//  Except
//    on E: EInvalidGraphic do
//    begin
//    end
//    else
//    begin
//      Raise;
//    end;
//  end;
//  Finally
    if ( RecordCDS.Active ) and
       ( RecordCDS.RecordCount = 0) and
       ( RecordViewMode <> rvmAdd ) then
    begin  { TODO : Not really clean. But we only have the datamodule for the recordview in the constructor of the recordview }
      Raise EPPWRecordAlreadyDeleted.Create (rsPPWFrameWorkRecordAlreadyDeleted);
    end;
//  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.LinkDataSourcesToDataSets
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to link the DataSources on the
                   form to the DataSets on the RecordDataModule.
  History        :

  Date         By                   Description
  ----         --                   -----------
  25/08/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.LinkDataSourcesToDataSets;
begin

end;

function TPPWFrameWorkRecordView.GetDetailListViews: IInterfaceList;
begin
  Result := FDetailListViews;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.RefreshVisibleDetailListViews
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to refresh all the visible
                   Detail ListViews for the current RecordView form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  16/09/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.RefreshVisibleDetailListViews;
var
  aDetailIndex : Integer;
  aDetailLV    : IPPWFrameWorkListView;
begin
  for aDetailIndex := 0 to Pred( DetailListViews.Count ) do
  begin
    if ( Supports( DetailListViews[ aDetailIndex ], IPPWFrameWorkListView, aDetailLV ) ) then
    begin
      if ( aDetailLV.FrameWorkDataModule.Visible ) then
      begin
        aDetailLV.FrameWorkDataModule.RefreshListDataSet;
      end;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkRecordView.PostDetailListViews
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to post all changes op the
                   different Detail ListViews if they havn't been posted yet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  07/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkRecordView.PostDetailListViews;
var
  aDetailIndex : Integer;
  aDetailLV    : IPPWFrameWorkListView;
begin
  for aDetailIndex := 0 to Pred( DetailListViews.Count ) do
  begin
    if ( Supports( DetailListViews[ aDetailIndex ], IPPWFrameWorkListView, aDetailLV ) ) then
    begin
      if ( aDetailLV.FrameWorkDataModule.DataSet.State in dsEditModes ) then
      begin
        aDetailLV.FrameWorkDataModule.DataSet.Post;
        aDetailLV.FrameWorkDataModule.DoApplyPendingUpdates;
      end;
    end;
  end;
end;

procedure TPPWFrameWorkRecordView.SetUseFocusController(
  const Value: Boolean);
begin
  if ( Value <> FUseFocusController ) then
  begin
    FUseFocusController := Value;

    if ( UseFocusController ) then
    begin
      FocusController.OnLostFocus     := UpdateControlColor;
      FocusController.OnReceivedFocus := UpdateControlColor;
    end
    else
    begin
      FocusController.OnLostFocus     := Nil;
      FocusController.OnReceivedFocus := Nil;
      FreeAndNil( FFocusCtrl );
    end;
  end;
end;

function TPPWFrameWorkRecordView.GetFocusController: TPPWFrameWorkFocusControl;
begin
  if not ( Assigned( FFocusCtrl ) ) then
  begin
    FFocusCtrl := TPPWFrameWorkFocusControl.Create( Self );
  end;
  Result := FFocusCtrl;
end;

{*****************************************************************************
  New constructor for the TPPWFrameWorkRecordView

  @Name       TPPWFrameWorkRecordView.Create
  @author     slesage
  @param      aOwner        The Owner of the DataModule.
  @param      aDataModule   The DataModule for which the FrameWorkForm must
                            be Created.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TPPWFrameWorkRecordView.Create(aOwner: TComponent;
  aDataModule: IPPWFrameWorkDataModule = Nil );
begin
  {$IFDEF CODESITE}
  csFWRecordView.EnterMethod( Self, 'Create' );
  {$ENDIF}

  FRecordViewDataModule := aDataModule;
  FReadOnlyComponents   := TComponentList.Create( False );

  inherited Create( aOwner, aDataModule );

  { Create an Interface List which will hold references to the Detail List
    Views }
  FDetailListViews   := TInterfaceList.Create;
  FDockedRecordViews := TInterfaceList.Create;

  FMode := rvmDefault;

  if ( Not ( csDesigning in ComponentState ) ) and
     ( UseFocusController ) then
  begin
    FocusController.OnLostFocus     := UpdateControlColor;
    FocusController.OnReceivedFocus := UpdateControlColor;
  end;

  CloneDataSetFromDataModule;
  LinkDataSourcesToDataSets;

  UpdateFormCaption;

  {$IFDEF CODESITE}
  csFWRecordView.ExitMethod( Self, 'Create' );
  {$ENDIF}
end;

procedure TPPWFrameWorkRecordView.AddReadOnlyComponent(
  aComponent: TComponent; aHandled : Boolean = False);
begin
  if ( Assigned( aComponent ) ) then
  begin
    if ( FReadOnlyComponents.IndexOf( aComponent ) = -1 ) then
    begin
      FReadOnlyComponents.Add( aComponent );
    end;

    if ( IsPublishedProp( aComponent, 'ReadOnly' ) ) then
    begin
      SetOrdProp( aComponent, 'ReadOnly', Ord( True ) );
    end;
  end;
end;

procedure TPPWFrameWorkRecordView.ResetReadOnlyComponents;
var
  lcv : Integer;
begin
  for lcv := Pred( FReadOnlyComponents.Count ) downto 0 do
  begin
    RemoveReadOnlyComponent( FReadOnlyComponents[ lcv ] );
  end;
end;

procedure TPPWFrameWorkRecordView.RemoveReadOnlyComponent(
  aComponent: TComponent; aHandled : Boolean = False);
begin
  if ( Assigned( aComponent ) ) then
  begin
    if ( FReadOnlyComponents.IndexOf( aComponent ) <> -1 ) then
    begin
      FReadOnlyComponents.Remove( aComponent );
    end;

    if not ( aHandled ) then
    begin
      if ( IsPublishedProp( aComponent, 'ReadOnly' ) ) then
      begin
        SetOrdProp( aComponent, 'ReadOnly', Ord( False ) );
      end;
    end;
  end;
end;

function TPPWFrameWorkRecordView.GetDockedRecordViews: IInterfaceList;
begin
  Result := FDockedRecordViews;
end;

end.
