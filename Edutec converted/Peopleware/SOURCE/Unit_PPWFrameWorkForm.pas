{*****************************************************************************
  This unit contains the Base PPW FrameWork form, all other forms ( like the
  ListView and RecordView ) will descend from this form.  This form can be used
  to create new types of FrameWork Forms.

  @Name       Unit_PPWFrameWorkForm
  @Author     slesage
  @Copyright  (c) 2002 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  22/12/2004   slesage              The default RegistryKey for a FrameWorkForm
                                    is now
                                    HKEY_CURRENT_USER\SoftWare\PeopleWare\<ApplicationName>\<FormClassName>
  20/12/2004   slesage              Fixed a bug when setting the FormStyle
                                    property of the form.  This resulted in
                                    the Size of the Form and its constraints
                                    being changed.  We fixed this by temporary
                                    storing the Form's constraints and restoring
                                    them once the FormStyle was set.
  29/11/2004   sLesage              Reworked the Constructor of the form.
  29/11/2002   sLesage              Cleaned up unnecessary Code and added
                                    Unit / Procedure Headers.
  29/11/2002   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_PPWFrameWorkForm;

interface

uses
  { Delphi Units }
  Forms, DB, Controls, Classes, Windows,
  { FrameWork Units }
  Unit_PPWFrameWorkInterfaces, Unit_PPWFrameWorkClasses;

type
  TPPWFrameWorkForm = class(TForm, IPPWFrameWorkForm)
  private
    FFrameWorkDataModule : IPPWFrameWorkDataModule;
    FRegistered          : Boolean;
    FDataSource          : TDataSource;
    FOriginalCaption     : String;
  protected
    { Property Getters }
    function GetAlign               : TAlign; virtual;
    function GetBorderStyle         : TFormBorderStyle; virtual;
    function GetCaption             : TCaption; virtual;
    function GetDataSource          : TDataSource; virtual;
    function GetDataSet             : TDataSet; virtual;
    function GetKeyFields           : string; virtual;
    function GetHandle              : HWND; virtual;
    function GetHeight              : Integer; virtual;
    function GetOriginalCaption     : String; virtual;
    function GetRegistered          : Boolean; virtual;
    function GetRegistryKey         : String; virtual;
    function GetFormStyle           : TFormStyle; virtual;
    function GetFrameWorkClassName  : String;
    function GetFrameWorkDataModule : IPPWFrameWorkDataModule; virtual;
    function GetMinHeight           : Integer; virtual;
    function GetMinWidth            : Integer; virtual;
    function GetWindowState         : TWindowState; virtual;
    function GetSettingsSaved       : Boolean; virtual; //added WL 06/01
    function GetVisible             : Boolean; virtual;
    function GetTop                 : Integer; virtual;
    function GetLeft                : Integer; virtual;
    function GetWidth               : Integer; virtual;
    function GetPosition            : TPosition; virtual;

    { Property Setters }
    procedure SetAlign(const Value: TAlign); virtual;
    procedure SetBorderStyle( const Value : TFormBorderStyle ); virtual;
    procedure SetCaption(const Value: TCaption); virtual;
    procedure SetDataSource(const Value: TDataSource); Virtual;
    procedure SetFormStyle( const Value : TFormStyle ); Virtual;
    procedure SetHeight( Const Value : Integer ); Virtual;
    procedure SetName( const Value : TComponentName ); override;
    procedure SetOriginalCaption( const Value : String ); virtual;
    procedure SetRegistered(const Value: Boolean); virtual;
    procedure SetUseDockManager( Const Value : Boolean ); virtual;
    procedure SetVisible( const Value : Boolean ); virtual;
    procedure SetWindowState( const Value : TWindowState ); virtual;
    procedure SetLeft( const Value : Integer ); virtual;
    procedure SetTop( const Value : Integer ); virtual;
    procedure SetWidth( const Value : Integer ); virtual;
    procedure SetPosition( const Value : TPosition ); Virtual;

    { Methods }
    procedure DoShow; override;
    procedure Notification( aComponent : TComponent; Operation : TOperation ); Override;
    procedure UpdateFormCaption; virtual;

    { Registration Routines }
    procedure RegisterSelf; virtual;
    procedure UnRegisterSelf; virtual;

    property OriginalCaption : string             read  GetOriginalCaption
                                                  write SetOriginalCaption;
    property KeyFields       : String             read  GetKeyFields;
  public
    { Constructor / Destructor }
    constructor Create( aOwner : TComponent; aDataModule : IPPWFrameWorkDataModule = Nil ); reintroduce; virtual;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure ModuleNotified (Sender: TComponent; const ModuleName: string); virtual;

    { Properties }
    property DataSet             : TDataSet       read GetDataSet;

    property FrameWorkDataModule : IPPWFrameWorkDataModule
                                                  read GetFrameWorkDataModule;
    property RegistryKey : String                 read GetRegistryKey;
    property SettingsSaved: Boolean               read GetSettingsSaved; //added WL 06/01
  published
    property Align: TAlign                        read GetAlign
                                                  write SetAlign;
    property Caption: TCaption                    read GetCaption
                                                  write SetCaption;
    property DataSource : TDataSource             read GetDataSource
                                                  write SetDataSource;
    property FormStyle  : TFormStyle              read GetFormStyle
                                                  write SetFormStyle;
    property Height     : Integer                 read GetHeight
                                                  write SetHeight;
    property Registered : Boolean                 read GetRegistered
                                                  write SetRegistered;
    property WindowState : TWindowState           read GetWindowState
                                                  write SetWindowState;
  end;

  TPPWFrameWorkFormClass = class of TPPWFrameWorkForm;

implementation

uses
  { Delphi Units }
  SysUtils, 

  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects,
  {$ENDIF}
  { FrameWork Units }
  Unit_PPWFrameWorkController;

{$R *.dfm}

{ TPPWFrameWorkForm }

{*****************************************************************************
  Ovverridden Destructor in which we will clean up everything.

  @Name       TPPWFrameWorkForm.Destroy
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

destructor TPPWFrameWorkForm.Destroy;
begin
  if not (csDesigning in ComponentState) then
  begin
    FFrameWorkDataModule := Nil;
  end;
  inherited Destroy;
end;

{*****************************************************************************
  The DoShow is called automatically when the Form is first displayed.  We
  have overridden it so we can set the Visible property of the Associated
  Datamodule to true as well.

  @Name       TPPWFrameWorkForm.DoShow
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.DoShow;
begin
  inherited DoShow;

  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    FrameWorkDataModule.Visible := True;
  end;

  UpdateFormCaption;
end;

{*****************************************************************************
  Property Getter for the Align Property.

  @Name       TPPWFrameWorkForm.GetAlign
  @author     slesage
  @param      None
  @return     The current Value of the Align Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetAlign: TAlign;
begin
  Result := Inherited Align;
end;

{*****************************************************************************
  Property Getter for the Caption Property.

  @Name       TPPWFrameWorkForm.GetCaption
  @author     slesage
  @param      None
  @return     The current Value of the Caption Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetCaption: TCaption;
begin
  Result := inherited Caption;
end;

{*****************************************************************************
  Property Getter for the DataSet Property.

  @Name       TPPWFrameWorkForm.GetDataSet
  @author     slesage
  @param      None
  @return     The current Value of the DataSet Property.  On this Form it will
              return the Main DataSet of the associated DataModule.  This
              behaviour can be overridden on descendant forms if necessary.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetDataSet: TDataSet;
begin
  if ( Assigned( FFrameWorkDataModule ) ) then
  begin
    Result := FFrameWorkDataModule.DataSet;
  end
  else
  begin
    Result := Nil;
  end;
end;

{*****************************************************************************
  Property Getter for the DataSource Property.

  @Name       TPPWFrameWorkForm.GetDataSource
  @author     slesage
  @param      None
  @return     The current Value of the DataSource Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetDataSource: TDataSource;
begin
  Result := FDataSource;
end;

{*****************************************************************************
  Property Getter for the FormStyle Property.

  @Name       TPPWFrameWorkForm.GetFormStyle
  @author     slesage
  @param      None
  @return     The current Value of the FormStyle Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetFormStyle: TFormStyle;
begin
  Result := Inherited FormStyle;
end;

{*****************************************************************************
  Property Getter for the FrameWorkDataModule Property.

  @Name       TPPWFrameWorkForm.GetFrameWorkDataModule
  @author     slesage
  @param      None
  @return     The current Value of the FrameWorkDataModule Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetFrameWorkDataModule: IPPWFrameWorkDataModule;
begin
  Result := FFrameWorkDataModule;
end;

{*****************************************************************************
  Property Getter for the Height Property.

  @Name       TPPWFrameWorkForm.GetHeight
  @author     slesage
  @param      None
  @return     The current Value of the Height Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetHeight: Integer;
begin
  Result := Inherited Height;
end;

{*****************************************************************************
  Property Getter for the KeyFields Property.

  @Name       TPPWFrameWorkForm.GetKeyFields
  @author     slesage
  @param      None
  @return     The current Value of the KeyFields Property.  This will in fact
              return the KeyFields property of the associated FrameWorkDataModule.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetKeyFields: string;
begin
  Result := '';

  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    Result := FrameWorkDataModule.KeyFields;
  end;
end;

{*****************************************************************************
  Property Getter for the OriginalCaption Property.

  @Name       TPPWFrameWorkForm.GetOriginalCaption
  @author     slesage
  @param      None
  @return     The current Value of the OriginalCaption Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetOriginalCaption: String;
begin
  Result := FOriginalCaption;
end;

{*****************************************************************************
  Property Getter for the Registered Property.

  @Name       TPPWFrameWorkForm.GetRegistered
  @author     slesage
  @param      None
  @return     The current Value of the Registered Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetRegistered: Boolean;
begin
  Result := FRegistered;
end;

{*****************************************************************************
  Property Getter for the RegistryKey Property.

  @Name       TPPWFrameWorkForm.GetRegistryKey
  @author     slesage
  @param      None
  @return     The current Value of the RegistryKey Property.  This is the Base
              RegistryKey which can be used to store specific things for this
              instance of the form.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetRegistryKey: String;
begin
  Result := 'SoftWare\PeopleWare\' +
            ExtractFileName( ChangeFileExt( Application.ExeName, '' ) ) +
            '\' + ClassName;
end;

{*****************************************************************************
  Property Getter for the WindowState Property.

  @Name       TPPWFrameWorkForm.GetWindowState
  @author     slesage
  @param      None
  @return     The current Value of the WindowState Property.
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetWindowState: TWindowState;
begin
  Result := Inherited WindowState;
end;

{*****************************************************************************
  this is needed by the controller, so that we can determine whether
  modifications to the sizes of the recordviews and listviews should be made.
  Since the formstorage component is not yet introduced on this level, in this
  class the function always returns false

  @Name       TPPWFrameWorkForm.GetSettingsSaved
  @author     wlambrechts
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TPPWFrameWorkForm.GetSettingsSaved: Boolean;
begin
  result := false;
end;

{*****************************************************************************
  This is an overridden Loaded method which will be used to performs fix-ups
  when the form is first loaded into memory.

  @Name       TPPWFrameWorkForm.Loaded
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.Loaded;
begin
  inherited Loaded;
  FOriginalCaption := Caption;
end;

{*****************************************************************************
  This overridden Notification method is used to respond to notifications that
  objects are about to be inserted or removed from the Form.

  @Name       TPPWFrameWorkForm.Notification
  @author     slesage
  @param      aComponent   The Component that is about to be inserted or
                           deleted.
  @param      Operation  - The Actual Operation ( opRemove / opInsert )
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.Notification(aComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification( aComponent, Operation );

  if ( Operation = opRemove ) and ( aComponent = DataSource ) then
  begin
    DataSource := Nil;
  end;
end;

{*****************************************************************************
  Property Setter for the Align Property.

  @Name       TPPWFrameWorkForm.SetAlign
  @author     slesage
  @param      Value   The new Value for the Align Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetAlign(const Value: TAlign);
begin
  Inherited Align := Value;
end;

{*****************************************************************************
  Property Setter for the Caption Property.

  @Name       TPPWFrameWorkForm.SetCaption
  @author     slesage
  @param      Value   The new Value for the Caption Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetCaption(const Value: TCaption);
begin
  Inherited Caption := Value;
end;

{*****************************************************************************
  Property Setter for the DataSource Property.

  @Name       TPPWFrameWorkForm.SetDataSource
  @author     slesage
  @param      Value   The new Value for the DataSource Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetDataSource(const Value: TDataSource);
begin
  if ( Value <> FDataSource ) then
  begin
    if ( FDataSource <> nil ) then
    begin
      FDataSource.RemoveFreeNotification( Self );
    end;

    FDataSource := Value;

    if ( FDataSource <> nil ) then
    begin
      FDataSource.FreeNotification( Self );
    end;
  end;
end;

{*****************************************************************************
  Property Setter for the FormStyle Property.

  @Name       TPPWFrameWorkForm.SetFormStyle
  @author     slesage
  @param      Value   The new Value for the FormStyle Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetFormStyle(const Value: TFormStyle);
var
  aConstraints : TSizeConstraints;
begin
  if ( Value <> FormStyle ) then
  begin
    { Store the Size Constraints to solve a bug with SizeConstraints getting
      changed when modifying the FormStyle }
    aConstraints := TSizeConstraints.Create( Self );
    Try
      aConstraints.Assign( Constraints );
      Inherited FormStyle := Value;
      Constraints.Assign( aConstraints );
    finally
      FreeAndNil( aConstraints );
    end;
  end;
end;

{*****************************************************************************
  Property Setter for the Height Property.

  @Name       TPPWFrameWorkForm.SetHeight
  @author     slesage
  @param      Value   The new Value for the Height Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetHeight(const Value: Integer);
begin
  if ( Value <> Height ) then
  begin
    Inherited Height := Value;
  end;
end;

{*****************************************************************************
  Property Setter for the Name Property.

  @Name       TPPWFrameWorkForm.SetName
  @author     slesage
  @param      Value   The new Value for the Name Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetName(const Value: TComponentName);
begin
  if ( Value <> Name ) then
  begin
    if Not ( csLoading in ComponentState ) and
           ( csDesigning in ComponentState ) and
           ( Registered ) then
    begin
      UnRegisterSelf;
    end;

    Inherited SetName( Value );

    if Not ( csLoading in ComponentState ) and
           ( csDesigning in ComponentState ) and
           ( Registered ) then
    begin
      RegisterSelf;
    end;
  end;
end;

{*****************************************************************************
  Property Setter for the Registered Property.

  @Name       TPPWFrameWorkForm.SetRegistered
  @author     slesage
  @param      Value   The new Value for the Registered Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetRegistered(const Value: Boolean);
begin
  {$IFDEF CODESITE}
  csFWForm.EnterMethod( Self, 'SetRegistered' );
  {$ENDIF}

  if ( Value <> FRegistered ) then
  begin
    FRegistered := Value;

    if Not ( csLoading in ComponentState ) and
           ( csDesigning in ComponentState ) then
    begin
      if ( Registered ) then
      begin
        RegisterSelf;
      end
      else
      begin
        UnRegisterSelf;
      end;
    end;
  end;
  
  {$IFDEF CODESITE}
  csFWForm.SendBoolean( 'Registered set to', Registered );
  csFWForm.ExitMethod( Self, 'SetRegistered' );
  {$ENDIF}
end;

{*****************************************************************************
  Property Setter for the UseDockManager Property.

  @Name       TPPWFrameWorkForm.SetUseDockManager
  @author     slesage
  @param      Value   The new Value for the UseDockManager Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetUseDockManager(const Value: Boolean);
begin
  Inherited UseDockManager := Value;
end;

{*****************************************************************************
  Property Setter for the WindowState Property.

  @Name       TPPWFrameWorkForm.SetWindowState
  @author     slesage
  @param      Value   The new Value for the WindowState Property
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetWindowState(const Value: TWindowState);
begin
  Inherited WindowState := Value;
end;


procedure TPPWFrameWorkForm.ModuleNotified(Sender: TComponent;
  const ModuleName: string); //no implementation here, might be implemented in
  //the descendant forms
begin
end;

{*****************************************************************************
  Property Setter for the OriginalCaption property.

  @Name       TPPWFrameWorkForm.SetOriginalCaption
  @author     slesage
  @param      Value   The new Value for the OriginalCaption property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TPPWFrameWorkForm.SetOriginalCaption(const Value: String);
begin
  if ( Value <> FOriginalCaption ) then
  begin
    FOriginalCaption := Value;

    Caption := FOriginalCaption;
  end;
end;

function TPPWFrameWorkForm.GetMinHeight: Integer;
begin
  Result := Constraints.MinHeight;
end;

function TPPWFrameWorkForm.GetMinWidth: Integer;
begin
  Result := Constraints.MinWidth;
end;

function TPPWFrameWorkForm.GetFrameWorkClassName: String;
begin
  Result := Self.ClassName;
end;

function TPPWFrameWorkForm.GetBorderStyle: TFormBorderStyle;
begin
  Result := Inherited BorderStyle;
end;

procedure TPPWFrameWorkForm.SetBorderStyle(const Value: TFormBorderStyle);
begin
  inherited BorderStyle := Value;
end;

procedure TPPWFrameWorkForm.SetVisible(const Value: Boolean);
begin
  inherited Visible := Value;
end;

function TPPWFrameWorkForm.GetVisible: Boolean;
begin
  Result := Inherited Visible;
end;

function TPPWFrameWorkForm.GetLeft: Integer;
begin
  Result := Inherited Left;
end;

function TPPWFrameWorkForm.GetTop: Integer;
begin
  Result := Inherited Top;
end;

function TPPWFrameWorkForm.GetWidth: Integer;
begin
  Result := Inherited Width;
end;

procedure TPPWFrameWorkForm.SetLeft(const Value: Integer);
begin
  Inherited Left := Value ;
end;

procedure TPPWFrameWorkForm.SetTop(const Value: Integer);
begin
  Inherited Top := Value;
end;

procedure TPPWFrameWorkForm.SetWidth(const Value: Integer);
begin
  Inherited Width := Value;
end;

{*****************************************************************************
  New constructor for the FrameWork form.

  @Name       TPPWFrameWorkForm.Create
  @author     slesage
  @param      aOwner        The Owner of the DataModule.
  @param      aDataModule   The DataModule for which the FrameWorkForm must
                            be Created.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TPPWFrameWorkForm.Create(aOwner: TComponent;
  aDataModule: IPPWFrameWorkDataModule);
begin
  FRegistered := False;
  FFrameWorkDataModule := aDataModule;
  Inherited Create( aOwner );
end;

function TPPWFrameWorkForm.GetHandle: HWND;
begin
  Result := Inherited Handle;
end;

procedure TPPWFrameWorkForm.UpdateFormCaption;
begin
  Caption := OriginalCaption;
end;

procedure TPPWFrameWorkForm.RegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWForm.EnterMethod( Self, 'RegisterSelf' );
  {$ENDIF}
  
  {$IFDEF CODESITE}
  csFWForm.ExitMethod( Self, 'RegisterSelf' );
  {$ENDIF}
end;

procedure TPPWFrameWorkForm.UnRegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWForm.EnterMethod( Self, 'UnRegisterSelf' );
  {$ENDIF}
  
  {$IFDEF CODESITE}
  csFWForm.ExitMethod( Self, 'UnRegisterSelf' );
  {$ENDIF}
end;

function TPPWFrameWorkForm.GetPosition: TPosition;
begin
  Result := Inherited Position;
end;

procedure TPPWFrameWorkForm.SetPosition(const Value: TPosition);
begin
  inherited Position := Value; 
end;

end.
