{*****************************************************************************
  This unit contains the form used by the PIF Entity Wizard.
  
  @Name       Form_PIFEntitiyWizard
  @Author     slesage
  @Copyright  (c) 2004 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  23/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_PPWFrameWorkEntitiyWizard;

interface

uses
  Forms, ComCtrls, ShlObj, cxShellCommon, cxLookAndFeelPainters,
  cxContainer, cxEdit, DB, Classes, ActnList, cxLookAndFeels, StdCtrls,
  cxButtons, ExtCtrls, cxCheckBox, cxMemo, Controls, cxTextEdit, cxLabel,
  cxMaskEdit, cxDropDownEdit, cxShellComboBox, cxGroupBox, cxPC, cxControls;

type
  TfrmPPWFrameWorkEntityWizard = class(TForm)
    cxpgcOptions: TcxPageControl;
    cxlfCxLookAndFeelController1: TcxLookAndFeelController;
    pnlBottomButtons: TPanel;
    cxtbsGeneral: TcxTabSheet;
    cxtbsListViewOptions: TcxTabSheet;
    cxtbsRecordViewOptions: TcxTabSheet;
    cxbtnCancel: TcxButton;
    cxbtnOK: TcxButton;
    btnCxButton3: TcxButton;
    cxgbProjectOptions: TcxGroupBox;
    cxgbBaseClasses: TcxGroupBox;
    cxscbProjectFolder: TcxShellComboBox;
    cxscbDataModuleFolder: TcxShellComboBox;
    cxscbFormFolder: TcxShellComboBox;
    cxlblProjectFolder: TcxLabel;
    cxlblDataModuleFolder: TcxLabel;
    cxlblFormFolder: TcxLabel;
    cxlblProject: TcxLabel;
    cxlblProjectName: TcxLabel;
    cxtedtBaseDataModuleClass: TcxTextEdit;
    cxtedtBaseListViewClass: TcxTextEdit;
    cxtedtBaseRecordViewClass: TcxTextEdit;
    cxlblBaseDataModule: TcxLabel;
    cxlblBaseListView: TcxLabel;
    cxlblBaseRecordView: TcxLabel;
    cxgbCxGroupBox1: TcxGroupBox;
    cxtedtEntityName: TcxTextEdit;
    cxlblEntityName: TcxLabel;
    cxlblListViewCaption: TcxLabel;
    cxtedtListViewCaption: TcxTextEdit;
    cxlblRecordViewCaption: TcxLabel;
    cxtedtRecordViewCaption: TcxTextEdit;
    cxbtnNext: TcxButton;
    alActionList1: TActionList;
    acNextPage: TAction;
    acPreviousPage: TAction;
    acCancelWizard: TAction;
    acFinishWizard: TAction;
    srcList: TDataSource;
    srcRecord: TDataSource;
    cxmmoListSQL: TcxMemo;
    cxmmoRecordSQL: TcxMemo;
    bvlBevel1: TBevel;
    cxcbInitialiseListViewSQL: TcxCheckBox;
    bvlBevel2: TBevel;
    cxcbInitialiseRecordViewSQL: TcxCheckBox;
    cxescMain: TcxEditStyleController;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acNextPageUpdate(Sender: TObject);
    procedure acPreviousPageUpdate(Sender: TObject);
    procedure acNextPageExecute(Sender: TObject);
    procedure acPreviousPageExecute(Sender: TObject);
    procedure acFinishWizardUpdate(Sender: TObject);
    procedure cxpgcOptionsPageChanging(Sender: TObject;
      NewPage: TcxTabSheet; var AllowChange: Boolean);
    procedure acCancelWizardExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acFinishWizardExecute(Sender: TObject);
  private
    FProjectName: String;
    FProjectPath: String;
    function GetDataModuleFolder: string;
    function GetFormFolder: string;
    procedure SetDataModuleFolder(const Value: string);
    procedure SetFormFolder(const Value: string);
    function GetBaseDataModule: string;
    function GetBaseListView: string;
    function GetBaseRecordView: string;
    procedure SetBaseDataModule(const Value: string);
    procedure SetBaseListView(const Value: string);
    procedure SetBaseRecordView(const Value: string);
    function GetListViewCaption: string;
    function GetRecordViewCaption: string;
    function GetListViewSQL: TStrings;
    function GetRecordViewSQL: TStrings;
    function GetEntityName: String;
    function GetInitialiseListSQL: Boolean;
    function GetInitialiseRecordSQL: Boolean;
    procedure SetInitialiseListSQL(const Value: Boolean);
    procedure SetInitialiseRecordSQL(const Value: Boolean);
    { Private declarations }
  protected
    function GetProjectRegistryKey: String; virtual;
    function GetRootRegistryKey   : String; virtual;

    procedure SetProjectName(const Value: String); virtual;
    procedure SetProjectPath(const Value: String); virtual;

    function ProjectSettingsExist : Boolean; Virtual;

    procedure LoadSettingsFromRegistry; virtual;
    procedure SaveSettingsToRegistry; virtual;

    procedure ValidateGeneralOptions; virtual;
    procedure ValidateListViewOptions; virtual;
    procedure ValidateRecordViewOptions; virtual;
  public
    { Public declarations }
    property RootRegistryKey    : String read GetRootRegistryKey;
    property ProjectRegistryKey : String read GetProjectRegistryKey;
    property ProjectName        : String read FProjectName write SetProjectName;
    property ProjectPath        : String read FProjectPath write SetProjectPath;

    property BaseDataModule     : string read GetBaseDataModule   write SetBaseDataModule;
    property BaseListView       : string read GetBaseListView     write SetBaseListView;
    property BaseRecordView     : string read GetBaseRecordView   write SetBaseRecordView;
    property DataModuleFolder   : string read GetDataModuleFolder write SetDataModuleFolder;
    property FormFolder         : string read GetFormFolder       write SetFormFolder;

    property EntityName         : String read GetEntityName;

    property InitialiseListSQL  : Boolean read GetInitialiseListSQL write SetInitialiseListSQL;
    property InitialiseRecordSQL: Boolean read GetInitialiseRecordSQL write SetInitialiseRecordSQL;
    property ListViewCaption    : string read GetListViewCaption;
    property ListViewSQL        : TStrings read GetListViewSQL;
    property RecordViewCaption  : string read GetRecordViewCaption;
    property RecordViewSQL      : TStrings read GetRecordViewSQL;
  end;

var
  frmPPWFrameWorkEntityWizard: TfrmPPWFrameWorkEntityWizard;

implementation

{$R *.dfm}

uses
  {$IFDEF CODESITE}
  csIntf,
  {$ENDIF}
  Registry, SysUtils;

{ TfrmPPWFrameWorkEntityWizard }

function TfrmPPWFrameWorkEntityWizard.GetDataModuleFolder: string;
begin
  Result := IncludeTrailingPathDelimiter( cxscbDataModuleFolder.AbsolutePath );
end;

function TfrmPPWFrameWorkEntityWizard.GetFormFolder: string;
begin
  Result := IncludeTrailingPathDelimiter( cxscbFormFolder.AbsolutePath );
end;

function TfrmPPWFrameWorkEntityWizard.GetProjectRegistryKey: String;
begin
  Result := RootRegistryKey + ProjectName + '\'
end;

function TfrmPPWFrameWorkEntityWizard.GetRootRegistryKey: String;
begin
  Result := 'Software\PeopleWare\PIF\';
end;

procedure TfrmPPWFrameWorkEntityWizard.LoadSettingsFromRegistry;
var
  aReg : TRegIniFile;
begin
  if ( ProjectSettingsExist ) then
  begin
    aReg := TRegIniFile.Create;
    try
      if ( aReg.OpenKey( ProjectRegistryKey, False ) ) then
      begin
        DataModuleFolder    := aReg.ReadString( 'ProjectSettings', 'DataModuleFolder', DataModuleFolder );
        FormFolder          := aReg.ReadString( 'ProjectSettings', 'FormFolder'      , FormFolder );
        BaseDataModule      := aReg.ReadString( 'ProjectSettings', 'BaseDataModule'  , 'TPPWFrameWorkDataModule' );
        BaseListView        := aReg.ReadString( 'ProjectSettings', 'BaseListView'    , 'TPPWFrameWorkListView' );
        BaseRecordView      := aReg.ReadString( 'ProjectSettings', 'BaseRecordView'  , 'TPPWFrameWorkRecordView' );
        InitialiseListSQL   := aReg.ReadBool( 'ProjectSettings', 'InitialiseListSQL', True );
        InitialiseRecordSQL := aReg.ReadBool( 'ProjectSettings', 'InitialiseRecordSQL', True );
      end;
    finally
      FreeAndNil( aReg );
    end;
  end;
end;

{*****************************************************************************
  This function will be used to check settings for the current project Exist.

  @Name       TfrmCMBEntityWizard.ProjectSettingsExist
  @author     slesage
  @param      None
  @return     Returns a boolean indicating if settings for the currently
              acitve project already exist in the registry.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPPWFrameWorkEntityWizard.ProjectSettingsExist: Boolean;
var
  aReg : TRegIniFile;
begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Self, 'ProjectSettingsExist' );
  {$ENDIF}

  aReg := TRegIniFile.Create;
  try
    Result := aReg.KeyExists( ProjectRegistryKey );
  finally
    FreeAndNil( aReg );
  end;

  {$IFDEF CODESITE}
  CodeSite.SendBoolean( 'Result', Result );
  CodeSite.ExitMethod( Self, 'ProjectSettingsExist' );
  {$ENDIF}
end;

procedure TfrmPPWFrameWorkEntityWizard.SaveSettingsToRegistry;
var
  aReg : TRegIniFile;
begin
  aReg := TRegIniFile.Create;
  try
    if ( aReg.OpenKey( ProjectRegistryKey, True ) ) then
    begin
      aReg.WriteString( 'ProjectSettings', 'DataModuleFolder'   , DataModuleFolder );
      aReg.WriteString( 'ProjectSettings', 'FormFolder'         , FormFolder );
      aReg.WriteString( 'ProjectSettings', 'BaseDataModule'     , BaseDataModule );
      aReg.WriteString( 'ProjectSettings', 'BaseListView'       , BaseListView );
      aReg.WriteString( 'ProjectSettings', 'BaseRecordView'     , BaseRecordView );
      aReg.WriteBool  ( 'ProjectSettings', 'InitialiseListSQL'  , InitialiseListSQL );
      aReg.WriteBool  ( 'ProjectSettings', 'InitialiseRecordSQL', InitialiseRecordSQL );
    end;
  finally
    FreeAndNil( aReg );
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetDataModuleFolder(const Value: string);
begin
  if ( Value <> cxscbDataModuleFolder.AbsolutePath ) then
  begin
    cxscbDataModuleFolder.AbsolutePath := Value;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetFormFolder(const Value: string);
begin
  if ( Value <> cxscbFormFolder.AbsolutePath ) then
  begin
    cxscbFormFolder.AbsolutePath := Value;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetProjectName(const Value: String);
begin
  if ( Value <> FProjectName ) then
  begin
    FProjectName := Value;
    cxlblProject.Caption := FProjectName;

    LoadSettingsFromRegistry;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetProjectPath(const Value: String);
begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Self, 'SetProjectPath' );
  {$ENDIF}

  if ( Value <> FProjectPath ) and
     ( DirectoryExists( Value ) ) then
  begin
    FProjectPath := Value;
    cxscbProjectFolder.Properties.Root.CustomPath := FProjectPath;
    cxscbDataModuleFolder.Properties.Root.CustomPath := FProjectPath;
    cxscbFormFolder.Properties.Root.CustomPath := FProjectPath;

    DataModuleFolder := FProjectPath + 'DataModules\';
    FormFolder       := FProjectPath + 'Forms\';
  end;

  {$IFDEF CODESITE}
  CodeSite.ExitMethod( Self, 'SetProjectPath' );
  {$ENDIF}

end;

procedure TfrmPPWFrameWorkEntityWizard.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SaveSettingsToRegistry;
  Action := caFree;
end;

function TfrmPPWFrameWorkEntityWizard.GetBaseDataModule: string;
begin
  Result := cxtedtBaseDataModuleClass.Text;
end;

function TfrmPPWFrameWorkEntityWizard.GetBaseListView: string;
begin
  Result := cxtedtBaseListViewClass.Text;
end;

function TfrmPPWFrameWorkEntityWizard.GetBaseRecordView: string;
begin
  Result := cxtedtBaseRecordViewClass.Text;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetBaseDataModule(const Value: string);
begin
  if ( Value <> cxtedtBaseDataModuleClass.Text ) then
  begin
    cxtedtBaseDataModuleClass.Text := Value;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetBaseListView(const Value: string);
begin
  if ( Value <> cxtedtBaseListViewClass.Text ) then
  begin
    cxtedtBaseListViewClass.Text := Value;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetBaseRecordView(const Value: string);
begin
  if ( Value <> cxtedtBaseRecordViewClass.Text ) then
  begin
    cxtedtBaseRecordViewClass.Text := Value;
  end;
end;

function TfrmPPWFrameWorkEntityWizard.GetListViewCaption: string;
begin
  Result := cxtedtListViewCaption.Text;
end;

function TfrmPPWFrameWorkEntityWizard.GetRecordViewCaption: string;
begin
  Result := cxtedtRecordViewCaption.Text;
end;

procedure TfrmPPWFrameWorkEntityWizard.ValidateGeneralOptions;
begin
  if ( BaseDataModule = '' ) then
  begin
    Raise Exception.Create( 'Base DataModule Class should be filled in' );
  end;

  if ( BaseListView = '' ) then
  begin
    Raise Exception.Create( 'Base ListView Class should be filled in' );
  end;

  if ( BaseRecordView = '' ) then
  begin
    Raise Exception.Create( 'Base RecordView Class should be filled in' );
  end;

  if ( EntityName = '' ) then
  begin
    Raise Exception.Create( 'Entity Name should be filled in' );
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.acNextPageUpdate(Sender: TObject);
begin
  TAction( Sender ).Enabled := ( cxpgcOptions.ActivePageIndex <> Pred( cxpgcOptions.PageCount ) );
end;

procedure TfrmPPWFrameWorkEntityWizard.acPreviousPageUpdate(Sender: TObject);
begin
  TAction( Sender ).Enabled := ( cxpgcOptions.ActivePageIndex <> 0 );
end;

procedure TfrmPPWFrameWorkEntityWizard.acNextPageExecute(Sender: TObject);
begin
  cxpgcOptions.ActivePageIndex := Succ( cxpgcOptions.ActivePageIndex );
end;

procedure TfrmPPWFrameWorkEntityWizard.acPreviousPageExecute(Sender: TObject);
begin
  cxpgcOptions.ActivePageIndex := Pred( cxpgcOptions.ActivePageIndex );
end;

procedure TfrmPPWFrameWorkEntityWizard.acFinishWizardUpdate(Sender: TObject);
begin
//  TAction( Sender ).Enabled := ( cxpgcOptions.ActivePageIndex = Pred( cxpgcOptions.PageCount ) );
end;

procedure TfrmPPWFrameWorkEntityWizard.cxpgcOptionsPageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
  AllowChange := False;
  case cxpgcOptions.ActivePageIndex of
    0 : ValidateGeneralOptions;
    1 : ValidateListViewOptions;
    2 : ValidateRecordViewOptions;
  end;
  AllowChange := True;
end;

procedure TfrmPPWFrameWorkEntityWizard.acCancelWizardExecute(Sender: TObject);
begin
  Close;
end;

function TfrmPPWFrameWorkEntityWizard.GetListViewSQL: TStrings;
begin
  Result := cxmmoListSQL.Lines;
end;

function TfrmPPWFrameWorkEntityWizard.GetRecordViewSQL: TStrings;
begin
  Result := cxmmoRecordSQL.Lines;
end;

procedure TfrmPPWFrameWorkEntityWizard.ValidateListViewOptions;
begin

end;

procedure TfrmPPWFrameWorkEntityWizard.ValidateRecordViewOptions;
begin

end;

function TfrmPPWFrameWorkEntityWizard.GetEntityName: String;
begin
  Result := cxtedtEntityName.Text;
end;

function TfrmPPWFrameWorkEntityWizard.GetInitialiseListSQL: Boolean;
begin
  Result := cxcbInitialiseListViewSQL.Checked;
end;

function TfrmPPWFrameWorkEntityWizard.GetInitialiseRecordSQL: Boolean;
begin
  Result := cxcbInitialiseRecordViewSQL.Checked;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetInitialiseListSQL(const Value: Boolean);
begin
  if ( Value <> cxcbInitialiseListViewSQL.Checked ) then
  begin
    cxcbInitialiseListViewSQL.Checked := Value;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.SetInitialiseRecordSQL(const Value: Boolean);
begin
  if ( Value <> cxcbInitialiseRecordViewSQL.Checked ) then
  begin
    cxcbInitialiseRecordViewSQL.Checked := Value;
  end;
end;

procedure TfrmPPWFrameWorkEntityWizard.FormCreate(Sender: TObject);
begin
  cxpgcOptions.ActivePage := cxtbsGeneral;
end;

procedure TfrmPPWFrameWorkEntityWizard.acFinishWizardExecute(
  Sender: TObject);
begin
  ValidateGeneralOptions;
  ValidateListViewOptions;
  ValidateRecordViewOptions;
  Close;
  ModalResult := mrOK;
end;

end.
