{*****************************************************************************
  This unit contains the actuall AddDataAwareControls Wizard Form.

  @Name       Form_PPWFrameWorkAddDataAwareControlsWizard
  @Author     slesage
  @Copyright  (c) 2004 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  17/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_PPWFrameWorkAddDataAwareControlsWizard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ExtCtrls, DBClient, cxLookAndFeels,
  cxLookAndFeelPainters, StdCtrls, cxButtons, cxEditRepositoryItems, Vcl.Menus,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator;

type
  TVisibleFields = ( vfAll, vfNone, vfInvert );

  TfrmPPWFrameWorkAddDataAwareControlsWizard = class(TForm)
    cdsRecordFields: TClientDataSet;
    cdsRecordFieldsF_FIELD_ID: TAutoIncField;
    cdsRecordFieldsF_FIELD_NAME: TStringField;
    cdsRecordFieldsF_FIELD_VISIBLE: TBooleanField;
    cdsRecordFieldsF_FIELD_COMPONENT: TStringField;
    srcRecordFields: TDataSource;
    pnlButtons: TPanel;
    cxgrdtblvRecordFields: TcxGridDBTableView;
    cxgrdlvlRecordFields: TcxGridLevel;
    cxgrdRecordFields: TcxGrid;
    cxgrdtblvRecordFieldsF_FIELD_ID: TcxGridDBColumn;
    cxgrdtblvRecordFieldsF_FIELD_NAME: TcxGridDBColumn;
    cxgrdtblvRecordFieldsF_FIELD_VISIBLE: TcxGridDBColumn;
    cxgrdtblvRecordFieldsF_FIELD_COMPONENT: TcxGridDBColumn;
    cxlfCxLookAndFeelController1: TcxLookAndFeelController;
    cxbtnCxButton1: TcxButton;
    cxbtnCxButton2: TcxButton;
    cxEditRepository1: TcxEditRepository;
    cxeriDBAwareComponentClassNames: TcxEditRepositoryComboBoxItem;
    cxbtnSelectAll: TcxButton;
    cxbtnDeselectAll: TcxButton;
    cxbtnToggleSelection: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxbtnSelectAllClick(Sender: TObject);
    procedure cxbtnDeselectAllClick(Sender: TObject);
    procedure cxbtnToggleSelectionClick(Sender: TObject);
  private
    FRecordDataSet: TDataSet;
    function GetSettingsDataSet: TDataSet;
    { Private declarations }
  protected
    function GetDefaultDBAwareComponentClassNames : String; virtual;
    function GetDefaultDBAwareComponentClassName( aField : TField ) : String; virtual;
    procedure ChangeVisibleFields( aVisibleFields : TVisibleFields ); virtual;
  public
    { Public declarations }
    procedure InitialiseSettings; virtual;

    function GetDBAwareComponentClass( aField : TField ) : TComponentClass; virtual;
    function GetDBAwareComponentVisible( aField : TField ) : Boolean;

    property RecordDataSet   : TDataSet read FRecordDataSet write FRecordDataSet;
    property SettingsDataSet : TDataSet read GetSettingsDataSet;
  end;

  TDBAwareControlWizardClass = Class of TfrmPPWFrameWorkAddDataAwareControlsWizard;

var
  frmPPWFrameWorkAddDataAwareControlsWizard: TfrmPPWFrameWorkAddDataAwareControlsWizard;

implementation

uses
  cxDBEdit, cxDropDownEdit;

{$R *.dfm}

{ TfrmAddDataAwareControlsWizard }

{*****************************************************************************
  This function will return the TComponentClass that should be used to create
  the DataAware component.  The result depends on the given Field and the
  options the user indicated in the Wizard form.

  @Name       TfrmAddDataAwareControlsWizard.GetDBAwareComponentClass
  @author     slesage
  @param      aField   The Field for which you want to return the DBAware
                       ComponentClass that should be used.
  @return     Depending on the given aField and the settings the user provided
              in the Wizard, this function will return the TComponentClass of
              the DataAware component that should be created for this field.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPPWFrameWorkAddDataAwareControlsWizard.GetDBAwareComponentClass(
  aField: TField): TComponentClass;
begin
  if ( cdsRecordFields.Locate( 'F_FIELD_NAME', aField.FieldName, [] ) ) then
  begin
    Result := TComponentClass( FindClass( cdsRecordFieldsF_FIELD_COMPONENT.AsString ) );
  end
  else
  begin
    Result := TcxDBTextEdit;
  end;
end;

{*****************************************************************************
  Returns the ClassName of the DBAware control that should be used as default
  for the given field type.

  @Name       TfrmAddDataAwareControlsWizard.GetDBAwareComponentClassName
  @author     slesage
  @param      aField   The field for which you want to get the default DBAware
                       component's ClassName.
  @return     Returns the ClassName of the DBAware component that should be
              used by default for the given aField.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPPWFrameWorkAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassName(
  aField: TField): String;
begin
  if ( Assigned( aField ) ) then
  begin
    case aField.DataType of
      ftString, ftAutoInc, ftFixedChar, ftWideString : Result := 'TcxDBTextEdit';
      ftSmallint, ftInteger, ftWord, ftLargeint      : Result := 'TcxDBCalcEdit';
      ftBoolean                                      : Result := 'TcxDBCheckBox';
      ftFloat, ftBCD                                 : Result := 'TcxDBCalcEdit';
      ftCurrency                                     : Result := 'TcxDBCurrencyEdit';
      ftDate, ftDateTime                             : Result := 'TcxDBDateEdit';
      ftTime                                         : Result := 'TcxDBTimeEdit';
      ftBlob                                         : Result := 'TcxDBBlobEdit';
      ftMemo                                         : Result := 'TcxDBMemo';
      ftGraphic                                      : Result := 'TcxDBImage';
      ftFmtMemo                                      : Result := 'TcxDBRichEdit';
      ftGuid                                         : Result := 'TcxDBButtonEdit';
      else Result := 'TcxDBTextEdit';
    end;
  end;
end;

{*****************************************************************************
  This function will be used to determine if the user wants to show the given
  aField on the RecordView From.

  @Name       TfrmAddDataAwareControlsWizard.GetDBAwareComponentVisible
  @author     slesage
  @param      aField   The field for which you want to check if the user
                       wants to see the field on the RecordView.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPPWFrameWorkAddDataAwareControlsWizard.GetDBAwareComponentVisible(
  aField: TField): Boolean;
begin
  if ( cdsRecordFields.Locate( 'F_FIELD_NAME', aField.FieldName, [] ) ) then
  begin
    Result := cdsRecordFieldsF_FIELD_VISIBLE.AsBoolean;
  end
  else
  begin
    Result := False;
  end;
end;

{*****************************************************************************
  Property Getter for the SettingsDataSet property.

  @Name       TfrmAddDataAwareControlsWizard.GetSettingsDataSet
  @author     slesage
  @param      None
  @return     Returns the DataSet in which the Settings for all fields are
              stored.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPPWFrameWorkAddDataAwareControlsWizard.GetSettingsDataSet: TDataSet;
begin
  Result := cdsRecordFields;
end;

{*****************************************************************************
  This method will be used to create the SettingsDataSet and fill in the
  defaults for all the fields in the RecordDataSet.

  @Name       TfrmAddDataAwareControlsWizard.InitialiseSettings
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkAddDataAwareControlsWizard.InitialiseSettings;
var
  lcv    : Integer;
  aField : TField;
begin
  if ( Assigned( RecordDataSet ) ) then
  begin
    cdsRecordFields.CreateDataSet;
    for lcv := 0 to Pred( RecordDataSet.Fields.Count ) do
    begin
      aField := RecordDataSet.Fields[ lcv ];

      cdsRecordFields.Append;
      cdsRecordFieldsF_FIELD_NAME.AsString := aField.FieldName;
      cdsRecordFieldsF_FIELD_VISIBLE.AsBoolean := aField.Visible;
      cdsRecordFieldsF_FIELD_COMPONENT.AsString := GetDefaultDBAwareComponentClassName( aField );
      cdsRecordFields.Post;
    end;
  end;
end;

{*****************************************************************************
  This function will return a list of default DB Aware Component ClassNames.

  @Name       TfrmPPWFrameWorkAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassNames
  @author     slesage
  @param      None
  @return     A list containing all DB Aware Component ClassNames that can be
              used in the Wizard.
  @Exception  None
  @See        None
******************************************************************************}

function TfrmPPWFrameWorkAddDataAwareControlsWizard.GetDefaultDBAwareComponentClassNames: String;
begin
  Result := 'TcxDBBlobEdit' + #13#10 +
            'TcxDBButtonEdit' + #13#10 +
            'TcxDBCalcEdit' + #13#10 +
            'TcxDBCheckBox' + #13#10 +
            'TcxDBCheckGroup' + #13#10 +
            'TcxDBComboBox' + #13#10 +
            'TcxDBCurrencyEdit' + #13#10 +
            'TcxDBDateEdit' + #13#10 +
            'TcxDBHyperLinkEdit' + #13#10 +
            'TcxDBImage' + #13#10 +
            'TcxDBImageComboBox' + #13#10 +
            'TcxDBLabel' + #13#10 +
            'TcxDBLookupComboBox' + #13#10 +
            'TcxDBMaskEdit' + #13#10 +
            'TcxDBMemo' + #13#10 +
            'TcxDBProgressBar' + #13#10 +
            'TcxDBRadioGroup' + #13#10 +
            'TcxDBRichEdit' + #13#10 +
            'TcxDBSpinEdit' + #13#10 +
            'TcxDBTextEdit' + #13#10 +
            'TcxDBTimeEdit';
end;

{*****************************************************************************
  This method will be executed when the form is created, and will be used to
  initialise some settings.

  @Name       TfrmPPWFrameWorkAddDataAwareControlsWizard.FormCreate
  @author     slesage
  @param      Sender   The Object from which the Method is Invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkAddDataAwareControlsWizard.FormCreate(
  Sender: TObject);
begin
  cxeriDBAwareComponentClassNames.Properties.Items.Text := GetDefaultDBAwareComponentClassNames;
end;

procedure TfrmPPWFrameWorkAddDataAwareControlsWizard.cxbtnSelectAllClick(
  Sender: TObject);
begin
  ChangeVisibleFields( vfAll );
end;

procedure TfrmPPWFrameWorkAddDataAwareControlsWizard.ChangeVisibleFields(
  aVisibleFields: TVisibleFields);
var
  aBookMark : TBookmark;
begin
  aBookMark := cdsRecordFields.GetBookmark;

  cdsRecordFields.DisableControls;
  cdsRecordFields.First;
  while not cdsRecordFields.Eof do
  begin
    cdsRecordFields.Edit;
    case aVisibleFields of
      vfAll    : cdsRecordFieldsF_FIELD_VISIBLE.AsBoolean := True;
      vfNone   : cdsRecordFieldsF_FIELD_VISIBLE.AsBoolean := False;
      vfInvert : cdsRecordFieldsF_FIELD_VISIBLE.AsBoolean := Not cdsRecordFieldsF_FIELD_VISIBLE.AsBoolean;
    end;
    cdsRecordFields.Post;
    cdsRecordFields.Next;
  end;

  cdsRecordFields.GotoBookmark(aBookMark);
  cdsRecordFields.FreeBookmark(aBookMark);
  cdsRecordFields.EnableControls;

end;

procedure TfrmPPWFrameWorkAddDataAwareControlsWizard.cxbtnDeselectAllClick(
  Sender: TObject);
begin
  ChangeVisibleFields( vfNone );
end;

procedure TfrmPPWFrameWorkAddDataAwareControlsWizard.cxbtnToggleSelectionClick(
  Sender: TObject);
begin
  ChangeVisibleFields( vfInvert );
end;

end.
