{*****************************************************************************
  Name           : GetMasterRecordView
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This unit contains the Base ListView form.  This form
                   contains no components yet, and should be used as a base
                   to construct Project or Client Dependant Base ListView forms.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/12/2004   sLesage              Added the AsDetail property which will
                                    indicate if the ListView is used as a
                                    Detail of another Entity.
  29/11/2004   sLesage              Reworked the Constructor of the form.
  28/10/2003   sLesage              The EditingRecordViewCount
                                    function has been moved from the
                                    template forms to the FrameWork.
  29/08/2003   sLesage              Added a property getter for the Parent
                                    property ( Was needed because we want to
                                    access the parent from the
                                    IPPWFrameWorkListView interface ).
  17/03/2003   sLesage              Added support for catching the
                                    WM_MASTERRECORDVIEWMODECHANGED message.
  10/12/2002   sLesage              Added support for sowing a ListView form
                                    for selection of records.
  6/12/2002    sLesage              Added the RefreshData Method which can
                                    be used to refresh the Data in the ListView
                                    Form.
  29/11/2002   sLesage              Cleaned up unnecessary Code and added
                                    Unit / Procedure Headers.
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkListView;

interface

uses
  Classes, Messages, Controls, Forms,
  Unit_PPWFrameWorkForm,
  Unit_PPWFrameWorkInterfaces, Unit_PPWFrameWorkClasses;

type
  TPPWFrameWorkListView = class(TPPWFrameWorkForm, IPPWFrameWorkListView)
  private
    FMasterRecordView : IPPWFrameWorkRecordView;
    FAsSelection      : Boolean;
    FAsDetail: Boolean;
    { Private declarations }
  protected
    { Property Getters }
    function GetAsSelection: Boolean; virtual;
    function GetMasterRecordView: IPPWFrameWorkRecordView; virtual;
    function GetParent : TWincontrol; virtual;
    function EditingRecordViewCount : Integer; virtual;

    { Property Setters }
    procedure SetAsDetail(const Value: Boolean); virtual;
    procedure SetAsSelection(const Value: Boolean); virtual;
    procedure SetMasterRecordView(const Value: IPPWFrameWorkRecordView); virtual;

    { Methods }
    procedure ApplyFilter; virtual;
    procedure CancelFilter; virtual;
    procedure RefreshData; virtual;

    procedure MasterRecordViewModeChanged; virtual;
    procedure UpdateFormCaption; override;

    procedure RegisterSelf; override;
    procedure UnRegisterSelf; override;

    property AsSelection      : Boolean           read GetAsSelection
                                                  write SetAsSelection;
    property MasterRecordView : IPPWFrameWorkRecordView
                                                  read GetMasterRecordView
                                                  write SetMasterRecordView;
  public
    { Public declarations }
    constructor Create( aOwner : TComponent; aDataModule : IPPWFrameWorkDataModule = nil; aAsDetail : Boolean = False; aRecordView : IPPWFrameWorkRecordView = Nil ); reintroduce; virtual;
    destructor Destroy; override;
    procedure Loaded; override;

    property AsDetail : Boolean read FAsDetail write SetAsDetail;
  published
    property Align;
    property Caption;
    property DataSource;
    property FormStyle;
    property Registered;
  end;

  TPPWFrameWorkListViewClass = class of TPPWFrameWorkListView;

var
  PPWFrameWorkListView: TPPWFrameWorkListView;

implementation

uses
  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects,
  {$ENDIF}
  Unit_PPWFrameWorkController;

{$R *.dfm}

{ TPPWFrameWorkListView }

{*****************************************************************************
  Name           : TPPWFrameWorkListView.ApplyFilter 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method is supplied so we can call the DoApplyFilter
                   method on the DataModule directly from within the ListView
                   form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.ApplyFilter;
begin
  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    FrameWorkDataModule.DoApplyFilter;
    UpdateFormCaption;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.CancelFilter 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method is supplied so we can call the DoCancelFilter
                   method on the DataModule directly from within the ListView
                   form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.CancelFilter;
begin
  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    FrameWorkDataModule.DoCancelFilter;
    UpdateFormCaption;
  end;
end;

{*****************************************************************************
  New Constructor for the PPWFrameWorkListView.

  @Name       TPPWFrameWorkListView.Create
  @author     slesage
  @param      aOwner        The Owner of the DataModule.
  @param      aDataModule   The DataModule for which the FrameWorkForm must
                            be Created.
  @param      aAsDetail     Boolean indicating if the DataModule is created as
                            a Detail of another DataModule.
  aParam      aRecordView   The RecordView of the MasterDataModule for which the
                            Detail DataModule is Created.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TPPWFrameWorkListView.Create( aOwner : TComponent;
                                          aDataModule : IPPWFrameWorkDataModule = Nil;
                                          aAsDetail : Boolean = False;
                                          aRecordView : IPPWFrameWorkRecordView = Nil );
begin
  if ( aAsDetail ) then
  begin
    FMasterRecordView := aRecordView;
  end;

  Inherited Create( AOwner, aDataModule );

  AsDetail := aAsDetail;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.Destroy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Overridden Destructor in which we will Clean Up some
                   things.
  History        :

  Date         By                   Description
  ----         --                   -----------
  18/03/2003   Pascal Legroux       Put the Litview assignment to nil in a
                                    condition (otherwise troubles in designtime)
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

destructor TPPWFrameWorkListView.Destroy;
begin
  if not (csDesigning in ComponentState) then
  begin
    FrameWorkDataModule.ListView := Nil;
    FMasterRecordView := Nil;
  end;
  inherited Destroy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.EditingRecordViewCount
  Author         : Wim Lambrechts
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : function returns the number of recordviews that are opened
                   for this listview and that have a mode rvmAdd, rvmDelete, rvmEdit
                   When no recordviews were found, returns 0
  History        :

  Date         By                   Description
  ----         --                   -----------
  13/02/2003   Wim Lambrechts       Initial creation of the Procedure.
 *****************************************************************************}

function TPPWFrameWorkListView.EditingRecordViewCount: Integer;
var
  mrv: IPPWFrameWorkRecordView;
  lcv: Integer;
begin
  result := 0;
  if (assigned( self.FrameWorkDatamodule)) and
     (assigned( self.FrameWorkDataModule.RecordViews)) then
  begin
    for lcv := 0 to self.FrameWorkDataModule.RecordViews.count - 1 do
    begin
      mrv := (self.FrameWorkDataModule.RecordViews[lcv] as IPPWFrameWorkRecordView);
      if mrv.Mode in [rvmAdd, rvmDelete, rvmEdit] then result := result + 1;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.GetAsSelection
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the AsSelection Property.
  Exceptions     : None
  Description    : Property Getter for the AsSelection property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkListView.GetAsSelection: Boolean;
begin
  Result := FAsSelection;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.GetMasterRecordView
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : The Current Value of the MasterRecordView Property.
  Exceptions     : None
  Description    : Property Getter for the MasterRecordView property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

function TPPWFrameWorkListView.GetMasterRecordView: IPPWFrameWorkRecordView;
begin
  Result := FMasterRecordView;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.GetParent 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value for the Parent Property.
  Exceptions     : None
  Description    : Property getter for the Parent property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/08/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkListView.GetParent: TWincontrol;
begin
  Result := Inherited Parent;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.Loaded 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Performs fix-ups when the form is first loaded into memory.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  24/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.Loaded;
begin
  inherited Loaded;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.RefreshData 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to refresh the contents of the
                   ListViewForm internally it will call the RefreshData method
                   from the DataModule.
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.MasterRecordViewModeChanged;
begin
 //
end;

procedure TPPWFrameWorkListView.RefreshData;
begin
  if ( Assigned( FrameWorkDataModule ) ) then
  begin
    FrameWorkDataModule.RefreshListDataSet;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.RegisterSelf
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This method is used by the FrameWork to Register the
                   ListView with the Controller, so it appears in the
                   Property Editors.  You should't call this method
                   directly sice it is done automatically by the FrameWork
                   at DesignTime.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.RegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWListView.EnterMethod( Self, 'TPPWFrameWorkListView.RegisterSelf' );
  csFWListView.SendString( 'ListView.ClassName', Self.ClassName );
  {$ENDIF}

  Inherited RegisterSelf;

  if ( Assigned( PPWController ) ) then
  begin
    PPWController.RegisterFrameWorkClass( TPPWFrameWorkListViewClass( Self.Classtype ) );
  end;

  {$IFDEF CODESITE}
  csFWListView.ExitMethod( Self, 'TPPWFrameWorkListView.RegisterSelf' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.SetAsSelection 
  Author         : Stefaan Lesage
  Arguments      : Value - The New Value for the AsSelection property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AsSelection Property.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  10/12/2002   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

procedure TPPWFrameWorkListView.SetAsDetail(const Value: Boolean);
begin
  if ( FAsDetail <> Value ) then
  begin
    FAsDetail := Value;
  end;
end;

procedure TPPWFrameWorkListView.SetAsSelection(const Value: Boolean);
begin
  if ( Value <> FAsSelection ) then
  begin
    FAsSelection := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.SetMasterRecordView 
  Author         : Stefaan Lesage
  Arguments      : Value - The New Value for the MasterRecordView property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the MasterRecordView Property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.SetMasterRecordView(
  const Value: IPPWFrameWorkRecordView);
begin
  FMasterRecordView := Value;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListView.UnRegisterSelf
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method is used by the FrameWork to UnRegister the
                   ListView with the Controller, so it appears in the
                   Property Editors.  You should't call this method
                   directly sice it is done automatically by the FrameWork
                   at DesignTime.
  History        :

  Date         By                   Description
  ----         --                   -----------
  29/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

procedure TPPWFrameWorkListView.UnRegisterSelf;
begin
  {$IFDEF CODESITE}
  csFWListView.EnterMethod( Self, 'TPPWFrameWorkListView.UnRegisterSelf' );
  csFWListView.SendString( 'ListView.ClassName', Self.ClassName );
  {$ENDIF}

  Inherited UnRegisterSelf;

  if ( Assigned( PPWController ) ) then
  begin
    PPWController.UnRegisterFrameWorkClass( TPPWFrameWorkListViewClass( Self.Classtype ) );
  end;
  
  {$IFDEF CODESITE}
  csFWListView.ExitMethod( Self, 'TPPWFrameWorkListView.UnRegisterSelf' );
  {$ENDIF}
end;

procedure TPPWFrameWorkListView.UpdateFormCaption;
begin
  if ( Assigned( FrameWorkDataModule ) ) and
     ( FrameWorkDataModule.Filtered ) then
  begin
    Caption := OriginalCaption + ' ( Filtered )';
  end
  else
  begin
    Caption := OriginalCaption;
  end;
end;

end.
