{*****************************************************************************
  This unit contiains the Wizard that will be used to initialise the
  Perisitent Fields on the DataModule.

  @Name       Form_CMBSetupDataModuleWizard
  @Author     slesage
  @Copyright  (c) 2004 PeopleWare.
  @History

  Date         By                   Description
  ----         --                   -----------
  09/12/2004   sLesage              Modified the wizard a bit so that the
                                    developer can also indicate which fields
                                    should be used as KeyFields on the
                                    DataModule.
  18/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Form_PPWFrameWorkPersistentFieldsWizard;

interface

uses
  Forms, cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, Classes, ActnList,
  cxGridTableView, DBClient, cxLookAndFeels, cxGridLevel,
  cxGridCustomTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls, cxButtons, Controls, ExtCtrls, Vcl.Menus,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxNavigator, System.Actions;

type
  TfrmPPWFrameWorkPersistentFieldsWizard = class(TForm)
    pnlButtons: TPanel;
    cxbtnCancel: TcxButton;
    cxbtnFinish: TcxButton;
    cxlfMain: TcxLookAndFeelController;
    cdsListFieldSettings: TClientDataSet;
    cdsListFieldSettingsF_FIELD_ID: TAutoIncField;
    cdsListFieldSettingsF_FIELD_NAME: TStringField;
    cdsListFieldSettingsF_FIELD_CAPTION: TStringField;
    cdsListFieldSettingsF_FIELD_VISIBLE: TBooleanField;
    srcListFieldSettings: TDataSource;
    cdsRecordFieldSettings: TClientDataSet;
    cdsRecordFieldSettingsF_FIELD_ID: TAutoIncField;
    cdsRecordFieldSettingsF_FIELD_NAME: TStringField;
    cdsRecordFieldSettingsF_FIELD_CAPTION: TStringField;
    BooleanField1: TBooleanField;
    srcRecordFieldSettings: TDataSource;
    cdsRecordFieldSettingsF_FIELD_READONLY: TBooleanField;
    cdsRecordFieldSettingsF_FIELD_REQUIRED: TBooleanField;
    cxsrCxStyleRepository1: TcxStyleRepository;
    cxsContent: TcxStyle;
    cxsContentOdd: TcxStyle;
    cxssDefaultTableView: TcxGridTableViewStyleSheet;
    cxgrdtblvListViewOptions: TcxGridDBTableView;
    cxgrdlvlListFieldOptions: TcxGridLevel;
    cxgrdFieldOptions: TcxGrid;
    cxgrdlvlRecordFieldOptions: TcxGridLevel;
    cxgrdtblvRecordViewOptions: TcxGridDBTableView;
    cxgrdtblvListViewOptionsF_FIELD_ID: TcxGridDBColumn;
    cxgrdtblvListViewOptionsF_FIELD_NAME: TcxGridDBColumn;
    cxgrdtblvListViewOptionsF_FIELD_CAPTION: TcxGridDBColumn;
    cxgrdtblvListViewOptionsF_FIELD_VISIBLE: TcxGridDBColumn;
    cxgrdtblvRecordViewOptionsF_FIELD_ID: TcxGridDBColumn;
    cxgrdtblvRecordViewOptionsF_FIELD_NAME: TcxGridDBColumn;
    cxgrdtblvRecordViewOptionsF_FIELD_CAPTION: TcxGridDBColumn;
    cxgrdtblvRecordViewOptionsF_FIELD_VISIBLE: TcxGridDBColumn;
    cxgrdtblvRecordViewOptionsF_FIELD_REQUIRED: TcxGridDBColumn;
    cxgrdtblvRecordViewOptionsF_FIELD_READONLY: TcxGridDBColumn;
    alActionList1: TActionList;
    acNextFinish: TAction;
    cdsListFieldSettingsF_FIELDINPK: TBooleanField;
    cxgrdtblvListViewOptionsF_FIELDINPK: TcxGridDBColumn;
    procedure cxgrdFieldOptionsFocusedViewChanged(Sender: TcxCustomGrid;
      APrevFocusedView, AFocusedView: TcxCustomGridView);
    procedure acNextFinishUpdate(Sender: TObject);
    procedure acNextFinishExecute(Sender: TObject);
  private
    FListDataSet: TDataSet;
    FRecordDataSet: TDataSet;
    procedure SetListDataSet(const Value: TDataSet);
    procedure SetRecordDataSet(const Value: TDataSet);
    { Private declarations }
  protected
    function GetKeyFields: string;           virtual;

    procedure InitialiseListFieldSettings;   virtual;
    procedure InitialiseRecordFieldSettings; virtual;
    procedure UpdateRecordFieldCaptions;     virtual;
  public
    { Public declarations }
    property ListDataSet   : TDataSet read FListDataSet   write SetListDataSet;
    property RecordDataSet : TDataSet read FRecordDataSet write SetRecordDataSet;
    property KeyFields     : string   read GetKeyFields;
  end;

var
  frmPPWFrameWorkPersistentFieldsWizard: TfrmPPWFrameWorkPersistentFieldsWizard;

implementation
{$IFDEF CODESITE}
uses
  csIntf;
{$ENDIF}

{$R *.dfm}

{ TfrmPPWFrameWorkPersistentFieldsWizard }

{*****************************************************************************
  This method is used to fill the ListFieldSettings dataset with Default Values.

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.InitialiseListFieldSettings
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.InitialiseListFieldSettings;
var
  lcv : Integer;
begin
  if ( Assigned( FListDataSet ) ) then
  begin
    cdsListFieldSettings.CreateDataSet;

    for lcv := 0 to Pred( FListDataSet.FieldCount ) do
    begin
      cdsListFieldSettings.Append;
      cdsListFieldSettings.FieldByName( 'F_FIELD_NAME' ).AsString     := FListDataSet.Fields[ lcv ].FieldName;
      cdsListFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString  := FListDataSet.Fields[ lcv ].DisplayLabel;
      cdsListFieldSettings.FieldByName( 'F_FIELD_VISIBLE' ).AsBoolean := FListDataSet.Fields[ lcv ].Visible;
      cdsListFieldSettings.FieldByName( 'F_FIELDINPK' ).AsBoolean := pfInKey in FListDataSet.Fields[ lcv ].ProviderFlags;
      cdsListFieldSettings.Post;
    end;

    cdsListFieldSettings.First;
  end;
end;

{*****************************************************************************
  This method is used to fill the RecordFieldSettings dataset with Default Values.

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.InitialiseRecordFieldSettings
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.InitialiseRecordFieldSettings;
var
  lcv : Integer;
begin
  if ( Assigned( FRecordDataSet ) ) then
  begin
    cdsRecordFieldSettings.CreateDataSet;

    for lcv := 0 to Pred( FRecordDataSet.FieldCount ) do
    begin
      cdsRecordFieldSettings.Append;
      cdsRecordFieldSettings.FieldByName( 'F_FIELD_NAME' ).AsString     := FRecordDataSet.Fields[ lcv ].FieldName;
      cdsRecordFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString := FRecordDataSet.Fields[ lcv ].DisplayLabel;
      cdsRecordFieldSettings.FieldByName( 'F_FIELD_VISIBLE' ).AsBoolean := FRecordDataSet.Fields[ lcv ].Visible;
      cdsRecordFieldSettings.FieldByName( 'F_FIELD_READONLY' ).AsBoolean := FRecordDataSet.Fields[ lcv ].ReadOnly;
      cdsRecordFieldSettings.FieldByName( 'F_FIELD_REQUIRED' ).AsBoolean := FRecordDataSet.Fields[ lcv ].Required;
      cdsRecordFieldSettings.Post;
    end;

    UpdateRecordFieldCaptions;
    cdsRecordFieldSettings.First;
  end;
end;

{*****************************************************************************
  Property Setter for the ListDataSet Property

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.SetListDataSet
  @author     slesage
  @param      Value   The new Value for the ListDataSet Property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.SetListDataSet(
  const Value: TDataSet);
begin
  if ( Value <> FListDataSet ) then
  begin
    FListDataSet := Value;
    InitialiseListFieldSettings;
  end;
end;

{*****************************************************************************
  Property Setter for the RecordDataSet Property

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.SetRecordDataSet
  @author     slesage
  @param      Value   The new Value for the RecordDataSet Property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.SetRecordDataSet(
  const Value: TDataSet);
begin
  if ( Value <> FRecordDataSet ) then
  begin
    FRecordDataSet := Value;
    InitialiseRecordFieldSettings;
  end;
end;

{*****************************************************************************
  This method will copy the captions from the ListFields to the RecordFields
  if necessary.

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.UpdateRecordFieldCaptions
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.UpdateRecordFieldCaptions;
var
  aFieldName    : String;
  aFieldCaption : String;
  aListBookMark : TBookmark;
  aRecBookMark  : TBookmark;
begin
  { Store bookmarks so we can reposition both DataSets }
  aRecBookMark  := cdsRecordFieldSettings.GetBookmark;
  aListBookMark := cdsListFieldSettings.GetBookmark;

  { Now loop over each field and check if we need to copy the caption from
    the ListFieldSettings into the RecordFieldSettings }
  cdsRecordFieldSettings.DisableControls;
  Try
    cdsRecordFieldSettings.First;
    while not cdsRecordFieldSettings.Eof do
    begin
      aFieldName    := cdsRecordFieldSettings.FieldByName( 'F_FIELD_NAME' ).AsString;
      aFieldCaption := cdsRecordFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString;

      if cdsListFieldSettings.Locate( 'F_FIELD_NAME', aFieldName , [ loCaseInsensitive ] ) then
      begin
        if ( cdsListFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString <> aFieldCaption ) then
        begin
          cdsRecordFieldSettings.Edit;
          cdsRecordFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString := cdsListFieldSettings.FieldByName( 'F_FIELD_CAPTION' ).AsString;
          cdsRecordFieldSettings.Post;
        end;
      end;
      cdsRecordFieldSettings.Next;
    end;
  finally
    { Restore the Bookmarks }
    cdsListFieldSettings.GoToBookmark(aListBookMark);
    cdsListFieldSettings.FreeBookmark(aListBookMark);
    cdsRecordFieldSettings.EnableControls;
    cdsRecordFieldSettings.GoToBookmark(aRecBookMark);
    cdsRecordFieldSettings.FreeBookmark(aRecBookMark);
  end;
end;

{*****************************************************************************
  When the form is created, we will make sure the correct TabSheet is Active.

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.FormCreate
  @author     slesage
  @param      Sender             The Object from which the Method is Invoked.
  @param      aPrevFocusedView   The Previously focused view in the Grid.
  @param      aFocusedView       The Currently focused view in the Grid.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.cxgrdFieldOptionsFocusedViewChanged(
  Sender: TcxCustomGrid; APrevFocusedView,
  AFocusedView: TcxCustomGridView);
begin
  if ( AFocusedView = cxgrdtblvRecordViewOptions ) then
  begin
    UpdateRecordFieldCaptions;
  end;
end;

{*****************************************************************************
  This method is executed when the NextFinish Action gets updated.  It will
  be used to set the Caption of the Action depending on the active Grid
  View.

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.acNextFinishUpdate
  @author     slesage
  @param      Sender   The Object from which the Method is Invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.acNextFinishUpdate(Sender: TObject);
begin
  if ( cxgrdFieldOptions.ActiveView = cxgrdtblvRecordViewOptions ) then
  begin
    TAction( Sender ).Caption := '&Finish';
  end
  else
  begin
    TAction( Sender ).Caption := '&Next';
  end;
end;

{*****************************************************************************
  This method will be executed when the NextFinish action gets executed.  It
  will be used to activate the RecordFieldOptions View or Close the form,
  depending on the currently active View.

  @Name       TfrmPPWFrameWorkPersistentFieldsWizard.acNextFinishExecute
  @author     slesage
  @param      Sender   The Object from which the Method is Invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TfrmPPWFrameWorkPersistentFieldsWizard.acNextFinishExecute(
  Sender: TObject);
begin
  if ( cxgrdFieldOptions.ActiveView = cxgrdtblvRecordViewOptions ) then
  begin
    Close;
    ModalResult := mrOk;
  end
  else
  begin
    cxgrdFieldOptions.ActiveLevel := cxgrdlvlRecordFieldOptions;
    cxgrdFieldOptions.SetFocus;
  end;
end;

function TfrmPPWFrameWorkPersistentFieldsWizard.GetKeyFields: string;
var
  aListBookMark : TBookMark;
  aKeyFields    : String;

  procedure AddFieldToKeyFields( aFieldName : String );
  begin
    {$IFDEF CODESITE}
    CodeSite.EnterMethod( Self, 'AddFieldToKeyFields' );
    CodeSite.SendString( 'aKeyFields', aKeyFields );
    CodeSite.SendString( 'aFieldName', aFieldName );
    {$ENDIF}

    if ( aKeyFields <> '' ) then
    begin
      aKeyFields := aKeyFields + '; ';
    end;
    aKeyFields := aKeyFields + aFieldName;

    {$IFDEF CODESITE}
    CodeSite.SendString( 'Result', Result );
    CodeSite.ExitMethod( Self, 'AddFieldToKeyFields' );
    {$ENDIF}
  end;

begin
  {$IFDEF CODESITE}
  CodeSite.EnterMethod( Self, 'AddFieldToKeyFields' );
  {$ENDIF}

  aListBookMark := cdsListFieldSettings.GetBookmark;

  Result := '';

  cdsListFieldSettings.DisableControls;
  Try
    cdsListFieldSettings.First;
    while not cdsListFieldSettings.Eof do
    begin
      if ( cdsListFieldSettings.FieldByName( 'F_FIELDINPK' ).AsBoolean ) then
      begin
        AddFieldToKeyFields( cdsListFieldSettings.FieldByName( 'F_FIELD_NAME' ).AsString );
      end;
      cdsListFieldSettings.Next;
      Result := aKeyFields;
    end;
  Finally
    cdsListFieldSettings.EnableControls;
    cdsListFieldSettings.GoToBookmark(aListBookMark);
    cdsListFieldSettings.FreeBookmark(aListBookMark);
  end;

  {$IFDEF CODESITE}
  CodeSite.SendString( 'Result', Result );
  CodeSite.ExitMethod( Self, 'AddFieldToKeyFields' );
  {$ENDIF}
end;

end.
