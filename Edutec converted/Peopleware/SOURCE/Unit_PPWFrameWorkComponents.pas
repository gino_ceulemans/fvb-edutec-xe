{*****************************************************************************
  Name           : Unit_PPWFrameWorkComponents 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2003 Peopleware                                      
  Description    : This unit will contain some Custom DataSet components
                   which can be used in conjunction with our PPWFrameWork.                                                         
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  10/07/2003   slesage              Initial creation of the Unit.                  
 *****************************************************************************}

unit Unit_PPWFrameWorkComponents;

interface

uses
  DBClient, Classes,
  Unit_PPWFrameWorkInterfaces;

type
  TPPWFrameWorkClientDataSet = class( TClientDataSet, IPPWFrameWorkDataSet )
  private
    FAutoOpen : Boolean;
  protected
    function GetAutoOpen: Boolean; virtual;
    procedure SetAutoOpen(const Value: Boolean); virtual;
  public
    constructor Create( AOwner : TComponent ); override;
  published
    property Active             stored  False;
    property AutoOpen : Boolean read    GetAutoOpen write   SetAutoOpen default True;
  end;

implementation

{ TPPWFrameWorkClientDataSet }

{*****************************************************************************
  Name           : TPPWFrameWorkClientDataSet.Create
  Author         : Stefaan Lesage
  Arguments      : AOwner - The Owner of the Component
  Return Values  : None
  Exceptions     : None
  Description    : Constructor for the component in which we will initialise
                   some properties.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

constructor TPPWFrameWorkClientDataSet.Create(AOwner: TComponent);
begin
  FAutoOpen := True;
  inherited Create( AOwner );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkClientDataSet.GetAutoOpen
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the AutoOpen property.
  Exceptions     : None
  Description    : Property Getter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkClientDataSet.GetAutoOpen: Boolean;
begin
  Result := FAutoOpen;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkClientDataSet.SetAutoOpen
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the AutoOpen property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkClientDataSet.SetAutoOpen(const Value: Boolean);
begin
  if ( Value <> FAutoOpen ) then
  begin
    FAutoOpen := Value;
  end;
end;

end.
