{*****************************************************************************
  Name           : Unit_PPWFrameWorkActions
  Author         : Lesage Stefaan
  Copyright      : (c) 2002 Peopleware
  Description    : This unit will contain all the Actions that are going
                   to be used by the FrameWork
  History        :

  Date         By                   Description
  ----         --                   -----------
  12/09/2007   ivdbossche	          TPPWFrameWorkListViewDeleteAction.DoExecuteTarget(Target: TObject);
				                            : Added check for IIPWFrameWorkFormDeleteMultiSelect
  13/04/2005   sLesage              The ShowListView action will now check
                                    if there is still an instance for that
                                    ListView in memory instead of creating
                                    a new one.
  27/12/2004   sLesage              The ListViewEdit, ...Add, ...Delete,
                                    ...View actions will now be disabled if
                                    no new RecordView can be opened for the
                                    Active DataModule.
  18/11/2004   sLesage              Removed DevExpress specific things and
                                    implemented those as Interfaces.
  28/10/2003   sLesage              Reworked all FrameWorkActions.  The actions
                                    should now have the basic functionality to
                                    enable / disable them depending on certain
                                    conditions.  Prior to the Execution of the
                                    action the BeforeExecute event gets called.
                                    The developer can use this event to
                                    prohibit the execution of the action,
                                    eg. depending on the security.
  28/02/2003   Pascal Legroux       Added TWindowCloseAll
  10/12/2002   sLesage              The Edit, View, Delete and Add actions
                                    should be disabled when the form is shown
                                    in selection mode.
  06/12/2002   sLesage              Added an action which can be used to Refresh
                                    the dataset on a ListView.
  22/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkActions;

interface

uses
  { Delphi Units }
  ActnList,
  StdActns,
  { FrameWork Units }
  Unit_PPWFrameWorkInterfaces,
  Unit_PPWFrameWorkClasses;

type
  TPPWFrameWorkAction = class;

  TPPWBeforeExecuteEvent = procedure( Sender : TPPWFrameWorkAction; var CanExecute : Boolean ) of Object;
  TPPWBeforeUpdateEvent  = procedure( Sender : TPPWFrameWorkAction; var DisableAction : Boolean ) of Object;

  { Basic FrameWork Action }

  TPPWFrameWorkAction = class( TAction )
  private
    FBeforeExecute : TPPWBeforeExecuteEvent;
    FBeforeUpdate  : TPPWBeforeUpdateEvent;

    function GetFrameWorkDataModule     : IPPWFrameWorkDataModule; virtual;
    function GetFrameWorkForm           : IPPWFrameWorkForm;       virtual;
    function GetFrameWorkListViewForm   : IPPWFrameWorkListView;   virtual;
    function GetFrameWorkRecordViewForm : IPPWFrameWorkRecordView; virtual;
  protected
    procedure DoExecuteTarget( Target : TObject ); virtual;
    procedure DoUpdateTarget ( Target : TObject ); virtual;
  public
    Function DataModuleHasActiveDataSet  : Boolean;
    Function DataModuleDataSetHasRecords : Boolean;

    procedure ExecuteTarget( Target : TObject ); override;
    procedure UpdateTarget ( Target : TObject ); override;

    property FrameWorkDataModule     : IPPWFrameWorkDataModule read GetFrameWorkDataModule;
    property FrameWorkForm           : IPPWFrameWorkForm       read GetFrameWorkForm;
    property FrameWorkListViewForm   : IPPWFrameWorkListView   read GetFrameWorkListViewForm;
    property FrameWorkRecordViewForm : IPPWFrameWorkRecordView read GetFrameWorkRecordViewForm;
  published
    property BeforeExecute : TPPWBeforeExecuteEvent read FBeforeExecute write FBeforeExecute;
    property BeforeUpdate  : TPPWBeforeUpdateEvent  read FBeforeUpdate  write FBeforeUpdate;
  end;

  { Open ListView Action, will be used to open the ListView for a given
    FrameWorkDataModule }
  TPPWFrameWorkShowListViewAction = class( TPPWFrameWorkAction )
  private
    FDataModuleClass: TPPWDataModuleClassName;
    procedure SetDataModuleClass(const Value: TPPWDataModuleClassName);
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget(Target: TObject); override;
    function HandlesTarget(Target: TObject): Boolean; override;
  published
    property DataModuleClass : TPPWDataModuleClassName read FDataModuleClass
                                                       write SetDataModuleClass;
  end;

  { Basic ListView related Action }
  TPPWFrameWorkListViewAction = class( TPPWFrameWorkAction )
  protected
  public
    Function ListViewAsSelection                : Boolean;
    Function ListViewMasterRecordViewIsModal    : Boolean;
    Function ListViewMasterRecordViewIsViewing  : Boolean;
    Function ListViewMasterRecordViewIsDeleting : Boolean;
    Function ListViewMasterRecordViewIsEditing  : Boolean;
    Function ListViewMasterRecordViewIsAdding   : Boolean;
    procedure DoUpdateTarget( Target : TObject ); override;
    function HandlesTarget( Target : TObject ) : Boolean; override;
  end;

  { Refresh ListView Action }
  TPPWFrameWorkListViewRefreshAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Add Record ListView Action }
  TPPWFrameWorkListViewAddAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { View Record ListView Action }
  TPPWFrameWorkListViewConsultAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Delete Record ListView Action }
  TPPWFrameWorkListViewDeleteAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Edit Record ListView Action }
  TPPWFrameWorkListViewEditAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Apply Filter ListView Action }
  TPPWFrameWorkListViewFilterAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Cancel Filter ListView Action }
  TPPWFrameWorkListViewCancelFilterAction = class( TPPWFrameWorkListViewAction )
  public
    procedure DoUpdateTarget( Target : TObject ); override;
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Basic DevExpress DB Grid ListView action }
  TPPWFrameWorkdxGridListViewAction = class( TPPWFrameWorkListViewAction )
  private
    function GetFrameWorkDevExpresListViewForm: IPIFDevExpressListView;
  public
    procedure DoUpdateTarget( Target : TObject ); override;
  protected
    property FrameWorkDevExpresListViewForm : IPIFDevExpressListView read GetFrameWorkDevExpresListViewForm;
  end;

  { Print Grid contents ListView Action }
  TPPWFrameWorkListViewGridPrint = class( TPPWFrameWorkdxGridListViewAction )
  public
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Export Grid contents to HTML ListView Action }
  TPPWFrameWorkListViewGridExportToHTML = class( TPPWFrameWorkdxGridListViewAction )
  public
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Export Grid contents to XLS ListView Action }
  TPPWFrameWorkListViewGridExportToXLS = class( TPPWFrameWorkdxGridListViewAction )
  public
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Export Grid contents to XML ListView Action }
  TPPWFrameWorkListViewGridExportToXML = class( TPPWFrameWorkdxGridListViewAction )
  public
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

  { Customise Columns ListView Action }
  TPPWFrameWorkListViewGridCustomiseColumns = class( TPPWFrameWorkdxGridListViewAction )
  public
    procedure DoExecuteTarget( Target : TObject ); override;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects,
  {$ENDIF}
  { Delphi Units }
  SysUtils, Forms,
  { FrameWork Untis }
  Unit_PPWFrameWorkController;

{ TPPWFrameWorkAction }

{*****************************************************************************
  Name           : TPPWFrameWorkAction.DataModuleDataSetHasRecords
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns a boolean indicating if the Currently active
                   FrameWorkDatamodule has an Acitve DataSet which contains
                   at least 1 Record.
  Exceptions     : None
  Description    : This funciton will be used to indicate if the Active
                   DataModule has an Active DataSet with Records.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkAction.DataModuleDataSetHasRecords: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'DataModuleDataSetHasRecords' );
  {$ENDIF}

  Result := DataModuleHasActiveDataSet and
            ( FrameWorkDataModule.DataSet.RecordCount <> 0 );

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'DataModuleDataSetHasRecords' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.DataModuleHasActiveDataSet 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns a boolean indicating if the Currently active
                   FrameWorkDatamodule has an Acitve DataSet.
  Exceptions     : None
  Description    : This funciton will be used to indicate if the Active
                   DataModule has an Active DataSet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkAction.DataModuleHasActiveDataSet: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'DataModuleHasActiveDataSet' );
  {$ENDIF}

  Result := Assigned( FrameWorkDataModule ) and
            Assigned( FrameWorkDataModule.DataSet ) and
            ( FrameWorkDataModule.DataSet.Active );

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'DataModuleHasActiveDataSet' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : This is an empty method on this level, implementation
                   should go into the descendant classes.  Instead of
                   overriding the ExecuteTarget method, descendants should
                   override the DoExecuteTarget Method.
  History        :

  Date         By                   Description
  ----         --                   -----------
  27/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkAction.DoExecuteTarget(Target: TObject);
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'DoExecuteTarget' );
  {$ENDIF}
  //
  {$IFDEF CODESITE}
  csFWActions.ExitMethod( Self, 'DoExecuteTarget' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.DoUpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None
  Exceptions     : None
  Description    : This is an empty method on this level, implementation
                   should go into the descendant classes.  Instead of
                   overriding the UpdateTarget method, descendants should
                   override the DoUpdateTarget Method.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkAction.DoUpdateTarget(Target: TObject);
begin
  //
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.ExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Overridden ExecuteTarget method.  The default funcionality
                   has been changed here.  Before actually executing the
                   target, we will check if we have a BeforeExecute event.
                   Possibly in the BeforeExecute event we will indicate that
                   the action can't be executed.
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkAction.ExecuteTarget(Target: TObject);
var
  aCanExecute : Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ExecuteTarget' );
  {$ENDIF}

  aCanExecute := True;

  inherited ExecuteTarget( Target );

  { First Check if we have an OnGetCanExecute event.  If we do, we should
    only Execute the action if the event returned a true for CanExecute }
  if ( Assigned( FBeforeExecute ) ) then
  begin
    FBeforeExecute( Self, aCanExecute );
  end;

  if ( aCanExecute ) then
  begin
    DoExecuteTarget( Target );
  end;

  {$IFDEF CODESITE}
  csFWActions.ExitMethod( Self, 'ExcuteTarget' );
  {$ENDIF}
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.GetFrameWorkDataModule
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns an interface to the currently active FrameWork
                   DataModule
  Exceptions     : None
  Description    : This function will be used to get the interface to the
                   currently active FrameWork DataModule.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkAction.GetFrameWorkDataModule: IPPWFrameWorkDataModule;
begin
  Result := Nil;

  if ( Assigned( PPWController ) ) then
  begin
    Result := PPWController.ActiveDataModule;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.GetFrameWorkForm
  Author         : Stefaan Lesage
  Return Values  : Returns an interface to the currently active FrameWork
                   From.
  Exceptions     : None
  Description    : This function will be used to get the interface to the
                   currently active FrameWork Form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkAction.GetFrameWorkForm: IPPWFrameWorkForm;
begin
  Result := Nil;

  if ( Assigned( PPWController ) ) then
  begin
    Result := PPWController.ActiveFrameWorkForm;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.GetFrameWorkListViewForm 
  Author         : Stefaan Lesage
  Return Values  : Returns an interface to the currently active FrameWork
                   List View Form.
  Exceptions     : None
  Description    : This function will be used to get the interface to the
                   currently active FrameWork List View Form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkAction.GetFrameWorkListViewForm: IPPWFrameWorkListView;
begin
  Result := Nil;

  if ( Assigned( PPWController ) ) then
  begin
    Result := PPWController.ActiveListViewForm;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.GetFrameWorkRecordViewForm 
  Author         : Stefaan Lesage
  Return Values  : Returns an interface to the currently active FrameWork
                   Record View Form.
  Exceptions     : None
  Description    : This function will be used to get the interface to the
                   currently active FrameWork Record View Form.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkAction.GetFrameWorkRecordViewForm: IPPWFrameWorkRecordView;
begin
  Result := Nil;

  if ( Assigned( PPWController ) ) then
  begin
    Result := PPWController.ActiveRecordViewForm;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkAction.UpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Overridden UpdateTarget method.  The default funcionality
                   has been changed here.  We will check if we have a
                   BeforeUpdate event.  Possibly in the BeforeUpdate event we
                   will indicate that the action should be disabled.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkAction.UpdateTarget(Target: TObject);
var
  aDisableAction : Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'UpdateTarget' );
  {$ENDIF}

  aDisableAction := False;

  inherited UpdateTarget( Target );

  DoUpdateTarget( Target );
  
  if ( Assigned( FBeforeUpdate ) ) then
  begin
    FBeforeUpdate( Self, aDisableAction );

    if ( aDisableAction ) then
    begin
      Enabled := False;
    end;
  end;

  {$IFDEF CODESITE}
  csFWActions.ExitMethod( Self, 'UpdateTarget' );
  {$ENDIF}
end;

{ TPPWFrameWorkShowListViewAction }

procedure TPPWFrameWorkShowListViewAction.DoExecuteTarget(Target: TObject);
var
  aDataModule      : IPPWFrameWorkDataModule;
  aDataModuleFound : Boolean;
  lcv              : Integer;
begin
  If ( Assigned( PPWController ) ) then
  begin
    aDataModuleFound := False;

    for lcv := 0 to Pred( Screen.DataModuleCount ) do
    begin
      { Check if the ListView DataModule already exists }
      if     ( Supports( Screen.DataModules[ lcv ], IPPWFrameWorkDataModule, aDataModule ) ) and
             ( aDataModule.FrameWorkDatamoduleClassName = FDataModuleClass ) and
         not ( Assigned( aDataModule.MasterDataModule ) ) and
         not ( Assigned( aDataModule.ListViewDataModule ) ) then
      begin
        aDataModuleFound := True;
        Break;
      end;
    end;

    if not ( aDataModuleFound ) then
    begin
      aDataModule := PPWController.CreateDataModule( Application, FDataModuleClass );
    end;

    if ( Assigned( aDataModule ) ) then
    begin
      PPWController.ShowListViewForDataModule( aDataModule );
    end;
  end;
end;

function TPPWFrameWorkShowListViewAction.HandlesTarget(Target: TObject): Boolean;
begin
  Result := true;
end;

procedure TPPWFrameWorkShowListViewAction.SetDataModuleClass(
  const Value: TPPWDataModuleClassName);
begin
  if ( Value <> FDataModuleClass ) then
  begin
    FDataModuleClass := Value;
  end;
end;

procedure TPPWFrameWorkShowListViewAction.DoUpdateTarget(Target: TObject);
begin
  inherited DoUpdateTarget( Target );

  Enabled := Enabled and ( FDataModuleClass <> '' );
end;

{ TPPWFrameWorkListViewAction }

function TPPWFrameWorkListViewAction.HandlesTarget(Target: TObject): Boolean;
begin
  Result := Assigned( FrameWorkListViewForm );
//  Result := DataModuleHasActiveDataSet;
end;

function TPPWFrameWorkListViewAction.ListViewAsSelection: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ListViewAsSelection' );
  {$ENDIF}

  Result := False;

  if ( Assigned( FrameWorkDataModule.ListView ) ) then
  begin
    Result := FrameWorkDataModule.ListView.AsSelection;
  end;

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'ListViewAsSelection' );
  {$ENDIF}
end;

function TPPWFrameWorkListViewAction.ListViewMasterRecordViewIsAdding: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ListViewMasterRecordViewIsAdding' );
  {$ENDIF}

  Result := False;

  if ( Assigned( FrameWorkDataModule.ListView.MasterRecordView ) ) then
  begin
    Result := ( FrameWorkDataModule.ListView.MasterRecordView.Mode = rvmAdd );
  end;

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'ListViewMasterRecordViewIsAdding' );
  {$ENDIF}
end;

function TPPWFrameWorkListViewAction.ListViewMasterRecordViewIsDeleting: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ListViewMasterRecordViewIsDeleting' );
  {$ENDIF}

  Result := False;

  if ( Assigned( FrameWorkDataModule.ListView.MasterRecordView ) ) then
  begin
    Result := ( FrameWorkDataModule.ListView.MasterRecordView.Mode = rvmDelete );
  end;

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'ListViewMasterRecordViewIsDeleting' );
  {$ENDIF}
end;

function TPPWFrameWorkListViewAction.ListViewMasterRecordViewIsEditing: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ListViewMasterRecordViewIsEditing' );
  {$ENDIF}

  Result := False;

  if ( Assigned( FrameWorkDataModule.ListView.MasterRecordView ) ) then
  begin
    Result := ( FrameWorkDataModule.ListView.MasterRecordView.Mode = rvmEdit );
  end;

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'ListViewMasterRecordViewIsEditing' );
  {$ENDIF}
end;

function TPPWFrameWorkListViewAction.ListViewMasterRecordViewIsModal: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ListViewMasterRecordViewIsModal' );
  {$ENDIF}

  Result := False;

  if ( Assigned( FrameWorkDataModule.ListView.MasterRecordView ) ) then
  begin
    Result := ( fsModal in FrameWorkDataModule.ListView.MasterRecordView.Form.FormState );
  end;

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'ListViewMasterRecordViewIsModal' );
  {$ENDIF}
end;

function TPPWFrameWorkListViewAction.ListViewMasterRecordViewIsViewing: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'ListViewMasterRecordViewIsViewing' );
  {$ENDIF}

  Result := False;

  if ( Assigned( FrameWorkDataModule.ListView.MasterRecordView ) ) then
  begin
    Result := ( FrameWorkDataModule.ListView.MasterRecordView.Mode = rvmView );
  end;

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'Result', Result );
  csFWActions.ExitMethod( Self, 'ListViewMasterRecordViewIsViewing' );
  {$ENDIF}
end;

procedure TPPWFrameWorkListViewAction.DoUpdateTarget(Target: TObject);
begin
  inherited DoUpdateTarget( Target );

//  Enabled := DataModuleHasActiveDataSet;
end;

{ TPPWFrameWorkListViewAddAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewAddAction.DoExecuteTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the Add method on
                   the DataModule of the Active ListView.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  30/10/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkListViewAddAction.DoExecuteTarget(Target: TObject);
begin
  if ( Assigned( FrameWorkListViewForm ) ) and
     ( Assigned( FrameWorkListViewForm.FrameWorkDataModule ) ) then
  begin
    FrameWorkListViewForm.FrameWorkDataModule.Add;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewAddAction.DoUpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Adding a new record should only be possible if there is an
                   Active DataSet, if the ListView isn't shown for Selection
                   purposes and if the ListView isn't shown as a detail of a
                   record that is in View, Add or Delete Mode
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewAddAction.DoUpdateTarget(Target: TObject);
var DisableAction: Boolean;
begin
  { Adding a new record should only be possible if there is an
    Active DataSet, if the ListView isn't shown for Selection
    purposes and if the ListView isn't shown as a detail of a
    record that is in View, Add or Delete Mode }
  inherited DoUpdateTarget( Target );

  DisableAction := false;

  if assigned (BeforeUpdate) then
  begin
    BeforeUpdate(self, DisableAction);
  end;

  Enabled := (not DisableAction) and ( Assigned( FrameWorkListViewForm ) ) and
             ( DataModuleHasActiveDataSet ) and
             ( not ListViewAsSelection ) and
             ( not ListViewMasterRecordViewIsModal ) and
             ( not ListViewMasterRecordViewIsAdding ) and
             ( not ListViewMasterRecordViewIsViewing ) and
             ( not ListViewMasterRecordViewIsDeleting ) and
             ( FrameWorkDataModule.CanOpenNewRecordView );
end;

{ TPPWFrameWorkListViewConsultAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewConsultAction.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the View method on
                   the DataModule of the Active ListView.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  30/10/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkListViewConsultAction.DoExecuteTarget(
  Target: TObject);
begin
  if ( Assigned( FrameWorkListViewForm ) ) and
     ( Assigned( FrameWorkListViewForm.FrameWorkDataModule ) ) then
  begin
    FrameWorkListViewForm.FrameWorkDataModule.View;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewConsultAction.DoUpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Viewing a record should only be possible if there is an
                   Active DataSet with Records, if the ListView isn't shown for
                   Selection purposes and if the ListView isn't shown as a
                   detail of a record that is in View, Add or Delete Mode.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  30/10/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkListViewConsultAction.DoUpdateTarget(
  Target: TObject);
var DisableAction: Boolean;
begin
  {$IFDEF CODESITE}
  csFWActions.EnterMethod( Self, 'DoUpdateTarget' );
  {$ENDIF}

  { Viewing a record should only be possible if there is an Active
    DataSet with Records, if the ListView isn't shown for Selection
    purposes and if the ListView isn't shown as a detail of a
    record that is in View, Add or Delete Mode }
  inherited DoUpdateTarget( Target );

  DisableAction := false;

  if assigned (BeforeUpdate) then
  begin
    BeforeUpdate(self, DisableAction);
  end;

  Enabled := (not DisableAction) and ( Assigned( FrameWorkListViewForm ) ) and
             ( DataModuleDataSetHasRecords ) and
             ( not ListViewAsSelection ) and
             ( not ListViewMasterRecordViewIsModal ) and
             ( not ListViewMasterRecordViewIsAdding ) and
             ( not ListViewMasterRecordViewIsDeleting ) and
             ( FrameWorkListViewForm.FrameWorkDataModule.CanOpenNewRecordView );

  {$IFDEF CODESITE}
  csFWActions.SendBoolean( 'TPPWFrameWorkListViewConsultAction.Enabled', Enabled );
  csFWActions.ExitMethod( Self, 'DoUpdateTarget' );
  {$ENDIF}
end;

{ TPPWFrameWorkListViewDeleteAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewDeleteAction.DoExecuteTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the Delete method on
                   the DataModule of the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  12/09/2007   ivdbossche           Added Supports check(IIPWFrameWorkFormDeleteMultiSelect)
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewDeleteAction.DoExecuteTarget(Target: TObject);
var
  aDeleteMultiSelect: IIPWFrameWorkFormDeleteMultiSelect;
begin
  if ( Assigned( FrameWorkListViewForm ) ) and
     ( Assigned( FrameWorkListViewForm.FrameWorkDataModule ) ) then
  begin
    if Supports(FrameWorkListViewForm, IIPWFrameWorkFormDeleteMultiSelect, aDeleteMultiSelect) then
    begin
      aDeleteMultiSelect.DeleteSelectedRecords;
    end else begin
      // Common case
      FrameWorkListViewForm.FrameWorkDataModule.Delete;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewDeleteAction.DoUpdateTarget
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Deleting a record should only be possible if there is an
                   Active DataSet with Records, if the ListView isn't shown for
                   Selection purposes and if the ListView isn't shown as a
                   detail of a record that is in View or Add Mode
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  30/10/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkListViewDeleteAction.DoUpdateTarget(Target: TObject);
var DisableAction: Boolean;
begin
  { Deleting a record should only be possible if there is an Active
    DataSet with Records, if the ListView isn't shown for Selection
    purposes and if the ListView isn't shown as a detail of a
    record that is in View or Add Mode }
  inherited DoUpdateTarget( Target );

  DisableAction := false;

  if assigned (BeforeUpdate) then
  begin
    BeforeUpdate(self, DisableAction);
  end;

  Enabled := (not DisableAction) and ( Assigned( FrameWorkListViewForm ) ) and
             ( DataModuleDataSetHasRecords ) and
             ( not ListViewAsSelection ) and
             ( not ListViewMasterRecordViewIsModal ) and
             ( not ListViewMasterRecordViewIsViewing ) and
             ( not ListViewMasterRecordViewIsAdding ) and
             ( FrameWorkDataModule.CanOpenNewRecordView ) ;
end;

{ TPPWFrameWorkListViewEditAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewEditAction.DoExecuteTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the Edit method on
                   the DataModule of the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewEditAction.DoExecuteTarget(Target: TObject);
begin
  if ( Assigned( FrameWorkListViewForm ) ) and
     ( Assigned( FrameWorkListViewForm.FrameWorkDataModule ) ) then
  begin
    FrameWorkListViewForm.FrameWorkDataModule.Edit;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewEditAction.DoUpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Editing a record should only be possible if there is an
                   Active DataSet with Records, if the ListView isn't shown for
                   Selection purposes and if the ListView isn't shown as a
                   detail of a record that is in View, Delete or Add Mode.
  History        :                                                          
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  30/10/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkListViewEditAction.DoUpdateTarget(Target: TObject);
var DisableAction: Boolean;
begin
  { Editing a record should only be possible if there is an Active
    DataSet with Records, if the ListView isn't shown for Selection
    purposes and if the ListView isn't shown as a detail of a
    record that is in View, Delete or Add Mode }
  inherited DoUpdateTarget( Target );

  if assigned (BeforeUpdate) then
  begin
    BeforeUpdate(self, DisableAction);
  end;

  Enabled := (not DisableAction) and ( Assigned( FrameWorkListViewForm ) ) and
             ( DataModuleDataSetHasRecords ) and
             ( not ListViewAsSelection ) and
             ( not ListViewMasterRecordViewIsModal ) and
             ( not ListViewMasterRecordViewIsViewing ) and
             ( not ListViewMasterRecordViewIsAdding ) and
             ( not ListViewMasterRecordViewIsDeleting ) and
             ( FrameWorkDataModule.CanOpenNewRecordView ) ;
end;

{ TPPWFrameWorkListViewFilterAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewFilterAction.DoExecuteTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the
                   ApplyFilter method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewFilterAction.DoExecuteTarget(Target: TObject);
begin
  if ( Assigned( FrameWorkListViewForm ) ) then
  begin
    FrameWorkListViewForm.ApplyFilter;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewFilterAction.DoUpdateTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Filtering should only be possible if there is an Active
                   DataSet, and there are no RecordViews open in Edit mode
                   for the current ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewFilterAction.DoUpdateTarget(Target: TObject);
begin
  inherited DoUpdateTarget( Target );

  Enabled := ( Assigned( FrameWorkListViewForm ) ) and
             DataModuleHasActiveDataSet and
             ( FrameWorkDataModule.ListView.EditingRecordViewCount = 0 );
end;

{ TPPWFrameWorkListViewCancelFilterAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewCancelFilterAction.DoExecuteTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the
                   CancelFilter method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewCancelFilterAction.DoExecuteTarget(
  Target: TObject);
begin
  if ( Assigned( FrameWorkListViewForm ) ) then
  begin
    FrameWorkListViewForm.CancelFilter;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewCancelFilterAction.DoUpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Caneling the Filter should only be possible if there is an
                   Active DataSet, and there are no RecordViews open in Edit
                   mode for the current ListView and the datamodule was
                   Filtered.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewCancelFilterAction.DoUpdateTarget(
  Target: TObject);
begin
  inherited DoUpdateTarget( Target );

  Enabled := ( Assigned( FrameWorkListViewForm ) ) and
             ( DataModuleHasActiveDataSet ) and
             ( FrameWorkDataModule.ListView.EditingRecordViewCount = 0 ) and
             ( FrameWorkDataModule.Filtered );
end;

{ TPPWFrameWorkdxGridListViewAction }

function TPPWFrameWorkdxGridListViewAction.GetFrameWorkDevExpresListViewForm: IPIFDevExpressListView;
var
  aDxGridListView : IPIFDevExpressListView;
begin
  Result := Nil;

  if ( Assigned( PPWController ) ) then
  begin
    if ( Supports( PPWController.ActiveListViewForm, IPIFDevExpressListView, aDxGridListView ) ) then
    begin
      Result := aDxGridListView;
    end;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkdxGridListViewAction.DoUpdateTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : dxGridListView actions should only be enabled if the
                   target is a FrameWorkDxGridListViewForm.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkdxGridListViewAction.DoUpdateTarget(Target: TObject);
begin
  inherited DoUpdateTarget( Target );

  Enabled := Assigned( FrameWorkDevExpresListViewForm );
end;

{ TPPWFrameWorkListViewGridPrint }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewGridPrint.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Executing this action should trigger the
                   PrintGrid method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewGridPrint.DoExecuteTarget(Target: TObject);
begin
  FrameWorkDevExpresListViewForm.PrintGrid( True );
end;

{ TPPWFrameWorkListViewGridExportToHTML }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewGridExportToHTML.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Executing this action should trigger the
                   ExportGridContentsToHTML method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewGridExportToHTML.DoExecuteTarget(
  Target: TObject);
begin
  FrameWorkDevExpresListViewForm.ExportGridContentsToHTML( Nil );
end;

{ TPPWFrameWorkListViewGridExportToXLS }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewGridExportToXLS.DoExecuteTarget 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Executing this action should trigger the
                   ExportGridContentsToXLS method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewGridExportToXLS.DoExecuteTarget(
  Target: TObject);
begin
  FrameWorkDevExpresListViewForm.ExportGridContentsToXLS( Nil );
end;

{ TPPWFrameWorkListViewGridExportToXML }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewGridExportToXML.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Executing this action should trigger the
                   ExportGridContentsToXML method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewGridExportToXML.DoExecuteTarget(
  Target: TObject);
begin
  FrameWorkDevExpresListViewForm.ExportGridContentsToXML( Nil );
end;

{ TPPWFrameWorkListViewGridCustomiseColumns }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewGridCustomiseColumns.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Executing this action should trigger the CustomiseColumns
                   method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------                                    
  30/10/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkListViewGridCustomiseColumns.DoExecuteTarget(
  Target: TObject);
begin
  FrameWorkDevExpresListViewForm.CustomiseColumns;
end;

{ TPPWFrameWorkListViewRefreshAction }

{*****************************************************************************
  Name           : TPPWFrameWorkListViewRefreshAction.DoExecuteTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Executing this action should trigger the RefreshData
                   method on the Active ListView.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewRefreshAction.DoExecuteTarget(
  Target: TObject);
begin
  if ( Assigned( FrameWorkListViewForm ) ) then
  begin
    FrameWorkListViewForm.RefreshData;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkListViewRefreshAction.DoUpdateTarget
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Overridden DoUpdateTarget in which we will disable the
                   Action if there are RecordViews open in Edit Mode or
                   changes havn't been applied yet.
  History        :

  Date         By                   Description
  ----         --                   -----------
  30/10/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkListViewRefreshAction.DoUpdateTarget(Target: TObject);
begin
  inherited DoUpdateTarget( Target );

  Enabled := ( Assigned( FrameWorkListViewForm ) ) and
             ( DataModuleHasActiveDataSet ) and
             ( FrameWorkDataModule.ListView.EditingRecordViewCount = 0 ) and
             ( FrameWorkDataModule.DataSet.ChangeCount = 0 );
end;

end.
