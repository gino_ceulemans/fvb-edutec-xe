{*****************************************************************************
  Name           : unit_PPWFrameWorkADO 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2003 Peopleware                                      
  Description    : This unit will contain the Subclassed ADO components
                   used in the PPWFrameWork sample.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  11/12/2003   sLesage              The method BuildGroupBy has been added to
                                    the IPPWFrameWorkDataSet interface, and
                                    has been implemented on the ADOQuery. 
  09/07/2003   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit unit_PPWFrameWorkADO;

interface

uses
  ADODB, Classes,
  Unit_PPWFrameWorkInterfaces;

Type
  TPPWFrameWorkADOConnection = class( TADOConnection );

  TPPWFrameWorkADOStoredProc = class( TADOStoredProc, IPPWFrameWorkDataSet )
  private
    FAutoOpen : Boolean;
  protected
    function GetAutoOpen: Boolean; virtual;
    procedure SetAutoOpen(const Value: Boolean); virtual;
  public
    constructor Create( AOwner : TComponent ); override;
  published
    property Active             stored  False;
    property AutoOpen : Boolean read    GetAutoOpen write   SetAutoOpen default True;
  end;

  TPPWFrameWorkADOTable = class( TADOTable, IPPWFrameWorkDataSet )
  private
    FAutoOpen : Boolean;
  protected
    function GetAutoOpen: Boolean; virtual;
    procedure SetAutoOpen(const Value: Boolean); virtual;
  public
    constructor Create( AOwner : TComponent ); override;
  published
    property Active             stored  False;
    property AutoOpen : Boolean read    GetAutoOpen write   SetAutoOpen default True;
  end;

  TPPWFrameWorkADOQuery = class( TADOQuery, IPPWFrameWorkQuery )
  private
    FAutoOpen           : Boolean;
    FDetailFilter       : TStrings;
    FGroupBy            : TStrings;
    FOrderBy            : TStrings;
    FPreferencesOrderBy : TStrings;
    FPreferencesFilter  : TStrings;
    FSelect             : TStrings;
    FUserFilter         : TStrings;
    FUserOrderBy        : TStrings;
    FWhere              : TStrings;
  protected
    { Property Getters }
    function GetAutoOpen           : Boolean; virtual;
    function GetDetailFilter       : TStrings; virtual;
    function GetGroupBy            : TStrings; virtual;
    function GetOrderBy            : TStrings; virtual;
    function GetPreferencesFilter  : TStrings; virtual;
    function GetPreferencesOrderBy : TStrings; virtual;
    function GetSelect             : TStrings; virtual;
    function GetUserFilter         : TStrings; virtual;
    function GetUserOrderBy        : TStrings; virtual;
    function GetWhere              : TStrings; virtual;

    { Property Setters }
    procedure SetAutoOpen           ( const Value: Boolean ); virtual;
    procedure SetDetailFilter       ( const Value : TStrings ); virtual;
    procedure SetGroupBy            ( const Value : TStrings ); virtual;
    procedure SetOrderBy            ( const Value : TStrings ); virtual;
    procedure SetPreferencesFilter  ( const Value : TStrings ); virtual;
    procedure SetPreferencesOrderBy ( const Value : TStrings ); virtual;
    procedure SetSelect             ( const Value : TStrings ); virtual;
    procedure SetUserFilter         ( const Value : TStrings ); virtual;
    procedure SetUserOrderBy        ( const Value : TStrings ); virtual;
    procedure SetWhere              ( const Value : TStrings ); virtual;

    { Methods }
    procedure BuildOrderBy;
    procedure BuildGroupBy;
    procedure BuildSQL( Sender : TObject );
    procedure BuildWhere( WhereClause : TStrings; var Add : Boolean ); 
  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;

    property DetailFilter       : TStrings read  GetDetailFilter
                                           write SetDetailFilter;
    property PreferencesFilter  : TStrings read  GetPreferencesFilter
                                           write SetPreferencesFilter;
    property PreferencesOrderBy : TStrings read  GetPreferencesOrderBy
                                           write SetPreferencesOrderBy;
    property UserFilter         : TStrings read  GetUserFilter
                                           write SetUserFilter;
    property UserOrderBy        : TStrings read  GetUserOrderBy
                                           write SetUserOrderBy;
  published
    property Active stored  False;
    property AutoOpen           : Boolean  read  GetAutoOpen
                                           write SetAutoOpen default True;
    property GroupBy            : TStrings read  GetGroupBy
                                           write SetGroupBy;
    property OrderBy            : TStrings read  GetOrderBy
                                           write SetOrderBy;
    property Select             : TStrings read  GetSelect
                                           write SetSelect;
    property Where              : TStrings read  GetWhere
                                           write SetWhere;
  end;

implementation

uses
  { Delphi Units }
  SysUtils;

{ TPPWFrameWorkADOStoredProc }

{*****************************************************************************
  Name           : TPPWFrameWorkADOStoredProc.Create 
  Author         : Stefaan Lesage
  Arguments      : AOwner - The Owner of the Component
  Return Values  : None
  Exceptions     : None
  Description    : Constructor for the component in which we will initialise
                   some properties.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

constructor TPPWFrameWorkADOStoredProc.Create(AOwner: TComponent);
begin
  FAutoOpen := False;
  inherited Create( AOwner );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOStoredProc.GetAutoOpen
  Author         : Stefaan Lesage
  Return Values  : Returns the current value of the AutoOpen property.
  Exceptions     : None
  Description    : Property Getter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOStoredProc.GetAutoOpen: Boolean;
begin
  Result := FAutoOpen;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOStoredProc.SetAutoOpen 
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the AutoOpen property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOStoredProc.SetAutoOpen(const Value: Boolean);
begin
  if ( Value <> FAutoOpen ) then
  begin
    FAutoOpen := Value;
  end;
end;

{ TPPWFrameWorkADOQuery }

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.BuildGroupBy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Build the 'Group By' Clause of the Select Statement.
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/12/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.BuildGroupBy;
begin
  if ( Length( Trim( FGroupBy.Text ) ) > 0 ) then
  begin
    SQL.Add( 'GROUP BY' );
    SQL.AddStrings( FGroupBy );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.BuildOrderBy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : None
  Exceptions     : None
  Description    : Build the 'Order By' Clause of the Select Statement.
                   If the User has provided an 'Order By' clause through
                   the UserOrderBy property, that order will be used instead
                   of the Default OrderBy.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.BuildOrderBy;
begin
  if ( Length ( Trim ( FUserOrderBy.Text ) ) > 0 ) then
  begin
    SQL.Add( 'ORDER BY' );
    SQL.AddStrings( FUserOrderBy );
  end
  else if ( Length( Trim( FPreferencesOrderBy.Text ) ) > 0 ) then
  begin
    SQL.Add( 'ORDER BY' );
    SQL.AddStrings( FPreferencesOrderBy );
  end
  else if ( Length( Trim( FOrderBy.Text ) ) > 0 ) then
  begin
    SQL.Add( 'ORDER BY' );
    SQL.AddStrings( FOrderBy );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.BuildSQL 
  Author         : Stefaan Lesage
  Arguments      : Sender - The Object from which the Method is invoked.                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : This method will be used to asseble the different parts
                   of the SQL Statement and combine them.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  09/07/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.BuildSQL(Sender: TObject);
var
  Add     : Boolean;
begin
  { First add the Select Part of the SQL Statement }
  SQL.Clear;
  SQL.AddStrings( FSelect );
  Add := False;

  { Now Add the Where Clause }
  BuildWhere( FWhere, Add );
  BuildWhere( FUserFilter, Add );
  BuildWhere( FDetailFilter, Add );
  BuildWhere( FPreferencesFilter, Add );

  BuildGroupBy;
  
  { Add the Sort Order }
  BuildOrderBy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.BuildWhere 
  Author         : Stefaan Lesage
  Arguments      : WhereClause - TStrings containing parts of the Where.
                   Add         - Boolean indicating if the current part
                                 of the Where Clause should be added to
                                 the Existing part ( using AND )
  Return Values  : None
  Exceptions     : None
  Description    : This method will be used to Build the Where Clause of
                   the SQL Statement.                                                         
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.BuildWhere(WhereClause: TStrings; var Add: Boolean);
begin
  if ( Length( Trim( WhereClause.Text ) ) > 0 ) then
  begin
    if Add then
    begin
      SQL.Add( 'AND ( ' );
    end
    else
    begin
      SQL.Add( 'WHERE ( ' );
      Add := True;
    end;
    SQL.AddStrings( WhereClause );
    SQL.Add( ' )' );
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.Create
  Author         : Stefaan Lesage
  Arguments      : AOwner - The Owner of the Component
  Return Values  : None
  Exceptions     : None
  Description    : Constructor for the component in which we will initialise
                   some properties.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

constructor TPPWFrameWorkADOQuery.Create(AOwner: TComponent);
begin
  FAutoOpen := False;
  inherited Create( AOwner );

  FDetailFilter := TStringList.Create;
  FSelect := TStringList.Create;
  FOrderBy := TStringList.Create;
  FUserFilter := TStringList.Create;
  FUserOrderBy := TStringList.Create;
  FWhere := TStringList.Create;
  FPreferencesOrderBy := TStringList.Create;
  FPreferencesFilter:= TStringList.Create;
  FGroupBy:= TStringList.Create;


  TStringList( FDetailFilter ).OnChange := BuildSQL;
  TStringList( FSelect ).OnChange := BuildSQL;
  TStringList( FOrderBy ).OnChange := BuildSQL;
  TStringList( FUserFilter ).OnChange := BuildSQL;
  TStringList( FUserOrderBy ).OnChange := BuildSQL;
  TStringList( FPreferencesOrderBy ).OnChange := BuildSQL;
  TStringList( FWhere ).OnChange := BuildSQL;
  TStringList( FPreferencesFilter ).OnChange := BuildSQL;
  TStringList( FGroupBy ).OnChange := BuildSQL;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.Destroy 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : None                                                     
  Exceptions     : None
  Description    : Destructor of the Component in which we will clean up
                   some things.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

destructor TPPWFrameWorkADOQuery.Destroy;
begin
  FreeAndNil( FDetailFilter );
  FreeAndNil( FSelect );
  FreeAndNil( FOrderBy );
  FreeAndNil( FUserFilter );
  FreeAndNil( FUserOrderBy );
  FreeAndNil( FOrderBy );
  FreeAndNil( FWhere );
  FreeAndNil( FPreferencesOrderBy );
  FreeAndNil( FPreferencesFilter );
  FreeAndNil( FGroupBy );

  inherited;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetAutoOpen 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the AutoOpen property.
  Exceptions     : None
  Description    : Property Getter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetAutoOpen: Boolean;
begin
  Result := FAutoOpen;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetDetailFilter 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the DetailFilter property.
  Exceptions     : None
  Description    : Property Getter for the DetailFilter property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetDetailFilter: TStrings;
begin
  Result := FDetailFilter;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetGroupBy 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the GroupBy property.
  Exceptions     : None
  Description    : Property Getter for the GroupBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetGroupBy: TStrings;
begin
  Result := FGroupBy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetOrderBy
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the OrderBy property.
  Exceptions     : None
  Description    : Property Getter for the OrderBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetOrderBy: TStrings;
begin
  Result := FOrderBy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetPreferencesFilter 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the PreferencesFilter
                   property.
  Exceptions     : None
  Description    : Property Getter for the PreferencesFilter property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetPreferencesFilter: TStrings;
begin
  Result := FPreferencesFilter;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetPreferencesOrderBy 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the PreferencesOrderBy
                   property.
  Exceptions     : None
  Description    : Property Getter for the PreferencesOrderBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetPreferencesOrderBy: TStrings;
begin
  Result := FPreferencesOrderBy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetSelect 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the Select property.
  Exceptions     : None
  Description    : Property Getter for the Select property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetSelect: TStrings;
begin
  Result := FSelect;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetUserFilter 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the UserFilter property.
  Exceptions     : None
  Description    : Property Getter for the UserFilter property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetUserFilter: TStrings;
begin
  Result := FUserFilter;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetUserOrderBy 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Returns the current value of the UserOrderBy property.
  Exceptions     : None
  Description    : Property Getter for the UserOrderBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetUserOrderBy: TStrings;
begin
  Result := FUserOrderBy;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.GetWhere 
  Author         : Stefaan Lesage
  Arguments      : None                                                     
  Return Values  : Returns the current value of the Where property.
  Exceptions     : None
  Description    : Property Getter for the Where property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOQuery.GetWhere: TStrings;
begin
  Result := FWhere;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetAutoOpen
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the AutoOpen property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetAutoOpen(const Value: Boolean);
begin
  if ( Value <> FAutoOpen ) then
  begin
    FAutoOpen := Value;
  end;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetDetailFilter
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the DetailFilter property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the DetailFilter property.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  09/07/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetDetailFilter(const Value: TStrings);
begin
  FDetailFilter.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetGroupBy
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the GroupBy property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the GroupBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetGroupBy(const Value: TStrings);
begin
  FGroupBy.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetOrderBy
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the OrderBy property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the OrderBy property.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  09/07/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetOrderBy(const Value: TStrings);
begin
  FOrderBy.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetPreferencesFilter
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the PreferencesFilter property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the PreferencesFilter property.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  09/07/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetPreferencesFilter(const Value: TStrings);
begin
  FPreferencesFilter.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetPreferencesOrderBy
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the PreferencesOrderBy property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the PreferencesOrderBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetPreferencesOrderBy(const Value: TStrings);
begin
  FPreferencesOrderBy.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetSelect
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the Select property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the Select property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetSelect(const Value: TStrings);
begin
  FSelect.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetUserFilter
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the UserFilter property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the UserFilter property.
  History        :
                                                                            
  Date         By                   Description                                    
  ----         --                   -----------                                    
  09/07/2003   slesage              Initial creation of the Method.                  
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetUserFilter(const Value: TStrings);
begin
  FUserFilter.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetUserOrderBy
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the UserOrderBy property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the UserOrderBy property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetUserOrderBy(const Value: TStrings);
begin
  FUserOrderBy.Assign( Value );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOQuery.SetWhere
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the Where property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the Where property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  09/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOQuery.SetWhere(const Value: TStrings);
begin
  FWhere.Assign( Value );
end;

{ TPPWFrameWorkADOTable }

{*****************************************************************************
  Name           : TPPWFrameWorkADOTable.Create 
  Author         : Stefaan Lesage
  Arguments      : AOwner - The Owner of the Component
  Return Values  : None
  Exceptions     : None
  Description    : Constructor for the component in which we will initialise
                   some properties.
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

constructor TPPWFrameWorkADOTable.Create(AOwner: TComponent);
begin
  FAutoOpen := False;
  inherited Create( AOwner );
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOTable.GetAutoOpen 
  Author         : Stefaan Lesage
  Arguments      : None
  Return Values  : Returns the current value of the AutoOpen property.
  Exceptions     : None
  Description    : Property Getter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

function TPPWFrameWorkADOTable.GetAutoOpen: Boolean;
begin
  Result := FAutoOpen;
end;

{*****************************************************************************
  Name           : TPPWFrameWorkADOTable.SetAutoOpen 
  Author         : Stefaan Lesage
  Arguments      : Value - The new value for the AutoOpen property.
  Return Values  : None
  Exceptions     : None
  Description    : Property Setter for the AutoOpen property.
  History        :

  Date         By                   Description
  ----         --                   -----------
  17/07/2003   slesage              Initial creation of the Method.
 *****************************************************************************}

procedure TPPWFrameWorkADOTable.SetAutoOpen(const Value: Boolean);
begin
  if ( Value <> FAutoOpen ) then
  begin
    FAutoOpen := Value;
  end;
end;

end.
