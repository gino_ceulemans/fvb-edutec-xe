{*****************************************************************************
  Name           : Unit_PPWFrameWorkInterfaces
  Author         : Lesage Stefaan
  Copyright      : (c) 2002 Peopleware
  Description    : This unit will contain all the Interfaces that are used
                   in our FrameWork.
  History        :

  Date         By                   Description
  ----         --                   -----------
  12/09/2007   ivdbossche           Added interface IIPWFrameWorkFormDeleteMultiSelect
  07/09/2007   ivdbossche           Added interface IPPWFrameWorkDataModuleDefaultFilter
  27/12/2004   sLesage              Added the CanOpenNewRecordView property
                                    to the IPPWFrameWorkDataModule.
  09/12/2004   sLesage              Added the BringToFront method to the
                                    IPPWFrameWorkForm.
  29/11/2004   sLesage              Added some more TForm properties to the
                                    IPPWFrameWorkForm interface.
  11/12/2003   sLesage              The method BuildGroupBy has been added to
                                    the IPPWFrameWorkDataSet interface.
  28/10/2003   sLesage              Added the EditingRecordViewCount
                                    to the IPPWFrameWorkListViewForm
                                    interface.
  16/09/2003   sLesage              Added a RefreshVisibleDetailListViews
                                    method the the RecordView interface.
  29/08/2003   sLesage              Added the DetailListViews property to the
                                    IPPWFrameWorkRecordView interface, and the
                                    Parent property to the
                                    IPPWFrameWorkRecordView interface.
  25/08/2003   sLesage              The UpdateControls method has been
                                    changed.  It will now also update all
                                    controls that the given Container control
                                    contains.
  10/12/2002   sLesage              Added support for sowing a ListView form
                                    for selection of records.
  6/12/2002    sLesage              Added the RefreshDataMethod to the
                                    IPPWFrameWorkDataModule and the
                                    DoUpdateControls and BeforeUpdateControls
                                    event to the IPPWFrameWorkRecordView.
  29/11/2002   sLesage              Cleaned up unnecessary Code and added
                                    Unit / Procedure Headers.
  21/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkInterfaces;

interface

uses
  { Delphi Units }
  Classes, DB, DBClient, Forms, Controls, ComCtrls, Windows,

  { FrameWork Units }
  Unit_PPWFrameWorkClasses;

type
  IPPWFrameWorkForm = interface;
  IPPWFrameWorkListView = interface;
  IPPWFrameWorkRecordView = interface;

  TPPWFrameWorkCreateFormEvent = procedure ( Sender : TObject; var aListView : IPPWFrameWorkForm ) of Object;
  TPPWFrameWorkLinkListViewEvent = procedure ( Sender : TObject; var aListView : IPPWFrameWorkListView ) of Object;
  TPPWFrameWorkLinkRecordViewEvent = procedure ( Sender : TObject; var aRecordView : IPPWFrameWorkRecordView ) of Object;


  IPPWFrameWorkDataModuleDefaultFilter = interface( IInterface )
  ['{3F37E2B8-758E-42EE-A95F-0FD884A404EF}']
    { Methods }
    procedure InitDefaultFilter(ASearchCriteria: TClientDataSet);
  end;

  IIPWFrameWorkFormDeleteMultiSelect = interface ( IInterface)
  ['{DBA68FAF-FBE6-4B71-A916-0084AE35341F}']
    procedure DeleteSelectedRecords;
  end;

  IPPWFrameWorkDataModule = interface( IInterface )
  ['{43059837-E805-4DB9-94C2-10F57DDC80EC}']
    { Property Getters }
    function GetAsSelection        : Boolean;
    function GetAutoDestroy        : Boolean;
    function GetAutoOpenDataSets   : Boolean;
    function GetCanOpenNewRecordView: Boolean;
    function ClassType             : TClass;
    function GetDataSet            : TClientDataSet;
    function GetRecordDataset      : TClientDataset; //WLUPDATE
    function GetDetailDataModules : TPPWFrameWorkDataModuleDetails;
    function GetFetchOnDemand      : Boolean;
    function GetFilterFirst        : Boolean;
    function GetFiltered           : Boolean;
    function GetKeyFields          : string;
    function GetListViewClass      : TPPWListViewClassName;
    function GetListView           : IPPWFrameWorkListView;
    function GetMasterDataModule   : IPPWFrameWorkDataModule;
    function GetListViewDataModule : IPPWFrameWorkDataModule; //WLUPDATE
    function GetMasterRecordView   : IPPWFrameWorkRecordView; //added WL 17/12
    function GetName               : TComponentName;
    function GetOnBeforeOpenRecordView : TPPWFrameWorkBeforeOpenRecordViewEvent;
    function GetOnCancelFilter     : TNotifyEvent;
    function GetOnBeforeCreateDetailDataModule: TPPWFrameWorkBeforeCreateDetailEvent;
    function GetOnAfterCreateDetailDataModule: TPPWFrameWorkAfterCreateDetailEvent; //added WL 18/12
    function GetOnCreateListView   : TPPWFrameWorkCreateFormEvent;
    function GetOnCreateRecordView : TPPWFrameWorkCreateFormEvent;
    function GetOnFilter           : TPPWFrameWorkOnFilterEvent;
    function GetOnFilterDetail     : TPPWFrameWorkFilterDetailEvent;
    function GetOnFilterRecord     : TPPWFrameWorkFilterDetailEvent;
    function GetOnLinkListView     : TPPWFrameWorkLinkListViewEvent;
    function GetOnLinkRecordView   : TPPWFrameWorkLinkRecordViewEvent;
    function GetOnSort             : TPPWFrameWorkSortEvent;
    function GetRecordViewClass    : TPPWRecordViewClassName;
    function GetRecordViews        : TInterfaceList;
    function GetRecordViewDataModules : TInterfaceList;
    function GetRegistered         : Boolean;
    function GetTableName          : String;
    function GetVisible            : Boolean;
    function GetForeignKeys        : String;

    { Property Setters }
    procedure SetAsSelection( const Value : Boolean );
    procedure SetAutoOpenDataSets( const Value : Boolean );
    procedure SetAutoDestroy( const Value : Boolean );
    procedure SetDataSet( const Value : TClientDataSet );
    procedure SetRecordDataSet( const Value : TClientDataSet );
    procedure SetDetailDataModules( const Value : TPPWFrameWorkDataModuleDetails );
    procedure SetFetchOnDemand( const Value : Boolean );
    procedure SetFilterFirst( const Value : Boolean );
    procedure SetKeyFields( const Value : string );
    procedure SetListView( const Value : IPPWFrameWorkListView );
    procedure SetListViewClass( const Value : TPPWListViewClassName );
    procedure SetName( const Value : TComponentName );
    procedure SetMasterDataModule( const Value : IPPWFrameWorkDataModule );
    procedure SetListViewDataModule (const Value: IPPWFrameWorkDatamodule);
    procedure SetMasterRecordView (const Value: IPPWFrameWorkRecordView);
    procedure SetRecordViewClass( const Value : TPPWRecordViewClassName );
    procedure SetRegistered( const Value : Boolean );
    procedure SetOnBeforeOpenRecordView( const Value : TPPWFrameWorkBeforeOpenRecordViewEvent );
    procedure SetOnCancelFilter( const Value : TNotifyEvent );
    procedure SetOnBeforeCreateDetailDataModule( const Value: TPPWFrameWorkBeforeCreateDetailEvent);
    procedure SetOnAfterCreateDetailDataModule ( const Value: TPPWFrameWorkAfterCreateDetailEvent ); //added WL 18/12/
    procedure SetOnCreateListView( const Value : TPPWFrameWorkCreateFormEvent );
    procedure SetOnCreateRecordView( const Value : TPPWFrameWorkCreateFormEvent );
    procedure SetOnFilter( const Value : TPPWFrameWorkOnFilterEvent );
    procedure SetOnFilterDetail( const Value: TPPWFrameWorkFilterDetailEvent);
    procedure SetOnFilterRecord( const Value: TPPWFrameWorkFilterDetailEvent);
    procedure SetOnLinkListView( const Value : TPPWFrameWorkLinkListViewEvent );
    procedure SetOnLinkRecordView( const Value : TPPWFrameWorkLinkRecordViewEvent );
    procedure SetOnSort( const Value : TPPWFrameWorkSortEvent );
    procedure SetTableName( const Value : String );
    procedure SetVisible( const Value : Boolean );
    procedure SetForeignKeys ( const Value : String);

    { Methods }
    procedure Add;
    Function BuildFilterExpressionForDetail( AMasterDataModule : IPPWFrameWorkDataModule;
                                             ADetailDataModule : IPPWFrameWorkDataModule;
                                             aForeignKeys      : String;
                                             aUseRecordDataSet : Boolean = False ) : String;
    function CreateFieldValuesVarArray( aDataSet : TDataSet; aFields : String ) : Variant;
    procedure Edit;
    procedure Delete;
    procedure DoApplyFilter;
    procedure DoBeforeOpenRecordView( Sender : TObject; RecordViewMode : TPPWFrameWorkRecordViewMode; var AllowRecordView : Boolean );
    procedure DoCancelFilter;
    procedure DoFilterDetail( KeyFields : String; KeyValues : Variant; Expression : String );
    procedure DoFilterRecord( KeyFields : String; KeyValues : Variant; Expression : String );
    procedure DoSort( Fields : String );
    procedure LinkToListView( var aListView : IPPWFrameWorkListView );
    procedure LinkToRecordView( var aRecordView : IPPWFrameWorkRecordView; const aRecordViewMode : TPPWFrameWorkRecordViewMode );
    procedure OpenDataSets;

    procedure RegisterSelf;
    procedure RefreshListDataSet;
    procedure RefreshRecordDataSet;

    procedure DoApplyPendingUpdates;

    procedure Sort( const Fields : String );
    procedure View;
    function ClientDatasetByName (const Name : String): TClientDataset; //WLUPDATE
    function FrameWorkDatamoduleClassName: String; //WLUPDATE

    { Properties }
    property AsSelection      : Boolean                read GetAsSelection
                                                       write SetAsSelection;
    property AutoDestroy      : Boolean                read GetAutoDestroy
                                                       write SetAutoDestroy;
    property AutoOpenDataSets : Boolean                read GetAutoOpenDataSets
                                                       write SetAutoOpenDataSets;
    property CanOpenNewRecordView : Boolean            read GetCanOpenNewRecordView;
    property DataSet          : TClientDataSet         read GetDataSet
                                                       write SetDataSet;
    property RecordDataset    : TClientDataset         read GetRecordDataset
                                                       write SetRecordDataSet;
    property DetailDataModules: TPPWFrameWorkDataModuleDetails
                                                       read GetDetailDataModules
                                                       write SetDetailDataModules;
    property FetchOnDemand    : Boolean                read GetFetchOnDemand
                                                       write SetFetchOnDemand;
    property FilterFirst      : Boolean                read GetFilterFirst
                                                       write SetFilterFirst;
    property Filtered         : Boolean                read GetFiltered;
    property KeyFields        : string                 read GetKeyFields
                                                       write SetKeyFields;
    property ListViewClass    : TPPWListViewClassName  read GetListViewClass
                                                       write SetListViewClass;
    property ListView         : IPPWFrameWorkListView  read GetListView
                                                       write SetListView;
    property MasterDataModule : IPPWFrameWorkDataModule read GetMasterDataModule
                                                        write SetMasterDataModule;
    property ListViewDataModule : IPPWFrameWorkDataModule read GetListViewDataModule
                                                          write SetListViewDataModule; //WLUPDATE
    property Name             : TComponentName          read GetName
                                                        write SetName;
    property MasterRecordView : IPPWFrameWorkRecordView read GetMasterRecordView //added WL 24/12
                                                        write SetMasterRecordView;
    property RecordViewClass  : TPPWRecordViewClassName read GetRecordViewClass
                                                        write SetRecordViewClass;
    property RecordViews      : TInterfaceList          read GetRecordViews;
    property RecordViewDataModules : TInterfaceList     read GetRecordViewDataModules;
    property Registered       : Boolean                 read GetRegistered
                                                        write SetRegistered;
    property TableName        : String                  read GetTableName
                                                        write SetTableName;
    property Visible          : Boolean                 read GetVisible
                                                        write SetVisible;
    //the ;-separated list of foreign keys are stored in the master datamodule
    //but we need this list on this datamodule (which is the detail)
    //when this datamodule is not docked as a detail, this property is empty
    property ForeignKeys      : String                  read GetForeignKeys
                                                        write SetForeignKeys;

    { Events }
    property OnBeforeOpenRecordView : TPPWFrameWorkBeforeOpenRecordViewEvent
                                                       read GetOnBeforeOpenRecordView
                                                       write SetOnBeforeOpenRecordView;
    property OnBeforeCreateDetailDataModule : TPPWFrameWorkBeforeCreateDetailEvent
                                                        read GetOnBeforeCreateDetailDataModule
                                                        write SetOnBeforeCreateDetailDataModule;
    property OnAfterCreateDetailDataModule  : TPPWFrameWorkAfterCreateDetailEvent
                                                        read GetOnAfterCreateDetailDataModule
                                                        write SetOnAfterCreateDetailDataModule;
    property OnCreateListView : TPPWFrameWorkCreateFormEvent
                                                        read GetOnCreateListView
                                                        write SetOnCreateListView;
    property OnCreateRecordView : TPPWFrameWorkCreateFormEvent
                                                        read GetOnCreateRecordView
                                                        write SetOnCreateRecordView;
    property OnLinkListView   : TPPWFrameWorkLinkListViewEvent
                                                        read GetOnLinkListView
                                                        write SetOnLinkListView;
    property OnLinkRecordView : TPPWFrameWorkLinkRecordViewEvent
                                                        read GetOnLinkRecordView
                                                        write SetOnLinkRecordView;
    property OnFilter         : TPPWFrameWorkOnFilterEvent
                                                        read GetOnFilter
                                                        write SetOnFilter;
    property OnFilterDetail   : TPPWFrameWorkFilterDetailEvent
                                                       read GetOnFilterDetail
                                                       write SetOnFilterDetail;
    property OnFilterRecord   : TPPWFrameWorkFilterDetailEvent
                                                       read GetOnFilterRecord
                                                       write SetOnFilterRecord;
    property OnCancelFilter   : TNotifyEvent            read GetOnCancelFilter
                                                        write SetOnCancelFilter;
    property OnSort           : TPPWFrameWorkSortEvent  read GetOnSort
                                                        write SetOnSort;
  end;

  IPPWFrameWorkForm = interface( IInterface )
  ['{F2A2D2A2-41A6-4732-96A9-A400F01E053F}']
    { Property Getters }
    function GetAlign : TAlign;
    function GetBorderStyle : TFormBorderStyle;
    function GetCaption : TCaption;
    function GetDataSource : TDataSource;
    function GetFormStyle : TFormStyle;
    function GetFrameWorkClassName : String;
    function GetFrameWorkDataModule : IPPWFrameWorkDataModule;
    function GetHandle : HWnd;
    function GetKeyFields : string;
    function GetHeight : Integer;
    function GetMinHeight : Integer;
    function GetMinWidth : Integer;
    function GetOriginalCaption : String;
    function GetPosition : TPosition;
    function GetRegistered : Boolean;
    function GetRegistryKey : String;
    function GetWindowState : TWindowState;
    function GetSettingsSaved: Boolean; //added WL 06/01
    function GetVisible : Boolean;
    function GetTop : Integer;
    function GetLeft : Integer;
    function GetWidth : Integer;

    { Property Setters }
    procedure SetAlign( const Value : TAlign );
    procedure SetBorderStyle( const Value: TFormBorderStyle);
    procedure SetCaption( const Value : TCaption );
    procedure SetDataSource( const Value : TDataSource );
    procedure SetFormStyle( const Value : TFormStyle );
    procedure SetHeight( const Value : Integer );
    procedure SetRegistered( const Value : Boolean );
    procedure SetOriginalCaption( const Value : String );
    procedure SetUseDockManager( const Value : Boolean );
    procedure SetWindowState( Const Value: TWindowState);
    procedure SetVisible( const Value : Boolean );
    procedure SetLeft( const Value : Integer );
    procedure SetWidth( const Value : Integer );
    procedure SetTop( const Value : Integer );
    procedure SetPosition ( const Value : TPosition );

    { Methods }
    procedure BringToFront;
    procedure Resize;
    procedure SetFocus;
    procedure Show;
    function ShowModal: Integer;
    procedure ModuleNotified (Sender: TComponent; const ModuleName: string);
    procedure UpdateFormCaption;


    function ManualDock(NewDockSite: TWinControl; DropControl: TControl = nil;
      ControlSide: TAlign = alNone): Boolean;
    procedure RegisterSelf;
    procedure UnRegisterSelf;

    { Properties }
    property Align: TAlign                        read GetAlign
                                                  write SetAlign;
    property BorderStyle : TFormBorderStyle       read GetBorderStyle
                                                  write SetBorderStyle;
    property Caption: TCaption                    read GetCaption
                                                  write SetCaption;
    property DataSource : TDataSource             read GetDataSource
                                                  write SetDataSource;
    property FormStyle : TFormStyle               read GetFormStyle
                                                  write SetFormStyle;
    property FrameWorkClassName : string          read GetFrameWorkClassName;
    property FrameWorkDataModule : IPPWFrameWorkDataModule
                                                  read GetFrameWorkDataModule;
    property Handle : HWND                        read GetHandle;
    property KeyFields : string                   read GetKeyFields;
    property Height : Integer                     read GetHeight
                                                  write SetHeight;
    property OriginalCaption : string             read  GetOriginalCaption
                                                  write SetOriginalCaption;
    property Registered : Boolean                 read GetRegistered
                                                  write SetRegistered;
    property RegistryKey : string                 read GetRegistryKey;
    property UseDockManager: Boolean              write SetUseDockManager;
    property WindowState: TWindowState            read GetWindowState
                                                  write SetWindowState;
    property Top          : Integer               read  GetTop
                                                  write SetTop;
    property Left         : Integer               read  GetLeft
                                                  write SetLeft;
    property Width        : Integer               read  GetWidth
                                                  write SetWidth;
    property SettingsSaved: Boolean               read GetSettingsSaved;
    property MinHeight    : Integer               read GetMinHeight;
    property MinWidth     : Integer               read GetMinWidth;
    property Visible      : Boolean               read GetVisible write SetVisible;
    property Position     : TPosition read GetPosition write SetPosition;
  end;

  IPPWFrameWorkListView = interface( IPPWFrameWorkForm )
  ['{F53C042F-35F1-4998-84F2-4555054EF371}']
    { Property Getters }
    function GetAsSelection      : Boolean;
    function GetMasterRecordView : IPPWFrameWorkRecordView;
    function GetParent           : TWinControl;
    function EditingRecordViewCount : Integer;

    { Property Setters }
    procedure SetAsSelection( const Value : Boolean );
    procedure SetMasterRecordView(const Value: IPPWFrameWorkRecordView);
    procedure SetParent(AParent: TWinControl); 

    procedure MasterRecordViewModeChanged;
    procedure ApplyFilter;
    procedure CancelFilter;
    procedure RefreshData;

    property AsSelection      : Boolean           read GetAsSelection
                                                  write SetAsSelection;
    property MasterRecordView : IPPWFrameWorkRecordView
                                                  read GetMasterRecordView
                                                  write SetMasterRecordView;
    property Parent           : TWinControl       read  GetParent
                                                  write SetParent;                    
  end;

  IPIFDevExpressListView = interface( IPPWFrameWorkForm )
  ['{F10FA000-DF71-43A1-9A79-CC57195B6376}']
    { Property Getters }
    function  GetAllowMultiSelect : Boolean;

    { Property Setters }
    procedure SetAllowMultiSelect ( const Value : Boolean );

    procedure CloneSelectedRecords( aSource, aDestination : TClientDataSet );
    procedure CustomiseColumns;
    procedure ExportGridContentsToHTML( Sender : TObject );
    procedure ExportGridContentsToXML( Sender : TObject );
    procedure ExportGridContentsToXLS( Sender : TObject );
    procedure PrintGrid( aPrintPreview : Boolean = True );
    procedure SaveGridContents( aFileExt, aFilter, aFileName : String; aMethod : TPPWFrameWorkSaveGridMethod );

    property AllowMultiSelect : Boolean   read  GetAllowMultiSelect
                                          write SetAllowMultiSelect;
  end;

//  IPPWFrameWorkdxDBGridListView = interface( IPPWFrameWorkForm )
//  ['{1AD3AF1F-A5D4-413A-B3B2-ED331383B1AA}']
//    { Property Getters }
//    function  GetAllowMultiSelect : Boolean;
//    function  GetDxDBGrid         : TDxDBGrid;
//
//    { Property Setters }
//    procedure SetAllowMultiSelect ( const Value : Boolean );
//
//    procedure CustomiseColumns;
//    procedure ExportGridContentsToHTML( Sender : TObject );
//    procedure ExportGridContentsToXML( Sender : TObject );
//    procedure ExportGridContentsToXLS( Sender : TObject );
//    procedure PrintGrid( aPrintPreview : Boolean = True );
//    procedure SaveGridContents( aFileExt, aFilter, aFileName : String; aMethod : TPPWFrameWorkSaveGridMethod );
//
//    property AllowMultiSelect : Boolean   read  GetAllowMultiSelect
//                                          write SetAllowMultiSelect;
//    property DxDBGrid         : TDxDBGrid read  GetDxDBGrid;
//  end;

  IPPWFrameWorkRecordView = interface( IPPWFrameWorkForm )
  ['{305311BF-D4C1-4F01-9CAE-3441343CA9CF}']
    { Property Getters }
    function GetOnAllDetailsCreated : TNotifyEvent; //added WL 21/12
    function GetOnDetailListViewCreated: TPPWFrameWorkOnDetailListViewCreatedEvent; //added WL 28/01
    function GetDetailListViews     : IInterfaceList;

//    function GetClonedDataset: TPPWFrameWorkClientDataSet; //added WL 16/12
    function GetMode : TPPWFrameWorkRecordViewMode;
    function GetForm : TCustomForm;
    function GetDetailName : string;

    function GetRecordViewDatamodule: IPPWFrameWorkDatamodule; //WLUPDATE

    { Property Setters }
    procedure SetOnAllDetailsCreated (const Value: TNotifyEvent); //added WL 21/12
    procedure SetOnDetailListViewCreated (const Value: TPPWFrameWorkOnDetailListViewCreatedEvent); //added WL 28/01
    procedure SetMode( const Value : TPPWFrameWorkRecordViewMode );

    { Methods }
    procedure DoModeChanged;

    function FindComponent(const AName: string): TComponent;
    procedure LinkDataSourcesToDataSets;

    procedure CloneDataSetFromDataModule;

    procedure RefreshRecordView (RecordViewMode: TPPWFrameWorkRecordViewMode);
    procedure RefreshVisibleDetailListViews;

    property DetailName : String read GetDetailName;
    property Form       : TCustomForm read GetForm;
    property Mode : TPPWFrameWorkRecordViewMode   read GetMode;
    property RecordViewDatamodule: IPPWFrameWorkDatamodule read GetRecordViewDatamodule;

    property OnAllDetailsCreated: TNotifyEvent    read GetOnAllDetailsCreated //added WL 21/12
                                                  write SetOnAllDetailsCreated;
//    property ClonedDataset: TPPWFrameWorkClientDataSet read GetClonedDataset; //Added WL 16/12
    //needed so that we can access the added master record in its details
    property OnDetailListViewCreated: TPPWFrameWorkOnDetailListViewCreatedEvent
      read GetOnDetailListViewCreated write SetOnDetailListViewCreated; //added WL 28/01
    property DetailListViews : IInterfaceList read GetDetailListViews;

  end;

  //added WL 17/12: when a listview implements this interface, the PPWFramework
  //actions will trigger the events to see whether the action is to be enabled
  //or not
  IPPWFrameworkAllowAction = interface
  ['{10BF8CCB-3074-4C54-8715-C01312E8109E}']
    procedure ShouldAllowAction(Sender : TObject; var AllowAction: Boolean);
  end;

  IPPWFrameWorkDataSet = interface
  ['{74E7FCED-C480-4DFF-8173-6530ED641017}']
    { Property Getters }
    function GetAutoOpen : Boolean;

    { Property Setters }
    procedure SetAutoOpen( const Value : Boolean );

    { Methods }

    { Properties }
    property AutoOpen : Boolean read  GetAutoOpen write SetAutoOpen;
  end;

  IPPWFrameWorkQuery = interface( IPPWFrameWorkDataSet )
  ['{8BE8AF63-25CB-453E-84B6-4F217F2E5F56}']
    { Property Getters }
    function GetDetailFilter       : TStrings;
    function GetGroupBy            : TStrings;
    function GetOrderBy            : TStrings;
    function GetPreferencesFilter  : TStrings;
    function GetPreferencesOrderBy : TStrings;
    function GetSelect             : TStrings;
    function GetUserFilter         : TStrings;
    function GetUserOrderBy        : TStrings;
    function GetWhere              : TStrings;

    { Property Setters }
    procedure SetDetailFilter       ( const Value : TStrings );
    procedure SetGroupBy            ( const Value : TStrings );
    procedure SetOrderBy            ( const Value : TStrings );
    procedure SetPreferencesFilter  ( const Value : TStrings );
    procedure SetPreferencesOrderBy ( const Value : TStrings );
    procedure SetSelect             ( const Value : TStrings );
    procedure SetUserFilter         ( const Value : TStrings );
    procedure SetUserOrderBy        ( const Value : TStrings );
    procedure SetWhere              ( const Value : TStrings );

    { Methods }
    procedure BuildOrderBy;
    procedure BuildSQL( Sender : TObject );
    procedure BuildWhere( WhereClause : TStrings; var Add : Boolean );
    procedure BuildGroupBy;
    procedure Open; 

    { Properties }
    property DetailFilter       : TStrings read  GetDetailFilter
                                           write SetDetailFilter;
    property GroupBy            : TStrings read  GetGroupBy
                                           write SetGroupBy;
    property OrderBy            : TStrings read  GetOrderBy
                                           write SetOrderBy;
    property PreferencesFilter  : TStrings read  GetPreferencesFilter
                                           write SetPreferencesFilter;
    property PreferencesOrderBy : TStrings read  GetPreferencesOrderBy
                                           write SetPreferencesOrderBy;
    property Select             : TStrings read  GetSelect
                                           write SetSelect;
    property UserFilter         : TStrings read  GetUserFilter
                                           write SetUserFilter;
    property UserOrderBy        : TStrings read  GetUserOrderBy
                                           write SetUserOrderBy;
    property Where              : TStrings read  GetWhere
                                           write SetWhere;

  end;

  IPPWFrameWorkControlledLayout = Interface( IInterface )
  ['{62DC842D-F4F5-45CF-9ED6-96715C5B5F41}']
    { Property Getters }
    function GetAfterUpdateControls  : TNotifyEvent;
    function GetBeforeUpdateControls : TNotifyEvent;

    { Property Setters }
    procedure SetAfterUpdateControls( const Value : TNotifyevent );
    procedure SetBeforeUpdateControls( const Value : TNotifyEvent );

    { Methods }
    procedure DoUpdateControls;

    procedure UpdateControls( Container : TWinControl );
    procedure UpdateControlColor( Control : TControl );
    procedure UpdateControlLayout( Control : TControl; ControlOptions : TPPWFrameWorkControlOptions );

    { Properties }
    property BeforeUpdateControls : TNotifyEvent  read GetBeforeUpdateControls
                                                  write SetBeforeUpdateControls;
    property AfterUpdateControls  : TNotifyEvent  read GetAfterUpdateControls
                                                  write SetAfterUpdateControls; //added WL 27/12
  end;

implementation

end.
