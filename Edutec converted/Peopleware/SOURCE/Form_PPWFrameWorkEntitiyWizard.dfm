object frmPPWFrameWorkEntityWizard: TfrmPPWFrameWorkEntityWizard
  Left = 192
  Top = 162
  Width = 553
  Height = 465
  Caption = 'PIF Entity Wizard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxpgcOptions: TcxPageControl
    Left = 0
    Top = 0
    Width = 545
    Height = 390
    ActivePage = cxtbsGeneral
    Align = alClient
    TabOrder = 0
    OnPageChanging = cxpgcOptionsPageChanging
    ClientRectBottom = 390
    ClientRectRight = 545
    ClientRectTop = 24
    object cxtbsGeneral: TcxTabSheet
      Caption = 'General Options'
      ImageIndex = 0
      object cxgbProjectOptions: TcxGroupBox
        Left = 8
        Top = 8
        Width = 528
        Height = 136
        Caption = 'Project Options'
        TabOrder = 0
        object cxscbProjectFolder: TcxShellComboBox
          Left = 120
          Top = 48
          Width = 392
          Height = 21
          Properties.Root.BrowseFolder = bfCustomPath
          Style.StyleController = cxescMain
          TabOrder = 0
        end
        object cxscbDataModuleFolder: TcxShellComboBox
          Left = 120
          Top = 80
          Width = 392
          Height = 21
          Properties.Root.BrowseFolder = bfCustomPath
          Style.StyleController = cxescMain
          TabOrder = 1
        end
        object cxscbFormFolder: TcxShellComboBox
          Left = 120
          Top = 104
          Width = 392
          Height = 21
          Properties.Root.BrowseFolder = bfCustomPath
          Style.StyleController = cxescMain
          TabOrder = 2
        end
        object cxlblProjectFolder: TcxLabel
          Left = 8
          Top = 50
          Width = 85
          Height = 17
          Caption = 'Project Directory:'
          FocusControl = cxscbProjectFolder
        end
        object cxlblDataModuleFolder: TcxLabel
          Left = 8
          Top = 82
          Width = 96
          Height = 17
          Caption = 'Datamodule Folder:'
          FocusControl = cxscbDataModuleFolder
        end
        object cxlblFormFolder: TcxLabel
          Left = 8
          Top = 106
          Width = 62
          Height = 17
          Caption = 'Form Folder:'
          FocusControl = cxscbFormFolder
        end
        object cxlblProject: TcxLabel
          Left = 120
          Top = 24
          Width = 58
          Height = 17
          Caption = 'cxlblProject'
        end
        object cxlblProjectName: TcxLabel
          Left = 8
          Top = 24
          Width = 71
          Height = 17
          Caption = 'Project Name:'
        end
      end
      object cxgbBaseClasses: TcxGroupBox
        Left = 8
        Top = 152
        Width = 528
        Height = 104
        Caption = 'PIF Base Class Options'
        TabOrder = 1
        object cxtedtBaseDataModuleClass: TcxTextEdit
          Left = 120
          Top = 24
          Width = 392
          Height = 21
          Style.StyleController = cxescMain
          TabOrder = 0
        end
        object cxtedtBaseListViewClass: TcxTextEdit
          Left = 120
          Top = 48
          Width = 392
          Height = 21
          Style.StyleController = cxescMain
          TabOrder = 1
        end
        object cxtedtBaseRecordViewClass: TcxTextEdit
          Left = 120
          Top = 72
          Width = 392
          Height = 21
          Style.StyleController = cxescMain
          TabOrder = 2
        end
        object cxlblBaseDataModule: TcxLabel
          Left = 8
          Top = 26
          Width = 91
          Height = 17
          Caption = 'Base Datamodule:'
          FocusControl = cxtedtBaseDataModuleClass
        end
        object cxlblBaseListView: TcxLabel
          Left = 8
          Top = 50
          Width = 73
          Height = 17
          Caption = 'Base ListView:'
          FocusControl = cxtedtBaseListViewClass
        end
        object cxlblBaseRecordView: TcxLabel
          Left = 8
          Top = 74
          Width = 92
          Height = 17
          Caption = 'Base RecordView:'
          FocusControl = cxtedtBaseRecordViewClass
        end
      end
      object cxgbCxGroupBox1: TcxGroupBox
        Left = 8
        Top = 264
        Width = 528
        Height = 104
        Caption = 'Entity Options'
        TabOrder = 2
        object cxtedtEntityName: TcxTextEdit
          Left = 120
          Top = 20
          Width = 392
          Height = 21
          Style.StyleController = cxescMain
          TabOrder = 0
        end
        object cxlblEntityName: TcxLabel
          Left = 8
          Top = 22
          Width = 64
          Height = 17
          Caption = 'Entity Name:'
          FocusControl = cxtedtEntityName
        end
        object cxlblListViewCaption: TcxLabel
          Left = 8
          Top = 46
          Width = 85
          Height = 17
          Caption = 'ListView Caption:'
          FocusControl = cxtedtListViewCaption
        end
        object cxtedtListViewCaption: TcxTextEdit
          Left = 120
          Top = 44
          Width = 392
          Height = 21
          Style.StyleController = cxescMain
          TabOrder = 3
        end
        object cxlblRecordViewCaption: TcxLabel
          Left = 8
          Top = 70
          Width = 104
          Height = 17
          Caption = 'RecordView Caption:'
          FocusControl = cxtedtRecordViewCaption
        end
        object cxtedtRecordViewCaption: TcxTextEdit
          Left = 120
          Top = 68
          Width = 392
          Height = 21
          Style.StyleController = cxescMain
          TabOrder = 5
        end
      end
    end
    object cxtbsListViewOptions: TcxTabSheet
      Caption = 'List View SQL'
      ImageIndex = 1
      object bvlBevel1: TBevel
        Left = 0
        Top = 344
        Width = 545
        Height = 22
        Align = alBottom
        Shape = bsSpacer
      end
      object cxmmoListSQL: TcxMemo
        Left = 0
        Top = 0
        Width = 545
        Height = 344
        Align = alClient
        TabOrder = 0
      end
      object cxcbInitialiseListViewSQL: TcxCheckBox
        Left = 0
        Top = 345
        Width = 241
        Height = 21
        Caption = 'Initialise ListView SQL'
        Properties.NullStyle = nssUnchecked
        State = cbsChecked
        Style.StyleController = cxescMain
        TabOrder = 1
      end
    end
    object cxtbsRecordViewOptions: TcxTabSheet
      Caption = 'Record View SQL'
      ImageIndex = 3
      object bvlBevel2: TBevel
        Left = 0
        Top = 344
        Width = 545
        Height = 22
        Align = alBottom
        Shape = bsSpacer
      end
      object cxmmoRecordSQL: TcxMemo
        Left = 0
        Top = 0
        Width = 545
        Height = 344
        Align = alClient
        Style.StyleController = cxescMain
        TabOrder = 0
      end
      object cxcbInitialiseRecordViewSQL: TcxCheckBox
        Left = 8
        Top = 345
        Width = 241
        Height = 21
        Caption = 'Initialise RecordView SQL'
        Properties.NullStyle = nssUnchecked
        State = cbsChecked
        Style.StyleController = cxescMain
        TabOrder = 1
      end
    end
  end
  object pnlBottomButtons: TPanel
    Left = 0
    Top = 390
    Width = 545
    Height = 41
    Align = alBottom
    TabOrder = 1
    object cxbtnCancel: TcxButton
      Left = 376
      Top = 8
      Width = 75
      Height = 25
      Action = acCancelWizard
      ModalResult = 2
      TabOrder = 0
    end
    object cxbtnOK: TcxButton
      Left = 464
      Top = 8
      Width = 75
      Height = 25
      Action = acFinishWizard
      TabOrder = 1
    end
    object cxbtnNext: TcxButton
      Left = 288
      Top = 8
      Width = 75
      Height = 25
      Action = acNextPage
      TabOrder = 3
    end
    object btnCxButton3: TcxButton
      Left = 200
      Top = 8
      Width = 75
      Height = 25
      Action = acPreviousPage
      TabOrder = 2
    end
  end
  object cxlfCxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfOffice11
    Left = 8
    Top = 336
  end
  object alActionList1: TActionList
    Left = 72
    Top = 336
    object acNextPage: TAction
      Category = 'Page'
      Caption = '&Next'
      OnExecute = acNextPageExecute
      OnUpdate = acNextPageUpdate
    end
    object acPreviousPage: TAction
      Category = 'Page'
      Caption = '&Previous'
      OnExecute = acPreviousPageExecute
      OnUpdate = acPreviousPageUpdate
    end
    object acCancelWizard: TAction
      Category = 'Wizard'
      Caption = '&Cancel'
      OnExecute = acCancelWizardExecute
    end
    object acFinishWizard: TAction
      Category = 'Wizard'
      Caption = '&Finish'
      OnExecute = acFinishWizardExecute
      OnUpdate = acFinishWizardUpdate
    end
  end
  object srcList: TDataSource
    Left = 104
    Top = 335
  end
  object srcRecord: TDataSource
    Left = 136
    Top = 335
  end
  object cxescMain: TcxEditStyleController
    Left = 40
    Top = 335
  end
end
