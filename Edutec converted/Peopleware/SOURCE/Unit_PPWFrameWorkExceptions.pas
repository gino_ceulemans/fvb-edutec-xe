{*****************************************************************************
  Name           : Unit_PPWFrameWordExceptions 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2002 Peopleware                                      
  Description    : This unit will contain all Exceptions that can be
                   Raised by the FrameWork.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  11/04/2003   WL                   Added EPPWRecordAlreadyDeleted and resourcestring
  20/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkExceptions;

interface

uses
  SysUtils;

type
  EPPWFrameWorkError = class( Exception )
  end;

  EPPWUnregisteredDataModuleError = class ( EPPWFrameWorkError )
  end;

  EPPWUnregisteredListViewError = class ( EPPWFrameWorkError )
  end;

  EPPWUnregisteredRecordViewError = class ( EPPWFrameWorkError )
  end;

  EPPWListViewClassNotSupplied = Class( EPPWFrameWorkError )
  end;

  EPPWRecordViewClassNotSupplied = Class( EPPWFrameWorkError )
  end;

  EPPWRecordViewAlreadyOpen = class( EPPWFrameWorkError )
  public
    constructor Create;
  end;

  EPPWRecordAlreadyDeleted = class (EPPWFrameWorkError);

resourcestring
  rsPPWFrameWorkOpenDataSetFailed         = 'An error occured while opening dataset %s' + #13#10 +
                                            'Error Message : %s';
  rsPPWFrameWorkUnRegisteredDataModule    = '%s is not a Registered PPWFrameWorkDataModuleClass';
  rsPPWFrameWorkUnRegisteredListView      = '%s is not a Registered PPWFrameWorkListViewClass';
  rsPPWFrameWorkUnRegisteredRecordView    = '%s is not a Registered PPWFrameWorkRecordViewClass';
  rsPPWFrameWorkRecordViewAlreadyExists   = 'There is already a screen open for the selected Record.';
  rsPPWFrameWorkNoRecordViewClassSupplied = 'There is no RecordView specified on the %s FrameWork DataModule';
  rsPPWFrameWorkNoListViewClassSupplied   = 'There is no ListView specified on the %s FrameWork DataModule';
  rsPPWFrameWorkRecordAlreadyDeleted      = 'The record could not be found in the database, it might have been deleted by another user. Please refresh.';
  rsPPWFrameWorkUnRegisteredDetailDataModule    = '%s is not a set up as a Detail of a %s';

implementation

{ EPPWRecordViewAlreadyOpen }

constructor EPPWRecordViewAlreadyOpen.Create;
begin
  inherited Create( rsPPWFrameWorkRecordViewAlreadyExists );
end;

end.
