object frmPPWFrameWorkAddDataAwareControlsWizard: TfrmPPWFrameWorkAddDataAwareControlsWizard
  Left = 192
  Top = 162
  BorderStyle = bsDialog
  Caption = 'Wizard - Add Data Aware Controls'
  ClientHeight = 453
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 412
    Width = 688
    Height = 41
    Align = alBottom
    TabOrder = 0
    object cxbtnCxButton1: TcxButton
      Left = 512
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object cxbtnCxButton2: TcxButton
      Left = 600
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Finish'
      ModalResult = 1
      TabOrder = 1
    end
    object cxbtnSelectAll: TcxButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Select All'
      TabOrder = 2
      OnClick = cxbtnSelectAllClick
    end
    object cxbtnDeselectAll: TcxButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Deselect All'
      TabOrder = 3
      OnClick = cxbtnDeselectAllClick
    end
    object cxbtnToggleSelection: TcxButton
      Left = 280
      Top = 8
      Width = 75
      Height = 25
      Caption = '&Toggle Sel.'
      TabOrder = 4
      OnClick = cxbtnToggleSelectionClick
    end
  end
  object cxgrdRecordFields: TcxGrid
    Left = 0
    Top = 0
    Width = 688
    Height = 412
    Align = alClient
    TabOrder = 1
    object cxgrdtblvRecordFields: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      Navigator.Buttons.Filter.Visible = False
      Navigator.Visible = True
      FilterBox.CustomizeDialog = False
      FilterBox.Visible = fvNever
      DataController.DataSource = srcRecordFields
      DataController.KeyFieldNames = 'F_FIELD_ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsView.ShowEditButtons = gsebAlways
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      object cxgrdtblvRecordFieldsF_FIELD_ID: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_ID'
        Options.Editing = False
      end
      object cxgrdtblvRecordFieldsF_FIELD_NAME: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_NAME'
        Options.Editing = False
        Width = 202
      end
      object cxgrdtblvRecordFieldsF_FIELD_COMPONENT: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_COMPONENT'
        RepositoryItem = cxeriDBAwareComponentClassNames
        Width = 338
      end
      object cxgrdtblvRecordFieldsF_FIELD_VISIBLE: TcxGridDBColumn
        DataBinding.FieldName = 'F_FIELD_VISIBLE'
        Width = 73
      end
    end
    object cxgrdlvlRecordFields: TcxGridLevel
      Caption = 'Data Aware Control Settings'
      GridView = cxgrdtblvRecordFields
      Options.DetailTabsPosition = dtpTop
    end
  end
  object cdsRecordFields: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 64
    Top = 352
    object cdsRecordFieldsF_FIELD_ID: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'F_FIELD_ID'
    end
    object cdsRecordFieldsF_FIELD_NAME: TStringField
      DisplayLabel = 'FieldName'
      FieldName = 'F_FIELD_NAME'
      Size = 200
    end
    object cdsRecordFieldsF_FIELD_VISIBLE: TBooleanField
      DisplayLabel = 'Visible'
      FieldName = 'F_FIELD_VISIBLE'
    end
    object cdsRecordFieldsF_FIELD_COMPONENT: TStringField
      DisplayLabel = 'Component'
      FieldName = 'F_FIELD_COMPONENT'
      Size = 200
    end
  end
  object srcRecordFields: TDataSource
    DataSet = cdsRecordFields
    Left = 96
    Top = 352
  end
  object cxlfCxLookAndFeelController1: TcxLookAndFeelController
    Kind = lfOffice11
    Left = 32
    Top = 352
  end
  object cxEditRepository1: TcxEditRepository
    Left = 128
    Top = 352
    object cxeriDBAwareComponentClassNames: TcxEditRepositoryComboBoxItem
      Properties.Sorted = True
    end
  end
end
