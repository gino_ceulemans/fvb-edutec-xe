{*****************************************************************************
  Name           : Unit_PPWFrameWorkActionResources 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2002 Peopleware                                      
  Description    : This unit contains the Basic Action Resources like their
                   captions, hints, images ...                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/12/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkActionResources;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ImgList, Unit_PPWFrameWorkActions, StdActns;

type
  TfrmPPWFrameWorkActionResources = class(TDataModule)
    ilImageList1: TImageList;
    alActionList1: TActionList;
    acPPWFWShowListView: TPPWFrameWorkShowListViewAction;
    acPPWFWListViewAdd: TPPWFrameWorkListViewAddAction;
    acPPWFWListViewDelete: TPPWFrameWorkListViewDeleteAction;
    acPPWFWListViewEdit: TPPWFrameWorkListViewEditAction;
    acPPWFWListViewConsult: TPPWFrameWorkListViewConsultAction;
    acPPWFWListViewCancelFilter: TPPWFrameWorkListViewCancelFilterAction;
    acPPWFWListViewFilter: TPPWFrameWorkListViewFilterAction;
    acPPWFWdxGridListViewPrint: TPPWFrameWorkListViewGridPrint;
    acPPWFWdxGridListViewExportToHTML: TPPWFrameWorkListViewGridExportToHTML;
    acPPWFWdxGridListViewExportToXLS: TPPWFrameWorkListViewGridExportToXLS;
    acPPWFWdxGridListViewExportToXML: TPPWFrameWorkListViewGridExportToXML;
    acPPWFWdxGridCustomiseColumns: TPPWFrameWorkListViewGridCustomiseColumns;
    acPPWFWListViewRefresh: TPPWFrameWorkListViewRefreshAction;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPPWFrameWorkActionResources: TfrmPPWFrameWorkActionResources;

implementation

{$R *.dfm}

end.
