{*****************************************************************************
  Name           : Unit_PPWFrameWorkCodeSiteObjects 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2002 Peopleware                                      
  Description    : This unit will contain the base CodeSite objects which
                   will be used for debugging purposes of the new FrameWork.                                                          
  History        :

  Date         By                   Description
  ----         --                   -----------
  28/10/2003   sLesage              Added a CodeSite object for Action stuff.
                                    This one is disabled by default.  
  09/09/2003   sLesage              Separated the Registration /
                                    UnRegistration routines on csFWRegister,
                                    which is Disabled by Default.
  21/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkCodeSiteObjects;

interface

{$IFDEF CODESITE}
uses
  csIntf, Graphics;

var
  csFW              : TCodeSite = nil;
  csFWActions       : TCodeSite = nil;
  csFWRegister      : TCodeSite = nil;
  csFWController    : TCodeSite = nil;
  csFWDataModule    : TCodeSite = nil;
  csFWForm          : TCodeSite = nil;
  csFWListView      : TCodeSite = nil;
  csFWRecordView    : TCodeSite = nil;
  csFWControlLayout : TCodeSite = nil;

  csFWColor      : TColor = clMoneyGreen;
{$ENDIF}

implementation

uses
  SysUtils;

{$IFDEF CODESITE}
initialization
  csFW                         := TCodeSite.Create( Nil );
  csFW.Name                    := 'csFW';
  csFW.Category                := 'FrameWork';
  csFW.Enabled                 := True;
  csFW.CategoryColor           := csFWColor;

  csFWActions                  := TCodeSite.Create( Nil );
  csFWActions.Name             := 'csFWActions';
  csFWActions.Category         := 'FrameWork Actions';
  csFWActions.Enabled          := False;
  csFWActions.CategoryColor    := csFWColor;

  { FrameWork Registration / Unregistration routines are working fine, so
    by default we have disabled the csFWRegister.  This way our CodeSite
    won't get filled with the Registration / Unregistration routine calls }
  csFWRegister                 := TCodeSite.Create( Nil );
  csFWRegister.Name            := 'csFWRegister';
  csFWRegister.Category        := 'FrameWork Registration / Unregistration Routins';
  csFWRegister.Enabled         := False;
  csFWRegister.CategoryColor   := csFWColor;

  csFWController               := TCodeSite.Create( Nil );
  csFWController.Name          := 'csFWController';
  csFWController.Category      := 'FrameWork Controller';
  csFWController.Enabled       := True;
  csFWController.CategoryColor := csFWColor;

  csFWDataModule               := TCodeSite.Create( Nil );
  csFWDataModule.Name          := 'csFWDataModule';
  csFWDataModule.Category      := 'FrameWork DataModule';
  csFWDataModule.Enabled       := True;
  csFWDataModule.CategoryColor := csFWColor;

  csFWForm                     := TCodeSite.Create( Nil );
  csFWForm.Name                := 'csFWForm';
  csFWForm.Category            := 'FrameWork Form';
  csFWForm.Enabled             := True;
  csFWForm.CategoryColor       := csFWColor;

  csFWListView                 := TCodeSite.Create( Nil );
  csFWListView.Name            := 'csFWListView';
  csFWListView.Category        := 'FrameWork ListView';
  csFWListView.Enabled         := True;
  csFWListView.CategoryColor   := csFWColor;

  csFWRecordView               := TCodeSite.Create( Nil );
  csFWRecordView.Name          := 'csFWRecordView';
  csFWRecordView.Category      := 'FrameWork RecordView';
  csFWRecordView.Enabled       := True;
  csFWRecordView.CategoryColor := csFWColor;

  csFWControlLayout               := TCodeSite.Create( Nil );
  csFWControlLayout.Name          := 'csFWRecordViewControlLayout';
  csFWControlLayout.Category      := 'FrameWork RecordView Control Layout';
  csFWControlLayout.Enabled       := False;
  csFWControlLayout.CategoryColor := csFWColor;

finalization
  FreeAndNil( csFW );
  FreeAndNil( csFWActions );
  FreeAndNil( csFWController );
  FreeAndNil( csFWDataModule );
  FreeAndNil( csFWForm );
  FreeAndNil( csFWListView );
  FreeAndNil( csFWRecordView );
  FreeAndNil( csFWControlLayout );
  FreeAndNil( csFWRegister );
{$ENDIF}

end.
