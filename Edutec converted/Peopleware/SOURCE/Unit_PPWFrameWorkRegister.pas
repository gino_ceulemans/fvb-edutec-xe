{*****************************************************************************
  Name           : Unit_PPWFrameWorkRegister 
  Author         : Lesage Stefaan                                           
  Copyright      : (c) 2002 Peopleware
  Description    : This unit will contain all the registration for the
                   Components, Modules, Actions, Component- and Property
                   Editors which make up the FrameWork.
  History        :

  Date         By                   Description
  ----         --                   -----------
  23/11/2004   sLesage              Added the RegisterWizard procedure.
  06/12/2002   sLesage              Added the Registration for the
                                    TPPWFrameWorkListViewRefreshAction action.
  21/11/2002   slesage              Initial creation of the Unit.
 *****************************************************************************}

unit Unit_PPWFrameWorkRegister;

interface

  procedure Register;
  procedure RegisterFrameWorkActions;
  procedure RegisterFrameWorkComponents;
  procedure RegisterFrameWorkPropertyEditors;
  procedure RegisterFrameWorkComponentEditors;
  procedure RegisterFrameWorkModules;
  procedure RegisterWizards;

implementation

{$R PPWFrameWorkComponents.res}

uses
  ActnList, Classes, DesignIntf, DesignEditors, ToolsAPI,//ActnRes,
  Actions,
  {$IFDEF CODESITE}
  Unit_PPWFrameWorkCodeSiteObjects,
  {$ENDIF}
  Unit_PPWFrameWorkActions,
  Unit_PPWFrameWorkActionResources,
  Unit_PPWFrameWorkADO,
  Unit_PPWFrameWorkController,
  Unit_PPWFrameWorkClasses,
  Unit_PPWFrameWorkDataModule,
  Unit_PPWFrameWorkForm,
  Unit_PPWFrameWorkListView,
  Unit_PPWFrameWorkPropertyEditors,
  Unit_PPWFrameWorkRecordView,
  Unit_PPWFrameWorkComponents, Unit_PPWFrameWorkCustomModules,
  Unit_PPWFrameWorkEntityWizard;

procedure Register;
begin
  RegisterFrameWorkComponents;
  RegisterFrameWorkComponentEditors;
  RegisterFrameWorkPropertyEditors;

  RegisterFrameWorkModules;
  RegisterFrameWorkActions;
  RegisterWizards;
end;

procedure RegisterFrameWorkActions;
begin
//  RegisterActions( 'Window',
//                   [ TWindowCloseAll ],
//                     TfrmPPWFrameWorkActionResources );
  RegisterActions( 'PeopleWare FrameWork Controller',
                   [ TPPWFrameWorkShowListViewAction ],
                     TfrmPPWFrameWorkActionResources );
  RegisterActions( 'PeopleWare FrameWork ListView',
                   [ TPPWFrameWorkListViewAddAction,
                     TPPWFrameWorkListViewDeleteAction,
                     TPPWFrameWorkListViewEditAction,
                     TPPWFrameWorkListViewConsultAction,
                     TPPWFrameWorkListViewCancelFilterAction,
                     TPPWFrameWorkListViewFilterAction,
                     TPPWFrameWorkListViewRefreshAction ],
                     TfrmPPWFrameWorkActionResources );
  RegisterActions( 'PeopleWare FrameWork ListView (DevExpress)',
                   [ TPPWFrameWorkListViewGridPrint,
                     TPPWFrameWorkListViewGridExportToHTML,
                     TPPWFrameWorkListViewGridExportToXLS,
                     TPPWFrameWorkListViewGridExportToXML,
                     TPPWFrameWorkListViewGridCustomiseColumns ],
                     TfrmPPWFrameWorkActionResources );
end;

procedure RegisterFrameWorkComponents;
begin
  RegisterComponents( 'PPW FrameWork', [ TPPWFrameWorkController,
                                         TPPWFrameWorkClientDataSet,
                                         TPPWFrameWorkADOConnection,
                                         TPPWFrameWorkADOQuery,
                                         TPPWFrameWorkADOStoredProc,
                                         TPPWFrameWorkADOTable ] );
end;

procedure RegisterFrameWorkComponentEditors;
begin
end;

procedure RegisterFrameWorkModules;
begin
  RegisterCustomModule( TPPWFrameWorkDataModule, TPPWFrameWorkDataModuleModule );
  RegisterCustomModule( TPPWFrameWorkForm, TCustomModule );
  RegisterCustomModule( TPPWFrameWorkListView, TCustomModule );
  RegisterCustomModule( TPPWFrameWorkRecordView, TPPWFrameWorkRecordViewModule {TCustomModule} );
end;

procedure RegisterFrameWorkPropertyEditors;
begin
  RegisterPropertyEditor ( TypeInfo( TPPWDataModuleClassName ),
                           TPPWFrameWorkController, 'DataModuleClass',
                           TPPWDataModuleClassProperty );
  RegisterPropertyEditor ( TypeInfo( TPPWRecordViewClassName ),
                           TPPWFrameWorkController, 'RecordViewClass',
                           TPPWRecordViewClassProperty );
  RegisterPropertyEditor ( TypeInfo( TPPWListViewClassName ),
                           TPPWFrameWorkController, 'ListViewClass',
                           TPPWListViewClassProperty );
  RegisterPropertyEditor ( TypeInfo( TPPWDataModuleClassName ),
                           TPPWFrameWorkShowListViewAction, 'DataModuleClass',
                           TPPWActionDataModuleClassProperty );
  RegisterPropertyEditor ( TypeInfo( TPPWListViewClassName ),
                           TPPWFrameWorkDataModule, 'ListViewClass',
                           TPPWDataModuleListViewClassProperty );
  RegisterPropertyEditor ( TypeInfo( TPPWRecordViewClassName ),
                           TPPWFrameWorkDataModule, 'RecordViewClass',
                           TPPWDataModuleRecordViewClassProperty );
  RegisterPropertyEditor ( TypeInfo( string ),
                           TPPWFrameWorkDataModule, 'KeyFields',
                           TPPWDataModuleKeyFieldsProperty );
  RegisterPropertyEditor ( TypeInfo( TPPWDataModuleClassName ),
                           TPPWFrameWorkDataModuleDetail, 'DataModuleClass',
                           TPPWActionDataModuleClassProperty );
  RegisterPropertyEditor( TypeInfo( TStrings ), TPPWFrameWorkADOQuery, 'SQL', Nil );
end;

procedure RegisterWizards;
begin
  RegisterPackageWizard( TPPWFrameWorkEntityCreatorWizard.Create );
end;    

end.
