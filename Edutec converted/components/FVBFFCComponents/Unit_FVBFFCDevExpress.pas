{*****************************************************************************
  This unit will contain some direct descendants of DevExpress Components.

  @Name       Unit_FVBFFCDevExpress
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  26/07/2005   sLesage              CharCase removed from the TextEdits.
  15/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCDevExpress;

interface

uses
  cxGrid, cxStyles, cxGridPopupMenu, cxLookAndFeels, dxPSCore, cxHint,
  cxEdit, cxSplitter, cxButtons, cxGroupBox, cxTextEdit, cxMaskEdit,
  cxMemo, cxCalendar, cxButtonEdit, cxCheckBox, cxDropDownEdit, cxSpinEdit,
  cxCalc, cxHyperLinkEdit, cxTimeEdit, cxCurrencyEdit, cxDBLookupComboBox,
  cxRadioGroup, cxLabel, cxProgressBar, cxCheckGroup, cxDBEdit, cxDBLabel,
  cxDBProgressBar, cxDBCheckGroup, cxRichEdit, cxDBRichEdit, dxNavBar,
  cxImageComboBox, cxImage, cxBlobEdit, dxStatusBar, Classes,
  Unit_FVBFFCInterfaces, cxShellListView, //dxsbar,
  StdCtrls, cxPC;

Type
  TFVBFFCTextEditProperties             = class( TcxTextEditProperties )
  published
    property CharCase default ecUpperCase;
  end;

  TFVBFFCPageControl                = class( TcxPageControl );
  TFVBFFCCustomMaskEditAccess       = class( TcxCustomMaskEdit );

  TFVBFFCButton                     = class( TcxButton )
  public
    Constructor Create( AOWner: TComponent ); override;
    destructor Destroy; override;
  published
    property  Margin  default 10;
    property  Spacing default -1;
  end;

  TFVBFFCGroupBox                   = class( TcxGroupBox );


  TFVBFFCEditRepository             = class( TcxEditRepository )
  end;

  TFVBFFCGrid                       = class( TcxGrid );

  TFVBFFCGridPopupMenu              = class( TcxGridPopupMenu )
  end;

  TFVBFFCStyleRepository            = class( TcxStyleRepository )
  end;


  TFVBFFCLookAndFeelController      = class( TcxLookAndFeelController )
  public
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
  published
    property Kind stored False;
  end;


  TFVBFFCComponentPrinter           = class( TdxComponentPrinter )
  end;

  TFVBFFCHintStyleController        = class( TcxHintStyleController )
  end;

  TFVBFFCDefaultEditStyleController = class( TcxDefaultEditStyleController )
  end;

  TFVBFFCEditStyleController        = class( TcxEditStyleController )
  end;

  TFVBFFCSplitter                   = class( TcxSplitter )
  end;


  TFVBFFCTextEdit                   = class( TcxTextEdit )
  end;


  TFVBFFCMaskEdit                   = class( TcxMaskEdit )
  public
    function GetEmptyString : String; virtual;
  end;

  TFVBFFCMemo                       = class( TcxMemo )
  end;

  TFVBFFCImage                      = class( TcxImage )
  end;

  TFVBFFCImageComboBox              = class( TcxImageComboBox )
  end;

  TFVBFFCBlobEdit                   = class( TcxBlobEdit )
  end;


  TFVBFFCDateEdit                   = class( TcxDateEdit )
  public
    function GetEmptyString : String; virtual;
  end;

  TFVBFFCButtonEdit                 = class( TcxButtonEdit )
  end;

  TFVBFFCCheckBox                   = class( TcxCheckBox )
  end;

  TFVBFFCComboBox                   = class( TcxComboBox )
  end;

  TFVBFFCSpinEdit                   = class( TcxSpinEdit )
  end;


  TFVBFFCCalcEdit                   = class( TcxCalcEdit )
  end;

  TFVBFFCHyperLinkEdit              = class( TcxHyperLinkEdit )
  end;

  TFVBFFCTimeEdit                   = class( TcxTimeEdit )
  end;

  TFVBFFCCurrencyEdit               = class( TcxCurrencyEdit )
  end;

  TFVBFFCLookupComboBox             = class( TcxLookupComboBox )
  end;

  TFVBFFCRadioButton                = class( TcxRadioButton )
  end;

  TFVBFFCRadioGroup                 = class( TcxRadioGroup )
  end;


  TFVBFFCLabel                      = class( TcxLabel{, IEDUParentColorAccess} )
//  protected
//    function GetParentColor : Boolean; Virtual;
//    procedure SetParentColor( const Value : Boolean ); virtual;
  end;

  TFVBFFCProgressBar                = class( TcxProgressBar )
  end;

  TFVBFFCRichEdit                   = class( TcxRichEdit )
  end;

  TFVBFFCCheckGroup                 = class( TcxCheckGroup )
  end;

  TFVBFFCNavBar                     = class( TdxNavBar )
  end;

  TFVBFFCStatusBar                  = class( TdxStatusBar )
  end;


  TFVBFFCDBTextEdit                   = class( TcxDBTextEdit )
  public
    constructor Create( AOwner : TComponent ); override;
    //class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
    destructor Destroy; override;
  end;

  TFVBFFCDBMaskEdit                   = class( TcxDBMaskEdit )
  public
    function GetEmptyString : String; virtual;
  end;

  TFVBFFCDBImage                      = class( TcxDBImage )
  end;

  TFVBFFCDBImageComboBox              = class( TcxDBImageComboBox )
  end;

  TFVBFFCDBBlobEdit                   = class( TcxDBBlobEdit )
  end;


  TFVBFFCDBMemo                       = class( TcxDBMemo )
  end;

  TFVBFFCDBDateEdit                   = class( TcxDBDateEdit )
  public
    function GetEmptyString : String; virtual;
  end;

  TFVBFFCDBButtonEdit                 = class( TcxDBButtonEdit )
  end;

  TFVBFFCDBCheckBox                   = class( TcxDBCheckBox )
  end;

  TFVBFFCDBComboBox                   = class( TcxDBComboBox )
  end;

  TFVBFFCDBSpinEdit                   = class( TcxDBSpinEdit )
  end;


  TFVBFFCDBCalcEdit                   = class( TcxDBCalcEdit )
  end;

  TFVBFFCDBHyperLinkEdit              = class( TcxDBHyperLinkEdit )
  end;

  TFVBFFCDBTimeEdit                   = class( TcxDBTimeEdit )
  end;


  TFVBFFCDBCurrencyEdit               = class( TcxDBCurrencyEdit )
  end;

  TFVBFFCDBLookupComboBox             = class( TcxDBLookupComboBox )
  end;

  TFVBFFCDBRadioGroup                 = class( TcxDBRadioGroup )
  end;


  TFVBFFCDBLabel                      = class( TcxDBLabel{, IEDUParentColorAccess} )
//  protected
//    function GetParentColor : Boolean; Virtual;
//    procedure SetParentColor( const Value : Boolean ); virtual;
  end;

  TFVBFFCDBProgressBar                = class( TcxDBProgressBar )
  end;

  TFVBFFCDBRichEdit                   = class( TcxDBRichEdit )
  end;

  TFVBFFCDBCheckGroup                 = class( TcxDBCheckGroup )
  end;

  TFVBFFCShellListView                = class( TcxShellListView )
  end;

  TFVBFFCSideBar                      = class( TdxNavBar )
  end;

  //TFVBFFCSideBarStore                    = class( TdxSideBarStore )
  //end;

  function GetEmptyMaskString( aMaskEdit : TcxCustomMaskEdit ) : String;

implementation

uses Variants;

{*****************************************************************************
  This function returns an Empty Mask String for a given aMaskEdit.
  This is somehow a Hack, since the Mode is declared as protected on the
  TcxCustomMaskEdit, we had to make a TcxCustomMaskEditAccess class to be
  able to call the Mode property.

  @Name       GetEmptyMaskString
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function GetEmptyMaskString( aMaskEdit : TcxCustomMaskEdit ) : String;
begin
  Result := '';

  if ( Assigned( aMaskEdit ) ) then
  begin
    if aMaskEdit.ActiveProperties.IsMasked then
    begin
      if TFVBFFCCustomMaskEditAccess( aMaskEdit ).Mode <> nil then
      begin
        Result := TFVBFFCCustomMaskEditAccess( aMaskEdit ).Mode.GetEmptyString
      end
      else
      begin
        Result := '';
      end;
    end
    else
    begin
      Result := '';
    end;
  end;
end;

{ TFVBFFCMaskEdit }

{*****************************************************************************
  This function will be used to return an empty and masked string for the
  MaskEdit.

  @Name       TFVBFFCMaskEdit.GetEmptyString
  @author     slesage
  @param      Returns the Empty MaskString for the MaskEdit.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCMaskEdit.GetEmptyString: String;
begin
  Result := GetEmptyMaskString( Self );
end;

{ TFVBFFCDBMaskEdit }

{*****************************************************************************
  This function will be used to return an empty and masked string for the
  MaskEdit.

  @Name       TFVBFFCDBMaskEdit.GetEmptyString
  @author     slesage
  @param      Returns the Empty MaskString for the MaskEdit.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCDBMaskEdit.GetEmptyString: String;
begin
  Result := GetEmptyMaskString( Self );
end;

{ TFVBFFCButton }

constructor TFVBFFCButton.Create(AOWner: TComponent);
begin
  inherited;
  Margin := 10;
  Spacing := -1;
end;

destructor TFVBFFCButton.Destroy;
begin
  inherited;
end;

//function TFVBFFCLabel.GetParentColor: Boolean;
//begin
//  Result := Inherited ParentColor;
//end;

//procedure TFVBFFCLabel.SetParentColor(const Value: Boolean);
//begin
//  Inherited ParentColor := Value;
//end;

{ TFVBFFCDBLabel }

//function TFVBFFCDBLabel.GetParentColor: Boolean;
//begin
//  Result := Inherited ParentColor;
//end;

//procedure TFVBFFCDBLabel.SetParentColor(const Value: Boolean);
//begin
//  Inherited ParentColor := Value;
//end;

{ TFVBFFCDBTextEdit }

constructor TFVBFFCDBTextEdit.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
end;

destructor TFVBFFCDBTextEdit.Destroy;
begin
  inherited Destroy;
end;

{
class function TFVBFFCDBTextEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TFVBFFCTextEditProperties;
end;
}

{ TFVBFFCLookAndFeelController }

constructor TFVBFFCLookAndFeelController.Create(aOwner: TComponent);
begin
  inherited Create( AOwner );
  Kind := lfOffice11;
end;

destructor TFVBFFCLookAndFeelController.Destroy;
begin
  inherited Destroy;
end;

{ TFVBFFCDateEdit }

function TFVBFFCDateEdit.GetEmptyString: String;
begin
  Result := GetEmptyMaskString( Self );
end;

{ TFVBFFCDBDateEdit }

function TFVBFFCDBDateEdit.GetEmptyString: String;
begin
  Result := GetEmptyMaskString( Self );
end;

end.

