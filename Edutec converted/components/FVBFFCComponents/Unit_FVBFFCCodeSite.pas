{*****************************************************************************
  This unit will contain the base CodeSite objects which will be used for
  debugging purposes.
  
  @Name       Unit_FVBFFCtecCodeSite
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  18/02/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCCodeSite;

interface

{$IFDEF CODESITE}
uses
  csIntf, Graphics;

var
  csFVBFFCClient        : TCodeSite = nil;
  csFVBFFCServer        : TCodeSite = nil;
  csFVBFFCComponents    : TCodeSite = nil;
  csFVBFFCDataModule    : TCodeSite = nil;
  csFVBFFCForm          : TCodeSite = nil;
  csFVBFFCListView      : TCodeSite = nil;
  csFVBFFCRecordView    : TCodeSite = nil;
  csFVBFFCControlLayout : TCodeSite = nil;

const
  csFVBFFCServerColor     = clSkyBlue;
  csFVBFFCClientColor     = TColor( $B5BDE3 ) ;
  csFVBFFCComponentsColor = TColor( $90A1D5 );
{$ENDIF}

implementation

uses
  SysUtils;

{$IFDEF CODESITE}
initialization
  csFVBFFCClient                   := TCodeSite.Create( Nil );
  csFVBFFCClient.Name              := 'csFVBFFC';
  csFVBFFCClient.Category          := 'FVBFFC Client';
  csFVBFFCClient.Enabled           := True;
  csFVBFFCClient.CategoryColor     := csFVBFFCClientColor;

  csFVBFFCServer                   := TCodeSite.Create( Nil );
  csFVBFFCServer.Name              := 'csFVBFFC';
  csFVBFFCServer.Category          := 'FVBFFC Server';
  csFVBFFCServer.Enabled           := True;
  csFVBFFCServer.CategoryColor     := csFVBFFCServerColor;

  csFVBFFCComponents               := TCodeSite.Create( Nil );
  csFVBFFCComponents.Name          := 'csFVBFFCComponents';
  csFVBFFCComponents.Category      := 'FVBFFC Components';
  csFVBFFCComponents.Enabled       := True;
  csFVBFFCComponents.CategoryColor := csFVBFFCComponentsColor;


  csFVBFFCDataModule               := TCodeSite.Create( Nil );
  csFVBFFCDataModule.Name          := 'csFVBFFCDataModule';
  csFVBFFCDataModule.Category      := 'FVBFFC DataModule';
  csFVBFFCDataModule.Enabled       := True;
  csFVBFFCDataModule.CategoryColor := csFVBFFCClientColor;

  csFVBFFCForm                     := TCodeSite.Create( Nil );
  csFVBFFCForm.Name                := 'csFVBFFCForm';
  csFVBFFCForm.Category            := 'FVBFFC Form';
  csFVBFFCForm.Enabled             := True;
  csFVBFFCForm.CategoryColor       := csFVBFFCClientColor;

  csFVBFFCListView                 := TCodeSite.Create( Nil );
  csFVBFFCListView.Name            := 'csFVBFFCListView';
  csFVBFFCListView.Category        := 'FVBFFC ListView';
  csFVBFFCListView.Enabled         := True;
  csFVBFFCListView.CategoryColor   := csFVBFFCClientColor;

  csFVBFFCRecordView               := TCodeSite.Create( Nil );
  csFVBFFCRecordView.Name          := 'csFVBFFCRecordView';
  csFVBFFCRecordView.Category      := 'FVBFFC RecordView';
  csFVBFFCRecordView.Enabled       := True;
  csFVBFFCRecordView.CategoryColor := csFVBFFCClientColor;

  csFVBFFCControlLayout               := TCodeSite.Create( Nil );
  csFVBFFCControlLayout.Name          := 'csFVBFFCRecordViewControlLayout';
  csFVBFFCControlLayout.Category      := 'FVBFFC RecordView Control Layout';
  csFVBFFCControlLayout.Enabled       := False;
  csFVBFFCControlLayout.CategoryColor := csFVBFFCClientColor;

finalization
  FreeAndNil( csFVBFFCClient );
  FreeAndNil( csFVBFFCServer );
  FreeAndNil( csFVBFFCComponents );
  FreeAndNil( csFVBFFCDataModule );
  FreeAndNil( csFVBFFCForm );
  FreeAndNil( csFVBFFCListView );
  FreeAndNil( csFVBFFCRecordView );
  FreeAndNil( csFVBFFCControlLayout );
{$ENDIF}

end.
