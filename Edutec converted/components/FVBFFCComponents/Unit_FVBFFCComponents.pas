{*****************************************************************************
  This unit contains some subclasses of general Delphi Components.
  
  @Name       Unit_EdutecComponents
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  05/10/2005   sLesage              Added the PVDKCommandLine component.
  15/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCComponents;

interface

uses
  ActnList, Controls, Classes;

type
  TFVBFFCActionList = class( TActionList );
  TFVBFFCImageList  = class( TImageList  );

  TFVBFFCCommandLine = class( TComponent)
  private
    initialized : Boolean;
    fParams : TStrings;
    fUnknownParams : TStrings;
    fSwitches: TStrings;
    FCaseSensitive: Boolean;
  protected
    { Property Getters }
    function GetParams: TStrings; virtual;
    function GetUnknownParams: TStrings; virtual;

    { Property Setters }
    procedure SetCaseSensitive( const Value: Boolean ); virtual;
    procedure SetSwitches( const Value: TStrings ); virtual;

    { Other Methods }
    procedure BuildParams; virtual;
  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;
    property Params : TStrings read GetParams;
    property UnknownParams : TStrings read GetUnknownParams;
  published
    property Switches : TStrings read fSwitches write SetSwitches;
    property CaseSensitive : Boolean read FCaseSensitive write SetCaseSensitive;
  end;

  
implementation

uses
  SysUtils;

{ TFVBFFCCommandLine }

procedure TFVBFFCCommandLine.BuildParams;
var
  lcvParams, lcvIdentifiers, lcvSeperatedIdentifiers : integer;
  fHelpStrings : TStringList;
  CurrentName : string;
  Found : Boolean;
begin
  if csDesigning in ComponentState then exit;
  fHelpStrings := TStringList.Create;
  fHelpStrings.Delimiter := ';';
  fHelpStrings.QuoteChar := #0;
  for lcvParams := 1 to ParamCount do
  begin
    Found := false;
    for lcvIdentifiers := 0 to Pred( fSwitches.Count) do
    begin
      CurrentName := fSwitches.Names[ lcvIdentifiers];
      fHelpStrings.DelimitedText := fSwitches.Values[ CurrentName];
      for lcvSeperatedIdentifiers := 0 to Pred( fHelpStrings.Count) do
      begin
        if Copy( ParamStr( lcvParams), 1, Length( fHelpStrings[ lcvSeperatedIdentifiers])) =  fHelpStrings[ lcvSeperatedIdentifiers] then
        begin
          if fParams.IndexOfName( CurrentName) <> - 1 then
            Raise Exception.CreateFmt( 'Duplicate CommandLine-Option "%s"', [CurrentName])
          else
          begin
            fParams.Add( CurrentName + '=' + Copy( ParamStr( lcvParams), Length( fHelpStrings[ lcvSeperatedIdentifiers]) + 1, 256));
            Found := true;
            break;
          end;
        end;
      end;
      if found then break;
    end;
    if not found then
      fUnknownParams.Add( ParamStr( lcvParams));
  end;
  fHelpStrings.Free;
  Initialized := true;
end;

constructor TFVBFFCCommandLine.Create(AOwner: TComponent);
begin
  inherited Create( AOwner );
  fParams := TStringList.Create;
  fSwitches := TStringList.Create;
  fUnknownParams := TSTringList.Create;
end;

destructor TFVBFFCCommandLine.Destroy;
begin
  FreeAndNil( fParams );
  FreeAndNil( fSwitches );
  FreeAndNil( fUnknownParams );
  inherited;
end;

function TFVBFFCCommandLine.GetParams: TStrings;
begin
  if not Initialized then BuildParams;
  Result := fParams;
end;

function TFVBFFCCommandLine.GetUnknownParams: TStrings;
begin
  if not Initialized then BuildParams;
  Result := fUnknownParams;
end;

procedure TFVBFFCCommandLine.SetCaseSensitive(const Value: Boolean);
begin
  if ( Value <> CaseSensitive ) then
  begin
    FCaseSensitive := Value;
    fParams.Clear;
    fUnknownParams.Clear;
    Initialized := false;
  end;
end;

procedure TFVBFFCCommandLine.SetSwitches(const Value: TStrings);
begin
  fSwitches.Assign( Value);
  fParams.Clear;
  fUnknownParams.Clear;
  Initialized := false
end;

end.

