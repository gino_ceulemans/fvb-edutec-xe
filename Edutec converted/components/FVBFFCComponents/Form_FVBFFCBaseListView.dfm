object FVBFFCBaseListView: TFVBFFCBaseListView
  Left = 501
  Top = 274
  Height = 528
  Align = alNone
  Caption = 'FVBFFC Base ListView'
  ClientHeight = 489
  ClientWidth = 591
  Color = clBtnFace
  Constraints.MinWidth = 528
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  FormStyle = fsNormal
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsNormal
  OnClose = FVBFFCListViewClose
  OnCloseQuery = FVBFFCListViewCloseQuery
  OnKeyDown = FVBFFCListViewKeyDown
  OnShow = FVBFFCListViewShow
  DataSource = srcMain
  Registered = False
  OnInitialise = FVBFFCListViewInitialise
  PixelsPerInch = 96
  TextHeight = 13
  object pnlSelection: TPanel
    Left = 0
    Top = 448
    Width = 591
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Color = clInactiveCaption
    TabOrder = 0
    Visible = False
    ExplicitTop = 385
    ExplicitWidth = 512
    object cxbtnEDUButton1: TFVBFFCButton
      Left = 304
      Top = 8
      Width = 96
      Height = 25
      Caption = 'Select'
      ModalResult = 1
      OptionsImage.Margin = 10
      OptionsImage.Spacing = -1
      TabOrder = 0
    end
    object cxbtnEDUButton2: TFVBFFCButton
      Left = 416
      Top = 8
      Width = 96
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      OptionsImage.Margin = 10
      OptionsImage.Spacing = -1
      TabOrder = 1
    end
  end
  object pnlList: TFVBFFCPanel
    Left = 4
    Top = 4
    Width = 583
    Height = 440
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 0
    TabOrder = 1
    StyleBackGround.BackColor = clAppWorkSpace
    StyleBackGround.BackColor2 = clAppWorkSpace
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clAppWorkSpace
    StyleClientArea.BackColor2 = clAppWorkSpace
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitWidth = 504
    ExplicitHeight = 377
    object cxgrdList: TcxGrid
      Left = 0
      Top = 29
      Width = 583
      Height = 411
      Align = alClient
      Constraints.MinHeight = 195
      PopupMenu = dxbpmnGrid
      TabOrder = 0
      ExplicitTop = 182
      ExplicitWidth = 504
      ExplicitHeight = 195
      object cxgrdtblvList: TcxGridDBTableView
        OnKeyDown = cxgrdtblvListKeyDown
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Filter.Enabled = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        OnCellDblClick = cxgrdtblvListCellDblClick
        OnCustomDrawCell = cxgrdtblvListCustomDrawCell
        DataController.DataModeController.GridMode = True
        DataController.DataSource = srcMain
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        DataController.OnSortingChanged = cxgrdtblvListDataControllerSortingChanged
        OptionsCustomize.ColumnFiltering = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsView.GroupByBox = False
        OptionsView.HeaderAutoHeight = True
        OptionsView.Indicator = True
        Styles.StyleSheet = dtmFVBFFCMainClient.GridTableViewStyleSheetDevExpress
      end
      object cxgrdlvlList: TcxGridLevel
        GridView = cxgrdtblvList
      end
    end
    object pnlFormTitle: TFVBFFCPanel
      Left = 0
      Top = 4
      Width = 583
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 0
      Caption = 'List'
      TabOrder = 1
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clWhite
      StyleClientArea.BackColor2 = clGradientInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      ExplicitTop = 157
      ExplicitWidth = 504
    end
    object pnlSearchCriteriaSpacer: TFVBFFCPanel
      Left = 0
      Top = 0
      Width = 583
      Height = 4
      Align = alTop
      BevelOuter = bvNone
      BorderWidth = 4
      ParentColor = True
      TabOrder = 2
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      ExplicitTop = 153
      ExplicitWidth = 504
    end
    object pnlSearchCriteria: TFVBFFCFoldablePanel
      Left = 0
      Top = 29
      Width = 583
      Height = 0
      Align = alTop
      BorderWidth = 0
      Caption = 'Search Criteria'
      Expanded = True
      ExpandedHeight = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleHeader.BackColor = clMenuHighlight
      StyleHeader.BackColor2 = clMenuHighlight
      StyleHeader.Font.Charset = DEFAULT_CHARSET
      StyleHeader.Font.Color = clHighlightText
      StyleHeader.Font.Height = -11
      StyleHeader.Font.Name = 'Verdana'
      StyleHeader.Font.Style = [fsBold]
      StyleFoldArea.BackColor = 15914442
      StyleFoldArea.BackColor2 = 15914442
      StyleFoldArea.Font.Charset = DEFAULT_CHARSET
      StyleFoldArea.Font.Color = clWindowText
      StyleFoldArea.Font.Height = -11
      StyleFoldArea.Font.Name = 'Verdana'
      StyleFoldArea.Font.Style = []
      ExplicitTop = 0
      ExplicitWidth = 504
      ExplicitHeight = 153
      object pnlSearchCriteriaButtons: TPanel
        Left = 1
        Top = -31
        Width = 581
        Height = 30
        Align = alBottom
        BevelOuter = bvNone
        Color = clInactiveCaption
        TabOrder = 0
        ExplicitTop = 122
        ExplicitWidth = 502
        DesignSize = (
          581
          30)
        object cxbtnApplyFilter: TFVBFFCButton
          Left = 413
          Top = 2
          Width = 75
          Height = 25
          Hint = 'Filter|Filter or enter Search Criteria'
          Anchors = [akTop, akRight]
          Caption = 'Filter'
          OptionsImage.Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A88D8D00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A0828100A88D8D00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A0828100DBD6D600A88D8D00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A0828100DBD6D600A0828100FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A0828100DBD6D600A0828100FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A0828100DBD6D600B5A0A000A082
            8100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00A0828100DBD6D600BBA9A900B5A0A000AE97
            9600A0828100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00A0828100DBD6D600C5B7B700BBA9A900B5A0A000AE97
            9600A88D8D00A0828100FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00A0828100DBD6D600D0C6C600C5B7B700A4888700A4888700AE97
            9600A88D8D00A1848300A0828100FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00A0828100E6E6E600DBD6D600A4888700A48887006FB1CD00A4888700A082
            8100A0828100A18483009B7B7A00A0828100FF00FF00FF00FF00FF00FF00FF00
            FF00A0828100A082810046A5BC00A4FFFF0045D0FD008AD7FD0045A8E3000022
            AC001725AC0039709200A0828100A0828100FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00A082810032A5CD00A4EBFB008AF5FD0045C5F6000022BF000000
            BE000000CC001725AC005D8E9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00AA521000586957004077870067C5F6001E4DD200191ECA000000
            D9000000DE000000C5008D8DD300FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00C0733100AC410000B74C00007E604C00412E99007084E800191E
            CA000000CF007171CF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0074474700DCA37200ECA54400AF6810009F7153003838C4003838
            C4008D8DDD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00B99E980097655400CBBABA00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          OptionsImage.Margin = 10
          OptionsImage.Spacing = -1
          TabOrder = 0
          OnClick = cxbtnApplyFilterClick
          ExplicitLeft = 334
        end
        object cxbtnClearFilter: TFVBFFCButton
          Left = 501
          Top = 2
          Width = 75
          Height = 25
          Action = dtmFVBFFCMainClient.acListViewCancelFilter
          Anchors = [akTop, akRight]
          OptionsImage.Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00AD8C8C00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A5848400AD8C8C00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A5848400DED6D600AD8C8C00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A5848400DED6D600A5848400FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A5848400DED6D600A5848400FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00A5848400DED6D600B5A5A500A584
            8400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00A5848400DED6D600BDADAD00B5A5A500AD94
            9400A5848400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00A5848400DED6D600C6B5B500BDADAD00B5A5A500AD94
            9400AD8C8C00A5848400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00A5848400DED6D600D6C6C600C6B5B500A58C8400A58C8400AD94
            9400AD8C8C00A5848400A5848400FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00A5848400E7E7E700DED6D600A58C8400A58C84006BB5CE00A58C8400A584
            8400A5848400A58484009C7B7B00A5848400FF00FF00FF00FF00FF00FF00FF00
            FF00A5848400A584840042A5BD00A5FFFF0042D6FF008CD6FF0042ADE7000021
            AD001021AD0039739400A5848400A5848400FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00A584840031A5CE00A5EFFF008CF7FF0042C6F7000021BD000000
            BD000000CE001021AD005A8C9C00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00AD5210005A6B52004273840063C6F700184AD6001818CE000000
            DE000000DE000000C6008C8CD600FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00C6733100AD420000B54A00007B634A0042299C007384EF001818
            CE000000CE007373CE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0073424200DEA57300EFA54200AD6B10009C7352003939C6003939
            C6008C8CDE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00BD9C9C0094635200CEBDBD00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          OptionsImage.Margin = 10
          OptionsImage.Spacing = -1
          TabOrder = 1
          ExplicitLeft = 422
        end
      end
    end
  end
  object pnlListTopSpacer: TFVBFFCPanel
    Left = 0
    Top = 0
    Width = 591
    Height = 4
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 4
    ParentColor = True
    TabOrder = 2
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitWidth = 512
  end
  object pnlListBottomSpacer: TFVBFFCPanel
    Left = 0
    Top = 444
    Width = 591
    Height = 4
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 4
    ParentColor = True
    TabOrder = 3
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitTop = 381
    ExplicitWidth = 512
  end
  object pnlListRightSpacer: TFVBFFCPanel
    Left = 587
    Top = 4
    Width = 4
    Height = 440
    Align = alRight
    BevelOuter = bvNone
    BorderWidth = 4
    ParentColor = True
    TabOrder = 4
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitLeft = 508
    ExplicitHeight = 377
  end
  object pnlListLeftSpacer: TFVBFFCPanel
    Left = 0
    Top = 4
    Width = 4
    Height = 440
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 4
    ParentColor = True
    TabOrder = 5
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    ExplicitHeight = 377
  end
  object srcMain: TFVBFFCDataSource
    DataSet = FVBFFCBaseDataModule.cdsList
    Left = 40
    Top = 392
  end
  object dxcpGridPrinter: TFVBFFCComponentPrinter
    CurrentLink = dxgrptlGridLink
    Version = 0
    Left = 8
    Top = 392
    object dxgrptlGridLink: TdxGridReportLink
      Component = cxgrdList
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 5080
      PrinterPage.Header = 5080
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210820
      PrinterPage.PageSize.Y = 297180
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object dxbmPopupMenu: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.NativeStyle = False
    MenuAnimations = maSlide
    PopupMenuLinks = <>
    SunkenBorder = True
    UseSystemFont = True
    Left = 72
    Top = 392
    object dxbbEdit: TdxBarButton
      Caption = 'Edit'
      Category = 0
      Hint = 'Edit|Edit the current Record'
      Visible = ivAlways
      ImageIndex = 2
      ShortCut = 113
    end
    object dxbbView: TdxBarButton
      Caption = 'View'
      Category = 0
      Hint = 'View|View the current Record'
      Visible = ivAlways
      ImageIndex = 3
    end
    object dxbbAdd: TdxBarButton
      Caption = 'Add'
      Category = 0
      Hint = 'Add|Add a new Record'
      Visible = ivAlways
      ImageIndex = 0
      ShortCut = 45
    end
    object dxbbDelete: TdxBarButton
      Caption = 'Delete'
      Category = 0
      Hint = 'Delete|Delete the current Record'
      Visible = ivAlways
      ImageIndex = 1
      ShortCut = 46
    end
    object dxbbRefresh: TdxBarButton
      Caption = 'Refresh Data'
      Category = 0
      Hint = 
        'Refresh Data|Refreshes the current data with the data from the D' +
        'B'
      Visible = ivAlways
      ImageIndex = 4
      ShortCut = 116
    end
    object dxBBCustomizeColumns: TdxBarButton
      Caption = 'Customise Columns'
      Category = 0
      Hint = 'Customise Columns|Show and Hide columns'
      Visible = ivAlways
      ImageIndex = 11
    end
    object dxbbPrintGrid: TdxBarButton
      Caption = 'Print List'
      Category = 0
      Hint = 'Print Grid|Print the Contents of the Grid'
      Visible = ivAlways
      ImageIndex = 7
    end
    object dxbbExportXLS: TdxBarButton
      Caption = 'Export XLS'
      Category = 0
      Hint = 'Export to Excel|Export the contents of the grid to an Excel File'
      Visible = ivAlways
      ImageIndex = 8
    end
    object dxbbExportXML: TdxBarButton
      Caption = 'Export XML'
      Category = 0
      Hint = 'Export to XML|Export the contents of the grid to an XML File'
      Visible = ivAlways
      ImageIndex = 10
    end
    object dxbbExportHTML: TdxBarButton
      Caption = 'Export HTML'
      Category = 0
      Hint = 'Export to HTML|Export the contents of the grid to a HTML File'
      Visible = ivAlways
      ImageIndex = 9
    end
  end
  object dxbpmnGrid: TdxBarPopupMenu
    BarManager = dxbmPopupMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxbbEdit'
      end
      item
        Visible = True
        ItemName = 'dxbbView'
      end
      item
        Visible = True
        ItemName = 'dxbbAdd'
      end
      item
        Visible = True
        ItemName = 'dxbbDelete'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbRefresh'
      end
      item
        Visible = True
        ItemName = 'dxBBCustomizeColumns'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbPrintGrid'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxbbExportXLS'
      end
      item
        Visible = True
        ItemName = 'dxbbExportXML'
      end
      item
        Visible = True
        ItemName = 'dxbbExportHTML'
      end>
    UseOwnFont = False
    Left = 104
    Top = 392
  end
  object srcSearchCriteria: TFVBFFCDataSource
    DataSet = FVBFFCBaseDataModule.cdsSearchCriteria
    Left = 136
    Top = 390
  end
end
