{*****************************************************************************
  This unit contains the Global PPWFrameWorkDataModule descendant used in the
  FVB / FCC Projects.
  
  @Name       Unit_FVBFFCDataModule
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  13/07/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCDataModule;

interface

uses
  Unit_PPWFrameWorkDataModule, Unit_PPWFrameWorkClasses, Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCInterfaces, Unit_PPWFrameWorkActions,
  SysUtils, Classes, DB;

type
  TFVBFFCDataSetEvent = procedure ( aDataSet : TDataSet ) of Object;

  TFVBFFCDataModule = class(TPPWFrameWorkDataModule, IFVBFFCDataModule )
  private
    FOnInitialise: TNotifyEvent;
    FAutoOpenWhenDetail: Boolean;
    FAutoOpenWhenSelection: Boolean;
    FAllowEmptyFilter: Boolean;
    FOnInitialisePrimaryKeyFields: TFVBFFCDataSetEvent;
    FOnInitialiseForeignKeyFields: TFVBFFCDataSetEvent;
    FOnInitialiseAdditionalFields: TFVBFFCDataSetEvent;
    FIsRecordViewDataModule: Boolean;
    FAsDetail: Boolean;
  protected
    { Property Getters }
    function GetIsRecordViewDataModule: Boolean;

    { Property Setters }
    procedure SetAllowEmptyFilter(const Value: Boolean); virtual;
    procedure SetIsRecordViewDataModule(const Value: Boolean);

    { Event Dispatching Methods }
    procedure DoInitialiseDataModule; virtual;
    procedure DoInitialiseRecord           ( aDataSet : TDataSet ); virtual;
    procedure DoInitialisePrimaryKeyFields ( aDataSet : TDataSet ); virtual;
    procedure DoInitialiseForeignKeyFields ( aDataSet : TDataSet ); virtual;
    procedure DoInitialiseAdditionalFields ( aDataSet : TDataSet ); virtual;

    { IEDUDataModule }
    function IsListViewActionAllowed( aAction                  : TPPWFrameWorkListViewAction ) : Boolean; virtual;
    function ShowRecordViewForCurrentRecord( aRecordViewMode   : TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False ) : IPPWFrameWorkRecordView; virtual;
    function CreateRecordViewForCurrentRecord( aRecordViewMode : TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False ) : IPPWFrameWorkRecordView; virtual;
  public
    { Public declarations }
    constructor Create( AOwner : TComponent; AAsSelection : Boolean = False ); override;
    constructor CreateAsDetail( AOwner : TComponent;
      AMasterDataModule : IPPWFrameWorkDataModule; AMasterRecordView: IPPWFrameworkRecordView ); override;
    constructor CreateNew(AOwner: TComponent; Dummy: Integer = 0); override;

    procedure OpenDataSets; override;

    procedure Add; override;
    procedure Delete; override;
    procedure Edit; override;
    procedure View; override;
    
    property IsRecordViewDataModule: Boolean read GetIsRecordViewDataModule write SetIsRecordViewDataModule;
  published
    property AsDetail              : Boolean read FAsDetail;
    property AllowEmptyFilter      : Boolean read FAllowEmptyFilter      write SetAllowEmptyFilter;
    property AutoOpenWhenDetail    : Boolean read FAutoOpenWhenDetail    write FAutoOpenWhenDetail;
    property AutoOpenWhenSelection : Boolean read FAutoOpenWhenSelection write FAutoOpenWhenSelection;

    property OnInitialise : TNotifyEvent                        read  FOnInitialise
                                                                write FOnInitialise;
    property OnInitialisePrimaryKeyFields : TFVBFFCDataSetEvent read  FOnInitialisePrimaryKeyFields
                                                                write FOnInitialisePrimaryKeyFields;
    property OnInitialiseForeignKeyFields : TFVBFFCDataSetEvent read  FOnInitialiseForeignKeyFields
                                                                write FOnInitialiseForeignKeyFields;
    property OnInitialiseAdditionalFields : TFVBFFCDataSetEvent read  FOnInitialiseAdditionalFields
                                                                write FOnInitialiseAdditionalFields;
  end;

var
  FVBFFCDataModule: TFVBFFCDataModule;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Unit_PPWFrameWorkController, Forms,
  Controls, TypInfo;

{$R *.dfm}

{ TEdutecDataModule }

{*****************************************************************************
  This method will be executed when the user Adds a new Record to a ListView.

  @Name       TEdutecDataModule.Add
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.Add;
begin
  ShowRecordViewForCurrentRecord( rvmAdd );
end;

{*****************************************************************************
  Overridden Constructor in which we will also call the DoInitialiseDataModule.

  @Name       TEdutecDataModule.Create
  @author     slesage
  @param      AOwner         The Owner of the DataModule.         
  @param      AAsSelection   Flag indicating if the DataModule was created for
                             selection purposes.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TFVBFFCDataModule.Create(AOwner: TComponent;
  AAsSelection: Boolean);
begin
  inherited Create( AOwner, AAsSelection );
  DoInitialiseDataModule;
end;

{*****************************************************************************
  Overridden Constructor in which we will also call the DoInitialiseDataModule.

  @Name       TEdutecDataModule.CreateAsDetail
  @author     slesage
  @param      AOwner              The Owner of the DataModule.
  @param      AMasterDataModule   The Master DataModule for this Detail
                                  DataModule.
  @param      AMasterRecordView   The Master RecordView from which this detail
                                  DataModule is created.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TFVBFFCDataModule.CreateAsDetail(AOwner: TComponent;
  AMasterDataModule: IPPWFrameWorkDataModule;
  AMasterRecordView: IPPWFrameworkRecordView);
begin
  FAsDetail := True;
  inherited CreateAsDetail( AOwner, AMasterDataModule, AMasterRecordView );
  DoInitialiseDataModule;
end;

{*****************************************************************************
  Overridden constructor in which we will initialise some settings.

  @Name       TEdutecDataModule.CreateNew
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TFVBFFCDataModule.CreateNew(AOwner: TComponent;
  Dummy: Integer);
begin
  inherited CreateNew( AOwner, Dummy );
  FAutoOpenWhenDetail := True;
  FAutoOpenWhenSelection := True;
end;

{*****************************************************************************
  This method will be executed when the user Deletes a Record from a ListView.

  @Name       TEdutecDataModule.Delete
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCDataModule.CreateRecordViewForCurrentRecord(
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False): IPPWFrameWorkRecordView;
var
  aCursorRestorer       : IPPWRestorer;
  aRecordDataModule     : IPPWFrameWorkDataModule;
  aRecordView           : IPPWFrameWorkRecordView;
  aCurrentKeyValues     : Variant;
  aCurrentKeyExpression : String;
  Allow : Boolean;
begin
  aCursorRestorer := TPPWCursorRestorer.Create( crHourGlass );
  Allow := CanOpenNewRecordView;

  DoBeforeOpenRecordView( Self, aRecordViewMode, Allow );

  if ( Allow ) then
  begin
    if ( Assigned( PPWController ) ) then
    begin
      aRecordDataModule := PPWController.CreateDataModule( nil, Self.ClassName, False );
      aRecordDataModule.AutoOpenDataSets   := False;
      aRecordDataModule.ListViewDataModule := Self;
      aRecordDataModule.MasterDataModule := MasterDataModule;

      aCurrentKeyValues     := CreateFieldValuesVarArray( DataSet, KeyFields );

      if ( aRecordViewMode = rvmAdd ) then
      begin
        aCurrentKeyExpression := '1 <> 1';
      end
      else
      begin
        aCurrentKeyExpression := BuildFilterExpressionForDetail( Self, aRecordDataModule, KeyFields, False );
      end;

      aRecordDataModule.DoFilterRecord( KeyFields, aCurrentKeyValues, aCurrentKeyExpression );

      aRecordView := PPWController.CreateRecordViewForDataModule( aRecordDataModule, aRecordViewMode );

      Result := aRecordView;
//      if ( assigned( aRecordView ) ) then
//      begin
//        if not ( aRecordView.SettingsSaved ) then
//        begin
//          if not ( fsShowing in aRecordView.Form.FormState ) and
//             not ( fsCreatedMDIChild in aRecordView.Form.FormState ) and
//                 ( aRecordView.WindowState = wsNormal ) then
//          begin
//            aRecordView.Height := aRecordView.Height + 1;
//          end;
//        end;
//
//        if ( not aShowModal ) then
//        begin
//          aRecordView.FormStyle := PPWController.RecordViewStyle;
//        end;
//      end;
    end;
  end;
end;

procedure TFVBFFCDataModule.Delete;
begin
  ShowRecordViewForCurrentRecord( rvmDelete );
end;

{*****************************************************************************
  This method will be used to Dispatch the OnInitialiseAdditionalFields event.

  @Name       TEdutecDataModule.DoInitialiseAdditionalFields
  @author     slesage
  @param      aDataSet   The DataSet on which some Additional Fields should
                         be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.DoInitialiseAdditionalFields ( aDataSet : TDataSet );
begin
  if ( Assigned( FOnInitialiseAdditionalFields ) ) then
  begin
    FOnInitialiseAdditionalFields( aDataSet );
  end;
end;

{*****************************************************************************
  This method will be triggered to dispatch the OnInitialiseDataModule event.

  @Name       TEdutecDataModule.DoInitialiseDataModule
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.DoInitialiseDataModule;
begin
  if ( Assigned( FOnInitialise ) ) then
  begin
    FOnInitialise( Self );
  end;
end;

{*****************************************************************************
  This method will be used to Dispatch the OnInitialiseForeignKeyFields event.

  @Name       TEdutecDataModule.DoInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which some Foreign Key Fields should
                         be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.DoInitialiseForeignKeyFields ( aDataSet : TDataSet );
begin
  if ( Assigned( FOnInitialiseForeignKeyFields ) ) then
  begin
    FOnInitialiseForeignKeyFields( aDataSet );
  end;
end;

{*****************************************************************************
  This method will be used to Dispatch the OnInitialisePrimaryKeyFields event.

  @Name       TEdutecDataModule.DoInitialisePrimaryKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which some Primary Key Fields should
                         be initialised.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.DoInitialisePrimaryKeyFields ( aDataSet : TDataSet );
begin
  if ( Assigned( FOnInitialisePrimaryKeyFields ) ) then
  begin
    FOnInitialisePrimaryKeyFields( aDataSet );
  end;
end;

{*****************************************************************************
  This method will be used to dispatch some events that should be triggered
  when a new record is added to the DataSet.

  @Name       TEdutecDataModule.DoInitialiseRecord
  @author     slesage
  @param      aDataSet   The DataSet on which a new record is added.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.DoInitialiseRecord(aDataSet: TDataSet);
begin
  DoInitialisePrimaryKeyFields( aDataSet );
  DoInitialiseForeignKeyFields( aDataSet );
  DoInitialiseAdditionalFields( aDataSet );
end;

{*****************************************************************************
  This method will be executed when the user Edit a Record From a ListView.

  @Name       TEdutecDataModule.Edit
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.Edit;
begin
  ShowRecordViewForCurrentRecord( rvmEdit );
end;

function TFVBFFCDataModule.GetIsRecordViewDataModule: Boolean;
begin
  Result := FIsRecordViewDataModule;
end;

function TFVBFFCDataModule.IsListViewActionAllowed(
  aAction: TPPWFrameWorkListViewAction): Boolean;
begin
  Result := True;
end;

procedure TFVBFFCDataModule.OpenDataSets;
var
  CursorRestorer    : IPPWRestorer;
  aFrameWorkDataSet : IPPWFrameWorkDataSet;
  lcv               : Integer;
  IsVisibleDetail   : Boolean;
begin
  {$IFDEF CODESITE}
  csFVBFFCDataModule.EnterMethod( Self, 'OpenDataSets' );
  {$ENDIF}

  CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );

  if ( Not ( AsSelection ) ) {or
     ( AsSelection and AutoOpenWhenSelection )} then
  begin
    IsVisibleDetail := ( Visible ) and ( MasterDataModule <> Nil );

    { DataSets should only be openend at Run Time }
    if not ( csLoading   in ComponentState ) and
       not ( csDesigning in ComponentState ) then
    begin
      for lcv := 0 to Pred( ComponentCount ) do
      begin
        { If the DataSet implements the IPPWFrameWorkDataSet we can
          is that interface to check for the AutoOpen property, otherwise
          we should use the RTTI information }
        if ( Supports( Components[ lcv ], IPPWFrameWorkDataSet, aFrameWorkDataSet ) ) then
        begin
          if ( aFrameWorkDataSet.AutoOpen ) and ((Components[lcv] <> DataSet) or not IsRecordViewDataModule) then
          begin
            OpenDataSet( TDataSet( Components[ lcv ] ) );
          end
          else
          begin
            { With FetchOnDemand we should check if this datamodule
              is't a visible detail datamodule, in that case we should
              open the main dataset even if its AutoOpen property is
              False }
            if ( IsVisibleDetail ) and
               ( AutoOpenWhenDetail ) and
               ( Components[ lcv ] = DataSet ) and
               ( FetchOnDemand ) then
            begin
              OpenDataSet( TDataSet( Components[ lcv ] ) );
            end;
          end;
        end

        { Check if the component is a TDataSet descendant }
        else if ( Components[ lcv ] is TDataSet ) then
        begin
          { Now check if it has an AutoOpen property }
          if ( IsPublishedProp( Components[ lcv ], 'AutoOpen' ) ) then
          begin
            { call the OpenDataSet method if that property is True }
            if ( GetOrdProp( Components[ lcv ], 'AutoOpen' ) = 1 ) then
            begin
              OpenDataSet( TDataSet( Components[ lcv ] ) );
            end
            else
            begin
              { With FetchOnDemand we should check if this datamodule
                is't a visible detail datamodule, in that case we should
                open the main dataset even if its AutoOpen property is
                False }
              if ( IsVisibleDetail ) and
                 ( Components[ lcv ] = DataSet ) and
                 ( FetchOnDemand ) then
              begin
                OpenDataSet( TDataSet( Components[ lcv ] ) );
              end;
            end;
          end
          else
          begin
            { If the DataSet has no AutoOpen property only call the
              OpenDataSet method if it isn't a StoredProcedure }
            if Not ( IsPublishedProp( Components[ lcv ], 'ProcedureName' ) ) and
               Not ( IsPublishedProp( Components[ lcv ], 'StoredProcName' ) ) then
            begin
              OpenDataSet( TDataSet( Components[ lcv ] ) );
            end;
          end;
        end;
      end;
    end;
  end
  else
  begin
    if AutoOpenWhenSelection then
    begin
      OpenDataSet( Self.DataSet );
    end
  end;

  {$IFDEF CODESITE}
  csFVBFFCDataModule.ExitMethod( Self, 'OpenDataSets' );
  {$ENDIF}
end;

{*****************************************************************************
  Property Setter for the AllowEmptyFilter property.

  @Name       TEdutecDataModule.SetAllowEmptyFilter
  @author     slesage
  @param      Value   The new value for the AllowEmptyFilter property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.SetAllowEmptyFilter(const Value: Boolean);
begin
  FAllowEmptyFilter := Value;
end;

{*****************************************************************************
  This function will be used to create ( if necessary ) or show a RecordView
  for the current record and show it in the given Mode.

  @Name       TEdutecDataModule.ShowRecordViewForCurrentRecord
  @author     slesage
  @param      aRecordViewMode   The Mode in which the RecordView must be
                                shown.
  @return     Returns an interface to the RecordView.
  @Exception  None
  @See        None
******************************************************************************}
function TFVBFFCDataModule.ShowRecordViewForCurrentRecord(
  aRecordViewMode: TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False): IPPWFrameWorkRecordView;
var
  aCursorRestorer       : IPPWRestorer;
  aRecordDataModule     : IPPWFrameWorkDataModule;
  aRecordView           : IPPWFrameWorkRecordView;
  aCurrentKeyValues     : Variant;
  aCurrentKeyExpression : String;
  Allow : Boolean;
begin
  aCursorRestorer := TPPWCursorRestorer.Create( crHourGlass );
  Allow := CanOpenNewRecordView;

  DoBeforeOpenRecordView( Self, aRecordViewMode, Allow );

  if ( Allow ) then
  begin
    if ( Assigned( PPWController ) ) then
    begin
      aRecordDataModule := PPWController.CreateDataModule( nil, Self.ClassName, False );
      aRecordDataModule.AutoOpenDataSets   := False;
      aRecordDataModule.ListViewDataModule := Self;
      aRecordDataModule.MasterDataModule := MasterDataModule;

      aCurrentKeyValues     := CreateFieldValuesVarArray( DataSet, KeyFields );

      if ( aRecordViewMode = rvmAdd ) then
      begin
        aCurrentKeyExpression := '1 <> 1';
      end
      else
      begin
        aCurrentKeyExpression := BuildFilterExpressionForDetail( Self, aRecordDataModule, KeyFields, False );
      end;

      aRecordDataModule.DoFilterRecord( KeyFields, aCurrentKeyValues, aCurrentKeyExpression );

      aRecordView := PPWController.CreateRecordViewForDataModule( aRecordDataModule, aRecordViewMode );

      if ( assigned( aRecordView ) ) then
      begin
        if not ( aRecordView.SettingsSaved ) then
        begin
          if not ( fsShowing in aRecordView.Form.FormState ) and
             not ( fsCreatedMDIChild in aRecordView.Form.FormState ) and
                 ( aRecordView.WindowState = wsNormal ) then
          begin
            aRecordView.Height := aRecordView.Height + 1;
          end;
        end;

        if ( not aShowModal ) then
        begin
          aRecordView.FormStyle   := PPWController.RecordViewStyle;
          aRecordView.WindowState := wsMaximized;
        end
        else
        begin
          aRecordView.ShowModal;
        end;
      end;
    end;
  end;
end;

procedure TFVBFFCDataModule.SetIsRecordViewDataModule(
  const Value: Boolean);
begin
  FIsRecordViewDataModule := Value;
end;

{*****************************************************************************
  This method will be executed when the user View a Record From a ListView.

  @Name       TEdutecDataModule.View
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCDataModule.View;
begin
  ShowRecordViewForCurrentRecord( rvmView );
end;

end.
