{*****************************************************************************
  This unit will contain all interfaces used in the Edutec Client Application,
  Application Server and Components.
  
  @Name       Unit_FVBFFCInterfaces
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  12/07/2006   tclaesen             Added TLoopThroughCallBackWithParams.
  16/11/2005   wlambrec             Added TLoopThroughCallBack and IFVBMultiSelectDeleteListView
                                    to allow for multiselect possibilities in the
                                    grids of the listview
  27/07/2005   sLesage              Added interfaces for the Person Edutec
                                    DataModule.
  27/07/2005   sLesage              Added interfaces for the CenInfrastructure,
                                    Organisation and Organisation Type Edutec
                                    DataModules.
  13/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCInterfaces;

interface

uses
  Unit_PPWFrameWorkInterfaces, Unit_PPWFrameWorkClasses, cxGrid,
  Unit_PPWFrameWorkActions, cxGridCustomView, Controls, DB, DBClient, classes, Contnrs;

Type
  TInitialiseFormEvent = procedure ( aFrameWorkDataModule : IPPWFrameWorkDataModule ) of Object;
  TLoopThroughCallBack = procedure of object;
  TLoopThroughCallBackWithParams = procedure( arrObj: array of TObject ) of object;

  IFVBFFCDataModule = Interface( IPPWFrameWorkDataModule )
  ['{939A2457-10D6-4AF1-B030-21F8D39A5799}']
    function CreateRecordViewForCurrentRecord( aRecordViewMode : TPPWFrameWorkRecordViewMode; aShowModal : Boolean = False ) : IPPWFrameWorkRecordView;
    function IsListViewActionAllowed         ( aAction         : TPPWFrameWorkListViewAction ) : Boolean;
    function GetIsRecordViewDataModule: Boolean;
    procedure SetIsRecordViewDataModule(const Value: Boolean);
    property IsRecordViewDataModule: Boolean read GetIsRecordViewDataModule write SetIsRecordViewDataModule;
  end;

  IFVBFFCBaseDataModule = Interface( IFVBFFCDataModule )
  ['{3E03F104-6447-4F48-B974-2CDEA6772BE9}']
    function GetSearchCriteriaDataSet : TClientDataSet;

    procedure OpenSearchCriteriaDataSet;

    property SearchCriteriaDataSet : TClientDataSet read GetSearchCriteriaDataSet;
  end;

  IFVBFFCListView   = Interface( IPPWFrameWorkListView )
  ['{31808ED0-81BA-4F61-B35A-B3256C0CF0BD}']
    function GetActiveGridView : TcxCustomGridView;
    function GetFilterString   : String;
    function GetOrderByString  : String;
    function GetGrid           : TcxGrid;

    procedure ClearFilterCriteria;

    procedure StoreActiveViewToIni;
    procedure RestoreActiveViewFromIni;

    property ActiveGridView : TcxCustomGridView read GetActiveGridView;
    property FilterString   : String            read GetFilterString;
    property OrderByString  : String            read GetOrderByString;
    property Grid           : TcxGrid           read GetGrid;

    function ExportGridContents(aFileExt, aFilter, aFileName: String;
      aMethod: TPPWFrameWorkSaveGridMethod): boolean;

  end;

  IFVBFFCRecordView = Interface( IPPWFrameWorkRecordView )
  ['{2A8F79CE-1344-45FD-901D-88976B6B0C96}']
    function GetActiveDetailForm           : IPPWFrameWorkForm;

    procedure ActivateControl( aControl : TWinControl );
    procedure FocusActiveDetailForm;
    procedure HideHeaderAndButtonPanel;

    property ActiveDetailForm        : IPPWFrameWorkForm             read GetActiveDetailForm;
  end;

  IFVBFFCForm       = Interface( IPPWFrameWorkForm )
  ['{022076D7-2FD7-4F2A-842C-D7EE42041145}']
    procedure Close;
    procedure DoInitialiseForm( aFrameWorkDataModule : IPPWFrameWorkDataModule );
    procedure CreateFormButtonOnToolbar;
    procedure DestroyFormButtonOnToolbar;
    procedure AddOrUpdateWindowMenuItem;
    procedure RemoveWindowMenuItem;
  end;

  IFVBFFCDataSetForFilterAndSort = interface
  ['{E6C2ADC7-72D4-4553-895E-AD3DBD29F83D}']
    procedure ApplyDetailFilter( const aFilter : String );
    procedure ApplyFilter      ( const aFilter : String );
    procedure ApplyOrderBy     ( const aOrderBy: String );
    procedure SetSQLStatement  ( const aSQL    : String );
    //procedure ApplyFilterAndSort(const aSelectClause, aFilterClause, aSortClause: String);
  end;

  IFVBMultiSelectDeleteListView = interface (IInterface)
    ['{DFAF09CA-AE66-45D6-BB08-DE68814E7143}']
    procedure LoopThroughSelectedRecords (callBack: TLoopThroughCallBack);
  end;

implementation

end.
