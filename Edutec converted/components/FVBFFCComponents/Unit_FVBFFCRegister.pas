{*****************************************************************************
  This unit contains all the Registration Routines needed for the Edutec
  Project.

  @Name       Unit_FVBFFCRegister
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   Wim Lambrechts       Added registration of TFVBFFCCommand
  13/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCRegister;

interface

procedure Register;

implementation

uses
  Classes, DesignIntf, Graphics, Controls, ExtCtrls, DesignEditors,
  Unit_PPWFrameWorkCustomModules,
  Unit_FVBFFCDevExpress, Unit_FVBFFCFoldablePanel, Unit_FVBFFCComponents,
  Unit_FVBFFCDBComponents, Unit_FVBFFCDataModule, Unit_FVBFFCListView,
  Unit_FVBFFCRecordView, Unit_FVBFFCRecordViewModule;
  
const
  cDataAccess = 'FVBFFC Data';  {Components for database}
  cStandard   = 'FVBFFC Std';
  cDevExpress = 'FVBFFC Dvx';   {Grid & menu bars}
  cEditors    = 'FVBFFC Edit';
  cDBEditors  = 'FVBFFC DBEdit';

procedure Register;
begin
  { Register the EDU Delphi Components }
  RegisterComponents( cStandard, [ TFVBFFCActionList,
                                   TFVBFFCImageList,
                                   TFVBFFCPanel,
                                   TFVBFFCFoldablePanel,
                                   TFVBFFCCommandLine ] );

  { Register the EDU Data Components }
  RegisterComponents( cDataAccess, [ {TFVBFFCIBDataBase,
                                     TFVBFFCIBTransaction,
                                     TFVBFFCIBUpdateSQL,
                                     TFVBFFCIBStoredProc,
                                     TFVBFFCIBQuery,
                                     TFVBFFCIBTable,
                                     TFVBFFCClientDataSet,}
                                     TFVBFFCQuery,
                                     TFVBFFCStoredProc,
                                     TFVBFFCConnection,
                                     TFVBFFCClientDataSet,
                                     TFVBFFCDataSetProvider,
                                     TFVBFFCDataSource,
                                     TFVBFFCSocketConnection,
                                     TFVBFFCSharedConnection,
                                     TFVBFFCCommand ] );

  { Register the EDU DevExpress Components }
  RegisterComponents( cDevExpress, [ TFVBFFCNavBar,
                                     TFVBFFCGrid,
                                     TFVBFFCGridPopupMenu,
                                     TFVBFFCStyleRepository,
                                     TFVBFFCLookAndFeelController,
                                     TFVBFFCComponentPrinter,
                                     TFVBFFCGrid,
                                     TFVBFFCGridPopupMenu,
                                     TFVBFFCPageControl,
                                     TFVBFFCStyleRepository,
                                     TFVBFFCLookAndFeelController,
                                     TFVBFFCComponentPrinter,
                                     TFVBFFCHintStyleController,
                                     TFVBFFCDefaultEditStyleController,
                                     TFVBFFCEditStyleController,
                                     TFVBFFCSplitter,
                                     TFVBFFCButton,
                                     TFVBFFCGroupBox,
                                     TFVBFFCStatusBar,
                                     TFVBFFCShellListView,
                                     TFVBFFCSideBar,
                                     //TFVBFFCSideBarStore,
                                     TFVBFFCEditRepository ] );

  { Register the EDU DevExpress Editors }
  RegisterComponents( cEditors, [ TFVBFFCTextEdit,
                                  TFVBFFCMaskEdit,
                                  TFVBFFCMemo,
                                  TFVBFFCDateEdit,
                                  TFVBFFCButtonEdit,
                                  TFVBFFCCheckBox,
                                  TFVBFFCComboBox,
                                  TFVBFFCImageComboBox,
                                  TFVBFFCSpinEdit,
                                  TFVBFFCCalcEdit,
                                  TFVBFFCHyperLinkEdit,
                                  TFVBFFCTimeEdit,
                                  TFVBFFCCurrencyEdit,
                                  TFVBFFCImage,
                                  TFVBFFCBlobEdit,
                                  TFVBFFCLookupComboBox,
                                  TFVBFFCRadioButton,
                                  TFVBFFCRadioGroup,
                                  TFVBFFCLabel,
                                  TFVBFFCProgressBar,
                                  TFVBFFCRichEdit,
                                  TFVBFFCCheckGroup
                                   ] );

  { Register the EDU DevExpress DB Editors }
  RegisterComponents( cDBEditors, [ TFVBFFCDBTextEdit,
                                    TFVBFFCDBMaskEdit,
                                    TFVBFFCDBMemo,
                                    TFVBFFCDBDateEdit,
                                    TFVBFFCDBButtonEdit,
                                    TFVBFFCDBCheckBox,
                                    TFVBFFCDBComboBox,
                                    TFVBFFCDBImageComboBox,
                                    TFVBFFCDBSpinEdit,
                                    TFVBFFCDBCalcEdit,
                                    TFVBFFCDBHyperLinkEdit,
                                    TFVBFFCDBTimeEdit,
                                    TFVBFFCDBCurrencyEdit,
                                    TFVBFFCDBImage,
                                    TFVBFFCDBBlobEdit,
                                    TFVBFFCDBLookupComboBox,
                                    TFVBFFCDBRadioGroup,
                                    TFVBFFCDBLabel,
                                    TFVBFFCDBProgressBar,
                                    TFVBFFCDBRichEdit,
                                    TFVBFFCDBCheckGroup ] );

  RegisterPropertyEditor( TypeInfo( TFont ) , TFVBFFCPanel, 'Font' , Nil );
  RegisterPropertyEditor( TypeInfo( TColor ), TFVBFFCPanel, 'Color', Nil );
  RegisterPropertyEditor( TypeInfo( TPanelBevel ), TFVBFFCPanel, 'BevelOuter', Nil );
  RegisterPropertyEditor( TypeInfo( TPanelBevel ), TFVBFFCPanel, 'BevelInner', Nil );
  RegisterPropertyEditor( TypeInfo( Integer ), TFVBFFCPanel, 'BevelWidth', Nil );
  RegisterPropertyEditor( TypeInfo( TFont ) , TFVBFFCFoldablePanel, 'Font' , Nil );
  RegisterPropertyEditor( TypeInfo( TColor ), TFVBFFCFoldablePanel, 'Color', Nil );

//  RegisterClass( TFVBFFCtecDataModule );
  RegisterCustomModule( TFVBFFCDataModule, TPPWFrameWorkDataModuleModule );
  RegisterCustomModule( TFVBFFCRecordView, TFVBFFCRecordViewModule );
  RegisterCustomModule( TFVBFFCListView, TCustomModule );
end;

end.
