unit Data_FVBFFCBaseDataModule;

interface

uses
  SysUtils, Classes, Provider, Contnrs, Unit_FVBFFCDBComponents,
  Unit_PPWFrameWorkComponents, Unit_PPWFrameWorkInterfaces,
  unit_PPWFrameWorkADO, Unit_FVBFFCDataModule, DBClient, DB, ADODB,
  Unit_FVBFFCInterfaces, data_FVBFFCMainClient;

type
  TFVBFFCBaseDataModule = class(TFVBFFCDataModule, IFVBFFCBaseDataModule )
    qryList: TFVBFFCQuery;
    qryRecord: TFVBFFCQuery;
    prvList: TFVBFFCDataSetProvider;
    prvRecord: TFVBFFCDataSetProvider;
    cdsList: TFVBFFCClientDataSet;
    cdsRecord: TFVBFFCClientDataSet;
    cdsSearchCriteria: TFVBFFCClientDataSet;
    procedure FVBFFCDataModuleLinkListView(Sender: TObject;
      var aListView: IPPWFrameWorkListView);
    procedure cdsListAfterDelete(DataSet: TDataSet);
    procedure cdsListAfterPost(DataSet: TDataSet);
    procedure cdsListBeforeDelete(DataSet: TDataSet);
    procedure cdsListNewRecord(DataSet: TDataSet);
    procedure cdsListReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure FVBFFCDataModuleInitialiseForeignKeyFields(
      aDataSet: TDataSet);
    procedure FVBFFCDataModuleLinkRecordView(Sender: TObject;
      var aRecordView: IPPWFrameWorkRecordView);
    procedure FVBFFCDataModuleInitialise(Sender: TObject);
    procedure FVBFFCDataModuleCancelFilter(Sender: TObject);
    procedure FVBFFCDataModuleFilter(Sender: TObject;
      var FilterApplied: Boolean);
    procedure FVBFFCDataModuleFilterDetail(Sender: TObject; Fields: String;
      Values: Variant; Expression: String);
    procedure FVBFFCDataModuleFilterRecord(Sender: TObject; Fields: String;
      Values: Variant; Expression: String);
  private
    { Private declarations }
    FDeletedKey : Variant;
    FWasAnInsert: Boolean;
  protected
    function GetAppServer: Variant; virtual; abstract;
    function GetSearchCriteriaDataSet : TClientDataSet; virtual;
    function GetMainDataModule: TdtmFVBFFCMainClient; virtual; abstract;

    procedure CopyFieldValues (Source: TClientDataset; Destination: TClientDataset); virtual;
    procedure DeleteMainDataset(KeyValues: Variant; Source: TDataset); virtual;
    procedure DoApplyUpdates( aDataSet : TDataSet ); virtual;
    procedure OpenSearchCriteriaDataSet; virtual;
    procedure UpdateMainDataset ( aDataSet : TDataset); virtual;

    property MainDataModule : TdtmFVBFFCMainClient read GetMainDataModule;
    procedure LinkComponentsToActions; virtual;
  public
    constructor Create( AOwner : TComponent; AAsSelection : Boolean = False ); override;
  end;

var
  FVBFFCBaseDataModule: TFVBFFCBaseDataModule;

implementation

uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite,
  {$ENDIF}
  Unit_PPWFrameWorkClasses,
  Controls, Unit_PPWFrameWorkDataModule,
  Form_FVBFFCReconcileError;

{$R *.dfm}

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleLinkListView(Sender: TObject;
  var aListView: IPPWFrameWorkListView);
var
  aEDUForm : IFVBFFCForm;
begin
  if ( Supports( aListView, IFVBFFCForm, aEDUForm ) ) then
  begin
    aEDUForm.DoInitialiseForm( Self );
  end;
end;

{*****************************************************************************
  This method will be executed when the RecordDataSet needs to be filtered.

  @Name       TFVBFFCDataModule.EdutecDataModuleFilterRecord
  @author     slesage
  @param      Sender       The Object from which the Method is invoked.
  @param      Fields       Comma sepperated list of fields which form the
                           PrimaryKey
  @param      Values       Variant Array which contains the values of all
                           PrimaryKey fields.
  @param      Expression   The Expression that can be added to the WhereClause
                           of the SQL Statement so only one record will be
                           fetched.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleFilterRecord(
  Sender: TObject; Fields: String; Values: Variant; Expression: String);
begin
  // Implementation should be done on Project Specific DataModules
end;

{*****************************************************************************
  This method will be executed when the List DataSet needs to be filtered as
  a Detail in a Master / Detail relation.

  @Name       TFVBFFCDataModule.EdutecDataModuleFilterDetail
  @author     slesage
  @param      Sender       The Object from which the Method is invoked.
  @param      Fields       Comma sepperated list of fields which form the
                           PrimaryKey
  @param      Values       Variant Array which contains the values of all
                           PrimaryKey fields.
  @param      Expression   The Expression that can be added to the WhereClause
                           of the SQL Statement so only one record will be
                           fetched.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleFilterDetail(
  Sender: TObject; Fields: String; Values: Variant; Expression: String);
begin
  // Implementation should be done on Project Specific DataModules
end;

{*****************************************************************************
  This method will be executed when the user applies a filter to the List
  DataSet.  In here we will call the FilterString property on the ListView
  Interface, which will execute the necessary code to build the WHERE
  clasue.


  @Name       TFVBFFCDataModule.EdutecDataModuleFilter
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleFilter(Sender: TObject;
  var FilterApplied: Boolean);
begin
  // Implementation should be done on Project Specific DataModules
end;

procedure TFVBFFCBaseDataModule.cdsListAfterDelete(DataSet: TDataSet);
begin
  if ( DataSet is TClientDataSet ) then
  begin
    { Apply the Updates through the Provider }
    DoApplyUpdates( DataSet );

    { Check if the changes were correctly applied to the DataBase, if the
      ChangeCount <> 0 then we still have pending changes in the ClientDataSet
      which means an error occured }
    if ( TClientDataSet( DataSet ).ChangeCount <> 0 ) then
    begin
      { If it was the RecordDataSet for which we wanted to apply the updates,
        try to refresh the record  }
      if ( DataSet = RecordDataset ) then
      begin
        DataSet.Close;
        DataSet.Open;
      end;
      Abort;
    end
    else
    { ChangeCount was = 0, so no problems occured during the ApplyUpdates.
      We should now also remove the record from the ListDataSet }
    begin
      DeleteMainDataset( FDeletedKey, DataSet);
    end;
  end;
end;

procedure TFVBFFCBaseDataModule.cdsListAfterPost(DataSet: TDataSet);
begin
  if ( DataSet is TClientDataSet ) then
  begin
    { Apply the Updates through the Provider }
    DoApplyUpdates( DataSet );

    { Check if the changes were correctly applied to the DataBase, if the
      ChangeCount <> 0 then we still have pending changes in the ClientDataSet
      which means an error occured }
    if ( TClientDataSet( DataSet ).ChangeCount = 0 ) then
    begin
      { If it was the RecordDataSet for which we wanted to apply the updates,
        try to refresh the record so we have all new field values in our
        RecordDataSet, this will include auto generated PK values,
        Calculated Fields from a view, and fields that might be filled in
        by a trigger }
      if ( DataSet = RecordDataset ) then
      begin
        RefreshRecordDataSet;
      end;

      { Now that our RecordDataSet contains the latest changes, we can make
        sure those changes are also sent back to the List DataSet }
      UpdateMainDataset (Dataset);
    end
    { ChangeCount was <> 0, so something went wrong in the ApplyUpdates, make
      sure the DataSet returns in Edit mode in that case, so the Apply Button
      on the DetailForm can still be used }
    else
    begin
      DataSet.Edit;
      Abort;
    end;
  end;

  FWasAnInsert := False;
end;

procedure TFVBFFCBaseDataModule.cdsListBeforeDelete(DataSet: TDataSet);
begin
  FDeletedKey := CreateFieldValuesVarArray( DataSet, KeyFields )
end;

procedure TFVBFFCBaseDataModule.cdsListNewRecord(DataSet: TDataSet);
begin
  DoInitialiseRecord( DataSet );
end;

{*****************************************************************************
  This method is executed when a Reconcile Error is generated.

  @Name       TFVBFFCDataModule.cdsListReconcileError
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.cdsListReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
  ErrorForm: TfrmFVBFFCReconcileError;
begin
  ErrorForm := TfrmFVBFFCReconcileError.Create(nil);
  try
    Action := ErrorForm.HandleReconcileError(DataSet, UpdateKind, E, getAppServer);
  finally
    ErrorForm.Free;
  end;
end;

{*****************************************************************************
  This method will be used to Apply the Updates to the DataBase after a Post
  or Delete.

  @Name       TFVBFFCDataModule.DoApplyUpdates
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.DoApplyUpdates(aDataSet: TDataSet);
begin
  try
    if ( aDataset is TClientDataset ) and
       ( aDataset.Tag = 1 ) then
    begin
      if ( TClientDataset( aDataset).ApplyUpdates( 0 ) = 0 ) then
      begin
        TClientDataSet( aDataset ).MergeChangeLog; //to avoid following problem:
        //delete a product from a contract, press apply, add a new product, press apply.
        //in the second Apply the applictaion would first launch a delete statement
      end;
    end;
  finally
    aDataset.Tag := 0;
  end;
end;

{*****************************************************************************
  When a record was successfully deleted in the recordview the record should
  be found in the listview and deleted.

  @Name       TFVBFFCDataModule.DeleteMainDataset
  @author     slesage
  @param      KeyValues   A VarArray of KeyValues of the Record that must
                          be deleted.
  @param      Source      The DataSet in which the Record was Deleted.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.DeleteMainDataset(KeyValues: Variant;
  Source: TDataset);
var
  CurrentKey : Variant;
begin
  if ( Source = RecordDataSet ) and
     ( ListViewDatamodule <> nil )  and
     ( ListViewDataModule.ListView <> nil ) then
  begin
    ListViewDataModule.Dataset.LogChanges := false;
    try
      CurrentKey := CreateFieldValuesVarArray( ListViewDataModule.Dataset, KeyFields );
      if ( ListViewDataModule.Dataset.Locate( KeyFields, KeyValues, [] ) ) then
      begin
        ListViewDataModule.Dataset.delete;
      end;
    finally
      ListViewDataModule.Dataset.LogChanges := true;
      ListViewDataModule.Dataset.Locate(KeyFields, CurrentKey, []);
    end;
  end; 
end;

{*****************************************************************************
  When a record was successfully added or updated in the recordview try and
  find the record in the listview and update or insert it.
  When the record was not found in the listview (and thus inserted, the listview
  should be refreshed.

  @Name       TFVBFFCDataModule.UpdateMainDataset
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.UpdateMainDataset(aDataSet: TDataset);
var
  RecFound       : Boolean;
  CurrentKey     : Variant;
  RecordKey      : Variant;
  CursorRestorer : IPPWRestorer;
begin
  RecFound := true;
  if (aDataSet = cdsRecord) and (ListViewDatamodule <> nil) then
  //No updates on cdsMain to be done when the cdsMain was edited
  begin
    CursorRestorer := TPPWCursorRestorer.Create( crSQLWait );
    ListViewDataModule.Dataset.LogChanges := false; //changes not to be stored in the delta of cdsMain
    try
      begin
        CurrentKey := CreateFieldValuesVarArray( ListViewDataModule.Dataset, KeyFields );
        RecordKey  := CreateFieldValuesVarArray( cdsRecord, KeyFields );
        RecFound := ListViewDataModule.Dataset.Locate(KeyFields, RecordKey, []);
      end;

      { The record wasn't found in the List, so we should add it }
      if Not RecFound then
      begin
        if ( FWasAnInsert ) then
        begin
          ListViewDataModule.Dataset.Append;
          CopyFieldValues( cdsRecord, ListViewDataModule.Dataset);

          try
            ListViewDataModule.Dataset.post;
          except
          // post might generate "Trying to modify a readonly field" and this
          // keeps the listview in editing mode
            on e:Exception do
            begin
              ListViewDataModule.Dataset.Cancel;
              raise Exception.CreateFmt ('Internal error (%s): please refresh listview', [e.message]);
            end;
          end;
        end
        else
        begin
          ListViewDataModule.RefreshListDataSet;
        end;
      end
      else
      begin
        ListViewDataModule.Dataset.Edit;
        CopyFieldValues (cdsRecord, ListViewDataModule.Dataset);

      try
        ListViewDataModule.Dataset.post;
        except
        // post might generate "Trying to modify a readonly field" and this
        //keeps the listview in editing mode
        on e:Exception do
        begin
          ListViewDataModule.Dataset.Cancel;
          raise Exception.CreateFmt ('Internal error (%s): please refresh listview', [e.message]);
        end;
      end;
      end;

    finally
      ListViewDataModule.Dataset.LogChanges := true;
      if RecFound then
      begin
        ListViewDataModule.Dataset.Locate(KeyFields, CurrentKey, []);
      end;
      CursorRestorer := Nil;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to initialise the ForeignKey Fields when a new
  record is added to the DataSet.

  @Name       TFVBFFCDataModule.EdutecDataModuleInitialiseForeignKeyFields
  @author     slesage
  @param      aDataSet   The DataSet on which you want to initialise the Fields.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleInitialiseForeignKeyFields(
  aDataSet: TDataSet);
var
  aDetail     : TPPWFrameWorkDataModuleDetail;
  aFieldList  : TList;
  lcv         : Integer;
begin
  { If the DataModule is shown as a detail of another datamodule, we should
    initialise the ForeignKey fields }
  if ( Assigned( MasterDataModule ) ) then
  begin
    { Fetch the Detail from the MasterDatamodule, so we can access it's
      ForeignKeys property }
    aDetail := MasterDataModule.DetailDataModules.ItemByDetailClassName( Self.ClassName );

    if ( Assigned( aDetail ) ) then
    begin
      { Now initialise the ForeingKeys field in our dataset with the value
        found in the ForeignKeys field in the Master DataSet }

      { Get all the Fields specified in the ForeignKeys into aFieldList }
      aFieldList := TList.Create;
      try
        aDataSet.GetFieldList( aFieldList, aDetail.ForeignKeys );

        { Loop over each field and initialise it's value }
        for lcv := 0 to Pred( aFieldList.Count ) do
        begin
          TField( aFieldList[ lcv ] ).ReadOnly := False;
          TField( aFieldList[ lcv ] ).Value :=
            MasterDataModule.RecordDataset.FieldByName( TField( aFieldList[ lcv ] ).FieldName ).Value;
          TField( aFieldList[ lcv ] ).ReadOnly := True;
        end;
      finally
        FreeAndNil( aFieldList );
      end;
    end;
  end;
end;

{*****************************************************************************
  This method will be used to copy all field values from one dataset to
  another dataset.

  @Name       TFVBFFCDataModule.CopyFieldValues
  @author     slesage
  @param      Source        The dataset from which the Field Values must be
                            copied.
  @param      Destination   The dataset to which the Field Values must be
                            copied.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.CopyFieldValues(Source,
  Destination: TClientDataset);
var
  lcv: Integer;
  SourceField, DestinationField : TField;
begin
  for lcv := 0 to Pred( Source.FieldCount ) do
  begin
    SourceField := Source.Fields[ lcv ];

    { We only want to copy the DataFields which aren't DataSet Fields }
    if     ( SourceField.FieldKind = fkData ) and
       not ( SourceField is TDataSetField ) then
    begin
      DestinationField := Destination.FindField( SourceField.FieldName );

      { Only copy the value if the Source Field is found in the Destination
        DataSet }
      if ( Assigned( DestinationField ) ) then
      begin
        //11/09/2003   mBellekens
        //LargeIntfields cannot be accessed as variants, we have to check first
        if DestinationField is TLargeintField then
        begin
          TLargeintField( DestinationField).AsLargeInt := TLargeintField( SourceField).AsLargeInt;
        end
        else
        begin
          DestinationField.Value := SourceField.Value;
        end;
      end;
    end;
  end;
end;

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleLinkRecordView(Sender: TObject;
  var aRecordView: IPPWFrameWorkRecordView);
var
  aEDUForm       : IFVBFFCForm;
  aEDURecordView : IPPWFrameWorkControlledLayout;
begin
  if ( Supports( aRecordView, IFVBFFCForm, aEDUForm ) ) then
  begin
    aEDUForm.DoInitialiseForm( Self );
  end;
  if ( Supports( aRecordView, IPPWFrameWorkControlledLayout, aEDURecordView ) ) then
  begin
    aEDURecordView.DoUpdateControls;
  end;
end;

function TFVBFFCBaseDataModule.GetSearchCriteriaDataSet: TClientDataSet;
begin
  Result := cdsSearchCriteria;
end;

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleInitialise(Sender: TObject);
begin
  OpenSearchCriteriaDataSet;
end;

procedure TFVBFFCBaseDataModule.FVBFFCDataModuleCancelFilter(Sender: TObject);
begin
  if ( Assigned( ListViewDataModule.ListView ) ) then
  begin
    ListViewDataModule.ListView.CancelFilter;
  end;
end;

{*****************************************************************************
  This method will be used to open the SearchCriteria DataSet.

  @Name       TFVBFFCDataModule.OpenSearchCriteriaDataSet
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCBaseDataModule.OpenSearchCriteriaDataSet;
begin
  if not ( cdsSearchCriteria.Active ) then
  begin
    if ( cdsSearchCriteria.FieldCount <> 0 ) then
    begin
      cdsSearchCriteria.CreateDataSet;
      cdsSearchCriteria.Append;
    end;
  end;
end;

procedure TFVBFFCBaseDataModule.LinkComponentsToActions;
begin
  qryList.Connection := MainDataModule.adocnnMain;
  qryRecord.Connection := MainDataModule.adocnnMain;
end;

{*****************************************************************************
  @Name       TFVBFFCBaseDataModule.Create
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
constructor TFVBFFCBaseDataModule.Create( AOwner : TComponent; AAsSelection : Boolean = False );
begin
  inherited Create (AOwner, AAsSelection);
  LinkComponentsToActions;
end;

end.
