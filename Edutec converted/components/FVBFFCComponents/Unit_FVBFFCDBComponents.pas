{*****************************************************************************
  This unit contains some subclasses of general DB Related Delphi Components.

  @Name       Unit_FVBFFCDBComponents
  @Author     slesage
  @Copyright  (c) 2005 PeopleWare
  @History

  Date         By                   Description
  ----         --                   -----------
  14/07/2005   Wim Lambrechts       Added TFVBFFCCommand
  15/06/2005   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCDBComponents;

interface

uses
  DB, MConnect, SConnect, ADODb, Provider, Classes, Unit_PPWFrameWorkComponents,
  Unit_PPWFrameWorkInterfaces, Unit_FVBFFCInterfaces, Unit_PPWFrameWorkADO;

type
  TFVBFFCDataSource = Class( TDataSource );

  TFVBFFCCommand = class( TADOCommand );

  TFVBFFCDataSetProvider = class( TDataSetProvider )
  public
    constructor Create( AOwner : TComponent ); override;
  published
    property UpdateMode default upWhereKeyOnly;
  end;

  TFVBFFCSocketConnection = class(TSocketConnection)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
    Property Connected Stored False;
  end;

  //TFVBFFCSharedConnection = class (TSharedConnection);

  TFVBFFCSharedConnection = Class( TDispatchConnection )
  Private
    FDummy: String;
    FParentConnection: TDispatchConnection;
    FObjectName: String;
    FAfterConnect: TNotifyEvent;
    FAfterDisconnect: TNotifyEvent;
    FBeforeConnect: TNotifyEvent;
    FBeforeDisconnect: TNotifyEvent;
    FReallyConnected, FInternalConnected: Boolean;
  Protected
    Procedure DoConnect; Override;
    Procedure SetConnected( Value: Boolean ); Override;
    Function GetAppServer: Variant; Override;
    Procedure Notification( AComponent: TComponent; Operation: TOperation ); Override;
    Function GetConnected: Boolean; Override;
  Published
    Procedure CloseAllDataSets;

    Property ComputerName: String Read FDummy; // "Hide" these properties from the OI
    Property ServerGUID: String Read FDummy;
    Property ServerName: String Read FDummy;
    Property ParentConnection: TDispatchConnection Read FParentConnection Write FParentConnection;
    Property ChildName: String Read FObjectName Write FObjectName;

    Property AfterConnect: TNotifyEvent Read FAfterConnect Write FAfterConnect;
    Property AfterDisconnect: TNotifyEvent Read FAfterDisconnect Write FAfterDisconnect;
    Property BeforeConnect: TNotifyEvent Read FBeforeConnect Write FBeforeConnect;
    Property BeforeDisconnect: TNotifyEvent Read FBeforeDisconnect Write FBeforeDisconnect;
  Public
    Constructor Create( AOwner: TComponent ); Override;
  Published
    Property Connected Stored False;
  End;


  TFVBFFCStoredProc = class( TADOStoredProc, IPPWFrameWorkDataSet )
  private
    FAutoOpen           : Boolean;
  protected
    { Property Getters }
    function GetAutoOpen           : Boolean; virtual;

    { Property Setters }
    procedure SetAutoOpen           ( const Value: Boolean ); virtual;
  published
    property Active stored  False;
    property AutoOpen           : Boolean  read  GetAutoOpen
                                           write SetAutoOpen default True;
  end;

  TFVBFFCQuery      = class( TADOQuery, IPPWFrameWorkDataSet, IFVBFFCDataSetForFilterAndSort )
  private
    FAutoOpen           : Boolean;
    FDesignTimeSQL      : TStringList;
    FDetailFilter       : String;
  protected
    { Property Getters }
    function GetAutoOpen           : Boolean; virtual;

    { Property Setters }
    procedure SetAutoOpen           ( const Value: Boolean ); virtual;

    procedure Loaded; override;

    procedure OpenCursor(InfoQuery: Boolean = False); override;

    {$IFDEF CODESITE}
    procedure SendSQLToCodeSite;    virtual;
    procedure SendParamsToCodeSite; virtual;
    {$ENDIF}
  public
    constructor Create( AOwner : TComponent ); override;
    destructor Destroy; override;

    procedure AddWhereClause( aWhereClause : String ); virtual;

    procedure ApplyDetailFilter( const aFilter : String ); virtual;
    procedure ApplyFilter      ( const aFilter : String ); virtual;
    procedure ApplyOrderBy     ( const aOrderBy: String ); virtual;
    procedure SetSQLStatement  ( const aSQL    : String ); virtual;
  published
    //property Connection stored true;
    property Active stored  False;
    property AutoOpen           : Boolean  read  GetAutoOpen
                                           write SetAutoOpen default True;
  end;

  TFVBFFCConnection = class( TPPWFrameWorkADOConnection );

  TFVBFFCClientDataSet = class( TPPWFrameWorkClientDataSet )
  private
    FClearEmptyStrings: Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Post; override;
  published
    property ClearEmptyStrings: Boolean read FClearEmptyStrings write FClearEmptyStrings default False;
  end;

  function Q_PosText(const FindString, SourceString: string; StartPos: Integer): Integer;
  procedure SplitSQL(aSQL : TStrings; var aSelect, aWhere, aGroupBy, aOrderBy, aHaving : String);
  function CombineSQL(aSelect, aWhere, aGroupBy, aOrderBy, aHaving : String) : String;

Implementation

Uses
  {$IFDEF CODESITE}
  Unit_FVBFFCCodeSite, 
  {$ENDIF}
  ActiveX, ComObj, SysUtils;

type
  TSQLKind = (sqlWhere, sqlGroupBy, sqlOrderBy, sqlHaving, sqlIn);

const
  cSQL : array[TSQLKind] of string = ('WHERE', 'GROUP BY', 'ORDER BY', 'HAVING', 'IN');


{*****************************************************************************
  Name           : IsWholeWord
  Author         : amuylaer
  Arguments      : const aWord, aSourceString : string; aStartPos : Integer
  Return Values  : boolean
  Exceptions     : None
  Description    : Checks if a word inside a string is a whole word or part of another.
  History        :

  Date         By                   Description
  ----         --                   -----------
  21/10/2004    amuylaer               Initial creation of the Procedure/function.
*****************************************************************************}

function  IsWholeWord(const aSource : string; aStartPos, aLength : Integer) : boolean;
var
  aEndPos : Integer;
begin
  Result := False;
  if (aStartPos > 1) and not (aSource[aStartPos - 1] in [#0..#32, ')']) then exit;
  aEndPos := aStartPos + aLength;
  if (aEndPos < Length(aSource)) and not (aSource[aEndPos] in [#0..#32, '(', ',']) then exit;
  Result := True;
end;

{*****************************************************************************
  Name           : FindNextWholeWord
  Author         : amuylaer
  Arguments      : const aSource, aWord : string; aStartPos : Integer
  Return Values  : Integer
  Exceptions     : None
  Description    : Scans the string for the first WHOLE occurance of a word
  History        :

  Date         By                   Description
  ----         --                   -----------
  21/10/2004    amuylaer               Initial creation of the Procedure/function.
*****************************************************************************}

function FindNextWholeWord(const aSource, aWord : string; aStartPos : Integer) : Integer;
var
  SP, P, L : Integer;
begin
  L := Length(aWord);
  SP := aStartPos;
  while SP < Length(aSource) do begin
    P := Q_PosText(aWord, aSource, SP);
    if P = 0 then begin
      Result := 0;
      exit;
    end;
    if IsWholeWord(aSource, P, L) then begin
      Result := P;
      exit;
    end;
    SP := P + L;
  end;
  {We never get here, but prevent compiler from nagging}
  Result := 0;
end;

{*****************************************************************************
  Name           : SkipInBlock
  Author         : amuylaer
  Arguments      : const S : string; aStartPos : Integer; var aEndPos : Integer
  Return Values  : Boolean
  Exceptions     : None
  Description    : Skips the part between the brackets,  recursion is not a problem.
  History        :

  Date         By                   Description
  ----         --                   -----------
  21/10/2004    amuylaer               Initial creation of the Procedure/function.
*****************************************************************************}

function SkipInBlock(const S : string; aStartPos : Integer; var aEndPos : Integer) : Boolean;
var
  I, L, Recursion : Integer;
begin
  I := aStartPos;
  L := Length(S);
  Recursion := 0;
  while I < L do begin
    case S[I] of
      '(' : Inc(Recursion);
      ')' : if Recursion > 0 then begin
              Dec(Recursion);
              if Recursion = 0 then begin
                Result := True;
                aEndPos := I + 1;
                exit;
              end;
            end else begin
              Result := false;
              exit;
            end;
    end;
    Inc(I);
  end;
  Result := false;
end;

{*****************************************************************************
  Name           : FindNextBlock
  Author         : amuylaer
  Arguments      : const S : string; const aStartPos : Integer; var aFoundPos : Integer; var aKind : TSQLKind
  Return Values  : boolean
  Exceptions     : None
  Description    : Searches the next big block
  History        :

  Date         By                   Description
  ----         --                   -----------
  21/10/2004    amuylaer               Initial creation of the Procedure/function.
*****************************************************************************}

function  FindNextBlock(const S : string; const aStartPos : Integer; aStartKind : TSQLKind; var aFoundPos : Integer; var aKind : TSQLKind) : boolean;
var
  Pos : array[TSQLKind] of Integer;
  I : TSQLKind;
  EndOfInblock : Integer;

  procedure CalcPos(aSK : TSQLKind; aSP : Integer);
  var
    Kind : TSQLKind;
  begin
    FillChar(Pos, SizeOf(Pos), 0);
    for Kind := aSK to High(TSQLKind) do begin
      Pos[Kind] := FindNextWholeWord(S, cSQL[Kind], aSP);
    end;
  end;
begin
  CalcPos(aStartKind, aStartPos);

  if pos[sqlWhere] <> 0 then begin
    aKind := sqlWhere;
    aFoundPos := pos[sqlWhere];
    Result := True;
    exit;
  end;

  {Check if the IN statement is used and make sure this is not used by a level 1 having clause}
  if (Pos[sqlIn] <> 0) and ((Pos[sqlHaving] = 0) or (Pos[sqlIn] < Pos[sqlHaving])) then begin
    if SkipInBlock(S, Pos[sqlIn] + length(cSQL[sqlIn]) + 1, EndOfInblock) then begin
      CalcPos(sqlGroupBy, EndOfInBlock);
    end else begin
      Result := False;
      exit;
    end;
  end;

  {Return the lowest next block}
  for I := aStartKind to sqlHaving do begin
    if Pos[I] <> 0 then begin
      aKind := I;
      aFoundPos := Pos[I];
      Result := True;
      exit;
    end;
  end;

  Result := false;
end;

{*****************************************************************************
  Name           : RemoveWeirdChards
  Author         : amuylaer
  Arguments      : const S: string
  Return Values  : string
  Exceptions     : None
  Description    : Trims unwanted characters from the string,
                   Replace all spaces by a single space
                   Replaces CR by ' ' when needed

                   Strings ' or " are left untouched

  History        :

  Date         By                   Description
  ----         --                   -----------
  21/10/2004    amuylaer               Initial creation of the Procedure/function.
*****************************************************************************}

function  RemoveWeirdChars(const S: string) : string;
var
  L : Integer;
  I, C : Integer;
  Current, Prior : Char;
  StringMode : Boolean;
begin
  L := Length(S);
  Result := '';
  Prior := #0;
  StringMode := False;
  if L > 0 then begin
    C := 0;
    SetLength(Result, L);
    try
      for I := 1 to L do begin
        Current := S[I];
        if not StringMode then begin
          if Current in [#0..#31] then begin
            if Prior in [#0, ' '] then continue;
            Current := ' ';
          end;
          if (Current = ' ') and (Prior = ' ') then continue;
        end;

        Prior := Current;
        Inc(C);
        Result[C] := Prior;
        {Prevent spaces inside strings of being trimmed}
        if Prior in ['"', ''''] then StringMode := not StringMode;
      end;
    finally
      SetLength(Result, C);
    end;
  end;
end;

{*****************************************************************************
  This method will BreakUp the SQL Statement into the different parts.

  @Name       BreakStatement
  @author     amuylaer
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure BreakStatement(aSQL : TStrings; var aSelect, aWhere, aGroupBy, aOrderBy, aHaving : String);
var
  slSelect, slWhere, slGroupBy, slOrderBy, slHaving : TStringList;

  Statement : string;
  StartPos, FoundPos : Integer;
  StartKind, FoundKind : TSQLKind;
  procedure PushPart;
  var
    Part : string;
  begin
    Part := Copy(Statement, StartPos, FoundPos - StartPos);
    {$IFDEF CODESITE}
    csFVBFFCComponents.SendString( 'Part', Part );
    {$ENDIF}

    if Part <> '' then begin
      case StartKind of
        sqlIn       : raise Exception.Create('We cannot get here?');
        sqlWhere    : slWhere.Add(Part);
        sqlGroupBy  : slGroupBy.Add(Part);
        sqlOrderBy  : slOrderBy.Add(Part);
        sqlHaving   : slHaving.Add(Part);
      end;
    end;
  end;
begin
  slSelect := TStringList.Create;
  slWhere  := TStringList.Create;
  slGroupBy:= TStringList.Create;
  slOrderby:= TStringList.Create;
  slHaving := TStringList.Create;

  Try
    Statement := RemoveWeirdChars(aSQL.Text);
    StartPos := 1;

    {Fetch the select part}
    if FindNextBlock(Statement, StartPos, sqlWhere, FoundPos, FoundKind) then begin
      slSelect.Add(Copy(Statement, 1, FoundPos - 1));
    end else begin
      slSelect.Add(Statement);
      aSelect := slSelect.Text;
      exit;
    end;

    StartKind := FoundKind;
    StartPos := FoundPos + Length(cSQL[FoundKind]);

    while FindNextBlock(Statement, StartPos, Succ(StartKind), FoundPos, FoundKind) do begin
      PushPart;
      StartKind := FoundKind;
      StartPos := FoundPos + Length(cSQL[FoundKind]);
    end;

    FoundPos := Length(Statement) + 1;
    PushPart;

    aSelect := slSelect.Text;
    aWhere  := slWhere.Text;
    aGroupBy:= slGroupBy.Text;
    aOrderBy:= slOrderBy.Text;
    aHaving := slHaving.Text;
  finally
    FreeAndNil( slSelect  );
    FreeAndNil( slWhere   );
    FreeAndNil( slGroupBy );
    FreeAndNil( slOrderby );
    FreeAndNil( slHaving  );
  end;
end;

{*****************************************************************************
  This method will be used to split the given aSQL into different parts.

  @Name       SplitSQL
  @author     amuylaer
  @param      aSQL       This StringList will contain the SQL Statement that
                         should be splitted.
  @param      aSelect    This StringList will contain the SELECT clause of the
                         aSQL SQL Statement.
  @param      aWhere     This StringList will contain the WHERE clause of the
                         aSQL SQL Statement.
  @param      aGroupBy   This StringList will contain the GROUP BY clause of the
                         aSQL SQL Statement.
  @param      aOrderBy   This StringList will contain the ORDER BY clause of the
                         aSQL SQL Statement.
  @param      aHaving    This StringList will contain the HAVING clause of the
                         aSQL SQL Statement.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure SplitSQL(aSQL : TStrings; var aSelect, aWhere, aGroupBy, aOrderBy, aHaving : String);
//  { Prepares all the stringslists.
//    Makes sure begin and endupdate are called
//    Warning, this works recursivly
//  }
//  procedure Execute(aParts : array of TStrings; aIndex : Integer);
//  begin
//    if aIndex < Length(aParts) then begin
//      with aParts[aIndex] do begin
//        BeginUpdate;
//        try
//          Clear;
//          Execute(aParts, aIndex + 1);
//        finally
//          EndUpdate;
//        end;
//      end;
//    end else begin
//      BreakStatement(aSQL, aSelect, aWhere, aGroupBy, aOrderBy, aHaving);
//    end;
//  end;
begin
  {$IFDEF CODESITE}
  csFVBFFCComponents.EnterMethod( Nil, 'SplitSQL' );
  csFVBFFCComponents.SendStringList( 'aSQL', aSQL );
  csFVBFFCComponents.SendString( 'aSelect', aSelect );
  csFVBFFCComponents.SendString( 'aWhere', aWhere );
  csFVBFFCComponents.SendString( 'aGroupBy', aGroupBy );
  csFVBFFCComponents.SendString( 'aOrderBy', aOrderBy );
  csFVBFFCComponents.SendString( 'aHaving', aHaving );
  {$ENDIF}

  BreakStatement(aSQL, aSelect, aWhere, aGroupBy, aOrderBy, aHaving);
//  Execute([aSelect, aWhere, aGroupBy, aOrderBy, aHaving], 0);

  {$IFDEF CODESITE}
  csFVBFFCComponents.SendStringList( 'aSQL', aSQL );
  csFVBFFCComponents.SendString( 'aSelect', aSelect );
  csFVBFFCComponents.SendString( 'aWhere', aWhere );
  csFVBFFCComponents.SendString( 'aGroupBy', aGroupBy );
  csFVBFFCComponents.SendString( 'aOrderBy', aOrderBy );
  csFVBFFCComponents.SendString( 'aHaving', aHaving );
  csFVBFFCComponents.ExitMethod( Nil, 'SplitSQL' );
  {$ENDIF}
end;

const
  { Deze tabel mapt chars naar een accentloze uppercase variant}
  { rij + kolom geeft waarde}
  cCharMap: array[0..255] of Char =
    {   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F}
{0} (#$00,#$01,#$02,#$03,#$04,#$05,#$06,#$07,#$08,#$09,#$0A,#$0B,#$0C,#$0D,#$0E,#$0F,
{1}  #$10,#$11,#$12,#$13,#$14,#$15,#$16,#$17,#$18,#$19,#$1A,#$1B,#$1C,#$1D,#$1E,#$1F,
{2}  ' ' ,#$21,#$22,#$23,#$24,#$25,#$26,#$27,#$28,#$29,#$2A,#$2B,#$2C,#$2D,#$2E,#$2F,
{3}  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', #$3A,#$3B,#$3C,#$3D,#$3E,#$3F,
{4}  #$40,'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
{5}  'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', #$5B,#$5C,#$5D,#$5E,#$5F,
{6}  #$60,'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
{7}  'P', 'Q', 'R', 'S' ,'T', 'U', 'V', 'W', 'X', 'Y', 'Z', #$7B,#$7C,#$7D,#$7E,#$7F,
{Extended set}
{8}  'C', 'U', 'E' ,'A' ,'A' ,'A' ,'A' ,'C', 'E', 'E', 'E' ,'I', 'I', 'I', 'A', 'A',
{9}  'E', #$91,#$92,'O', 'O', 'O', 'U', 'U', #$98,'O', 'U', #$9B,#$8C,#$8D,#$8E,'A',
{A}  'I', 'O', 'U', 'N', 'N',#$A5,#$A6,#$A7,#$A8,#$A9,#$AA,#$AB,#$AC,#$AD,#$AE,#$AF,
{B}  #$B0,#$B1,#$B2,#$B2,#$A5,#$B5,#$B6,#$B7,#$A8,#$B9,#$AA,#$BB,#$A3,#$BD,#$BD,#$AF,
{C}  #$C0,#$C1,#$C2,#$C3,#$C4,#$C5,#$C6,#$C7,#$C8,#$C9,#$CA,#$CB,#$CC,#$CD,#$CE,#$CF,
{D}  #$D0,#$D1,#$D2,#$D3,#$D4,#$D5,#$D6,#$D7,#$D8,#$D9,#$DA,#$DB,#$DC,#$DD,#$DE,#$DF,
{E}  #$C0,#$C1,#$C2,#$C3,#$C4,#$C5,#$C6,#$C7,#$C8,#$C9,#$CA,#$CB,#$CC,#$CD,#$CE,#$CF,
{F}  #$D0,#$D1,#$D2,#$D3,#$D4,#$D5,#$D6,#$D7,#$D8,#$D9,#$DA,#$DB,#$DC,#$DD,#$DE,#$DF);

function Q_PosText(const FindString, SourceString: string; StartPos: Integer): Integer;
asm
        PUSH    ESI
        PUSH    EDI
        PUSH    EBX
        TEST    EAX,EAX
        JE      @@qt
        TEST    EDX,EDX
        JE      @@qt0
        MOV     ESI,EAX
        MOV     EDI,EDX
        PUSH    EDX
        MOV     EAX,[EAX-4]
        MOV     EDX,[EDX-4]
        DEC     EAX
        SUB     EDX,EAX
        DEC     ECX
        PUSH    EAX
        SUB     EDX,ECX
        JNG     @@qtx
        ADD     EDI,ECX
        MOV     ECX,EDX
        MOV     EDX,EAX
        MOVZX   EBX,BYTE PTR [ESI]
        MOV     AL,BYTE PTR [EBX+cCharMap]
@@lp1:  MOVZX   EBX,BYTE PTR [EDI]
        CMP     AL,BYTE PTR [EBX+cCharMap]
        JE      @@uu
@@fr:   INC     EDI
        DEC     ECX
        JE      @@qtx
        MOVZX   EBX,BYTE PTR [EDI]
        CMP     AL,BYTE PTR [EBX+cCharMap]
        JE      @@uu
        INC     EDI
        DEC     ECX
        JE      @@qtx
        MOVZX   EBX,BYTE PTR [EDI]
        CMP     AL,BYTE PTR [EBX+cCharMap]
        JE      @@uu
        INC     EDI
        DEC     ECX
        JE      @@qtx
        MOVZX   EBX,BYTE PTR [EDI]
        CMP     AL,BYTE PTR [EBX+cCharMap]
        JE      @@uu
        INC     EDI
        DEC     ECX
        JNE     @@lp1
@@qtx:  ADD     ESP,$08
@@qt0:  XOR     EAX,EAX
@@qt:   POP     EBX
        POP     EDI
        POP     ESI
        RET
@@ms:   MOVZX   EBX,BYTE PTR [ESI]
        MOV     AL,BYTE PTR [EBX+cCharMap]
        MOV     EDX,[ESP]
        JMP     @@fr
@@uu:   TEST    EDX,EDX
        JE      @@fd
@@lp2:  MOV     BL,BYTE PTR [ESI+EDX]
        MOV     AH,BYTE PTR [EDI+EDX]
        CMP     BL,AH
        JE      @@eq
        MOV     AL,BYTE PTR [EBX+cCharMap]
        MOVZX   EBX,AH
        CMP     AL,BYTE PTR [EBX+cCharMap]
        JNE     @@ms
@@eq:   DEC     EDX
        JNZ     @@lp2
@@fd:   LEA     EAX,[EDI+1]
        POP     ECX
        SUB     EAX,[ESP]
        POP     ECX
        POP     EBX
        POP     EDI
        POP     ESI
end;

function CombineSQL(aSelect, aWhere, aGroupBy, aOrderBy, aHaving : String) : String;
begin
  Result := aSelect;

  if ( aWhere <> '' ) then
  begin
    Result := Result + ' WHERE ' + aWhere;
  end;

  if ( aGroupBy <> '' ) then
  begin
    Result := Result + ' GROUP BY ' + aGroupBy;
  end;

  if ( aOrderBy <> '' ) then
  begin
    Result := Result + ' ORDER BY ' + aOrderBy;
  end;

  if ( aHaving <> '' ) then
  begin
    Result := Result + ' HAVING ' + aHaving;
  end;
end;

Function GetProperty( Obj: IDispatch; Const Name: String ): OleVariant;
Var
  ID: Integer;
  WideName: WideString;
  DispParams: TDispParams;
  ExcepInfo: TExcepInfo;
  Status: Integer;
Begin
  WideName := Name;
  OleCheck( Obj.GetIDsOfNames( GUID_NULL, @WideName, 1, 0, @ID ) );
  FillChar( DispParams, SizeOf( DispParams ), 0 );
  FillChar( ExcepInfo, SizeOf( ExcepInfo ), 0 );
  Status := Obj.Invoke( ID, GUID_NULL, 0, DISPATCH_PROPERTYGET, DispParams,
    @Result, @ExcepInfo, Nil );
  If Status <> 0 Then DispatchInvokeError( Status, ExcepInfo );
End;


Procedure TFVBFFCSharedConnection.DoConnect;
Begin
  If Not Assigned( FParentConnection ) Then
    Raise Exception.Create( 'ParentConnection is not assigned' );
  If FObjectName = '' Then
    Raise Exception.Create( 'ObjectName cannot be blank' );
  If Not FParentConnection.Connected Then
    FParentConnection.Connected := True;
  SetAppServer( GetProperty( IDispatch( FParentConnection.AppServer ), FObjectName ) );
End;

Procedure TFVBFFCSharedConnection.Notification( AComponent: TComponent; Operation: TOperation );
Begin
  Inherited Notification( AComponent, Operation );
  If ( Operation = opRemove ) And ( FParentConnection <> Nil ) And
    ( AComponent = FParentConnection ) Then
    FParentConnection := Nil;
End;

Procedure TFVBFFCSharedConnection.SetConnected( Value: Boolean );
Begin
  If Value Then
  Begin
    If Assigned( FBeforeConnect ) Then
    Begin
      FBeforeConnect( Self );
    End;
  End
  Else
  Begin
    If Assigned( FBeforeDisconnect ) Then
    Begin
      FBeforeDisconnect( Self );
    End;
  End; { if connected = true }

  Inherited SetConnected( Value );

  FReallyConnected := Value;
  FInternalConnected := Value;

  If Value Then
  Begin
    If ( Assigned( FAfterConnect )
      And ( Connected = true ) ) Then
    Begin
      FBeforeConnect( Self );
    End;
  End
  Else
  Begin
    If ( Assigned( FAfterDisconnect )
      And ( Connected = false ) ) Then
    Begin
      FBeforeDisconnect( Self );
    End;
  End; { if connected = true }
End;

Procedure TFVBFFCSharedConnection.CloseAllDataSets;
Var
  Lcv: Integer;
Begin
  For Lcv := Pred( self.DataSetCount ) Downto 0 Do
  Begin
    Self.DataSets[ Lcv ].Close( );
  End;
End;

Function TFVBFFCSharedConnection.GetAppServer: Variant;
Begin
  If Not FInternalConnected Then
  Begin
    FInternalConnected := True;
    SetConnected( True );
  End;
  Result := Inherited GetAppServer;
End;

Constructor TFVBFFCSharedConnection.Create( AOwner: TComponent );
Begin
//  {$IFDEF CODESITE}
//  csFVBFFCComponents.EnterMethod( Self, 'Create' );
//  {$ENDIF}
  FInternalConnected := False;
  FReallyConnected := False;
  Inherited;
//  {$IFDEF CODESITE}
//  csFVBFFCComponents.ExitMethod( Self, 'Create' );
//  {$ENDIF}
End;

Function TFVBFFCSharedConnection.GetConnected: Boolean;
Begin
  Result := FReallyConnected;
End;


{ TFVBFFCDataSetProvider }

constructor TFVBFFCDataSetProvider.Create(AOwner: TComponent);
begin
  inherited;
  UpdateMode := upWhereKeyOnly;
end;

{ TFVBFFCQuery }

{*****************************************************************************
  This method will be used to add a given WhereClause to the SQL Statement.
  If the SQL Statement already contains a Where Clause, they will be joined
  together using an AND statement.

  @Name       TFVBFFCQuery.AddWhereClause
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.AddWhereClause(aWhereClause: String);
var
  aSelect, aWhere, aOrderBy, aGroupBy, aHaving : String;
begin
  {$IFDEF CODESITE}
  csFVBFFCComponents.EnterMethod( Self, 'AddWhereClause' );
  csFVBFFCComponents.SendStringList( 'Old SQL', SQL );
  {$ENDIF}

  SplitSQL( Self.SQL, aSelect, aWhere, aGroupBy, aOrderBy, aHaving );

  if ( aWhere <> '' ) then
  begin
    aWhere := '( ' + aWhere + ' ) AND ( ' + aWhereClause + ' )';
  end
  else
  begin
    aWhere := aWhereClause;
  end;

  Self.SQL.Text := CombineSQL( aSelect, aWhere, aGroupBy, aOrderBy, aHaving );

  {$IFDEF CODESITE}
  csFVBFFCComponents.SendStringList( 'New SQL', SQL );
  csFVBFFCComponents.ExitMethod( Self, 'AddWhereClause' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to add a Where clause to the SQL Statement.  This
  WHERE clause can later be combined with other where clauses.

  @Name       TFVBFFCQuery.ApplyDetailFilter
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.ApplyDetailFilter(const aFilter: String);
var
  aSelect, aWhere, aGroupBy, aOrderBy, aHaving: String;
begin
  {$IFDEF CODESITE}
  csFVBFFCComponents.EnterMethod( Self, 'ApplyDetailFilter' );
  csFVBFFCComponents.SendObject( 'TCMBIBQuery', Self );
  csFVBFFCComponents.SendObject( 'TCMBIBQuery.Owner', Self.Owner );
  csFVBFFCComponents.SendStringList( 'SQL before applying Detail Filter', SQL );
  {$ENDIF}

  FDetailFilter := aFilter;

  SplitSQL( Self.FDesignTimeSQL, aSelect, aWhere, aGroupBy, aOrderBy, aHaving );


  if ( FDetailFilter <> '' ) then
  begin
    if ( aWhere <> '' ) then
    begin
      aWhere := '( ' + aWhere + ' ) AND ( ' + FDetailFilter + ' )';
    end
    else
    begin
      aWhere := '(' + FDetailFilter + ')';
    end;
  end;

  Self.SQL.Text := CombineSQL( aSelect, aWhere, aGroupBy, aOrderBy, aHaving );

  {$IFDEF CODESITE}
  csFVBFFCComponents.SendString( 'Detail Filter', FDetailFilter );
  csFVBFFCComponents.SendStringList( 'SQL After applying Detail Filter', SQL );
  csFVBFFCComponents.ExitMethod( Self, 'ApplyDetailFilter' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to add a Where clause to the SQL Statement.

  @Name       TFVBFFCQuery.ApplyFilter
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.ApplyFilter(const aFilter: String);
var
  aSelect, aWhere, aGroupBy, aOrderBy, aHaving: String;
begin
  {$IFDEF CODESITE}
  csFVBFFCComponents.EnterMethod( Self, 'ApplyFilter' );
  csFVBFFCComponents.SendStringList( 'SQL before applying Filter', SQL );
  csFVBFFCComponents.SendString( 'Detail Filter', FDetailFilter );
  {$ENDIF}

  SplitSQL( Self.FDesignTimeSQL, aSelect, aWhere, aGroupBy, aOrderBy, aHaving );

  if ( FDetailFilter <> '' ) then
  begin
    if ( aWhere <> '' ) then
    begin
      aWhere := '( ' + aWhere + ' ) AND ( ' + FDetailFilter + ' )';
    end
    else
    begin
      aWhere := FDetailFilter;
    end;
  end;

  if ( aFilter <> '' ) then
  begin
    if ( aWhere <> '' ) then
    begin
      aWhere := '( ' + aWhere + ' ) AND ( ' + aFilter + ' )';
    end
    else
    begin
      aWhere := aFilter;
    end;
  end;

  Self.SQL.Text := CombineSQL( aSelect, aWhere, aGroupBy, aOrderBy, aHaving );

  {$IFDEF CODESITE}
  csFVBFFCComponents.SendStringList( 'SQL before applying Filter', SQL );
  csFVBFFCComponents.ExitMethod( Self, 'ApplyFilter' );
  {$ENDIF}
end;

{*****************************************************************************
  This method will be used to add an ORDER BY clause to the SQL Statement.
  @Name       TFVBFFCQuery.ApplyOrderBy
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.ApplyOrderBy(const aOrderBy: String);
var
  aSelect, aWhere, aGroupBy, aSortOrder, aHaving: String;
begin
  {$IFDEF CODESITE}
  csFVBFFCComponents.EnterMethod( Self, 'ApplyOrderBy' );
  csFVBFFCComponents.SendStringList( 'SQL before applying Filter', SQL );
  {$ENDIF}

  SplitSQL( Self.SQL, aSelect, aWhere, aGroupBy, aSortOrder, aHaving );

  Self.SQL.Text := CombineSQL( aSelect, aWhere, aGroupBy, aOrderBy, aHaving );

  {$IFDEF CODESITE}
  csFVBFFCComponents.SendString( 'SQL', Self.SQL.Text );
  csFVBFFCComponents.ExitMethod( Self, 'ApplyOrderBy' );
  {$ENDIF}
end;

{*****************************************************************************
  Property Getter for the AutoOpen property.

  @Name       TFVBFFCQuery.GetAutoOpen
  @author     slesage
  @param      None
  @return     Returns the value of the AutoOpen property.
  @Exception  None
  @See        None
******************************************************************************}

function TFVBFFCQuery.GetAutoOpen: Boolean;
begin
  Result := FAutoOpen;
end;

{*****************************************************************************
  Stuff FDesignTimeSQL with the design time SQL.  Needed to be able to start
  with the original SQL statement when filtering and/or sorting.

  @Name       TFVBFFCQuery.Loaded
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.Loaded;
begin
  inherited;
  FDesignTimeSQL.Assign(SQL);
end;

{*****************************************************************************
  Property Setter for the AutoOpen property.

  @Name       TFVBFFCQuery.SetAutoOpen
  @author     slesage
  @param      Value   The new value for the AutoOpen property.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.SetAutoOpen(const Value: Boolean);
begin
  if ( Value <> FAutoOpen ) then
  begin
    FAutoOpen := Value;
  end;
end;

{*****************************************************************************
  This method will be executed when the Cursor of the DataSet is opened.

  @Name       TFVBFFCQuery.OpenCursor
  @author     slesage
  @param      Sender   The Object from which the method was invoked.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.OpenCursor(InfoQuery: Boolean);
begin
  {$IFDEF CODESITE}
  csFVBFFCComponents.EnterMethod( Self, 'OpenCursor' );
  csFVBFFCComponents.SendObject( 'TCMBIBQuery', Self );
  csFVBFFCComponents.SendObject( 'TCMBIBQuery.Owner', Self.Owner );
  SendSQLToCodeSite;
  SendParamsToCodeSite;
  {$ENDIF}
  inherited OpenCursor( InfoQuery );
  {$IFDEF CODESITE}
  if ( Active ) then
  begin
    csFVBFFCComponents.SendInteger( 'Number of Records', RecordCount );
  end;
  csFVBFFCComponents.ExitMethod( Self, 'OpenCursor' );
  {$ENDIF}
end;

{$IFDEF CODESITE}

{*****************************************************************************
  This method will be used to Send all Parameter information to csFVBFFCComponents.

  @Name       TFVBFFCQuery.SendParamsToCodeSite
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.SendParamsToCodeSite;
var
  lcv : Integer;
begin
  csFVBFFCComponents.EnterMethod( Self, 'SendParamsToCodeSite' );

  for lcv := 0 to Pred( Parameters.Count ) do
  begin
    csFVBFFCComponents.SendVariant( Parameters[ lcv ].Name, Parameters[ lcv ].Value );
  end;

  csFVBFFCComponents.ExitMethod( Self, 'SendParamsToCodeSite' );
end;
{$ENDIF}

{$IFDEF CODESITE}

{*****************************************************************************
  This method will be used to send the SQL Statement to csFVBFFCComponents.

  @Name       TFVBFFCQuery.SendSQLToCodeSite
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

procedure TFVBFFCQuery.SendSQLToCodeSite;
begin
  csFVBFFCComponents.EnterMethod( Self, 'SendSQLToCodeSite' );
  csFVBFFCComponents.SendStringList( 'SQL Statement', SQL );
  csFVBFFCComponents.ExitMethod( Self, 'SendSQLToCodeSite' );
end;
{$ENDIF}

{*****************************************************************************
  Constructor for the TFVBFFCQuery.

  @Name       TFVBFFCQuery.Create
  @author     slesage
  @param      AOwner   The owner of the TFVBFFCQuery
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

constructor TFVBFFCQuery.Create(AOwner: TComponent);
begin
  FAutoOpen := False;
  FDesignTimeSQL := TStringList.Create;
  inherited Create( AOwner );
end;

{*****************************************************************************
  Destructor for the TFVBFFCQuery Component.

  @Name       TFVBFFCQuery.Destroy
  @author     slesage
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

destructor TFVBFFCQuery.Destroy;
begin
  FreeAndNil( FDesignTimeSQL );
  inherited Destroy;
end;

{ TFVBFFCStoredProc }

function TFVBFFCStoredProc.GetAutoOpen: Boolean;
begin
  Result := FAutoOpen;
end;

procedure TFVBFFCStoredProc.SetAutoOpen(const Value: Boolean);
begin
  if ( Value <> FAutoOpen ) then
  begin
    FAutoOpen := Value;
  end;
end;

{ TFVBFFCClientDataSet }

constructor TFVBFFCClientDataSet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FClearEmptyStrings := False;
end;

procedure TFVBFFCClientDataSet.Post;
var
  i: Integer;
begin
  if FClearEmptyStrings then
  begin
    if State in [dsEdit, dsInsert] then
    begin
      for i := 0 to FieldCount - 1 do
      begin
        if Fields[i].DataType = ftString then
        begin
          if Length(Trim(Fields[i].AsString)) = 0 then
          begin
            if not Fields[i].ReadOnly then
            begin
              Fields[i].Clear;
            end;
          end;
        end;
      end;
    end;
  end;
  inherited Post;
end;

procedure TFVBFFCQuery.SetSQLStatement(const aSQL: String);
begin
  Self.SQL.Text := aSQL;
end;

end.
