object FVBFFCBaseRecordView: TFVBFFCBaseRecordView
  Left = 224
  Top = 209
  Height = 400
  Align = alNone
  Caption = 'FVBFFC Base RecordView'
  ClientHeight = 361
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 400
  Constraints.MinWidth = 800
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  FormStyle = fsNormal
  OldCreateOrder = False
  WindowState = wsNormal
  OnClose = FVBFFCRecordViewClose
  OnCloseQuery = FVBFFCRecordViewCloseQuery
  OnCreate = FVBFFCRecordViewCreate
  OnShow = FVBFFCRecordViewShow
  DataSource = srcMain
  Registered = False
  UseFocusController = False
  OnInitialise = FVBFFCRecordViewInitialise
  PixelsPerInch = 96
  TextHeight = 13
  object bbtnBtnESCPressed: TBitBtn
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'bbtnBtnESCPressed'
    TabOrder = 0
    OnClick = bbtnBtnESCPressedClick
  end
  object pnlBottom: TFVBFFCPanel
    Left = 0
    Top = 329
    Width = 784
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 0
    ParentColor = True
    TabOrder = 1
    StyleBackGround.BackColor = clInactiveCaption
    StyleBackGround.BackColor2 = clInactiveCaption
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clInactiveCaption
    StyleClientArea.BackColor2 = clInactiveCaption
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object pnlBottomButtons: TFVBFFCPanel
      Left = 448
      Top = 0
      Width = 336
      Height = 32
      Align = alRight
      BevelOuter = bvNone
      BorderWidth = 0
      ParentColor = True
      TabOrder = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object cxbtnOK: TFVBFFCButton
        Left = 9
        Top = 4
        Width = 104
        Height = 25
        Caption = 'OK'
        Default = True
        ModalResult = 1
        OptionsImage.Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        OptionsImage.Margin = 10
        OptionsImage.NumGlyphs = 2
        OptionsImage.Spacing = -1
        TabOrder = 0
        OnClick = cxbtnOKClick
      end
      object cxbtnCancel: TFVBFFCButton
        Left = 119
        Top = 4
        Width = 104
        Height = 25
        Caption = 'Cancel'
        ModalResult = 2
        OptionsImage.Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333333333000033338833333333333333333F333333333333
          0000333911833333983333333388F333333F3333000033391118333911833333
          38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
          911118111118333338F3338F833338F3000033333911111111833333338F3338
          3333F8330000333333911111183333333338F333333F83330000333333311111
          8333333333338F3333383333000033333339111183333333333338F333833333
          00003333339111118333333333333833338F3333000033333911181118333333
          33338333338F333300003333911183911183333333383338F338F33300003333
          9118333911183333338F33838F338F33000033333913333391113333338FF833
          38F338F300003333333333333919333333388333338FFF830000333333333333
          3333333333333333333888330000333333333333333333333333333333333333
          0000}
        OptionsImage.Margin = 10
        OptionsImage.NumGlyphs = 2
        OptionsImage.Spacing = -1
        TabOrder = 1
        OnClick = cxbtnCancelClick
      end
      object cxbtnApply: TFVBFFCButton
        Left = 229
        Top = 4
        Width = 104
        Height = 25
        Caption = 'Apply'
        OptionsImage.Margin = 10
        OptionsImage.Spacing = -1
        TabOrder = 2
        OnClick = cxbtnOKClick
      end
    end
  end
  object pnlTop: TFVBFFCPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 329
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 0
    TabOrder = 2
    StyleBackGround.BackColor = clAppWorkSpace
    StyleBackGround.BackColor2 = clAppWorkSpace
    StyleBackGround.Font.Charset = DEFAULT_CHARSET
    StyleBackGround.Font.Color = clWindowText
    StyleBackGround.Font.Height = -11
    StyleBackGround.Font.Name = 'Verdana'
    StyleBackGround.Font.Style = []
    StyleClientArea.BackColor = clWindow
    StyleClientArea.BackColor2 = 15254445
    StyleClientArea.Font.Charset = DEFAULT_CHARSET
    StyleClientArea.Font.Color = clWindowText
    StyleClientArea.Font.Height = -11
    StyleClientArea.Font.Name = 'Verdana'
    StyleClientArea.Font.Style = [fsBold]
    object pnlRecord: TFVBFFCPanel
      Left = 255
      Top = 0
      Width = 525
      Height = 329
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 0
      ParentColor = True
      TabOrder = 0
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
      object pnlRecordHeader: TFVBFFCPanel
        Left = 0
        Top = 4
        Width = 525
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 0
        Caption = 'Fixed Record Information'
        ParentColor = True
        TabOrder = 0
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clHighlightText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clMenuHighlight
        StyleClientArea.BackColor2 = clMenuHighlight
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clHighlightText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object pnlRecordHeaderTopSpacer: TFVBFFCPanel
        Left = 0
        Top = 0
        Width = 525
        Height = 4
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 4
        ParentColor = True
        TabOrder = 1
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clInactiveCaption
        StyleClientArea.BackColor2 = clInactiveCaption
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object pnlRecordFixedData: TFVBFFCPanel
        Left = 0
        Top = 29
        Width = 525
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 4
        ParentColor = True
        TabOrder = 2
        StyleBackGround.BackColor = 15914442
        StyleBackGround.BackColor2 = 15914442
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = 15914442
        StyleClientArea.BackColor2 = 15914442
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object pnlRecordDetailSeperator: TFVBFFCPanel
        Left = 0
        Top = 66
        Width = 525
        Height = 4
        Align = alTop
        BevelOuter = bvNone
        BorderWidth = 4
        ParentColor = True
        TabOrder = 3
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clInactiveCaption
        StyleClientArea.BackColor2 = clInactiveCaption
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object pnlEDUPanel1: TFVBFFCPanel
        Left = 0
        Top = 325
        Width = 525
        Height = 4
        Align = alBottom
        BevelOuter = bvNone
        BorderWidth = 4
        ParentColor = True
        TabOrder = 4
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clInactiveCaption
        StyleClientArea.BackColor2 = clInactiveCaption
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
      end
      object pnlRecordDetail: TFVBFFCPanel
        Left = 0
        Top = 70
        Width = 525
        Height = 255
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 0
        TabOrder = 5
        StyleBackGround.BackColor = clInactiveCaption
        StyleBackGround.BackColor2 = clInactiveCaption
        StyleBackGround.Font.Charset = DEFAULT_CHARSET
        StyleBackGround.Font.Color = clWindowText
        StyleBackGround.Font.Height = -11
        StyleBackGround.Font.Name = 'Verdana'
        StyleBackGround.Font.Style = []
        StyleClientArea.BackColor = clInactiveCaption
        StyleClientArea.BackColor2 = clInactiveCaption
        StyleClientArea.Font.Charset = DEFAULT_CHARSET
        StyleClientArea.Font.Color = clWindowText
        StyleClientArea.Font.Height = -11
        StyleClientArea.Font.Name = 'Verdana'
        StyleClientArea.Font.Style = [fsBold]
        object pnlRecordDetailTitle: TFVBFFCPanel
          Left = 0
          Top = 0
          Width = 525
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 0
          Caption = 'Record Detail'
          ParentColor = True
          TabOrder = 0
          StyleBackGround.BackColor = clInactiveCaption
          StyleBackGround.BackColor2 = clInactiveCaption
          StyleBackGround.Font.Charset = DEFAULT_CHARSET
          StyleBackGround.Font.Color = clWindowText
          StyleBackGround.Font.Height = -11
          StyleBackGround.Font.Name = 'Verdana'
          StyleBackGround.Font.Style = []
          StyleClientArea.BackColor = clWhite
          StyleClientArea.BackColor2 = clGradientInactiveCaption
          StyleClientArea.Font.Charset = DEFAULT_CHARSET
          StyleClientArea.Font.Color = clWindowText
          StyleClientArea.Font.Height = -11
          StyleClientArea.Font.Name = 'Verdana'
          StyleClientArea.Font.Style = [fsBold]
        end
        object sbxMain: TScrollBox
          Left = 0
          Top = 25
          Width = 525
          Height = 230
          HorzScrollBar.Style = ssFlat
          VertScrollBar.Style = ssFlat
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15914442
          ParentColor = False
          TabOrder = 1
        end
      end
    end
    object pnlRightSpacer: TFVBFFCPanel
      Left = 780
      Top = 0
      Width = 4
      Height = 329
      Align = alRight
      BevelOuter = bvNone
      BorderWidth = 4
      TabOrder = 1
      StyleBackGround.BackColor = clInactiveCaption
      StyleBackGround.BackColor2 = clInactiveCaption
      StyleBackGround.Font.Charset = DEFAULT_CHARSET
      StyleBackGround.Font.Color = clWindowText
      StyleBackGround.Font.Height = -11
      StyleBackGround.Font.Name = 'Verdana'
      StyleBackGround.Font.Style = []
      StyleClientArea.BackColor = clInactiveCaption
      StyleClientArea.BackColor2 = clInactiveCaption
      StyleClientArea.Font.Charset = DEFAULT_CHARSET
      StyleClientArea.Font.Color = clWindowText
      StyleClientArea.Font.Height = -11
      StyleClientArea.Font.Name = 'Verdana'
      StyleClientArea.Font.Style = [fsBold]
    end
    object dxnbNavBar: TFVBFFCNavBar
      Left = 0
      Top = 0
      Width = 247
      Height = 329
      Align = alLeft
      ActiveGroupIndex = 0
      TabOrder = 2
      View = 10
      OptionsStyle.DefaultStyles.Background.BackColor = clNone
      OptionsStyle.DefaultStyles.Background.BackColor2 = clNone
      OptionsStyle.DefaultStyles.Background.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.Background.Font.Color = clWindowText
      OptionsStyle.DefaultStyles.Background.Font.Height = -11
      OptionsStyle.DefaultStyles.Background.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.Background.Font.Style = []
      OptionsStyle.DefaultStyles.Background.AssignedValues = [savBackColor, savBackColor2, savFont]
      OptionsStyle.DefaultStyles.Button.BackColor = clBtnFace
      OptionsStyle.DefaultStyles.Button.BackColor2 = clBtnFace
      OptionsStyle.DefaultStyles.Button.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.Button.Font.Color = clWindowText
      OptionsStyle.DefaultStyles.Button.Font.Height = -11
      OptionsStyle.DefaultStyles.Button.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.Button.Font.Style = []
      OptionsStyle.DefaultStyles.Button.AssignedValues = [savBackColor, savBackColor2, savFont]
      OptionsStyle.DefaultStyles.GroupHeader.BackColor = clNone
      OptionsStyle.DefaultStyles.GroupHeader.BackColor2 = clNone
      OptionsStyle.DefaultStyles.GroupHeader.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.GroupHeader.Font.Color = clNone
      OptionsStyle.DefaultStyles.GroupHeader.Font.Height = -11
      OptionsStyle.DefaultStyles.GroupHeader.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.GroupHeader.Font.Style = [fsBold]
      OptionsStyle.DefaultStyles.GroupHeader.AssignedValues = [savFont]
      OptionsStyle.DefaultStyles.Item.BackColor = clWhite
      OptionsStyle.DefaultStyles.Item.BackColor2 = clWhite
      OptionsStyle.DefaultStyles.Item.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.Item.Font.Color = clNone
      OptionsStyle.DefaultStyles.Item.Font.Height = -11
      OptionsStyle.DefaultStyles.Item.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.Item.Font.Style = []
      OptionsStyle.DefaultStyles.Item.AssignedValues = [savFont]
      OptionsStyle.DefaultStyles.ItemDisabled.BackColor = clWhite
      OptionsStyle.DefaultStyles.ItemDisabled.BackColor2 = clWhite
      OptionsStyle.DefaultStyles.ItemDisabled.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.ItemDisabled.Font.Color = clNone
      OptionsStyle.DefaultStyles.ItemDisabled.Font.Height = -11
      OptionsStyle.DefaultStyles.ItemDisabled.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.ItemDisabled.Font.Style = []
      OptionsStyle.DefaultStyles.ItemDisabled.AssignedValues = [savFont]
      OptionsStyle.DefaultStyles.ItemHotTracked.BackColor = clWhite
      OptionsStyle.DefaultStyles.ItemHotTracked.BackColor2 = clWhite
      OptionsStyle.DefaultStyles.ItemHotTracked.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.ItemHotTracked.Font.Color = clNone
      OptionsStyle.DefaultStyles.ItemHotTracked.Font.Height = -11
      OptionsStyle.DefaultStyles.ItemHotTracked.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.ItemHotTracked.Font.Style = [fsItalic, fsUnderline]
      OptionsStyle.DefaultStyles.ItemHotTracked.AssignedValues = [savFont]
      OptionsStyle.DefaultStyles.ItemPressed.BackColor = clWhite
      OptionsStyle.DefaultStyles.ItemPressed.BackColor2 = clWhite
      OptionsStyle.DefaultStyles.ItemPressed.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.ItemPressed.Font.Color = clNone
      OptionsStyle.DefaultStyles.ItemPressed.Font.Height = -11
      OptionsStyle.DefaultStyles.ItemPressed.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.ItemPressed.Font.Style = []
      OptionsStyle.DefaultStyles.ItemPressed.AssignedValues = [savFont]
      OptionsStyle.DefaultStyles.NavigationPaneHeader.BackColor = clWhite
      OptionsStyle.DefaultStyles.NavigationPaneHeader.BackColor2 = clWhite
      OptionsStyle.DefaultStyles.NavigationPaneHeader.Font.Charset = ANSI_CHARSET
      OptionsStyle.DefaultStyles.NavigationPaneHeader.Font.Color = clWindowText
      OptionsStyle.DefaultStyles.NavigationPaneHeader.Font.Height = -11
      OptionsStyle.DefaultStyles.NavigationPaneHeader.Font.Name = 'Verdana'
      OptionsStyle.DefaultStyles.NavigationPaneHeader.Font.Style = []
      OptionsStyle.DefaultStyles.NavigationPaneHeader.AssignedValues = [savBackColor, savBackColor2, savFont]
      OptionsView.ExplorerBar.ShowSpecialGroup = True
      object dxnbgGeneral: TdxNavBarGroup
        Caption = 'General'
        SelectedLinkIndex = -1
        TopVisibleLinkIndex = 0
        OptionsExpansion.Expandable = False
        OptionsExpansion.ShowExpandButton = False
        Links = <
          item
            Item = dxnbiRecordDetail
          end>
      end
      object dxnbgRelatedInfo: TdxNavBarGroup
        Caption = 'Related Info'
        SelectedLinkIndex = -1
        TopVisibleLinkIndex = 0
        Links = <>
      end
      object dxnbiRecordDetail: TdxNavBarItem
        Action = acShowRecordDetail
      end
    end
    object FVBFFCSplitter1: TFVBFFCSplitter
      Left = 247
      Top = 0
      Width = 8
      Height = 329
      HotZoneClassName = 'TcxMediaPlayer9Style'
      NativeBackground = False
      Control = dxnbNavBar
      Color = clInactiveCaption
      ParentColor = False
    end
  end
  object srcMain: TFVBFFCDataSource
    OnStateChange = srcMainStateChange
    OnDataChange = srcMainDataChange
    Left = 100
    Top = 312
  end
  object alRecordView: TFVBFFCActionList
    Left = 64
    Top = 312
    object acShowRecordDetail: TAction
      Caption = 'Record Detail'
      OnExecute = acShowRecordDetailExecute
    end
  end
end
