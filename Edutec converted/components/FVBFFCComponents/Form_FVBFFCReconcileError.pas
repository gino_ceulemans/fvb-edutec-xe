{*****************************************************************************
  Name           : form_FVBFFCReconcileError
  Author         : Wim Lambrechts
  Copyright      : (c) 2001 Peopleware
  Description    : This unit is a replacement form for ReconcileErrors
                   and contains a similar HandleReconcileError function
  History        :

  Date         By                   Description
  ----         --                   -----------
  07/09/2001   Cools Pascal         Changed HandleReconcileError
  08/06/2001   Lambrechts Wim       Added HandleReconcileError
  17/04/2001   Lambrechts Wim       Initial creation of the Unit.
 *****************************************************************************}
unit Form_FVBFFCReconcileError;

interface

uses Variants, 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ComCtrls, ExtCtrls, DB, dbclient, MConnect,
  Unit_FVBFFCDBComponents;

type
  TShowReconcileErrorEvent = procedure(UpdateKind: TUpdateKind; var aMessage, aLongMessage: String) of object;

  TErrorButton = (ebOK, ebCancel, ebYes, ebNo);
  TErrorButtons = set of TErrorButton;

  TfrmFVBFFCReconcileError = class(TForm)
    Panel1: TPanel;
    btnOK: TBitBtn;
    Panel2: TPanel;
    sbtnDetails: TSpeedButton;
    pnlLongMessage: TPanel;
    mmLongMessage: TMemo;
    btnCancel: TBitBtn;
    btnYes: TBitBtn;
    btnNo: TBitBtn;
    Panel3: TPanel;
    Image1: TImage;
    Panel4: TPanel;
    lblMessage: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure sbtnDetailsClick(Sender: TObject);
  private
    FShowDetail: Boolean;
    FErrorButtons: TErrorButtons;
    FShortMessage: String;
    FLongMessage: String;
    FMsgDlgType: TMsgDlgType;
    FOnShowReconcileError: TShowReconcileErrorEvent;
  protected
    procedure SetShowDetail(const Value: Boolean);
    procedure SetLongMessage(const Value: String);
    procedure SetShortMessage(const Value: String);
    procedure SetErrorButtons(const Value: TErrorButtons);
    procedure SetMsgDlgType(const Value: TMsgDlgType);
    procedure DoShowReconcileError(UpdateKind: TUpdateKind; var aMessage, aLongMessage: String);
    function GetErrorMessage(const msg : String; AppServer: Variant) : String;
    function GetErrorMessageFromDB(const aNumber : Integer; AppServer: Variant) : String; overload;
    function GetErrorMessageFromDB(const aConstraint : String; AppServer: Variant) : String; overload;
  public
    function HandleReconcileError(DataSet: TDataSet; UpdateKind: TUpdateKind;
      ReconcileError: EReconcileError; AppServer: Variant): TReconcileAction;
    function ShowErrorDialog(aShortMessage, aLongMessage: String; DlgType: TmsgDlgType;
      Buttons: TErrorButtons = [ebOK]; HelpCtx: LongInt = 0; ShowDetails: Boolean = False): Word;
    property ShowDetail: Boolean read FShowDetail write SetShowDetail;
    property ErrorButtons: TErrorButtons read FErrorButtons write SetErrorButtons;
    property ShortMessage: String read FShortMessage write SetShortMessage;
    property LongMessage: String read FLongMessage write SetLongMessage;
    property MsgDlgType: TMsgDlgType read FMsgDlgType write SetMsgDlgType;
    property OnShowReconcileError: TShowReconcileErrorEvent read FOnShowReconcileError write FOnShowReconcileError;

    class function GetMessage (const msg: String; AppServer: Variant): String;
  end;

implementation

{$R *.DFM}

resourcestring
  rcReconcileErrorUpdate = 'Error bij het aanpassen van gegevens';
  rcReconcileErrorDelete = 'Error bij het wissen van gegevens.';
  rcReconcileErrorInsert = 'Error bij het toevoegen van gegevens.';
  rcMsgDlgWarning        = 'Waarschuwing';
  rcMsgDlgError          = 'Fout';
  rcMsgDlgInformation    = 'Informatie';
  rcMsgDlgConfirmation   = 'Bevestiging';

procedure TfrmFVBFFCReconcileError.FormCreate(Sender: TObject);
begin
  ShowDetail := false;
  sbtnDetails.Down := false;
  ErrorButtons := [ebOK];
end;

procedure TfrmFVBFFCReconcileError.SetShowDetail(const Value: Boolean);
begin
  FShowDetail := Value;
  if FShowDetail then //show detail
  begin
    Height := 238;
    pnlLongMessage.Height  := 92;
    pnlLongMessage.Visible := true;
    end
  else begin //Don't show detail
    Height := 144;
    pnlLongMessage.Height  := 0;
    pnlLongMessage.Visible := false;
  end;
end;

procedure TfrmFVBFFCReconcileError.SetLongMessage(const Value: String);
begin
  FLongMessage := Value;
  mmLongMessage.Lines.Text := FLongMessage;
end;

procedure TfrmFVBFFCReconcileError.SetShortMessage(const Value: String);
begin
  FShortMessage := Value;
  lblMessage.Caption := FShortMessage;
end;

procedure TfrmFVBFFCReconcileError.sbtnDetailsClick(Sender: TObject);
begin
  ShowDetail := (Sender as TSpeedbutton).Down;
end;

{*****************************************************************************
  Name           : TfrmFVBFFCReconcileError.ShowErrorDialog
  Author         : Lambrechts Wim
  Last Revision  : 18/07/2001
  Arguments      : ShortMessage: Message to be shown in the top part
                                 of the errorform (always visible)
                   LongMessage: Message to be shown in the bottom part (detail)
                   DlgType: one of the DlgTypes (determines the caption of the form)
                   Buttons: Visible buttons
                     ebOK:     if clicked -> returns mrOK
                     ebCancel:                       mrCancel
                     ebYes                           mrYes
                     ebNo                            mrNo
                   HelpCtx: context sensitive help ID (not implemented yet)
  Return Values  : mrOK, mrCancel, mrYes or mrNo
  Description    : Displays an error message screen
  History        :

  Date       By              Description
  ----       --              -----------
  21/08/2003 Wim Verheyen    Added support for automaticaaly showing details
                             when form is created.
  18/07/2001 Lambrechts Wim  Initial creation of the procedure.
 ****************************************************************************}
function TfrmFVBFFCReconcileError.ShowErrorDialog(aShortMessage, aLongMessage: String; DlgType: TmsgDlgType;
  Buttons: TErrorButtons = [ebOK]; HelpCtx: Longint = 0; ShowDetails: Boolean = False): Word;
begin
  //Deal with incomplete messages
  if aShortMessage = '' then
    aShortMessage := aLongMessage;
  if aLongMessage  = '' then
    aLongMessage  := aShortMessage;
  //Display error form
  ErrorButtons := Buttons;
  MsgDlgType   := DlgType;
  ShortMessage := aShortMessage;
  LongMessage  := aLongMessage;
  ShowDetail   := ShowDetails;
  result := ShowModal;
end;

{*****************************************************************************
  Name           : TfrmReconcileError.SetErrorButtons
  Author         : Lambrechts Wim
  Last Revision  : 18/07/2001
  Arguments      : Value: value for property that was set
  Return Values  : None
  Description    : Setter for ErrorButtons-property
                   It sets the private variable FErrorButtons, but also
                   displays the correct buttons
  History        :

  Date       By              Description
  ----       --              -----------
  18/07/2001 Lambrechts Wim  Initial creation of the procedure.
 ****************************************************************************}
procedure TfrmFVBFFCReconcileError.SetErrorButtons(const Value: TErrorButtons);
var
  ButtonLeftPos: Integer;
  Buttons: array [0..3] of TBitBtn;
  lcv: TErrorButton;
  buttonsvisible: integer;
begin
  Buttons[0] := btnOK;
  Buttons[1] := btnCancel;
  Buttons[2] := btnYes;
  Buttons[3] := btnNo;

  FErrorButtons := Value;
  if FErrorButtons * [ebOK, ebYes] = [] then
    FErrorButtons := FErrorButtons + [ebOK]; //at least ebOK or ebYes should have been choosen

  ButtonsVisible := 0; //if only one button visible, then the leftpos should be shifted
  for lcv := ebOK to ebNo do
  begin
    if lcv in FErrorButtons then Inc (ButtonsVisible);
  end;

  if ButtonsVisible > 1 then
    ButtonLeftPos := 160 //left position of the next button to be made visible
  else
    ButtonLeftPos := 240;

  for lcv := ebOK to ebNo do
  begin
    if lcv in FErrorButtons then
    begin
      Buttons[Integer(lcv)].Visible := lcv in FErrorButtons;
      Buttons[Integer(lcv)].Left    := ButtonLeftPos;
      Inc (ButtonLeftPos, 80);
    end
    else Buttons[Integer(lcv)].Visible := false;
  end; //for lcv := ebOK to ebNo do

  self.width := ButtonLeftPos + 20; //ButtonLeftPos is already the position
    //of the button that was not drawn
end;

procedure TfrmFVBFFCReconcileError.SetMsgDlgType(const Value: TMsgDlgType);
begin
  FMsgDlgType := Value;
  case Value of
    mtWarning:      caption := rcMsgDlgWarning;
    mtError:        caption := rcMsgDlgError;
    mtInformation:  caption := rcMsgDlgInformation;
    mtConfirmation: caption := rcMsgDlgConfirmation;
    mtCustom:       caption := Application.ExeName;
  end;
end;

{*****************************************************************************
  Name           : TfrmFVBFFCReconcileError.HandleReconcileError
  Author         : Lambrechts Wim
  Arguments      : Same as 'original'HandleReconcileError, but added
                   'OnShowReconcileError': event that can be triggered
                   to supply a custom made short and long description
  Return Values  : None
  Exceptions     : None
  Description    : OnReconcileError eventhandler fot the clientdataset 'behind'
                   the dataset-property of this datamodule
  History        :

  Date         By                   Description
  ----         --                   -----------
  07/09/2001   Cools Pascal         Added GetErrorMessage : Retreives a more
                                    detailed message from the database.
  17/04/2001   Lambrechts Wim       Initial creation of the Unit.
  08/06/2001   Lambrechts Wim       Created this general function (as similar to
                                    the' 'original' HandleReconcileError

 *****************************************************************************}
function TfrmFVBFFCReconcileError.HandleReconcileError(DataSet: TDataSet;
  UpdateKind: TUpdateKind; ReconcileError: EReconcileError; AppServer: Variant): TReconcileAction;
var
  aMessage, aLongMessage: String;
begin
  //Obtain default error messages
  aLongMessage := ReconcileError.Message;

  { Gets a more detailed message from the database }
  aMessage := GetErrorMessage(ReconcileError.Message, AppServer);

  if aMessage = '' then
  begin
    case UpdateKind of
      ukModify: aMessage := rcReconcileErrorUpdate;
      ukInsert: aMessage := rcReconcileErrorInsert;
      ukDelete: aMessage := rcReconcileErrorDelete;
    end;
  end;

  DoShowReconcileError(UpdateKind, aMessage, aLongMessage);

  //Display reconcile-error form
  ErrorButtons := [ebOK];
  MsgDlgType   := mtError;
  ShortMessage := aMessage;
  LongMessage  := aLongMessage;
  ShowModal;

  result := raAbort;
end;

procedure TfrmFVBFFCReconcileError.DoShowReconcileError(UpdateKind: TUpdateKind; var aMessage, aLongMessage: String);
begin
  if Assigned (OnShowReconcileError) then
  begin
    OnShowReconcileError(UpdateKind, aMessage, aLongMessage);
  end;
end;

{*****************************************************************************
  Name           : TfrmFVBFFCReconcileError.GetErrorMessage
  Author         : Cools Pascal
  Arguments      : msg - String returned when a database error occurs.
  Return Values  : The detailed description of the error message.
  Exceptions     : None
  Description    : This function will examine if the input message concerns
                   a trigger or an other kind of error. In both cases it will
                   look up the appropriate function to retreive the detailed
                   message from the database.
  History        :

  Date         By                   Description
  ----         --                   -----------
  06/09/2001   Cools Pascal         Initial creation of the Unit.
 *****************************************************************************}
function TfrmFVBFFCReconcileError.GetErrorMessage(
  const msg: String; AppServer: Variant): String;
var
  aStart : Integer;
  aStop : Integer;
  aTriggerNumber : Integer;
  aCounter : Integer;
  isFinished : Boolean;
begin

  aStop := 0;

  if (AnsiPos('TR_', msg)>0) then
  begin

    { In case there is a TRIGGER, we need the value that is right behind it,
      this errornumber is needed to look up the right messages. }

    { The message will look like 'TR_MESSAGE 123'. It's not for sure that the
      number will always consist of the last 3 characters of the string;
      therefor we will find the number following the first white space
      after the triggername till the end of the string or the next white space }

    { Find the first position of the name starting with TR_ (as used for
      all trigger error messages through the application. }

    aStart := AnsiPos('TR_', msg);

    aCounter := aStart;
    isFinished := False;

    while (aCounter <= Length(msg)) and (not isFinished) do
    begin

      { Stop when the first white space has been found }

      if (AnsiCompareText(Copy(msg,aCounter,1),' ') = 0) then
      begin
        aStart := aCounter;
        isFinished := True;
      end;

    aCounter := aCounter + 1;

    end;

    aCounter := aStart + 1;
    isFinished := False;

    while (aCounter <= Length(msg)) and (not isFinished) do
    begin

      { Stop when the next white space has been found or when the
        string is finished. }

      if ( (AnsiCompareText(Copy(msg,aCounter,1),' ') = 0)
         or ( aCounter = Length(msg) ) ) then
      begin
        aStop := aCounter;
        isFinished := True;
      end;

    aCounter := aCounter + 1;

    end;

    { We have found the number }
    aTriggerNumber := StrToInt(Trim(Copy(msg,aStart,aStop-aStart+1)));

    { Retreive the detailed error message from the database }
    Result := GetErrorMessageFromDB(aTriggerNumber, AppServer);

  end
  else
  begin

    { The next line is used to retreive the detailed error messages
      from all other db generated error messages. Because all this messages
      are still not translated in an understandable way (T_SYS_MESSAGES);
      the line below is still in comment }

    Result := GetErrorMessageFromDB(msg, AppServer);

  end;

end;

{*****************************************************************************
  Name           : TfrmFVBFFCReconcileError.GetErrorMessageFromDB
  Author         : Cools Pascal
  Arguments      : aNumber - Integer value of the error message to be found.
  Return Values  : Description of the error message
  Exceptions     : None
  Description    : This function calls the function on the server to get
                   the detailed error description from a given text.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/09/2001   Cools Pascal         Initial creation of the Unit.
 *****************************************************************************}
function TfrmFVBFFCReconcileError.GetErrorMessageFromDB(
  const aNumber: Integer; AppServer: Variant): String;
begin
  Result := AppServer.GetErrorMessageFromNumber(aNumber);
end;

{*****************************************************************************
  Name           : TfrmFVBFFCReconcileError.GetErrorMessageFromDB
  Author         : Cools Pascal
  Arguments      : aConstraint - String with a constraint name included.
  Return Values  : Description of the error message
  Exceptions     : None
  Description    : This function calls the function on the server to get
                   the detailed error description from a given number.
  History        :

  Date         By                   Description
  ----         --                   -----------
  10/09/2001   Cools Pascal         Initial creation of the Unit.
 *****************************************************************************}
function TfrmFVBFFCReconcileError.GetErrorMessageFromDB(
  const aConstraint: String; AppServer: Variant): String;
begin
  Result := AppServer.GetErrorMessageFromConstraint(aConstraint);
end;

{*****************************************************************************
  When we need the transformed message without the form

  @Name       TfrmFVBFFCReconcileError.GetErrorMessage
  @author     wlambrec
  @param      None
  @return     None
  @Exception  None
  @See        None
******************************************************************************}
class function TfrmFVBFFCReconcileError.GetMessage(const msg: String;
  AppServer: Variant): String;
var
  frm: TfrmFVBFFCReconcileError;
begin
  frm := TfrmFVBFFCReconcileError.Create (Application);
  try
    result := frm.GetErrorMessage(msg, AppServer);
  finally
    frm.Free;
  end;
end;

end.

