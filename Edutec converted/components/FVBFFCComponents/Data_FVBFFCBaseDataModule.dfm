object FVBFFCBaseDataModule: TFVBFFCBaseDataModule
  OldCreateOrder = False
  AutoDestroy = True
  AutoOpenDataSets = True
  DataSet = cdsList
  RecordDataset = cdsRecord
  FetchOnDemand = True
  FilterFirst = False
  Registered = False
  OnCancelFilter = FVBFFCDataModuleCancelFilter
  OnFilter = FVBFFCDataModuleFilter
  OnFilterDetail = FVBFFCDataModuleFilterDetail
  OnFilterRecord = FVBFFCDataModuleFilterRecord
  OnLinkListView = FVBFFCDataModuleLinkListView
  OnLinkRecordView = FVBFFCDataModuleLinkRecordView
  AllowEmptyFilter = False
  AutoOpenWhenDetail = True
  AutoOpenWhenSelection = True
  OnInitialise = FVBFFCDataModuleInitialise
  OnInitialiseForeignKeyFields = FVBFFCDataModuleInitialiseForeignKeyFields
  Left = 212
  Top = 305
  Height = 324
  Width = 739
  DetailDataModules = <>
  object qryList: TFVBFFCQuery
    Connection = dtmFVBFFCMainClient.adocnnMain
    Parameters = <>
    AutoOpen = False
    Left = 72
    Top = 56
  end
  object qryRecord: TFVBFFCQuery
    Parameters = <>
    AutoOpen = False
    Left = 168
    Top = 56
  end
  object prvList: TFVBFFCDataSetProvider
    DataSet = qryList
    Left = 72
    Top = 104
  end
  object prvRecord: TFVBFFCDataSetProvider
    DataSet = qryRecord
    Left = 168
    Top = 104
  end
  object cdsList: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvList'
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    Left = 72
    Top = 160
  end
  object cdsRecord: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'prvRecord'
    AfterPost = cdsListAfterPost
    BeforeDelete = cdsListBeforeDelete
    AfterDelete = cdsListAfterDelete
    OnNewRecord = cdsListNewRecord
    OnReconcileError = cdsListReconcileError
    AutoOpen = False
    Left = 168
    Top = 160
  end
  object cdsSearchCriteria: TFVBFFCClientDataSet
    Aggregates = <>
    Params = <>
    AutoOpen = False
    Left = 280
    Top = 160
  end
end
