{*****************************************************************************
  This Unit will contain the RecordViewModule descendant for Edutec.
  This RecordViewDescendant will contain the necessary code to add Edutec
  DataAware components to the RecordView form for the fields in the
  RecordDataSet.
  
  @Name       Unit_FVBFFCRecordViewModule
  @Author     slesage
  @Copyright  (c) 2004 USB
  @History

  Date         By                   Description
  ----         --                   -----------
  17/11/2004   slesage              Initial creation of the Unit.
******************************************************************************}

unit Unit_FVBFFCRecordViewModule;

interface

uses
  Unit_PPWFrameWorkCustomModules, Form_PPWFrameWorkAddDataAwareControlsWizard,
  DB, Classes, Form_FVBFFCAddDataAwareControlsWizard;

type
  { Edutec RecordViewModule Descendant which implements the code necessary to
    add Edutec specific DataAware components to the RecordView Form }
  TFVBFFCRecordViewModule = class( TPPWFrameWorkRecordViewModule )
  protected
    function DefaultWizardClass : TDBAwareControlWizardClass; override;
    function DefaultLabelClass  : TComponentClass; override;

//    procedure CreateDBAwareComponents( aParent : TComponent; aDataSource : TDataSource; aFields : TFields; aFieldDefs : TFieldDefs ); override;
  end;

implementation

uses
  {$IFDEF CODESITE}
  Unit_EdutecCodeSite,
  {$ENDIF}
  Graphics, Controls, SysUtils, TypInfo, cxDBEdit, cxDropDownEdit,
  Unit_PPWFrameWorkInterfaces,
  Unit_FVBFFCDevExpress, Unit_FVBFFCRecordView;

{ TFVBFFCRecordViewModule }

{*****************************************************************************
  This method will be executed when the PIF.AddDataAwareComponents menu item
  is clicked.  It will show the EdutecAddDataAwareControls Wizard which will
  guide the developer through the process of adding the Edutec specific DataAware
  components to the RecordView form.

  @Name       TFVBFFCRecordViewModule.CreateDBAwareComponents
  @author     slesage
  @param      aFields   The TFields for which we need to create DBAware
                        components
  @param      aParent   The Parent component on which the new DBAware
                        components should reside.
  @return     None
  @Exception  None
  @See        None
******************************************************************************}

//procedure TFVBFFCRecordViewModule.CreateDBAwareComponents(aParent : TComponent; aDataSource : TDataSource; aFields : TFields; aFieldDefs : TFieldDefs);
//var
//  lcv : Integer;
//  aLabel : TControl;
//  aEdit  : TWinControl;
//
//  aDataBinding : TcxDBEditDataBinding;
//
//  aTop   , aLeft  : Integer;
//  aWidth          : Integer;
//  aMaxCaptionWidth: Integer;
//  aDBLeft         : Integer;
//  aRecordView     : IPPWFrameWorkRecordView;
//  aDBAwareClass   : TComponentClass;
//  aDBAwareVisible : Boolean;
//begin
//  { First make sure the procedure was triggered on a FrameWorkRecordView }
//  if ( Supports( Root, IPPWFrameWorkRecordView, aRecordView ) ) then
//  begin
//    { Now Create and Show the wizard so the user can specify all the options }
//    frmAddDataAwareControlsWizard := TfrmAddDataAwareControlsWizard.Create( Nil );
//    try
//      frmAddDataAwareControlsWizard.RecordDataSet := aRecordView.DataSource.DataSet;
//      frmAddDataAwareControlsWizard.InitialiseSettings;
//
//      { If the user closed the Wizard  using the OK button, we can continue the
//        process }
//      if ( frmAddDataAwareControlsWizard.ShowModal = mrOK ) then
//      begin
//        { By default the label components should start at 8,8 in the Parent Container }
//        aTop  := 8;
//        aLeft := 8;
//        aWidth:= 121;
//        aMaxCaptionWidth := MaxFieldCaptionLength( aFields );
//
//        { Now set the intial Left Position for our DBAware controls according
//          to the MaxCaptionWidth }
//        aDBLeft := 24 + ( ( ( aMaxCaptionWidth div 8 ) + 1 ) * 8 );
//
//        { Loop over all fields to create the Label and DBAwareComponent }
//        for lcv := 0 to Pred( aFields.Count ) do
//        begin
//          { Get some settings from the Wizard form }
//          aDBAwareClass := frmAddDataAwareControlsWizard.GetDBAwareComponentClass( aFields[ lcv ] );
//          aDBAwareVisible := frmAddDataAwareControlsWizard.GetDBAwareComponentVisible( aFields[ lcv ] );
//
//          { Only create the components if the user indicated he wants to see them }
//          if aDBAwareVisible then
//          begin
//            { Now create the Label and the DBAware Control }
//            aLabel := TControl   ( Designer.CreateComponent( TFVBFFCLabel, aParent, aLeft , aTop, aMaxCaptionWidth, 17 ) );
//            aEdit  := TWinControl( Designer.CreateComponent( aDBAwareClass, aParent, aDBLeft, aTop, aWidth, 21 ) );
//
//            { Now Set the Label Properties }
//            aLabel.Name        := 'cxlbl' + aFields[ lcv ].FieldName;
//            aLabel.HelpType    := htKeyWord;
//            aLabel.HelpKeyword := Root.Name + '.' + aFields[ lcv ].FieldName;
//
//            { Set the additional properties using RTTI }
//            if ( IsPublishedProp( aLabel, 'FocusControl' ) ) then
//            begin
//              SetObjectProp( aLabel, 'FocusControl', aEdit );
//            end;
//            if ( IsPublishedProp( aLabel, 'Caption' ) ) then
//            begin
//              SetStrProp( aLabel, 'Caption', aFields[ lcv ].DisplayLabel );
//            end;
//
//            { Now set the Edit Properites }
//
//            aEdit.Name        := {'cxlbl' +} aFields[ lcv ].FieldName;
//            aEdit.HelpType    := htKeyWord;
//            aEdit.HelpKeyword := Root.Name + '.' + aFields[ lcv ].FieldName;
//
//            { Set the additional properties using RTTI }
//            if ( IsPublishedProp( aEdit, 'DataBinding' ) ) then
//            begin
//              aDataBinding := TcxDBEditDataBinding( GetObjectProp( aEdit, 'DataBinding' ) );
//              SetObjectProp( aDataBinding, 'DataSource', aDataSource );
//              SetStrProp   ( aDataBinding, 'DataField' , aFields[ lcv ].FieldName );
//            end;
//
//            if ( aEdit is TcxCustomDropDownEdit ) then
//            begin
//              TFVBFFCDBTextEdit( aEdit ).Width := aWidth + 16;
//            end;
//
//            { Now increment the Top position for the next control }
//            inc( aTop, ( ( ( aEdit.Height div 8 ) + 1 ) * 8 ) );
//          end;
//        end;
//      end;
//    finally
//      freeAndNil( frmAddDataAwareControlsWizard );
//    end;
//  end;
//end;

function TFVBFFCRecordViewModule.DefaultLabelClass: TComponentClass;
begin
  Result := TFVBFFCLabel;
end;

function TFVBFFCRecordViewModule.DefaultWizardClass: TDBAwareControlWizardClass;
begin
  Result := TFVBFFCAddDataAwareControlsWizard;
end;

end.
